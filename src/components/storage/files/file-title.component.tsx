import React from "react";

export interface IFileTitleProps {
  title: string;
  subtitle: string;
}

export const FileTitleComponent = ({ title, subtitle }: IFileTitleProps) => {
  return (
    <>
      <h4 className="mb-3">{title}</h4>
      <h6>{subtitle}</h6>
    </>
  );
};
