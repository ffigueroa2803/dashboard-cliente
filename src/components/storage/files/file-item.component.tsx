/* eslint-disable no-unused-vars */
import { useConfirm } from "@common/confirm/use-confirm";
import { storageRequest } from "@services/storage.request";
import React from "react";
import { toast } from "react-toastify";
import { useFileDeleteService } from "src/application/storage/files/file-delete.service";
import { FileEntity } from "src/domain/storage/files/file.entity";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { FileIconComponent } from "./file-icon.component";

const request = storageRequest();

export interface IFileProps {
  file: IFileEntityInterface;
  onDelete: (file: IFileEntityInterface) => void;
}

export const FileItemComponent = ({ file, onDelete }: IFileProps) => {
  const fileEntity = new FileEntity(file);

  const confirm = useConfirm();
  const fileDelete = useFileDeleteService(fileEntity);

  const handleDelete = () => {
    confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en eliminar el archivo?",
        labelSuccess: "Eliminar",
        labelError: "Cancelar",
      })
      .then(() =>
        fileDelete
          .execute()
          .then(() => {
            toast.success("El archivo se eliminó correctamente!");
            onDelete(file);
          })
          .catch(() => toast.error("No se pudo eliminar el archivo"))
      )
      .catch(() => null);
  };

  const handleView = () => {
    const a = document.createElement("a");
    a.href = request.generateUrl(`files/${fileEntity.id}/binary`);
    a.target = "_blank";
    a.title = fileEntity.name;
    a.click();
  };

  return (
    <li className="file-box mb-2">
      <div
        className="file-top cursor-pointer"
        onDoubleClick={handleView}
        title="Doble click para ver el archivo"
      >
        <FileIconComponent extname={fileEntity.extname} />
        <i
          className="fa fa-times cursor-pointer f-14 ellips"
          onClick={handleDelete}
        ></i>
      </div>
      <div className="file-bottom">
        <h6>{fileEntity.name}</h6>
        <p className="mb-1">{fileEntity.displaySize()}</p>
        <p>
          <b>creado : </b>
          {fileEntity.displayCreatedAt()}
        </p>
      </div>
    </li>
  );
};
