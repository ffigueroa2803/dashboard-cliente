import React, { FC } from "react";
import { CardBody } from "reactstrap";

export const FileContentComponent: FC = ({ children }) => {
  return (
    <div className="file-content">
      <CardBody className="file-manager py-1">{children || null}</CardBody>
    </div>
  );
};
