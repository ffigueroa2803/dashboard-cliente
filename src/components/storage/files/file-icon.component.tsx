export interface IFileIconProps {
  extname: string;
}

const fileIconSupported = (extname: string): string => {
  const data: any = {
    txt: "fa fa-file-text-o text-info",
    pdf: "fa fa-file-pdf-o text-secondary",
    zip: "fa fa-file-archive-o text-warning",
    rar: "fa fa-file-archive-o text-secondary",
    tar: "fa fa-file-archive-o text-success",
    xls: "fa fa-file-excel-o text-success",
    csv: "fa fa-file-excel-o text-warning",
    xlsx: "fa fa-file-excel-o text-success",
    doc: "fa fa-file-word-o text-primary",
    docx: "fa fa-file-word-o text-primary",
    docs: "fa fa-file-word-o text-primary",
    dir: "fa fa-folder f-36 text-warning",
    png: "fa fa-file-image-o text-danger",
    jpg: "fa fa-file-image-o text-primary",
    jpge: "fa fa-file-image-o text-primary",
  };

  return data[extname] || "fa fa-file-text text-muted";
};

export const FileIconComponent = ({ extname }: IFileIconProps) => {
  const iconSupported = fileIconSupported(extname);
  return <i className={`${iconSupported} cursor-pointer`}></i>;
};
