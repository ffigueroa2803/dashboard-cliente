import React from "react";

export interface IFileListProps {
  title?: string;
  children?: any;
}

export const FileListComponent = ({ title, children }: IFileListProps) => {
  return (
    <>
      <h6 className="mt-4">{title || ""}</h6>
      <ul className="files">{children || null}</ul>
    </>
  );
};
