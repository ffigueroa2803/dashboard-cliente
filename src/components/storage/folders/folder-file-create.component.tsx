/* eslint-disable no-unreachable */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { useEffect, useState } from "react";
import { Trash, Upload } from "react-feather";
import { useFolderFileCreateService } from "src/application/storage/folders/folder-file-create.service";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import xbytes from "xbytes";
import { UUIDv4 } from "uuid-v4-validator";

export class FileBuffer {
  id!: string;
  buffer!: File;
  constructor() {
    this.id = UUIDv4.generate();
  }
}

export interface IFolderFileCreateProps {
  folder: IFolderEntityInterface;
  file: FileBuffer;
  onSave: (buffer: FileBuffer, file: IFileEntityInterface) => void;
  onCancel: (buffer: FileBuffer) => void;
}

export const FolderFileCreateComponent = ({
  folder,
  file,
  onSave,
  onCancel,
}: IFolderFileCreateProps) => {
  const folderFileCreate = useFolderFileCreateService(folder);
  const [isUpload, setIsUpload] = useState<boolean>(false);

  const handle = () => {
    folderFileCreate
      .execute(file.buffer)
      .then((data) => {
        setIsUpload(true);
        setTimeout(() => onSave(file, data), 2000);
      })
      .catch(() => null);
  };

  const backgroundProgress = () => {
    if (folderFileCreate.isError) return "bg-danger";
    return isUpload ? "bg-success" : "bg-primary";
  };

  const colorProgress = () => {
    if (folderFileCreate.isError) return "text-danger";
    return isUpload ? "text-success" : "text-primary";
  };

  const percentProgress = () => {
    if (folderFileCreate.isError) return "100%";
    return `${folderFileCreate.percent}%`;
  };

  const handleCancel = () => {
    folderFileCreate.abort();
    onCancel(file);
  };

  useEffect(() => {
    handle();
    return () => folderFileCreate.abort();
  }, []);

  return (
    <div className="mt-3">
      <div>
        <Show condition={folderFileCreate.pending || folderFileCreate.isError}>
          <span className="close cursor-pointer" style={{ opacity: 1 }}>
            <Trash className="icon text-danger" onClick={handleCancel} />
          </span>
        </Show>
        <Upload className={`icon ${colorProgress()}`} />
        {file.buffer.name}
      </div>
      <div className="m-t-15">
        <div className="progress sm-progress-bar mb-1">
          <div
            className={`progress-bar ${backgroundProgress()}`}
            role="progressbar"
            style={{ width: percentProgress() }}
          ></div>
        </div>
        <p className="text-right">Tamaño: {xbytes(file.buffer.size)}</p>
      </div>
      <hr />
    </div>
  );
};
