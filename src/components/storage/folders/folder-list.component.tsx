export interface IFolderListProps {
  title?: string;
  children?: any;
}

export const FolderListComponent = ({ title, children }: IFolderListProps) => {
  return (
    <>
      <h6 className="mt-4">{title || ""}</h6>
      <ul className="folder folder-list">{children || null}</ul>
    </>
  );
};
