export enum EnumFolderSupported {
  "default" = "fa fa-folder f-36 txt-warning",
  "compress" = "fa fa-file-archive-o f-36 txt-warning",
}

export interface IFolderIconProps {
  type?: EnumFolderSupported;
}

export const FolderIconComponent = ({
  type = EnumFolderSupported.default,
}: IFolderIconProps) => {
  return <i className={`${type}`}></i>;
};
