/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { useEffect, useState } from "react";
import { useFolderFindService } from "src/application/storage/folders/folder-find.service";
import { FileContentComponent } from "src/components/storage/files/file-content.component";
import { FolderITemComponent } from "src/components/storage/folders/folder-item.component";
import { FolderListComponent } from "src/components/storage/folders/folder-list.component";
import { FolderModalFilesComponent } from "src/components/storage/folders/folder-modal-files.component";
import { FolderEntity } from "src/domain/storage/folders/folder.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface IFolderInfoProps {
  id: string;
}

export const FolderInfoComponent = ({ id }: IFolderInfoProps) => {
  const folderFindService = useFolderFindService();
  const [folder, setFolder] = useState<IFolderEntityInterface>();
  const [openForder, setOpenFolder] = useState<boolean>(false);

  const handle = () => {
    folderFindService
      .execute(id)
      .then((data) => {
        const newFolder = new FolderEntity(data);
        setFolder(newFolder);
      })
      .catch(() => null);
  };

  useEffect(() => {
    if (id) handle();
  }, [id]);

  return (
    <>
      <Show
        condition={!folderFindService.pending}
        isDefault={
          <div className="text-center">
            <b className="text-muted">Recuperando folder...</b>
          </div>
        }
      >
        <Show
          condition={typeof folder != "undefined"}
          isDefault={
            <div className="text-center mt-3">
              <b className="text-muted">El folder fué eliminado!!!</b>
            </div>
          }
        >
          <FileContentComponent>
            <FolderListComponent>
              <FolderITemComponent
                folder={folder as any}
                onClick={() => setOpenFolder(true)}
                fluid
              />
            </FolderListComponent>
          </FileContentComponent>
        </Show>
      </Show>
      {/* modal */}
      <Show condition={typeof folder != "undefined"}>
        <FolderModalFilesComponent
          folder={folder as any}
          isOpen={openForder}
          toggle={() => setOpenFolder(false)}
          onRefresh={handle}
        />
      </Show>
    </>
  );
};
