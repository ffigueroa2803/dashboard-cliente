/* eslint-disable no-unused-vars */
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { FolderIconComponent } from "./folder-icon.component";

export interface IFolderItemProps {
  folder: IFolderEntityInterface;
  fluid?: boolean;
  onClick?: (e: any, folder: IFolderEntityInterface) => void | undefined;
}

export const FolderITemComponent = ({
  folder,
  fluid,
  onClick,
}: IFolderItemProps) => {
  const executeOnClick = (e: any) => {
    if (typeof onClick == "function") {
      onClick(e, folder);
    }
  };

  return (
    <li className={`folder-box file-manager ${fluid ? "fluid" : ""}`}>
      <div
        onClick={executeOnClick}
        className={`media ${
          typeof onClick == "function" ? "cursor-pointer" : ""
        }`}
      >
        <FolderIconComponent />
        <div className="media-body ms-3 ml-2">
          <h6 className="mb-0">{folder.displayTitle()}</h6>
          <p>
            {folder.displayTotalFiles()}, {folder.displayOveallSize()}
          </p>
        </div>
      </div>
    </li>
  );
};
