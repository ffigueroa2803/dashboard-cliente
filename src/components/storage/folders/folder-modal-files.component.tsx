/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { useFolderFileListService } from "src/application/storage/folders/folder-file-list.service";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { FileContentComponent } from "../files/file-content.component";
import { FileItemComponent } from "../files/file-item.component";
import { FileListComponent } from "../files/file-list.component";
import { FileTitleComponent } from "../files/file-title.component";
import { FolderFileUploadComponent } from "./folder-file-upload.component";

export interface IFolderModalFilesProps {
  isOpen?: boolean | undefined;
  toggle?(): void;
  folder: IFolderEntityInterface;
  onRefresh?: () => void;
}

export const FolderModalFilesComponent = ({
  isOpen,
  toggle,
  folder,
  onRefresh,
}: IFolderModalFilesProps) => {
  const [files, setFiles] = useState<IFileEntityInterface[]>([]);

  const folderFileList = useFolderFileListService(folder);

  const handleList = () => {
    folderFileList
      .execute()
      .then((data) => setFiles(data))
      .catch(() => null);
  };

  const handleRefresh = () => {
    if (typeof onRefresh == "function") onRefresh();
  };

  const handleUpload = (file: IFileEntityInterface) => {
    setFiles((prev) => [...prev, file]);
  };

  const handleDelete = (file: IFileEntityInterface) => {
    setFiles((prev) => prev.filter((item) => file.id != item.id));
    handleRefresh();
  };

  useEffect(() => {
    if (folder.id && isOpen) handleList();
  }, [folder.id, isOpen]);

  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalHeader toggle={toggle}>
        <i className="fa fa-folder text-warning"></i> {folder.displayTitle()}
      </ModalHeader>
      <ModalBody>
        {/* subir archivos */}
        <FolderFileUploadComponent
          folder={folder}
          onUpload={handleUpload}
          onReady={handleRefresh}
        />
        {/* lista de archivos */}
        <FileContentComponent>
          <FileTitleComponent
            title="Lista de archivos"
            subtitle={`ID: ${folder.id}`}
          />
          <small>
            <i className="fa fa-files-o"></i> {folder.displayTotalFiles()},{" "}
            {folder.displayOveallSize()}
          </small>
          <FileListComponent>
            {files.map((file) => (
              <FileItemComponent
                file={file}
                key={file.id}
                onDelete={handleDelete}
              />
            ))}
          </FileListComponent>
        </FileContentComponent>
      </ModalBody>
    </Modal>
  );
};
