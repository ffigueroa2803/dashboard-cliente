/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Dropzone } from "@common/dropzone";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { useCallback, useEffect, useState } from "react";
import { AlertCircle } from "react-feather";
import { toast } from "react-toastify";
import { useWorkspaceFindService } from "src/application/storage/workspaces/workspace-find.service";
import { FileEntity } from "src/domain/storage/files/file.entity";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { WorkspaceEntity } from "src/domain/storage/workspaces/workspace.entity";
import {
  FileBuffer,
  FolderFileCreateComponent,
} from "./folder-file-create.component";

interface IFolderFileUploadProps {
  folder: IFolderEntityInterface;
  onUpload: (file: IFileEntityInterface) => void;
  onReady: () => void;
}

export const FolderFileUploadComponent = ({
  folder,
  onUpload,
  onReady,
}: IFolderFileUploadProps) => {
  const [workspace, setWorkspace] = useState<WorkspaceEntity>();
  const [uploads, setUploads] = useState<FileBuffer[]>([]);
  const [isUpload, setIsUpload] = useState<boolean>(false);
  const workspaceFindService = useWorkspaceFindService();

  const handle = () => {
    workspaceFindService
      .execute(folder.workspaceId)
      .then((data) => setWorkspace(new WorkspaceEntity(data)))
      .catch(() => null);
  };

  const onSave = (buffer: FileBuffer, file: IFileEntityInterface) => {
    // elimnar de la subida
    setUploads((prev) => {
      return prev?.filter((item) => item.id != buffer.id);
    });
    // emit event
    onUpload(file);
  };

  const onCancel = (buffer: FileBuffer) => {
    // eliminar de la subida
    setUploads((prev) => {
      return prev?.filter((item) => item.id != buffer.id);
    });
  };

  const handleReady = () => {
    if (!uploads.length) {
      onReady();
      setIsUpload(false);
    }
  };

  useEffect(() => {
    if (folder?.id) handle();
  }, [folder]);

  useEffect(() => {
    if (isUpload) handleReady();
  }, [isUpload, uploads]);

  if (workspaceFindService.pending) {
    return (
      <div className="text-center">
        <LoadingSimple loading />
      </div>
    );
  }

  if (workspace == undefined) {
    return (
      <div className="text-center">
        <div>
          <AlertCircle className="text-danger" />
        </div>
        <h6 className="text-muted">
          No se pudo recuperar el espacio de trabajo
        </h6>
        <hr />
      </div>
    );
  }

  const uploadFile = (files: File[]) => {
    if (!files.length) return;
    const fileAcceptables: FileBuffer[] = [];
    // process file
    files.forEach((tmpFile) => {
      try {
        const newFile = new FileEntity();
        newFile.name = tmpFile.name;
        newFile.mimetype = tmpFile.type;
        newFile.size = tmpFile.size;
        workspace.verifyValidation(newFile);
        workspace.verifyLimit(newFile);
        const buffer = new FileBuffer();
        buffer.buffer = tmpFile;
        fileAcceptables.push(buffer);
      } catch (error: any) {
        toast.error(`${error.message}`);
      }
    });
    // save
    setUploads((prev) => {
      if (!prev?.length) return fileAcceptables;
      return [...prev, ...fileAcceptables];
    });
    setIsUpload(true);
  };

  return (
    <>
      <Dropzone
        title={`Subir archivos a ${folder.displayTitle()}`}
        onFiles={uploadFile}
        multiple={true}
        accept={workspace.displayValidations()}
        files={[]}
      />
      {/* mostrar archivos a subir */}
      {uploads?.map((up, index) => (
        <FolderFileCreateComponent
          onSave={onSave}
          onCancel={onCancel}
          key={`folder-file-create-${index}`}
          folder={folder}
          file={up}
        />
      ))}
    </>
  );
};
