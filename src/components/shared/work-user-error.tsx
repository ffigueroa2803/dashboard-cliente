import { Info } from "react-feather";

export const WorkUserError = () => {
  return (
    <div className="text-center mt-4">
      <div>
        <Info className="text-danger" />
      </div>
      <b className="text-muted">El trabajador no tiene usuario</b>
      <div>
        <small>contacte al administrador del sistema</small>
      </div>
    </div>
  );
};
