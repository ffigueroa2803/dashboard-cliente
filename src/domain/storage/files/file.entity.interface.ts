export interface IFileEntityInterface {
  id: string;
  name: string;
  size: number;
  mimetype: string;
  extname: string;
  createdAt: string;
  displaySize(): string;
  displayCreatedAt(): string;
}
