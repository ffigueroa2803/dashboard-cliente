import { IFileEntityInterface } from "./file.entity.interface";
import xbytes from "xbytes";
import { UUIDv4 } from "uuid-v4-validator";
import { DateTime } from "luxon";

export class FileEntity implements IFileEntityInterface {
  id!: string;
  name!: string;
  size!: number;
  mimetype!: string;
  extname!: string;
  createdAt!: string;

  constructor(partial?: Partial<any>) {
    this.id = UUIDv4.generate();
    if (partial) Object.assign(this, partial);
  }

  displaySize(): string {
    return xbytes(this.size);
  }

  displayCreatedAt(): string {
    return DateTime.fromISO(this.createdAt).toRelative() || "N/A";
  }
}
