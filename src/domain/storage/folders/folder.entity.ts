import xbytes from "xbytes";
import { IFolderEntityInterface } from "./folder.entity.interface";

export class FolderEntity implements IFolderEntityInterface {
  id!: string;
  name!: string;
  overallSize!: number;
  totalFiles!: number;
  workspaceId!: string;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  private title!: string;

  setTitle(title: string) {
    this.title = title;
  }

  displayOveallSize(): string {
    return xbytes(this.overallSize);
  }

  displayTotalFiles(): string {
    return `${this.totalFiles} Files`;
  }

  displayTitle(): string {
    return this.title || this.name;
  }
}
