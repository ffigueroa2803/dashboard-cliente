export interface IFolderEntityInterface {
  id: string;
  name: string;
  overallSize: number;
  totalFiles: number;
  workspaceId: string;
  displayOveallSize(): string;
  displayTotalFiles(): string;
  displayTitle(): string;
}
