import { IFileEntityInterface } from "../files/file.entity.interface";
import { IWorkspaceEntityInterface } from "./workspace.entity.interface";

export class WorkspaceEntity implements IWorkspaceEntityInterface {
  id!: string;
  name!: string;
  uploadLimit!: number;
  validations!: string[];

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  verifyValidation(file: IFileEntityInterface) {
    if (!this.validations?.length) return;
    if (!this.validations.includes(file.mimetype)) {
      throw new Error("El tipo del archivo es invalido");
    }
  }

  verifyLimit(file: IFileEntityInterface) {
    if (this.uploadLimit < file.size) {
      throw new Error("El archivo supera el limite de subida");
    }
  }

  displayValidations(): string {
    if (!this.validations?.length) return "";
    return this.validations.join(",");
  }
}
