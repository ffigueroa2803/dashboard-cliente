/* eslint-disable no-unused-vars */
import { IFileEntityInterface } from "../files/file.entity.interface";

export interface IWorkspaceEntityInterface {
  id: string;
  name: string;
  uploadLimit: number;
  validations: string[];
  verifyValidation: (file: IFileEntityInterface) => void;
  verifyLimit: (file: IFileEntityInterface) => void;
  displayValidations(): string;
}
