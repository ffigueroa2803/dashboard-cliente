/* eslint-disable no-unused-vars */
import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { AfpEntity } from "@modules/scale/afps/dtos/afp.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface IWorkEntityInterface {
  id: number;
  personId: number;
  afpId: number;
  affiliationOfDate?: string;
  numberOfCussp?: string;
  isPrimaSeguro: boolean;
  numberOfEssalud?: string;
  dateOfAdmission: string;
  orderBy: string;
  isBonificationOtherEntity: boolean;
  folderId?: string;
  state: boolean;
  person?: PersonEntity;
  afp?: AfpEntity;
  setFolder: (folder: IFolderEntityInterface) => void;
  getFolder: () => IFolderEntityInterface | undefined;
}
