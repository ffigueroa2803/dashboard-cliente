import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { AfpEntity } from "@modules/scale/afps/dtos/afp.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IWorkEntityInterface } from "./work.entity.interface";

export class WorkEntity implements IWorkEntityInterface {
  id!: number;
  personId!: number;
  afpId!: number;
  affiliationOfDate?: string | undefined;
  numberOfCussp?: string | undefined;
  isPrimaSeguro!: boolean;
  numberOfEssalud?: string | undefined;
  dateOfAdmission!: string;
  orderBy!: string;
  isBonificationOtherEntity!: boolean;
  folderId?: string;
  state!: boolean;
  person?: PersonEntity | undefined;
  afp?: AfpEntity | undefined;

  private folder: IFolderEntityInterface | undefined;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface): void {
    this.folder = folder;
    this.folderId = folder.id;
  }

  getFolder(): IFolderEntityInterface | undefined {
    return this.folder;
  }
}
