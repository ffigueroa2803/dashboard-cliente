import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IJustificationEntityInterface } from "./justification.entity.interface";

export class JustificationEntity implements IJustificationEntityInterface {
  id!: number;
  title!: string;
  description!: string;
  folderId?: string;
  folder?: IFolderEntityInterface;

  fill(partial: Partial<any>): void {
    Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface): void {
    this.folder = folder;
    this.folderId = folder.id;
  }
}
