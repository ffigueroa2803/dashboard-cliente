/* eslint-disable no-unused-vars */
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface IJustificationEntityInterface {
  id: number;
  title: string;
  description: string;
  folderId?: string;
  folder?: IFolderEntityInterface;
  fill(partial: Partial<any>): void;
  setFolder(folder: IFolderEntityInterface): void;
}
