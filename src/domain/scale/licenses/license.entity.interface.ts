/* eslint-disable no-unused-vars */
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { ITypeLicenseEntity } from "@modules/scale/type-licenses/dtos/type-license.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface ILicenseEntityInterface {
  id: number;
  contractId: number;
  typeLicenseId: number;
  resolution: string;
  dateOfResolution: string;
  dateOfAdmission: string;
  terminationDate: string;
  daysUsed: number;
  description: string;
  folderId?: string;
  isPay: boolean;
  state: boolean;
  contract?: IContractEntity;
  typeLicense?: ITypeLicenseEntity;
  setFolder: (folder: IFolderEntityInterface) => void;
  getFolder: () => IFolderEntityInterface | undefined;
}
