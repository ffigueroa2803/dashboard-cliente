import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { ITypeLicenseEntity } from "@modules/scale/type-licenses/dtos/type-license.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { ILicenseEntityInterface } from "./license.entity.interface";

export class LicenseEntity implements ILicenseEntityInterface {
  id!: number;
  contractId!: number;
  typeLicenseId!: number;
  resolution!: string;
  dateOfResolution!: string;
  dateOfAdmission!: string;
  terminationDate!: string;
  daysUsed!: number;
  description!: string;
  folderId?: string;
  isPay!: boolean;
  state!: boolean;
  contract?: IContractEntity | undefined;
  typeLicense?: ITypeLicenseEntity | undefined;

  private folder: IFolderEntityInterface | undefined;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface) {
    this.folder = folder;
    this.folderId = folder.id;
  }

  getFolder() {
    return this.folder;
  }
}
