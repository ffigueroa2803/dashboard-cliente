/* eslint-disable no-unused-vars */
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IContractEntityInterface } from "./contract.entity.interface";
import { ContractConditionEnum } from "./contract.enum";

export class ContractEntity implements IContractEntityInterface {
  id!: number;
  workId!: number;
  dependencyId!: number;
  profileId!: number;
  typeCategoryId!: number;
  condition!: ContractConditionEnum;
  ley?: string | undefined;
  plaza?: string | undefined;
  codeAIRHSP?: string | undefined;
  hourhandId!: number;
  code!: string;
  resolution!: string;
  dateOfResolution!: string;
  dateOfAdmission!: string;
  terminationDate!: string;
  observation?: string | undefined;
  hours!: number;
  folderId?: string | undefined;
  state!: boolean;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  private folder!: IFolderEntityInterface;

  setFolder(folder: IFolderEntityInterface): void {
    this.folder = folder;
    this.folderId = folder.id;
  }

  getFolder(): IFolderEntityInterface {
    return this.folder;
  }
}
