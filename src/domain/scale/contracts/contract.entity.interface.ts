/* eslint-disable no-unused-vars */
import { IDependencyEntity } from "@modules/auth/dependencies/dtos/dependency.entity";
import { IHourhandEntity } from "@modules/scale/hourhands/dtos/hourhand.entity";
import { IProfileEntity } from "@modules/scale/profiles/dtos/profile.entity";
import { ITypeCategoryEntity } from "@modules/scale/type-categories/dtos/type-category.entity";
import { WorkEntity } from "@modules/scale/works/dtos/work.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { ContractConditionEnum } from "./contract.enum";

export interface IContractEntityInterface {
  id: number;
  workId: number;
  dependencyId: number;
  profileId: number;
  typeCategoryId: number;
  condition: ContractConditionEnum;
  ley?: string;
  plaza?: string;
  hourhandId: number;
  code: string;
  resolution: string;
  dateOfResolution: string;
  dateOfAdmission: string;
  terminationDate: string;
  observation?: string;
  hours: number;
  folderId?: string;
  state: boolean;
  work?: WorkEntity;
  typeCategory?: ITypeCategoryEntity;
  dependency?: IDependencyEntity;
  profile?: IProfileEntity;
  hourhand?: IHourhandEntity;
  setFolder(folder: IFolderEntityInterface): void;
  getFolder(): IFolderEntityInterface;
}
