import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IMeritEntityInterface } from "./merit.entity.interface";
import { MeritModeEnum } from "./merit.enum";

export class MeritEntity implements IMeritEntityInterface {
  id!: number;
  contractId!: number;
  title!: string;
  description!: string;
  documentNumber!: string;
  documentDate!: string;
  startDate!: string;
  terminationDate!: string;
  mode!: MeritModeEnum;
  folderId?: string | undefined;

  private folder: IFolderEntityInterface | undefined;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface) {
    this.folder = folder;
    this.folderId = folder.id;
  }

  getFolder(): IFolderEntityInterface | undefined {
    return this.folder;
  }
}
