/* eslint-disable no-unused-vars */
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { MeritModeEnum } from "./merit.enum";

export interface IMeritEntityInterface {
  id: number;
  contractId: number;
  title: string;
  description: string;
  documentNumber: string;
  documentDate: string;
  startDate: string;
  terminationDate: string;
  mode: MeritModeEnum;
  folderId?: string;
  setFolder: (folder: IFolderEntityInterface) => void;
  getFolder: () => IFolderEntityInterface | undefined;
}
