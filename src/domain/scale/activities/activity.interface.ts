/* eslint-disable no-unused-vars */
export interface IActivityInterface {
  load(partial: Partial<any>): IActivityInterface;
  getId(): string;
  getType(): string;
  setType(type: string): void;
  getTitle(): string;
  setTitle(title: string): void;
  getDescription(): string;
  setDescription(descripcion: string): void;
}
