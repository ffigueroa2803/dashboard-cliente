import { IActivityInterface } from "./activity.interface";

export class ActivityEntity implements IActivityInterface {
  private id!: string;
  private type!: string;
  private title!: string;
  private description!: string;

  getId(): string {
    return this.id;
  }

  getType(): string {
    return this.type;
  }

  setType(type: string): void {
    this.type = type;
  }

  getTitle(): string {
    return this.title;
  }

  setTitle(title: string): void {
    this.title = title;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(descripcion: string): void {
    this.description = descripcion;
  }

  load(partial: Partial<any>): IActivityInterface {
    Object.assign(this, partial);
    return this;
  }
}
