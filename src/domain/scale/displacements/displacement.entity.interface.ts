/* eslint-disable no-unused-vars */
import { IDependencyEntity } from "@modules/auth/dependencies/dtos/dependency.entity";
import { IProfileEntity } from "@modules/scale/profiles/dtos/profile.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface IDisplacementEntityInterface {
  id: number;
  contractId: number;
  sourceDependencyId: number;
  sourceProfileId: number;
  targetDependencyId: number;
  targetProfileId: number;
  date: string;
  resolutionDate: string;
  resolutionNumber: string;
  folderId?: string;
  sourceDependency?: IDependencyEntity;
  sourceProfile?: IProfileEntity;
  targetDependency?: IDependencyEntity;
  targetProfile?: IProfileEntity;
  setFolder: (folder: IFolderEntityInterface) => void;
  getFolder: () => IFolderEntityInterface | undefined;
}
