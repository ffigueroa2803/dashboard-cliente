import { IDependencyEntity } from "@modules/auth/dependencies/dtos/dependency.entity";
import { IProfileEntity } from "@modules/scale/profiles/dtos/profile.entity";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IDisplacementEntityInterface } from "./displacement.entity.interface";

export class DisplacementEntity implements IDisplacementEntityInterface {
  id!: number;
  contractId!: number;
  sourceDependencyId!: number;
  sourceProfileId!: number;
  targetDependencyId!: number;
  targetProfileId!: number;
  date!: string;
  resolutionDate!: string;
  resolutionNumber!: string;
  folderId?: string | undefined;
  sourceDependency?: IDependencyEntity | undefined;
  sourceProfile?: IProfileEntity | undefined;
  targetDependency?: IDependencyEntity | undefined;
  targetProfile?: IProfileEntity | undefined;

  private folder: IFolderEntityInterface | undefined;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface) {
    this.folder = folder;
  }

  getFolder(): IFolderEntityInterface | undefined {
    return this.folder;
  }
}
