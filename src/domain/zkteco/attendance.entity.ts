export class AttendanceEntity {
  id!: string;
  date!: string;
  checkInTime!: string;
  verify!: boolean;
}
