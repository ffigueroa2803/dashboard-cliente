import { personActions } from "@modules/auth/person/store";
import { authV2Request } from "@services/auth-v2.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const request = authV2Request();

export const useCampusUserFind = () => {
  const dispatch = useDispatch();
  let abortController: AbortController;
  const { user } = useSelector((state: RootState) => state.auth);
  const [isError, setIsError] = useState<boolean>(false);
  const [pending, setPending] = useState<boolean>(false);

  const abort = () => abortController?.abort();

  const fetch = () => {
    setPending(true);
    setIsError(false);
    abortController = new AbortController();
    return new Promise((resolve, reject) => {
      request
        .get(`campuses/${user?.campusId}/users`, {
          signal: abortController.signal,
        })
        .then(({ data }) => {
          dispatch(personActions.setUser(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(true);
          dispatch(personActions.setUser({} as any));
          reject(err);
        });
    });
  };

  return {
    pending,
    isError,
    abort,
    fetch,
  };
};
