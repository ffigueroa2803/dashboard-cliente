/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { IInputHandle } from "@common/dtos/input-handle";
import { scheduleActions, initialState } from "@modules/scale/schedules/store";
import { PaginateDto } from "@services/dtos";

const request = scaleRequest();

export interface AssistanceListRequest extends PaginateDto {
  year?: number;
  month?: number;
  dateStart: string;
  dateOver: string;
  condition?: any;
  typeCargoId?: any;
}

export const useScheduleList = (defaultQuery?: AssistanceListRequest) => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const defaultData = {
    page: defaultQuery?.page || 1,
    limit: defaultQuery?.limit || 30,
    dateStart: defaultQuery?.dateStart || "",
    dateOver: defaultQuery?.dateOver || "",
    querySearch: defaultQuery?.querySearch || "",
    typeCargoId: defaultQuery?.typeCargoId || null,
    condition: defaultQuery?.condition || null,
  };

  const [query, setQuery] = useState<AssistanceListRequest>(defaultData);

  const handleQuery = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("limit", `${query.limit}`);
      params.set("querySearch", `${query.querySearch}`);
      params.set("orderByDate", "ASC");
      // filter type cargo
      if (query.typeCargoId) {
        params.set("typeCargoIds[0]", `${query.typeCargoId}`);
      }
      // filter condition
      if (query.condition) {
        params.set("conditions[0]", `${query.condition}`);
      }
      // filter dateStart
      if (query.dateStart) {
        params.set("dateStart", query.dateStart);
      }
      // filter dateOver
      if (query.dateOver) {
        params.set("dateOver", query.dateOver);
      }
      // request
      await request
        .get(`schedules`, { params })
        .then(({ data }) => {
          dispatch(scheduleActions.paginate(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(scheduleActions.paginate(initialState.schedules));
          reject(err);
        });
      setPending(false);
    });
  };

  const clearQuery = () => {
    setQuery(defaultData);
  };

  const clearQueryToPage = () => {
    setQuery((prev) => ({
      ...prev,
      page: defaultData.page,
    }));
  };

  return {
    pending,
    query,
    isError,
    fetch,
    handleQuery,
    setQuery,
    clearQuery,
    clearQueryToPage,
  };
};
