/* eslint-disable no-async-promise-executor */
import { storageRequest } from "@services/storage.request";
import { useState } from "react";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = storageRequest();

export const useFolderFileListService = (folder: IFolderEntityInterface) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFileEntityInterface[]>(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .get(`folders/${folder.id}/files`, {
          signal: abortController.signal,
        })
        .then(({ data }) => resolve(data))
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
