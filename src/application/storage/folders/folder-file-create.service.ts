/* eslint-disable no-async-promise-executor */
import { storageRequest } from "@services/storage.request";
import { useState } from "react";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = storageRequest();

export const useFolderFileCreateService = (folder: IFolderEntityInterface) => {
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);
  const [percent, setPercent] = useState<number>(0);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const onUploadProgress = (progressEvent: any) => {
    const { loaded, total } = progressEvent;
    let percent = Math.floor((loaded * 100) / total) || 0;
    setPercent(percent);
  };

  const execute = (file: File) => {
    return new Promise<IFileEntityInterface>(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      const payload = new FormData();
      payload.set("file", file);
      payload.set("isPermanent", `true`);
      payload.set("name", file.name);
      payload.set("folderId", folder.id);
      abortController = new AbortController();
      await request
        .post(`files`, payload, {
          onUploadProgress,
          signal: abortController.signal,
        })
        .then(({ data }) => resolve(data))
        .catch((err) => {
          setIsError(true);
          reject(err);
        });
      setPending(false);
    });
  };

  return { pending, percent, isError, execute, abort };
};
