/* eslint-disable no-async-promise-executor */
import { storageRequest } from "@services/storage.request";
import { useState } from "react";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = storageRequest();

export const useFolderFindService = () => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = (id: string) => {
    return new Promise<IFolderEntityInterface>(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .get(`folders/${id}`, {
          signal: abortController.signal,
        })
        .then(({ data }) => resolve(data))
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
