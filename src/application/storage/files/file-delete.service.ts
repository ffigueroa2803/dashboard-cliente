/* eslint-disable no-async-promise-executor */
import { storageRequest } from "@services/storage.request";
import { useState } from "react";
import { IFileEntityInterface } from "src/domain/storage/files/file.entity.interface";

const request = storageRequest();

export const useFileDeleteService = (file: IFileEntityInterface) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .destroy(`files/${file.id}`, {
          signal: abortController.signal,
        })
        .then(() => resolve(true))
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
