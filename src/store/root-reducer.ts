import { badgeReducer } from "@common/badges/store";
import { authReducer } from "@common/store/auth.thunk";
import { screenReducer } from "@common/store/screen.thunk";
import { businessReducer } from "@modules/auth/businesses/store";
import { clientReducer } from "@modules/auth/clients/store";
import { dependencyReducer } from "@modules/auth/dependencies/store";
import { documentTypeRtk } from "@modules/auth/document-types/features/document-type-rtk";
import { institutionRtk } from "@modules/auth/institution/features/institution-rtk";
import { permissionRtk } from "@modules/auth/permissions/features/permission.rtk";
import { permissionReducer } from "@modules/auth/permissions/features/permission.slice";
import { personReducer } from "@modules/auth/person/store";
import { roleRtk } from "@modules/auth/roles/features/role.rtk";
import { roleReducer } from "@modules/auth/roles/store";
import { userRtk } from "@modules/auth/users/features/user-rtk";
import { userReducer } from "@modules/auth/users/store";
import { clockReducer } from "@modules/clock/clocks/store";
import { affiliationReducer } from "@modules/planilla/affiliations/store";
import { afpReducer } from "@modules/planilla/afp/store";
import { airhspRtk } from "@modules/planilla/airhsp/infrastructure/features/airhsp-rtk";
import { aportationReducer } from "@modules/planilla/aportations/store";
import { cargoReducer } from "@modules/planilla/cargos/store";
import { configTypeDiscountReducer } from "@modules/planilla/config-type-discounts/store";
import { configTypeRemunerationReducer } from "@modules/planilla/config-type-remunerations/store";
import { config_aportation_maxReducer } from "@modules/planilla/configAportationMax/store";
import { cronogramaRtk } from "@modules/planilla/cronogramas/features/cronograma.rtk";
import { cronogramaReducer } from "@modules/planilla/cronogramas/store";
import { discountReducer } from "@modules/planilla/discounts/store";
import { historialReducer } from "@modules/planilla/historials/store";
import { infoTypeAffiliationReducer } from "@modules/planilla/info-type-affiliations/store";
import { infoTypeAportationReducer } from "@modules/planilla/info-type-aportations/store";
import { infoTypeDiscountReducer } from "@modules/planilla/info-type-discounts/store";
import { infoTypeRemunerationReducer } from "@modules/planilla/info-type-remunerations/store";
import { InfoRtk } from "@modules/planilla/infos/features/info.rtk";
import { infoReducer } from "@modules/planilla/infos/store";
import { labelReducer } from "@modules/planilla/labels/store";
import { metaReducer } from "@modules/planilla/metas/store";
import { obligationReducer } from "@modules/planilla/obligations/store";
import { pimReducer } from "@modules/planilla/pims/store";
import { planillaReducer } from "@modules/planilla/planillas/store";
import { remunerationReducer } from "@modules/planilla/remunerations/store";
import { typeAffiliationReducer } from "@modules/planilla/type-affiliations/store";
import { typeObligationReducer } from "@modules/planilla/type-obligations/store";
import { type_aportacionReducer } from "@modules/planilla/type_aportacion/store";
import { type_descuentoReducer } from "@modules/planilla/type_descuento/store";
import { type_remunerationReducer } from "@modules/planilla/type_remuneration/store";
import { type_sindicatoReducer } from "@modules/planilla/type_sindicato/store";
import { ascentReducer } from "@modules/scale/ascents/store";
import { clockRtk } from "@modules/scale/assistances/features/clock.rtk";
import { assistanceReducer } from "@modules/scale/assistances/store";
import { ballotReducer } from "@modules/scale/ballots/store";
import { configHourhandRtk } from "@modules/scale/config-hourhands/infrastructure/features/config-hourhand-rtk";
import { configVacationReducer } from "@modules/scale/config-vacations/store";
import { contractReducer } from "@modules/scale/contracts/store";
import { degreeRtk } from "@modules/scale/degrees/features/degree-rtk";
import { degreeReducer } from "@modules/scale/degrees/features/degree.splice";
import { discountRtk } from "@modules/scale/discounts/features/discount-feature";
import { scaleDiscountReducer } from "@modules/scale/discounts/store";
import { displacementReducer } from "@modules/scale/displacements/store";
import { familyReducer } from "@modules/scale/families/store";
import { hourhandReducer } from "@modules/scale/hourhands/store";
import { scaleJustificationReducer } from "@modules/scale/justifications/store";
import { licenseReducer } from "@modules/scale/licenses/store";
import { meritReducer } from "@modules/scale/merits/store";
import { profileReducer } from "@modules/scale/profiles/store";
import { scheduleReducer } from "@modules/scale/schedules/store";
import { type_categoriaReducer } from "@modules/scale/type-categories/store";
import { typeDegreeRtk } from "@modules/scale/type-degrees/features/type-degree-rtk";
import { vacationReducer } from "@modules/scale/vacations/store";
import { workReducer } from "@modules/scale/works/store";
import { zktecoClockReducer } from "@modules/zkteco/clocks/store";
import { markerRtk } from "@modules/zkteco/marker/features/marker-rkt";
import { markerReducer } from "@modules/zkteco/marker/features/marker-slice";
import { combineReducers } from "@reduxjs/toolkit";

export const rootReducer = combineReducers({
  screen: screenReducer,
  client: clientReducer,
  business: businessReducer,
  auth: authReducer,
  person: personReducer,
  user: userReducer,
  role: roleReducer,
  work: workReducer,
  pim: pimReducer,
  planilla: planillaReducer,
  type_remuneration: type_remunerationReducer,
  type_descuento: type_descuentoReducer,
  type_aportacion: type_aportacionReducer,
  type_categoria: type_categoriaReducer,
  type_sindicato: type_sindicatoReducer,
  typeAffiliation: typeAffiliationReducer,
  afp: afpReducer,
  contract: contractReducer,
  family: familyReducer,
  license: licenseReducer,
  schedule: scheduleReducer,
  aportationMax: config_aportation_maxReducer,
  ballot: ballotReducer,
  displacement: displacementReducer,
  assistance: assistanceReducer,
  configVacation: configVacationReducer,
  vacation: vacationReducer,
  ascent: ascentReducer,
  merit: meritReducer,
  info: infoReducer,
  cargo: cargoReducer,
  historial: historialReducer,
  cronograma: cronogramaReducer,
  remuneration: remunerationReducer,
  discount: discountReducer,
  affiliation: affiliationReducer,
  obligation: obligationReducer,
  aportation: aportationReducer,
  infoTypeRemuneration: infoTypeRemunerationReducer,
  infoTypeDiscount: infoTypeDiscountReducer,
  infoTypeAportation: infoTypeAportationReducer,
  infoTypeAffiliation: infoTypeAffiliationReducer,
  typeObligation: typeObligationReducer,
  meta: metaReducer,
  badge: badgeReducer,
  dependency: dependencyReducer,
  profile: profileReducer,
  clock: clockReducer,
  scaleDiscount: scaleDiscountReducer,
  scaleJustification: scaleJustificationReducer,
  zktecoClock: zktecoClockReducer,
  configTypeRemuneration: configTypeRemunerationReducer,
  configTypeDiscount: configTypeDiscountReducer,
  label: labelReducer,
  hourhand: hourhandReducer,
  permission: permissionReducer,
  marker: markerReducer,
  degree: degreeReducer,
  [markerRtk.reducerPath]: markerRtk.reducer,
  [roleRtk.reducerPath]: roleRtk.reducer,
  [permissionRtk.reducerPath]: permissionRtk.reducer,
  [clockRtk.reducerPath]: clockRtk.reducer,
  [configHourhandRtk.reducerPath]: configHourhandRtk.reducer,
  [InfoRtk.reducerPath]: InfoRtk.reducer,
  [discountRtk.reducerPath]: discountRtk.reducer,
  [degreeRtk.reducerPath]: degreeRtk.reducer,
  [institutionRtk.reducerPath]: institutionRtk.reducer,
  [typeDegreeRtk.reducerPath]: typeDegreeRtk.reducer,
  [airhspRtk.reducerPath]: airhspRtk.reducer,
  [documentTypeRtk.reducerPath]: documentTypeRtk.reducer,
  [userRtk.reducerPath]: userRtk.reducer,
  [cronogramaRtk.reducerPath]: cronogramaRtk.reducer,
});
