import { permissionRtk } from "@modules/auth/permissions/features/permission.rtk";
import { roleRtk } from "@modules/auth/roles/features/role.rtk";
import { airhspRtk } from "@modules/planilla/airhsp/infrastructure/features/airhsp-rtk";
import { InfoRtk } from "@modules/planilla/infos/features/info.rtk";
import { clockRtk } from "@modules/scale/assistances/features/clock.rtk";
import { configHourhandRtk } from "@modules/scale/config-hourhands/infrastructure/features/config-hourhand-rtk";
import { degreeRtk } from "@modules/scale/degrees/features/degree-rtk";
import { discountRtk } from "@modules/scale/discounts/features/discount-feature";
import { typeDegreeRtk } from "@modules/scale/type-degrees/features/type-degree-rtk";
import { markerRtk } from "@modules/zkteco/marker/features/marker-rkt";
import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import { rootReducer } from "./root-reducer";
import { userRtk } from "@modules/auth/users/features/user-rtk";
import { cronogramaRtk } from "@modules/planilla/cronogramas/features/cronograma.rtk";

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(roleRtk.middleware)
      .concat(permissionRtk.middleware)
      .concat(clockRtk.middleware)
      .concat(markerRtk.middleware)
      .concat(configHourhandRtk.middleware)
      .concat(InfoRtk.middleware)
      .concat(discountRtk.middleware)
      .concat(degreeRtk.middleware)
      .concat(typeDegreeRtk.middleware)
      .concat(airhspRtk.middleware)
      .concat(userRtk.middleware)
      .concat(cronogramaRtk.middleware),
  devTools: process.env.NODE_ENV !== "production",
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export const wrapper = createWrapper(() => store);
