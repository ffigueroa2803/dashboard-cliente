import { BaseRequest } from "./base-request";

export const storageRequest = (token: null | string = "") => {
  const request = BaseRequest(process.env.NEXT_PUBLIC_STORAGE_URL || "", token);

  return request;
};
