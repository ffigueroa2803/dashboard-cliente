import { BaseRequest } from "./base-request";

export const zktecoRequest = (token: null | string = "") => {
  const request = BaseRequest(process.env.NEXT_PUBLIC_ZKTECO_URL || "", token);

  return request;
};
