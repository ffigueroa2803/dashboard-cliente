import { BaseRequest } from "./base-request";

export const scaleRequest = (token: null | string = "") => {
  const request = BaseRequest(process.env.NEXT_PUBLIC_SCALE_URL || "", token);

  return request;
};

export const scaleRequestV2 = (token: null | string = "") => {
  return BaseRequest(process.env.NEXT_PUBLIC_SCALEV2_URL || "", token);
};
