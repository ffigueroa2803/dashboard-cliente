import { BaseRequest } from "./base-request";

export const authV2Request = (token: null | string = "") => {
  const request = BaseRequest(process.env.NEXT_PUBLIC_AUTHV2_URL || "", token);

  return request;
};
