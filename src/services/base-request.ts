import axios, { AxiosRequestConfig } from "axios";
import urljoin from "url-join";
import cookies from "js-cookie";

export const BaseHeaders = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${cookies.get("accessToken")}`,
  "Cache-Control": "no-cache",
  Pragma: "no-cache",
  Expires: "0",
};

export const BaseRequest = (urlBase: string, token: string | null = null) => {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    axios.defaults.headers.common["Authorization"] = `Bearer ${cookies.get(
      "AccessToken"
    )}`;
  }

  const get = (link: string, config?: AxiosRequestConfig): Promise<any> => {
    return axios.get(urljoin(urlBase, link), config);
  };

  const post = (
    link: string,
    data?: any,
    config?: AxiosRequestConfig
  ): Promise<any> => {
    return axios.post(urljoin(urlBase, link), data, config);
  };

  const put = (
    link: string,
    data?: any,
    config?: AxiosRequestConfig
  ): Promise<any> => {
    return axios.put(urljoin(urlBase, link), data, config);
  };

  const destroy = (link: string, config?: AxiosRequestConfig): Promise<any> => {
    return axios.delete(urljoin(urlBase, link), config);
  };

  const generateUrl = (link: string) => {
    return urljoin(urlBase, link);
  };

  return {
    urlBase,
    get,
    post,
    put,
    destroy,
    generateUrl,
    headers: axios.defaults.headers.common,
  };
};
