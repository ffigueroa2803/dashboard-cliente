export interface PaginateDto {
  ids?: number[];
  querySearch?: string;
  page: number;
  limit?: number;
  state?: boolean;
}

export interface MetaDto {
  totalItems: number;
  totalPages: number;
  itemsPerPage: number;
}

export interface InputDto {
  name: string;
  value: any;
}
