/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from "react";
import Select from "@atlaskit/select";
import ObjectID from "bson-objectid";
import { InputActionMeta } from "react-select";

interface IProps {
  className?: string;
  name: string;
  isClearable?: boolean;
  isDisabled?: boolean;
  value: number | string | any;
  options: { label: string; value: any; obj?: any }[];
  isLoading?: boolean;
  placeholder?: string;
  onChange?: (option: any) => void;
  onSearch?: (value: string, actionMeta: InputActionMeta) => void;
  onBlur?: (option: any) => void;
}

const placeholderDefault = "Seleccionar";

export const SelectBasic = ({
  isLoading,
  className,
  name,
  value,
  isClearable = true,
  isDisabled = false,
  onChange,
  onSearch,
  onBlur,
  options,
  placeholder,
}: IProps) => {
  const [isChangeOption, setIsChangeOption] = useState<string>(
    new ObjectID().toHexString()
  );
  const [objectSelect, setObjectSelect] = useState<
    { label: string; value: AnalyserNode; obj?: any } | undefined
  >(undefined);

  const SelectCurrentOption = () => {
    const currentOption = options?.find((d) => d?.value == value) || undefined;
    if (currentOption?.value == objectSelect?.value) return;
    setObjectSelect(currentOption);
  };

  const changeOption = useCallback(
    (option) => {
      if (typeof onChange == "function") {
        onChange({ ...option, name });
      }

      setIsChangeOption(new ObjectID().toHexString());
    },
    [value, options]
  );

  const handleBlur = (options: any) => {
    const target = options.target;
    target.name = name;
    if (typeof onBlur == "function") {
      onBlur({ target });
    }
  };

  useEffect(() => {
    if (isChangeOption) SelectCurrentOption();
  }, [isChangeOption, value, options]);

  return (
    <div>
      <Select
        isLoading={isLoading}
        options={options}
        className={`${className || ""} capitalize cursor-pointer`}
        classNamePrefix="hero"
        placeholder={`${placeholder || placeholderDefault}`}
        name={name}
        value={objectSelect || null}
        isClearable={isClearable}
        onChange={(option: any) => changeOption(option)}
        isDisabled={isDisabled}
        onBlur={handleBlur}
        onInputChange={(value, actionMeta) =>
          typeof onSearch == "function" ? onSearch(value, actionMeta) : ""
        }
      />
    </div>
  );
};
