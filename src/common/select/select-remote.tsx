/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useMemo, useState } from "react";
import Select from "@atlaskit/select";
import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import ObjectID from "bson-objectid";

interface ISelectRemote {
  value: (row: any) => any;
  label: (row: any) => any;
}

interface IProps {
  defaultRefresh?: boolean;
  url?: string;
  defaultQuerySearch?: string;
  handle: Function;
  selectRow: ISelectRemote;
  className?: string;
  name: string;
  value: number | string;
  placeholder?: string;
  isDisabled?: boolean;
  onChange?: (option: any) => void;
  onInputChange?: (value: string) => void;
  onBlur?: (option: any) => void;
  query?: { [key: string]: any };
}

export const SelectRemote = ({
  defaultRefresh,
  handle,
  selectRow,
  className,
  placeholder,
  defaultQuerySearch,
  isDisabled,
  url,
  name,
  value,
  onChange,
  onInputChange,
  onBlur,
  query,
}: IProps) => {
  const [pending, setPending] = useState(false);
  const [datos, setDatos] = useState<any[]>([]);
  const [, setIsError] = useState<boolean>(false);
  const [isExecute, setIsExecute] = useState<boolean>(defaultRefresh || false);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );

  const objSelect = useMemo(() => {
    const option = datos.find((d) => d?.value == value);
    return option || undefined;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [datos, value]);

  const handleSearch = (value: any) => {
    if (typeof onInputChange == "function") onInputChange(value);
    setQuerySearch(value);
  };

  const handleBlur = (options: any) => {
    const target = options.target;
    target.name = name;
    if (typeof onBlur == "function") {
      onBlur({ target });
    }
  };

  const getData = async () => {
    setPending(true);
    setIsError(false);
    const filter = Object.assign(
      { page: 1, querySearch, limit: 100 },
      query || {}
    );
    const params = url ? [url, filter] : [filter];
    // execute
    await handle(...params)
      .then(({ items }: ResponsePaginateDto<any>) => {
        return setDatos(
          items.map((i: any) => {
            const label = selectRow.label(i);
            const value = selectRow.value(i);
            return { name, label, value, row: i };
          })
        );
      })
      .catch(() => setIsError(true));
    setPending(false);
  };

  const handleOnChange = (option: any) => {
    if (typeof onChange == "function") {
      if (!option) {
        return onChange({ name: name, value: null });
      }

      return onChange(option);
    }
  };

  useEffect(() => {
    if (!querySearch) setIsExecute(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [querySearch]);

  useEffect(() => {
    if (querySearch) setIsExecute(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [querySearch]);
  useEffect(() => {
    if (isExecute) getData();
  }, [isExecute]);

  useEffect(() => {
    if (isExecute) setIsExecute(false);
  }, [isExecute]);

  return (
    <div>
      <Select
        isDisabled={isDisabled}
        isLoading={pending}
        options={datos}
        className={`${className || ""} capitalize cursor-pointer`}
        classNamePrefix="hero"
        placeholder={placeholder || "seleccionar"}
        name={name}
        value={objSelect || null}
        isClearable
        onChange={handleOnChange}
        onInputChange={handleSearch}
        onBlur={handleBlur}
      />
    </div>
  );
};
