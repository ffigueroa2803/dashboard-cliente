import { confirmAlert } from "react-confirm-alert";

interface IConfigProps {
  title: string;
  message: string;
  labelSuccess: string;
  labelError: string;
}

export const useConfirm = () => {
  const alert = ({
    title,
    message,
    labelSuccess,
    labelError,
  }: IConfigProps) => {
    return new Promise((resolve, reject) => {
      confirmAlert({
        title,
        message,
        buttons: [
          {
            label: labelSuccess,
            className: "btn-primary",
            onClick: () => resolve(true),
          },
          {
            label: labelError,
            className: "btn-danger",
            onClick: () => reject(false),
          },
        ],
      });
    });
  };

  return {
    alert,
  };
};
