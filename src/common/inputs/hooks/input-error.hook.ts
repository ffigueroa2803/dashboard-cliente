/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-undef */
import { FormikProps } from "formik";
import { useMemo } from "react";

export interface InputErrorHooksProps {
  formik: FormikProps<any>;
  name: string;
}

export function useInputErrorHook({ formik, name }: InputErrorHooksProps) {
  const getError = useMemo(() => {
    let fields: string[] = name.split(".");
    if (fields.length <= 1) return formik.errors[name] || "";
    let value: any = undefined;
    fields.forEach((attr) => {
      if (typeof value == "undefined") {
        value = formik.errors[attr];
      } else if (typeof value == "object") {
        value = value[attr];
      }
    });
    // response
    return value || "";
  }, [formik.errors]);

  const getTouched = useMemo(() => {
    let fields: string[] = name.split(".");
    if (fields.length <= 1) return formik.touched[name] ? true : false;
    let value: any = undefined;
    fields.forEach((attr) => {
      if (typeof value == "undefined") {
        value = formik.touched[attr];
      } else if (typeof value == "object") {
        value = value[attr];
      }
    });
    // response
    return value ? true : false;
  }, [formik.touched]);

  return {
    errorMessage: getError,
    isError: getTouched && getError,
    isTouched: getTouched,
  };
}
