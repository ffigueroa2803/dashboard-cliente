import { FormikProps } from "formik";
import { useInputErrorHook } from "./hooks/input-error.hook";

export interface InputErrorProps {
  formik: FormikProps<any>;
  name: string;
}

export function InputErrorComponent(props: InputErrorProps) {
  const { isError, errorMessage } = useInputErrorHook(props);

  return isError ? (
    <div className="text-danger">{errorMessage || null}</div>
  ) : null;
}
