import { FormikProps, getIn } from "formik";
import { Input, InputProps } from "reactstrap";

export interface IProps extends InputProps {
  title?: string;
  formik: FormikProps<any>;
  name: string;
  isRequired?: boolean;
}

export interface IWrapperProps {
  title?: string;
  name: string;
  formik: FormikProps<any>;
  children?: any;
  isRequired?: boolean;
}

export const generateObject = (formik: FormikProps<any>, name: string) => {
  let valueError: any = formik.errors;
  let valueTouched: any = formik.touched;

  const error = getIn(valueError, name);
  const touch = getIn(valueTouched, name);

  return {
    valueError: error || null,
    valueTouched: touch || false,
  };
};

export const funcIsInvalid = (name: string, formik: any) => {
  const { valueError, valueTouched } = generateObject(formik, name);
  return valueTouched && valueError ? true : false;
};

export const funcIsValid = (name: string, formik: any) => {
  const { valueError, valueTouched } = generateObject(formik, name);
  return valueTouched && !valueError;
};

export const InputWrapper = ({
  title,
  name,
  formik,
  children,
  isRequired,
}: IWrapperProps) => {
  const displayError = () => {
    const { valueTouched, valueError } = generateObject(formik, name);
    return valueTouched ? valueError : null;
  };

  return (
    <>
      <label>
        {title} {isRequired ? <b className="text-danger">*</b> : null}
      </label>
      {children || null}
      <div className="invalid-feedback">{displayError()}</div>
    </>
  );
};

export const InputBasic = (props: IProps) => {
  const handleIsInvalid = (): string => {
    const attr: any = props["name"];
    return funcIsInvalid(attr, props.formik) ? "is-invalid" : "";
  };

  const handleIsValid = (): string => {
    const attr: any = props["name"];
    return funcIsValid(attr, props.formik) ? "is-valid" : "";
  };

  const newProps = Object.assign({}, props);
  delete newProps.isRequired;

  return (
    <InputWrapper
      name={props.name || ""}
      title={props.title}
      formik={props.formik}
      isRequired={props.isRequired || false}
    >
      <Input
        {...newProps}
        className={`${
          props.className || ""
        }  ${handleIsInvalid()} ${handleIsValid()}`}
      />
    </InputWrapper>
  );
};
