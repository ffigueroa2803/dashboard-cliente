/* eslint-disable react-hooks/exhaustive-deps */
import { FormikProps } from "formik";
import { Input } from "reactstrap";
import { useInputErrorHook } from "./hooks/input-error.hook";

export interface InputProps {
  formik: FormikProps<any>;
  type: any;
  name: string;
}

export function InputComponent({ formik, type, name }: InputProps) {
  const { isError, errorMessage } = useInputErrorHook({ formik, name });

  return (
    <>
      <Input
        className={`input ${isError ? "is-danger" : ""}`}
        type={type}
        name={name}
        value={formik?.values?.[name] || ""}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        autoComplete="false"
        autoCorrect="false"
      />
      {isError ? (
        <div className="text-danger">{errorMessage || null}</div>
      ) : null}
    </>
  );
}
