import React from "react";
import { css } from "@emotion/react";
import ScaleLoader from "react-spinners/ScaleLoader";
import styles from "./Loading.module.scss";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

interface IProps {
  loading: boolean;
}

export const LoadingContent = ({ loading }: IProps) => {
  return (
    <div
      className={
        loading
          ? styles.loading__content__background
          : styles.loading__content__background__disabled
      }
    >
      <ScaleLoader color={"#6610f2"} loading={true} css={override} />
    </div>
  );
};
