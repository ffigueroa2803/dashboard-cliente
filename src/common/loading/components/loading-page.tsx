import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import styles from "./Loading.module.scss";

export const LoadingPage = () => {
  const router = useRouter();
  const [percent, setPercent] = useState<number>(0);

  let currentTimeout: any | null = null;

  // start router
  useEffect(() => {
    const handleRouteChange = () => {
      setPercent(5);
      currentTimeout = setTimeout(() => {
        setPercent(30);
      }, 1000);
    };

    // events
    router.events.on("routeChangeStart", handleRouteChange);
    return () => router.events.off("routeChangeStart", handleRouteChange);
  }, []);

  // complete router
  useEffect(() => {
    const handleRouterComplete = () => {
      clearTimeout(currentTimeout || undefined);
      setPercent(100);
      setTimeout(() => setPercent(0), 300);
    };

    // events
    router.events.on("routeChangeComplete", handleRouterComplete);
    return () => router.events.off("routeChangeComplete", handleRouterComplete);
  }, []);

  // error router
  useEffect(() => {
    const handleRouterError = () => {
      clearTimeout(currentTimeout || undefined);
      setPercent(0);
    };

    // events
    router.events.on("routeChangeError", handleRouterError);
    return () => router.events.off("routeChangeError", handleRouterError);
  }, []);

  return (
    <div className={styles.loading__page__content}>
      <div
        className={styles.loading__page__spinner}
        style={{ width: `${percent}%` }}
      >
        <div className={styles.loading__page__spinner__loading}></div>
      </div>
    </div>
  );
};
