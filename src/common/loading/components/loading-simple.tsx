import { css } from "@emotion/react";
import CircleLoader from "react-spinners/BeatLoader";

interface IProps {
  loading: boolean;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export const LoadingSimple = ({ loading }: IProps) => {
  return (
    <CircleLoader
      color={"#6610f2"}
      size={10}
      loading={loading}
      css={override}
    />
  );
};
