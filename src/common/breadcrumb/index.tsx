import React from "react";
import { Breadcrumb, BreadcrumbItem, Col, Row } from "reactstrap";
import Link from "next/link";
import { Home } from "react-feather";

interface IProps {
  title?: string;
  parent?: string;
  current?: string;
  description?: string;
}

export const BreadCrumb = ({ title, parent, current, description }: IProps) => {
  return (
    <div className="page-title">
      <Row>
        <Col>
          <h3>{title || ""}</h3>
          <div className="text-muted">{description || ""}</div>
        </Col>
        <Col>
          <Breadcrumb>
            <BreadcrumbItem>
              <Link href={"/"}>
                <a>
                  <Home style={{ color: "#2c323f" }} />
                </a>
              </Link>
            </BreadcrumbItem>
            <BreadcrumbItem>{parent}</BreadcrumbItem>
            <BreadcrumbItem active>{current ? current : title}</BreadcrumbItem>
          </Breadcrumb>
        </Col>
      </Row>
    </div>
  );
};
