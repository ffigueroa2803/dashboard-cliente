import { toast } from "react-toastify";

export const answers: any = {
  "402": (message: string) =>
    toast.error(message || `Error en el envío de datos`),
  "403": (message?: string) =>
    toast.warning(message || `Usuario no autorizado!!!`),
  "404": (message?: string) =>
    toast.warn(message || `Recurso no encontrado!!!`),
  "409": (message?: string) =>
    toast.error(message || `El Recurso ya existe!!!`),
  "500": (message?: string) =>
    toast.error(message || `Ocurrio un error al procesar la operación!!!`),
};

export const alerStatus = (status: number, message?: string) => {
  const handle: any = answers[`${status}`];
  if (!handle) return toast.error(`Algo salió mal!!!`);
  handle(message);
};
