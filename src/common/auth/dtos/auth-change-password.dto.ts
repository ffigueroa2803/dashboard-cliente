import { Yup } from "@common/yup";

export interface IAuthChangePassword {
  email: string;
  code: string;
}

export const authChangePasswordValidate = Yup.object().shape({
  email: Yup.string().required().email(),
  code: Yup.string().required().length(8),
});
