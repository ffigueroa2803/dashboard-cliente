import { Yup } from "@common/yup";

export interface IAuthSendCode {
  email: string;
}

export const authSendCodeValidate = Yup.object().shape({
  email: Yup.string().required().email(),
});
