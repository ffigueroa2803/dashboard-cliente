import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { authSendCodeValidate } from "../dtos/auth-send-code.dto";

const { request } = AuthRequest();

export const useAuthSendCode = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);
  const [counterTimer, setCounterTime] = useState<number>(0);

  let timerSend: any = null;

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .post(`recoveryPassword/${values.email}/sendCode`)
      .then(() => {
        setIsUpdated(true);
        setCounterTime(15);
        handleTimer();
        toast.success(`Se envió el código de recuperación a su correo!`);
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const handleTimer = () => {
    timerSend = setInterval(() => {
      setCounterTime((prev) => {
        const counter = prev - 1;
        if (counter > 0) return counter;
        clearTimeout(timerSend);
        return 0;
      });
    }, 1000);
  };

  const formik = useFormik({
    initialValues: { email: "" },
    validationSchema: authSendCodeValidate,
    onSubmit: save,
  });

  return {
    pending,
    isUpdated,
    isValid: formik.isValid,
    counterTimer,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
