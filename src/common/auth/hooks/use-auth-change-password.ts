import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { authSendCodeValidate } from "../dtos/auth-send-code.dto";

const { request } = AuthRequest();

export const useAuthChangePassword = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);
  const [newPassword, setNewPassword] = useState<string>("");

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .post(`recoveryPassword/${values.email}/changePassword/${values.code}`)
      .then(({ data }) => {
        toast.success(`Su contraseña fue generada!!!`);
        setIsUpdated(true);
        setNewPassword(data.password);
        resetForm();
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: { email: "", code: "" },
    validationSchema: authSendCodeValidate,
    onSubmit: save,
  });

  return {
    pending,
    isUpdated,
    isValid: formik.isValid,
    form: formik.values,
    formik,
    newPassword,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
