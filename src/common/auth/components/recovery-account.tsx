import React, { useState, useMemo, MouseEventHandler, useEffect } from "react";
import {
  TabContent,
  TabPane,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { ArrowRight, Check, Send, Trash } from "react-feather";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { Show } from "@common/show";
import { useAuthSendCode } from "../hooks/use-auth-send-code";
import { InputBasic } from "@common/inputs/input-basic";
import { useAuthChangePassword } from "../hooks/use-auth-change-password";
import { toast } from "react-toastify";

interface IPropsLoginForm {
  onResetPassword?: Function;
  onSignIn?: Function;
}

export const RecoveryAccount = (props: IPropsLoginForm) => {
  const [hasCode, setHasCode] = useState<boolean>(false);

  const authSendCode = useAuthSendCode();
  const authChangePassword = useAuthChangePassword();

  const { mode } = useSelector((state: RootState) => state.screen);
  const { currentClient } = useSelector((state: RootState) => state.client);

  const nextStep = () => {
    authChangePassword.formik.setFieldValue("email", authSendCode.form.email);
    setHasCode(true);
  };

  const handleCopyPassword = async () => {
    navigator.clipboard.writeText(authChangePassword.newPassword);
    toast.dismiss();
    toast.success(`La contraseña se copío correctamente!!!`);
  };

  return (
    <div
      className={`login-main ${mode == "xs" ? "block" : ""}`}
      style={{ width: "100%" }}
    >
      <TabContent activeTab={"jwt"} className="content-login">
        <TabPane className="fade show" tabId={"jwt"}>
          <Form className="theme-form" onSubmit={(e) => e.preventDefault()}>
            <h4 className="text-center mb-4">
              {currentClient?.business?.name}
            </h4>

            <Show
              condition={authChangePassword.newPassword == ""}
              isDefault={
                <FormGroup className="mb-3">
                  <label>Nueva contraseña generada</label>
                  <Input
                    className="block input-code"
                    type="text"
                    formik={authChangePassword.formik}
                    value={authChangePassword.newPassword}
                    readOnly
                    onClick={handleCopyPassword}
                  />
                  <b>
                    <small>copiar contraseña generada</small>
                  </b>
                </FormGroup>
              }
            >
              <Show
                condition={hasCode}
                isDefault={
                  <>
                    <FormGroup className="mb-3">
                      <InputBasic
                        className="block"
                        title="Correo Electrónico"
                        placeholder="example@example.com"
                        type="text"
                        name="email"
                        disabled={authSendCode.pending}
                        formik={authSendCode.formik}
                        value={authSendCode.form.email || ""}
                        onBlur={authSendCode.handleBlur}
                        onChange={authSendCode.handleChange}
                      />
                    </FormGroup>

                    <Show
                      condition={authSendCode.counterTimer == 0}
                      isDefault={
                        <span className="text-muted">
                          Volver a intentar en {authSendCode.counterTimer} seg
                        </span>
                      }
                    >
                      <div className="form-group mb-0 mt-2">
                        <Button
                          color="primary"
                          outline
                          className="btn-block"
                          disabled={
                            !authSendCode.isValid || authSendCode.pending
                          }
                          onClick={() => authSendCode.handleSubmit()}
                          style={{
                            paddingTop: "0.8em",
                            paddingBottom: "0.8em",
                          }}
                        >
                          <Send className="icon" /> Enviar Código
                        </Button>
                      </div>
                    </Show>

                    <Show condition={authSendCode.isUpdated}>
                      <div className="form-group mb-0 mt-2">
                        <Button
                          color="primary"
                          className="btn-block"
                          onClick={nextStep}
                          style={{
                            paddingTop: "0.8em",
                            paddingBottom: "0.8em",
                          }}
                        >
                          Ya tengo el código <ArrowRight className="icon" />
                        </Button>
                      </div>
                    </Show>
                  </>
                }
              >
                <FormGroup className="mb-3">
                  <label>Correo Electrónico</label>
                  <h6 className="text-success">
                    <Trash
                      className="icon text-danger cursor-pointer"
                      onClick={() => setHasCode(false)}
                    />
                    <b>{authChangePassword.form.email || "N/A"}</b>
                  </h6>
                </FormGroup>

                <FormGroup className="mb-3" style={{ position: "relative" }}>
                  <InputBasic
                    className="block input-code"
                    title="Código"
                    placeholder="0000000"
                    type="text"
                    name="code"
                    disabled={authChangePassword.pending}
                    formik={authChangePassword.formik}
                    value={authChangePassword.form.code || ""}
                    onBlur={authChangePassword.handleBlur}
                    onChange={authChangePassword.handleChange}
                  />
                </FormGroup>

                <div className="form-group mb-0 mt-2">
                  <Button
                    color="primary"
                    className="btn-block"
                    disabled={authChangePassword.pending}
                    onClick={() => authChangePassword.handleSubmit()}
                    style={{
                      paddingTop: "0.8em",
                      paddingBottom: "0.8em",
                    }}
                  >
                    <Check className="icon" /> Verificar
                  </Button>
                </div>
              </Show>
            </Show>

            <Show condition={typeof props.onSignIn == "function"}>
              <div className="form-group mb-0 text-right mt-3">
                <div className="checkbox ml-3"></div>
                <a href="#" onClick={props.onSignIn as MouseEventHandler}>
                  Ya tengo cuenta
                </a>
              </div>
            </Show>
          </Form>
        </TabPane>
      </TabContent>
    </div>
  );
};
