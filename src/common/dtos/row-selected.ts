export interface IRowSelected<T> {
  allSelected: boolean;
  selectedCount: number;
  selectedRows: T[];
}
