/* eslint-disable react-hooks/exhaustive-deps */
import { BaseRequest } from "@services/base-request";
import PDFMerger from 'pdf-merger-js/browser';
import { useEffect, useState } from "react";
import b64ToBlob from "b64-to-blob";
import { toast } from "react-toastify";

export function useModalDownload(url: string, limit: number) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [tmpBlobs, setTmpBlobs] = useState<Blob[]>([]);
  const [isSetting, setIsSetting] = useState<boolean>(false);
  const [urlResult, setUrlResult] = useState<string>("");
  const [isRequest, setIsRequest] = useState<boolean>(false);
  const [isRender, setIsRender] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);

  const request = BaseRequest(url);

  const reset = () => {
    setTmpBlobs([]);
    setIsSetting(false);
    setUrlResult("");
    setIsRequest(false);
    setIsRender(false);
    setIsLoading(false);
    setPage(1);
  }

  const fetch = () => {
    setIsLoading(true);
    const params = new URLSearchParams();
    params.set("page", page.toString());
    params.set("limit", limit.toString());
    request.get("", { params }).then((res) => {
      const { data, type, meta } = res.data;
      const blob = b64ToBlob(data, type);
      setTmpBlobs((prev) => ([...prev, blob]));
      if (meta.totalPages == page) setIsSetting(true);
      else {
        setPage((prev) => prev + 1)
        setIsRequest(true);
      }
    }).catch((err) => {
      console.log(err);
      toast.error("No se pudo obtener el reporte")
    });
  }

  const handlePdf = async () => {
    const merger = new PDFMerger();
    for (const blob of tmpBlobs) {
      await merger.add(blob);
    }
    const mergedPdf = await merger.saveAsBlob();
    setUrlResult(URL.createObjectURL(mergedPdf));
    setIsRender(true);
    setIsRequest(false);
    setIsSetting(false);
    setIsLoading(false);
    setPage(1);
  }

  useEffect(() => {
    if (isRequest) fetch();
  }, [isRequest, page]);

  useEffect(() => {
    if (isSetting) handlePdf();
  }, [isSetting])

  return {
    isLoading,
    isRender,
    page,
    setPage,
    urlResult,
    fetch,
    reset
  }
}