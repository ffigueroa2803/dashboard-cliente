/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react-hooks/exhaustive-deps */
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { useModalDownload } from "../hooks/use-modal-download";
import { useEffect } from "react";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import imageReport from "../../../assets/images/report.png";

export interface ModalPaginateReportProps {
  title: string;
  isOpen: boolean;
  url: string;
  limit: number;
  onClose(): void;
}

export function ModalPaginateReport({
  url,
  title,
  limit,
  isOpen,
  onClose,
}: ModalPaginateReportProps) {
  const modalDowload = useModalDownload(url, limit);

  useEffect(() => {
    if (!isOpen) modalDowload.reset();
  }, [isOpen]);

  useEffect(() => {
    if (url && isOpen) modalDowload.fetch();
  }, [isOpen, url]);

  return (
    <Modal isOpen={isOpen} size="xl">
      <ModalHeader toggle={onClose}>{title}</ModalHeader>
      <ModalBody style={{ minHeight: "75vh" }}>
        <Show condition={modalDowload.isLoading}>
          <div className="text-center h-100">
            <img src={imageReport.src} className="mb-4" />
            <LoadingSimple loading />
          </div>
        </Show>
        <Show condition={modalDowload.isRender}>
          <iframe
            src={modalDowload.urlResult}
            style={{
              overflow: "hidden",
              overflowX: "hidden",
              overflowY: "hidden",
              height: "100%",
              width: "100%",
              position: "absolute",
              top: "0px",
              left: "0px",
              right: "0px",
              bottom: "0px",
            }}
            height="100%"
            width="100%"
          />
        </Show>
      </ModalBody>
    </Modal>
  );
}
