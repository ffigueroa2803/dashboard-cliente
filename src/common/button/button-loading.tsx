import ScaleLoader from "react-spinners/BarLoader";
import { css } from "@emotion/react";
import { Button } from "reactstrap";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

interface IProps {
  loading: boolean;
  color: string;
  title: string;
}

export const ButtonLoading = ({
  loading,
  color,
  title = "Cargando...",
}: IProps) => {
  return (
    <Button color={color} disabled>
      {title}
      <ScaleLoader color="white" loading={loading} css={override} />
    </Button>
  );
};
