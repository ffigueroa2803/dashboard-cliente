import React from "react";
import { Button } from "reactstrap";

interface IProps {
  icon: any;
  color?: string;
  title?: string;
  onClick: () => void;
}

export const FloatButton = ({ icon, color, title, onClick }: IProps) => {
  return (
    <Button
      color={color || "secundary"}
      className="float-button cursor-pointer"
      onClick={onClick}
      title={title}
    >
      {icon}
    </Button>
  );
};
