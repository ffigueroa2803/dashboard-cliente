import { IPermissionEntity } from "@modules/auth/permissions/dtos/permission.entity";
import { IUserEntity } from "@modules/auth/users/dtos/user.entity";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

export interface AuthState {
  logged: boolean;
  token: string | null;
  user: IUserEntity | null;
  permissions: IPermissionEntity[];
}

const initialState: AuthState = {
  logged: false,
  token: null,
  user: null,
  permissions: [],
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLogged: (state, action: PayloadAction<boolean>) => {
      state.logged = action.payload;
      return state;
    },
    setToken: (state, action: PayloadAction<string | null>) => {
      state.token = action.payload;
      return state;
    },
    setUser: (state, action: PayloadAction<IUserEntity | null>) => {
      state.user = action.payload;
      return state;
    },
    editUser: (state, action: PayloadAction<IUserEntity | null>) => {
      state.user = Object.assign(state.user || {}, action.payload);
      return state;
    },
    setPermissions: (
      state,
      { payload }: PayloadAction<IPermissionEntity[]>
    ) => {
      state.permissions = payload;
      return state;
    },
    logout: (state) => {
      state.user = null;
      state.token = null;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.auth };
    },
  },
});

export const authReducer = authSlice.reducer;

export const {
  setLogged,
  setToken,
  setUser,
  editUser,
  logout,
  setPermissions,
} = authSlice.actions;
