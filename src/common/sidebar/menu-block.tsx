import { Show } from "@common/show";
import { Fragment } from "react";

export interface IProps {
  title: string;
  content: string;
  children?: any;
  visibled: boolean;
  components: any[];
}

export const MenuBlock = ({ visibled, title, content, components }: IProps) => {
  return (
    <>
      <Show condition={visibled}>
        <li className="sidebar-main-title">
          <div>
            <h6 className="lan-1">{title}</h6>
            <p className="lan-2">{content}</p>
          </div>
        </li>
      </Show>
      {/* iter */}
      {components.map((c, index) => (
        <Fragment key={`lsit-component-${index}`}>{c}</Fragment>
      ))}
    </>
  );
};
