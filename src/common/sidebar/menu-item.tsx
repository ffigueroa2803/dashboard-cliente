import { Show } from "@common/show";
import Link from "next/link";

export interface IBadgePropsMenu {
  version: number;
  color: string;
}

interface IProps {
  type: "sub" | "link";
  title: string;
  icon?: any;
  active: boolean;
  badge?: IBadgePropsMenu;
  children?: any;
  path?: string;
  onClick?: () => void;
}

export const MenuItem = ({
  type,
  title,
  badge,
  icon,
  active,
  children,
  path,
  onClick,
}: IProps) => {
  if (type == "sub") {
    return (
      <li className="sidebar-list" onClick={onClick}>
        <a
          href="#"
          className={`sidebar-link sidebar-title ${active ? "active" : ""}`}
        >
          {icon}
          <span>{title}</span>
          <Show condition={typeof badge == "object"}>
            <label className={`badge badge-${badge?.color}`}>
              v{badge?.version}
            </label>
          </Show>
          <div className="according-menu">
            <Show
              condition={active}
              isDefault={<i className="fa fa-angle-right"></i>}
            >
              <i className="fa fa-angle-down"></i>
            </Show>
          </div>
        </a>
        {/* childrens */}
        <ul
          className="nav-sub-childmenu submenu-content"
          style={{ display: active ? "block" : "none" }}
        >
          {children || null}
        </ul>
      </li>
    );
  }

  if (type == "link") {
    return (
      <li className="sidebar-main-title">
        <Link href={path || ""}>
          <a className={`lan-4 ${active ? "active" : ""}`}>{title}</a>
        </Link>
      </li>
    );
  }

  return null;
};
