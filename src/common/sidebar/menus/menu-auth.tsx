import { Show } from "@common/show";
import { businessEntityName } from "@modules/auth/businesses/dtos/business.entity";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { clientEntityName } from "@modules/auth/clients/dtos/client.entity";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { personEntityName } from "@modules/auth/person/dtos/person.entity";
import { roleEntityName } from "@modules/auth/roles/dtos/role.entity";
import { userEntityName } from "@modules/auth/users/dtos/user.entity";
import { useRouter } from "next/router";
import { Fragment, useContext, useEffect } from "react";
import { Users } from "react-feather";
import { septionNames } from "../menu";
import { MenuItem } from "../menu-item";
import { MenuContext } from "../menu.contenxt";

const url = process?.env?.NEXT_PUBLIC_URL;

export const menuAuthName = "auth";

export const menuAuthKeys = {
  USER: `/${menuAuthName}/users`,
  PERSON: `/${menuAuthName}/people`,
  ROLE: `/${menuAuthName}/roles`,
  CLIENT: `/${menuAuthName}/clients`,
  BUSINESS: `/${menuAuthName}/businesses`,
};

export const MenuAuth = () => {
  const { addSeption, septions, addModule, modules, toggle, setToggle } =
    useContext(MenuContext);
  const ability = useContext(CaslContext);

  const router = useRouter();

  const records = [
    {
      valid: ability.can(PermissionAction.MANAGE, personEntityName),
      render: (
        <MenuItem
          title="Personas"
          type="link"
          active={router.pathname == menuAuthKeys.PERSON}
          path={`${url}${menuAuthKeys.PERSON}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.MANAGE, userEntityName),
      render: (
        <MenuItem
          title="Usuarios"
          type="link"
          active={router.pathname == menuAuthKeys.USER}
          path={`${url}${menuAuthKeys.USER}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.MANAGE, roleEntityName),
      render: (
        <MenuItem
          title="Roles"
          type="link"
          active={router.pathname == menuAuthKeys.ROLE}
          path={`${url}${menuAuthKeys.ROLE}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.MANAGE, clientEntityName),
      render: (
        <MenuItem
          title="Clientes"
          type="link"
          active={router.pathname == menuAuthKeys.CLIENT}
          path={`${url}${menuAuthKeys.CLIENT}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.MANAGE, businessEntityName),
      render: (
        <MenuItem
          title="Empresas"
          type="link"
          active={router.pathname == menuAuthKeys.BUSINESS}
          path={`${url}${menuAuthKeys.BUSINESS}`}
        />
      ),
    },
  ];

  const handle = () => {
    const isCan = records.filter((r) => r.valid).length;
    recordStep(isCan);
    recordMenu(isCan);
  };

  const recordStep = (record: number) => {
    if (septions.includes(septionNames.ADMIN)) return;
    if (!record) return;
    addSeption(septionNames.ADMIN);
  };

  const recordMenu = (record: number) => {
    if (modules.includes(menuAuthName)) return;
    if (!record) return;
    addModule(menuAuthName);
  };

  useEffect(() => {
    handle();
  }, []);

  if (!modules.includes(menuAuthName)) return null;

  return (
    <>
      {/* main */}
      <MenuItem
        title="Autorización"
        type="sub"
        icon={<Users />}
        active={toggle == menuAuthName}
        onClick={() => setToggle(menuAuthName)}
        badge={{
          version: 1,
          color: "warning",
        }}
      >
        {/* records */}
        {records.map((r, index) => (
          <Fragment key={`list-auth-${index}`}>
            <Show condition={r.valid} isDefault={null}>
              {r.render}
            </Show>
          </Fragment>
        ))}
      </MenuItem>
    </>
  );
};
