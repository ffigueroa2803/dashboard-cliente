/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { cargoEntityName } from "@modules/planilla/cargos/dtos/cargo.entity";
import { cronogramaEntityName } from "@modules/planilla/cronogramas/dtos/cronograma.entity";
import { historialEntityName } from "@modules/planilla/historials/dtos/historial.entity";
import { pimEntityName } from "@modules/planilla/pims/dtos/pim.entity";
import { typeAffiliationName } from "@modules/planilla/type-affiliations/dtos/type-affiliation.entity";
import { typeAportationName } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";
import { typeDiscountEntityName } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";
import { typeRemunerationEntityName } from "@modules/planilla/type_remuneration/dtos/type_remuneration.entity";
import { afpEntityName } from "@modules/scale/afps/dtos/afp.entity";
import { useRouter } from "next/router";
import { Fragment, useContext, useEffect } from "react";
import { Database } from "react-feather";
import { septionNames } from "../menu";
import { MenuItem } from "../menu-item";
import { MenuContext } from "../menu.contenxt";

const url = process?.env?.NEXT_PUBLIC_URL;

export const menuPlanillaName = "planilla";

export const menuPlanillaKeys = {
  CRONOGRAMA: "/planilla/cronogramas",
  HISTORIAL: "/planilla/historials",
  CARGO: "/planilla/cargos",
  AFP: "/planilla/afp",
  TYPE_REMUNERATION: "/planilla/type-remunerations",
  TYPE_DISCOUNT: "/planilla/type-discounts",
  TYPE_APORTATION: "/planilla/type-aportations",
  TYPE_AFFILIATION: "/planilla/type-affiliations",
  PIM: "/planilla/pims",
  PLAME: "/planilla/plames",
};

export const MenuPlanilla = () => {
  const { addSeption, septions, addModule, modules, toggle, setToggle } =
    useContext(MenuContext);
  const ability = useContext(CaslContext);

  const router = useRouter();

  const records = [
    {
      valid: ability.can(PermissionAction.READ, cronogramaEntityName),
      render: (
        <MenuItem
          title="Cronogramas"
          type="link"
          active={router.pathname == menuPlanillaKeys.CRONOGRAMA}
          path={`${url}${menuPlanillaKeys.CRONOGRAMA}?principal=true`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, historialEntityName),
      render: (
        <MenuItem
          title="Historial Boletas"
          type="link"
          active={router.pathname == menuPlanillaKeys.HISTORIAL}
          path={`${url}${menuPlanillaKeys.HISTORIAL}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, cargoEntityName),
      render: (
        <MenuItem
          title="Partición Presp."
          type="link"
          active={router.pathname == menuPlanillaKeys.CARGO}
          path={`${url}${menuPlanillaKeys.CARGO}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, afpEntityName),
      render: (
        <MenuItem
          title="Leyes Sociales"
          type="link"
          active={router.pathname == menuPlanillaKeys.AFP}
          path={`${url}${menuPlanillaKeys.AFP}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, typeRemunerationEntityName),
      render: (
        <MenuItem
          title="Tip. Remuneración"
          type="link"
          active={router.pathname == menuPlanillaKeys.TYPE_REMUNERATION}
          path={`${url}${menuPlanillaKeys.TYPE_REMUNERATION}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, typeDiscountEntityName),
      render: (
        <MenuItem
          title="Tip. Descuentos"
          type="link"
          active={router.pathname == menuPlanillaKeys.TYPE_DISCOUNT}
          path={`${url}${menuPlanillaKeys.TYPE_DISCOUNT}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, typeAportationName),
      render: (
        <MenuItem
          title="Tip. Aportaciones"
          type="link"
          active={router.pathname == menuPlanillaKeys.TYPE_APORTATION}
          path={`${url}${menuPlanillaKeys.TYPE_APORTATION}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, typeAffiliationName),
      render: (
        <MenuItem
          title="Tip. Afiliación"
          type="link"
          active={router.pathname == menuPlanillaKeys.TYPE_AFFILIATION}
          path={`${url}${menuPlanillaKeys.TYPE_AFFILIATION}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, pimEntityName),
      render: (
        <MenuItem
          title="PIM Anual"
          type="link"
          active={router.pathname == menuPlanillaKeys.PIM}
          path={`${url}${menuPlanillaKeys.PIM}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, cronogramaEntityName),
      render: (
        <MenuItem
          title="PDT-Plame"
          type="link"
          active={router.pathname == menuPlanillaKeys.PLAME}
          path={`${url}${menuPlanillaKeys.PLAME}?principal=true`}
        />
      ),
    },
  ];

  const handle = () => {
    const isCan = records.filter((r) => r.valid).length;
    recordStep(isCan);
    recordMenu(isCan);
  };

  const recordStep = (record: number) => {
    if (septions.includes(septionNames.FINANZA)) return;
    if (!record) return;
    addSeption(septionNames.FINANZA);
  };

  const recordMenu = (record: number) => {
    if (modules.includes(menuPlanillaName)) return;
    if (!record) return;
    addModule(menuPlanillaName);
  };

  useEffect(() => {
    handle();
  }, []);

  if (!modules.includes(menuPlanillaName)) return null;

  return (
    <>
      {/* main */}
      <MenuItem
        title="Planilla"
        type="sub"
        icon={<Database />}
        active={toggle == menuPlanillaName}
        onClick={() => setToggle(menuPlanillaName)}
        badge={{
          version: 2,
          color: "warning",
        }}
      >
        {/* records */}
        {records.map((r, index) => (
          <Fragment key={`list-planilla-${index}`}>
            <Show condition={r.valid} isDefault={null}>
              {r.render}
            </Show>
          </Fragment>
        ))}
      </MenuItem>
    </>
  );
};
