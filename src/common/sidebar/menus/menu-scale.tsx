/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { typeCategoryEntityName } from "@modules/scale/type-categories/dtos/type-category.entity";
import { assistanceEntityName } from "@modules/scale/assistances/dtos/assistance.entity";
import { discountEntityName } from "@modules/scale/discounts/dtos/discount.entity";
import { workEntityName } from "@modules/scale/works/dtos/work.entity";
import { useRouter } from "next/router";
import { Fragment, useContext, useEffect } from "react";
import { Save } from "react-feather";
import { septionNames } from "../menu";
import { MenuItem } from "../menu-item";
import { MenuContext } from "../menu.contenxt";
import { hourhandEntityName } from "@modules/scale/hourhands/dtos/hourhand.entity";

const url = process?.env?.NEXT_PUBLIC_URL;

export const menuScaleName = "scale";

export const menuScaleKeys = {
  WORK: `/${menuScaleName}/works`,
  TYPE_CATEGORIES: `/${menuScaleName}/type-categories`,
  ASSISTANCE: `/${menuScaleName}/assistances`,
  DISCOUNT: `/${menuScaleName}/discounts`,
  HOURHAND: `/${menuScaleName}/hourhands`,
};

export const MenuScale = () => {
  const { addSeption, septions, addModule, modules, toggle, setToggle } =
    useContext(MenuContext);
  const ability = useContext(CaslContext);

  const router = useRouter();

  const records = [
    {
      valid: ability.can(PermissionAction.READ, workEntityName),
      render: (
        <MenuItem
          title="Trabajadores"
          type="link"
          active={router.pathname == menuScaleKeys.WORK}
          path={`${url}${menuScaleKeys.WORK}?state=true`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, typeCategoryEntityName),
      render: (
        <MenuItem
          title="Tipo Categorías"
          type="link"
          active={router.pathname == menuScaleKeys.TYPE_CATEGORIES}
          path={`${url}${menuScaleKeys.TYPE_CATEGORIES}?state=true`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, hourhandEntityName),
      render: (
        <MenuItem
          title="Horarios"
          type="link"
          active={router.pathname == menuScaleKeys.HOURHAND}
          path={`${url}${menuScaleKeys.HOURHAND}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, assistanceEntityName),
      render: (
        <MenuItem
          title="Asistencia"
          type="link"
          active={router.pathname == menuScaleKeys.ASSISTANCE}
          path={`${url}${menuScaleKeys.ASSISTANCE}`}
        />
      ),
    },
    {
      valid: ability.can(PermissionAction.READ, discountEntityName),
      render: (
        <MenuItem
          title="Descuentos"
          type="link"
          active={router.pathname == menuScaleKeys.DISCOUNT}
          path={`${url}${menuScaleKeys.DISCOUNT}`}
        />
      ),
    },
  ];

  const handle = () => {
    const isCan = records.filter((r) => r.valid).length;
    recordStep(isCan);
    recordMenu(isCan);
  };

  const recordStep = (record: number) => {
    if (septions.includes(septionNames.FINANZA)) return;
    if (!record) return;
    addSeption(septionNames.FINANZA);
  };

  const recordMenu = (record: number) => {
    if (modules.includes(menuScaleName)) return;
    if (!record) return;
    addModule(menuScaleName);
  };

  useEffect(() => {
    handle();
  }, []);

  if (!modules.includes(menuScaleName)) return null;

  return (
    <>
      {/* main */}
      <MenuItem
        title="Escalafón"
        type="sub"
        icon={<Save />}
        active={toggle == menuScaleName}
        onClick={() => setToggle(menuScaleName)}
        badge={{
          version: 1,
          color: "warning",
        }}>
        {/* records */}
        {records.map((r, index) => (
          <Fragment key={`list-scale-${index}`}>
            <Show condition={r.valid} isDefault={null}>
              {r.render}
            </Show>
          </Fragment>
        ))}
      </MenuItem>
    </>
  );
};
