/* eslint-disable no-unused-vars */
import React, { useContext } from "react";
import { MenuBlock } from "./menu-block";
import { MenuContext } from "./menu.contenxt";
import { MenuAuth } from "./menus/menu-auth";
import { MenuPlanilla } from "./menus/menu-planilla";
import { MenuScale } from "./menus/menu-scale";

export const septionNames = {
  ADMIN: "admin",
  FINANZA: "finanza",
};

export const Menu = () => {
  const { septions } = useContext(MenuContext);

  return (
    <>
      {/* administrativo */}
      <MenuBlock
        visibled={septions.includes(septionNames.ADMIN)}
        title="Administración"
        content="Personas, Usuarios, Roles"
        components={[<MenuAuth />]}
      />
      {/* finanzas */}
      <MenuBlock
        visibled={septions.includes(septionNames.FINANZA)}
        title="Finanzas"
        content="Trabajadores, Reportes, Pagos"
        components={[<MenuScale />, <MenuPlanilla />]}
      />
    </>
  );
};
