import { createContext, FC, useState } from "react";

class InitialContext {
  septions!: string[];
  addSeption(obj: any) {}
  modules!: string[];
  addModule(obj: any) {}
  toggle!: string;
  setToggle(value: string) {}
}

export const MenuContext = createContext<InitialContext>({} as any);

export const MenuProvider: FC = ({ children }) => {
  const [septions, setSeptions] = useState<string[]>([]);
  const [modules, setModules] = useState<string[]>([]);
  const [toggle, setToggle] = useState<string>("");

  const addSeption = (value: string) => {
    setSeptions((prev) => {
      if (prev.includes(value)) return prev;
      return [...prev, value];
    });
  };

  const addModule = (value: string) => {
    setModules((prev) => {
      if (prev.includes(value)) return prev;
      return [...prev, value];
    });
  };

  return (
    <MenuContext.Provider
      value={{
        septions,
        addSeption,
        modules,
        addModule,
        toggle,
        setToggle,
      }}
    >
      {children}
    </MenuContext.Provider>
  );
};
