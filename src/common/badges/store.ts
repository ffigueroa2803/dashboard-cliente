import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IBadgeEntity } from "./dtos/badge.entity";

export interface BadgeState {
  badges: ResponsePaginateDto<IBadgeEntity>;
  badge: IBadgeEntity;
}

export const initialState: BadgeState = {
  badges: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  badge: {} as any,
};

export const badgeSlice = createSlice({
  name: "auth@badges",
  initialState,
  reducers: {
    paginate: (
      state: BadgeState,
      { payload }: PayloadAction<ResponsePaginateDto<IBadgeEntity>>
    ) => {
      state.badges = payload;
      return state;
    },
    setPerson: (
      state: BadgeState,
      { payload }: PayloadAction<IBadgeEntity>
    ) => {
      state.badge = payload;
      state.badges?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }

        return item;
      });

      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.badges };
    },
  },
});

export const badgeReducer = badgeSlice.reducer;

export const badgeActions = badgeSlice.actions;
