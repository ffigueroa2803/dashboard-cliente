import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch } from "react-redux";
import { badgeActions } from "../store";
import { PaginateDto } from "@services/dtos";

const { request } = AuthRequest();

export const useBadgeList = (paginate: PaginateDto) => {
  const dispatch = useDispatch();

  const [page, setPage] = useState<number>(paginate.page || 1);
  const [limit, setLimit] = useState<number>(paginate.limit || 100);
  const [querySearch, setQuerySearch] = useState<string>(
    paginate.querySearch || ""
  );
  const [pending, setPending] = useState<boolean>(false);

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`badges`, { params })
      .then((res) => dispatch(badgeActions.paginate(res.data)))
      .catch(() => dispatch(badgeActions.paginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    page,
    setPage,
    limit,
    setLimit,
    querySearch,
    setQuerySearch,
    fetch,
  };
};
