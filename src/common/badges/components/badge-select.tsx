import React, { useEffect, useState } from "react";
import { IBadgeEntity } from "../dtos/badge.entity";
import { SelectBasic } from "@common/select/select-basic";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { useBadgeList } from "../hooks/use-badge-list";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange?: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const BadgeSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const { badges } = useSelector((state: RootState) => state.badge);

  const badgeList = useBadgeList({
    querySearch: defaultQuerySearch,
    limit: 100,
    page: 1,
  });

  const settingsData = (row: IBadgeEntity) => {
    return {
      label:
        `${row.departament} / ${row.province} / ${row.district}`.toLowerCase(),
      value: row.codUbi,
      onj: row,
    };
  };

  useEffect(() => {
    if (badgeList.querySearch) setIsFetch(true);
  }, [badgeList.querySearch]);

  useEffect(() => {
    if (isFetch) badgeList.fetch();
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={badges?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={badgeList.setQuerySearch}
    />
  );
};
