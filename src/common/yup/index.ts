/* eslint-disable no-empty */
import { setLocale } from "yup";
import * as y from "yup";
import cookies from "js-cookie";

export const lang = cookies?.get("lang") || "es";

try {
  const data = require(`./locales/${lang}.json`);
  setLocale(data || {});
} catch (error) {}

export const Yup = y;
