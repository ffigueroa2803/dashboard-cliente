/* eslint-disable no-unused-vars */
import { createContext, FC, useEffect, useState } from "react";

const initialProps = {
  isOpen: false,
  show: false,
  content: <></>,
  title: "",
  toggle: () => {},
  close: () => {},
  setShow: (value: boolean) => {},
  setContent: (content: any) => content,
  setTitle: (value: string) => {},
};

export const NavbarContext = createContext(initialProps);

export const NavbarProvider: FC = ({ children }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [show, setShow] = useState<boolean>(false);
  const [content, setContent] = useState<any>(<></>);
  const [title, setTitle] = useState<string>("Más Opciones");

  const toggle = () => setIsOpen((prev) => !prev);

  const close = () => setIsOpen(false);

  const removeScroll = () => {
    document.body.setAttribute("style", "overflow: hidden");
  };

  const addScroll = () => {
    document.body.setAttribute("style", "overflow: auto");
  };

  useEffect(() => {
    if (isOpen) removeScroll();
    else addScroll();
  }, [isOpen]);

  return (
    <NavbarContext.Provider
      value={{
        show,
        isOpen,
        content,
        setShow,
        toggle,
        close,
        setContent,
        title,
        setTitle,
      }}
    >
      {children}
    </NavbarContext.Provider>
  );
};
