import { Show } from "@common/show";
import React, { useContext } from "react";
import { Layers } from "react-feather";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { NavbarContext } from "../navbar-context";

export const LeftBar = () => {
  const { isOpen, show, content, toggle, close, title } =
    useContext(NavbarContext);

  const handleOpen = (e: any) => {
    e.preventDefault();
    toggle();
  };

  return (
    <div className="left-header horizontal-wrapper pl-0 col">
      <ul className="horizontal-menu">
        <Show condition={show}>
          <li className="mega-menu outside">
            <a
              className={`nav-link cursor-pointer ${isOpen ? "active" : ""}`}
              onClick={handleOpen}
            >
              <Layers />
              <span>Opciones</span>
            </a>
            {/* modal */}
            <Modal isOpen={isOpen}>
              <ModalHeader toggle={close}>{title}</ModalHeader>
              <ModalBody>{content}</ModalBody>
            </Modal>
          </li>
        </Show>
      </ul>
    </div>
  );
};
