/* eslint-disable no-async-promise-executor */
import { read, utils, write } from "xlsx";

export function useExcelToJson<T>() {
  const readFile = async (file: File): Promise<T> => {
    return new Promise(async (resolve, reject) => {
      const extension = file.name.split(".").pop()?.toUpperCase();
      if (extension !== "XLSX") {
        return reject(new Error("El archivo no es de tipo xlsx"));
      } else {
        try {
          const result: any = {};
          const data = await file.arrayBuffer();
          const workbook = read(data);
          workbook.SheetNames.forEach((sheet) => {
            const rowObjects = utils.sheet_to_json(workbook.Sheets[sheet]);
            result[sheet] = rowObjects;
          });
          return resolve(result);
        } catch (error) {
          return reject(error);
        }
      }
    });
  };

  const formatter = (attrs: string[], name: string) => {
    const wb = utils.book_new();
    utils.book_append_sheet(wb, utils.aoa_to_sheet([attrs]), name);
    const xlsx = write(wb, { bookType: "xlsx", type: "buffer" });
    const blob = new Blob([xlsx]);
    const a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.target = "_blank";
    a.download = `${name}.xlsx`;
    a.click();
  };

  return { readFile, formatter };
}
