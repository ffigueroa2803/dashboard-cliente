/* eslint-disable react-hooks/exhaustive-deps */
import type { NextPage } from "next";
import { AuthLayout } from "@common/layouts";
import { authorize } from "@services/authorize";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { personActions } from "@modules/auth/person/store";
import { userActions } from "@modules/auth/users/store";
import { ModalPaginateReport } from "@common/modal/components/modal-paginate-report";
import { Button } from "reactstrap";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const { user } = useSelector((state: RootState) => state.auth);

  useEffect(() => {
    if (user?.id) {
      dispatch(personActions.setPerson(user.userableObject as any));
      dispatch(userActions.setUser(user));
    }
  }, [user]);

  return (
    <AuthLayout>
      <Button onClick={() => setIsOpen(true)}>open</Button>
      <ModalPaginateReport
        url="http://localhost:3336/api/cronogramas/278/reports/spreadsheets.pdf?isCheck=false&neto=false&negative=false&isDouble=false"
        limit={120}
        title="Dev"
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
      />
    </AuthLayout>
  );
};

export default Home;

export const getServerSideProps = authorize("Dev");
