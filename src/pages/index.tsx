/* eslint-disable react-hooks/exhaustive-deps */
import type { NextPage } from "next";
import { AuthLayout } from "@common/layouts";
import { authorize } from "@services/authorize";
import { useEffect } from "react";
import { BreadCrumb } from "@common/breadcrumb";
import { Col, Row } from "reactstrap";
import { PersonInfo } from "@modules/auth/person/components/person-info";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { personActions } from "@modules/auth/person/store";
import { UserInfo } from "@modules/auth/users/components/user-info";
import { userActions } from "@modules/auth/users/store";
import { IUserEntity } from "@modules/auth/users/dtos/user.entity";
import { setUser } from "@common/store/auth.thunk";
import { UserAuthAuditPaginate } from "@modules/auth/users/components/user-auth-audit-paginate";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const { user } = useSelector((state: RootState) => state.auth);

  const handleUpdate = (user: IUserEntity) => {
    dispatch(setUser(user));
  };

  useEffect(() => {
    if (user?.id) {
      dispatch(personActions.setPerson(user.userableObject as any));
      dispatch(userActions.setUser(user));
    }
  }, [user]);

  return (
    <AuthLayout>
      <BreadCrumb title="Editar Perfil" parent="Inicio" />
      <Row>
        <Col md="5">
          <UserInfo onUpdate={handleUpdate} />
        </Col>
        <Col md="7">
          <PersonInfo />
          <UserAuthAuditPaginate />
        </Col>
      </Row>
    </AuthLayout>
  );
};

export default Home;

export const getServerSideProps = authorize("Home");
