import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { HistorialContent } from "@modules/planilla/historials/components/historial-content";
import { authorize } from "@services/authorize";

const IndexPim = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Historial Boletas" parent="Planilla" />
      <HistorialContent />
    </AuthLayout>
  );
};

export default IndexPim;

export const getServerSideProps = authorize("Historial Boletas");
