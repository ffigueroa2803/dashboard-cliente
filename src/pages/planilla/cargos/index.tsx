import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { CargoContent } from "@modules/planilla/cargos/components/cargo-content";
import { authorize } from "@services/authorize";

const IndexCargo = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Partición Presupuestal" parent="Autorización" />
      <CargoContent />
    </AuthLayout>
  );
};

export default IndexCargo;

export const getServerSideProps = authorize("Partición Presupuestal");
