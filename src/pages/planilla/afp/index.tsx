import React, { useState, useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { AfpTable } from "@modules/planilla/afp/components/afp-table";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { configDefaultServer } from "@modules/planilla/afp/default";
import { AfpEdit } from "@modules/planilla/afp/components/afp-edit";
import { useAfpFind } from "@modules/planilla/afp/hooks/use-afp-find";
import { Afp } from "@modules/planilla/afp/dtos/afp.entity";
import { AfpCreate } from "@modules/planilla/afp/components/afp-create";

const Index = () => {
  const { afps } = useSelector((state: RootState) => state.afp);
  const [options, setOptions] = useState<string>("UNDEFINED");
  const [pending, setPending] = useState<boolean>(false);
  const router = useRouter();

  const afpFind = useAfpFind();

  const switchOptions: any = {
    CREATE: (
      <AfpCreate
        onClose={() => setOptions("UNDEFINED")}
        onSave={(afp) => handleQuerySearch(afp.name || "")}
      />
    ),

    EDIT: (
      <AfpEdit
        onClose={() => setOptions("UNDEFINED")}
        onSave={(afp) => handleQuerySearch(afp.name || "")}
      />
    ),
    UNDEFINED: undefined,
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handleQuerySearch = (querySearch: string | string[]) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        querySearch,
      },
    });
  };

  const edit = async (afp: Afp) => {
    await afpFind.fetch(afp.id);
    setOptions("EDIT");
  };

  const desactivate = async () => {};

  const activate = async () => {};

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Leyes Sociales" parent="Planilla" />
      <div className="card card-body ">
        <AfpTable
          defaultQuerySearch={router?.query?.querySearch || ""}
          loading={pending}
          data={afps.items}
          perPage={afps?.meta.itemsPerPage}
          totalItems={afps?.meta.totalItems}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={{ edit: edit, desactivate: desactivate, activate: activate }}
        />

        <FloatButton
          icon={<Plus className="icon" />}
          color="success"
          onClick={() => setOptions("CREATE")}
        />
        {/*modal create*/}

        {switchOptions[options]}
      </div>
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize(
  "Leyes Sociales",
  async (ctx: any, store: Store) => configDefaultServer(ctx, store)
);
