/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { useRouter } from "next/router";
import { getCronogramas } from "@modules/planilla/cronogramas/apis";
import { cronogramaActions } from "@modules/planilla/cronogramas/store";
import { DateTime } from "luxon";
import {
  PlameContext,
  PlameProvider,
} from "@modules/planilla/plames/components/plame-context";
import { PlameContent } from "@modules/planilla/plames/components/plame-content";

const currentDate = DateTime.now();

const WrapperIndex = () => {
  const router = useRouter();

  const plameContext = useContext(PlameContext);

  const handleChangePerPage = (limit: number, page: number) => {
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handlePage = (page: number) => {
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleQuerySearch = (
    year: number,
    month: number,
    principal: boolean
  ) => {
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        year,
        month,
        principal,
      },
    });
  };

  useEffect(() => {
    plameContext.setYear(
      parseInt((router.query?.year as any) || currentDate.year)
    );
    plameContext.setMonth(
      parseInt((router.query?.month as any) || currentDate.month)
    );
    plameContext.setPrincipal(JSON.parse(`${router.query?.principal}`));
  }, [router]);

  return (
    <PlameContent
      onChangePage={handlePage}
      onChangeRowsPerPage={handleChangePerPage}
      onQuerySearch={handleQuerySearch}
    />
  );
};

const Index = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="PDT-Plame" parent="Planilla" />
      <PlameProvider>
        <WrapperIndex />
      </PlameProvider>
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize(
  "Cronogramas",
  async (ctx: any, store: Store) => {
    const currentDate = DateTime.now();
    const page = ctx.query?.page || 1;
    const querySearch = ctx.query?.querySearch || "";
    const limit = ctx.query?.limit || 30;
    const year = ctx.query?.year || currentDate.year;
    const month = ctx.query?.month || currentDate.month;
    const principal: boolean = ctx.query?.principal == "true" ? true : false;
    const result = await getCronogramas({
      page,
      querySearch,
      limit,
      year,
      month,
      principal,
      isPlame: true,
    })
      .then((data) => {
        return data;
      })
      .catch(() => {
        return { items: [], meta: {} };
      });
    store.dispatch(cronogramaActions.paginate(result));
  }
);
