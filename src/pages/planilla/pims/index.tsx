import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { PimContent } from "@modules/planilla/pims/components/pim-content";
import { authorize } from "@services/authorize";

const IndexPim = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="PIM Anual" parent="Planilla" />
      <PimContent />
    </AuthLayout>
  );
};

export default IndexPim;

export const getServerSideProps = authorize("Pim");
