import React, { useState, useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { DescuentoCreate } from "@modules/planilla/type_descuento/components/descuento-create";
import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";
import { DescuentoTable } from "@modules/planilla/type_descuento/components/descuento-table";
import { useRouter } from "next/router";
import { RootState } from "@store/store";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { configDefaultServer } from "@modules/planilla/type_descuento/default";
import { find } from "@modules/planilla/type_descuento/store";
import {
  findTypeDescuento,
  desactiveTypeDescuento,
} from "@modules/planilla/type_descuento/apis";
import { DescuentoEdit } from "@modules/planilla/type_descuento/components/descuento-edit";

const Index = () => {
  const { type_descuentos } = useSelector(
    (state: RootState) => state.type_descuento
  );
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<string>("UNDEFINED");
  const router = useRouter();
  const disptach = useDispatch();

  //swtich render
  const switchOptions: any = {
    CREATE: (
      <DescuentoCreate
        onClose={() => setOptions("UNDEFINED")}
        onSave={(type_descuento) =>
          handleQuerySearch(type_descuento.description || "")
        }
      />
    ),
    EDIT: (
      <DescuentoEdit
        onClose={() => setOptions("UNDEFINED")}
        onSave={(type_descuento) =>
          handleQuerySearch(type_descuento.description || "")
        }
      />
    ),
    UNDEFINED: undefined,
  };

  const show = async (typedescuento: TypeDescuento) => {
    const result = await findTypeDescuento(typedescuento.id);
    disptach(find(result));
    setOptions("EDIT");
  };

  const desactive = async (typedescuento: TypeDescuento) => {
    await desactiveTypeDescuento(typedescuento.id);
    handleQuerySearch(typedescuento.description || "");
  };
  const activate = async (typedescuento: TypeDescuento) => {
    await desactiveTypeDescuento(typedescuento.id);
    handleQuerySearch(typedescuento.description || "");
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handleQuerySearch = (querySearch: string | string[]) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        querySearch,
      },
    });
  };

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Tip. Descuento" parent="Planilla" />
      <div className="card card-body">
        <DescuentoTable
          defaultQuerySearch={router?.query?.querySearch || ""}
          loading={pending}
          data={type_descuentos.items}
          perPage={type_descuentos.meta.itemsPerPage}
          totalItems={type_descuentos.meta.totalItems}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={{ show: show, desactive: desactive, activate: activate }}
        />

        {/* btn flotante */}
        <FloatButton
          icon={<Plus className="icon" />}
          color="success"
          onClick={() => setOptions("CREATE")}
        />
        {/* modal switch options */}
        {switchOptions[options]}
      </div>
    </AuthLayout>
  );
};
export default Index;

export const getServerSideProps = authorize(
  "Tipos Descuentos",
  async (ctx: any, store: Store) => await configDefaultServer(ctx, store)
);
