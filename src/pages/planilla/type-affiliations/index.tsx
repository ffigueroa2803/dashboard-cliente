import React, { useState, useEffect, ReactElement } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { useRouter } from "next/router";
import { RootState } from "@store/store";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { useSelector, useDispatch } from "react-redux";
import { SindicatoCreate } from "@modules/planilla/type_sindicato/components/sindicato-create";
import { SindicatoTable } from "@modules/planilla/type_sindicato/components/sindicato-table";
import { TypeSindicato } from "@modules/planilla/type_sindicato/dtos/type_sindicato.entity";
import { authorize } from "@services/authorize";
import { find } from "@modules/planilla/type_sindicato/store";
import { Store } from "redux";
import { configDefaultServer } from "@modules/planilla/type_sindicato/deafult";
import { SindicatoEdit } from "@modules/planilla/type_sindicato/components/sindicate-edit";
import {
  findTypeSindicato,
  desactiveTypeSindicato,
} from "@modules/planilla/type_sindicato/apis";

const Index = () => {
  const { type_sindicatos } = useSelector(
    (state: RootState) => state.type_sindicato
  );
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<string>("UNDEFINED");
  const router = useRouter();
  const disptach = useDispatch();

  const switchOptions: { [key: string]: ReactElement } = {
    CREATE: (
      <SindicatoCreate
        onClose={() => setOptions("UNDEFINED")}
        onSave={(sindicato) => handleQuerySearch(sindicato.name || "")}
      />
    ),
    EDIT: (
      <SindicatoEdit
        onClose={() => setOptions("UNDEFINED")}
        onSave={(type_sindicato) =>
          handleQuerySearch(type_sindicato.name || "")
        }
      />
    ),
    UNDEFINED: <></>,
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const show = async (type_sindicato: TypeSindicato) => {
    const result = await findTypeSindicato(type_sindicato.id);
    disptach(find(result));
    setOptions("EDIT");
  };

  const desactive = async (type_sindicato: TypeSindicato) => {
    await desactiveTypeSindicato(type_sindicato.id);
    handleQuerySearch(type_sindicato.name || "");
  };

  const activate = async (type_sindicato: TypeSindicato) => {
    await desactiveTypeSindicato(type_sindicato.id);
    handleQuerySearch(type_sindicato.name || "");
  };
  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handleQuerySearch = (querySearch: string | string[]) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        querySearch,
      },
    });
  };

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Tip. Afiliaciones" parent="Planilla" />
      <div className="card card-body">
        <SindicatoTable
          defaultQuerySearch={router?.query?.querySearch || ""}
          loading={pending}
          data={type_sindicatos.items}
          perPage={type_sindicatos.meta.itemsPerPage}
          totalItems={type_sindicatos.meta.totalItems}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={{ show: show, desactive: desactive, activate: activate }}
        />

        {/* btn flotante */}
        <FloatButton
          icon={<Plus className="icon" />}
          color="success"
          onClick={() => setOptions("CREATE")}
        />
        {switchOptions[options]}
      </div>
    </AuthLayout>
  );
};
export default Index;

export const getServerSideProps = authorize(
  "Tipo Afiliaciones",
  async (ctx: any, store: Store) => configDefaultServer(ctx, store)
);
