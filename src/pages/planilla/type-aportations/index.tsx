import React, { useState, useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { useRouter } from "next/router";
import { RootState } from "@store/store";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { AportacionCreate } from "@modules/planilla/type_aportacion/components/aportacion-create";
import { AportacionTable } from "@modules/planilla/type_aportacion/components/aportacion-table";
import { TypeAportacion } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { configDefaultServer } from "@modules/planilla/type_aportacion/default";
import { AportationEdit } from "@modules/planilla/type_aportacion/components/aportation-edit";

import {
  findTypeAportacion,
  desactiveTypeAportacion,
} from "@modules/planilla/type_aportacion/apis";
import { find } from "@modules/planilla/type_aportacion/store";
import { AportacionConfig } from "@modules/planilla/type_aportacion/components/aportacion-config";

const Index = () => {
  const { type_aportaciones } = useSelector(
    (state: RootState) => state.type_aportacion
  );
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<string>("UNDEFINED");
  const router = useRouter();
  const dispatch = useDispatch();

  const switchOptions: any = {
    CREATE: (
      <AportacionCreate
        onClose={() => setOptions("UNDEFINED")}
        onSave={(typeaportacion) =>
          handleQuerySearch(typeaportacion.name || "")
        }
      />
    ),
    EDIT: (
      <AportationEdit
        onClose={() => setOptions("UNDEFINED")}
        onSave={(typeaportacion) =>
          handleQuerySearch(typeaportacion.name || "")
        }
      />
    ),
    CONFIGMAX: (
      <AportacionConfig
        onClose={() => setOptions("UNDEFINED")}
        onSave={(typeaportacion) =>
          handleQuerySearch(typeaportacion.name || "")
        }
      />
    ),
    UNDEFINED: undefined,
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handleQuerySearch = (querySearch: string | string[]) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        querySearch,
      },
    });
  };

  const edit = async (typeAportation: TypeAportacion) => {
    const result = await findTypeAportacion(typeAportation.id);
    dispatch(find(result));
    setOptions("EDIT");
  };

  const configMaxima = async (typeAportation: TypeAportacion) => {
    const result = await findTypeAportacion(typeAportation.id);
    dispatch(find(result));
    setOptions("CONFIGMAX");
  };

  const activate = async (typeAportation: TypeAportacion) => {
    await desactiveTypeAportacion(typeAportation.id);
    handleQuerySearch(typeAportation.name || "");
  };

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Tip. Aportación" parent="Planilla" />
      <div className="card card-body">
        <AportacionTable
          defaultQuerySearch={router?.query?.querySearch || ""}
          loading={pending}
          data={type_aportaciones.items}
          perPage={type_aportaciones.meta.itemsPerPage}
          totalItems={type_aportaciones.meta.totalItems}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={{
            edit: edit,
            configMaxima: configMaxima,
            activate: activate,
          }}
        />

        {/* btn flotante */}
        <FloatButton
          icon={<Plus className="icon" />}
          color="success"
          onClick={() => setOptions("CREATE")}
        />
        {/* modal create */}
        {switchOptions[options]}
      </div>
    </AuthLayout>
  );
};
export default Index;

export const getServerSideProps = authorize(
  "Tipos Aportación",
  async (ctx: any, store: Store) => configDefaultServer(ctx, store)
);
