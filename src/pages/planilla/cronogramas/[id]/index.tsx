import React, { useMemo } from "react";
import { decrypt } from "@services/crypt";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { workActions } from "@modules/scale/works/store";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { AuthLayout } from "@common/layouts";
import { setTitle } from "@common/store/screen.thunk";
import { BreadCrumb } from "@common/breadcrumb";
import { findCronograma } from "@modules/planilla/cronogramas/apis";
import { cronogramaActions } from "@modules/planilla/cronogramas/store";
import { CronogramaContent } from "@modules/planilla/cronogramas/components/cronograma-content";

const CronogramaId = () => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const displayTitle = useMemo(() => {
    let displayName = cronograma?.planilla?.name;
    // config
    const title = `Planilla ${displayName} - ${cronograma.month}/${cronograma.year}`;
    let description = ``;
    // adicional
    if (cronograma.adicional) {
      description += ` Adicional`;
      description += `${cronograma.remanente ? ` Remanente` : ""}`;
      description += ` ${cronograma.adicional}`;
    }
    // response
    return { title, description };
  }, [cronograma]);

  return (
    <AuthLayout>
      <BreadCrumb
        parent="Planilla"
        title={displayTitle.title}
        current={`Cronograma #${cronograma.id}`}
        description={displayTitle.description}
      />
      <CronogramaContent />
    </AuthLayout>
  );
};

export default CronogramaId;

export const getServerSideProps = authorize(
  `Cronograma`,
  async (ctx: any, store: Store) => {
    const query = ctx?.query || {};
    const id = parseInt(decrypt(query?.id) || "_error");
    const cronograma = await findCronograma(id);
    if (cronograma?.id) {
      store.dispatch(cronogramaActions.find(cronograma));
      store.dispatch(setTitle(`Cronograma #${cronograma.id}`));
    } else {
      store.dispatch(workActions.find({} as any));
      return {
        redirect: {
          destination: "/404",
          permanent: false,
        },
      };
    }
  }
);
