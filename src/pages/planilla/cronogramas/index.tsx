import React, { useContext, useEffect, useState } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { CronogramaTable } from "@modules/planilla/cronogramas/components/cronograma-table";
import { useRouter } from "next/router";
import { encrypt } from "@services/crypt";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { getCronogramas } from "@modules/planilla/cronogramas/apis";
import { cronogramaActions } from "@modules/planilla/cronogramas/store";
import {
  cronogramaEntityName,
  ICronogramaEntity,
} from "@modules/planilla/cronogramas/dtos/cronograma.entity";
import { DateTime } from "luxon";
import { CronogramaAddInfo } from "@modules/planilla/cronogramas/components/cronograma-add-info";
import { CronogramaRemoveHistorial } from "@modules/planilla/cronogramas/components/cronograma-remove-historial";
import { CronogramaCreate } from "@modules/planilla/cronogramas/components/cronograma-create";
import { CronogramaReport } from "@modules/planilla/cronogramas/components/cronograma-report";
import { useCronogramaDelete } from "@modules/planilla/cronogramas/hooks/use-cronograma-delete";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { Show } from "@common/show";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { CronogramaEdit } from "@modules/planilla/cronogramas/components/cronograma-edit";
import { historialEntityName } from "@modules/planilla/historials/dtos/historial.entity";

const Index = () => {
  const dispatch = useDispatch();

  const cronogramaDelete = useCronogramaDelete();

  const ability = useContext(CaslContext);

  const { cronogramas, cronograma } = useSelector(
    (state: RootState) => state.cronograma
  );
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<undefined | string>(undefined);
  const router = useRouter();

  const { query } = router;

  const switchOptions = {
    CREATE: "CREATE",
    ADD: "ADD",
    REMOVE: "REMOVE",
    REPORT: "REPORT",
    DELETE: "DELETE",
    EDIT: "EDIT",
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleQuerySearch = (
    year: number,
    month: number,
    principal: boolean
  ) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        year,
        month,
        principal,
      },
    });
  };

  const handleClick = (cronograma: ICronogramaEntity, action: string) => {
    const id = encrypt(`${cronograma.id}`);
    switch (action) {
      case "ADD":
      case "REMOVE":
      case "REPORT":
      case "DELETE":
      case "EDIT":
        setOptions(action);
        dispatch(cronogramaActions.find(cronograma));
        break;
      case "INFO":
        router.push(`${router.pathname}/ID:${id}`);
        break;
      default:
        break;
    }
  };

  const handleSave = (cronograma: ICronogramaEntity) => {
    setOptions("");
    handleQuerySearch(
      cronograma.year,
      cronograma.month,
      JSON.parse(`${router.query?.principal || "false"}`) as any
    );
  };

  const handleSelect = () => {
    switch (options) {
      case switchOptions.DELETE:
        cronogramaDelete.execute().catch(() => setOptions(""));
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (cronograma.id) handleSelect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cronograma, options]);

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Cronogramas" parent="Planilla" />
      <div className="card card-body">
        <CronogramaTable
          defaultYear={parseInt(`${query.year}`)}
          defaultMonth={parseInt(`${query.month}`)}
          defaultPrincipal={JSON.parse(`${query.principal || "false"}`) as any}
          loading={pending}
          data={cronogramas?.items || []}
          perPage={cronogramas?.meta?.itemsPerPage || 0}
          totalItems={cronogramas?.meta?.totalItems || 30}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={handleClick}
        />
      </div>
      {/* btn create */}
      <Show
        condition={ability.can(PermissionAction.CREATE, cronogramaEntityName)}>
        <FloatButton
          icon={<Plus />}
          color="success"
          onClick={() => setOptions(switchOptions.CREATE)}
        />
        <CronogramaCreate
          principal={JSON.parse((router.query?.principal as any) || "false")}
          isOpen={switchOptions.CREATE == options}
          onClose={() => setOptions("")}
          onSave={handleSave}
        />
      </Show>
      {/* modal add */}
      <CronogramaAddInfo
        isOpen={switchOptions.ADD == options}
        onClose={() => setOptions("")}
      />
      {/* modal remove */}
      <Show
        condition={ability.can(PermissionAction.DELETE, historialEntityName)}>
        <CronogramaRemoveHistorial
          isOpen={switchOptions.REMOVE == options}
          onClose={() => setOptions("")}
        />
      </Show>
      {/* modal report */}
      <CronogramaReport
        isOpen={switchOptions.REPORT == options}
        onClose={() => setOptions("")}
      />
      {/* modal edit */}
      <CronogramaEdit
        isOpen={switchOptions.EDIT == options}
        onClose={() => setOptions("")}
        onSave={() => null}
      />
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize(
  "Cronogramas",
  async (ctx: any, store: Store) => {
    const query = ctx.query;
    const currentDate = DateTime.now();
    const page = ctx.query?.page || 1;
    const querySearch = ctx.query?.querySearch || "";
    const limit = ctx.query?.limit || 30;
    const year = ctx.query?.year || currentDate.year;
    const month = ctx.query?.month || currentDate.month;
    const principal: boolean =
      typeof query?.principal == "undefined"
        ? false
        : JSON.parse(`${query.principal || "false"}`);
    const result = await getCronogramas({
      page,
      querySearch,
      limit,
      year,
      month,
      principal,
    })
      .then((data) => {
        return data;
      })
      .catch(() => {
        return { items: [], meta: {} };
      });
    store.dispatch(cronogramaActions.paginate(result));
  }
);
