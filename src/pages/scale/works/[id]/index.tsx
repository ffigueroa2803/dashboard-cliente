/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo } from "react";
import { decrypt } from "@services/crypt";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { findWork } from "@modules/scale/works/apis";
import { workActions } from "@modules/scale/works/store";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { AuthLayout } from "@common/layouts";
import { setTitle } from "@common/store/screen.thunk";
import { BreadCrumb } from "@common/breadcrumb";
import { PersonProfile } from "@modules/auth/person/components/person-profile";
import { WorkTab } from "@modules/scale/works/components/work-tab";
import { personActions } from "@modules/auth/person/store";
import { usePersonUserFind } from "@modules/auth/person/hooks/use-person-user-find";

const WorkId = () => {
  const dispatch = useDispatch();

  const personUserFind = usePersonUserFind();

  const { work } = useSelector((state: RootState) => state.work);
  const { person } = useSelector((state: RootState) => state.person);

  const displayTitle = useMemo(() => {
    const textName = `${work?.person?.name}`.toLowerCase();
    return `Trabajador ${textName}`;
  }, [work]);

  useEffect(() => {
    if (work?.id) {
      const person = work.person;
      dispatch(personActions.setPerson(person || ({} as any)));
    }
  }, [work]);

  useEffect(() => {
    if (person?.id) personUserFind.fetch().catch(() => null);
    return () => personUserFind.abort();
  }, [person]);

  return (
    <AuthLayout>
      <BreadCrumb parent="escalafón" title={displayTitle} />
      <PersonProfile />
      <WorkTab />
    </AuthLayout>
  );
};

export default WorkId;

export const getServerSideProps = authorize(
  `Información del Trabajador`,
  async (ctx: any, store: Store) => {
    const query = ctx?.query || {};
    const id = parseInt(decrypt(query?.id) || "_error");
    const work = await findWork(id);
    if (work?.id) {
      store.dispatch(workActions.find(work));
      store.dispatch(
        setTitle(`Trabajador ${work?.person?.name?.toLowerCase()}`)
      );
    } else {
      store.dispatch(workActions.find(null));
      return {
        redirect: {
          destination: "/404",
          permanent: false,
        },
      };
    }
  }
);
