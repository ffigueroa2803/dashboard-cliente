import React, { useContext, useEffect, useState } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { workActions } from "@modules/scale/works/store";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { getWorks } from "@modules/scale/works/apis";
import { WorkTable } from "@modules/scale/works/components/work-table";
import { useRouter } from "next/router";
import {
  WorkEntity,
  workEntityName,
} from "@modules/scale/works/dtos/work.entity";
import { encrypt } from "@services/crypt";
import { FloatButton } from "@common/button/float-button";
import { File, Plus } from "react-feather";
import { WorkStepCreate } from "@modules/scale/works/components/work-step-create";
import { WorkReport } from "@modules/scale/works/components/work-report";
import { Show } from "@common/show";
import { WorkReportRenta } from "@modules/planilla/works/components/work-report-renta";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { historialEntityName } from "@modules/planilla/historials/dtos/historial.entity";
import { IWorkPaginate } from "@modules/scale/works/work.interfaces";

const Index = () => {
  const dispatch = useDispatch();

  const ability = useContext(CaslContext);

  const { works } = useSelector((state: RootState) => state.work);
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<undefined | string>(undefined);
  const [isRenta, setIsRenta] = useState<boolean>(false);
  const router = useRouter();

  const { query } = router;

  const switchOptions = {
    CREATE: "CREATE",
  };

  const workOptions = (): any => {
    const currentOptions: any[] = [];
    // validar renta
    if (ability.can(PermissionAction.READ, historialEntityName)) {
      currentOptions.push({
        name: "Renta",
        grow: true,
        center: true,
        selector: (row: WorkEntity) => (
          <File
            className="icon cursor-pointer"
            onClick={() => handleReport(row)}
          />
        ),
      });
    }
    // response
    return currentOptions;
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleQuerySearch = (params: any) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        ...params,
      },
    });
  };

  const handleClick = (work: WorkEntity) => {
    const id = encrypt(`${work.id}`);
    router.push(`${router.pathname}/ID:${id}`);
  };

  const handleReport = (work: WorkEntity) => {
    dispatch(workActions.find(work));
    setIsRenta(true);
  };

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <WorkReport>
        <BreadCrumb title="Trabajadores" parent="Escalafón" />
        <div className="card card-body">
          <WorkTable
            defaultFilters={{
              ...query,
              state: query.state as any,
            }}
            loading={pending}
            data={works.items}
            perPage={works.meta.itemsPerPage}
            totalItems={works.meta.totalItems}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleChangePerPage}
            onQuerySearch={handleQuerySearch}
            onClick={handleClick}
            options={workOptions()}
          />
        </div>
        {/* btn flotante */}
        <Show condition={ability.can(PermissionAction.CREATE, workEntityName)}>
          <FloatButton
            icon={<Plus />}
            color="success"
            onClick={() => setOptions(switchOptions.CREATE)}
          />
          {/* modal create */}
          {switchOptions.CREATE == options ? (
            <WorkStepCreate
              onClose={() => setOptions(undefined)}
              onSave={(work) => handleQuerySearch(work.orderBy.toLowerCase())}
            />
          ) : null}
        </Show>
      </WorkReport>
      {/* report renta*/}
      <Show
        condition={
          isRenta && ability.can(PermissionAction.REPORT, historialEntityName)
        }>
        <WorkReportRenta isOpen={true} onClose={() => setIsRenta(false)} />
      </Show>
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize(
  "Trabajadores",
  async (ctx: any, store: Store) => {
    const { query } = ctx;

    // setting query
    query.page = query.page || 1;

    query.limit = query.limit || 30;

    if (query.state == "") {
      query.state = undefined;
    } else {
      query.state = query.state == "true";
    }

    // setting params
    const params: IWorkPaginate = {
      page: query.page,
      limit: query.limit,
      state: query.state,
      querySearch: query.querySearch,
      typeCargoIds: query.typeCargoId ? [query.typeCargoId] : undefined,
      conditions: query.condition ? [query.condition] : undefined,
      gender: query.gender || undefined,
    };

    const result = await getWorks(params);

    if (!result?.err) store.dispatch(workActions.paginate(result));
    else
      store.dispatch(
        workActions.paginate({
          items: [],
          meta: {
            totalItems: 0,
            totalPages: 0,
            itemsPerPage: query.limit,
          },
        })
      );
  }
);
