/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { useCampusUserFind } from "src/application/auth/campuses/use-campus-user-find";
import { HourhandContent } from "@modules/scale/hourhands/components/hourhand-content";

const Index = () => {
  const campusUserFind = useCampusUserFind();

  const handleUser = () => campusUserFind.fetch().catch(() => null);

  useEffect(() => {
    handleUser();
  }, []);

  return (
    <AuthLayout>
      <BreadCrumb title="Horarios" parent="Escalafón" />
      <HourhandContent />
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize("Horarios");
