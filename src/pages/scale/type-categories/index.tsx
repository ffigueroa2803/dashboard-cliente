/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { CategoriaCreate } from "@modules/scale/type-categories/components/categoria-create";
import { CategoriaTable } from "@modules/scale/type-categories/components/categoria-table";
import { useRouter } from "next/router";
import { RootState } from "@store/store";
import { FloatButton } from "@common/button/float-button";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { authorize } from "@services/authorize";
import { Store } from "redux";
import { configDefaultServer } from "@modules/scale/type-categories/default";
import { useTypeCategoriaFind } from "@modules/scale/type-categories/hooks/use-type-categoria-find";
import { CategoriaEdit } from "@modules/scale/type-categories/components/categoria-edit";
import { CategoriaCoin } from "@modules/scale/type-categories/components/categoria-coin";
import { ITypeCategoryEntity } from "@modules/scale/type-categories/dtos/type-category.entity";
import { CategoriaDiscount } from "@modules/scale/type-categories/components/categoria-discount";
import { typeCategoryActions } from "@modules/scale/type-categories/store";

const Index = () => {
  const dispatch = useDispatch();

  const { type_categorias } = useSelector(
    (state: RootState) => state.type_categoria
  );
  const [pending, setPending] = useState(true);
  const [options, setOptions] = useState<string>("UNDEFINED");
  const { fetch } = useTypeCategoriaFind();
  const router = useRouter();

  const switchOptions: any = {
    CREATE: (
      <CategoriaCreate
        onClose={() => setOptions("UNDEFINED")}
        onSave={() => handleQuerySearch("")}
      />
    ),
    EDIT: (
      <CategoriaEdit
        onClose={() => setOptions("UNDEFINED")}
        onSave={() => handleQuerySearch("")}
      />
    ),
    COIN: <CategoriaCoin onClose={() => setOptions("UNDEFINED")} />,
    DISCOUNT: <CategoriaDiscount onClose={() => setOptions("UNDEFINED")} />,
    UNDEFINED: undefined,
  };

  const handlePage = (page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: { ...router.query, page },
    });
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page,
        limit,
      },
    });
  };

  const handleQuerySearch = (querySearch: string | string[]) => {
    setPending(true);
    router.push({
      pathname: router.pathname,
      query: {
        ...router.query,
        page: 1,
        querySearch,
      },
    });
  };

  const edit = async (typecategoria: ITypeCategoryEntity) => {
    dispatch(typeCategoryActions.find(typecategoria));
    setOptions("EDIT");
  };

  const coin = async (typecategoria: ITypeCategoryEntity) => {
    dispatch(typeCategoryActions.find(typecategoria));
    setOptions("COIN");
  };

  const discount = (typecategoria: ITypeCategoryEntity) => {
    dispatch(typeCategoryActions.find(typecategoria));
    setOptions("DISCOUNT");
  };

  const delet = (typecategoria: ITypeCategoryEntity) => {
    console.log(typecategoria);
  };

  useEffect(() => {
    if (pending) setPending(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return (
    <AuthLayout>
      <BreadCrumb title="Tip. Categoría" parent="Planilla" />
      <div className="card card-body">
        <CategoriaTable
          defaultQuerySearch={router?.query?.querySearch || ""}
          loading={pending}
          data={type_categorias.items}
          perPage={type_categorias.meta.itemsPerPage}
          totalItems={type_categorias.meta.totalItems}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onQuerySearch={handleQuerySearch}
          onClick={{
            edit: edit,
            mont: coin,
            discount: discount,
            delet: delet,
          }}
        />

        {/* btn flotante */}
        <FloatButton
          icon={<Plus className="icon" />}
          color="success"
          onClick={() => setOptions("CREATE")}
        />
        {/* modal create */}
        {switchOptions[options]}
      </div>
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize(
  "Tipo Categoría",
  async (ctx: any, store: Store) => configDefaultServer(ctx, store)
);
