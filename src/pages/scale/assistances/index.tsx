import React from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { AssistanceContent } from "@modules/scale/assistances/components/assistance-content";
import { AssistanceOptions } from "@modules/scale/assistances/components/assistance-options";

const Index = () => {
  return (
    <AuthLayout>
      <AssistanceOptions>
        <BreadCrumb title="Asistencias" parent="Escalafón" />
        <AssistanceContent />
      </AssistanceOptions>
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize("Asistencias");
