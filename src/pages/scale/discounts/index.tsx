/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { AuthLayout } from "@common/layouts";
import { BreadCrumb } from "@common/breadcrumb";
import { authorize } from "@services/authorize";
import { DiscountContent } from "@modules/scale/discounts/components/discount-content";
import { useCampusUserFind } from "src/application/auth/campuses/use-campus-user-find";

const Index = () => {
  const campusUserFind = useCampusUserFind();

  const handleUser = () => campusUserFind.fetch().catch(() => null);

  useEffect(() => {
    handleUser();
  }, []);

  return (
    <AuthLayout>
      <BreadCrumb title="Descuentos" parent="Escalafón" />
      <DiscountContent />
    </AuthLayout>
  );
};

export default Index;

export const getServerSideProps = authorize("Descuentos");
