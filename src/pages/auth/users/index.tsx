import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { UserContent } from "@modules/auth/users/components/user-content";
import { authorize } from "@services/authorize";

const IndexUser = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Usuarios" parent="Autorización" />
      <UserContent />
    </AuthLayout>
  );
};

export default IndexUser;

export const getServerSideProps = authorize("Usuarios");
