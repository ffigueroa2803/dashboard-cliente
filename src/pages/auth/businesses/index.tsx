import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { BusinessContent } from "@modules/auth/businesses/components/business-content";
import { authorize } from "@services/authorize";

const IndexRole = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Empresas" parent="Autorización" />
      <BusinessContent />
    </AuthLayout>
  );
};

export default IndexRole;

export const getServerSideProps = authorize("Empresas");
