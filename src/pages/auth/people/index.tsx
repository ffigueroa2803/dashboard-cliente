import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { PersonContent } from "@modules/auth/person/components/person-content";
import { authorize } from "@services/authorize";

const IndexPeople = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Personas" parent="Autorización" />
      <PersonContent />
    </AuthLayout>
  );
};

export default IndexPeople;

export const getServerSideProps = authorize("Personas");
