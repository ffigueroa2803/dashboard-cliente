import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { RoleContent } from "@modules/auth/roles/components/role-content";
import { authorize } from "@services/authorize";

const IndexRole = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Roles" parent="Autorización" />
      <RoleContent />
    </AuthLayout>
  );
};

export default IndexRole;

export const getServerSideProps = authorize("Roles");
