import { BreadCrumb } from "@common/breadcrumb";
import { AuthLayout } from "@common/layouts";
import { ClientContent } from "@modules/auth/clients/components/client-content";
import { authorize } from "@services/authorize";

const IndexRole = () => {
  return (
    <AuthLayout>
      <BreadCrumb title="Clientes" parent="Autorización" />
      <ClientContent />
    </AuthLayout>
  );
};

export default IndexRole;

export const getServerSideProps = authorize("Clientes");
