import type { AppProps, AppContext } from "next/app";
import store from "@store/index";
import "../assets/scss/app.scss";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import { CaslProvider } from "@modules/auth/casls/casl.context";
import { MenuProvider } from "@common/sidebar/menu.contenxt";

const MyDashboard = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <CaslProvider>
        <MenuProvider>
          <Component {...pageProps} />
        </MenuProvider>
      </CaslProvider>
      <ToastContainer position="bottom-right" />
    </>
  );
};

MyDashboard.getInitialProps = async ({ Component, ctx }: AppContext) => {
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};
  return { pageProps };
};

export default store.withRedux(MyDashboard);
