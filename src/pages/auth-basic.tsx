/* eslint-disable react-hooks/exhaustive-deps */
import type { NextPage } from "next";
import { useEffect, useMemo, useState } from "react";
import { GuestLayout } from "@common/layouts";
import { Container, Row, Col } from "reactstrap";
import { LoginForm } from "@common/auth";
import styles from "../assets/scss/pages/SignIn.module.scss";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { Logo } from "../common/logo";
import { useRouter } from "next/router";
import { RootState } from "../store/store";
import { external } from "@services/authorize";

const AuthBasic: NextPage = () => {
  const router = useRouter();

  const [backgrond, setBackground] = useState<string | undefined>(undefined);

  const { width } = useSelector((state: RootState) => state.screen);
  const { currentClient } = useSelector((state: RootState) => state.client);

  const hasLogo = `${currentClient.logoUrl || ""}`.length > 0;
  const hasBackground = currentClient.backgrounds?.length > 0;

  const showBackgroud = useMemo<boolean>(() => {
    return width >= 1200;
  }, [width]);

  const randomBackground = () => {
    const max = currentClient.backgrounds.length;
    const min = 0;
    const index = Math.floor(Math.random() * (max - min)) + min;
    const image = currentClient.backgrounds[index];
    setBackground(image.url);
  };

  const handleRedirectPage = (data: any) => {
    const clientRedirect: string = router.query?.redirect?.toString() || "";
    const url = new URL(clientRedirect);
    url.searchParams.set("accessToken", data.accessToken);
    url.searchParams.set("rememberToken", data.rememberToken);
    url.searchParams.set("type", data.type);
    location.href = url.toString();
  };

  useEffect(() => {
    if (hasBackground) randomBackground();
  }, [currentClient]);

  return (
    <GuestLayout>
      <Container fluid={true}>
        <Row>
          <Show condition={showBackgroud}>
            <Col md="6" lg="7" xl="5" className={styles.without_padding}>
              <div
                className={styles.login__background}
                style={{
                  backgroundImage: `url(${backgrond})`,
                }}
              />
            </Col>
          </Show>
          <Col xl="7" className="p-0">
            <div className="login-card">
              <div>
                <Show condition={hasLogo}>
                  <div className={styles.login__icon}>
                    <Logo urlImage={currentClient.logoUrl} />
                  </div>
                </Show>
                <LoginForm
                  external
                  onResetPassword={() => router.push("/recovery-account")}
                  onSignIn={handleRedirectPage}
                />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </GuestLayout>
  );
};

export default AuthBasic;

export const getServerSideProps = external("Auth Basic");
