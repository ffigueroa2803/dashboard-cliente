import { IClockEntity } from "@modules/clock/clocks/dtos/clock.entity";

export interface IUserEntity {
  id: string;
  clockId: number;
  credentialNumber: string;
  token: string;
  clock: IClockEntity;
  state: boolean;
}
