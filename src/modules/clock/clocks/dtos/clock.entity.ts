export interface IClockEntity {
  id: string;
  clientId: string;
  name: string;
  host: string;
  port: number;
  onLine: boolean;
}
