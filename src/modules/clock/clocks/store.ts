import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IClockEntity } from "./dtos/clock.entity";

export interface ClocksState {
  clocks: ResponsePaginateDto<IClockEntity>;
}

const initialState: ClocksState = {
  clocks: {
    meta: {
      totalItems: 0,
      itemsPerPage: 30,
      totalPages: 0,
    },
    items: [],
  },
};

export const clockSlice = createSlice({
  name: "clock@clocks",
  initialState,
  reducers: {
    paginate: (
      state: ClocksState,
      action: PayloadAction<ResponsePaginateDto<IClockEntity>>
    ) => {
      state.clocks = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.clock };
    },
  },
});

export const clockReducer = clockSlice.reducer;

export const clockActions = clockSlice.actions;
