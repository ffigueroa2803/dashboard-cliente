import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getBanks } from "../apis";
import { IBankEntity } from "../dtos/bank.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange?: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const BankSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={getBanks}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: IBankEntity) => `${row.name}`.toLowerCase(),
        value: (row: IBankEntity) => row.id,
      }}
    />
  );
};
