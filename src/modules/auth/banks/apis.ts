import { AuthRequest } from "@services/auth.request";
import { PaginateDto } from "@services/dtos";

const { request } = AuthRequest();

export const getBanks = async ({ page, querySearch, limit }: PaginateDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  return await request.get(`banks`, { params }).then((res) => res.data);
};
