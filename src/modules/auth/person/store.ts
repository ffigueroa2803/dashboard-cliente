import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IUserEntity } from "../users/dtos/user.entity";
import { PersonEntity } from "./dtos/person.entity";

export interface PersonState {
  people: ResponsePaginateDto<PersonEntity>;
  person: PersonEntity;
  user: IUserEntity;
}

export const initialState: PersonState = {
  people: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  person: {} as any,
  user: {} as any,
};

export const personSlice = createSlice({
  name: "person",
  initialState,
  reducers: {
    paginate: (
      state: PersonState,
      { payload }: PayloadAction<ResponsePaginateDto<PersonEntity>>
    ) => {
      state.people = payload;
      return state;
    },
    setPerson: (
      state: PersonState,
      { payload }: PayloadAction<PersonEntity>
    ) => {
      state.person = payload;
      state.people?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }

        return item;
      });

      return state;
    },
    changeImage: (state: PersonState, { payload }: PayloadAction<string>) => {
      state.person.imageUrl = payload;
      return state;
    },
    setUser: (state: PersonState, { payload }: PayloadAction<IUserEntity>) => {
      state.user = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<PersonState>) => {
      return { ...state, ...payload.person };
    },
  },
});

export const personReducer = personSlice.reducer;

export const personActions = personSlice.actions;
