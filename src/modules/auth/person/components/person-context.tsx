import { createContext, FC, useState } from "react";

export enum personContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  PLUS = "PLUS",
  MINUS = "MINUS",
}

export const PersonContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: personContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const HistorialProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<personContextEnum>(
    personContextEnum.DEFAULT
  );

  return (
    <PersonContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}
    >
      {children}
    </PersonContext.Provider>
  );
};
