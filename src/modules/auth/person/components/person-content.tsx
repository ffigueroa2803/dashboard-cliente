import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import {
  PersonContext,
  HistorialProvider,
  personContextEnum,
} from "./person-context";
import { usePersonList } from "../hooks/use-person-list";
import { PersonTable } from "./person-table";
import { PersonEdit } from "./person-edit";
import { PersonCreate } from "./person-create";
import { FloatButton } from "@common/button/float-button";
import { PersonEntity } from "../dtos/person.entity";

const PersonWrapper = () => {
  const personContext = useContext(PersonContext);

  const personList = usePersonList({
    page: 1,
    limit: 30,
    querySearch: personContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    personList.changeQuery({ name: "page", value: page });
    personContext.setClearRows(true);
    personContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    personList.changeQuery({ name: "limit", value: limit });
    personContext.setClearRows(true);
    personContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = (person: PersonEntity) => {
    personContext.setOptions(personContextEnum.DEFAULT);
    personContext.setQuerySearch(person.documentNumber);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    personList.changeQuery({
      name: "querySearch",
      value: personContext.querySearch || "",
    });
  }, [personContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      personContext.setClearRows(true);
      personContext.setIds([]);
      personList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={personContext.querySearch}
                  disabled={personList.pending}
                  onChange={({ target }) =>
                    personContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <PersonTable
            loading={personList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => personContext.setOptions(personContextEnum.CREATE)}
      />
      <PersonCreate
        isOpen={personContext.options == personContextEnum.CREATE}
        onClose={() => personContext.setOptions(personContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <PersonEdit
        isOpen={personContext.options == personContextEnum.EDIT}
        onClose={() => personContext.setOptions(personContextEnum.DEFAULT)}
      />
    </>
  );
};

export const PersonContent = () => {
  return (
    <HistorialProvider>
      <PersonWrapper />
    </HistorialProvider>
  );
};
