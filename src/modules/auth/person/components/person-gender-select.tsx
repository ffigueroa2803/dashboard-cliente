/* eslint-disable no-unused-vars */
import { SelectBasic } from "@common/select/select-basic";

export interface PersonGenderSelectProps {
  onChange(opt: any): void;
  name: string;
  value: any;
  disabled?: boolean;
  placeholder?: string;
}

export function PersonGenderSelect({
  onChange,
  name,
  value,
  disabled,
  placeholder,
}: PersonGenderSelectProps) {
  const optionsGender = [
    { label: "Hombre", value: "M" },
    { label: "Mujer", value: "F" },
  ];

  return (
    <SelectBasic
      options={optionsGender}
      placeholder={placeholder}
      name={name}
      value={value || ""}
      onChange={(obj) => onChange(obj)}
      isDisabled={disabled}
    />
  );
}
