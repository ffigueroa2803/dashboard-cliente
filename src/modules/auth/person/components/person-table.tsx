import { CaslContext } from "@modules/auth/casls/casl.context";
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { PersonEntity } from "../dtos/person.entity";
import { personActions } from "../store";
import { personContextEnum, PersonContext } from "./person-context";

interface IProps {
  onClick?: (person: PersonEntity, action: personContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const PersonTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { people } = useSelector((state: RootState) => state.person);

  const personContext = useContext(PersonContext);
  const ability = useContext(CaslContext);

  const handleClick = (person: PersonEntity, action: personContextEnum) => {
    dispatch(personActions.setPerson(person));
    personContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(person, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      grow: true,
      cell: (row: PersonEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, personContextEnum.EDIT)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,

        cell: (row: PersonEntity) => row.id,
      },
      {
        name: "Nombre Completo",
        wrap: true,
        cell: (row: PersonEntity) => (
          <span className="uppercase">{row?.fullName || ""}</span>
        ),
      },
      {
        name: "Género",
        wrap: true,
        center: true,
        cell: (row: PersonEntity) => (
          <span className="uppercase">{row?.gender || ""}</span>
        ),
      },
      {
        name: "Tipo Documento",
        cell: (row: PersonEntity) => (
          <span className="uppercase">{row?.documentType?.name || ""}</span>
        ),
      },
      {
        name: "N° Documento",
        center: true,
        cell: (row: PersonEntity) => (
          <span className="uppercase">{row?.documentNumber || ""}</span>
        ),
      },
      {
        name: "F. Nacimiento",
        center: true,
        cell: (row: PersonEntity) => (
          <span className="uppercase">
            {new Date(row?.dateOfBirth || "").toLocaleDateString()}
          </span>
        ),
      },
      {
        name: "Ubigeo Nac.",
        center: true,
        cell: (row: PersonEntity) => (
          <span className="uppercase">{row?.badgeId || ""}</span>
        ),
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [people]);

  return (
    <>
      <Datatable
        data={people.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={people?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={people?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
