import React, { useMemo } from "react";
import { Card, CardHeader, Row, Col } from "reactstrap";
import Background from "./assets/bg1.jpg";
import { DateTime } from "luxon";
import { PersonChangeImage } from "./person-change-image";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps {
  children?: any;
}

export const PersonProfile = ({ children }: IProps) => {
  const { person } = useSelector((state: RootState) => state.person);

  const displayDate = useMemo(() => {
    if (!person?.dateOfBirth) return "N/A";
    return DateTime.fromSQL(person?.dateOfBirth).toFormat("dd LLL yyyy");
  }, [person]);

  const displayTitle = useMemo(() => {
    const textName = `${person?.name}`.toLowerCase();
    return `Trabajador ${textName}`;
  }, [person]);

  return (
    <div className="user-profile">
      <Card className="card hovercard text-center">
        <CardHeader
          className="cardheader"
          style={{
            background: `url(${Background.src})`,
            height: "350px",
          }}
        />
        {/* editar image */}
        <PersonChangeImage />
        {/* info */}
        <div className="info">
          <Row>
            <Col sm="6" lg="4" className="order-sm-1 order-xl-0">
              <Row>
                <Col md="6">
                  <div className="ttl-info text-left">
                    <h6>
                      <i className="fa fa-envelope mr-2"></i> Email
                    </h6>
                    <span>{person?.emailContact || "N/A"}</span>
                  </div>
                </Col>
                <Col md="6">
                  <div className="ttl-info text-left ttl-sm-mb-0">
                    <h6>
                      <i className="fa fa-calendar"></i> F. Nacimiento
                    </h6>
                    <span>{displayDate}</span>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col sm="12" lg="4" className="order-sm-0 order-xl-1">
              <div className="user-designation">
                <div className="title">
                  <a target="_blank" href="#javascript">
                    {displayTitle}
                  </a>
                </div>
                <div className="desc mt-2">
                  {person?.documentNumber || "N/A"}
                </div>
              </div>
            </Col>
            <Col sm="6" lg="4" className="order-sm-2 order-xl-2">
              <Row>
                <Col md="6">
                  <div className="ttl-info text-left ttl-xs-mt">
                    <h6>
                      <i className="fa fa-phone"></i> Contacto
                    </h6>
                    <span>{person?.phone || "N/A"}</span>
                  </div>
                </Col>
                <Col md="6">
                  <div className="ttl-info text-left ttl-sm-mb-0">
                    <h6>
                      <i className="fa fa-location-arrow"></i> Ubicación
                    </h6>
                    <span>{person?.address || "N/A"}</span>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          {children || null}
        </div>
      </Card>
    </div>
  );
};
