import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { useState } from "react";
import { Edit, Info } from "react-feather";
import { useSelector } from "react-redux";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import { PersonSerialize } from "../serializes/person.serialize";
import { PersonEdit } from "./person-edit";

export const PersonInfo = () => {
  const { person } = useSelector((state: RootState) => state.person);

  const [isEdit, setIsEdit] = useState<boolean>(false);
  const personSerialize = plainToClass(PersonSerialize, person);

  return (
    <>
      <Card>
        <CardHeader>
          <h6>
            <Edit
              className="icon close cursor-pointer"
              onClick={() => setIsEdit(true)}
            />
            <Info className="icon" /> Datos Personales
          </h6>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="6" className="mb-3">
              <label>Prefijo</label>
              <h6 className="uppercase">{person.prefix || "N/A"}</h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>Nombre Completo</label>
              <h6 className="uppercase">{person.fullName || "N/A"}</h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>Tipo De Documento</label>
              <h6 className="uppercase">
                {person.documentType?.name || "N/A"}
              </h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>N° Documento</label>
              <h6 className="uppercase">{person.documentNumber || "N/A"}</h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>Sexo</label>
              <h6 className="uppercase">
                {personSerialize.displayGender || "N/A"}
              </h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>Fecha de Nacimiento</label>
              <h6 className="uppercase">
                {personSerialize.displayDateOfBirth || "N/A"}
              </h6>
            </Col>

            <Col md="12" className="mb-3">
              <label>Lugar de Nacimiento</label>
              <h6 className="uppercase">N/A</h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>N° Teléfono</label>
              <h6 className="uppercase">{person.phone || "N/A"}</h6>
            </Col>

            <Col md="6" className="mb-3">
              <label>Correo</label>
              <h6>{person.emailContact || "N/A"}</h6>
            </Col>

            <Col md="12" className="mb-3">
              <label>Dirección</label>
              <h6 className="uppercase">{person.address || "N/A"}</h6>
            </Col>
          </Row>
        </CardBody>
      </Card>
      {/* editar persona */}
      <PersonEdit
        isOpen={isEdit}
        onClose={() => setIsEdit(false)}
        onSave={() => null}
      />
    </>
  );
};
