import { LoadingContent } from "@common/loading/components/loading-content";
import { css } from "@emotion/react";
import React, { useEffect, useState } from "react";
import { usePersonChangeImage } from "../hooks/use-person-change-image";
import ScaleLoader from "react-spinners/ClipLoader";
import { Media } from "reactstrap";
import ProfileImage from "@assets/images/perfil.jpg";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export const PersonChangeImage = () => {
  const { person } = useSelector((state: RootState) => state.person);

  const [isUpdate, setIsUpdate] = useState<boolean>(false);

  const personChangeImage = usePersonChangeImage();

  const handleFile = ({ files }: { files: FileList | null }) => {
    if (!files) return personChangeImage.setImage("");
    personChangeImage.setImage(files[0]);
    setIsUpdate(true);
  };

  useEffect(() => {
    if (isUpdate) {
      personChangeImage.save();
    }
  }, [isUpdate]);

  useEffect(() => {
    if (isUpdate) setIsUpdate(false);
  }, [isUpdate]);

  return (
    <>
      <div className="user-image">
        <div className="avatar">
          <Media
            body
            src={person?.imageUrl ? person?.imageUrl : ProfileImage.src}
            alt=""
            data-intro="This is Profile image"
            style={{ objectFit: "cover" }}
          />
        </div>
        <div className="icon-wrapper" data-intro="Change Profile image here">
          <i className="icofont icofont-pencil-alt-5">
            <input
              disabled={personChangeImage.pending}
              onChange={({ target }) => handleFile(target)}
              className="upload"
              type="file"
              accept="image/png,image/jpg,image/jpeg"
            />
          </i>
        </div>
      </div>
    </>
  );
};
