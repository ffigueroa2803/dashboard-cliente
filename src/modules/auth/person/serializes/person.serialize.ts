import { DateTime } from "luxon";

export class PersonSerialize {
  public dateOfBirth!: string;
  public gender!: string;

  get displayDateOfBirth() {
    const date = DateTime.fromSQL(this.dateOfBirth);
    if (!date.isValid) return undefined;
    return date.toFormat("dd/MM/yyyy");
  }

  get displayGender() {
    return this.gender == "M" ? "Hombre" : "Mujer";
  }
}
