import { authV2Request } from "@services/auth-v2.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { personActions } from "../store";

const request = authV2Request();

export const usePersonUserFind = () => {
  const dispatch = useDispatch();
  let abortController: AbortController;
  const { person } = useSelector((state: RootState) => state.person);
  const { user } = useSelector((state: RootState) => state.auth);
  const [isError, setIsError] = useState<boolean>(false);
  const [pending, setPending] = useState<boolean>(false);

  const abort = () => abortController?.abort();

  const fetch = () => {
    setPending(true);
    setIsError(false);
    abortController = new AbortController();
    return new Promise((resolve, reject) => {
      request
        .get(`people/${person.id}/users/${user?.campusId}`, {
          signal: abortController.signal,
        })
        .then(({ data }) => {
          dispatch(personActions.setUser(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(true);
          dispatch(personActions.setUser({} as any));
          reject(err);
        });
    });
  };

  return {
    pending,
    isError,
    abort,
    fetch,
  };
};
