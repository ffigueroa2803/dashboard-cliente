import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { personActions } from "../store";
import { toast } from "react-toastify";

const { request } = AuthRequest();

export const usePersonChangeImage = () => {
  const dispatch = useDispatch();

  const { person } = useSelector((state: RootState) => state.person);

  const [image, setImage] = useState<Blob | string>("");
  const [pending, setPending] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    const form = new FormData();
    form.append("file", image);
    // send request
    await request
      .put(`people/${person.id}/changeImage`, form)
      .then(({ data }) => {
        dispatch(personActions.changeImage(data.imageUrl));
        toast.success("Imagen cambiada!!!");
      })
      .catch(() => toast.error("No se pudo cambiar la imagen"));
    setPending(false);
  };

  return {
    pending,
    image,
    setImage,
    save,
  };
};
