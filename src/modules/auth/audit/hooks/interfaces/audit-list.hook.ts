import { PaginateEntity } from "@services/entities/paginate.entity";
import { AuditEntity } from "../../domain/audit.entity";

export interface AuditListHook {
  isLoading: boolean;
  data: PaginateEntity<AuditEntity> | undefined;
}
