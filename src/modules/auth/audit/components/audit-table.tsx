import { Clock } from "react-feather";
import { AuditListHook } from "../hooks/interfaces/audit-list.hook";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { AuditIcon } from "./audit-icon";
import { DateTime } from "luxon";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

export interface AuditTableProps {
  hook: AuditListHook;
}

export function AuditTable({ hook }: AuditTableProps) {
  const { dark } = useSelector((state: RootState) => state.screen);

  return (
    <div className="appointment-table table-responsive">
      <table className="table table-bordernone">
        <tbody>
          {hook.data?.items?.map((item) => (
            <tr key={`item-audit-${item.id}`}>
              <td width="50px">
                <AuditIcon action={item.action} />
              </td>
              <td className="img-content-box">
                <a className="d-block f-w-500">{item.name}</a>
                <span className={`f-light ${dark ? "text-white" : ""}`}>
                  {DateTime.fromISO(item.dateEvent).toFormat(
                    "dd/MM/yyyy HH:mm"
                  )}{" "}
                  <Clock className="icon" />
                </span>
              </td>
            </tr>
          ))}
          {/* loading */}
          <Show condition={hook.isLoading}>
            <tr>
              <td colSpan={2}>
                <div className="text-center">
                  <LoadingSimple loading />
                </div>
              </td>
            </tr>
          </Show>
          {/* no data */}
          <Show condition={!hook.isLoading && !hook.data?.meta?.totalItems}>
            <tr>
              <td colSpan={2}>
                <div className="text-center">
                  <b className={dark ? "text-white" : ""}>
                    No hay registros disponibles
                  </b>
                </div>
              </td>
            </tr>
          </Show>
        </tbody>
      </table>
    </div>
  );
}
