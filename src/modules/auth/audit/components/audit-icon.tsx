import { RootState } from "@store/store";
import { Activity, BookOpen, Edit, File, Trash } from "react-feather";
import { useSelector } from "react-redux";

export interface AuditIconProps {
  action: string;
}

export function AuditIcon({ action }: AuditIconProps) {
  const { dark } = useSelector((state: RootState) => state.screen);

  if (action === "READ") {
    return <BookOpen className={dark ? "text-white" : ""} />;
  }

  if (action === "DELETE") {
    return <Trash className={dark ? "text-white" : ""} />;
  }

  if (action === "UPDATE") {
    return <Edit className={dark ? "text-white" : ""} />;
  }

  if (action === "REPORT") {
    return <File className={dark ? "text-white" : ""} />;
  }

  return <Activity className={dark ? "text-white" : ""} />;
}
