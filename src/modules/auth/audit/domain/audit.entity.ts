import { IUserEntity } from "@modules/auth/users/dtos/user.entity";

export interface AuditEntity {
  id: string;
  name: string;
  action: string;
  method: string;
  payload?: any;
  dateEvent: string;
  userId: number;
  systemId: number;

  user: IUserEntity;
}
