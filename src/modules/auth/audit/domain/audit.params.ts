export interface AuditFindParams {
  id?: string;
  userId?: number;
}

export interface AuditListParams extends AuditFindParams {}

export interface AuditPaginateParams extends AuditListParams {
  page: number;
  limit: number;
}
