import { PermissionCondition } from "@modules/auth/casls/ability.factory";
import { IObjectEntity } from "@modules/auth/objects/dtos/object.entity";
import { PermissionAction } from "./permission.action";

export interface IPermissionEntity {
  id: number;
  systemId: number;
  action: PermissionAction;
  objectId: number;
  condition?: PermissionCondition;
  object?: IObjectEntity;
}

export const parseCondition = (
  condition: PermissionCondition,
  variables: Record<string, any>
) => {
  if (!condition) return null;
  const parsedCondition: any = {};
  // iterar condition
  if (typeof condition == "object") {
    for (const [key, rawValue] of Object.entries(condition)) {
      // validar object
      if (rawValue != null && typeof rawValue == "object") {
        const value = parseCondition(rawValue, variables);
        parsedCondition[key] = value;
        continue;
      } else {
        parsedCondition[key] = rawValue;
      }
      // find placeholder
      const matches = /^\$\{([a-zA-Z0-9]+)}$/.exec(rawValue);
      if (!matches) {
        parsedCondition[key] = rawValue;
        continue;
      }
      // remplazar
      const value = variables[matches[1]];
      if (typeof value == "undefined") {
        break;
      }
      // save
      parsedCondition[key] = value;
    }
  }
  // response
  return parsedCondition;
};
