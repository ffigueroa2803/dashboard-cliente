export enum PermissionAction {
  MANAGE = "manage",
  CREATE = "create",
  READ = "read",
  UPDATE = "update",
  DELETE = "delete",
  AGGREGATE = "aggregate",
  DISAGGREGATE = "disaggregate",
  UPLOAD = "upload",
  DOWNLOAD = "download",
  REPORT = "report",
}
