import { IPermissionEntity } from "@modules/auth/permissions/dtos/permission.entity";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { PaginateDto } from "@services/dtos";
import { PaginateEntity } from "@services/entities/paginate.entity";

const baseUrl = process.env.NEXT_PUBLIC_AUTH_URL || "";

export const permissionRtk = createApi({
  reducerPath: "permissionRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    getPermissions: builder.query<
      PaginateEntity<IPermissionEntity>,
      PaginateDto
    >({
      query: (params) => ({
        url: `permissions`,
        method: "GET",
        headers: BaseHeaders,
        params,
      }),
    }),
  }),
});

export const { useLazyGetPermissionsQuery } = permissionRtk;
