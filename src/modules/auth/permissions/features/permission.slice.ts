import {
  PaginateEntity,
  paginateInitial,
} from "@services/entities/paginate.entity";
import { IPermissionEntity } from "../dtos/permission.entity";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

export interface IPermissionType {
  permissions: PaginateEntity<IPermissionEntity>;
  permissionSelected: IPermissionEntity | null;
}

export const initialState: IPermissionType = {
  permissions: paginateInitial,
  permissionSelected: null,
};

export const permissionSlice = createSlice({
  name: "auth@permission",
  initialState,
  reducers: {
    setPermission: (
      state,
      { payload }: PayloadAction<IPermissionEntity | null>
    ) => {
      state.permissionSelected = payload;
    },
    paginate: (
      state,
      { payload }: PayloadAction<PaginateEntity<IPermissionEntity>>
    ) => {
      state.permissions = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.permission };
    },
  },
});

export const permissionReducer = permissionSlice.reducer;

export const permissionActions = permissionSlice.actions;
