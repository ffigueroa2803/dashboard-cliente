/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { IPermissionEntity } from "../dtos/permission.entity";
import { useLazyGetPermissionsQuery } from "../features/permission.rtk";
import { useDispatch, useSelector } from "react-redux";
import { permissionActions } from "../features/permission.slice";
import { RootState } from "@store/store";

interface IProps {
  name: string;
  defaultQuerySearch?: string;
}

export const PermissionSelect = ({ name }: IProps) => {
  const dispatch = useDispatch();
  const [fetch, { isLoading, isFetching, data }] = useLazyGetPermissionsQuery();

  const { permissionSelected } = useSelector(
    (state: RootState) => state.permission
  );

  const settingsData = (row: IPermissionEntity) => {
    return {
      label: `${row.object?.name} - ${row.action}`,
      value: row.id,
      obj: row,
    };
  };

  const handleOnchange = (option: any) => {
    if (option) {
      dispatch(permissionActions.setPermission(option.obj));
    } else {
      dispatch(permissionActions.setPermission(null));
    }
  };

  useEffect(() => {
    fetch({ limit: 1000, page: 1 });
  }, []);

  return (
    <SelectBasic
      name={name}
      value={permissionSelected?.id}
      options={data?.items?.map((row) => settingsData(row)) || []}
      onChange={handleOnchange}
      isLoading={isLoading || isFetching}
    />
  );
};
