/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import dynamic from "next/dynamic";
import { RootState } from "@store/store";
import { useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Trash } from "react-feather";
import { useSelector } from "react-redux";
import { IPermissionEntity } from "../dtos/permission.entity";

const ReactJson = dynamic(() => import("react-json-view"), { ssr: false });

interface IProps {
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const PermissionTable = ({
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const { permissions } = useSelector((state: RootState) => state.permission);

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      grow: true,
      cell: (row: IPermissionEntity) => (
        <>
          <Trash className="icon cursor-pointer" />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,
        cell: (row: IPermissionEntity) => row.id,
      },
      {
        name: "Objecto",
        cell: (row: IPermissionEntity) => (
          <span>{row?.object?.name || ""}</span>
        ),
      },
      {
        name: "Acción",
        grow: true,
        center: true,
        cell: (row: IPermissionEntity) => <span>{row.action}</span>,
      },
      {
        name: "Condicion",
        cell: (row: IPermissionEntity) => {
          try {
            const conditionJSON = JSON.parse((row.condition as any) || "{}");
            return (
              <span>
                <ReactJson src={conditionJSON} name={false} />
              </span>
            );
          } catch (error) {
            return null;
          }
        },
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [permissions]);

  return (
    <>
      <Datatable
        data={permissions.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={permissions?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={permissions?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
