import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IDependencyEntity } from "./dtos/dependency.entity";

export interface DependenciesState {
  dependencies: ResponsePaginateDto<IDependencyEntity>;
}

const initialState: DependenciesState = {
  dependencies: {
    meta: {
      totalItems: 0,
      itemsPerPage: 30,
      totalPages: 0,
    },
    items: [],
  },
};

export const dependencieSlice = createSlice({
  name: "auth@dependencies",
  initialState,
  reducers: {
    paginate: (
      state: DependenciesState,
      action: PayloadAction<ResponsePaginateDto<IDependencyEntity>>
    ) => {
      state.dependencies = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.dependency };
    },
  },
});

export const dependencyReducer = dependencieSlice.reducer;

export const dependencyActions = dependencieSlice.actions;
