/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { IDependencyEntity } from "../dtos/dependency.entity";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { useDependencyList } from "../hooks/use-depedency-list";
import { SelectBasic } from "@common/select/select-basic";

interface IProps {
  onChange: (option: any) => void;
  onBlur?: (e: any) => void;
  value: any;
  name: string;
  defaultQuerySearch?: string;
}

export const DependencySelect = ({
  name,
  value,
  defaultQuerySearch,
  onBlur,
  onChange,
}: IProps) => {
  const { dependencies } = useSelector((state: RootState) => state.dependency);
  const dependencyList = useDependencyList({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
  });

  const settingsData = (row: IDependencyEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    dependencyList.fetch();
  }, [dependencyList.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={dependencies?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={dependencyList.setQuerySearch}
      onBlur={onBlur}
    />
  );
};
