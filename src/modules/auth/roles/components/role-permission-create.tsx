/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { PermissionSelect } from "@modules/auth/permissions/components/permission-select";
import { RootState } from "@store/store";
import { Plus } from "react-feather";
import { useSelector } from "react-redux";
import { Button } from "reactstrap";
import dynamic from "next/dynamic";
import { useCreatePermissionMutation } from "../features/role.rtk";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { toast } from "react-toastify";
import { IPermissionEntity } from "@modules/auth/permissions/dtos/permission.entity";

const ReactJson = dynamic(() => import("react-json-view"), { ssr: false });

export interface RolePermissionCreateProps {
  onSave: (data: IPermissionEntity) => void;
}

export function RolePermissionCreate({ onSave }: RolePermissionCreateProps) {
  const { role } = useSelector((state: RootState) => state.role);

  const { permissionSelected } = useSelector(
    (state: RootState) => state.permission
  );

  const [fetch, { isLoading }] = useCreatePermissionMutation();

  const renderJSON = () => {
    try {
      const conditionJSON = JSON.parse(
        (permissionSelected?.condition as any) || "{}"
      );
      return (
        <span>
          <ReactJson src={conditionJSON} name={false} />
        </span>
      );
    } catch (error) {
      return null;
    }
  };

  const handleSave = () => {
    fetch({ id: role.id, permissionId: permissionSelected?.id || 0 })
      .unwrap()
      .then((data) => {
        toast.success(`El permiso se asignó correctamente!`);
        onSave(data);
      })
      .catch(() => toast.error(`No se pudo asignar el permiso`));
  };

  return (
    <div className="row">
      <div className="col-12 col-md-10">
        <PermissionSelect name="permissionId" />
      </div>
      <div className="col-12 col-md-2">
        <Button color="primary" onClick={handleSave} disabled={isLoading}>
          <Plus />
        </Button>
      </div>

      <Show
        condition={!isLoading}
        isDefault={
          <div className="col-12 mt-4 text-center">
            <LoadingSimple loading />
          </div>
        }>
        <Show condition={typeof permissionSelected?.id === "number"}>
          <div className="col-12">{renderJSON()}</div>
        </Show>
      </Show>
    </div>
  );
}
