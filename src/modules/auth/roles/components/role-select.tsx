/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useRoleList } from "../hooks/use-role-list";
import { IRoleEntity } from "../dtos/role.entity";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const RoleSelect = ({ name, value, onChange }: IProps) => {
  const { roles } = useSelector((state: RootState) => state.role);

  const roleList = useRoleList();

  const [isFetch, setIsFetch] = useState<boolean>(true);

  const settingsData = (row: IRoleEntity) => {
    return {
      label: `${row.name} ${row.isMaster ? "[Master]" : ""} ${
        row.isDefault ? "[Predeterminado]" : ""
      }`.toLocaleLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    if (isFetch) roleList.fetch();
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={roles?.items?.map((row) => settingsData(row))}
      onChange={onChange}
    />
  );
};
