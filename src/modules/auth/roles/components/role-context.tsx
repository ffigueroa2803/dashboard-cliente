/* eslint-disable no-unused-vars */
import { createContext, FC, useState } from "react";

export enum roleContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  SHIELD = "SHIELD",
}

export const RoleContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: roleContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const RoleProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<roleContextEnum>(
    roleContextEnum.DEFAULT
  );

  return (
    <RoleContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}>
      {children}
    </RoleContext.Provider>
  );
};
