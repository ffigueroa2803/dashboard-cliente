/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRoleEdit } from "../hooks/use-role-edit.hook";
import { FormGroup, Modal, ModalBody, ModalHeader } from "reactstrap";
import { useLazyGetPermissionsQuery } from "../features/role.rtk";
import { PermissionTable } from "@modules/auth/permissions/components/permission-table";
import { permissionActions } from "@modules/auth/permissions/features/permission.slice";
import { paginateInitial } from "@services/entities/paginate.entity";
import { RolePermissionCreate } from "./role-permission-create";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const RolePermission = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();
  const { role } = useSelector((state: RootState) => state.role);

  const roleEdit = useRoleEdit();

  const [fetch, { isLoading, isFetching }] = useLazyGetPermissionsQuery();

  const handleFetch = () => {
    fetch(role.id)
      .unwrap()
      .then((data) => dispatch(permissionActions.paginate(data)))
      .catch(() => dispatch(permissionActions.paginate(paginateInitial)));
  };

  useEffect(() => {
    if (!isOpen) roleEdit.setForm(role);
  }, [isOpen]);

  useEffect(() => {
    if (isOpen) handleFetch();
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen} size="xl">
      <ModalHeader toggle={onClose}>Asignar Permisos</ModalHeader>
      <ModalBody>
        <div>
          <FormGroup>
            <label>Nombre</label>
            <h6>{role.name || "N/A"}</h6>
          </FormGroup>
          <FormGroup>
            <label>Predeterminado</label>
            <h6>{role.isDefault ? "SI" : "NO"}</h6>
          </FormGroup>
          <FormGroup>
            <label>Tipo</label>
            <h6>{role.isMaster ? "MASTER" : "NORMAL"}</h6>
          </FormGroup>
          <div className="mt-5">
            <RolePermissionCreate onSave={() => handleFetch()} />
          </div>
          <div className="mt-5">
            <PermissionTable loading={isLoading || isFetching} />
          </div>
        </div>
      </ModalBody>
    </Modal>
  );
};
