/* eslint-disable no-unused-vars */
import { InputBasic } from "@common/inputs/input-basic";
import Toggle from "@atlaskit/toggle";
import { Form, FormGroup } from "reactstrap";

interface IProps {
  formik: any;
  form: any;
  handleBlur: (e: any) => void;
  handleChange: (e: any) => void;
  handleSubmit: () => void;
}

export const RoleForm = ({
  formik,
  form,
  handleBlur,
  handleChange,
  handleSubmit,
}: IProps) => {
  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <FormGroup>
        <InputBasic
          title="Nombre"
          type="text"
          name="name"
          formik={formik}
          value={form.name}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <InputBasic
          title="Descripción"
          type="textarea"
          name="description"
          formik={formik}
          value={form.description}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <label>Master</label>
        <div>
          <Toggle
            name="isMaster"
            isChecked={form.isMaster}
            onChange={handleChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <label>Predeterminado</label>
        <div>
          <Toggle
            name="isDefault"
            isChecked={form.isDefault}
            onChange={handleChange}
          />
        </div>
      </FormGroup>
    </Form>
  );
};
