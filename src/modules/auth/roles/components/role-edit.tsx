/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useRoleEdit } from "../hooks/use-role-edit.hook";
import { RoleForm } from "./role-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const RoleEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { role } = useSelector((state: RootState) => state.role);

  const roleEdit = useRoleEdit();

  useEffect(() => {
    if (roleEdit.isUpdated) onSave();
  }, [roleEdit.isUpdated]);

  useEffect(() => {
    if (!isOpen) roleEdit.setForm(role);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Rol</ModalHeader>
      <ModalBody>
        <RoleForm
          form={roleEdit.form}
          formik={roleEdit.formik}
          handleChange={roleEdit.handleChange}
          handleBlur={roleEdit.handleBlur}
          handleSubmit={roleEdit.handleSubmit}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          onClick={() => roleEdit.handleSubmit()}
          disabled={roleEdit.pending}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
