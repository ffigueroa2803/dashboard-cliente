/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit, Shield } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IRoleEntity } from "../dtos/role.entity";
import { roleActions } from "../store";
import { roleContextEnum, RoleContext } from "./role-context";

interface IProps {
  onClick?: (role: IRoleEntity, action: roleContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const RoleTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { roles } = useSelector((state: RootState) => state.role);

  const roleContext = useContext(RoleContext);

  const handleClick = (role: IRoleEntity, action: roleContextEnum) => {
    dispatch(roleActions.setRole(role));
    roleContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(role, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      grow: true,
      cell: (row: IRoleEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, roleContextEnum.EDIT)}
          />
          <Shield
            className="icon cursor-pointer"
            onClick={() => handleClick(row, roleContextEnum.SHIELD)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,

        cell: (row: IRoleEntity) => row.id,
      },
      {
        name: "Nombre",
        wrap: true,
        cell: (row: IRoleEntity) => (
          <span className="uppercase">{row?.name || ""}</span>
        ),
      },
      {
        name: "Predeterminado",
        wrap: true,
        center: true,
        cell: (row: IRoleEntity) => (
          <span className="uppercase">{row?.isDefault ? "SI" : "NO"}</span>
        ),
      },
      {
        name: "Tipo",
        wrap: true,
        center: true,
        cell: (row: IRoleEntity) => (
          <span className="uppercase">
            {row?.isMaster ? "Master" : "Normal"}
          </span>
        ),
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [roles]);

  return (
    <>
      <Datatable
        data={roles.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={roles?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={roles?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
