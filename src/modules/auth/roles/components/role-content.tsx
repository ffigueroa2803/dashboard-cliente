/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import { RoleContext, RoleProvider, roleContextEnum } from "./role-context";
import { useRoleList } from "../hooks/use-role-list";
import { FloatButton } from "@common/button/float-button";
import { RoleTable } from "./role-table";
import { RoleCreate } from "./role-create";
import { RoleEdit } from "./role-edit";
import { RolePermission } from "./role-permission";

const RoleWrapper = () => {
  const roleContext = useContext(RoleContext);

  const roleList = useRoleList({
    page: 1,
    limit: 30,
    querySearch: roleContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    roleList.setPage(page);
    roleContext.setClearRows(true);
    roleContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    roleList.setLimit(limit);
    roleContext.setClearRows(true);
    roleContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = () => {
    roleContext.setOptions(roleContextEnum.DEFAULT);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    roleList.setFilter({
      querySearch: roleContext.querySearch,
    });
  }, [roleContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      roleContext.setClearRows(true);
      roleContext.setIds([]);
      roleList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}>
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={roleContext.querySearch}
                  disabled={roleList.pending}
                  onChange={({ target }) =>
                    roleContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <RoleTable
            loading={roleList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => roleContext.setOptions(roleContextEnum.CREATE)}
      />
      {/* crear */}
      <RoleCreate
        isOpen={roleContext.options == roleContextEnum.CREATE}
        onClose={() => roleContext.setOptions(roleContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <RoleEdit
        isOpen={roleContext.options == roleContextEnum.EDIT}
        onClose={() => roleContext.setOptions(roleContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* permission */}
      <RolePermission
        isOpen={roleContext.options == roleContextEnum.SHIELD}
        onClose={() => roleContext.setOptions(roleContextEnum.DEFAULT)}
      />
    </>
  );
};

export const RoleContent = () => {
  return (
    <RoleProvider>
      <RoleWrapper />
    </RoleProvider>
  );
};
