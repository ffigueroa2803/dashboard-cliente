export interface IRoleEntity {
  id: number;
  name: string;
  description: string;
  isMaster: boolean;
  isDefault: boolean;
}

export const roleEntityName = "RoleEntityName";
