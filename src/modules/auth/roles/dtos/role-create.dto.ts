import { Yup } from "@common/yup";

export const roleCreateValidate = Yup.object().shape({
  name: Yup.string().required().min(2).max(50),
  description: Yup.string().required().min(2).max(255),
  isMaster: Yup.boolean(),
  isDefault: Yup.boolean(),
});

export interface IRoleCreateDto {
  name: string;
  description: string;
  isMaster: boolean;
  isDefault: boolean;
}
