/* eslint-disable react-hooks/exhaustive-deps */
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { roleCreateValidate } from "../dtos/role-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { roleActions } from "../store";

const { request } = AuthRequest();

export const useRoleEdit = () => {
  const dispatch = useDispatch();

  const { role } = useSelector((state: RootState) => state.role);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (values: any): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`roles/${role.id}`, values)
      .then((res) => {
        setIsUpdated(true);
        toast.success(`El rol se actualizó correctamente!`);
        dispatch(roleActions.setRole(res.data));
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: role,
    validationSchema: roleCreateValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (role?.id) formik.setValues(role);
  }, [role]);

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
