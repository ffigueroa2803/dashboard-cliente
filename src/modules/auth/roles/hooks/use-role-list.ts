import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch } from "react-redux";
import { initialState, roleActions } from "../store";
import { PaginateDto } from "@services/dtos";

const { request } = AuthRequest();

export const useRoleList = (paginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(paginate?.page || 1);
  const [limit, setLimit] = useState<number>(paginate?.limit || 30);
  const [filter, setFilter] = useState<any>({
    querySearch: paginate?.querySearch || "",
  });

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("limit", `${limit || 30}`);
    await request
      .get(`roles`, { params })
      .then((res) => dispatch(roleActions.paginate(res.data)))
      .catch(() => dispatch(roleActions.paginate(initialState.roles)));
    setPending(false);
  };

  return {
    pending,
    page,
    setPage,
    limit,
    setLimit,
    filter,
    setFilter,
    fetch,
  };
};
