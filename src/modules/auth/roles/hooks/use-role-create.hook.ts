import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { roleCreateValidate } from "../dtos/role-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";

const { request } = AuthRequest();

export const useRoleCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isCreated, setIsCreated] = useState<boolean>(false);

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsCreated(false);
    await request
      .post(`roles`, values)
      .then(() => {
        resetForm();
        setIsCreated(true);
        toast.success(`El rol se guardó correctamente!`);
      })
      .catch((err) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      isDefault: false,
      isMaster: false,
    },
    validationSchema: roleCreateValidate,
    onSubmit: save,
  });

  return {
    pending,
    isCreated,
    form: formik.values,
    formik: formik,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
