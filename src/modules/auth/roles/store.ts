import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IRoleEntity } from "./dtos/role.entity";

export interface IRoleState {
  roles: ResponsePaginateDto<IRoleEntity>;
  role: IRoleEntity;
}

export const initialState: IRoleState = {
  roles: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  role: {} as any,
};

export const roleSlice = createSlice({
  name: "auth@role",
  initialState,
  reducers: {
    setRole: (state: IRoleState, { payload }: PayloadAction<IRoleEntity>) => {
      state.role = payload;
      return state;
    },
    paginate: (
      state: IRoleState,
      { payload }: PayloadAction<ResponsePaginateDto<IRoleEntity>>
    ) => {
      state.roles = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.role };
    },
  },
});

export const roleReducer = roleSlice.reducer;

export const roleActions = roleSlice.actions;
