import { IPermissionEntity } from "@modules/auth/permissions/dtos/permission.entity";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { PaginateEntity } from "@services/entities/paginate.entity";

const baseUrl = process.env.NEXT_PUBLIC_AUTH_URL || "";

export const roleRtk = createApi({
  reducerPath: "roleRtk",
  baseQuery: fetchBaseQuery({ baseUrl: baseUrl }),
  endpoints: (builder) => ({
    getPermissions: builder.query<PaginateEntity<IPermissionEntity>, number>({
      query: (id) => ({
        url: `/roles/${id}/permissions`,
        method: "GET",
        headers: BaseHeaders,
      }),
    }),
    createPermission: builder.mutation<
      IPermissionEntity,
      { id: number; permissionId: number }
    >({
      query: ({ id, permissionId }) => ({
        url: `/roles/${id}/permissions`,
        method: "POST",
        headers: BaseHeaders,
        body: { permissionId },
      }),
    }),
  }),
});

export const { useLazyGetPermissionsQuery, useCreatePermissionMutation } =
  roleRtk;
