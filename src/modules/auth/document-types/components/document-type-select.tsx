/* eslint-disable no-unused-vars */
import { SelectRemote} from "../../../../common/select/select-remote";
import { DocumentTypeEntity } from "../domain/document-type.entity";
import { useInstitutionPaginate } from "../hooks/use-document-type-paginate";
import React from "react";

interface IProps {
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const DocumentTypeSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
}: IProps) => {
  const hook = useInstitutionPaginate({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
  });

  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={hook.handle}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: DocumentTypeEntity) =>
          `${row.code} - ${row.name}`.toUpperCase(),
        value: (row: DocumentTypeEntity) => row.code,
      }}
    />
  );
};
