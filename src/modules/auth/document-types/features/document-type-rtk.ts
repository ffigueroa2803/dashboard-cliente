import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { DocumentTypeEntity } from "../domain/document-type.entity";
import { DocumentTypePaginateParams } from "../domain/document-type.params";

const baseUrl = process.env.NEXT_PUBLIC_IDENTITY_URL || "";

export const documentTypeRtk = createApi({
  reducerPath: "documentTypeRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    paginateDocumentTypes: builder.query<PaginateEntity<DocumentTypeEntity>, DocumentTypePaginateParams>({
      query: (params) => ({
        url: 'documentTypes',
        params
      })
    })
  }),
});

export const { useLazyPaginateDocumentTypesQuery } = documentTypeRtk;
