import { useState } from "react";
import { toast } from "react-toastify";
import { DocumentTypePaginateParams } from "../domain/document-type.params";
import { useLazyPaginateDocumentTypesQuery } from "../features/document-type-rtk";

export function useInstitutionPaginate(defaultFilters?: DocumentTypePaginateParams) {
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateDocumentTypesQuery();
  const [filters, setFilters] = useState<DocumentTypePaginateParams>(
    Object.assign({ page: 1, limit: 100 }, defaultFilters || {})
  )

  const handle = async () => {
    return fetch(filters)
      .unwrap()
      .catch((err: any) => {
        toast.error(err.data?.message || "Algo salío mal")
      })
  }

  return {
    isFetching,
    isLoading,
    handle,
    filters,
    setFilters,
    data
  }
}