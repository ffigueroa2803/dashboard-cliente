export interface DocumentTypeEntity {
  code: string;
  name: string;
}