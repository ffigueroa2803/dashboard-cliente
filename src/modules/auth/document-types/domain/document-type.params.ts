export interface DocumentTypeFindParams {
  querySearch?: string;
}

export interface DocumentTypeListParams extends DocumentTypeFindParams {

}

export interface DocumentTypePaginateParams extends DocumentTypeListParams {
  page: number;
  limit: number;
}