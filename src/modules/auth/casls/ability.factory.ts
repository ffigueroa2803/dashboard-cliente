import { IUserEntity } from "../users/dtos/user.entity";
import { Ability } from "@casl/ability";
import { PermissionAction } from "../permissions/dtos/permission.action";
import {
  IPermissionEntity,
  parseCondition,
} from "../permissions/dtos/permission.entity";

export type PermissionObjectType = any;
export type AppAbility = Ability<[PermissionAction, PermissionObjectType]>;
export interface PermissionCondition {}

export interface CaslPermission {
  action: PermissionAction;
  subject: string;
  condition?: PermissionCondition;
}

export const AbilityFactory = (
  user: IUserEntity,
  permissions: IPermissionEntity[]
) => {
  // role master
  if (user.role?.isMaster) {
    return new Ability<[PermissionAction, PermissionObjectType]>(
      [{ action: PermissionAction.MANAGE, subject: "all" }],
      { detectSubjectType: (object) => object.__typename }
    );
  }
  // role común
  const caslPermissions: CaslPermission[] = permissions.map((p) => ({
    action: p.action,
    subject: p?.object?.name || "object",
    conditions: parseCondition(p.condition || "object", user),
  }));
  // response
  return new Ability<[PermissionAction, PermissionObjectType]>(
    caslPermissions,
    { detectSubjectType: (object) => object.__typename }
  );
};
