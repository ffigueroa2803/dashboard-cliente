import { createContext, FC } from "react";
import { createContextualCan } from "@casl/react";
import { AbilityFactory, AppAbility } from "./ability.factory";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

export const CaslContext = createContext<AppAbility>({} as any);
export const Can = createContextualCan(CaslContext.Consumer);

export const CaslProvider: FC = ({ children }) => {
  const { user, permissions } = useSelector((state: RootState) => state.auth);

  const ability = AbilityFactory(user || ({} as any), permissions);

  return (
    <CaslContext.Provider value={ability}>{children}</CaslContext.Provider>
  );
};
