import { CaslContext } from "@modules/auth/casls/casl.context";
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit, Sliders } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IBusinessEntity } from "../dtos/business.entity";
import { businessActions } from "../store";
import { BusinessContext, businessContextEnum } from "./business-context";

interface IProps {
  onClick?: (business: IBusinessEntity, action: businessContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const BusinessTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { businesses } = useSelector((state: RootState) => state.business);

  const businessContext = useContext(BusinessContext);
  const ability = useContext(CaslContext);

  const handleClick = (
    client: IBusinessEntity,
    action: businessContextEnum
  ) => {
    dispatch(businessActions.setBusiness(client));
    businessContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(client, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      cell: (row: IBusinessEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, businessContextEnum.EDIT)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,
        cell: (row: IBusinessEntity) => row.id,
      },
      {
        name: "Slug",
        wrap: true,
        cell: (row: IBusinessEntity) => (
          <span className="uppercase">{row.slug || "N/A"}</span>
        ),
      },
      {
        name: "Nombre",
        wrap: true,
        cell: (row: IBusinessEntity) => (
          <span className="uppercase">{row.name || "N/A"}</span>
        ),
      },
      {
        name: "Tipo Documento",
        wrap: true,
        cell: (row: IBusinessEntity) => (
          <span className="uppercase">{row.documentType?.name || "N/A"}</span>
        ),
      },
      {
        name: "N° Documento",
        wrap: true,
        cell: (row: IBusinessEntity) => (
          <span className="uppercase">{row.documentNumber || "N/A"}</span>
        ),
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [businesses]);

  return (
    <>
      <Datatable
        data={businesses.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={businesses?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={businesses?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
