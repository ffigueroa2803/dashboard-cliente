import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useBusinessEdit } from "../hooks/use-business-edit";
import { BusinessChangeLogo } from "./business-change-logo";
import { BusinessForm } from "./business-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const BusinessEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { business } = useSelector((state: RootState) => state.business);

  const businessEdit = useBusinessEdit();

  useEffect(() => {
    if (businessEdit.isUpdated) onSave();
  }, [businessEdit.isUpdated]);

  useEffect(() => {
    if (!isOpen) businessEdit.setForm(business);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Empresa</ModalHeader>
      <Form onSubmit={businessEdit.handleSubmit}>
        <ModalBody>
          <div className="text-center mb-4">
            <BusinessChangeLogo />
          </div>
          <BusinessForm
            form={businessEdit.form}
            formik={businessEdit.formik}
            handleChange={businessEdit.handleChange}
            handleBlur={businessEdit.handleBlur}
            handleSubmit={businessEdit.handleSubmit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            disabled={businessEdit.pending || !businessEdit.isValid}
          >
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
