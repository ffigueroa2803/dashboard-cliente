import { createContext, FC, useState } from "react";

export enum businessContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  CONFIG = "CONFIG",
}

export const BusinessContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: businessContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const BusinessProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<businessContextEnum>(
    businessContextEnum.DEFAULT
  );

  return (
    <BusinessContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}
    >
      {children}
    </BusinessContext.Provider>
  );
};
