import { InputBasic, InputWrapper } from "@common/inputs/input-basic";
import { SelectBasic } from "@common/select/select-basic";
import ObjectID from "bson-objectid";
import { FormikProps } from "formik";
import { FormGroup } from "reactstrap";

interface IProps {
  formik: FormikProps<any>;
  form: any;
  handleBlur: (e: any) => void;
  handleChange: (e: any) => void;
  handleSubmit: () => void;
}

export const BusinessForm = ({
  formik,
  form,
  handleBlur,
  handleChange,
}: IProps) => {
  const generateSecret = () => {
    formik.setFieldValue("slug", new ObjectID().toHexString().substring(0, 10));
  };

  return (
    <>
      <FormGroup className="mb-3">
        <InputBasic
          title="Slug"
          type="text"
          name="slug"
          formik={formik}
          value={form.slug}
          onBlur={handleBlur}
          onChange={handleChange}
        />
        <div className="text-right">
          <b className="text-primary cursor-pointer" onClick={generateSecret}>
            <u>generar slug</u>
          </b>
        </div>
      </FormGroup>

      <FormGroup className="mb-3">
        <InputBasic
          title="Nombre"
          type="text"
          name="name"
          formik={formik}
          value={form.name}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <InputWrapper
        name="documentTypeId"
        title="Tipo Documento"
        formik={formik}
      >
        <SelectBasic
          options={[
            { label: "DNI/LE", value: "01" },
            { label: "Pasaporte", value: "02" },
            { label: "Cédula", value: "03" },
            { label: "RUC", value: "04" },
          ]}
          name="documentTypeId"
          value={form.documentTypeId}
          onChange={(opt) => handleChange({ target: opt })}
        />
      </InputWrapper>

      <FormGroup className="mt-3">
        <InputBasic
          title="N° Documento"
          type="text"
          name="documentNumber"
          formik={formik}
          value={form.documentNumber}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>
    </>
  );
};
