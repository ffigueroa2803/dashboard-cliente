import React, { useEffect, useState } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useBusinessList } from "../hooks/use-business-list";
import { IBusinessEntity } from "../dtos/business.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const BusinessSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { businesses } = useSelector((state: RootState) => state.business);

  const businessList = useBusinessList({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch || "",
  });

  const [isFetch, setIsFetch] = useState<boolean>(true);

  const settingsData = (row: IBusinessEntity) => {
    return {
      label: `${row.name}`.toLocaleLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    if (isFetch) businessList.fetch();
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={businesses?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={(value) => {
        if (!value) return;
        businessList.changeFilter({ name: "querySearch", value });
        setIsFetch(true);
      }}
    />
  );
};
