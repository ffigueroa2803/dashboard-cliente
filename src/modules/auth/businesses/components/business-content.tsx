import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import { FloatButton } from "@common/button/float-button";
import {
  BusinessContext,
  businessContextEnum,
  BusinessProvider,
} from "./business-context";
import { useBusinessList } from "../hooks/use-business-list";
import { BusinessTable } from "./business-table";
import { BusinessCreate } from "./business-create";
import { BusinessEdit } from "./business-edit";

const BusinessWrapper = () => {
  const businessContext = useContext(BusinessContext);

  const clientList = useBusinessList({
    page: 1,
    limit: 30,
    querySearch: businessContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    clientList.changeFilter({ name: "page", value: page });
    businessContext.setClearRows(true);
    businessContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    clientList.changeFilter({ name: "limit", value: limit });
    businessContext.setClearRows(true);
    businessContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = () => {
    businessContext.setOptions(businessContextEnum.DEFAULT);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    clientList.changeFilter({
      name: "querySearch",
      value: businessContext.querySearch,
    });
  }, [businessContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      businessContext.setClearRows(true);
      businessContext.setIds([]);
      clientList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={businessContext.querySearch}
                  disabled={clientList.pending}
                  onChange={({ target }) =>
                    businessContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <BusinessTable
            loading={clientList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => businessContext.setOptions(businessContextEnum.CREATE)}
      />
      {/* crear */}
      <BusinessCreate
        isOpen={businessContext.options == businessContextEnum.CREATE}
        onClose={() => businessContext.setOptions(businessContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <BusinessEdit
        isOpen={businessContext.options == businessContextEnum.EDIT}
        onClose={() => businessContext.setOptions(businessContextEnum.DEFAULT)}
        onSave={handleSave}
      />
    </>
  );
};

export const BusinessContent = () => {
  return (
    <BusinessProvider>
      <BusinessWrapper />
    </BusinessProvider>
  );
};
