import ObjectID from "bson-objectid";
import { useEffect } from "react";
import { Save } from "react-feather";
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useBusinessCreate } from "../hooks/use-business-create";
import { BusinessForm } from "./business-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const BusinessCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const businessCreate = useBusinessCreate();

  const handleForm = (e: any) => {
    e.preventDefault();
    businessCreate.handleSubmit();
  };

  useEffect(() => {
    if (isOpen) {
      businessCreate.formik.setFieldValue(
        "clientSecret",
        new ObjectID().toHexString()
      );
      // clear errors
      businessCreate.formik.setErrors({});
      businessCreate.formik.setTouched({});
    }
  }, [isOpen]);

  useEffect(() => {
    if (businessCreate.isCreated) onSave();
  }, [businessCreate.isCreated]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Empresa</ModalHeader>
      <Form onSubmit={handleForm}>
        <ModalBody>
          <BusinessForm
            form={businessCreate.form}
            formik={businessCreate.formik}
            handleChange={businessCreate.handleChange}
            handleBlur={businessCreate.handleBlur}
            handleSubmit={businessCreate.handleSubmit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            disabled={businessCreate.pending || !businessCreate.isValid}
          >
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
