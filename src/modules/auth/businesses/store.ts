import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IBusinessEntity } from "./dtos/business.entity";

export interface BusinessState {
  businesses: ResponsePaginateDto<IBusinessEntity>;
  business: IBusinessEntity;
}

export const initialState: BusinessState = {
  businesses: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  business: {} as any,
};

export const businessSlice = createSlice({
  name: "auth@businesses",
  initialState,
  reducers: {
    paginate: (
      state: BusinessState,
      { payload }: PayloadAction<ResponsePaginateDto<IBusinessEntity>>
    ) => {
      state.businesses = payload;
      return state;
    },
    setBusiness: (
      state: BusinessState,
      action: PayloadAction<IBusinessEntity>
    ) => {
      state.business = action.payload;
      return state;
    },
    changeLogo: (state: BusinessState, { payload }: PayloadAction<string>) => {
      state.business.logoUrl = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.business };
    },
  },
});

export const businessReducer = businessSlice.reducer;

export const businessActions = businessSlice.actions;
