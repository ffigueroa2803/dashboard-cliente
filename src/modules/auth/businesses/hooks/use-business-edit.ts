import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { businessActions } from "../store";
import { businessEditValidate } from "../dtos/business-edit.dto";

const { request } = AuthRequest();

export const useBusinessEdit = () => {
  const dispatch = useDispatch();

  const { business } = useSelector((state: RootState) => state.business);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (values: any): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`businesses/${business.id}`, values)
      .then((res) => {
        setIsUpdated(true);
        toast.success(`Los datos se actualizaron correctamente!`);
        dispatch(businessActions.setBusiness(res.data));
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: business,
    validationSchema: businessEditValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (business?.id) formik.setValues(business);
  }, [business]);

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    isValid: formik.isValid,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
