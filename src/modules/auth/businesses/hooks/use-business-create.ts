import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import ObjectID from "bson-objectid";
import {
  businessCreateValidate,
  IBusinessCreateDto,
} from "../dtos/business-create.dto";

const { request } = AuthRequest();

export const useBusinessCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isCreated, setIsCreated] = useState<boolean>(false);

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsCreated(false);
    await request
      .post(`businesses`, values)
      .then(() => {
        resetForm();
        setIsCreated(true);
        toast.success(`La empresa se guardó correctamente!`);
      })
      .catch((err) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik<IBusinessCreateDto>({
    initialValues: {
      slug: new ObjectID().toHexString().substring(0, 10),
      name: "",
      documentNumber: "",
      documentTypeId: "",
    },
    validationSchema: businessCreateValidate,
    onSubmit: save,
  });

  return {
    pending,
    isCreated,
    form: formik.values,
    formik: formik,
    errors: formik.errors,
    isValid: formik.isValid,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
