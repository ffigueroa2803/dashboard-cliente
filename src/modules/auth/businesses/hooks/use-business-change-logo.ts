import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { businessActions } from "../store";

const { request } = AuthRequest();

export const useBusinessChangeLogo = () => {
  const dispatch = useDispatch();

  const { business } = useSelector((state: RootState) => state.business);

  const [image, setImage] = useState<Blob | string>("");
  const [pending, setPending] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    const form = new FormData();
    form.append("file", image);
    // send request
    await request
      .put(`businesses/${business.id}/changeLogo`, form)
      .then(({ data }) => {
        dispatch(businessActions.changeLogo(data.logoUrl));
        toast.success("Logo cambiado!!!");
      })
      .catch(() => toast.error("No se pudo cambiar el logo"));
    setPending(false);
  };

  return {
    pending,
    image,
    setImage,
    save,
  };
};
