import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch } from "react-redux";
import { PaginateDto } from "@services/dtos";
import { businessActions, initialState } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";

const { request } = AuthRequest();

export const useBusinessList = (paginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [filter, setFilter] = useState<PaginateDto>(
    paginate || {
      page: 1,
      limit: 30,
    }
  );

  const changeFilter = ({ name, value }: IInputHandle) => {
    setFilter((prev) => ({ ...prev, [name]: value }));
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${filter.page}`);
    params.set("limit", `${filter.limit || 30}`);
    await request
      .get(`businesses`, { params })
      .then((res) => dispatch(businessActions.paginate(res.data)))
      .catch(() => dispatch(businessActions.paginate(initialState.businesses)));
    setPending(false);
  };

  return {
    pending,
    filter,
    setFilter,
    changeFilter,
    fetch,
  };
};
