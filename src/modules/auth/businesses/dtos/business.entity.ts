import { IDocumentTypeEntity } from "@services/entities/document-type.entity";

export interface IBusinessEntity {
  id: number;
  slug: string;
  name: string;
  documentNumber: string;
  documentTypeId: string;
  documentType?: IDocumentTypeEntity;
  logoUrl: string;
}

export const businessEntityName = "BusinessEntity";
