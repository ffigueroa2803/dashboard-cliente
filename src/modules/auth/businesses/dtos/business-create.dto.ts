import { Yup } from "@common/yup";

export const businessCreateValidate = Yup.object().shape({
  slug: Yup.string().required().min(2).max(10),
  name: Yup.string().required().min(2).max(100),
  documentNumber: Yup.string().required().min(2).max(40),
  documentTypeId: Yup.number(),
});

export interface IBusinessCreateDto {
  slug: string;
  name: string;
  documentNumber: string;
  documentTypeId: string;
}
