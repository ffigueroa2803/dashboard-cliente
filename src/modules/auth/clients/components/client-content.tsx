import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import { FloatButton } from "@common/button/float-button";
import {
  ClientContext,
  clientContextEnum,
  ClientProvider,
} from "./client-context";
import { ClientTable } from "./client-table";
import { useClientList } from "../hooks/use-client-list";
import { ClientCreate } from "./client-create";
import { ClientEdit } from "./client-edit";
import { ClientConfig } from "./client-config";

const ClientWrapper = () => {
  const clientContext = useContext(ClientContext);

  const clientList = useClientList({
    page: 1,
    limit: 30,
    querySearch: clientContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    clientList.changeFilter({ name: "page", value: page });
    clientContext.setClearRows(true);
    clientContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    clientList.changeFilter({ name: "limit", value: limit });
    clientContext.setClearRows(true);
    clientContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = () => {
    clientContext.setOptions(clientContextEnum.DEFAULT);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    clientList.changeFilter({
      name: "querySearch",
      value: clientContext.querySearch,
    });
  }, [clientContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      clientContext.setClearRows(true);
      clientContext.setIds([]);
      clientList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={clientContext.querySearch}
                  disabled={clientList.pending}
                  onChange={({ target }) =>
                    clientContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <ClientTable
            loading={clientList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => clientContext.setOptions(clientContextEnum.CREATE)}
      />
      {/* crear */}
      <ClientCreate
        isOpen={clientContext.options == clientContextEnum.CREATE}
        onClose={() => clientContext.setOptions(clientContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <ClientEdit
        isOpen={clientContext.options == clientContextEnum.EDIT}
        onClose={() => clientContext.setOptions(clientContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* config */}
      <ClientConfig
        isOpen={clientContext.options == clientContextEnum.CONFIG}
        onClose={() => clientContext.setOptions(clientContextEnum.DEFAULT)}
        onSave={handleSave}
      />
    </>
  );
};

export const ClientContent = () => {
  return (
    <ClientProvider>
      <ClientWrapper />
    </ClientProvider>
  );
};
