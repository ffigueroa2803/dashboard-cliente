import { css } from "@emotion/react";
import React, { useEffect, useState } from "react";
import { Media } from "reactstrap";
import ProfileImage from "@assets/images/perfil.jpg";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useClientChangeLogo } from "../hooks/use-client-change-logo";
import { Show } from "@common/show";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export const ClientChangeLogo = () => {
  const { client } = useSelector((state: RootState) => state.client);

  const [isUpdate, setIsUpdate] = useState<boolean>(false);

  const clientChangeLogo = useClientChangeLogo();

  const handleFile = ({ files }: { files: FileList | null }) => {
    if (!files) return clientChangeLogo.setImage("");
    clientChangeLogo.setImage(files[0]);
    setIsUpdate(true);
  };

  useEffect(() => {
    if (isUpdate) {
      clientChangeLogo.save();
    }
  }, [isUpdate]);

  useEffect(() => {
    if (isUpdate) setIsUpdate(false);
  }, [isUpdate]);

  return (
    <div
      style={{
        display: "inline-block",
        width: "auto",
        position: "relative",
      }}
    >
      <Show
        condition={client.logoUrl != ""}
        isDefault={
          <div className="mb-1">
            <u className="text-primary cursor-pointer">
              {clientChangeLogo.pending ? "subiendo..." : "Cambiar de imagen"}
              <input
                style={{ width: "100%" }}
                disabled={clientChangeLogo.pending}
                onChange={({ target }) => handleFile(target)}
                className="upload cursor-pointer"
                type="file"
                accept="image/png,image/jpg,image/jpeg"
              />
            </u>
          </div>
        }
      >
        <div className="avatar">
          <Media
            body
            src={client?.logoUrl || ProfileImage.src}
            alt=""
            data-intro="This is Profile image"
            style={{
              objectFit: "cover",
              width: "100px",
              height: "100px",
              borderRadius: "50%",
              border: "0.5em solid white",
              boxShadow: "0px 0px 0px 1px black",
            }}
          />
        </div>
        <div
          className="icon-wrapper cursor-pointer"
          data-intro="Change Profile image here"
        >
          <i
            className="icofont icofont-pencil-alt-5 cursor-pointer"
            style={{
              position: "absolute",
              bottom: "5px",
              right: "40%",
            }}
          >
            <input
              id="change-sello"
              disabled={clientChangeLogo.pending}
              onChange={({ target }) => handleFile(target)}
              className="upload"
              type="file"
              accept="image/png,image/jpg,image/jpeg"
            />
          </i>
        </div>
      </Show>
    </div>
  );
};
