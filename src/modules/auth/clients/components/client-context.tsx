import { createContext, FC, useState } from "react";

export enum clientContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  CONFIG = "CONFIG",
}

export const ClientContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: clientContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const ClientProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<clientContextEnum>(
    clientContextEnum.DEFAULT
  );

  return (
    <ClientContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}
    >
      {children}
    </ClientContext.Provider>
  );
};
