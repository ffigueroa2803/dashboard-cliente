import { CaslContext } from "@modules/auth/casls/casl.context";
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit, Sliders } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IClientEntity } from "../dtos/client.entity";
import { clientActions } from "../store";
import { ClientContext, clientContextEnum } from "./client-context";

interface IProps {
  onClick?: (client: IClientEntity, action: clientContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const ClientTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { clients } = useSelector((state: RootState) => state.client);

  const clientContext = useContext(ClientContext);
  const ability = useContext(CaslContext);

  const handleClick = (client: IClientEntity, action: clientContextEnum) => {
    dispatch(clientActions.setClient(client));
    clientContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(client, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      cell: (row: IClientEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, clientContextEnum.EDIT)}
          />

          <Sliders
            className="icon cursor-pointer ml-2"
            onClick={() => handleClick(row, clientContextEnum.CONFIG)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,
        cell: (row: IClientEntity) => row.id,
      },
      {
        name: "Nombre",
        wrap: true,
        cell: (row: IClientEntity) => (
          <span className="uppercase">{row.name || "N/A"}</span>
        ),
      },
      {
        name: "Empresa",
        wrap: true,
        cell: (row: IClientEntity) => (
          <span className="uppercase">{row.business?.name || "N/A"}</span>
        ),
      },
      {
        name: "ClientId",
        wrap: true,
        center: true,
        cell: (row: IClientEntity) => row.clientId,
      },
      {
        name: "ClientSecret",
        wrap: true,
        center: true,
        cell: (row: IClientEntity) => row.clientSecret,
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clients]);

  return (
    <>
      <Datatable
        data={clients.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={clients?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={clients?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
