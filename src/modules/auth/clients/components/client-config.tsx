import { Dropzone } from "@common/dropzone";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Image, Trash, X } from "react-feather";
import { useSelector } from "react-redux";
import {
  Card,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import { useClientAddBackground } from "../hooks/use-client-add-background";
import { useClientFind } from "../hooks/use-client-find";
import { useClientRemoveBackground } from "../hooks/use-client-remove-background";
import { ClientChangeLogo } from "./client-change-logo";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const ClientConfig = ({ isOpen, onClose, onSave }: IProps) => {
  const { client } = useSelector((state: RootState) => state.client);

  const clientFind = useClientFind();
  const clientAddBackground = useClientAddBackground();
  const clientRemoveBackground = useClientRemoveBackground();

  const [addBackground, setAddBackground] = useState<boolean>(false);

  const handleAddBackground = (files: any[]) => {
    if (!files.length) return;
    clientAddBackground.setImage(files[0]);
    setAddBackground(true);
  };

  const handleRemoveBackground = (id: number) => {
    clientRemoveBackground
      .destroy(id)
      .then(() => clientFind.fetch(client.id))
      .catch(() => null);
  };

  useEffect(() => {
    if (addBackground) {
      clientAddBackground
        .save()
        .then(() => {
          clientAddBackground.setImage(undefined);
          clientFind.fetch(client.id);
        })
        .catch(() => null);
    }
  }, [addBackground]);

  useEffect(() => {
    if (isOpen) clientFind.fetch(client.id);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        <span className="badge badge-dark">{client.name || ""}</span>
        <span className="ml-2">Configurar Cliente</span>
      </ModalHeader>
      <Show
        condition={!clientFind.pending}
        isDefault={
          <div className="text-center">
            <LoadingSimple loading />
          </div>
        }
      >
        <ModalBody>
          <div className="text-center mb-4">
            <ClientChangeLogo />
          </div>

          <h6>
            <hr />
            <Image className="icon" /> Fondos{" "}
            <small>(máximo de 5 imagenes)</small>
            <hr />
          </h6>

          <Row className="mb-1">
            <Show
              condition={client.backgrounds?.length > 0}
              isDefault={
                <Col md="12 text-center">
                  <b>No hay imagenes disponibles</b>
                </Col>
              }
            >
              {client.backgrounds?.map((background, index) => (
                <Col md="4" key={`list-item-${index}`}>
                  <X
                    className="close icon cursor-pointer text-danger"
                    onClick={() => handleRemoveBackground(background.id)}
                  />
                  <img
                    src={`${background.url}`}
                    alt="background"
                    style={{
                      width: "100%",
                      objectFit: "contain",
                      height: "100px",
                    }}
                  />
                </Col>
              ))}
            </Show>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Dropzone
            multiple={false}
            title="Fondos"
            accept="image/jpg,image/png"
            files={clientAddBackground.image ? [clientAddBackground.image] : []}
            onFiles={handleAddBackground}
          />
        </ModalFooter>
      </Show>
    </Modal>
  );
};
