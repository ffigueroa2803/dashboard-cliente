import ObjectID from "bson-objectid";
import { useEffect } from "react";
import { Save } from "react-feather";
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useClientCreate } from "../hooks/use-client-create.hook";
import { ClientForm } from "./client-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const ClientCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const clientCreate = useClientCreate();

  const handleForm = (e: any) => {
    e.preventDefault();
    clientCreate.handleSubmit();
  };

  useEffect(() => {
    if (isOpen) {
      clientCreate.formik.setFieldValue(
        "clientSecret",
        new ObjectID().toHexString()
      );
      // clear errors
      clientCreate.formik.setErrors({});
      clientCreate.formik.setTouched({});
    }
  }, [isOpen]);

  useEffect(() => {
    if (clientCreate.isCreated) onSave();
  }, [clientCreate.isCreated]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nuevo Cliente</ModalHeader>
      <Form onSubmit={handleForm}>
        <ModalBody>
          <ClientForm
            form={clientCreate.form}
            formik={clientCreate.formik}
            handleChange={clientCreate.handleChange}
            handleBlur={clientCreate.handleBlur}
            handleSubmit={clientCreate.handleSubmit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            disabled={clientCreate.pending || !clientCreate.isValid}
          >
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
