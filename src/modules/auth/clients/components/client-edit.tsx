import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useClientEdit } from "../hooks/use-client-edit.hook";
import { ClientForm } from "./client-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const ClientEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { client } = useSelector((state: RootState) => state.client);

  const clientEdit = useClientEdit();

  useEffect(() => {
    if (clientEdit.isUpdated) onSave();
  }, [clientEdit.isUpdated]);

  useEffect(() => {
    if (!isOpen) clientEdit.setForm(client);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Cliente</ModalHeader>
      <Form onSubmit={clientEdit.handleSubmit}>
        <ModalBody>
          <ClientForm
            form={clientEdit.form}
            formik={clientEdit.formik}
            handleChange={clientEdit.handleChange}
            handleBlur={clientEdit.handleBlur}
            handleSubmit={clientEdit.handleSubmit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            disabled={clientEdit.pending || !clientEdit.isValid}
          >
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
