import { InputBasic, InputWrapper } from "@common/inputs/input-basic";
import { BusinessSelect } from "@modules/auth/businesses/components/business-select";
import ObjectID from "bson-objectid";
import {
  FormikFormProps,
  FormikProps,
  FormikState,
  FormikValues,
} from "formik";
import { FormGroup } from "reactstrap";

interface IProps {
  formik: FormikProps<any>;
  form: any;
  handleBlur: (e: any) => void;
  handleChange: (e: any) => void;
  handleSubmit: () => void;
}

export const ClientForm = ({
  formik,
  form,
  handleBlur,
  handleChange,
}: IProps) => {
  const generateSecret = () => {
    formik.setFieldValue("clientSecret", new ObjectID().toHexString());
  };

  return (
    <>
      <FormGroup>
        <InputBasic
          title="Nombre"
          type="text"
          name="name"
          formik={formik}
          value={form.name}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <InputBasic
          title="Client Secret"
          type="text"
          name="clientSecret"
          formik={formik}
          value={form.clientSecret}
          onBlur={handleBlur}
          onChange={handleChange}
        />
        <div className="text-right">
          <b className="text-primary cursor-pointer" onClick={generateSecret}>
            <u>generar client secret</u>
          </b>
        </div>
      </FormGroup>

      <InputWrapper name="businessId" title="Empresa" formik={formik}>
        <BusinessSelect
          name="businessId"
          value={form.businessId}
          onChange={(opt) => handleChange({ target: opt })}
        />
      </InputWrapper>
    </>
  );
};
