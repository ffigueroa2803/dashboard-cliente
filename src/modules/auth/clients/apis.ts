import { AuthRequest } from "@services/auth.request";
import { IClientEntity } from "./dtos/client.entity";
import * as base64Url from "base64-url";

const { request } = AuthRequest();

export const authorize = async (
  clientSecret: string = process.env.NEXT_PUBLIC_CLIENT_SECRET || ""
): Promise<IClientEntity> => {
  const clientId = process.env.NEXT_PUBLIC_CLIENT_ID || "";
  const code = base64Url.encode(`${clientId}#${clientSecret}`);
  return await request.get(`clients/${code}/authorize`).then((res) => res.data);
};
