import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { clientActions } from "../store";

const { request } = AuthRequest();

export const useClientAddBackground = () => {
  const dispatch = useDispatch();

  const { client } = useSelector((state: RootState) => state.client);

  const [image, setImage] = useState<Blob | any>("");
  const [pending, setPending] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    const form = new FormData();
    form.append("file", image);
    // send request
    await request
      .post(`clients/${client.id}/addBackground`, form)
      .then(({ data }) => {
        dispatch(clientActions.changeLogo(data.url));
        toast.success("fondo cambiado!!!");
      })
      .catch(() => toast.error("No se pudo cambiar el fondo"));
    setPending(false);
  };

  return {
    pending,
    image,
    setImage,
    save,
  };
};
