import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch } from "react-redux";
import { initialState, clientActions } from "../store";
import { PaginateDto } from "@services/dtos";
import { IInputHandle } from "@common/dtos/input-handle";

const { request } = AuthRequest();

export const useClientList = (paginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [filter, setFilter] = useState<PaginateDto>(
    paginate || {
      page: 1,
      limit: 30,
    }
  );

  const changeFilter = ({ name, value }: IInputHandle) => {
    setFilter((prev) => ({ ...prev, [name]: value }));
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${filter.page}`);
    params.set("limit", `${filter.limit}`);
    params.set("querySearch", `${filter.querySearch || ""}`);
    await request
      .get(`clients`, { params })
      .then((res) => dispatch(clientActions.paginate(res.data)))
      .catch(() => dispatch(clientActions.paginate(initialState.clients)));
    setPending(false);
  };

  return {
    pending,
    filter,
    changeFilter,
    setFilter,
    fetch,
  };
};
