import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch } from "react-redux";
import { clientActions } from "../store";

const { request } = AuthRequest();

export const useClientFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async (id: number) => {
    setPending(true);
    await request
      .get(`clients/${id}`)
      .then((res) => dispatch(clientActions.setClient(res.data)))
      .catch(() => dispatch(clientActions.setClient({} as any)));
    setPending(false);
  };

  return {
    pending,
    fetch,
  };
};
