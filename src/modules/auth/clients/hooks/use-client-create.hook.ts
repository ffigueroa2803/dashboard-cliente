import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { clientCreateValidate } from "../dtos/client-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import ObjectID from "bson-objectid";

const { request } = AuthRequest();

export const useClientCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isCreated, setIsCreated] = useState<boolean>(false);

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsCreated(false);
    await request
      .post(`clients`, values)
      .then(() => {
        resetForm();
        setIsCreated(true);
        toast.success(`El rol se guardó correctamente!`);
      })
      .catch((err) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: {
      clientSecret: new ObjectID().toHexString(),
    },
    validationSchema: clientCreateValidate,
    onSubmit: save,
  });

  return {
    pending,
    isCreated,
    form: formik.values,
    formik: formik,
    errors: formik.errors,
    isValid: formik.isValid,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
