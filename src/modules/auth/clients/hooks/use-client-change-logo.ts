import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { clientActions } from "../store";

const { request } = AuthRequest();

export const useClientChangeLogo = () => {
  const dispatch = useDispatch();

  const { client } = useSelector((state: RootState) => state.client);

  const [image, setImage] = useState<Blob | string>("");
  const [pending, setPending] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    const form = new FormData();
    form.append("file", image);
    // send request
    await request
      .put(`clients/${client.id}/changeLogo`, form)
      .then(({ data }) => {
        dispatch(clientActions.changeLogo(data.logoUrl));
        toast.success("Logo cambiado!!!");
      })
      .catch(() => toast.error("No se pudo cambiar el logo"));
    setPending(false);
  };

  return {
    pending,
    image,
    setImage,
    save,
  };
};
