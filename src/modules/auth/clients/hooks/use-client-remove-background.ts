import { useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { clientActions } from "../store";

const { request } = AuthRequest();

export const useClientRemoveBackground = () => {
  const dispatch = useDispatch();

  const { client } = useSelector((state: RootState) => state.client);
  const [pending, setPending] = useState<boolean>(false);

  const destroy = async (id: number) => {
    setPending(true);
    // send request
    await request
      .destroy(`clients/${client.id}/removeBackground/${id}`)
      .then(({ data }) => {
        dispatch(clientActions.changeLogo(data.url));
        toast.success("fondo eliminado!!!");
      })
      .catch(() => toast.error("No se pudo eliminar el fondo"));
    setPending(false);
  };

  return {
    pending,
    destroy,
  };
};
