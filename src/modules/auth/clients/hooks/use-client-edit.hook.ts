import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { clientActions } from "../store";
import { clientCreateValidate } from "../dtos/client-create.dto";

const { request } = AuthRequest();

export const useClientEdit = () => {
  const dispatch = useDispatch();

  const { client } = useSelector((state: RootState) => state.client);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (values: any): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`clients/${client.id}`, values)
      .then((res) => {
        setIsUpdated(true);
        toast.success(`Los datos se actualizaron correctamente!`);
        dispatch(clientActions.setClient(res.data));
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: client,
    validationSchema: clientCreateValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (client?.id) formik.setValues(client);
  }, [client]);

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    isValid: formik.isValid,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
