import { IBusinessEntity } from "@modules/auth/businesses/dtos/business.entity";

export interface IClientEntity {
  id: number;
  name: string;
  businessId: number;
  clientId: number;
  clientSecret: number;
  logoUrl: string;
  state: boolean;
  business?: IBusinessEntity;
  backgrounds: { id: number; url: string }[];
}

export const clientEntityName = "ClientEntity";
