import { Yup } from "@common/yup";

export const clientCreateValidate = Yup.object().shape({
  name: Yup.string().required().min(2).max(100),
  clientSecret: Yup.string().required().min(2).max(100),
  bussinesId: Yup.number(),
});

export interface IClientCreateDto {
  name: string;
  clientSecret: string;
  bussinesId: number;
}
