import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IClientEntity } from "./dtos/client.entity";

export interface ClientState {
  clients: ResponsePaginateDto<IClientEntity>;
  client: IClientEntity;
  currentClient: IClientEntity;
}

export const initialState: ClientState = {
  clients: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  client: {} as any,
  currentClient: {} as any,
};

export const clientSlice = createSlice({
  name: "auth@clients",
  initialState,
  reducers: {
    paginate: (
      state: ClientState,
      { payload }: PayloadAction<ResponsePaginateDto<IClientEntity>>
    ) => {
      state.clients = payload;
      return state;
    },
    setClient: (state: ClientState, action: PayloadAction<IClientEntity>) => {
      state.client = action.payload;
      return state;
    },
    setCurrentClient: (
      state: ClientState,
      action: PayloadAction<IClientEntity>
    ) => {
      state.currentClient = action.payload;
      return state;
    },
    changeLogo: (state: ClientState, { payload }: PayloadAction<string>) => {
      state.client.logoUrl = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.client };
    },
  },
});

export const clientReducer = clientSlice.reducer;

export const clientActions = clientSlice.actions;
