import { RootState } from "@store/store";
import { Monitor } from "react-feather";
import { useSelector } from "react-redux";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { useUserAuditPaginate } from "../hooks/use-user-audit-paginate.hook";
import { AuditTable } from "@modules/auth/audit/components/audit-table";

export interface UserAuditModalProps {
  isOpen: boolean;
  onClose(): void;
}

export function UserAuditModal({ isOpen, onClose }: UserAuditModalProps) {
  const { user } = useSelector((state: RootState) => state.user);
  const hook = useUserAuditPaginate(user?.id, isOpen);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        <Monitor className="icon" /> Monitoreo de actividades
      </ModalHeader>
      <ModalBody>
        <AuditTable hook={hook} />
      </ModalBody>
    </Modal>
  );
}
