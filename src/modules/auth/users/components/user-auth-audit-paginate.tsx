import { useUserAuditPaginate } from "../hooks/use-user-audit-paginate.hook";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { Card, CardBody, CardHeader } from "reactstrap";
import { Monitor } from "react-feather";
import { AuditTable } from "@modules/auth/audit/components/audit-table";

export function UserAuthAuditPaginate() {
  const { user } = useSelector((state: RootState) => state.auth);
  const hook = useUserAuditPaginate(user?.id);

  return (
    <Card>
      <CardHeader>
        <Monitor className="icon" /> Monitoreo de actividades
      </CardHeader>
      <CardBody>
        <AuditTable hook={hook} />
      </CardBody>
    </Card>
  );
}
