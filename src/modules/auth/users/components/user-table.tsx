/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit, Monitor } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IUserEntity } from "../dtos/user.entity";
import { userActions } from "../store";
import { userContextEnum, UserContext } from "./user-context";

interface IProps {
  onClick?: (user: IUserEntity, action: userContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const UserTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { users } = useSelector((state: RootState) => state.user);

  const userContext = useContext(UserContext);
  const ability = useContext(CaslContext);

  const handleClick = (user: IUserEntity, action: userContextEnum) => {
    dispatch(userActions.setUser(user));
    userContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(user, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      grow: true,
      cell: (row: IUserEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, userContextEnum.EDIT)}
          />
          <Monitor
            className="icon cursor-pointer"
            onClick={() => handleClick(row, userContextEnum.AUDIT)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,

        cell: (row: IUserEntity) => row.id,
      },
      {
        name: "Email",
        wrap: true,
        cell: (row: IUserEntity) => row.email,
      },
      {
        name: "Username",
        wrap: true,
        center: true,
        cell: (row: IUserEntity) => row.username,
      },
      {
        name: "Descripción",
        wrap: true,
        selector: (row: IUserEntity) => {
          if (row.userableType == "PersonEntity") {
            const person: PersonEntity = row.userableObject as any;
            return (
              <span className="uppercase">
                <Show condition={row.userableType == "PersonEntity"}>
                  <span>{person?.fullName || ""}</span>
                </Show>
              </span>
            );
          }

          return null;
        },
      },
      {
        name: "Rol",
        wrap: true,
        selector: (row: IUserEntity) => <span>{row?.role?.name || ""}</span>,
      },
      {
        name: "Estado",
        grow: true,
        selector: (row: IUserEntity) => {
          return (
            <span className={`badge badge-${row.state ? "success" : "danger"}`}>
              {row.state ? "Activo" : "Inactivo"}
            </span>
          );
        },
      },
    ];

    rows.push(options);

    return rows;
  }, [users]);

  return (
    <>
      <Datatable
        data={users.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={users?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={users?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
