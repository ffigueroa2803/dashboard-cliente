/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useUserEdit } from "../hooks/use-user-edit.hook";
import { UserForm } from "./user-form";

interface IProps {
  isOpen: boolean;
  isRole?: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const UserEdit = ({
  isOpen,
  isRole = true,
  onClose,
  onSave,
}: IProps) => {
  const { user } = useSelector((state: RootState) => state.user);

  const userEdit = useUserEdit();

  useEffect(() => {
    if (userEdit.isUpdated) onSave();
  }, [userEdit.isUpdated]);

  useEffect(() => {
    if (!isOpen) userEdit.setForm(user);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Usuario</ModalHeader>
      <ModalBody>
        <UserForm
          isEdit={true}
          isRole={isRole}
          form={userEdit.form}
          formik={userEdit.formik}
          handleChange={userEdit.handleChange}
          handleBlur={userEdit.handleBlur}
          handleSubmit={userEdit.handleSubmit}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          onClick={() => userEdit.handleSubmit()}
          disabled={userEdit.pending}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
