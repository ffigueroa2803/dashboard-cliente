/* eslint-disable no-unused-vars */
import { createContext, FC, useState } from "react";

export enum userContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  AUDIT = "AUDIT",
}

export const UserContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: userContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const UserProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<userContextEnum>(
    userContextEnum.DEFAULT
  );

  return (
    <UserContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};
