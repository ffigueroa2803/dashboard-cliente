import React, { useEffect, useState } from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Button,
  FormGroup,
  Input,
} from "reactstrap";
import { UserForm } from "./user-form";
import { ProgressIndicator } from "@atlaskit/progress-indicator";
import { Save, ArrowLeft, User } from "react-feather";
import { PersonSearchSelect } from "@modules/auth/person/components/person-search-select";
import { personEntityName } from "@modules/auth/person/dtos/person.entity";
import { useUserCreate } from "../hooks/use-user-create";

interface IProps {
  onClose: () => void;
  isOpen?: boolean;
  onSave: () => void;
}

export const UserStepCreate = ({ isOpen = true, onClose, onSave }: IProps) => {
  const userCreate = useUserCreate();

  const [steps] = useState(["person", "works"]);
  const [currentStep, setCurrentStep] = useState(0);

  const handleSelectObject = (type: string, subject: any) => {
    userCreate.addUserableObject(type, subject);
    setCurrentStep(1);
  };

  useEffect(() => {
    if (userCreate.isCreated) onSave();
  }, [userCreate.isCreated]);

  const ComponentCreateWork = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>Apellidos y Nombres</label>
          <Input
            type="text"
            className="capitalize"
            readOnly
            value={`${userCreate.userableObject?.fullName || ""}`.toLowerCase()}
          />
        </FormGroup>

        <FormGroup>
          <label>Tipo Documento</label>
          <Input
            type="text"
            className="uppercase"
            readOnly
            value={`${
              userCreate.userableObject?.documentType?.name || ""
            }`.toLowerCase()}
          />
        </FormGroup>

        <FormGroup>
          <label>N° Documento</label>
          <Input
            type="text"
            className="capitalize"
            readOnly
            value={userCreate.userableObject?.documentNumber}
          />
        </FormGroup>
        <hr />
      </div>
      <UserForm
        isRole={true}
        isEdit={false}
        form={userCreate.form}
        formik={userCreate.formik}
        handleChange={userCreate.handleChange}
        handleBlur={userCreate.handleBlur}
        handleSubmit={userCreate.handleSubmit}
      />
      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => setCurrentStep(0)}
            disabled={userCreate.pending}
          >
            <ArrowLeft className="icon" />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={() => userCreate.handleSubmit()}
            disabled={userCreate.pending}
          >
            <Save className="icon" />
          </Button>
        </Col>
      </Row>
    </div>
  );

  const ComponentSearch = (
    <div className="mt-5">
      <label>
        <User className="icon" /> Validar Persona
      </label>
      <PersonSearchSelect
        onAdd={(person) => handleSelectObject(personEntityName, person)}
      />
    </div>
  );

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nuevo Usuario</ModalHeader>
      <ModalBody>
        <ProgressIndicator
          selectedIndex={currentStep}
          values={steps}
          appearance={"primary"}
        />
        {currentStep == 0 ? ComponentSearch : null}
        {currentStep == 1 ? ComponentCreateWork : null}
      </ModalBody>
    </Modal>
  );
};
