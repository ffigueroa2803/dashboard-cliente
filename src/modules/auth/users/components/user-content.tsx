/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import { UserContext, UserProvider, userContextEnum } from "./user-context";
import { useUserList } from "../hooks/use-user-list";
import { FloatButton } from "@common/button/float-button";
import { UserTable } from "./user-table";
import { UserStepCreate } from "./user-step-create";
import { UserEdit } from "./user-edit";
import { UserAuditModal } from "./user-audit-modal";
// import { RoleEdit } from "./role-edit";

const UserWrapper = () => {
  const userContext = useContext(UserContext);

  const userList = useUserList({
    page: 1,
    limit: 30,
    querySearch: userContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    userList.setPage(page);
    userContext.setClearRows(true);
    userContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    userList.setLimit(limit);
    userContext.setClearRows(true);
    userContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = () => {
    userContext.setOptions(userContextEnum.DEFAULT);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    userList.setQuery({
      querySearch: userContext.querySearch,
    });
  }, [userContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      userContext.setClearRows(true);
      userContext.setIds([]);
      userList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={userContext.querySearch}
                  disabled={userList.pending}
                  onChange={({ target }) =>
                    userContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <UserTable
            loading={userList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => userContext.setOptions(userContextEnum.CREATE)}
      />
      {/* crear */}
      <UserStepCreate
        isOpen={userContext.options == userContextEnum.CREATE}
        onClose={() => userContext.setOptions(userContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <UserEdit
        isOpen={userContext.options == userContextEnum.EDIT}
        onClose={() => userContext.setOptions(userContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* audit */}
      <UserAuditModal
        isOpen={userContext.options === userContextEnum.AUDIT}
        onClose={() => userContext.setOptions(userContextEnum.DEFAULT)}
      />
    </>
  );
};

export const UserContent = () => {
  return (
    <UserProvider>
      <UserWrapper />
    </UserProvider>
  );
};
