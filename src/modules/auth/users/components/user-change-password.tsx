import { InputBasic } from "@common/inputs/input-basic";
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Key, Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useUserChangePassword } from "../hooks/use-user-change-password.hook";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const UserChangePassword = ({ isOpen, onClose, onSave }: IProps) => {
  const { user } = useSelector((state: RootState) => state.auth);

  const userChangePassword = useUserChangePassword();

  const handleSave = (e: any) => {
    e.preventDefault();
    if (!userChangePassword.isValid) return;
    userChangePassword.handleSubmit();
  };

  useEffect(() => {
    if (isOpen) {
      userChangePassword.formik.setFieldValue("password", "");
      userChangePassword.formik.setFieldValue("newPassword", "");
    }
  }, [isOpen]);

  useEffect(() => {
    if (userChangePassword.isUpdated) {
      onSave();
    }
  }, [userChangePassword.isUpdated]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        <Key className="icon" /> Cambiar contraseña
      </ModalHeader>
      <Form onSubmit={handleSave}>
        <ModalBody>
          <FormGroup className="mb-3">
            <label>Username</label>
            <h6>{user?.username || "N/A"}</h6>
          </FormGroup>

          <FormGroup className="mb-3">
            <InputBasic
              title="Contraseña"
              type="password"
              name="password"
              formik={userChangePassword.formik}
              value={userChangePassword.form.password || ""}
              onBlur={userChangePassword.handleBlur}
              onChange={userChangePassword.handleChange}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <InputBasic
              title="Nueva Contraseña"
              type="password"
              name="newPassword"
              formik={userChangePassword.formik}
              value={userChangePassword.form.newPassword || ""}
              onBlur={userChangePassword.handleBlur}
              onChange={userChangePassword.handleChange}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter className="text-right">
          <Button
            color="primary"
            disabled={userChangePassword.pending || !userChangePassword.isValid}
          >
            <Save className="icon" /> Actualizar
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
