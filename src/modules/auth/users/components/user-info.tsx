/* eslint-disable @next/next/no-img-element */
/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import { useState } from "react";
import { Edit, Key, User } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  FormGroup,
  Row,
} from "reactstrap";
import UserDefault from "src/assets/images/user.jpg";
import { IUserEntity } from "../dtos/user.entity";
import { useUserFind } from "../hooks/use-user-find";
import { UserChangePassword } from "./user-change-password";
import { UserEdit } from "./user-edit";

interface IProps {
  onUpdate?: (user: IUserEntity) => void;
}

export const UserInfo = ({ onUpdate }: IProps) => {
  const { user } = useSelector((state: RootState) => state.user);

  const userFind = useUserFind();

  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [changePassword, setChangePassword] = useState<boolean>(false);

  const handleUpdate = async () => {
    await userFind
      .fetch(user.id)
      .then((data) => handleOnUpdate(data))
      .catch(() => handleOnUpdate(user));
  };

  const handleOnUpdate = (user: IUserEntity) => {
    if (typeof onUpdate == "function") {
      onUpdate(user);
    }
  };

  return (
    <>
      <Card>
        <CardHeader>
          <h6>
            <Edit
              className="icon close cursor-pointer"
              onClick={() => setIsEdit(true)}
            />
            <User className="icon" /> Editar Usuario
          </h6>
        </CardHeader>
        <CardBody>
          <Row className="mb-3">
            <div className="col-auto">
              <img
                alt="profile"
                src={user.imageUrl || UserDefault.src}
                className="img-70 rounded-circle media"
              />
            </div>
            <div className="col">
              <h3 className="mb-1">{user.username || "@"}</h3>
              <p className="mb-4">{user?.role?.name || "N/A"}</p>
            </div>
          </Row>

          <FormGroup className="mb-3">
            <label>Correo Electrónico</label>
            <h6>{user.email || "N/A"}</h6>
          </FormGroup>

          <FormGroup className="mb-5">
            <label>Nombre de Usuario</label>
            <h6>{user.username || "N/A"}</h6>
          </FormGroup>
        </CardBody>
        <CardFooter className="text-center">
          <Button
            color="primary"
            outline
            onClick={() => setChangePassword(true)}
          >
            <Key className="icon" />
            Cambiar Contraseña
          </Button>
        </CardFooter>
      </Card>
      {/* editar usuario */}
      <UserEdit
        isOpen={isEdit}
        isRole={false}
        onClose={() => setIsEdit(false)}
        onSave={handleUpdate}
      />
      {/* cambiar password */}
      <UserChangePassword
        isOpen={changePassword}
        onClose={() => setChangePassword(false)}
        onSave={() => setChangePassword(false)}
      />
    </>
  );
};
