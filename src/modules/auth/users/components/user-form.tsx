/* eslint-disable no-unused-vars */
import { InputBasic, InputWrapper } from "@common/inputs/input-basic";
import { Form, FormGroup } from "reactstrap";
import { RoleSelect } from "@modules/auth/roles/components/role-select";
import { Show } from "@common/show";
import Toggle from "@atlaskit/toggle";

interface IProps {
  form: any;
  isEdit: boolean;
  isRole: boolean;
  formik: any;
  handleBlur: (e: any) => void;
  handleChange: (e: any) => void;
  handleSubmit: () => void;
}

export const UserForm = ({
  form,
  isEdit,
  isRole,
  formik,
  handleBlur,
  handleChange,
  handleSubmit,
}: IProps) => {
  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}>
      <FormGroup>
        <InputBasic
          title="Correo Electrónico"
          type="text"
          name="email"
          formik={formik}
          value={form.email}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <InputBasic
          title="Nombre de usuario"
          type="text"
          name="username"
          formik={formik}
          value={form.username}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <Show condition={isRole}>
        <FormGroup>
          <InputWrapper name="roleId" title="Rol" formik={formik}>
            <RoleSelect
              name="roleId"
              value={form.roleId}
              onChange={(opt) => handleChange({ target: opt })}
            />
          </InputWrapper>
        </FormGroup>
      </Show>

      <FormGroup>
        <InputBasic
          title="Contraseña"
          type="password"
          name="password"
          formik={formik}
          value={form.password}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <Show condition={!isEdit}>
        <FormGroup>
          <InputBasic
            title="Confirmar Contraseña"
            type="password"
            name="passwordConfirm"
            formik={formik}
            value={form.passwordConfirm}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        </FormGroup>
      </Show>

      <Show condition={isEdit && isRole}>
        <FormGroup>
          <InputWrapper title="Estado" name="state" formik={formik}>
            <div>
              <Toggle
                name="state"
                isChecked={form.state}
                onBlur={handleBlur}
                onChange={handleChange}
              />
            </div>
          </InputWrapper>
        </FormGroup>
      </Show>
    </Form>
  );
};
