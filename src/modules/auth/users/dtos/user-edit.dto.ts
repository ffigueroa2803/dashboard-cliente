import { Yup } from "@common/yup";

export interface IUserCreateDto {
  email: string;
  username: string;
  roleId: number;
  campusId: number;
  state: boolean;
}

export const userEditValidate = Yup.object().shape({
  email: Yup.string().required().email(),
  username: Yup.string().required().length(10),
  roleId: Yup.number().required(),
  campusId: Yup.number().required(),
  state: Yup.boolean().required(),
});
