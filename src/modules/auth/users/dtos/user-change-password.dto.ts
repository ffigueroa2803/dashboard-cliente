import { Yup } from "@common/yup";

export interface IUserChangePasswordDto {
  username: string;
  password: string;
  newPassword: string;
}

export const userChangePasswordValidate = Yup.object().shape({
  username: Yup.string().required().min(2),
  password: Yup.string().required().min(2),
  newPassword: Yup.string().required().min(8).max(40),
});
