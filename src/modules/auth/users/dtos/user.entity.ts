import { IBusinessEntity } from "@modules/auth/businesses/dtos/business.entity";
import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { IRoleEntity } from "@modules/auth/roles/dtos/role.entity";

export interface IUserEntity {
  id: number;
  email: string;
  username: string;
  roleId: number;
  role: IRoleEntity;
  userableType: string;
  userableId: number;
  userableObject?: PersonEntity | IBusinessEntity;
  campusId: number;
  imageUrl?: string;
  state: boolean;
}

export const userEntityName = "UserEntity";
