import { Yup } from "@common/yup";

export interface IUserCreateDto {
  userableType: string;
  userableId: number;
  email: string;
  username: string;
  password: string;
  passwordConfirm: string;
  roleId: number;
  campusId: number;
}

export const userCreateValidate = Yup.object().shape({
  userableType: Yup.string().required().min(2).max(50),
  userableId: Yup.number().required(),
  email: Yup.string().required().email(),
  username: Yup.string().required().length(10),
  password: Yup.string().required().min(8).max(12),
  passwordConfirm: Yup.string().required().min(8).max(12),
  roleId: Yup.number().required(),
  campusId: Yup.number().required(),
});
