import { AuditPaginateParams } from "@modules/auth/audit/domain/audit.params";

export interface UserAuditPaginateDto {
  id: number;
  params: AuditPaginateParams;
}
