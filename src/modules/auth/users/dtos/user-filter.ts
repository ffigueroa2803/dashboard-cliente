export interface IUserFilterDto {
  querySearch?: string;
  state?: boolean;
}
