export interface IUserFormDto {
  userableType: string;
  userableId: number;
  email: string;
  username: string;
  password: string;
  passwordConfirm?: string;
  roleId: number;
  campusId: number;
}
