import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IUserEntity } from "./dtos/user.entity";

export interface IUserState {
  users: ResponsePaginateDto<IUserEntity>;
  user: IUserEntity;
}

export const initialState: IUserState = {
  users: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 30,
    },
  },
  user: {} as any,
};

export const userSlice = createSlice({
  name: "auth@user",
  initialState,
  reducers: {
    setUser: (state, { payload }: PayloadAction<IUserEntity>) => {
      state.user = payload as IUserEntity;
      return state;
    },
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IUserEntity>>
    ) => {
      state.users = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.user };
    },
  },
});

export const userReducer = userSlice.reducer;

export const userActions = userSlice.actions;
