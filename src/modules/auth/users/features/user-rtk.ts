import { AuditEntity } from "@modules/auth/audit/domain/audit.entity";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { UserAuditPaginateDto } from "../dtos/user-audit-paginate.dto";

const baseUrl = process.env.NEXT_PUBLIC_IDENTITY_URL || "";

export const userRtk = createApi({
  reducerPath: "userRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    paginateAudits: builder.query<
      PaginateEntity<AuditEntity>,
      UserAuditPaginateDto
    >({
      query: ({ id, params }) => ({
        url: `users/${id}/audits`,
        params,
      }),
    }),
  }),
});

export const { useLazyPaginateAuditsQuery } = userRtk;
