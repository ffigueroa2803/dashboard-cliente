import { useState } from "react";
import { useDispatch } from "react-redux";
import { AuthRequest } from "@services/auth.request";
import { initialState, userActions } from "../store";
import { alerStatus } from "@common/errors/utils/alert-status";
import { IUserEntity } from "../dtos/user.entity";

const { request } = AuthRequest();

export const useUserFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async (id: number): Promise<IUserEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      // response
      await request
        .get(`users/${id}`)
        .then((res) => {
          dispatch(userActions.setUser(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(userActions.setUser(initialState.user));
          if (err?.response) {
            const status = err?.response?.status;
            alerStatus(status);
          }
          // send error
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
  };
};
