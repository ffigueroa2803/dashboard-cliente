import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import ObjectId from "bson-objectid";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import {
  PersonEntity,
  personEntityName,
} from "@modules/auth/person/dtos/person.entity";
import { userCreateValidate } from "../dtos/user-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { FormikHelpers, useFormik } from "formik";

const { request } = AuthRequest();

export const userTypes = {
  PERSON: personEntityName,
};

export const useUserCreate = () => {
  const { user } = useSelector((state: RootState) => state.auth);

  const [pending, setPending] = useState<boolean>(false);
  const [isCreated, setIsCreated] = useState<boolean>(false);

  const dataDefault = {
    userableId: 0,
    userableType: userTypes.PERSON,
    email: "",
    username: `${new ObjectId().toHexString()}`.substring(0, 10),
    password: "",
    passwordConfirm: "",
    roleId: 0,
    campusId: user?.campusId || 0,
  };

  const [userableObject, setUserableObject] = useState<any>({});

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsCreated(false);
    await request
      .post(`users`, values)
      .then(() => {
        resetForm();
        setIsCreated(true);
        toast.success(`El rol se guardó correctamente!`);
      })
      .catch((err) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: dataDefault,
    validationSchema: userCreateValidate,
    onSubmit: save,
    validate(values) {
      const errors: any = {};
      if (
        values.password &&
        values.passwordConfirm &&
        values.passwordConfirm != values.password
      ) {
        errors.passwordConfirm = "El valor debe ser igual a la contraseña";
        return errors;
      }
    },
  });

  const addUserableObject = (type: string, subject: any) => {
    switch (type) {
      case userTypes.PERSON:
        const person: PersonEntity = subject;
        let username = person.lastname.substring(0, 1);
        username += person.name.substring(0, 1);
        username += person.documentNumber;
        setUserableObject(person);
        formik.setFieldTouched("password", false);
        formik.setFieldTouched("passwordConfirm", false);
        formik.setFieldTouched("email", true);
        formik.setFieldTouched("username", true);
        formik.setValues((prev) => ({
          ...prev,
          userableType: userTypes.PERSON,
          userableId: person.id,
          email: `${person.emailContact || ""}`.toLowerCase(),
          username: username.toUpperCase(),
          password: "",
          passwordConfirm: "",
        }));
        break;
      default:
        break;
    }
  };

  return {
    pending,
    isCreated,
    form: formik.values,
    touched: formik.touched,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
    addUserableObject,
    userableObject,
    formik,
  };
};
