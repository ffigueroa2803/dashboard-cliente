/* eslint-disable react-hooks/exhaustive-deps */
import { AuditListHook } from "@modules/auth/audit/hooks/interfaces/audit-list.hook";
import { useLazyPaginateAuditsQuery } from "../features/user-rtk";
import { useEffect } from "react";

export function useUserAuditPaginate(
  userId?: number,
  isRefresh?: boolean
): AuditListHook {
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateAuditsQuery();

  const handle = () => {
    if (!userId) return;
    if (typeof isRefresh === "boolean" && !isRefresh) return;
    fetch({ id: userId, params: { page: 1, limit: 20 } });
  };

  useEffect(() => {
    handle();
  }, [userId, isRefresh]);

  return { isLoading: isLoading || isFetching, data };
}
