import { useState } from "react";
import { useDispatch } from "react-redux";
import { AuthRequest } from "@services/auth.request";
import { initialState, userActions } from "../store";
import { IUserFilterDto } from "../dtos/user-filter";
import { alerStatus } from "@common/errors/utils/alert-status";
import { PaginateDto } from "@services/dtos";

const { request } = AuthRequest();

export const useUserList = (paginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(paginate?.page || 1);
  const [limit, setLimit] = useState<number>(paginate?.limit || 30);
  const [query, setQuery] = useState<IUserFilterDto>({
    querySearch: paginate?.querySearch || "",
  });

  const fetch = async () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const params = new URLSearchParams();
      params.set("page", `${page}`);
      params.set("limit", `${limit}`);
      // validar attr
      const values: any = Object.assign(query, {});
      Object.keys(values).forEach((attr: any) => {
        const value = values[attr];
        if (typeof value != "undefined") {
          params.set(`${attr}`, `${value}`);
        }
      });
      // response
      await request
        .get(`users`, { params })
        .then((res) => {
          dispatch(userActions.paginate(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(userActions.paginate(initialState.users));
          if (err?.response) {
            const status = err?.response?.status;
            alerStatus(status);
          }
          // send error
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    page,
    setPage,
    limit,
    setLimit,
    query,
    setQuery,
    fetch,
  };
};
