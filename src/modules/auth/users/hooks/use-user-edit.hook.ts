import { FormikHelpers, useFormik } from "formik";
import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { userEditValidate } from "../dtos/user-edit.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { userActions } from "../store";

const { request } = AuthRequest();

export const useUserEdit = () => {
  const dispatch = useDispatch();

  const { user } = useSelector((state: RootState) => state.user);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (
    values: any,
    { setTouched }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`users/${user.id}`, values)
      .then((res) => {
        setIsUpdated(true);
        toast.success(`El usuario se actualizó correctamente!`);
        dispatch(userActions.setUser(res.data));
        setTouched({});
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: user,
    validationSchema: userEditValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (user?.id) formik.setValues(user);
  }, [user]);

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
