import { FormikHelpers, useFormik } from "formik";
import { useEffect, useState } from "react";
import { AuthRequest } from "@services/auth.request";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { userChangePasswordValidate } from "../dtos/user-change-password.dto";

const { request } = AuthRequest();

export const useUserChangePassword = () => {
  const { user } = useSelector((state: RootState) => state.auth);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`auth/changePassword`, values)
      .then(() => {
        setIsUpdated(true);
        resetForm();
        toast.success(`La contraseña se actualizó correctamente!`);
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: user as any,
    validationSchema: userChangePasswordValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (user?.id)
      formik.setValues({
        username: user.email,
        password: "",
        newPassword: "",
      });
  }, [user]);

  return {
    pending,
    isUpdated,
    isValid: formik.isValid,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
