import { useState } from "react";
import { toast } from "react-toastify";
import { InstitutionPaginateParams } from "../domain/institution.params";
import { useLazyPaginateInstitutionsQuery } from "../features/institution-rtk";

export function useInstitutionPaginate(defaultFilters?: InstitutionPaginateParams) {
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateInstitutionsQuery();
  const [filters, setFilters] = useState<InstitutionPaginateParams>(
    Object.assign({ page: 1, limit: 100 }, defaultFilters || {})
  )

  const handle = async () => {
    return fetch(filters)
      .unwrap()
      .catch((err: any) => {
        toast.error(err.data?.message || "Algo salío mal")
      })
  }

  return {
    isFetching,
    isLoading,
    handle,
    filters,
    setFilters,
    data
  }
}