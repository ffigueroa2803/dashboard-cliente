/* eslint-disable no-unused-vars */
import { SelectRemote } from "@common/select/select-remote";
import { InstitutionEntity } from "../domain/institution.entity";
import { useInstitutionPaginate } from "../hooks/use-institution-paginate";

interface IProps {
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const InstitutionSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
}: IProps) => {
  const hook = useInstitutionPaginate({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
  });

  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={hook.handle}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: InstitutionEntity) =>
          `${row.code} - ${row.name}`.toLowerCase(),
        value: (row: InstitutionEntity) => row.code,
      }}
    />
  );
};
