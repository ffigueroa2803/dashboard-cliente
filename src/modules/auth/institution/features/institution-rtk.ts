import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { InstitutionEntity } from "../domain/institution.entity";
import { InstitutionPaginateParams } from "../domain/institution.params";

const baseUrl = process.env.NEXT_PUBLIC_IDENTITY_URL || "";

export const institutionRtk = createApi({
  reducerPath: "institutionRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    paginateInstitutions: builder.query<PaginateEntity<InstitutionEntity>, InstitutionPaginateParams>({
      query: (params) => ({
        url: 'institutions',
        params
      })
    })
  }),
});

export const { useLazyPaginateInstitutionsQuery } = institutionRtk;
