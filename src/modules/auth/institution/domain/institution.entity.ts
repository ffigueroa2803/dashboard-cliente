export interface InstitutionEntity {
  code: string;
  name: string;
}