export interface InstitutionFindParams {
  querySearch?: string;
}

export interface InstitutionListParams extends InstitutionFindParams {

}

export interface InstitutionPaginateParams extends InstitutionListParams {
  page: number;
  limit: number;
}