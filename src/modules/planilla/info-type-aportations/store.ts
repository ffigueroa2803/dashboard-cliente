import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IInfoTypeAportationEntity } from "./dtos/info-type-aportation.entity";

export interface InfoTypeAportationState {
  infoTypeAportations: ResponsePaginateDto<IInfoTypeAportationEntity>;
  infoTypeAportation: IInfoTypeAportationEntity;
  option: string;
}

const initialState: InfoTypeAportationState = {
  infoTypeAportations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  infoTypeAportation: {} as any,
  option: "",
};

const InfoTypeAportationState = createSlice({
  name: "planilla@infoTypeAportation",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IInfoTypeAportationEntity>>
    ) => {
      state.infoTypeAportations = payload as any;
    },
    find: (state, { payload }: PayloadAction<IInfoTypeAportationEntity>) => {
      state.infoTypeAportation = payload as any;
    },
    updateItem: (
      state,
      { payload }: PayloadAction<IInfoTypeAportationEntity>
    ) => {
      state.infoTypeAportations?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }
        return item;
      });
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.infoTypeAportation };
    },
  },
});

export const infoTypeAportationReducer = InfoTypeAportationState.reducer;

export const infoTypeAportationActions = InfoTypeAportationState.actions;
