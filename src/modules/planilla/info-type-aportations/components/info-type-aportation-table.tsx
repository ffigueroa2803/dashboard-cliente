import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { Search, Edit, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import currencyFormatter from "currency-formatter";
import { useInfoTypeAportationDelete } from "../hooks/use-info-type-aportation-delete";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (querySearch: string | string[]) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: IInfoTypeAportationEntity) => any;
}

interface IProps {
  data: IInfoTypeAportationEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: (infoTypeAportation: IInfoTypeAportationEntity) => void;
  onDelete?: (infoTypeAportation: IInfoTypeAportationEntity) => void;
}

export const InfoTypeAportationTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onDelete,
  options,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const infoDelete = useInfoTypeAportationDelete();

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IInfoTypeAportationEntity) => row.id,
      },
      {
        name: "Descripción",
        wrap: true,
        selector: (row: IInfoTypeAportationEntity) => {
          return <span>{row?.typeAportation?.name}</span>;
        },
      },
      {
        name: "PIM",
        wrap: true,
        selector: (row: IInfoTypeAportationEntity) => (
          <Show condition={(row?.pimId || 0) > 0} isDefault={<span>N/A</span>}>
            <span>
              {row?.pim?.code} [{row?.pim?.cargo?.extension}]
            </span>
          </Show>
        ),
      },
      {
        name: "Porcentaje",
        center: true,
        selector: (row: IInfoTypeAportationEntity) => (
          <span>{row?.typeAportation?.percent || 0} %</span>
        ),
      },
      {
        name: "Mínimo",
        center: true,
        selector: (row: IInfoTypeAportationEntity) => (
          <span>{row?.typeAportation?.min || 0}</span>
        ),
      },
      {
        name: "Predeterminado",
        center: true,
        selector: (row: IInfoTypeAportationEntity) => (
          <span>
            {currencyFormatter.format(row?.typeAportation?.default || 0, {
              code: "PEN",
            })}
          </span>
        ),
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: IInfoTypeAportationEntity) => (
          <>
            <Edit
              className="cursor-pointer icon"
              onClick={() => handleClick(row)}
            />
            <Trash
              className="cursor-pointer icon"
              onClick={() => handleDelete(row)}
            />
          </>
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (infoTypeAportation: IInfoTypeAportationEntity) => {
    if (typeof onClick == "function") onClick(infoTypeAportation);
  };

  const handleDelete = (infoTypeAportation: IInfoTypeAportationEntity) => {
    infoDelete
      .destroy(infoTypeAportation.id)
      .then(() => {
        if (typeof onDelete == "function") {
          onDelete(infoTypeAportation);
        }
      })
      .catch(() => null);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
