/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { Plus, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { useInfoTypeAportationsToInfo } from "../hooks/use-info-type-discounts-to-info";
import { infoTypeAportationActions } from "../store";
import { InfoTypeAportationCreate } from "./info-type-aportation-create";
import { InfoTypeAportationEdit } from "./info-type-aportation-edit";
import { InfoTypeAportationTable } from "./info-type-aportation-table";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onCreated?: (infoTypeAportation: IInfoTypeAportationEntity) => void;
  onUpdated?: (infoTypeAportation: IInfoTypeAportationEntity) => void;
}

export const InfoTypeAportationContent = ({
  isOpen,
  onClose,
  onCreated,
  onUpdated,
}: IProps) => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const { infoTypeAportations, option } = useSelector(
    (state: RootState) => state.infoTypeAportation
  );

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(30);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const infoTypeAportation = useInfoTypeAportationsToInfo(info.id || 0, {
    page,
    limit,
  });

  const handlePage = (newPage: number) => {
    setPage(newPage);
    setIsRefresh(true);
  };

  const handleRowPer = (newRowPer: number, currentPage: number) => {
    setLimit(newRowPer);
    setPage(currentPage);
    setIsRefresh(true);
  };

  const handleCreated = (
    createdInfoTypeAportation: IInfoTypeAportationEntity
  ) => {
    dispatch(infoTypeAportationActions.changeOption("REFRESH"));
    // actions
    if (typeof onCreated == "function") {
      onCreated(createdInfoTypeAportation);
    }
  };

  const handleEdit = (infoTypeAportation: IInfoTypeAportationEntity) => {
    dispatch(infoTypeAportationActions.find(infoTypeAportation));
    dispatch(infoTypeAportationActions.changeOption("EDIT"));
  };

  const handleDeleted = () => {
    dispatch(infoTypeAportationActions.changeOption("REFRESH"));
  };

  const handleUpdated = (
    updatedInfoTypeAportation: IInfoTypeAportationEntity
  ) => {
    dispatch(infoTypeAportationActions.updateItem(updatedInfoTypeAportation));
    dispatch(infoTypeAportationActions.changeOption("REFRESH"));
    // actions
    if (typeof onCreated == "function") {
      onCreated(updatedInfoTypeAportation);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setPage(1);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && info?.id) setIsRefresh(true);
  }, [info, isOpen]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(infoTypeAportationActions.changeOption(""));
    }
  }, [option]);

  useEffect(() => {
    if (isRefresh) infoTypeAportation.execute();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (infoTypeAportation.datos) {
      dispatch(infoTypeAportationActions.paginate(infoTypeAportation.datos));
    }
  }, [infoTypeAportation.datos]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        style={{ minWidth: "90vh" }}
        backdrop={!["CREATE", "EDIT"].includes(option)}>
        <ModalHeader toggle={onClose}>
          <Settings className="icon" /> Configurarción Aportaciones
        </ModalHeader>
        <ModalBody>
          <InfoTypeAportationTable
            perPage={infoTypeAportations?.meta?.itemsPerPage}
            totalItems={infoTypeAportations?.meta?.totalItems}
            data={infoTypeAportations?.items || []}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleRowPer}
            onDelete={handleDeleted}
            onClick={handleEdit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            outline
            onClick={() =>
              dispatch(infoTypeAportationActions.changeOption("CREATE"))
            }>
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* create */}
      <InfoTypeAportationCreate
        isOpen={option == "CREATE"}
        onClose={() => dispatch(infoTypeAportationActions.changeOption(""))}
        onSave={handleCreated}
      />
      {/* edit */}
      <InfoTypeAportationEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(infoTypeAportationActions.changeOption(""))}
        onSave={handleUpdated}
      />
    </>
  );
};
