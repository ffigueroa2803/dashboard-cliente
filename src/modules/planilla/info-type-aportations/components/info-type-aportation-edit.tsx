/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeAportationForm } from "./info-type-aportation-form";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { useInfotypeAportationForm } from "../hooks/use-info-type-aportation-form";
import { RootState } from "@store/store";
import { useInfoTypeAportationEdit } from "../hooks/use-info-type-aportation-edit";
import { useInfoTypeAportationFind } from "../hooks/use-info-type-aportation-find";
import { infoTypeAportationActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeAportationEntity) => void;
}

export const InfoTypeAportationEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { infoTypeAportation } = useSelector(
    (state: RootState) => state.infoTypeAportation
  );

  const infoForm = useInfotypeAportationForm();
  const infoEdit = useInfoTypeAportationEdit();
  const infoFind = useInfoTypeAportationFind();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await infoEdit
      .update(infoTypeAportation.id, infoForm.form)
      .then(async (data) => {
        const result = await infoFind
          .find(infoTypeAportation.id)
          .then((res) => res)
          .catch(() => data);
        // update info type remuneration
        dispatch(infoTypeAportationActions.find(result));
        if (typeof onSave == "function") onSave(result);
      })
      .catch(() => null);
    setPending(false);
  };

  useEffect(() => {
    if (infoTypeAportation?.id && isOpen) infoForm.setForm(infoTypeAportation);
  }, [infoTypeAportation, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Configuración Aportación
      </ModalHeader>
      <ModalBody>
        <InfoTypeAportationForm
          isEdit={true}
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
