import { IInputHandle } from "@common/dtos/input-handle";
import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IFormInfoTypeAportationDto } from "../dtos/form-info-type-aportation.dto";
import { TypeAportationSelect } from "@modules/planilla/type_aportacion/components/type-aportation-select";
import { DateTime } from "luxon";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  yearDefault?: number;
  onChange: (input: IInputHandle) => void;
}

const currentDate = DateTime.now();

export const InfoTypeAportationForm = ({
  form,
  isEdit,
  yearDefault,
  onChange,
}: IProps<IFormInfoTypeAportationDto>) => {
  const [year, setYear] = useState<number>(yearDefault || currentDate.year);

  useEffect(() => {
    if (yearDefault) setYear(yearDefault);
  }, [yearDefault]);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Aportación <b className="text-danger">*</b>
          </label>
          <div>
            <TypeAportationSelect
              isDisabled={isEdit}
              name="typeAportationId"
              value={form?.typeAportationId}
              onChange={onChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Año del Pim</label>
          <Input
            type="number"
            value={year}
            onChange={({ target }) => setYear(parseInt(`${target.value}`))}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Pim <b className="text-danger">*</b>
          </label>
          <div>
            <PimSelect
              year={year}
              name="pimId"
              value={form?.pimId || 0}
              onChange={onChange}
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
