/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeAportationForm } from "./info-type-aportation-form";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { useInfotypeAportationForm } from "../hooks/use-info-type-aportation-form";
import { useInfoTypeAportationCreate } from "../hooks/use-info-type-aportation-create";
import { RootState } from "@store/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeAportationEntity) => void;
}

export const InfoTypeAportationCreate = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const { info } = useSelector((state: RootState) => state.info);

  const infoForm = useInfotypeAportationForm();
  const infoCreate = useInfoTypeAportationCreate();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // payload
    const payload = Object.assign(infoForm.form, {
      infoId: info.id,
    });
    // send create contract
    await infoCreate
      .save(payload)
      .then((data) => {
        infoForm.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => null);
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Configuración Aportación</ModalHeader>
      <ModalBody>
        <InfoTypeAportationForm
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
