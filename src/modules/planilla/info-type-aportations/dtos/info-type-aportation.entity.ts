import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { TypeAportacion } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";

export interface IInfoTypeAportationEntity {
  id: number;
  typeAportationId: number;
  infoId: number;
  pimId: number;
  typeAportation?: TypeAportacion;
  pim?: IPimEntity;
}
