export interface ICreateInfoTypeAportationDto {
  infoId: number;
  typeAportationId: number;
  pimId: number;
}
