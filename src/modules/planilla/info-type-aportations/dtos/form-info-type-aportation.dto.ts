export interface IFormInfoTypeAportationDto {
  typeAportationId: number;
  infoId: number;
  pimId: number;
}
