import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { ICreateInfoTypeAportationDto } from "../dtos/create-info-type-aportation.dto";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useInfoTypeAportationCreate = () => {
  const [pending, setPending] = useState<boolean>(false);

  const save = (
    payload: ICreateInfoTypeAportationDto
  ): Promise<IInfoTypeAportationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`infoTypeAportations`, {
          ...payload,
        })
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data as IInfoTypeAportationEntity);
        })
        .catch((err) => {
          toast.error(`No se pudo guardar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    save,
  };
};
