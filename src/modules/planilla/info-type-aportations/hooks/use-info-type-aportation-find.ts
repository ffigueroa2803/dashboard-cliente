import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";

const request = planillaRequest();

export const useInfoTypeAportationFind = () => {
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IInfoTypeAportationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`infoTypeAportations/${id}`)
        .then((res) => {
          resolve(res.data as IInfoTypeAportationEntity);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
