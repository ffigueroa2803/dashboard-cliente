import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";

const request = planillaRequest();

export const useInfoTypeAportationDelete = () => {
  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async (id: number) => {
    return await confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en eliminar el regístro?",
        labelSuccess: "Eliminar",
        labelError: "No",
      })
      .then(async () => {
        toast.info(`Eliminando...`);
        setPending(true);
        await request
          .destroy(`infoTypeAportations/${id}`)
          .then((res) => {
            toast.dismiss();
            toast.success(`Los datos se eliminarón correctamente!`);
            return res.data;
          })
          .catch((err) => {
            toast.dismiss();
            toast.error(`No se pudo eliminar los datos`);
            return err;
          });
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    destroy,
  };
};
