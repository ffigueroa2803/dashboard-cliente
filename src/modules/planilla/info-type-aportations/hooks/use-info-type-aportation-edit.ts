import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";
import { toast } from "react-toastify";
import { IEditInfoTypeAportationDto } from "../dtos/edit-info-type-aportation.dto";

const request = planillaRequest();

export const useInfoTypeAportationEdit = () => {
  const [pending, setPending] = useState<boolean>(false);

  const update = (
    id: number,
    payload: IEditInfoTypeAportationDto
  ): Promise<IInfoTypeAportationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      setPending(true);
      await request
        .put(`infoTypeAportations/${id}`, {
          ...payload,
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IInfoTypeAportationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    update,
  };
};
