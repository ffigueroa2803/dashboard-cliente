import { IInputHandle } from "@common/dtos/input-handle";
import { useState } from "react";
import { IFormInfoTypeAportationDto } from "../dtos/form-info-type-aportation.dto";

const dataDefaultCurrent: IFormInfoTypeAportationDto = {
  typeAportationId: 0,
  infoId: 0,
  pimId: 0,
};

export const useInfotypeAportationForm = (
  dataDefault: IFormInfoTypeAportationDto = dataDefaultCurrent
) => {
  const [form, setForm] = useState<IFormInfoTypeAportationDto>(dataDefault);
  const [errors, setErrors] = useState<any>({});

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(dataDefault);

  return {
    dataDefault,
    form,
    setForm,
    errors,
    handleForm,
    clearForm,
  };
};
