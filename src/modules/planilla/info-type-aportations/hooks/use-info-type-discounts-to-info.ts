import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { PaginateDto } from "@services/dtos";
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAportationEntity } from "../dtos/info-type-aportation.entity";

const request = planillaRequest();

export const useInfoTypeAportationsToInfo = (
  id: number,
  { page, limit, querySearch }: PaginateDto
) => {
  const datosDefault: ResponsePaginateDto<IInfoTypeAportationEntity> = {
    items: [],
    meta: {
      itemsPerPage: limit || 30,
      totalItems: 0,
      totalPages: 0,
    },
  };

  const [pending, setPending] = useState<boolean>(false);
  const [datos, setDatos] =
    useState<ResponsePaginateDto<IInfoTypeAportationEntity>>(datosDefault);

  const execute = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("limit", `${limit}`);
    params.set("querySearch", querySearch || "");
    await request
      .get(`infos/${id}/typeAportations`, { params })
      .then((res) => setDatos(res.data))
      .catch(() => setDatos(datosDefault));
    setPending(false);
  };

  return {
    pending,
    execute,
    datos,
  };
};
