import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { AirhspEntity } from "../../domain/airhsp.entity";
import { AirhspPaginateParams } from "../../domain/airhsp.params";

const baseUrl = process.env.NEXT_PUBLIC_PLANILLA_URL || "";

export const airhspRtk = createApi({
  reducerPath: "airhspRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    paginateAirhsp: builder.query<PaginateEntity<AirhspEntity>, AirhspPaginateParams>({
      query: (params) => ({
        url: 'airhsp',
        params
      })
    })
  }),
});

export const { useLazyPaginateAirhspQuery } = airhspRtk;
