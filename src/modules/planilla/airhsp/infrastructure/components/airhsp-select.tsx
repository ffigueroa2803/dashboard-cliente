/* eslint-disable no-unused-vars */
import { SelectRemote } from "@common/select/select-remote";
import { AirhspEntity } from "../../domain/airhsp.entity";
import { AirhspPaginateParams } from "../../domain/airhsp.params";
import { useAirhspPaginate } from "../hooks/use-airhsp-paginate";

export interface AirhspSelectProps {
  isDisabled?: boolean;
  defaultQuerySearch?: string;
  name: string;
  value: any;
  query: AirhspPaginateParams;
  onChange: (option: any) => void;
}

export function AirhspSelect(props: AirhspSelectProps) {
  const hook = useAirhspPaginate();

  return (
    <SelectRemote
      {...props}
      handle={hook.handle}
      selectRow={{
        label: (row: AirhspEntity) => `${row.code} - ${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
}
