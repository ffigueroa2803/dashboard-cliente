import { toast } from "react-toastify";
import { AirhspPaginateParams } from "../../domain/airhsp.params";
import { useLazyPaginateAirhspQuery } from "../features/airhsp-rtk";

export function useAirhspPaginate() {
  const [fetch, { isLoading, isFetching }] = useLazyPaginateAirhspQuery();

  const handle = async (params: AirhspPaginateParams) => {
    return fetch(params)
      .unwrap()
      .catch((err) => toast.error(err?.data?.message || "Algo salío mal!!!"))
  }

  return {
    isLoading,
    isFetching,
    handle
  }
}