export interface AirhspEntity {
  id: string;
  code: string;
  name: string;
  description: string;
  level: number;
}