export interface AirhspPaginateParams {
  page: number;
  limit: number;
  level?: number;
}