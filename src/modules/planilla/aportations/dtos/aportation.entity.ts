import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { TypeAportacion } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";

export interface IAportationEntity {
  id: number;
  historialId: number;
  typeAportationId: number;
  pimId: number;
  percent: number;
  min: number;
  default: number;
  amount: number;
  typeAportation?: TypeAportacion;
  pim?: IPimEntity;
}

export const aportationEntityName = "AportationEntity";
