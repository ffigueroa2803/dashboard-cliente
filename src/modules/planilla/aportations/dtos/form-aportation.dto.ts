export interface IFormAportationDto {
  historialId: number;
  typeAportationId: number;
  pimId: number;
  amount: number;
}
