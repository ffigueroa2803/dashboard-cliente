import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IAportationEntity } from "./dtos/aportation.entity";

export interface AportationState {
  aportations: ResponsePaginateDto<IAportationEntity>;
  aportation: IAportationEntity;
  option: string;
}

const initialState: AportationState = {
  aportations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  aportation: {} as any,
  option: "",
};

const aportationStore = createSlice({
  name: "planilla@aportation",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IAportationEntity>>
    ) => {
      state.aportations = payload;
      return state;
    },
    removeItem: (state, { payload }: PayloadAction<number>) => {
      state.aportations.items = state.aportations?.items?.filter(
        (item) => item.id != payload
      );
      // change meta
      state.aportations.meta.totalItems =
        state.aportations.meta.totalItems || 0 - 1;
      // response
      return state;
    },
    find: (state, { payload }: PayloadAction<IAportationEntity>) => {
      state.aportation = payload;
      state.aportations.items?.map((item) => {
        if (payload.id == item.id) {
          return Object.assign(item, payload);
        }
        // default
        return item;
      });
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.aportation };
    },
  },
});

export const aportationReducer = aportationStore.reducer;

export const aportationActions = aportationStore.actions;
