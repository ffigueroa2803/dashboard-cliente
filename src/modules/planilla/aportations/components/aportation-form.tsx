import { IInputHandle } from "@common/dtos/input-handle";
import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { DateTime } from "luxon";
import { IFormAportationDto } from "../dtos/form-aportation.dto";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { TypeAportationSelect } from "@modules/planilla/type_aportacion/components/type-aportation-select";
import { format } from "currency-formatter";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";

interface IProps<T> {
  form: T;
  yearDefault?: number;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
  onSubmit?: () => void;
}

const currentDate = DateTime.now();

export const AportationForm = ({
  form,
  isEdit,
  yearDefault,
  onChange,
}: IProps<IFormAportationDto>) => {
  const { aportation } = useSelector((state: RootState) => state.aportation);

  const [year, setYear] = useState<number>(yearDefault || currentDate.year);

  useEffect(() => {
    if (yearDefault) setYear(yearDefault);
  }, [yearDefault]);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Aportación <b className="text-danger">*</b>
          </label>
          <div>
            <Show
              condition={!isEdit}
              isDefault={<h6>{aportation?.typeAportation?.name || "N/A"}</h6>}
            >
              <TypeAportationSelect
                isDisabled={isEdit}
                name="typeAportationId"
                value={form?.typeAportationId}
                onChange={onChange}
              />
            </Show>
          </div>
        </FormGroup>

        <Show condition={isEdit || false}>
          <FormGroup className="mb-3">
            <label>
              Monto <b className="text-danger">*</b>
            </label>
            <h6>{format(aportation.amount || 0, { code: "PEN" })}</h6>
          </FormGroup>
        </Show>

        <FormGroup className="mb-3">
          <label>Año del Pim</label>
          <Input
            type="number"
            value={year}
            onChange={({ target }) => setYear(parseInt(`${target.value}`))}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Pim</label>
          <div>
            <PimSelect
              year={year}
              name="pimId"
              value={form?.pimId || 0}
              onChange={onChange}
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
