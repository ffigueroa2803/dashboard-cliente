/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from "react";
import { Frown, Settings } from "react-feather";
import { Card, CardBody, CardHeader } from "reactstrap";
import { HistorialResume } from "@modules/planilla/historials/components/historial-resume";
import { AportationBlock } from "./aportation-block";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "@store/store";
import { useHistorialAportation } from "@modules/planilla/historials/hooks/use-historial-aportation";
import { AportationContext, AportationProvider } from "./aportation-context";
import { infoActions } from "@modules/planilla/infos/store";
import { toast } from "react-toastify";
import { Show } from "@common/show";
import { InfoTypeAportationContent } from "@modules/planilla/info-type-aportations/components/info-type-aportation-content";

export const WrapperAportationContext = () => {
  const dispatch = useDispatch();

  const historialAportation = useHistorialAportation();

  const { historial } = useSelector((state: RootState) => state.historial);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const { refreshResume } = useContext(AportationContext);

  const [isConfig, setIsConfig] = useState<boolean>(false);

  const handleConfig = () => {
    dispatch(infoActions.find(historial?.info || ({} as any)));
    setIsConfig(true);
  };

  const handleSaveConfig = () => {
    toast.dismiss();
    toast.info(`Calculando...`);
    setTimeout(async () => {
      await historialAportation.execute();
      refreshResume();
    }, 2000);
  };

  useEffect(() => {
    if (historial?.id) historialAportation.execute();
  }, [historial]);

  return (
    <>
      <Card>
        <CardBody>
          <HistorialResume />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Show condition={cronograma.state}>
            <Settings
              className="icon close cursor-pointer ml-3"
              onClick={handleConfig}
            />
          </Show>
          <Frown className="icon" /> Aportación
        </CardHeader>
        <CardBody>
          <AportationBlock loading={historialAportation.pending} />
        </CardBody>
      </Card>
      {/* config aportation */}
      <Show condition={isConfig}>
        <InfoTypeAportationContent
          isOpen={isConfig}
          onClose={() => setIsConfig(false)}
          onCreated={handleSaveConfig}
          onUpdated={handleSaveConfig}
        />
      </Show>
    </>
  );
};

export const AportationContent = () => {
  return (
    <AportationProvider>
      <WrapperAportationContext />
    </AportationProvider>
  );
};
