/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import currencyForrmater from "currency-formatter";
import { Show } from "@common/show";
import { Clock } from "react-feather";
import { Col } from "reactstrap";
import { useDispatch } from "react-redux";
import { IAportationEntity } from "../dtos/aportation.entity";
import { aportationActions } from "../store";
import { AportationEdit } from "./aportation-edit";

interface ItemProps {
  aportation: IAportationEntity;
  isEdit: boolean;
  onUpdate?: (aportation: IAportationEntity) => void;
  onDelete?: (aportation: IAportationEntity) => void;
}

export const AportationItem = ({
  aportation,
  isEdit,
  onUpdate,
  onDelete,
}: ItemProps) => {
  const dispatch = useDispatch();
  const [isDialog, setIsDialog] = useState<boolean>(false);

  const displayAmount = () => {
    return currencyForrmater.format(aportation.amount, { code: "PEN" });
  };

  const handleEdit = () => {
    if (!isEdit) return;
    dispatch(aportationActions.find(aportation));
    setIsDialog(true);
  };

  const isAmount = parseFloat(`${aportation.amount}`) > 0;

  return (
    <>
      <Col md="3" className="mb-4 text-ellipsis">
        {/* info discount */}
        <label
          className={`capitalize ${isAmount ? "" : "text-muted"}`}
          title={aportation?.typeAportation?.name}
        >
          <>
            <div className="text-left">
              <Show
                condition={!!aportation.typeAportation?.airhspId}
                isDefault={
                  <small className="badge badge-danger">SIN CODE</small>
                }
              >
                <small
                  className="badge badge-light"
                  title={`Codigo AIRHSP: ${aportation.typeAportation?.airhsp?.name}`}
                >
                  {aportation.typeAportation?.airhsp?.code || ""}
                </small>
              </Show>
              <small className="badge badge-warning">
                {aportation?.pim?.code} [{aportation?.pim?.cargo?.extension}]
              </small>
            </div>
            <span className="disabled-selection">
              <b className={isAmount ? "text-danger" : "text-muted"}>
                {aportation?.typeAportation?.code}
              </b>
              .- {aportation?.typeAportation?.name}
            </span>
          </>
        </label>
        <h6 onDoubleClick={handleEdit}>
          <Show
            condition={isAmount}
            isDefault={<span className="text-muted">{displayAmount()}</span>}
          >
            {displayAmount()}
          </Show>
          {/* action isEdit */}
          <span
            className="close"
            style={{ marginTop: "-3px" }}
            onClick={handleEdit}
          >
            <Clock
              className={
                isEdit ? "icon cursor-pointer text-danger" : "icon text-muted"
              }
            />
          </span>
        </h6>
      </Col>
      {/* dialog edit */}
      <Show condition={isDialog}>
        <AportationEdit
          isOpen={isDialog}
          onClose={() => setIsDialog(false)}
          onSave={onUpdate}
          onDeleted={onDelete}
        />
      </Show>
    </>
  );
};
