import { RootState } from "@store/store";
import React from "react";
import { Save, Trash } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IAportationEntity } from "../dtos/aportation.entity";
import { useAportationDelete } from "../hooks/use-aportation-delete";
import { useAportationEdit } from "../hooks/use-aportation-edit";
import { useAportationFind } from "../hooks/use-aportation-find";
import { AportationForm } from "./aportation-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (aportation: IAportationEntity) => void;
  onDeleted?: (aportation: IAportationEntity) => void;
}

export const AportationEdit = ({
  isOpen,
  onClose,
  onSave,
  onDeleted,
}: IProps) => {
  const aportationEdit = useAportationEdit();
  const aportationFind = useAportationFind();
  const aportationDelete = useAportationDelete();

  const { aportation } = useSelector((state: RootState) => state.aportation);

  const handleUpdate = () => {
    aportationEdit
      .update()
      .then(async () => {
        await aportationFind
          .fetch(aportation.id)
          .then((data) => {
            if (typeof onSave == "function") {
              onSave(data);
              aportationEdit.setForm(data);
            }
          })
          .catch(() => null);
      })
      .catch(() => null);
  };

  const handleDelete = () => {
    aportationDelete
      .destroy()
      .then(async () => {
        if (typeof onDeleted == "function") {
          onDeleted(aportation);
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Aportación</ModalHeader>
      <ModalBody>
        <AportationForm
          isEdit={true}
          form={aportationEdit.form}
          onChange={aportationEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          outline
          color="danger"
          disabled={aportationEdit.pending}
          onClick={handleDelete}
        >
          <Trash className="icon" />
        </Button>

        <Button
          color="primary"
          disabled={aportationEdit.pending}
          onClick={handleUpdate}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
