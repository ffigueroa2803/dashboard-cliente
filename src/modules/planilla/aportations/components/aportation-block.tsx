/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import React, { Fragment, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { AportationItem } from "./aportation-item";
import { AportationContext } from "./aportation-context";
import { IAportationEntity } from "../dtos/aportation.entity";
import { aportationActions } from "../store";
import Skeleton from "react-loading-skeleton";
import { Show } from "@common/show";

interface IProps {
  loading: boolean;
}

const PlaceholderItems = () => {
  const datos = [1, 2, 3, 4, 5];
  return (
    <>
      {datos.map((index) => (
        <Fragment key={`item-loading-${index}`}>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
        </Fragment>
      ))}
    </>
  );
};

export const AportationBlock = ({ loading }: IProps) => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { aportations } = useSelector((state: RootState) => state.aportation);

  const { isEdit, setIsEdit, refreshResume } = useContext(AportationContext);

  const handleOnDelete = (aportation: IAportationEntity) => {
    dispatch(aportationActions.removeItem(aportation.id));
  };

  useEffect(() => {
    if (cronograma?.id) setIsEdit(cronograma.state);
  }, [cronograma]);

  return (
    <Row>
      {/* loading */}
      <Show condition={!loading} isDefault={<PlaceholderItems />}>
        {/* items */}
        {aportations?.items?.map((apo, index) => (
          <AportationItem
            key={`item-aportation-${apo.id}-${index}`}
            isEdit={isEdit}
            aportation={apo}
            onUpdate={refreshResume}
            onDelete={handleOnDelete}
          />
        ))}
      </Show>
    </Row>
  );
};
