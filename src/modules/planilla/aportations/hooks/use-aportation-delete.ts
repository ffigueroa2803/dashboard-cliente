import { useState } from "react";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";

const request = planillaRequest();

export const useAportationDelete = () => {
  const { aportation } = useSelector((state: RootState) => state.aportation);

  const [pending, setPending] = useState<boolean>(false);

  const destroy = () => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Eliminando aportación...`);
      setPending(true);
      await request
        .destroy(`aportations/${aportation.id}`)
        .then((res) => {
          toast.dismiss();
          toast.success(`La aportación se eliminó correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo eliminar la aportación`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    destroy,
  };
};
