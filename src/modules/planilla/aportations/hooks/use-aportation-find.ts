import { useState } from "react";
import { useDispatch } from "react-redux";
import { planillaRequest } from "@services/planilla.request";
import { aportationActions } from "../store";
import { IAportationEntity } from "../dtos/aportation.entity";

const request = planillaRequest();

export const useAportationFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = (id: number): Promise<IAportationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`aportations/${id}`)
        .then((res) => {
          dispatch(aportationActions.find(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(aportationActions.find({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
  };
};
