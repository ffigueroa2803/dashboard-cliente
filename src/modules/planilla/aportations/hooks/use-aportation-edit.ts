import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { IFormAportationDto } from "../dtos/form-aportation.dto";
import { planillaRequest } from "@services/planilla.request";

const request = planillaRequest();

export const useAportationEdit = () => {
  const { aportation } = useSelector((state: RootState) => state.aportation);

  const dataDefault: IFormAportationDto = {
    historialId: aportation.historialId || 0,
    typeAportationId: aportation.typeAportationId || 0,
    pimId: aportation.pimId || 0,
    amount: aportation.amount || 0,
  };

  const [form, setForm] = useState<IFormAportationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = () => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando aportación...`);
      setPending(true);
      await request
        .put(`aportations/${aportation.id}`, {
          ...form,
          pimId: parseInt(`${form.pimId}`),
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los cambios se guardarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo guardar los cambios`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    form,
    setForm,
    changeForm,
    update,
  };
};
