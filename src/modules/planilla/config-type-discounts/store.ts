import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IConfigTypeDiscountEntity } from "./domain/config-type-discount.entity";

export interface ConfigTypeDiscountState {
  configTypeDiscounts: ResponsePaginateDto<IConfigTypeDiscountEntity>;
  configTypeDiscount: IConfigTypeDiscountEntity;
}

export const initialState: ConfigTypeDiscountState = {
  configTypeDiscounts: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  configTypeDiscount: {} as any,
};

const configTypeDiscountStore = createSlice({
  name: "planilla@configTypeDiscounts",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IConfigTypeDiscountEntity>>
    ) => {
      state.configTypeDiscounts = payload;
    },
    setConfigTypeDiscount: (
      state,
      { payload }: PayloadAction<IConfigTypeDiscountEntity>
    ) => {
      state.configTypeDiscount = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.configTypeDiscount };
    },
  },
});

export const configTypeDiscountReducer = configTypeDiscountStore.reducer;

export const configTypeDiscountActions = configTypeDiscountStore.actions;
