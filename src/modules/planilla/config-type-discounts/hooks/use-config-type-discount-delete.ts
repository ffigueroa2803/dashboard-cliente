/* eslint-disable no-async-promise-executor */
import { useConfirm } from "@common/confirm/use-confirm";
import { alerStatus } from "@common/errors/utils/alert-status";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useConfigTypeDiscountDelete = () => {
  const { configTypeDiscount } = useSelector(
    (state: RootState) => state.configTypeDiscount
  );

  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Eliminar",
          message: "Eliminar configuración de retención",
          labelError: "Cancelar",
          labelSuccess: "Elimnar",
        })
        .then(async () => {
          setPending(true);
          await request
            .destroy(`configTypeDiscounts/${configTypeDiscount.id}`)
            .then(({ data }) => {
              toast.success(`Los datos se guardarón correctamente!`);
              resolve(data);
            })
            .catch((err) => {
              const data = err?.response?.data;
              alerStatus(data?.status || 501, data?.message);
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return { pending, execute };
};
