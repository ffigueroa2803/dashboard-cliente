/* eslint-disable no-async-promise-executor */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";

const request = planillaRequest();

const defaultData = {
  planillaId: undefined,
  typeDiscountId: undefined,
  typeCategoryId: undefined,
  amount: 0,
};

export const useConfigTypeDiscountCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ConfigTypeDiscountRequest>(defaultData);

  const { planilla } = useSelector((state: RootState) => state.planilla);
  const { type_categoria } = useSelector(
    (state: RootState) => state.type_categoria
  );

  const onChange = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({ ...prev, [name]: value }));
  };

  const clearForm = () => {
    setForm({
      ...defaultData,
      planillaId: planilla?.id,
      typeCategoryId: type_categoria?.id,
    });
  };

  const execute = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`configTypeDiscounts`, {
          ...form,
          amount: parseInt(`${form.amount}`),
        })
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          const data = err?.response?.data;
          alerStatus(data?.status || 501, data?.message);
          reject(err);
        });
      setPending(false);
    });
  };

  const canExecute = (): boolean => {
    return form.typeDiscountId != undefined && !pending;
  };

  useEffect(() => {
    setForm({
      ...defaultData,
      typeCategoryId: type_categoria?.id,
      planillaId: planilla?.id,
    });
  }, []);

  return {
    form,
    setForm,
    pending,
    execute,
    onChange,
    clearForm,
    canExecute,
  };
};

export interface ConfigTypeDiscountRequest {
  planillaId: number | undefined;
  typeDiscountId: number | undefined;
  typeCategoryId: number | undefined;
  amount: number;
}
