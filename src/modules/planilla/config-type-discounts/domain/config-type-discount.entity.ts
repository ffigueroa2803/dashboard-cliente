import { IPlanillaEntity } from "@modules/planilla/planillas/dtos/planilla.entity";
import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";

export interface IConfigTypeDiscountEntity {
  id: number;
  amount: number;
  typeDiscount?: TypeDescuento;
  planilla?: IPlanillaEntity;
}
