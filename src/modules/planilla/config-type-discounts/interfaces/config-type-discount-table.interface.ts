import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { IConfigTypeDiscountEntity } from "../domain/config-type-discount.entity";

export interface IConfigTypeDiscountTableProvider {
  data: ResponsePaginateDto<IConfigTypeDiscountEntity>;
  fetch(): Promise<void>;
  pending: boolean;
  isError: boolean;
}
