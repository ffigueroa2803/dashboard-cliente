/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { format } from "currency-formatter";
import { useEffect, useState } from "react";
import { Edit, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IConfigTypeDiscountEntity } from "../domain/config-type-discount.entity";
import { useConfigTypeDiscountDelete } from "../hooks/use-config-type-discount-delete";
import { configTypeDiscountActions } from "../store";
import { ConfigTypeDiscountEdit } from "./config-type-discount-edit";

interface IProps {
  configTypeDiscount: IConfigTypeDiscountEntity;
  onEdit?(): void;
  onDelete?(): void;
}

export const ConfigTypeDiscountItem = ({
  configTypeDiscount,
  onEdit,
  onDelete,
}: IProps) => {
  const dispatch = useDispatch();

  const [event, setEvent] = useState<string>("");
  const [openEdit, setOpenEdit] = useState<boolean>(false);
  const { dark } = useSelector((state: RootState) => state.screen);

  const configTypeRemunerationDelete = useConfigTypeDiscountDelete();

  const dispatchItem = (e: any, event: string) => {
    e.preventDefault();
    setEvent(event);
    dispatch(
      configTypeDiscountActions.setConfigTypeDiscount(configTypeDiscount)
    );
  };

  const handleEdit = () => setOpenEdit(true);

  const handleDelete = () => {
    configTypeRemunerationDelete
      .execute()
      .then(() => (typeof onDelete == "function" ? onDelete() : null))
      .catch(() => null);
  };

  const handleUpdate = () => {
    if (typeof onEdit === "function") onEdit();
  };

  useEffect(() => {
    if (event === "edit") handleEdit();
    if (event === "delete") handleDelete();
    return () => setEvent("");
  }, [event]);

  return (
    <>
      <tr>
        <td>
          <h6 className={dark ? "text-white" : ""}>
            {configTypeDiscount?.typeDiscount?.code}
          </h6>
          <p className={dark ? "text-white" : ""}>
            {configTypeDiscount?.typeDiscount?.description}
          </p>
        </td>
        <td>
          <h6
            className="badge badge-warning badge-lg"
            style={{ fontSize: "13px" }}
          >
            {format(configTypeDiscount.amount, { code: "PEN" })}
          </h6>
        </td>
        <td width={50}>
          <a className="me-2" href="#" onClick={(e) => dispatchItem(e, "edit")}>
            <Edit className="icon" />
          </a>
        </td>
        <td width={50}>
          <a
            className="me-2"
            href="#"
            onClick={(e) => dispatchItem(e, "delete")}
          >
            <Trash className="icon" />
          </a>
        </td>
      </tr>
      {/* edit */}
      <Show condition={openEdit}>
        <ConfigTypeDiscountEdit
          onClose={() => setOpenEdit(false)}
          open={true}
          onSave={handleUpdate}
        />
      </Show>
    </>
  );
};
