/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IConfigTypeDiscountTableProvider } from "../interfaces/config-type-discount-table.interface";
import Skeleton from "react-loading-skeleton";
import { Col, Row } from "reactstrap";
import { Show } from "@common/show";
import { ConfigTypeDiscountItem } from "./config-type-discount-item";

interface IProps {
  provider: IConfigTypeDiscountTableProvider;
}

const LoadingComponent = () => (
  <>
    <Col md="6">
      <Skeleton height={30} />
    </Col>
    <Col md="4">
      <Skeleton height={30} />
    </Col>
    <Col md="2">
      <Skeleton height={30} />
    </Col>
  </>
);

const ConfigTypeDiscountTable = ({ provider }: IProps) => {
  const { planilla } = useSelector((state: RootState) => state.planilla);

  const { data, fetch, pending } = provider;

  const handleEdit = () => fetch();

  const handleDelete = () => fetch();

  useEffect(() => {
    if (planilla?.id) fetch();
  }, [planilla]);

  return (
    <>
      {/* cargar */}
      <Show condition={pending}>
        <Row>
          <LoadingComponent />
          <LoadingComponent />
          <LoadingComponent />
        </Row>
      </Show>
      {/* list */}
      <div className="table-responsive">
        <table className="table">
          <tbody>
            {data.items?.map((item) => (
              <ConfigTypeDiscountItem
                onEdit={handleEdit}
                onDelete={handleDelete}
                configTypeDiscount={item}
                key={`configTypeRemuneration-config-type-remuneration-${item.id}`}
              />
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ConfigTypeDiscountTable;
