/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { RootState } from "@store/store";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  Col,
  Row,
  FormGroup,
  Input,
  Form,
} from "reactstrap";
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { useConfigTypeDiscountEdit } from "../hooks/use-config-type-discount-edit";

interface Iprops {
  onSave: () => void;
  open: boolean;
  onClose: () => void;
}

export const ConfigTypeDiscountEdit = ({ open, onSave, onClose }: Iprops) => {
  const { configTypeDiscount } = useSelector(
    (state: RootState) => state.configTypeDiscount
  );
  const { dark } = useSelector((state: RootState) => state.screen);
  const { form, setForm, onChange, execute, pending, canExecute } =
    useConfigTypeDiscountEdit();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await execute()
      .then(() => onSave())
      .catch(() => null);
  };

  useEffect(() => {
    setForm({ amount: configTypeDiscount.amount });
  }, []);

  return (
    <Modal isOpen={open} style={{ marginTop: "6rem" }}>
      <ModalHeader toggle={onClose}>
        Crear Config. Tip. Remuneración
      </ModalHeader>
      <ModalBody>
        <Form onSubmit={handleSubmit}>
          <FormGroup className="mb-4">
            <label>Planilla</label>
            <h6>{configTypeDiscount?.planilla?.name || "N/A"}</h6>
          </FormGroup>

          <FormGroup>
            <label>
              Tip. Remuneración <b className="text-danger">*</b>
            </label>
            <h6>{configTypeDiscount?.typeDiscount?.description || "N/A"}</h6>
          </FormGroup>

          <FormGroup>
            <label>
              Monto <b className="text-danger">*</b>
            </label>
            <Input
              name="amount"
              type="number"
              style={{
                opacity: "1",
                backgroundColor: dark ? "" : "#FFF",
              }}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.value })
              }
              placeholder="Ingrese el monto"
              value={form?.amount || ""}
            />
          </FormGroup>

          <Row className="justify-content-center">
            <Col md="6 col-6 text-left"></Col>
            <Col md="6 col-6 text-right">
              <Show
                condition={!pending}
                isDefault={
                  <ButtonLoading
                    title="guardando..."
                    loading={true}
                    color="primary"
                  />
                }
              >
                <Button
                  color="primary"
                  title="Guardar datos"
                  disabled={!canExecute()}
                >
                  <Save className="icon" />
                </Button>
              </Show>
            </Col>
          </Row>
        </Form>
      </ModalBody>
    </Modal>
  );
};
