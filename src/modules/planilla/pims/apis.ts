import { planillaRequest } from "@services/planilla.request";
import { IFilterGetPimsDto } from "./dtos/filter-pims.dto";

const request = planillaRequest();

export const getPims = async ({
  page,
  querySearch,
  limit,
  year,
}: IFilterGetPimsDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  params.set("year", `${year}`);
  return await request.get(`pims`, { params }).then((res) => res.data);
};
