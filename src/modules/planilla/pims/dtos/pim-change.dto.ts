export enum pimChangeModeEnum {
  ENTRY = "ENTRY",
  EXIT = "EXIT",
}

export interface IPimChangeDto {
  pimId: number;
  amount: number;
  mode: pimChangeModeEnum;
  observation?: string;
}
