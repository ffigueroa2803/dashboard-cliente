export interface IPimEditDto {
  code: string;
  metaId: number;
  cargoId: number;
  amount: number;
  state: boolean;
}
