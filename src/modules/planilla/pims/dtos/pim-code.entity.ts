export interface IPimCodeEntity {
  pliegoId: string;
  pliego: string;
  unidadId: string;
  unidad: string;
  programaId: string;
  programa: string;
  functionId: string;
  function: string;
  subProgramaId: string;
  subPrograma: string;
  actividadId: string;
  actividad: string;
  code: string;
  year: number;
  metaId: number;
  name: string;
  sectorId: string;
  sector: string;
}
