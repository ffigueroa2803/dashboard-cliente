import { ICargoEntity } from "@modules/planilla/cargos/dtos/cargo.entity";
import { IMetaEntity } from "@modules/planilla/metas/dtos/meta.entity";

export interface IPimEntity {
  id: number;
  code: string;
  metaId: number;
  cargoId: number;
  year: number;
  amount: number;
  executedAmount: number;
  diffAmount: number;
  state: boolean;
  meta?: IMetaEntity;
  cargo?: ICargoEntity;
}

export const pimEntityName = "PimEntity";
