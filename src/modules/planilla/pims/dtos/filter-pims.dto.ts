import { PaginateDto } from "@services/dtos";

export interface IFilterGetPimsDto extends PaginateDto {
  year: number;
}
