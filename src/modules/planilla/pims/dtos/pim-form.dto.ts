export interface IPimFormDto {
  code: string;
  metaId: number;
  cargoId: number;
  year: number;
  amount: number;
}
