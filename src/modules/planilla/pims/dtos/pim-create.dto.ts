export interface IPimCreateDto {
  code: string;
  metaId: number;
  cargoId: number;
  year: number;
  amount: number;
}
