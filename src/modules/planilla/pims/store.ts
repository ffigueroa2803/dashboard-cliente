import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IPimCodeEntity } from "./dtos/pim-code.entity";
import { IPimEntity } from "./dtos/pim.entity";

export interface PimState {
  pims: ResponsePaginateDto<IPimEntity>;
  pim: IPimEntity;
  pimCodes: ResponsePaginateDto<IPimCodeEntity>;
  pimCode: IPimCodeEntity;
  option: string;
}

export const initialState: PimState = {
  pims: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  pim: {} as any,
  pimCodes: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  pimCode: {} as any,
  option: "",
};

const pimStore = createSlice({
  name: "planilla@pims",
  initialState,
  reducers: {
    paginate: (
      state: PimState,
      { payload }: PayloadAction<ResponsePaginateDto<IPimEntity>>
    ) => {
      state.pims = payload;
      return state;
    },
    updateItem: (state: PimState, { payload }: PayloadAction<IPimEntity>) => {
      state.pims?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
      // response
      return state;
    },
    find: (state: PimState, { payload }: PayloadAction<IPimEntity>) => {
      state.pim = payload;
      return state;
    },
    paginateCode: (
      state: PimState,
      { payload }: PayloadAction<ResponsePaginateDto<IPimCodeEntity>>
    ) => {
      state.pimCodes = payload;
      return state;
    },
    findCode: (state: PimState, { payload }: PayloadAction<IPimCodeEntity>) => {
      state.pimCode = payload;
      return state;
    },
    changeOption: (state: PimState, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.pim };
    },
  },
});

export const pimReducer = pimStore.reducer;

export const pimActions = pimStore.actions;
