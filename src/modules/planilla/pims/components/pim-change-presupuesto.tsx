/* eslint-disable no-unused-vars */
import { ButtonLoading } from "@common/button/button-loading";
import { Dropzone } from "@common/dropzone";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { format } from "currency-formatter";
import { useEffect, useMemo } from "react";
import { Save, Settings } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Col,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import { pimChangeModeEnum } from "../dtos/pim-change.dto";
import { IPimEntity } from "../dtos/pim.entity";
import { usePimChangePresupuesto } from "../hooks/use-pim-change-presupuesto";

interface IProps {
  isOpen: boolean;
  pimType: pimChangeModeEnum;
  onClose?: () => void;
  onSave?: (pim: IPimEntity) => void;
}

export const PimChangePresupuesto = ({
  isOpen,
  pimType,
  onClose,
  onSave,
}: IProps) => {
  const { pim } = useSelector((state: RootState) => state.pim);

  const { form, files, setFiles, changeForm, pending, save } =
    usePimChangePresupuesto();

  const handleSave = () => {
    save()
      .then((data) => {
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch(() => null);
  };

  const displayAmount = useMemo(() => {
    if (pimType == pimChangeModeEnum.ENTRY) {
      return parseFloat(`${pim.amount}`) + parseFloat(`${form.amount}`);
    }
    // default
    return parseFloat(`${pim.amount}`) - parseFloat(`${form.amount}`);
  }, [pim, form]);

  const displayDiffAmount = useMemo(() => {
    if (pimType == pimChangeModeEnum.ENTRY) {
      return parseFloat(`${pim.diffAmount}`) + parseFloat(`${form.amount}`);
    }
    // default
    return parseFloat(`${pim.diffAmount}`) - parseFloat(`${form.amount}`);
  }, [pim, form]);

  useEffect(() => {
    changeForm({ name: "mode", value: pimType });
  }, [pimType]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        {pimType == "ENTRY" ? "Aumentar" : "Disminuir"} Presupesto
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col md="6">
            <label>Monto Actual</label>
            <h6>{format(displayAmount || 0, { code: "PEN" })}</h6>
          </Col>
          <Col md="6">
            <label>Saldo</label>
            <h6>{format(displayDiffAmount || 0, { code: "PEN" })}</h6>
          </Col>
          <Col md="12">
            <h6>
              <hr />
              <Settings className="icon" /> Editar Presupuesto
              <hr />
            </h6>
          </Col>

          <Col md="12 mb-3">
            <label htmlFor="">
              {pimType == "ENTRY" ? "Aumentar" : "Disminuir"} Presupuesto
            </label>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) => changeForm(target)}
            />
          </Col>

          <Col md="12 mb-3">
            <label htmlFor="">Observación</label>
            <Input
              type="textarea"
              name="observation"
              value={form?.observation || ""}
              onChange={({ target }) => changeForm(target)}
            />
          </Col>

          <Col md="12">
            <label htmlFor="">Archivo</label>
            <Dropzone
              multiple={false}
              onFiles={setFiles}
              files={files}
              accept="application/pdf"
            />
          </Col>
        </Row>
      </ModalBody>
      <ModalFooter className="text-right">
        <Show
          condition={!pending}
          isDefault={
            <ButtonLoading
              loading={true}
              title="Actualizando..."
              color="primary"
            />
          }>
          <Button color="primary" onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </Show>
      </ModalFooter>
    </Modal>
  );
};
