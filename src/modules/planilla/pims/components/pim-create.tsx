/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { useContext, useEffect } from "react";
import { Save } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IPimEntity } from "../dtos/pim.entity";
import { usePimCreate } from "../hooks/use-pim-create";
import { PimContext } from "./pim-context";
import { PimForm } from "./pim-form";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onSave?: (pim: IPimEntity) => void;
}

export const PimCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const pimCreate = usePimCreate();

  const pimContext = useContext(PimContext);

  const handleSave = () => {
    pimCreate
      .save()
      .then((data) => {
        pimCreate.clearForm();
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch(() => null);
  };

  useEffect(() => {
    pimCreate.changeForm({ name: "year", value: pimContext.year });
  }, []);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nuevo Pim Anual</ModalHeader>
      <ModalBody>
        <PimForm form={pimCreate.form} onChange={pimCreate.changeForm} />
      </ModalBody>
      <ModalFooter className="text-right">
        <Show
          condition={!pimCreate.pending}
          isDefault={
            <ButtonLoading
              loading={true}
              title="Guardando..."
              color="success"
            />
          }>
          <Button color="success" onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </Show>
      </ModalFooter>
    </Modal>
  );
};
