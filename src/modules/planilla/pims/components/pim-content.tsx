import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { usePimList } from "../hooks/use-pim-list";
import { PimContext, pimContextEnum, PimProvider } from "./pim-context";
import { PimTable } from "./pim-table";
import { useRouter } from "next/router";
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { pimEntityName } from "../dtos/pim.entity";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { FloatButton } from "@common/button/float-button";
import { Clock, Plus } from "react-feather";
import { PimCreate } from "./pim-create";
import { PimEdit } from "./pim-edit";
import { usePimCalc } from "../hooks/use-pim-calc";
import { ButtonLoading } from "@common/button/button-loading";
import { PimChangePresupuesto } from "./pim-change-presupuesto";
import { pimChangeModeEnum } from "../dtos/pim-change.dto";

const PimWrapper = () => {
  const pimContext = useContext(PimContext);
  const ability = useContext(CaslContext);

  const pimCalc = usePimCalc();

  const pimList = usePimList({
    page: 1,
    limit: 30,
    year: pimContext.year,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(false);

  const changePage = (page: number) => {
    pimList.changePim({ name: "page", value: page });
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    pimList.changePim({ name: "limit", value: limit });
    setIsFetch(true);
  };

  const handleSave = () => {
    setIsFetch(true);
    pimContext.setOptions(pimContextEnum.DEFAULT);
  };

  const handleCalc = () => {
    pimCalc
      .execute()
      .then(() => setIsFetch(true))
      .catch(() => null);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    pimList.changePim({ value: pimContext.year, name: "year" });
    if (`${pimContext.year}`.length == 4) setIsFetch(true);
  }, [pimContext.year]);

  useEffect(() => {
    if (isFetch) pimList.fetch().catch(() => null);
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form className="mb-4">
            <Row>
              <Col md="5">
                <Input
                  type="number"
                  name="querySearch"
                  value={pimContext.year}
                  disabled={pimList.pending}
                  onChange={({ target }) =>
                    pimContext.setYear(parseInt(`${target.value}`))
                  }
                />
              </Col>
              <Col md="3">
                <Show
                  condition={!pimCalc.pending}
                  isDefault={
                    <ButtonLoading
                      title="Calculando..."
                      loading={true}
                      color="dark"
                    />
                  }
                >
                  <Button color="dark" onClick={handleCalc}>
                    <Clock className="icon" />
                  </Button>
                </Show>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <PimTable
            loading={pimList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* crear pim */}
      <Show condition={ability.can(PermissionAction.CREATE, pimEntityName)}>
        <FloatButton
          color="success"
          icon={<Plus />}
          onClick={() => pimContext.setOptions(pimContextEnum.CREATE)}
        />
        <PimCreate
          isOpen={pimContext.options == pimContextEnum.CREATE}
          onClose={() => pimContext.setOptions(pimContextEnum.DEFAULT)}
          onSave={handleSave}
        />
      </Show>
      {/* editar */}
      <PimEdit
        isOpen={pimContext.options == pimContextEnum.EDIT}
        onClose={() => pimContext.setOptions(pimContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* editar presupuesto */}
      <Show condition={ability.can(PermissionAction.UPDATE, pimEntityName)}>
        {/* aumentar */}
        <PimChangePresupuesto
          pimType={pimChangeModeEnum.ENTRY}
          isOpen={pimContext.options == pimContextEnum.PLUS}
          onClose={() => pimContext.setOptions(pimContextEnum.DEFAULT)}
          onSave={handleSave}
        />
        {/* diminuir */}
        <PimChangePresupuesto
          pimType={pimChangeModeEnum.EXIT}
          isOpen={pimContext.options == pimContextEnum.MINUS}
          onClose={() => pimContext.setOptions(pimContextEnum.DEFAULT)}
          onSave={handleSave}
        />
      </Show>
    </>
  );
};

export const PimContent = () => {
  return (
    <PimProvider>
      <PimWrapper />
    </PimProvider>
  );
};
