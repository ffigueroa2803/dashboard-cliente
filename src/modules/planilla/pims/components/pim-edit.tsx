import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { Save } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IPimEntity } from "../dtos/pim.entity";
import { usePimEdit } from "../hooks/use-pim-edit";
import { PimForm } from "./pim-form";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onSave?: (pim: IPimEntity) => void;
}

export const PimEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const pimEdit = usePimEdit();

  const handleSave = () => {
    pimEdit
      .save()
      .then((data) => {
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Pim Anual</ModalHeader>
      <ModalBody>
        <PimForm
          form={pimEdit.form}
          onChange={pimEdit.changeForm}
          disableds={pimEdit.disabled}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Show
          condition={!pimEdit.pending}
          isDefault={
            <ButtonLoading
              loading={true}
              title="Actualizando..."
              color="primary"
            />
          }
        >
          <Button color="primary" onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </Show>
      </ModalFooter>
    </Modal>
  );
};
