import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { RootState } from "@store/store";
import { format } from "currency-formatter";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit, Minus, Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IPimEntity, pimEntityName } from "../dtos/pim.entity";
import { pimActions } from "../store";
import { PimContext, pimContextEnum } from "./pim-context";

interface IProps {
  onClick?: (pim: IPimEntity, action: pimContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  loading: boolean;
}

export const PimTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { pims } = useSelector((state: RootState) => state.pim);

  const pimContext = useContext(PimContext);
  const ability = useContext(CaslContext);

  const handleClick = (pim: IPimEntity, action: pimContextEnum) => {
    dispatch(pimActions.find(pim));
    pimContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(pim, action);
    }
  };

  const columns = useMemo(() => {
    const options = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IPimEntity) => row.id,
      },
      {
        name: "Código",
        grow: true,
        selector: (row: IPimEntity) => row.code,
      },
      {
        name: "Meta",
        wrap: true,
        selector: (row: IPimEntity) => row?.meta?.name || "",
      },
      {
        name: "Partición Presp",
        wrap: true,
        selector: (row: IPimEntity) => row?.cargo?.name || "",
      },
      {
        name: "Extensión Presp",
        wrap: true,
        center: true,
        selector: (row: IPimEntity) => (
          <span className="badge badge-dark">
            {row?.cargo?.extension || ""}
          </span>
        ),
      },
      {
        name: "Año",
        wrap: true,
        center: true,
        selector: (row: IPimEntity) => row?.year || "",
      },
      {
        name: "Monto",
        wrap: true,
        right: true,
        selector: (row: IPimEntity) => format(row.amount, { code: "PEN" }),
      },
      {
        name: "Ejecutado",
        wrap: true,
        right: true,
        selector: (row: IPimEntity) =>
          format(row.executedAmount, { code: "PEN" }),
      },
      {
        name: "Saldo",
        wrap: true,
        center: true,
        right: true,
        selector: (row: IPimEntity) => (
          <span className={row.diffAmount <= 0 ? "text-danger" : ""}>
            {format(row.diffAmount, { code: "PEN" })}
          </span>
        ),
      },
    ];

    if (ability.can(PermissionAction.UPDATE, pimEntityName)) {
      options.push({
        name: "Opciones",
        wrap: true,
        center: true,
        right: true,
        selector: (row: IPimEntity) => (
          <div>
            <Plus
              className="icon cursor-pointer text-success"
              onClick={() => handleClick(row, pimContextEnum.PLUS)}
            />
            <Minus
              className="icon ml-2 cursor-pointer text-danger"
              onClick={() => handleClick(row, pimContextEnum.MINUS)}
            />
            <Edit
              className="icon ml-2 cursor-pointer"
              onClick={() => handleClick(row, pimContextEnum.EDIT)}
            />
          </div>
        ),
      });
    }

    return options;
  }, [pims.items]);

  return (
    <>
      <Datatable
        data={pims.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={pims?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={pims?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
