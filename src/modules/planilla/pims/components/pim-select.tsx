/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { usePimList } from "../hooks/use-pim-list";
import { IPimEntity } from "../dtos/pim.entity";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  year: number;
  defaultQuerySearch?: string;
}

export const PimSelect = ({
  name,
  value,
  year,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const pimList = usePimList();

  const { pims } = useSelector((state: RootState) => state.pim);

  const settingsData = (row: IPimEntity) => {
    return {
      label: `${row.code} - ${row.cargo?.name || ""} [ ${
        row.cargo?.extension || ""
      } ] - ${row.year}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  const handleSearch = (value: string) => {
    if (!value) return;
    pimList.changePim({ name: "querySearch", value: value });
  };

  useEffect(() => {
    pimList.changePim({ value: defaultQuerySearch, name: "querySearch" });
  }, [defaultQuerySearch]);

  useEffect(() => {
    pimList.changePim({ value: year, name: "year" });
  }, [year]);

  useEffect(() => {
    if (`${pimList.query?.year}`.length == 4) {
      pimList.fetch().catch(() => null);
    }
  }, [pimList.query]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={pims?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={handleSearch}
    />
  );
};
