import { DateTime } from "luxon";
import { createContext, FC, useState } from "react";

const currentDate = DateTime.now();

export enum pimContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  PLUS = "PLUS",
  MINUS = "MINUS",
}

export const PimContext = createContext({
  year: 0,
  setYear: (value: number) => {},
  options: "",
  setOptions: (value: pimContextEnum) => {},
});

export const PimProvider: FC = ({ children }) => {
  const [year, setYear] = useState<number>(currentDate.year);
  const [options, setOptions] = useState<pimContextEnum>(
    pimContextEnum.DEFAULT
  );

  return (
    <PimContext.Provider
      value={{
        year,
        setYear,
        options,
        setOptions,
      }}
    >
      {children}
    </PimContext.Provider>
  );
};
