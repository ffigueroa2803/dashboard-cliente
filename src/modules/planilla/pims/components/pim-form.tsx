/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { CargoSelect } from "@modules/planilla/cargos/components/cargo-select";
import { MetaSelect } from "@modules/planilla/metas/components/meta-select";
import { Form, FormGroup, Input } from "reactstrap";

interface IProps {
  form: any;
  onChange: (input: IInputHandle) => void;
  disableds?: string[];
}

export const PimForm = ({ form, onChange, disableds = [] }: IProps) => {
  const handleChange = ({ name, value }: IInputHandle) => {
    if (typeof onChange == "function") {
      onChange({ name, value });
    }
  };

  return (
    <Form>
      <FormGroup className="mb-3">
        <label>
          Código <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="code"
          value={form?.code || ""}
          onChange={({ target }) => handleChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Meta <b className="text-danger">*</b>
        </label>
        <MetaSelect
          name="metaId"
          value={form?.metaId || ""}
          onChange={(target) => handleChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Partición Presp <b className="text-danger">*</b>
        </label>
        <CargoSelect
          name="cargoId"
          value={form?.cargoId || ""}
          onChange={(target) => handleChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Año <b className="text-danger">*</b>
        </label>
        <Input
          type="number"
          name="year"
          disabled={disableds?.includes("year")}
          value={form?.year || ""}
          onChange={({ target }) => handleChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Monto <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="amount"
          disabled={disableds?.includes("amount")}
          value={form?.amount || ""}
          onChange={({ target }) => handleChange(target)}
        />
      </FormGroup>
    </Form>
  );
};
