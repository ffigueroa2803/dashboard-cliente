import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IFilterGetPimsDto } from "../dtos/filter-pims.dto";
import { useDispatch } from "react-redux";
import { initialState, pimActions } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";

const request = planillaRequest();

export const usePimList = (dataDefault?: IFilterGetPimsDto) => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [query, setQuery] = useState<IFilterGetPimsDto>({
    page: dataDefault?.page || 1,
    limit: dataDefault?.limit || 100,
    year: dataDefault?.year || 0,
  });

  const changePim = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("querySearch", query.querySearch || "");
      params.set("limit", `${query.limit || 30}`);
      params.set("year", `${query.year}`);
      await request
        .get(`pims`, { params })
        .then((res) => {
          dispatch(pimActions.paginate(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(pimActions.paginate(initialState.pims));
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    query,
    setQuery,
    changePim,
    pending,
    fetch,
  };
};
