import { useEffect, useMemo, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { IPimEntity } from "../dtos/pim.entity";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IPimEditDto } from "../dtos/pim-edit.dto";

const request = planillaRequest();

export const usePimEdit = () => {
  const { pim } = useSelector((state: RootState) => state.pim);

  const defaultData = {
    code: pim.code,
    metaId: pim.metaId || 0,
    cargoId: pim.cargoId || 0,
    year: pim.year || 0,
    amount: pim.amount || 0,
    state: pim.state || false,
  };

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IPimEditDto>(defaultData);

  const disabled = useMemo(() => {
    const condition: string[] = ["year"];
    // verificar fidd
    if (pim.executedAmount > 0) condition.push("amount");
    // response
    return condition;
  }, [form]);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(defaultData);

  const save = (): Promise<IPimEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const payload: IPimEditDto = Object.assign(form, {
        metaId: parseInt(`${form.metaId}`),
        cargoId: parseInt(`${form.cargoId}`),
        amount: parseFloat(`${form.amount}`),
        state: JSON.parse(`${form.state}`),
      });
      // send
      await request
        .put(`pims/${pim.id}`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los cambios se guardarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  useEffect(() => {
    if (pim.id) clearForm();
  }, [pim]);

  return {
    disabled,
    form,
    setForm,
    changeForm,
    clearForm,
    pending,
    save,
  };
};
