import { useEffect, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { IPimEntity } from "../dtos/pim.entity";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IPimChangeDto, pimChangeModeEnum } from "../dtos/pim-change.dto";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

export const usePimChangePresupuesto = () => {
  const { pim } = useSelector((state: RootState) => state.pim);

  const [pending, setPending] = useState<boolean>(false);

  const defaultData = {
    pimId: pim.id,
    amount: 0,
    mode: pimChangeModeEnum.ENTRY,
  };

  const [form, setForm] = useState<IPimChangeDto>(defaultData);
  const [files, setFiles] = useState<File[]>([]);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => {
    setForm(defaultData);
    setFiles([]);
  };

  const save = (): Promise<IPimEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const obj: any = Object.assign({}, form);
      const payload = new FormData();
      Object.keys(obj).forEach((key) => payload.set(key, `${obj[key]}`));
      // add file
      if (files.length) {
        payload.set("file", files[0]);
      }
      // send
      await request
        .post(`pimLogs`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los cambios se guardarón correctamente!`);
          clearForm();
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  useEffect(() => {
    if (pim.id) clearForm();
  }, [pim]);

  return {
    pending,
    files,
    form,
    changeForm,
    clearForm,
    setFiles,
    setForm,
    save,
  };
};
