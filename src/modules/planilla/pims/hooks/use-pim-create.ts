import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { IPimCreateDto } from "../dtos/pim-create.dto";
import { IPimFormDto } from "../dtos/pim-form.dto";
import { IPimEntity } from "../dtos/pim.entity";

const request = planillaRequest();

const defaultData = {
  code: "",
  metaId: 0,
  cargoId: 0,
  year: 0,
  amount: 0,
};

export const usePimCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IPimFormDto>(defaultData);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(defaultData);

  const save = (): Promise<IPimEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const payload: IPimCreateDto = Object.assign(form, {
        metaId: parseInt(`${form.metaId}`),
        cargoId: parseInt(`${form.cargoId}`),
        year: parseInt(`${form.year}`),
        amount: parseFloat(`${form.amount}`),
      });
      // send
      await request
        .post(`pims`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`El PIM Anual se guardó correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    form,
    setForm,
    changeForm,
    clearForm,
    pending,
    save,
  };
};
