import { useContext, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { IPimCreateDto } from "../dtos/pim-create.dto";
import { IPimFormDto } from "../dtos/pim-form.dto";
import { IPimEntity } from "../dtos/pim.entity";
import { PimContext } from "../components/pim-context";

const request = planillaRequest();

const defaultData = {
  code: "",
  metaId: 0,
  cargoId: 0,
  year: 0,
  amount: 0,
};

export const usePimCalc = () => {
  const pimContext = useContext(PimContext);

  const [pending, setPending] = useState<boolean>(false);

  const execute = (): Promise<IPimEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`pims/${pimContext.year}/calcExecuted`)
        .then((res) => {
          toast.dismiss();
          toast.success(`El PIM anual se calculó correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    execute,
  };
};
