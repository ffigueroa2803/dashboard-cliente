/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch } from "react-redux";
import { initialState, labelActions } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { PaginateDto } from "@services/dtos";

const request = planillaRequest();

export const useLabelList = (dataDefault?: PaginateDto) => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [query, setQuery] = useState<PaginateDto>({
    page: dataDefault?.page || 1,
    limit: dataDefault?.limit || 100,
  });

  const changePim = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("limit", `${query.limit || 30}`);
      params.set("querySearch", query.querySearch || "");
      await request
        .get(`labels`, { params })
        .then((res) => {
          dispatch(labelActions.paginate(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(labelActions.paginate(initialState.labels));
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    query,
    setQuery,
    changePim,
    pending,
    fetch,
  };
};
