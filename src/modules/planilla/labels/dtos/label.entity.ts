export interface ILabelEntity {
  id: number;
  name: string;
}
