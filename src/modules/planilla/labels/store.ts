import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { ILabelEntity } from "./dtos/label.entity";

export interface LabelState {
  labels: ResponsePaginateDto<ILabelEntity>;
  label: ILabelEntity;
}

export const initialState: LabelState = {
  labels: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  label: {} as any,
};

const labelStore = createSlice({
  name: "planilla@labels",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<ILabelEntity>>
    ) => {
      state.labels = payload;
      return state;
    },
    updateItem: (state, { payload }: PayloadAction<ILabelEntity>) => {
      state.labels?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
      // response
      return state;
    },
    find: (state, { payload }: PayloadAction<ILabelEntity>) => {
      state.label = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.label };
    },
  },
});

export const labelReducer = labelStore.reducer;

export const labelActions = labelStore.actions;
