/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { ILabelEntity } from "../dtos/label.entity";
import { useLabelList } from "../hooks/use-label-list";

interface IProps {
  onChange: (option: any) => void;
  value: any;
  name: string;
  defaultQuerySearch?: string;
}

export const LabelSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const labelList = useLabelList();

  const { labels } = useSelector((state: RootState) => state.label);

  const [isFetch, setIsFetch] = useState<boolean>(false);

  const settingsData = (row: ILabelEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  const handleSearch = (value: string) => {
    if (!value) return;
    labelList.changePim({ name: "querySearch", value: value });
    setIsFetch(true);
  };

  useEffect(() => {
    labelList.changePim({ value: defaultQuerySearch, name: "querySearch" });
    setIsFetch(true);
  }, [defaultQuerySearch]);

  useEffect(() => {
    if (isFetch) labelList.fetch().catch(() => null);
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={labels?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={handleSearch}
    />
  );
};
