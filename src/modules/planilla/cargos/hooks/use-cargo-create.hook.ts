import { FormikHelpers, useFormik } from "formik";
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { cargoCreateValidate, ICargoCreateDto } from "../dtos/cargo-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";

const request = planillaRequest();

export const useRoleCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isCreated, setIsCreated] = useState<boolean>(false);

  const save = async (
    values: any,
    { resetForm }: FormikHelpers<any>
  ): Promise<any> => {
    setPending(true);
    setIsCreated(false);
    await request
      .post(`cargos`, values)
      .then(() => {
        resetForm();
        setIsCreated(true);
        toast.success(`La partición presupuestal se guardó correctamente!`);
      })
      .catch((err) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      description: "",
      extension: "",
    } as ICargoCreateDto,
    validationSchema: cargoCreateValidate,
    onSubmit: save,
  });

  return {
    pending,
    isCreated,
    form: formik.values,
    formik: formik,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
