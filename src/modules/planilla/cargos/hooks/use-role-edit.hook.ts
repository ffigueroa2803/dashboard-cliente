/* eslint-disable react-hooks/exhaustive-deps */
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { cargoCreateValidate } from "../dtos/cargo-create.dto";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { cargoActions } from "../store";

const request = planillaRequest();

export const useRoleEdit = () => {
  const dispatch = useDispatch();

  const { cargo } = useSelector((state: RootState) => state.cargo);

  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (values: any): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`cargos/${cargo.id}`, values)
      .then((res) => {
        setIsUpdated(true);
        toast.success(`La partición presupuestal se actualizó correctamente!`);
        dispatch(cargoActions.setCargo(res.data));
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: cargo,
    validationSchema: cargoCreateValidate,
    onSubmit: save,
  });

  useEffect(() => {
    if (cargo?.id) formik.setValues(cargo);
  }, [cargo]);

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
