/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch } from "react-redux";
import { initialState, cargoActions } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { PaginateDto } from "@services/dtos";

const request = planillaRequest();

export const useCargoList = (dataDefault?: PaginateDto) => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [query, setQuery] = useState<PaginateDto>({
    page: dataDefault?.page || 1,
    limit: dataDefault?.limit || 100,
  });

  const changeForm = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("querySearch", query.querySearch || "");
      params.set("limit", `${query.limit || 30}`);
      await request
        .get(`cargos`, { params })
        .then((res) => {
          dispatch(cargoActions.paginate(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(cargoActions.paginate(initialState.cargos));
          alerStatus(err?.response?.status);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    query,
    setQuery,
    changeForm,
    pending,
    fetch,
  };
};
