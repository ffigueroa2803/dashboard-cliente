/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useCargoList } from "../hooks/use-cargo-list";
import { ICargoEntity } from "../dtos/cargo.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const CargoSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const cargoList = useCargoList();

  const { cargos } = useSelector((state: RootState) => state.cargo);

  const [isFetch, setIsFetch] = useState<boolean>(false);

  const settingsData = (row: ICargoEntity) => {
    return {
      label: `${row.name} [${row.extension}]`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  const handleSearch = (value: string) => {
    if (!value) return;
    cargoList.changeForm({ name: "querySearch", value: value });
    setIsFetch(true);
  };

  useEffect(() => {
    cargoList.changeForm({ value: defaultQuerySearch, name: "querySearch" });
    setIsFetch(true);
  }, [defaultQuerySearch]);

  useEffect(() => {
    if (isFetch) cargoList.fetch().catch(() => null);
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={cargos?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={handleSearch}
    />
  );
};
