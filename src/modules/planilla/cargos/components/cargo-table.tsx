/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Edit } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { ICargoEntity } from "../dtos/cargo.entity";
import { cargoActions } from "../store";
import { cargoContextEnum, CargoContext } from "./cargo-context";

interface IProps {
  onClick?: (role: ICargoEntity, action: cargoContextEnum) => void;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
}

export const CargoTable = ({
  onClick,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
}: IProps) => {
  const dispatch = useDispatch();

  const { cargos } = useSelector((state: RootState) => state.cargo);

  const roleContext = useContext(CargoContext);

  const handleClick = (cargo: ICargoEntity, action: cargoContextEnum) => {
    dispatch(cargoActions.setCargo(cargo));
    roleContext.setOptions(action);
    if (typeof onClick == "function") {
      onClick(cargo, action);
    }
  };

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      center: true,
      grow: true,
      cell: (row: ICargoEntity) => (
        <>
          <Edit
            className="icon cursor-pointer"
            onClick={() => handleClick(row, cargoContextEnum.EDIT)}
          />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,

        cell: (row: ICargoEntity) => row.id,
      },
      {
        name: "Nombre",
        wrap: true,
        cell: (row: ICargoEntity) => (
          <span className="uppercase">{row?.name || ""}</span>
        ),
      },
      {
        name: "Descripción",
        wrap: true,
        center: true,
        cell: (row: ICargoEntity) => (
          <span className="uppercase">{row.description}</span>
        ),
      },
      {
        name: "Extensión Presupuestal",
        wrap: true,
        center: true,
        cell: (row: ICargoEntity) => (
          <span className="uppercase">{row.extension}</span>
        ),
      },
    ];

    rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cargos]);

  return (
    <>
      <Datatable
        data={cargos.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={cargos?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={cargos?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
