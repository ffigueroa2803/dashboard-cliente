/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { Save } from "react-feather";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useRoleCreate } from "../hooks/use-cargo-create.hook";
import { CargoForm } from "./cargo-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const CargoCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const roleCreate = useRoleCreate();

  useEffect(() => {
    if (roleCreate.isCreated) onSave();
  }, [roleCreate.isCreated]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Crear Partición Presupuestal</ModalHeader>
      <ModalBody>
        <CargoForm
          form={roleCreate.form}
          formik={roleCreate.formik}
          handleChange={roleCreate.handleChange}
          handleBlur={roleCreate.handleBlur}
          handleSubmit={roleCreate.handleSubmit}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="success"
          onClick={() => roleCreate.handleSubmit()}
          disabled={roleCreate.pending}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
