/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useRoleEdit } from "../hooks/use-role-edit.hook";
import { CargoForm } from "./cargo-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const RoleEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { cargo } = useSelector((state: RootState) => state.cargo);

  const cargoEdit = useRoleEdit();

  useEffect(() => {
    if (cargoEdit.isUpdated) onSave();
  }, [cargoEdit.isUpdated]);

  useEffect(() => {
    if (!isOpen) cargoEdit.setForm(cargo);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Partición Presupuestal</ModalHeader>
      <ModalBody>
        <CargoForm
          form={cargoEdit.form}
          formik={cargoEdit.formik}
          handleChange={cargoEdit.handleChange}
          handleBlur={cargoEdit.handleBlur}
          handleSubmit={cargoEdit.handleSubmit}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          onClick={() => cargoEdit.handleSubmit()}
          disabled={cargoEdit.pending}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
