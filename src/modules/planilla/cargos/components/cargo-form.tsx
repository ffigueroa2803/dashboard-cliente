/* eslint-disable no-unused-vars */
import { InputBasic } from "@common/inputs/input-basic";
import { Form, FormGroup } from "reactstrap";

interface IProps {
  formik: any;
  form: any;
  handleBlur: (e: any) => void;
  handleChange: (e: any) => void;
  handleSubmit: () => void;
}

export const CargoForm = ({
  formik,
  form,
  handleBlur,
  handleChange,
  handleSubmit,
}: IProps) => {
  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <FormGroup>
        <InputBasic
          title="Nombre"
          type="text"
          name="name"
          formik={formik}
          value={form.name}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <InputBasic
          title="Descripción"
          type="textarea"
          name="description"
          formik={formik}
          value={form.description}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>

      <FormGroup>
        <InputBasic
          title="Extensión Presupuestal"
          type="text"
          name="extension"
          formik={formik}
          value={form.extension}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </FormGroup>
    </Form>
  );
};
