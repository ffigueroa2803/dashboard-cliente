/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import { useRouter } from "next/router";
import { Plus, Search } from "react-feather";
import { CargoContext, CargoProvider, cargoContextEnum } from "./cargo-context";
import { useCargoList } from "../hooks/use-cargo-list";
import { FloatButton } from "@common/button/float-button";
import { CargoTable } from "./cargo-table";
import { CargoCreate } from "./cargo-create";
import { RoleEdit } from "./role-edit";

const CargoWrapper = () => {
  const cargoContext = useContext(CargoContext);

  const cargoList = useCargoList({
    page: 1,
    limit: 30,
    querySearch: cargoContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    cargoList.changeForm({ name: "page", value: page });
    cargoContext.setClearRows(true);
    cargoContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    cargoList.changeForm({ name: "limit", value: limit });
    cargoContext.setClearRows(true);
    cargoContext.setIds([]);
    setIsFetch(true);
  };

  const handleSave = () => {
    cargoContext.setOptions(cargoContextEnum.DEFAULT);
    setIsFetch(true);
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    cargoList.changeForm({
      name: "queryChange",
      value: cargoContext.querySearch,
    });
  }, [cargoContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      cargoContext.setClearRows(true);
      cargoContext.setIds([]);
      cargoList.fetch().catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={cargoContext.querySearch}
                  disabled={cargoList.pending}
                  onChange={({ target }) =>
                    cargoContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <CargoTable
            loading={cargoList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </CardBody>
      </Card>
      {/* create */}
      <FloatButton
        color="success"
        icon={<Plus />}
        onClick={() => cargoContext.setOptions(cargoContextEnum.CREATE)}
      />
      {/* crear */}
      <CargoCreate
        isOpen={cargoContext.options == cargoContextEnum.CREATE}
        onClose={() => cargoContext.setOptions(cargoContextEnum.DEFAULT)}
        onSave={handleSave}
      />
      {/* edit */}
      <RoleEdit
        isOpen={cargoContext.options == cargoContextEnum.EDIT}
        onClose={() => cargoContext.setOptions(cargoContextEnum.DEFAULT)}
        onSave={handleSave}
      />
    </>
  );
};

export const CargoContent = () => {
  return (
    <CargoProvider>
      <CargoWrapper />
    </CargoProvider>
  );
};
