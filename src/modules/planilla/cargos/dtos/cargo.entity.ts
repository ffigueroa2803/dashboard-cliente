export interface ICargoEntity {
  id: number;
  name: string;
  description: string;
  extension: string;
}

export const cargoEntityName = "CargoEntity";
