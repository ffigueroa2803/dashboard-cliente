import { Yup } from "@common/yup";

export const cargoCreateValidate = Yup.object().shape({
  name: Yup.string().required().min(2).max(50),
  description: Yup.string().required().min(2).max(255),
  extension: Yup.string().required().min(2).max(50),
});

export interface ICargoCreateDto {
  name: string;
  description: string;
  extension: string;
}
