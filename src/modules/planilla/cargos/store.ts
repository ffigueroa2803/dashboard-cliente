import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { ICargoEntity } from "./dtos/cargo.entity";

export interface CargoState {
  cargos: ResponsePaginateDto<ICargoEntity>;
  cargo: ICargoEntity;
  option: string;
}

export const initialState: CargoState = {
  cargos: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  cargo: {} as any,
  option: "",
};

const cargoStore = createSlice({
  name: "planilla@cargos",
  initialState,
  reducers: {
    paginate: (
      state: CargoState,
      { payload }: PayloadAction<ResponsePaginateDto<ICargoEntity>>
    ) => {
      state.cargos = payload;
    },
    updateItem: (
      state: CargoState,
      { payload }: PayloadAction<ICargoEntity>
    ) => {
      state.cargos?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
    },
    find: (state: CargoState, { payload }: PayloadAction<ICargoEntity>) => {
      state.cargo = payload;
    },
    setCargo: (state: CargoState, { payload }: PayloadAction<ICargoEntity>) => {
      state.cargo = payload;
    },
    changeOption: (state: CargoState, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.cargo };
    },
  },
});

export const cargoReducer = cargoStore.reducer;

export const cargoActions = cargoStore.actions;
