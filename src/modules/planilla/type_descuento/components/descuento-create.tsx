/* eslint-disable no-unused-vars */
import Toggle from "@atlaskit/toggle";
import { AirhspSelect } from "@modules/planilla/airhsp/infrastructure/components/airhsp-select";
import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";
import { InputDto } from "@services/dtos";
import { useState } from "react";
import { ArrowLeft, Save } from "react-feather";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { createTypeDescuento } from "../apis";
import { ICreateTypeDescuentoDto } from "../dtos/create-type_descuento.dto";

interface IProps {
  onClose: () => void;
  onSave: (typedescuento: TypeDescuento) => void;
}

export const defaultTypeDescuento: ICreateTypeDescuentoDto = {
  description: "",
  code: "",
  plame: false,
  isEdit: false,
  state: false,
};

export const DescuentoCreate = ({ onClose, onSave }: IProps) => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] =
    useState<ICreateTypeDescuentoDto>(defaultTypeDescuento);

  const handleOnChange = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    setPending(true);
    await createTypeDescuento(form as any)
      .then((data) => {
        // setForm(defaultWork);
        setForm(defaultTypeDescuento);
        toast.success(`Los datos se guardarón correctamente!`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`Ocurrio un error al guardar los datos`));
    setPending(false);
    onClose();
  };

  const ComponentCreateType_Remuneration = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            name="code"
            className="capitalize"
            value={form?.code}
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            className="capitalize"
            name="description"
            value={form?.description}
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>¿Mostrar en el Reporte Plame?</label>
          <div>
            <Toggle
              name="plame"
              isChecked={form?.plame || false}
              onChange={({ target }) =>
                handleOnChange({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>Edición</label>
          <div>
            <Toggle
              name="state"
              isChecked={form?.state || false}
              onChange={({ target }) =>
                handleOnChange({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>Código AIRHSP</label>
          <AirhspSelect
            name="airhspId"
            value={form?.airhspId || ""}
            query={{ page: 1, limit: 100, level: 2 }}
            onChange={(opt) =>
              handleOnChange({ name: opt.name, value: opt.value })
            }
          />
        </FormGroup>
        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} className="icon" />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} className="icon" />
          </Button>
        </Col>
      </Row>
    </div>
  );

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Tip. Descuento</ModalHeader>
      <ModalBody>{ComponentCreateType_Remuneration}</ModalBody>
    </Modal>
  );
};
