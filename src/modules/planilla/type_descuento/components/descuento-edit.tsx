/* eslint-disable no-unused-vars */
import Toggle from "@atlaskit/toggle";
import { AirhspSelect } from "@modules/planilla/airhsp/infrastructure/components/airhsp-select";
import { updateTypeDescuento } from "@modules/planilla/type_descuento/apis";
import { InputDto } from "@services/dtos";
import { RootState } from "@store/store";
import { useState } from "react";
import { ArrowLeft, Save } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { TypeDescuento } from "../dtos/type_descuento.enitity";

interface IProps {
  onClose: () => void;
  onSave: (typedescuento: TypeDescuento) => void;
}

export const DescuentoEdit = ({ onClose, onSave }: IProps) => {
  const { type_descuento } = useSelector(
    (state: RootState) => state.type_descuento
  );
  const [form, setForm] = useState<TypeDescuento>(type_descuento);
  const [pending, setPending] = useState<boolean>(false);

  const handleInput = ({ name, value }: InputDto) => {
    setForm({ ...form, [name]: value });
  };

  const handleSave = async () => {
    setPending(true);
    await updateTypeDescuento(type_descuento.id, form)
      .then((data) => {
        toast.success("Los datos se guardaron correctamente");
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error("Ocrruio un error al actulizar los datos"));
    setPending(false);
    onClose();
  };
  const ComponenteEditType_Remuneration = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            className="capitalize"
            name="code"
            onChange={({ target }) => handleInput(target)}
            value={form?.code || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Alias</label>
          <Input
            type="text"
            className="capitalize"
            name="description"
            onChange={({ target }) => handleInput(target)}
            defaultValue={form?.description || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>¿Mostrar en el Reporte Plame?</label>
          <div>
            <Toggle
              name="isEdit"
              defaultChecked={form?.isEdit || false}
              onChange={({ target }) =>
                handleInput({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>Edicion</label>
          <div>
            <Toggle
              name="state"
              defaultChecked={form?.state || false}
              onChange={({ target }) =>
                handleInput({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>Código AIRHSP</label>
          <AirhspSelect
            name="airhspId"
            value={form?.airhspId || ""}
            query={{ page: 1, limit: 100, level: 2 }}
            onChange={(opt) =>
              handleInput({ name: opt.name, value: opt.value })
            }
          />
        </FormGroup>
        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} className="icon" />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} className="icon" />
          </Button>
        </Col>
      </Row>
    </div>
  );

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Editar Tip. Descuento</ModalHeader>
      <ModalBody>{ComponenteEditType_Remuneration}</ModalBody>
    </Modal>
  );
};
