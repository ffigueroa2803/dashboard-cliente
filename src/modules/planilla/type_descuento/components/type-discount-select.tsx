import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getTypeDescuentos } from "../apis";
import { TypeDescuento } from "../dtos/type_descuento.enitity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
  isDisabled?: boolean;
}

export const TypeDiscountSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
  isDisabled,
}: IProps) => {
  return (
    <SelectRemote
      isDisabled={isDisabled}
      defaultQuerySearch={defaultQuerySearch}
      handle={getTypeDescuentos}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: TypeDescuento) =>
          `${row.code}.- ${row.description}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
