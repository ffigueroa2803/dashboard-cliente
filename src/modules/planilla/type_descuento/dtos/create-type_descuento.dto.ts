export interface ICreateTypeDescuentoDto {
  description: string,
  code: string,
  plame: boolean,
  state: boolean
  isEdit?: boolean,
  is_escalafon?: boolean,
  judicial?: boolean,
  except?: boolean,
  renta?: boolean,
  airhspId?: number;
}