import { AirhspEntity } from "@modules/planilla/airhsp/domain/airhsp.entity";

export interface TypeDescuento {
  id: number;
  description: string;
  isEdit: boolean;
  is_escalafon?: boolean;
  judicial?: boolean;
  code?: string;
  except?: boolean;
  plame?: boolean;
  renta?: boolean;
  airhspId?: number;
  state?: boolean;

  airhsp?: AirhspEntity;
}

export const typeDiscountEntityName = "TypeDiscountEntity";
