import { useState } from "react";
import { paginate } from "../store";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch } from "react-redux";

const request = planillaRequest();
export const useTypeDiscount = (defaultQuerySearch?: string) => {
  const dispatch = useDispatch();
  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );
  const [pending, setPending] = useState<boolean>(false);
  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`typeDiscounts`, { params })
      .then((res) => dispatch(paginate(res.data)))
      .catch((err) => dispatch(paginate(err)));
    setPending(false);
  };
  return {
    pending,
    page,
    limit,
    querySearch,
    setQuerySearch,
    fetch,
  };
};
