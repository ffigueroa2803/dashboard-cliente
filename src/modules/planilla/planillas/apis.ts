import { planillaRequest } from "@services/planilla.request";
import { IFilterPlanillaDto } from "./dtos/filter-planilla.dto";

const request = planillaRequest();

export const getPlanillas = async ({
  page,
  querySearch,
  limit,
  principal,
}: IFilterPlanillaDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (typeof principal == "boolean") params.set("principal", `${principal}`);
  return await request.get(`planillas`, { params }).then((res) => res.data);
};
