import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IPlanillaEntity } from "./dtos/planilla.entity";

export interface PlanillaState {
  planillas: ResponsePaginateDto<IPlanillaEntity>;
  planilla: IPlanillaEntity;
}

export const initialState: PlanillaState = {
  planillas: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  planilla: {} as any,
};

const planillaStore = createSlice({
  name: "planilla@planillas",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IPlanillaEntity>>
    ) => {
      state.planillas = payload;
      return state;
    },
    setPlanilla: (state, { payload }: PayloadAction<IPlanillaEntity>) => {
      state.planilla = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.planilla };
    },
  },
});

export const planillaReducer = planillaStore.reducer;

export const planillaActions = planillaStore.actions;
