import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getPlanillas } from "../apis";
import { IPlanillaEntity } from "../dtos/planilla.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: any;
  name: string;
  defaultQuerySearch?: string;
  principal?: boolean;
}

export const PlanillaSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
  principal,
}: IProps) => {
  return (
    <SelectRemote
      query={{ principal }}
      defaultQuerySearch={defaultQuerySearch}
      handle={getPlanillas}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: IPlanillaEntity) => `${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
