import { PaginateDto } from "@services/dtos";

export interface IFilterPlanillaDto extends PaginateDto {
  principal?: boolean;
}
