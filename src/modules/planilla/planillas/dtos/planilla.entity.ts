export interface IPlanillaEntity {
  id: number;
  name: string;
  principal: boolean;
}
