import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IInfoEntity } from "./dtos/info.entity";

export interface InfoState {
  infos: ResponsePaginateDto<IInfoEntity>;
  info: IInfoEntity;
  option: string;
}

const initialState: InfoState = {
  infos: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  info: {} as any,
  option: "",
};

const infoStore = createSlice({
  name: "planilla@infos",
  initialState,
  reducers: {
    paginate: (
      state: InfoState,
      { payload }: PayloadAction<ResponsePaginateDto<IInfoEntity>>
    ) => {
      state.infos = payload as any;
    },
    updateItem: (state: InfoState, { payload }: PayloadAction<IInfoEntity>) => {
      state.infos?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
    },
    find: (state: InfoState, { payload }: PayloadAction<IInfoEntity>) => {
      state.info = payload as any;
    },
    changeOption: (state: InfoState, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    delete: (state: InfoState, { payload }: PayloadAction<number>) => {
      state.infos.items = state.infos.items.filter(
        (item) => item.id != payload
      );
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.info };
    },
  },
});

export const infoReducer = infoStore.reducer;

export const infoActions = infoStore.actions;
