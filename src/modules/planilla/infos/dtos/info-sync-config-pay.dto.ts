import { Yup } from "@common/yup";

export interface InfoSyncConfigPayDto {
  planillaId?: number;
  typeCargoId?: number;
  typeCategoryId?: number;
}

export const InfoSyncConfigPayYup = Yup.object({
  planillaId: Yup.number(),
  typeCargoId: Yup.number(),
  typeCategotyId: Yup.number(),
});

export const InfoSyncConfigPayDefault = {
  planillaId: undefined,
  typeCargoId: undefined,
  typeCategoryId: undefined,
};
