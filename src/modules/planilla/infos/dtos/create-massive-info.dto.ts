import { ICreateInfoDto } from "./create-info.dto";

export interface CreateMassiveInfoDto {
  infos: ICreateInfoDto[]
}