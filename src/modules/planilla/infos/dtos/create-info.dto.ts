export interface ICreateInfoDto {
  contractId: number;
  planillaId: number;
  pimId: number;
  bankId: number;
  numberOfAccount?: string;
  isSync: boolean;
  isEmail: boolean;
  labelId: number | null;
  state: boolean;
}