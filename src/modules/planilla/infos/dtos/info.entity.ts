import { IBankEntity } from "@modules/auth/banks/dtos/bank.entity";
import { ILabelEntity } from "@modules/planilla/labels/dtos/label.entity";
import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { IPlanillaEntity } from "@modules/planilla/planillas/dtos/planilla.entity";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";

export interface IInfoEntity {
  id: number;
  contractId: number;
  codeAIRHSP: string;
  planillaId: number;
  pimId: number;
  bankId: number;
  numberOfAccount: string;
  isPay: boolean;
  isEmail: boolean;
  isSync: boolean;
  observation?: string;
  labelId?: number;
  state: boolean;
  contract?: IContractEntity;
  planilla?: IPlanillaEntity;
  bank?: IBankEntity;
  pim: IPimEntity;
  label: ILabelEntity;
}

export const infoEntityName = "InfoEntity";
