import { PaginateDto } from "@services/dtos";

export interface InfoPaginateDto extends PaginateDto {
  contractId?: number;
  planillaId?: number;
  pimId?: number;
  typeCargoId?: number;
}