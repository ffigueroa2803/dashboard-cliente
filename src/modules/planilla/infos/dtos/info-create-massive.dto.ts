import { Yup } from "@common/yup";

export interface InfoCreateMassiveDto {
  contractId: number;
  planillaId: number;
  pimId: number;
  bankId: number;
  numberOfAccount?: string;
  isSync: boolean;
  isEmail: boolean;
  labelId: number | null;
  state: boolean;
}

export const InfoCreateMassiveYup = Yup.object({
  planillaId: Yup.number().required(),
  pimId: Yup.number().required(),
  bankId: Yup.number().required(),
  numberOfAccount: Yup.string().optional(),
  isSync: Yup.bool().required(),
  isEmail: Yup.bool().required(),
  labelId: Yup.number().optional(),
  observation: Yup.string().optional(),
  state: Yup.bool().required()
});

export const InfoCreateMassiveDefault = {
  planillaId: undefined,
  pimId: undefined,
  bankId: undefined,
  numberOfAccount: undefined,
  isSync: true,
  isEmail: true,
  labelId: undefined,
  observation: undefined,
  state: true
}