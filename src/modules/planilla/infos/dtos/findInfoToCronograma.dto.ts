import { PaginateDto } from "../../../../services/dtos";

export interface findInfosToCronogramaDto extends PaginateDto {
  typeCargoId?: number;
  condition?: string;
  typeCategoryId?: number;
}
