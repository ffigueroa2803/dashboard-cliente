export interface IFormInfoDto {
  contractId: number;
  planillaId: number;
  codeAIRHSP: string;
  pimId: number;
  bankId: number;
  numberOfAccount?: string;
  isSync: boolean;
  isEmail: boolean;
  state: boolean;
  observation?: string;
  labelId: number | null;
}
