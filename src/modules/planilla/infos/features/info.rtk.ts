import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { CreateMassiveInfoDto } from "../dtos/create-massive-info.dto";
import { InfoPaginateDto } from "../dtos/info-paginate.dto";
import { IInfoEntity } from "../dtos/info.entity";
import { InfoEditInterface } from "../interfaces/info-edit.interface";
import { InfoSyncConfigPayDto } from "../dtos/info-sync-config-pay.dto";
import { RootState } from "@store/store";
const URL = process.env.NEXT_PUBLIC_PLANILLA_URL || "";

export const InfoRtk = createApi({
  reducerPath: "infoRtk",
  baseQuery: fetchBaseQuery({
    baseUrl: `${URL}`,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).auth.token;
      if (token) headers.set("authorization", `Bearer ${token}`);
      return headers;
    },
  }),
  endpoints: (builder) => ({
    paginateInfos: builder.query<PaginateEntity<IInfoEntity>, InfoPaginateDto>({
      query: (params) => ({
        url: "infos",
        method: "GET",
        params,
      }),
    }),
    editInfo: builder.mutation<IInfoEntity, InfoEditInterface>({
      query: ({ id, body }) => ({
        url: `infos/${id}`,
        method: "PUT",
        body,
      }),
    }),
    storeMassive: builder.mutation<IInfoEntity, CreateMassiveInfoDto>({
      query: (body) => ({
        url: "infos/store/massive",
        method: "POST",
        body,
      }),
    }),
    syncMassive: builder.mutation<any, InfoSyncConfigPayDto>({
      query: (body) => ({
        url: "infos/process/syncMassive",
        method: "POST",
        body,
      }),
    }),
  }),
});

export const {
  useStoreMassiveMutation,
  useEditInfoMutation,
  useLazyPaginateInfosQuery,
  useSyncMassiveMutation,
} = InfoRtk;
