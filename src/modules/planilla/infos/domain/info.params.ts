export interface InfoEditParams {
  planillaId: number;
  pimId: number;
  bankId: number;
  numberOfAccount?: string;
  isEmail: boolean;
  labelId: number | null;
  state: boolean;
}