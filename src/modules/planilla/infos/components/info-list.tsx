/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { RefreshCcw } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Row } from "reactstrap";
import { useInfosToWork } from "../hooks/use-infos-to-work";
import { infoActions } from "../store";
import { InfoItem } from "./info-item";
import { SelectBasic } from "@common/select/select-basic";

export const InfoList = () => {
  const dispatch = useDispatch();

  const { work } = useSelector((state: RootState) => state.work);
  const { infos, option } = useSelector((state: RootState) => state.info);

  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [querySearch, setQuerySearch] = useState<string>("");
  const [state, setState] = useState<boolean | undefined>(true);

  const infoToWork = useInfosToWork(work?.id || 0, {
    page,
    limit: 30,
    querySearch,
    state,
  });

  const onRefresh = () => {
    setPage(1);
    setQuerySearch("");
    setIsRefresh(true);
  };

  useEffect(() => {
    if (work?.id) infoToWork.execute();
  }, [work, isRefresh]);

  useEffect(() => {
    if (option == "REFRESH") {
      onRefresh();
    }
  }, [option]);

  useEffect(() => {
    dispatch(infoActions.paginate(infoToWork.datos));
  }, [infoToWork.datos]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <>
      <div className="row justify-content-end pr-3">
        <div className="col-md-3 col-sm-8">
          <SelectBasic
            placeholder="Todos"
            name="state"
            value={state}
            onChange={({ value }) => {
              setState(value);
              setIsRefresh(true);
            }}
            options={[
              { value: undefined, label: "Todos" },
              { value: true, label: "Activos" },
              { value: false, label: "Deshabilitar" },
            ]}
          />
        </div>
        <div className="col-xs mb-4">
          <Button color="dark" outline onClick={infoToWork.execute}>
            <RefreshCcw className="icon" /> Actualizar
          </Button>
        </div>
      </div>
      {/* cargando */}
      <Show condition={infoToWork.pending}>
        <div className="text-center mb-3">
          <LoadingSimple loading />
        </div>
      </Show>
      {/* datos */}
      <Row>
        {infos?.items?.map((item) => (
          <Col md="6" key={`item-info-${item.id}`}>
            <InfoItem info={item} />
          </Col>
        ))}
      </Row>
    </>
  );
};
