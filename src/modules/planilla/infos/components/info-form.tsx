/* eslint-disable no-unused-vars */
import Toggle from "@atlaskit/toggle";
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import { BankSelect } from "@modules/auth/banks/components/bank-select";
import { LabelSelect } from "@modules/planilla/labels/components/label-select";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { PlanillaSelect } from "@modules/planilla/planillas/components/planilla-select";
import { ContractChange } from "@modules/scale/contracts/components/contract-change";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { contractActions } from "@modules/scale/contracts/store";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { useEffect, useState } from "react";
import { Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, FormGroup, Input } from "reactstrap";
import { IFormInfoDto } from "../dtos/form-info.dto";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  yearDefault?: number;
  onChange: (input: IInputHandle) => void;
}

const currentDate = DateTime.now();

export const InfoForm = ({
  form,
  yearDefault,
  isEdit,
  onChange,
}: IProps<IFormInfoDto>) => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const [isContract, setIsContract] = useState<boolean>(false);
  const [year, setYear] = useState<number>(yearDefault || currentDate.year);

  const handleAdd = (data: IContractEntity) => {
    dispatch(contractActions.find(data));
    setIsContract(false);
  };

  useEffect(() => {
    if (yearDefault) setYear(yearDefault);
  }, [yearDefault]);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* contract */}
        <FormGroup>
          <Show condition={!isEdit}>
            <Button
              outline
              color="dark mb-2"
              onClick={() => setIsContract(true)}
            >
              <Settings className="icon" />
            </Button>
          </Show>

          {/* info contract */}
          <Show condition={typeof contract?.id != "undefined"}>
            <ContractDetail contract={contract || ({} as any)} />
            <hr />

            <FormGroup>
              <label>
                Código AIRHSP <b className="text-danger">*</b>
              </label>
              <Input
                type="text"
                name="codeAIRHSP"
                value={form?.codeAIRHSP || ""}
                onChange={({ target }) => onChange(target)}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Planilla <b className="text-danger">*</b>
              </label>
              <div>
                <PlanillaSelect
                  name="planillaId"
                  value={form?.planillaId}
                  onChange={onChange}
                />
              </div>
            </FormGroup>

            {/* form info */}
            <FormGroup>
              <label>Año del Pim</label>
              <Input
                type="number"
                value={year}
                onChange={({ target }) => setYear(parseInt(`${target.value}`))}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Pim <b className="text-danger">*</b>
              </label>
              <div>
                <PimSelect
                  year={year}
                  name="pimId"
                  value={form?.pimId}
                  onChange={onChange}
                />
              </div>
            </FormGroup>

            <FormGroup>
              <label>
                Banco <b className="text-danger">*</b>
              </label>
              <div>
                <BankSelect
                  name="bankId"
                  value={form?.bankId}
                  onChange={onChange}
                />
              </div>
            </FormGroup>

            <FormGroup>
              <label>N° Cuenta</label>
              <Input
                type="text"
                name="numberOfAccount"
                value={form?.numberOfAccount}
                onChange={({ target }) => onChange(target)}
              />
            </FormGroup>

            <FormGroup className="mb-3">
              <label>Agrupación</label>
              <LabelSelect
                name="labelId"
                value={form?.labelId}
                onChange={(target) =>
                  onChange({ name: target.name, value: target.value || null })
                }
              />
            </FormGroup>

            <FormGroup>
              <label>¿Sincronizar al contrato?</label>
              <div>
                <Toggle
                  name="isSync"
                  isChecked={form?.isSync || false}
                  onChange={({ target }) =>
                    onChange({ name: target.name, value: target.checked })
                  }
                />
              </div>
            </FormGroup>

            <FormGroup>
              <label>¿Enviar al correo?</label>
              <div>
                <Toggle
                  name="isEmail"
                  isChecked={form?.isEmail || false}
                  onChange={({ target }) =>
                    onChange({ name: target.name, value: target.checked })
                  }
                />
              </div>
            </FormGroup>

            <FormGroup>
              <label>Observación</label>
              <Input
                type="textarea"
                name="observation"
                value={form?.observation}
                onChange={({ target }) => onChange(target)}
              />
            </FormGroup>

            <FormGroup>
              <label>Estado</label>
              <div>
                <Toggle
                  name="state"
                  isChecked={form?.state || false}
                  onChange={({ target }) =>
                    onChange({ name: target.name, value: target.checked })
                  }
                />
              </div>
            </FormGroup>
          </Show>
        </FormGroup>
      </Form>
      {/* select contract */}
      <ContractChange
        isOpen={isContract}
        onClose={() => setIsContract(false)}
        onAdd={handleAdd}
      />
    </>
  );
};
