/* eslint-disable no-unused-vars */
import Toggle from "@atlaskit/toggle";
import { BankSelect } from "@modules/auth/banks/components/bank-select";
import { LabelSelect } from "@modules/planilla/labels/components/label-select";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { PlanillaSelect } from "@modules/planilla/planillas/components/planilla-select";
import { useFormik } from "formik";
import { DateTime } from "luxon";
import { useState } from "react";
import { ArrowRight } from "react-feather";
import { Button, Form, FormGroup, Input } from "reactstrap";
import {
  InfoCreateMassiveDefault,
  InfoCreateMassiveDto,
  InfoCreateMassiveYup,
} from "../dtos/info-create-massive.dto";

export interface InfoCreateMassiveStepConfigProps {
  nextStep(data: InfoCreateMassiveDto): void;
}

export function InfoCreateMassiveStepConfig({
  nextStep,
}: InfoCreateMassiveStepConfigProps) {
  const [year, setYear] = useState<number>(DateTime.now().year);

  const formik = useFormik({
    initialValues: InfoCreateMassiveDefault,
    validationSchema: InfoCreateMassiveYup,
    onSubmit: (values) => nextStep(values as any),
  });

  return (
    <Form className="form-group" onSubmit={formik.handleSubmit}>
      <FormGroup>
        <label>Año del Pim</label>
        <Input
          type="number"
          value={year}
          onChange={({ target }) => setYear(parseInt(target.value))}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Pim <b className="text-danger">*</b>
        </label>
        <div>
          <PimSelect
            year={year}
            name="pimId"
            value={formik.values?.pimId || ""}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <label>
          Planilla <b className="text-danger">*</b>
        </label>
        <div>
          <PlanillaSelect
            name="planillaId"
            value={formik.values?.planillaId}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <label>
          Banco <b className="text-danger">*</b>
        </label>
        <div>
          <BankSelect
            name="bankId"
            value={formik.values?.bankId || ""}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <FormGroup className="mb-3">
        <label>Agrupación</label>
        <LabelSelect
          name="labelId"
          value={formik.values?.labelId}
          onChange={(target) => formik.handleChange({ target })}
        />
      </FormGroup>

      <FormGroup>
        <label>¿Sincronizar al contrato?</label>
        <div>
          <Toggle
            name="isSync"
            isChecked={formik.values?.isSync || false}
            onChange={formik.handleChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <label>¿Enviar al correo?</label>
        <div>
          <Toggle
            name="isEmail"
            isChecked={formik.values?.isEmail || false}
            onChange={formik.handleChange}
          />
        </div>
      </FormGroup>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={formik.values?.observation}
          onChange={formik.handleChange}
        />
      </FormGroup>
      {/* siguiente */}
      <div className="text-right">
        <hr />
        <Button color="dark" type="submit" outline>
          <ArrowRight className="icon" />
        </Button>
      </div>
    </Form>
  );
}
