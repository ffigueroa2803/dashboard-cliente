import { Button, Form, FormGroup } from "reactstrap";
import { RefreshCcw } from "react-feather";
import { PlanillaSelect } from "@modules/planilla/planillas/components/planilla-select";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { useInfoSyncConfigPaysMassive } from "../hooks/use-info-sync-config-pays-massive";
import { Show } from "@common/show";
import { ButtonLoading } from "@common/button/button-loading";

export function InfoSyncConfigPay() {
  const { formik, isLoading } = useInfoSyncConfigPaysMassive();

  return (
    <Form className="form-group" onSubmit={formik.handleSubmit}>
      <FormGroup className="mb-4">
        <label>Planilla</label>
        <div>
          <PlanillaSelect
            name="planillaId"
            value={formik.values?.planillaId}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <FormGroup className="mb-4">
        <label>Tipo Trabajador</label>
        <div>
          <TypeCargoSelect
            name="typeCargoId"
            value={formik.values?.typeCargoId}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <FormGroup className="mb-4">
        <label>Tipo Categoria</label>
        <div>
          <TypeCategorySelect
            name="typeCategoryId"
            value={formik.values?.typeCategoryId}
            onChange={(target) => formik.handleChange({ target })}
          />
        </div>
      </FormGroup>

      <div className="text-right">
        <hr />
        <Show
          condition={!isLoading}
          isDefault={
            <ButtonLoading loading color="primary" title="Sincronizando..." />
          }
        >
          <Button color="primary" type="submit">
            <RefreshCcw className="icon" />
          </Button>
        </Show>
      </div>
    </Form>
  );
}
