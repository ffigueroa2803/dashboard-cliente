import { useState } from "react";
import { InfoCreateMassiveStepContract } from "./info-create-massive-step-contract";
import { ProgressIndicator } from "@atlaskit/progress-indicator";
import { InfoCreateMassiveStepConfig } from "./info-create-massive-step-config";
import { InfoCreateMassiveDto } from "../dtos/info-create-massive.dto";

export function InfoCreateMassive() {
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [steps] = useState<string[]>(["configs", "save"]);
  const [config, setConfig] = useState<InfoCreateMassiveDto | undefined>();

  const handleNext = (config: InfoCreateMassiveDto) => {
    setCurrentStep((data) => data + 1);
    setConfig(config);
  };

  const handleSelectStep = (index: number) => {
    if (index > currentStep) return;
    setCurrentStep(index);
  };

  return (
    <div>
      <ProgressIndicator
        selectedIndex={currentStep}
        values={steps}
        appearance={"primary"}
        onSelect={({ index }) => handleSelectStep(index)}
      />
      {/* content */}
      {currentStep === 0 ? (
        <InfoCreateMassiveStepConfig nextStep={handleNext} />
      ) : null}
      {currentStep === 1 ? (
        <InfoCreateMassiveStepContract
          config={config as any}
          onSave={() => setCurrentStep(0)}
        />
      ) : null}
    </div>
  );
}
