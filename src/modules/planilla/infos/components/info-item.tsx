import { Show } from "@common/show";
import { InfoTypeAportationContent } from "@modules/planilla/info-type-aportations/components/info-type-aportation-content";
import { InfoTypeDiscountContent } from "@modules/planilla/info-type-discounts/components/info-type-discount-content";
import { InfoTypeRemunerationContent } from "@modules/planilla/info-type-remunerations/components/info-type-remuneration-content";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { contractActions } from "@modules/scale/contracts/store";
import { useState } from "react";
import {
  DollarSign,
  Edit,
  File,
  Frown,
  RefreshCcw,
  ShoppingCart,
} from "react-feather";
import { useDispatch } from "react-redux";
import { Card, CardBody, CardFooter, CardHeader, Col, Row } from "reactstrap";
import { IInfoEntity } from "../dtos/info.entity";
import { useInfoSyncConfigPays } from "../hooks/use-info-sync-config-pays";
import { infoActions } from "../store";
import { InfoEdit } from "./info-edit";

interface IProps {
  info: IInfoEntity;
}

export const InfoItem = ({ info }: IProps) => {
  const dispatch = useDispatch();

  const syncConfig = useInfoSyncConfigPays(info.id);

  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [isRemuneration, setIsRemuneration] = useState<boolean>(false);
  const [isDiscount, setIsDiscount] = useState<boolean>(false);
  const [isAportation, setIsAportation] = useState<boolean>(false);

  const handleEdit = () => {
    dispatch(infoActions.find(info));
    dispatch(contractActions.find(info?.contract || ({} as any)));
    setIsEdit(true);
  };

  const handleRemuneration = () => {
    dispatch(infoActions.find(info));
    setIsRemuneration(true);
  };

  const handleDiscount = () => {
    dispatch(infoActions.find(info));
    setIsDiscount(true);
  };

  const handleAportation = () => {
    dispatch(infoActions.find(info));
    setIsAportation(true);
  };

  const handleSave = (data: IInfoEntity) => {
    setIsEdit(false);
    dispatch(infoActions.updateItem(data));
  };

  return (
    <>
      <Card>
        <CardHeader>
          <Edit className="icon cursor-pointer close" onClick={handleEdit} />
          <Row>
            <Col md="6">
              <label>Planilla</label>
              <h6 className="capitalize">{info?.planilla?.name || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Estado</label>
              <h6 className="capitalize">
                <span
                  className={`badge badge-${info.state ? "success" : "danger"}`}
                >
                  {info.state ? "Habilitado" : "Deshabilitado"}
                </span>
              </h6>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="6">
              <label>COD. AIRHSP</label>
              <h6 className="capitalize">{info?.codeAIRHSP || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>PIM</label>
              <h6 className="capitalize">
                {info?.pim?.code || "N/A"} [
                {info?.pim?.cargo?.extension || "N/A"}]
              </h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Partición Presp.</label>
              <h6 className="capitalize">{info?.pim?.cargo?.name || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Banco</label>
              <h6 className="capitalize">{info?.bank?.name || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Medio de Pago</label>
              <h6 className="capitalize">
                {info.numberOfAccount ? "En Cuenta" : "En Cheque"}
              </h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>N° Cuenta</label>
              <h6 className="capitalize">{info.numberOfAccount || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Agrupación</label>
              <h6 className="capitalize">{info?.label?.name || "N/A"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Sincronizado</label>
              <h6 className="capitalize">{info.isSync ? "SI" : "NO"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Remunerado</label>
              <h6 className="capitalize">{info.isPay ? "SI" : "NO"}</h6>
            </Col>
            <Col md="6" className="mb-2">
              <label>Envio al correo</label>
              <h6 className="capitalize">{info.isEmail ? "SI" : "NO"}</h6>
            </Col>
            <Col md="6" className="mb-2" />
            <Col md="12" className="mb-2">
              <label>Observación</label>
              <h6 className="capitalize">{info.observation || "N/A"}</h6>
            </Col>
            <Col md="12" className="mb-2">
              <hr />
              <h6>
                <File className="icon" /> Información del Contrato
              </h6>
              <hr />
              <ContractDetail
                contract={info?.contract || ({} as any)}
                outline
              />
            </Col>
          </Row>
        </CardBody>
        <CardFooter className="text-right">
          <DollarSign
            className="icon cursor-pointer"
            onClick={handleRemuneration}
          />
          <ShoppingCart
            className="icon ml-2 cursor-pointer"
            onClick={handleDiscount}
          />
          <Frown
            className="icon ml-2 cursor-pointer"
            onClick={handleAportation}
          />
          <RefreshCcw
            className={`icon ml-2 ${
              syncConfig.pending ? "" : "cursor-pointer"
            }`}
            onClick={syncConfig.pending ? undefined : syncConfig.execute}
          />
        </CardFooter>
      </Card>
      {/* editar */}
      <InfoEdit
        isOpen={isEdit}
        onClose={() => setIsEdit(false)}
        onSave={handleSave}
      />
      {/* remuneration */}
      <Show condition={isRemuneration}>
        <InfoTypeRemunerationContent
          isOpen={isRemuneration}
          onClose={() => setIsRemuneration(false)}
        />
      </Show>
      {/* discounts */}
      <Show condition={isDiscount}>
        <InfoTypeDiscountContent
          isOpen={isDiscount}
          onClose={() => setIsDiscount(false)}
        />
      </Show>
      {/* aportations */}
      <Show condition={isAportation}>
        <InfoTypeAportationContent
          isOpen={isAportation}
          onClose={() => setIsAportation(false)}
        />
      </Show>
    </>
  );
};
