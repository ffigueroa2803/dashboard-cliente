/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { Trash } from "react-feather";
import { Button } from "reactstrap";
import { InfoMassiveDetailInterface } from "../interfaces/info-massive-detail.interface";

export interface InfoCreateMassiveTableProps {
  data: InfoMassiveDetailInterface[];
  onRemove: (contract: InfoMassiveDetailInterface) => void;
}

export function InfoCreateMassiveTable({
  data,
  onRemove,
}: InfoCreateMassiveTableProps) {
  return (
    <div className="mt-3 table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Cod. AIRHSP</th>
            <th>Nombre Completo</th>
            <th>Categoria</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          {data?.map((contract) => (
            <tr key={`item-${contract.contractId}`}>
              <td>{contract.contractId}</td>
              <td className="uppercase">{contract.contractId}</td>
              <td className="uppercase">{contract.fullname}</td>
              <td className="uppercase">{contract.typeCategoryName}</td>
              <td className="text-center">
                <Button
                  color="danger"
                  size="xs"
                  onClick={() => onRemove(contract)}
                >
                  <Trash className="icon" />
                </Button>
              </td>
            </tr>
          ))}
          {/* no hay datos */}
          <Show condition={!data?.length}>
            <tr>
              <td className="text-center" colSpan={4}>
                <b>No hay contratos agregados</b>
              </td>
            </tr>
          </Show>
        </tbody>
      </table>
    </div>
  );
}
