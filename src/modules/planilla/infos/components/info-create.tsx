/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { contractActions } from "@modules/scale/contracts/store";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { createInfo } from "../apis";
import { IFormInfoDto } from "../dtos/form-info.dto";
import { IInfoEntity } from "../dtos/info.entity";
import { useInfoForm } from "../hooks/use-info-form";
import { useInfoValidate } from "../hooks/use-info-validate";
import { InfoForm } from "./info-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoEntity) => void;
}

export const InfoCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const infoValidate = useInfoValidate();
  const infoForm = useInfoForm();

  const { contract } = useSelector((state: RootState) => state.contract);
  const [pending, setPending] = useState<boolean>(false);

  const handleValidate = () => {
    const { planillaId } = infoForm.form;
    if (!contract || !planillaId) return;
    infoValidate.validate(contract.id, planillaId);
  };

  const handleInfo = () => {
    if (!infoValidate.info) return;
    const { bankId, numberOfAccount } = infoValidate.info;
    infoForm.handleForm({ name: "bankId", value: bankId });
    infoForm.handleForm({ name: "numberOfAccount", value: numberOfAccount });
  };

  const handleSave = async () => {
    setPending(true);
    const payload: IFormInfoDto = Object.assign(infoForm.form, {
      contractId: contract?.id || 0,
    });
    // send create contract
    await createInfo(payload)
      .then((data) => {
        toast.success(`La configuración se guardó correctamente!`);
        infoForm.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch((err) =>
        toast.error(
          err?.response?.data?.message || `No se pudo guardar los datos`
        )
      );
    setPending(false);
  };

  useEffect(() => {
    if (isOpen) dispatch(contractActions.find({} as any));
  }, [isOpen]);

  useEffect(() => {
    handleValidate();
  }, [infoForm.form, contract]);

  useEffect(() => {
    handleInfo();
  }, [infoValidate.info]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Configuración de Pago</ModalHeader>
      <ModalBody>
        <InfoForm form={infoForm.form} onChange={infoForm.handleForm} />
      </ModalBody>
      <Show condition={infoValidate.isValid}>
        <ModalFooter className="text-right">
          <Button color="primary" disabled={pending} onClick={handleSave}>
            <Save size={17} />
          </Button>
        </ModalFooter>
      </Show>
    </Modal>
  );
};
