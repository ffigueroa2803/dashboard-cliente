import { workActions } from "@modules/scale/works/store";
import { RootState } from "@store/store";
import React from "react";
import { Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody, CardHeader } from "reactstrap";
import { infoActions } from "../store";
import { InfoCreate } from "./info-create";
import { InfoList } from "./info-list";

export const InfoContent = () => {
  const dispatch = useDispatch();

  const { option } = useSelector((state: RootState) => state.work);

  const handleSave = () => {
    dispatch(infoActions.changeOption("REFRESH"));
    dispatch(workActions.changeOption(""));
  };

  return (
    <>
      <Card>
        <CardHeader>
          <h6>
            <Settings className="icon" /> Configuraciones
          </h6>
        </CardHeader>
        <CardBody>
          <InfoList />
        </CardBody>
      </Card>
      {/* create */}
      <InfoCreate
        isOpen={option == "CREATE_INFO"}
        onClose={() => dispatch(workActions.changeOption(""))}
        onSave={handleSave}
      />
    </>
  );
};
