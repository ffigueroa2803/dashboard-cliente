/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IRowSelected } from "@common/dtos/row-selected";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { InputDto } from "@services/dtos";
import { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Plus, Search, Trash } from "react-feather";
import { Button, Col, Form, FormGroup, Input, Row } from "reactstrap";
import { IInfoEntity } from "../dtos/info.entity";

export type FilterInfoTableType = {
  querySearch?: string;
  typeCargoId?: number;
  condition?: string;
  typeCategoryId?: number;
};

interface IProps {
  data: IInfoEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  defaultQuerySearch?: string;
  isOptions?: boolean;
  clearSelectedRows?: boolean;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSearch?: (data: FilterInfoTableType) => void;
  onSelectedRowsChange?: (selected: IRowSelected<IInfoEntity>) => void;
}

export const InfoTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  isOptions,
  clearSelectedRows,
  onSearch,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
}: IProps) => {
  const [filters, setFilters] = useState<FilterInfoTableType>({
    querySearch: defaultQuerySearch,
  });

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      grow: true,
      selector: (row: IInfoEntity) => (
        <>
          <Plus className="icon" />
          <Trash className="icon ml-2" />
        </>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IInfoEntity) => row.id,
      },
      {
        name: "COD. AIRHSP",
        grow: true,
        selector: (row: IInfoEntity) => row.codeAIRHSP || "",
      },
      {
        name: "Nombre Completo",
        wrap: true,
        selector: (row: IInfoEntity) => (
          <span className="uppercase">
            {row?.contract?.work?.person?.fullName || ""}
          </span>
        ),
      },
      {
        name: "N° Documento",
        grow: true,
        selector: (row: IInfoEntity) =>
          `${row?.contract?.work?.person?.documentNumber || ""}`,
      },
      {
        name: "Tipo Categoría",
        selector: (row: IInfoEntity) => row?.contract?.typeCategory?.name || "",
      },
      {
        name: "Condición",
        selector: (row: IInfoEntity) => row?.contract?.condition || "",
      },
      {
        name: "PIM",
        selector: (row: IInfoEntity) => (
          <span>
            {row?.pim?.code || ""}
            <span className="ml-2 badge badge-sm badge-dark">
              {row?.pim?.cargo?.extension || ""}
            </span>
          </span>
        ),
      },
    ];

    if (isOptions) rows.push(options);

    return rows;
  }, [data]);

  function handleSearch() {
    if (typeof onSearch == "function") {
      onSearch(filters);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleOnSelectedRowsChange = (selected: IRowSelected<IInfoEntity>) => {
    if (typeof onSelectedRowsChange == "function")
      onSelectedRowsChange(selected);
  };

  const handleFilter = ({ name, value }: InputDto) => {
    setFilters((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="3 col-12" lg="3" sm="6" className="mb-2">
              <Input
                value={filters.querySearch}
                onChange={({ target }) => handleFilter(target)}
                disabled={loading}
                name="querySearch"
              />
            </Col>
            <Col md="3 col-12" lg="2" sm="6" className="mb-2">
              <TypeCargoSelect
                name="typeCargoId"
                placeholder="Tipo. Trabajador"
                value={filters.typeCargoId}
                onChange={(target) => handleFilter(target)}
              />
            </Col>
            <Col md="3 col-12" lg="2" sm="6" className="mb-2">
              <ContractConditionSelect
                name="condition"
                placeholder="Condición"
                value={filters.condition}
                onChange={(target) => handleFilter(target)}
              />
            </Col>
            <Col md="3 col-12" lg="2" sm="6" className="mb-2">
              <TypeCategorySelect
                name="typeCategoryId"
                placeholder="Tip. Categoría"
                value={filters.typeCategoryId}
                onChange={(target) => handleFilter(target)}
              />
            </Col>
            <Col className="mb-2 col-sx">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        selectableRows={typeof onSelectedRowsChange == "function"}
        onSelectedRowsChange={handleOnSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
      />
    </>
  );
};
