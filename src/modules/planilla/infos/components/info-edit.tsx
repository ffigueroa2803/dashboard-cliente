/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { RootState } from "@store/store";
import { useContext, useEffect } from "react";
import { Save, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IInfoEntity, infoEntityName } from "../dtos/info.entity";
import { useInfoDelete } from "../hooks/use-info-delete";
import { useInfoForm } from "../hooks/use-info-form";
import { InfoForm } from "./info-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoEntity) => void;
  onDelete?: (info: IInfoEntity) => void;
}

export const InfoEdit = ({ isOpen, onClose, onSave, onDelete }: IProps) => {
  const dispatch = useDispatch();

  const ability = useContext(CaslContext);

  const { info } = useSelector((state: RootState) => state.info);

  const infoForm = useInfoForm();
  const infoDestroy = useInfoDelete();

  const handleSave = async () => {
    const id = info?.id || 0;
    if (!id) return toast.warning("Algo salió mal, actualiza la página");
    infoForm.handle(id);
  };

  const handleOnDelete = () => {
    if (typeof onDelete == "function") {
      onDelete(info);
    }
  };

  useEffect(() => {
    if (infoDestroy.isSuccess) handleOnDelete();
  }, [infoDestroy.isSuccess]);

  useEffect(() => {
    if (info?.id && isOpen) infoForm.setForm(info as any);
  }, [info?.id, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Configuración de Pago</ModalHeader>
      <ModalBody>
        <InfoForm
          yearDefault={info?.pim?.year}
          isEdit={true}
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Show
          condition={!infoForm.isLoading}
          isDefault={
            <ButtonLoading title="Actualizando..." color="primary" loading />
          }
        >
          <Show
            condition={ability.can(PermissionAction.DELETE, infoEntityName)}
          >
            <Button
              color="danger"
              outline
              disabled={infoForm.isLoading}
              onClick={infoDestroy.destroy}
            >
              <Trash className="icon" />
            </Button>
          </Show>
          <Button
            color="primary"
            disabled={infoForm.isLoading}
            onClick={handleSave}
          >
            <Save className="icon" />
          </Button>
        </Show>
      </ModalFooter>
    </Modal>
  );
};
