/* eslint-disable react-hooks/exhaustive-deps */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { ContractSearchSelect } from "@modules/scale/contracts/components/contract-search-select";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { useEffect, useState } from "react";
import { Save } from "react-feather";
import { toast } from "react-toastify";
import { Button } from "reactstrap";
import { InfoCreateMassiveDto } from "../dtos/info-create-massive.dto";
import { useStoreMassiveMutation } from "../features/info.rtk";
import { useInfoValidate } from "../hooks/use-info-validate";
import { InfoMassiveDetailInterface } from "../interfaces/info-massive-detail.interface";
import { InfoCreateMassiveTable } from "./info-create-massive-table";

export interface InfoCreateMassiveStepContractProps {
  onSave: () => void;
  config: InfoCreateMassiveDto;
}

export function InfoCreateMassiveStepContract({
  onSave,
  config,
}: InfoCreateMassiveStepContractProps) {
  const [contracts, setContracts] = useState<InfoMassiveDetailInterface[]>([]);
  const [tmpContract, setTmpContract] = useState<IContractEntity>();
  const [fetch, { isLoading }] = useStoreMassiveMutation();

  const infoValidate = useInfoValidate();

  const handleValidate = (contract: IContractEntity) => {
    const exists = contracts.find((item) => item.contractId === contract.id);
    if (exists) return;
    setTmpContract(contract);
    infoValidate.validate(contract.id, config.planillaId);
  };

  const handleAdd = () => {
    if (!tmpContract) return;
    const payload: InfoMassiveDetailInterface = {
      fullname: tmpContract.work?.person?.fullName || "N/A",
      typeCategoryName: tmpContract?.typeCategory?.name || "N/A",
      contractId: tmpContract.id,
      planillaId: config.planillaId,
      pimId: config.pimId,
      bankId: config.bankId,
      numberOfAccount: infoValidate.info?.numberOfAccount,
      isSync: config.isSync,
      isEmail: config.isEmail,
      labelId: config.labelId,
      state: true,
    };
    setContracts((item) => [...item, payload]);
    setTmpContract(undefined);
  };

  const handleRemove = (info: InfoMassiveDetailInterface) => {
    const data = contracts.filter(
      (item) => item.contractId !== info.contractId
    );
    setContracts(data);
  };

  const handleSave = () => {
    fetch({ infos: contracts })
      .unwrap()
      .then(() => {
        toast.success(`Los datos se guardarón correctamente!!!`);
        onSave();
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
  };

  useEffect(() => {
    if (infoValidate.isValid) handleAdd();
  }, [infoValidate.isValid]);

  return (
    <div className="form-group">
      <Show
        condition={!infoValidate.isLoading}
        isDefault={
          <div className="text-center mt-3">
            <ButtonLoading loading title="Validando Contrato" color={"dark"} />
          </div>
        }
      >
        <label>Buscar Contrato</label>
        <ContractSearchSelect onAdd={handleValidate} />
      </Show>
      {/* listar info de contratos */}
      <InfoCreateMassiveTable onRemove={handleRemove} data={contracts} />
      {/* siguiente */}
      <Show condition={contracts.length > 0}>
        <div className="text-right">
          <hr />
          <Show
            condition={!isLoading}
            isDefault={
              <ButtonLoading loading title="Guardando..." color="primary" />
            }
          >
            <Button color="primary" onClick={handleSave}>
              <Save className="icon" />
            </Button>
          </Show>
        </div>
      </Show>
    </div>
  );
}
