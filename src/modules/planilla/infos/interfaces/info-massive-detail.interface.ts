import { InfoCreateMassiveDto } from "../dtos/info-create-massive.dto";


export interface InfoMassiveDetailInterface extends InfoCreateMassiveDto {
  fullname: string;
  typeCategoryName: string;
}