import { InfoEditParams } from "../domain/info.params";

export interface InfoEditInterface {
  id: number;
  body: InfoEditParams
}