import { useState } from "react";
import { toast } from "react-toastify";
import { IInfoEntity } from "../dtos/info.entity";
import { useLazyPaginateInfosQuery } from "../features/info.rtk";

export function useInfoValidate() {
  const [isValid, setIsValid] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [info, setInfo] = useState<IInfoEntity | undefined>();
  const [fetch] = useLazyPaginateInfosQuery();

  const validate = async (contractId: number, planillaId: number) => {
    setIsLoading(true);
    setIsValid(false);
    const result = await fetch({ planillaId, contractId, page: 1, limit: 1 })
      .unwrap()
      .then((data) => data)
      .catch(() => undefined);

    // validar
    if (!result) {
      toast.error(`Ocurrio un error al validar el contrato`);
      return setIsLoading(false);
    }

    // validar si no tiene configuración de pago
    const currentInfo = result.items[0];
    if (!currentInfo) {
      setIsValid(true);
      setInfo(undefined);
      return setIsLoading(false);
    }

    // revisar si está activo
    if (currentInfo.state) {
      setIsValid(false);
      setInfo(undefined);
      toast.error(`Ya existe una configuración de pago`);
      return setIsLoading(false);
    }

    setIsValid(true);
    setInfo(currentInfo);
    return setIsLoading(false);
  }


  return {
    isLoading,
    validate,
    isValid,
    info
  }
}