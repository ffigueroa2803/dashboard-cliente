/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { IFormInfoDto } from "../dtos/form-info.dto";
import { useEditInfoMutation } from "../features/info.rtk";

const dataDefaultCurrent: IFormInfoDto = {
  contractId: 0,
  planillaId: 0,
  codeAIRHSP: "",
  pimId: 0,
  bankId: 0,
  numberOfAccount: "",
  isSync: true,
  isEmail: false,
  labelId: null,
  state: true,
};

export const useInfoForm = (dataDefault: IFormInfoDto = dataDefaultCurrent) => {
  const [form, setForm] = useState<IFormInfoDto>(dataDefault);
  const [errors] = useState<any>({});
  const [fetch, { isLoading, isSuccess, isError, error }] = useEditInfoMutation();

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(dataDefault);

  const handle = (id: number) => {
    fetch({ id, body: form });
  }

  useEffect(() => {
    if (isSuccess) toast.success("Configuración de pago actualizada!");
  }, [isSuccess])

  useEffect(() => {
    if (isError) {
      const customError: any = error;
      toast.error(customError?.data?.message || "No se pudo actualizar el registro!")
    }
  }, [isError]);

  return {
    dataDefault,
    form,
    setForm,
    errors,
    handleForm,
    clearForm,
    handle,
    isLoading
  };
};
