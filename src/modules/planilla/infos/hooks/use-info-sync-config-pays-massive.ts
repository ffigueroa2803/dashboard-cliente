import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";
import { useFormik } from "formik";
import {
  InfoSyncConfigPayDefault,
  InfoSyncConfigPayYup,
} from "../dtos/info-sync-config-pay.dto";
import { useSyncMassiveMutation } from "../features/info.rtk";

export const useInfoSyncConfigPaysMassive = () => {
  const [fetch, { isLoading }] = useSyncMassiveMutation();

  const confirm = useConfirm();
  const formik = useFormik({
    initialValues: InfoSyncConfigPayDefault,
    validationSchema: InfoSyncConfigPayYup,
    onSubmit: (filters, helpers) =>
      confirm
        .alert({
          title: "Sincronizar Pagos",
          message: "¿Estás seguro en sincronizar la configuración de pagos?",
          labelSuccess: "Estoy seguro",
          labelError: "No",
        })
        .then(async () => {
          fetch(filters)
            .unwrap()
            .then(() => {
              helpers.resetForm();
              toast.success(`Sincronización completa!!!`);
            })
            .catch(() => toast.error(`No se pudo sincronizar!!!`));
        })
        .catch(() => null),
  });

  return {
    formik,
    isLoading,
  };
};
