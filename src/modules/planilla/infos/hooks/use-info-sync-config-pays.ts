import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";

const request = planillaRequest();

export const useInfoSyncConfigPays = (id: number) => {
  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const execute = () => {
    confirm
      .alert({
        title: "Sincronizar Pagos",
        message: "¿Estás seguro en sincronizar la configuración de pagos?",
        labelSuccess: "Estoy seguro",
        labelError: "No",
      })
      .then(async () => {
        setPending(true);
        toast.info("Sincronizando configuración");
        await request
          .post(`infos/${id}/process/syncConfigPays`)
          .then(() => {
            toast.dismiss();
            toast.success(`La configuración se sincronizó correctamente!`);
          })
          .catch(() => toast.error(`No se pudo sincronizar la configuración!`));
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    execute,
  };
};
