import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";
import { infoActions } from "../store";

const request = planillaRequest();

export const useInfoDelete = () => {
  const [pending, setPending] = useState<boolean>(false);
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async () => {
    return confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en eliminar la configuración de pago?",
        labelError: "Cancelar",
        labelSuccess: "Eliminar",
      })
      .then(async () => {
        setPending(true);
        setIsSuccess(false);
        setIsError(false);
        await request
          .destroy(`infos/${info?.id}`)
          .then(() => {
            setIsSuccess(true);
            toast.success("La configuración de pago se eliminó correctamente!");
            dispatch(infoActions.delete(info.id));
          })
          .catch((err) => {
            setIsError(true);
            const status = err?.response?.data?.status || undefined;
            if (!status) return toast.error("Algo salió mal, intente de nuevo");
          });
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    destroy,
    isSuccess,
    isError,
  };
};
