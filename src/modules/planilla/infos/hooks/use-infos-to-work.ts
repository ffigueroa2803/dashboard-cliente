import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { IInfoEntity } from "@modules/planilla/infos/dtos/info.entity";
import { PaginateDto } from "@services/dtos";
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";

const request = planillaRequest();

export const useInfosToWork = (
  id: number,
  { page, limit, querySearch, state }: PaginateDto
) => {
  const datosDefault: ResponsePaginateDto<IInfoEntity> = {
    items: [],
    meta: {
      itemsPerPage: limit || 30,
      totalItems: 0,
      totalPages: 0,
    },
  };

  const [pending, setPending] = useState<boolean>(false);
  const [datos, setDatos] =
    useState<ResponsePaginateDto<IInfoEntity>>(datosDefault);

  const execute = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("limit", `${limit}`);
    params.set("querySearch", querySearch || "");
    if (typeof state != "undefined") params.set("state", `${state}`);
    await request
      .get(`works/${id}/infos`, { params })
      .then((res) => setDatos(res.data))
      .catch(() => setDatos(datosDefault));
    setPending(false);
  };

  return {
    pending,
    execute,
    datos,
  };
};
