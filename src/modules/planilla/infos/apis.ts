import { planillaRequest } from "@services/planilla.request";
import { ICreateInfoDto } from "./dtos/create-info.dto";
import { IEditInfoDto } from "./dtos/edit-info.dto";
import { IInfoEntity } from "./dtos/info.entity";

const request = planillaRequest();

export const createInfo = async (
  payload: ICreateInfoDto
): Promise<IInfoEntity> => {
  // send request
  payload.labelId = payload.labelId || null;
  return request.post(`infos`, payload).then((res) => res.data);
};

export const findInfo = async (id: number) => {
  return await request.get(`infos/${id}`).then((res) => res.data);
};

export const editInfo = async (
  id: number,
  payload: IEditInfoDto
): Promise<IInfoEntity> => {
  payload.labelId = payload.labelId || null;
  return request.put(`infos/${id}`, payload).then((res) => res.data);
};
