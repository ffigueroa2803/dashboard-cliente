import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { Afp } from "@modules/planilla/afp/dtos/afp.entity";
import { afpActiones } from "../store";

const request = scaleRequest();

export const useAfpFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);

  const fetch = async (id: number): Promise<Afp> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`afps/${id}`)
        .then((res) => {
          dispatch(afpActiones.find(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };
  return {
    pending,
    fetch,
  };
};
