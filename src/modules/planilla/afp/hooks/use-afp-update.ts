/* eslint-disable no-async-promise-executor */
import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { Afp } from "../dtos/afp.entity";
import { afpActiones } from "../store";

const request = scaleRequest();

export const useAfpUpdate = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<Boolean>(false);

  const fetch = async (id: number, payload: Afp): Promise<Afp> => {
    const parset = Object.assign(payload, {
      percent: parseFloat(`${payload.percent}`),
      aportPercent: parseFloat(`${payload.aportPercent}`),
      primaLimit: parseFloat(`${payload.primaLimit}`),
      primaPercent: parseFloat(`${payload.primaPercent}`),
    });
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`afps/${id}`, parset)
        .then((res) => {
          dispatch(afpActiones.find(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };
  return {
    pending,
    fetch,
  };
};
