import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { afpActiones } from "../store";

const request = scaleRequest();

export const useAfpCode = (defaultQuerySearch?: string) => {
  const dispatch = useDispatch();

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );
  const [pending, setPending] = useState<boolean>(false);

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`afps/toCodes`, { params })
      .then((res) => dispatch(afpActiones.paginateCode(res.data)))
      .catch(() => dispatch(afpActiones.paginateCode([])));
    setPending(false);
  };

  return {
    pending,
    page,
    limit,
    querySearch,
    setQuerySearch,
    fetch,
  };
};
