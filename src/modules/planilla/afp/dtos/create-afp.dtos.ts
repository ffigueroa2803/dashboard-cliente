export interface ICreateAfpDto {
  code: number,
  name: string,
  typeAfpCode: number,
  typeAfp: string,
  typeDiscountId: number,
  percent: number,
  aportDiscountId: number,
  aportPercent: number,
  primaDiscountId: number,
  primaPercent: number,
  primaLimit: number,
  isPrivate: boolean,
  state: boolean

}