export interface IAfpCodeEntity {
  code: string;
  name: string;
  isPrivate: number;
  ids: string[];
  typeAfpCodes: string;
  typeDiscountIds: string;
  aportDiscountIds: string;
  primaDiscountIds: string;
}
