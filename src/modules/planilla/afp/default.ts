import { Store } from "redux";
import { afpActiones } from "@modules/planilla/afp/store";
import { getAfp } from "@modules/planilla/afp/apis";

export const configDefaultServer = async (ctx: any, store: Store) => {
  const page = ctx.query?.page || 1;
  const querySearch = ctx.query?.querySearch || "";
  const limit = ctx.query?.limit || 30;
  const result = await getAfp({ page, querySearch, limit });
  if (!result?.err) store.dispatch(afpActiones.paginate(result));
};
