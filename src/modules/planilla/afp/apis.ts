import { ICreateAfpDto } from './dtos/create-afp.dtos';
import { scaleRequest } from '@services/scale.request'
import { PaginateDto } from '@services/dtos'
import { Afp } from './dtos/afp.entity'

const request = scaleRequest()

export const getAfp = async ({ page, limit, querySearch }: PaginateDto) => {
  const params = new URLSearchParams();
  params.set('page', `${page}`)
  params.set('querySearch', querySearch || '')
  params.set('limit', `${limit || 30}`)
  return await request.get(`afps`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }))

}
export const findAfp = async (id: number): Promise<Afp> => {
  return await request.get(`afps/${id}`)
    .then(res => res.data)
    .catch(() => ({ err: true }))
}
export const createAfp = async (payload: ICreateAfpDto): Promise<Afp> => {
  return await request.post(`afps`, payload)
    .then(res => res.data)
    .catch(() => ({ err: true }))
}