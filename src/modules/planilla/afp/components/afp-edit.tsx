/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { Afp } from "../dtos/afp.entity";
import { Save, X } from "react-feather";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { AfpForm } from "../components/afp-form";
import { ICreateAfpDto } from "../dtos/create-afp.dtos";
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import { useAfpUpdate } from "../hooks/use-afp-update";
import { toast } from "react-toastify";

interface IProps {
  onClose: () => void;
  onSave: (afp: Afp) => void;
}

export const AfpEdit = ({ onSave, onClose }: IProps) => {
  const { afp } = useSelector((state: RootState) => state.afp);
  const [form, setForm] = useState<Afp>(afp);
  const [pending, setPending] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const { fetch } = useAfpUpdate();

  const handleOnChange = ({ name, value }: IInputHandle) => {
    setIsEdit(true);
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setPending(true);
    await fetch(afp.id, form)
      .then((res) => {
        if (typeof onSave == "function") onSave(res);
        onClose();
        toast.success(`Los datos se guardarón correctamente!`);
      })
      .catch(() => {
        onClose();
        toast.dismiss();
        toast.error(`No se pudo guardar los cambios`);
      });
    setPending(false);
  };
  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Editar Ley Socials</ModalHeader>
      <ModalBody>
        <AfpForm
          disabled={pending}
          form={form as ICreateAfpDto}
          onChange={handleOnChange}
        />

        <Show condition={isEdit}>
          <div className="text-right">
            <Button
              color="danger"
              onClick={() => {
                setIsEdit(false);
                setForm(afp);
              }}
              disabled={pending}>
              <X className="icon" />
            </Button>

            <Button
              color="primary"
              className="ml-2"
              onClick={handleSave}
              disabled={pending}>
              <Save className="icon" />
            </Button>
          </div>
        </Show>
      </ModalBody>
    </Modal>
  );
};
