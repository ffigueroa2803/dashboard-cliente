/* eslint-disable no-unused-vars */
import React from "react";
import { InputDto } from "@services/dtos";
import { ICreateAfpDto } from "../dtos/create-afp.dtos";
import { FormGroup, Input } from "reactstrap";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";
import { Show } from "@common/show";
import { SelectBasic } from "@common/select/select-basic";
interface IProps {
  form: ICreateAfpDto;
  disabled?: boolean;
  onChange: (input: InputDto) => void;
}

export const AfpForm = ({ form, onChange, disabled }: IProps) => {
  return (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            className="capitalize"
            name="code"
            onChange={({ target }) => onChange(target)}
            value={form?.code || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            className="capitalize"
            name="name"
            onChange={({ target }) => onChange(target)}
            value={form?.name || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>ID-TIPO-MANUAL</label>
          <Input
            type="number"
            className="capitalize"
            name="typeAfpCode"
            onChange={({ target }) => onChange(target)}
            value={form?.typeAfpCode || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Tipo</label>
          <Input
            type="text"
            className="capitalize"
            name="typeAfp"
            onChange={({ target }) => onChange(target)}
            value={form?.typeAfp || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Tip. Descuento</label>
          <TypeDiscountSelect
            name="typeDiscountId"
            value={form?.typeDiscountId || ""}
            onChange={(a) => onChange(a)}
            defaultQuerySearch={""}
          />
        </FormGroup>

        <FormGroup>
          <label> Porcentaje (%)</label>
          <Input
            type="text"
            className="capitalize"
            name="percent"
            onChange={({ target }) => onChange(target)}
            value={form?.percent || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Modo</label>
          <SelectBasic
            name="isPrivate"
            value={form.isPrivate}
            placeholder="Modo"
            options={[
              { label: "Público", value: false },
              { label: "Privado", value: true },
            ]}
            onChange={onChange}
          />
        </FormGroup>

        <Show condition={form?.isPrivate}>
          <>
            <hr />
            <h6>Configuración Aporte Obligatorio</h6>
            <hr />
            <FormGroup>
              <label>Tip. Descuento</label>
              <TypeDiscountSelect
                name="aportDiscountId"
                value={form?.aportDiscountId || ""}
                onChange={(a) => onChange(a)}
                defaultQuerySearch={""}
              />
            </FormGroup>

            <FormGroup>
              <label>Aporte(%)</label>
              <Input
                type="text"
                className="capitalize"
                name="aportPercent"
                onChange={({ target }) => onChange(target)}
                value={form?.aportPercent || ""}
              />
            </FormGroup>
            <hr />
            <h6>Configuración Prima Seguro</h6>
            <hr />

            <FormGroup>
              <label>Tip. Descuento</label>
              <TypeDiscountSelect
                name="primaDiscountId"
                value={form?.primaDiscountId || ""}
                onChange={(a) => onChange(a)}
                defaultQuerySearch={""}
              />
            </FormGroup>

            <FormGroup>
              <label>Prima(%)</label>
              <Input
                type="text"
                className="capitalize"
                name="primaPercent"
                onChange={({ target }) => onChange(target)}
                value={form?.primaPercent || ""}
              />
            </FormGroup>

            <FormGroup>
              <label>Prima Limite(%)</label>
              <Input
                type="text"
                className="capitalize"
                name="primaLimit"
                onChange={({ target }) => onChange(target)}
                value={form?.primaLimit || ""}
              />
            </FormGroup>
          </>
        </Show>
        <hr />
      </div>
    </div>
  );
};
