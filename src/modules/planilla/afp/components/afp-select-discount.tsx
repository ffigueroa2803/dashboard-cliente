import React, { useState, useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";
import { useTypeDiscount } from "@modules/planilla/type_descuento/hooks/use-type-discount";
interface Iprops {
  name: string;
  defaultQuerySearch: string;
  onChange: (option: any) => void;
  value: number | string;
}
export const AfpSelectDiscount = ({
  defaultQuerySearch,
  name,
  onChange,
  value,
}: Iprops) => {
  const { type_descuentos } = useSelector(
    (state: RootState) => state.type_descuento
  );

  const typeDiscount = useTypeDiscount(defaultQuerySearch);

  const settingData = (row: TypeDescuento) => {
    return {
      label: `${row.description}`.toLocaleLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    typeDiscount.fetch();
  }, [typeDiscount]);
  return (
    <SelectBasic
      name={name}
      value={value}
      options={type_descuentos?.items?.map((row) => settingData(row))}
      onChange={onChange}
    />
  );
};
