/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Modal, ModalBody, ModalHeader, Button } from "reactstrap";
import { ICreateAfpDto } from "@modules/planilla/afp/dtos/create-afp.dtos";
import { Save } from "react-feather";
import { Afp } from "@modules/planilla/afp/dtos/afp.entity";
import { createAfp } from "../apis";
import { toast } from "react-toastify";
import { IInputHandle } from "@common/dtos/input-handle";
import { AfpForm } from "./afp-form";

interface IProps {
  onClose: () => void;
onSave: (afp: Afp) => void;
}

const dataDefault: ICreateAfpDto = {
  aportDiscountId: 0,
  aportPercent: 0,
  code: 0,
  isPrivate: false,
  name: "",
  percent: 0,
  primaDiscountId: 0,
  primaLimit: 0,
  primaPercent: 0,
  state: true,
  typeAfp: "",
  typeAfpCode: 0,
  typeDiscountId: 0,
};
export const AfpCreate = ({ onClose, onSave }: IProps) => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ICreateAfpDto>(dataDefault);

  const handleOnChange = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    setPending(true);
    await createAfp(form as any)
      .then((data) => {
        setForm(dataDefault);
        toast.success(`Los datos se guardarón correctamente!`);
        if (typeof onSave == "function") onSave(data);
        onClose();
      })
      .catch((err) => {
        onClose();
        toast.dismiss();
        toast.error(`No se pudo guardar los cambios`);
      });
    setPending(false);
  };

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Categoria</ModalHeader>
      <ModalBody>
        <AfpForm disabled={pending} form={form} onChange={handleOnChange} />
        <div className="text-right">
          <Button color="primary" onClick={handleSave} disabled={pending}>
            <Save size={17} />
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
