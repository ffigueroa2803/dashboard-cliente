import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IAfpCodeEntity } from "../dtos/afp-code.entity";
import { useAfpCode } from "../hooks/use-afp-code";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const AfpCodeSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { afpCodes } = useSelector((state: RootState) => state.afp);

  const afpCode = useAfpCode(defaultQuerySearch);

  const settingsData = (row: IAfpCodeEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.code,
      onj: row,
    };
  };

  useEffect(() => {
    afpCode.fetch();
  }, [afpCode.querySearch]);

  return (
    <>
      <SelectBasic
        name={name}
        value={value}
        options={afpCodes?.map((row) => settingsData(row))}
        onChange={onChange}
        onSearch={afpCode.setQuerySearch}
      />
    </>
  );
};
