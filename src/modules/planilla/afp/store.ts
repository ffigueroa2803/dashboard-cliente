import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IAfpCodeEntity } from "./dtos/afp-code.entity";
import { Afp } from "./dtos/afp.entity";

export interface AfpState {
  afps: ResponsePaginateDto<Afp>;
  afp: Afp ;
  afpCodes: IAfpCodeEntity[];
  afpCode: IAfpCodeEntity;
}

const initialState: AfpState = {
  afps: {
    meta: {
      totalItems: 0,
      itemsPerPage: 0,
      totalPages: 0,
    },
    items: [],
  },
  afp: {} as any,
  afpCodes: [],
  afpCode: {} as any,
};

const AfpStore = createSlice({
  name: "planilla@afp",
  initialState,
  reducers: {
    paginate: (state, { payload }: PayloadAction<ResponsePaginateDto<Afp>>) => {
      state.afps = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<Afp>) => {
      state.afp = payload;
      return state;
    },
    paginateCode: (state, { payload }: PayloadAction<IAfpCodeEntity[]>) => {
      state.afpCodes = payload;
      return state;
    },
    findCode: (state, { payload }: PayloadAction<IAfpCodeEntity>) => {
      state.afpCode = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.afp };
    },
  },
});
export const afpReducer = AfpStore.reducer;

export const afpActiones = AfpStore.actions;
