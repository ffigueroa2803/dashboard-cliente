export interface CronogramaImportConceptInterface {
  NUMERO_DOCUMENTO: string;
  CONCEPTO: string;
  REFERENCIA: string;
  MONTO: string;
}
