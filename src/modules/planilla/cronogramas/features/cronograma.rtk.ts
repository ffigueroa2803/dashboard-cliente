import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { RootState } from "@store/store";
import { CronogramaImportRemunerationDto } from "../dtos/cronograma-import-remuneration.dto";
import { CronogramaImportDiscountDto } from "../dtos/cronograma-import-discount.dto";

const baseUrl = process.env.NEXT_PUBLIC_PLANILLA_URL || "";

export const cronogramaRtk = createApi({
  reducerPath: "cronogramaRtk",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).auth.token;
      if (token) headers.set("authorization", `Bearer ${token}`);
      return headers;
    },
  }),
  endpoints: (builder) => ({
    importRemuneration: builder.mutation<any, CronogramaImportRemunerationDto>({
      query: ({ id, remunerations }) => ({
        url: `cronogramas/${id}/process/importRemunerations`,
        method: "PUT",
        body: { remunerations },
      }),
    }),
    importDiscount: builder.mutation<any, CronogramaImportDiscountDto>({
      query: ({ id, discounts }) => ({
        url: `cronogramas/${id}/process/importDiscounts`,
        method: "PUT",
        body: { discounts },
      }),
    }),
  }),
});

export const { useImportRemunerationMutation, useImportDiscountMutation } =
  cronogramaRtk;
