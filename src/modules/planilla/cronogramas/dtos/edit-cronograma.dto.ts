export interface IEditCronogramaDto {
  year: number;
  month: number;
  campusId: number;
  observation?: string;
  planillaId: number;
  adicional: number;
  remanente: boolean;
}
