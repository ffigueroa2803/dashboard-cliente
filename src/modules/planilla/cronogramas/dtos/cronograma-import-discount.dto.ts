export interface CronogramaImportDiscountBodyDto {
  documentNumber: string;
  typeDiscountId: number;
  typeDiscountName: string;
  amount: number;
}

export interface CronogramaImportDiscountDto {
  id: number;
  discounts: CronogramaImportDiscountBodyDto[];
}
