import { ICampusEntity } from "@modules/auth/campuses/dtos/campus.entity";
import { IPlanillaEntity } from "@modules/planilla/planillas/dtos/planilla.entity";

export interface ICronogramaEntity {
  id: number;
  year: number;
  month: number;
  numberOfDays: number;
  calcOfDays: number;
  adicional: number;
  observation?: string;
  token: string;
  campusId: number;
  planillaId: number;
  selloId: number;
  selloUrl?: string;
  remanente: boolean;
  sendEmail: boolean;
  processing: boolean;
  countToken: number;
  historialsCount: number;
  isPlame: boolean;
  state: boolean;
  createdAt: string;
  campus?: ICampusEntity;
  planilla?: IPlanillaEntity;
}

export const cronogramaEntityName = "CronogramaEntity";
