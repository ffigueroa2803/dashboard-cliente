import { DateTime } from "luxon";

export class CronogramaSerialize {
  private createdAt!: string;

  get displayCreatedAt() {
    return DateTime.fromISO(`${this.createdAt}`).toFormat("dd/MM/yyyy");
  }
}
