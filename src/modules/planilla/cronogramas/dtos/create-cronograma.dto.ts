export interface ICreateCronogramaDto {
  year: number;
  month: number;
  campusId: number;
  observation?: string;
  planillaId: number;
  adicional: boolean;
  remanente: boolean;
}
