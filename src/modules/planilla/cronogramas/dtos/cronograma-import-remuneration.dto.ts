export interface CronogramaImportRemunerationBodyDto {
  documentNumber: string;
  typeRemunerationId: number;
  typeRemunerationName: string;
  amount: number;
}

export interface CronogramaImportRemunerationDto {
  id: number;
  remunerations: CronogramaImportRemunerationBodyDto[];
}
