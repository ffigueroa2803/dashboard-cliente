import { PaginateDto } from "@services/dtos";

export interface IFilterCronograma extends PaginateDto {
  year: number;
  month: number;
  principal?: boolean;
  isPlame?: boolean;
}

export interface IFilterHistorialToCronograma {
  querySearch: string;
}

export interface IFilterHistorialToCronogramaPaginateDto extends PaginateDto {
  pimIds?: number[];
}
