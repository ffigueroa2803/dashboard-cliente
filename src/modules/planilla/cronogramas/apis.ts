import { planillaRequest } from "@services/planilla.request";
import { ICronogramaEntity } from "./dtos/cronograma.entity";
import { ICreateCronogramaDto } from "./dtos/create-cronograma.dto";
import { IEditCronogramaDto } from "./dtos/edit-cronograma.dto";
import {
  IFilterCronograma,
  IFilterHistorialToCronogramaPaginateDto,
} from "./dtos/filter-cronogramas.dto";
import { findInfosToCronogramaDto } from "../infos/dtos/findInfoToCronograma.dto";

const request = planillaRequest();

export const getCronogramas = async ({
  page,
  querySearch,
  limit,
  year,
  month,
  principal,
  isPlame,
}: IFilterCronograma) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  params.set("year", `${year}`);
  params.set("month", `${month}`);
  if (typeof principal != "undefined") params.set("principal", `${principal}`);
  if (typeof isPlame != "undefined") params.set("isPlame", `${isPlame}`);
  return await request.get(`cronogramas`, { params }).then((res) => res.data);
};

export const findCronograma = async (
  id: number
): Promise<ICronogramaEntity> => {
  return await request
    .get(`cronogramas/${id}`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createCronograma = async (
  payload: ICreateCronogramaDto
): Promise<ICronogramaEntity> => {
  return await request.post(`cronogramas`, payload).then((res) => res.data);
};

export const editCronograma = async (
  id: number,
  payload: IEditCronogramaDto
): Promise<ICronogramaEntity> => {
  return await request
    .put(`cronogramas/${id}`, payload)
    .then((res) => res.data);
};

export const deleteCronograma = async (
  id: number
): Promise<{ deleted: boolean }> => {
  return await request.destroy(`cronogramas/${id}`).then((res) => res.data);
};

export const findInfosToCronograma = async (
  id: number,
  payload: findInfosToCronogramaDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${payload.page}`);
  params.set("querySearch", payload.querySearch || "");
  params.set("limit", `${payload.limit || 30}`);
  // filter  typeCargo
  if (payload.typeCargoId) {
    params.set("typeCargoId", `${payload.typeCargoId}`);
  }
  // filter condition
  if (payload.condition) {
    params.set("condition", `${payload.condition}`);
  }
  // filter typeCategory
  if (payload.typeCategoryId) {
    params.set("typeCategoryId", `${payload.typeCategoryId}`);
  }
  // respons
  return await request
    .get(`cronogramas/${id}/infos`, { params })
    .then((res) => res.data);
};

export const addInfosToCronograma = async (id: number, infoIds: number[]) => {
  return await request
    .post(`cronogramas/${id}/historials`, { ids: infoIds })
    .then((res) => res.data);
};

export const findHistorialsToCronograma = async (
  id: number,
  { page, querySearch, limit, pimIds }: IFilterHistorialToCronogramaPaginateDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (pimIds?.length) {
    pimIds?.forEach((pimId, index) =>
      params.append(`pimIds[${index}]`, `${pimId}`)
    );
  }
  // send
  return await request
    .get(`cronogramas/${id}/historials`, { params })
    .then((res) => res.data);
};

export const removeHistorialsToCronograma = async (
  id: number,
  historialIds: number[]
) => {
  const data = { ids: historialIds };
  return await request
    .destroy(`cronogramas/${id}/historials`, { data })
    .then((res) => res.data);
};

export const processCronograma = async (
  id: number
): Promise<ICronogramaEntity> => {
  return await request
    .post(`cronogramas/${id}/process`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
