/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useContext, useEffect, useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import {
  cronogramaEntityName,
  ICronogramaEntity,
} from "../dtos/cronograma.entity";
import {
  Search,
  ArrowRight,
  UserPlus,
  Info,
  UserMinus,
  File,
  Trash,
  ExternalLink,
  Edit,
} from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import { plainToClass } from "class-transformer";
import { CronogramaSerialize } from "../dtos/cronograma.serialize";
import { DateTime } from "luxon";
import { SelectBasic } from "@common/select/select-basic";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { historialEntityName } from "@modules/planilla/historials/dtos/historial.entity";
import { useHistorialExport } from "@modules/planilla/historials/hooks/use-historial-export";
import { LoadingContent } from "@common/loading/components/loading-content";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (
  year: number,
  month: number,
  principal: boolean
) => void;
// eslint-disable-next-line no-unused-vars
declare type onClick = (cronograma: ICronogramaEntity, action: string) => void;

declare type cronogramaActions = "VIEW" | "DELETE" | "REPORT";

interface IProps {
  onlyInfo?: boolean;
  selectableRows?: boolean;
  isOption?: boolean;
  btnExport?: any;
  hidden?: cronogramaActions[];
  data: ICronogramaEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  defaultYear?: number;
  defaultMonth?: number;
  defaultPrincipal?: boolean;
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: onClick;
  onSelectedRowsChange?: any;
  clearSelectedRows?: boolean;
}

export const CronogramaTable = ({
  data,
  isOption = true,
  selectableRows = false,
  btnExport,
  hidden = [],
  perPage,
  totalItems,
  loading,
  onlyInfo,
  defaultYear,
  defaultMonth,
  defaultPrincipal,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  onClick,
}: IProps) => {
  const currentDate = DateTime.now();

  const ability = useContext(CaslContext);

  const [isEvent, setIsEvent] = useState<boolean>(false);
  const [isPrincipal, setIsPrincipal] = useState<boolean>(
    defaultPrincipal || false
  );
  const [year, setYear] = useState<number>(defaultYear || currentDate.year);
  const [month, setMonth] = useState<number>(defaultMonth || currentDate.month);

  const historialExport = useHistorialExport();

  const columns = useMemo(() => {
    const options = [
      {
        name: "#ID",
        grow: true,
        selector: (row: ICronogramaEntity) => row.id,
      },
      {
        name: "Planilla",
        wrap: true,
        selector: (row: ICronogramaEntity) => {
          return (
            <span className="capitalize">
              <span>{row?.planilla?.name || ""}</span>
              <Show condition={row.adicional > 0}>
                <span className="ml-1">
                  <ArrowRight className="icon mr-1" />
                  <span className="badge badge-sm badge-primary mr-1">
                    {row?.adicional || ""}
                  </span>
                  Adicional
                </span>
              </Show>
            </span>
          );
        },
      },
      {
        name: "F. Creación",
        selector: (row: ICronogramaEntity) => {
          const cronogramaSerialize = plainToClass(CronogramaSerialize, row);
          return cronogramaSerialize.displayCreatedAt || "";
        },
      },
      {
        name: "N° Trab.",
        grow: true,
        selector: (row: ICronogramaEntity) => (
          <span className="badge badge-sm badge-dark">
            {row.historialsCount || 0}
          </span>
        ),
      },
      {
        name: "Plame",
        grow: true,
        selector: (row: ICronogramaEntity) => (
          <span className={`badge badge-${row.isPlame ? "dark" : "light"}`}>
            {row.isPlame ? "SI" : "NO"}
          </span>
        ),
      },
      {
        name: "Estado",
        grow: true,
        selector: (row: ICronogramaEntity) => (
          <span className={`badge badge-${row.state ? "success" : "danger"}`}>
            {row.state ? "En Curso" : "Cerrada"}
          </span>
        ),
      },
    ];
    // agregar options
    if (isOption) {
      options.push({
        name: "Opciones",
        wrap: true,
        center: true,
        selector: (row: ICronogramaEntity) => (
          <span>
            <Show condition={!hidden.includes("VIEW")}>
              <Info
                className="icon cursor-pointer"
                onClick={() => handleClick(row, "INFO")}
              />
            </Show>
            {/* only info */}
            <Show condition={!onlyInfo}>
              {/* state */}
              <Show condition={row.state}>
                {/* Agregar Historial */}
                <Show
                  condition={ability.can(
                    PermissionAction.CREATE,
                    historialEntityName
                  )}>
                  <UserPlus
                    className="icon cursor-pointer ml-1"
                    onClick={() => handleClick(row, "ADD")}
                  />
                </Show>
                {/* Eliminar Historial */}
                <Show
                  condition={ability.can(
                    PermissionAction.DELETE,
                    historialEntityName
                  )}>
                  <UserMinus
                    className="icon cursor-pointer ml-1"
                    onClick={() => handleClick(row, "REMOVE")}
                  />
                </Show>
              </Show>

              <Show
                condition={
                  !hidden.includes("REPORT") &&
                  ability.can(PermissionAction.REPORT, cronogramaEntityName)
                }>
                <File
                  className="icon cursor-pointer ml-1"
                  onClick={() => handleClick(row, "REPORT")}
                />
              </Show>

              <Show
                condition={
                  !hidden.includes("DELETE") &&
                  row.state &&
                  ability.can(PermissionAction.DELETE, cronogramaEntityName)
                }>
                <Trash
                  className="icon cursor-pointer ml-1"
                  onClick={() => handleClick(row, "DELETE")}
                />
              </Show>

              {/* edit cronograma */}
              <Show
                condition={ability.can(
                  PermissionAction.UPDATE,
                  cronogramaEntityName
                )}>
                <Edit
                  className="icon cursor-pointer ml-1"
                  onClick={() => handleClick(row, "EDIT")}
                />
              </Show>
            </Show>
          </span>
        ),
      } as any);
    }
    // response
    return options;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(year, month, isPrincipal);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (cronograma: ICronogramaEntity, action: string) => {
    if (typeof onClick == "function") onClick(cronograma, action);
  };

  const handleSelect = (value: boolean) => {
    setIsPrincipal(value);
    setIsEvent(true);
  };

  const handleExport = () => {
    historialExport.generateExcel().catch(() => null);
  };

  useEffect(() => {
    if (defaultYear) setYear(defaultYear);
    if (defaultMonth) setMonth(defaultMonth);
    if (defaultPrincipal) setIsPrincipal(defaultPrincipal);
  }, [defaultYear, defaultMonth]);

  useEffect(() => {
    if (isEvent) handleSearch();
  }, [isEvent]);

  useEffect(() => {
    if (isEvent) setIsEvent(false);
  }, [isEvent]);

  useEffect(() => {
    if (year) historialExport.setYear(year);
  }, [year]);

  useEffect(() => {
    if (month) historialExport.setMonth(month);
  }, [month]);

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="3 col-6" className="mb-2">
              <Input
                type="number"
                value={year || ""}
                name="year"
                onChange={({ target }) => setYear(parseInt(`${target.value}`))}
              />
            </Col>
            <Col md="2 col-6" className="mb-2">
              <Input
                type="number"
                value={month || ""}
                name="month"
                onChange={({ target }) => setMonth(parseInt(`${target.value}`))}
              />
            </Col>
            <Col md="3 col-6" className="mb-2">
              <SelectBasic
                isClearable={false}
                name="isPrincipal"
                value={isPrincipal}
                options={[
                  { label: "Principal", value: true },
                  { label: "Especial", value: false },
                ]}
                onChange={(obj) => handleSelect(obj.value)}
              />
            </Col>
            <Col md="2 col-6" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}>
                <Search size={15} />
              </Button>
            </Col>
            <Col md="2 col-6" className="mb-2">
              <Show condition={!btnExport} isDefault={btnExport}>
                <Button
                  color="success"
                  block
                  disabled={loading}
                  onClick={handleExport}>
                  <ExternalLink className="icon" />
                </Button>
              </Show>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        selectableRows={selectableRows}
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
      />
      {/* loading */}
      <LoadingContent loading={historialExport.pending} />
    </>
  );
};
