/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useCronogramaPim } from "../hooks/use-cronograma-pim";
import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const CronogramaPimSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { cronograma, pims } = useSelector(
    (state: RootState) => state.cronograma
  );

  const cronogramaPim = useCronogramaPim(defaultQuerySearch);

  const settingsData = (row: IPimEntity) => {
    return {
      label: `${row.code} - ${row.cargo?.name || ""} [ ${
        row.cargo?.extension || ""
      } ] - ${row.year}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    if (cronograma.id) cronogramaPim.fetch();
  }, [cronograma, cronogramaPim.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={pims?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={cronogramaPim.setQuerySearch}
    />
  );
};
