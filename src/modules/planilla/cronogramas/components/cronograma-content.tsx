import React, { useContext, useState } from "react";
import { CronogramaAlerts } from "./cronograma-alerts";
import { CronogramaTab } from "./cronograma-tab";
import { CronogramaContext, CronogramaProvider } from "../cronograma.context";
import { CronogramaHeader } from "./cronograma-header";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { CronogramaFilter } from "./cronograma-filter";
import { FloatButton } from "@common/button/float-button";
import { Repeat } from "react-feather";
import { CronogramaSwitch } from "./cronograma-switch";

const NoFound = () => {
  const { resetFilter } = useContext(CronogramaContext);
  return <RegisterEmptyError onRefresh={resetFilter} />;
};

export const CronogramaContent = () => {
  const { historials } = useSelector((state: RootState) => state.historial);

  const { isLoading } = useContext(CronogramaContext);

  const [isSwitch, setIsSwitch] = useState<boolean>(false);

  return (
    <CronogramaProvider>
      {/* alerts */}
      <CronogramaAlerts />
      <Show
        condition={historials?.meta?.totalItems > 0}
        isDefault={
          <Show condition={!isLoading}>
            <CronogramaFilter />
            <NoFound />
          </Show>
        }>
        {/* header */}
        <CronogramaHeader />
        {/* tabs */}
        <CronogramaTab />
      </Show>
      {/* button change cronograma */}
      <FloatButton
        color="info"
        onClick={() => setIsSwitch(true)}
        icon={<Repeat className="icon" />}
        title="Cambiar de Cronograma"
      />
      {/* switch cronograma */}
      <CronogramaSwitch isOpen={isSwitch} onClose={() => setIsSwitch(false)} />
    </CronogramaProvider>
  );
};
