import React, { useContext } from "react";
import { Search } from "react-feather";
import { Button, Col, Form, Input, Row } from "reactstrap";
import { CronogramaContext } from "../cronograma.context";

export const CronogramaFilter = () => {
  const { filters, changeFilter, searchData } = useContext(CronogramaContext);

  const handleSubmit = (e: any) => {
    e.preventDefault();
    searchData();
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Row className="mb-4">
        <Col md="4">
          <Input
            name="querySearch"
            value={filters.querySearch}
            onChange={({ target }) => changeFilter(target)}
            autoFocus
            style={{
              background: "rgba(255, 255, 255, 1)",
              color: "var(--indigo)",
            }}
          />
        </Col>
        <Col>
          <Button color="primary" onClick={searchData}>
            <Search className="icon" />
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
