import { Show } from "@common/show";
import { RootState } from "@store/store";
import React from "react";
import { Mail, Settings } from "react-feather";
import { useSelector } from "react-redux";
import { Alert } from "reactstrap";

export const CronogramaAlerts = () => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { historial } = useSelector((state: RootState) => state.historial);

  if (cronograma.state) {
    return (
      <Show condition={!historial.isSync}>
        <Alert color="warning">
          <b className="text-dark">
            <span className="mr-1">
              <Settings className="icon mb-2" />
              Sinconización de configuraciones deshabilitadas
            </span>
          </b>
          <div>
            <small className="ml-2 text-muted">
              Editar [Contrato, Obligaciones, Aportaciones, Afiliaciones]
            </small>
            <br />
            <small className="ml-2 text-muted">
              Eliminar [Obligaciones, Aportaciones, Afiliaciones]
            </small>
          </div>
        </Alert>
      </Show>
    );
  }

  return (
    <>
      <Show condition={cronograma.isPlame}>
        <Alert color="dark">
          <div>Planilla aplica al PDT-PLAME</div>
        </Alert>
      </Show>
      {/* info email */}
      <Alert color="info">
        <b>
          <span className="mr-1">El envío de boletas al correo son:</span>
          <span className="badge badge-sm badge-dark">5:00 am/8:00 pm</span>
          <div>
            <small>Solo se enviarán las boletas verificadas</small>
          </div>
        </b>
      </Alert>
      {/* boleta enviada */}
      <Show condition={historial?.sendEmail}>
        <Alert color="primary">
          <b>
            <span className="mr-1">
              <Mail className="icon mb-2" /> Boleta Enviada [
              {historial?.sendEmailContact || "N/A"}]
            </span>
          </b>
        </Alert>
      </Show>
    </>
  );
};
