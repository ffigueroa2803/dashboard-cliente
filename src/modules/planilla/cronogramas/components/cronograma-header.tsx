import { Show } from "@common/show";
import { RootState } from "@store/store";
import React, { useContext } from "react";
import { ArrowLeft, ArrowRight, Award, Filter, X } from "react-feather";
import { useSelector } from "react-redux";
import { Card, CardBody, Col, Row } from "reactstrap";
import { CronogramaContext } from "../cronograma.context";
import { CronogramaFilter } from "./cronograma-filter";
import { CronogramaOptions } from "./cronograma-options";

export const CronogramaHeader = () => {
  const {
    nextPage,
    prevPage,
    isLoading,
    canNext,
    canPrev,
    setIsFilter,
    isFilter,
    resetFilter,
    page,
  } = useContext(CronogramaContext);

  const { historial, historials } = useSelector(
    (state: RootState) => state.historial
  );

  return (
    <>
      <Card>
        <CardBody>
          {/* prev page */}
          <Show condition={canPrev}>
            <ArrowLeft
              className={isLoading ? "text-muted" : "cursor-pointer"}
              style={{
                position: "absolute",
                left: "10px",
                top: "35%",
                width: "50px",
              }}
              onClick={prevPage}
            />
          </Show>
          <Row className="pl-5 pr-5">
            <Col md="9">
              <h6 className="disabled-selection mt-2">
                <Show condition={historial.tokenVerify?.length > 0}>
                  <b>
                    <Award className="icon" color="#ff5722" />
                  </b>
                </Show>
                {page} DE {historials.meta?.totalPages || 0} /
                <b className="ml-2 uppercase">
                  <span className="text-primary">
                    {'"'}
                    {historial?.info?.contract?.work?.person?.fullName || "N/A"}
                    {'"'}
                  </span>
                  <Show
                    condition={!isFilter}
                    isDefault={
                      <span className="cursor-pointer" onClick={resetFilter}>
                        <X className="icon text-danger" />
                      </span>
                    }>
                    <Filter
                      className="icon cursor-pointer text-primary"
                      onClick={() => setIsFilter(true)}
                    />
                  </Show>
                </b>
              </h6>
            </Col>
            <Col md="3 text-center">
              <CronogramaOptions />
            </Col>
          </Row>
          {/* next page */}
          <Show condition={canNext}>
            <ArrowRight
              className={isLoading ? "text-muted" : "cursor-pointer"}
              style={{
                position: "absolute",
                right: "10px",
                top: "35%",
                width: "50px",
              }}
              onClick={nextPage}
            />
          </Show>
        </CardBody>
      </Card>
      {/* filtros */}
      <Show condition={isFilter}>
        <CronogramaFilter />
      </Show>
    </>
  );
};
