/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { createCronograma } from "../apis";
import { ICreateCronogramaDto } from "../dtos/create-cronograma.dto";
import { ICronogramaEntity } from "../dtos/cronograma.entity";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (cronograma: ICronogramaEntity) => void;
}

const currentDate = DateTime.now();

export const CronogramaEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const [pending, setPending] = useState<boolean>(false);

  const defaultData: ICreateCronogramaDto = {
    year: currentDate.year,
    month: currentDate.month,
    campusId: 0,
    observation: "",
    planillaId: 0,
    adicional: false,
    remanente: false,
  };

  const [form, setForm] = useState<ICreateCronogramaDto>(defaultData);

  const handleChange = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    toast.dismiss();
    setPending(true);
    await createCronograma(form)
      .then((data) => {
        toast.success(`Los cambios se guardarón correctamente`);
        setForm(defaultData);
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch((err) => {
        console.log(err);
        toast.error(`No se pudo guardar los cambios`);
      });
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={pending ? undefined : onClose}>
        Editar Cronograma
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">Planilla</label>
              <h6 className="capitalize">{cronograma?.planilla?.name}</h6>
            </FormGroup>
          </Col>

          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">Fecha</label>
              <h6 className="capitalize">
                {cronograma?.month}/{cronograma?.year}
              </h6>
            </FormGroup>
          </Col>

          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">Adicional</label>
              <h6 className="capitalize">
                {cronograma?.adicional ? `N° ${cronograma.adicional}` : "NO"}
              </h6>
            </FormGroup>
          </Col>

          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">Remanente</label>
              <h6 className="capitalize">
                {cronograma?.remanente ? "SI" : "NO"}
              </h6>
            </FormGroup>
          </Col>

          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">Plame</label>
              <h6 className="capitalize">
                {cronograma?.isPlame ? "SI" : "NO"}
              </h6>
            </FormGroup>
          </Col>

          <Col md="6" className="mb-2">
            <FormGroup>
              <label htmlFor="">N° Trabajadores</label>
              <h6 className="capitalize">{cronograma?.historialsCount}</h6>
            </FormGroup>
          </Col>

          <Col md="12" className="mb-2">
            <FormGroup>
              <label htmlFor="">Descripción</label>
              <Input
                type="text"
                name="observation"
                value={cronograma.observation}
                readOnly
              />
            </FormGroup>
          </Col>

          <Col md="12" className="mb-2">
            <FormGroup>
              <label htmlFor="">Observación</label>
              <Input
                type="textarea"
                name="observation"
                value={cronograma.observation}
                readOnly
              />
            </FormGroup>
          </Col>
        </Row>
      </ModalBody>
      {/* <ModalFooter>
        <div className="text-right">
          <Button
            color="primary"
            className="ml-2"
            onClick={handleSave}
            disabled={pending}
          >
            <Save className="icon" />
          </Button>
        </div>
      </ModalFooter> */}
    </Modal>
  );
};
