/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import React, { useMemo } from "react";
import { FormGroup } from "reactstrap";
import { CronogramaPimSelect } from "./cronograma-pim-select";
import { CronogramaPimCodeSelect } from "./cronograma-pim-code-select";
import { CronogramaCargoSelect } from "./cronograma-cargo-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { AfpCodeSelect } from "@modules/planilla/afp/components/afp-code-select";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";
import { TypeAffiliationSelect } from "@modules/planilla/type-affiliations/components/type-affiliation-select";
import { TypeAportationSelect } from "@modules/planilla/type_aportacion/components/type-aportation-select";
import { Filter } from "react-feather";
import Toggle from "@atlaskit/toggle";
import { IFormProps } from "./cronograma-report";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { CronogramaViewerSelect } from "./cronograma-viewer-select";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { LabelSelect } from "@modules/planilla/labels/components/label-select";

interface IProps {
  reportId: any;
  isFilter: boolean;
  form: IFormProps;
  onChange: (obj: IInputHandle) => void;
}

export const CronogramaReportFilter = ({
  reportId,
  isFilter,
  form,
  onChange,
}: IProps) => {
  const filterShow: { [key: string]: any[] } = {
    GENERAL: ["PIMCODE", "CARGO"],
    PLANILLA: ["PIMCODE", "CATEGORY", "TYPE_CARGO", "LABEL", "CONDITION"],
    BOLETA: [
      "PIMCODE",
      "CATEGORY",
      "TYPE_CARGO",
      "CONDITION",
      "LABEL",
      "DOUBLE",
    ],
    PAY: ["PAY", "NEGATIVE", "CATEGORY", "TYPE_CARGO", "CONDITION"],
    LEY: ["AFP", "TYPE_CARGO", "CONDITION"],
    REMUNERATION: ["REMUNERATION", "TYPE_CARGO", "CONDITION"],
    DISCOUNT: ["DISCOUNT", "TYPE_CARGO", "CONDITION"],
    OBLIGATION: ["PAY", "CATEGORY", "TYPE_CARGO", "CONDITION"],
    AFFILIATION: ["AFFILIATION", "TYPE_CARGO", "CONDITION"],
    APORTATION: ["APORTATION", "TYPE_CARGO", "CONDITION"],
    WORK: ["NEGATIVE", "CATEGORY", "TYPE_CARGO", "CONDITION", "LABEL"],
    EXECUTE: ["BRUTO"],
  };

  const currentFilter: any[] = useMemo(() => {
    return filterShow[reportId] || [];
  }, [reportId]);

  return (
    <Show condition={isFilter}>
      <FormGroup className="mb-3">
        <hr />
        <div>
          <Filter className="icon" /> Filtros avanzados
        </div>
        <hr />
      </FormGroup>
      {/* filter pay */}
      <Show condition={currentFilter.includes("PAY")}>
        <FormGroup className="mb-3">
          <label>¿Pago en cheque?</label>
          <div>
            <Toggle
              label="Checke"
              name="isCheck"
              isChecked={form.isCheck}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>
      </Show>
      {/* filter bruto */}
      <Show condition={currentFilter.includes("BRUTO")}>
        <FormGroup className="mb-3">
          <label>¿Montos Netos?</label>
          <div>
            <Toggle
              name="neto"
              isChecked={form.neto}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>
      </Show>
      {/* filter leyes sociales */}
      <Show condition={currentFilter.includes("AFP")}>
        <FormGroup className="mb-3">
          <label>Ley Sociales</label>
          <AfpCodeSelect
            name="code"
            value={form?.code || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-remuneration */}
      <Show condition={currentFilter.includes("REMUNERATION")}>
        <FormGroup className="mb-3">
          <label>Tipo Remuneración</label>
          <TypeRemunerationSelect
            name="typeRemunerationId"
            value={form?.typeRemunerationId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-discounts */}
      <Show condition={currentFilter.includes("DISCOUNT")}>
        <FormGroup className="mb-3">
          <label>Tipo Descuento</label>
          <TypeDiscountSelect
            name="typeDiscountId"
            value={form?.typeDiscountId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-affiliations */}
      <Show condition={currentFilter.includes("AFFILIATION")}>
        <FormGroup className="mb-3">
          <label>Tipo Afiliación</label>
          <TypeAffiliationSelect
            name="typeAffiliationId"
            value={form?.typeAffiliationId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-aportations */}
      <Show condition={currentFilter.includes("APORTATION")}>
        <FormGroup className="mb-3">
          <label>Tipo Aportación</label>
          <TypeAportationSelect
            name="typeAportationId"
            value={form?.typeAportationId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-cargo */}
      <Show condition={currentFilter.includes("TYPE_CARGO")}>
        <FormGroup className="mb-3">
          <label>Tip. Trabajador</label>
          <TypeCargoSelect
            name="typeCargoId"
            value={form?.typeCargoId || ""}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-category */}
      <Show condition={currentFilter.includes("CONDITION")}>
        <FormGroup className="mb-3">
          <label>Codición</label>
          <ContractConditionSelect
            name="condition"
            value={form?.condition || ""}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter type-category */}
      <Show condition={currentFilter.includes("CATEGORY")}>
        <FormGroup className="mb-3">
          <label>Categoría</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={form?.typeCategoryId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filtro pimCode */}
      <Show condition={currentFilter.includes("PIMCODE")}>
        <FormGroup className="mb-3">
          <label>PIM Código</label>
          <CronogramaPimCodeSelect
            name="pimCode"
            value={form?.pimCode || ""}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filtro pim */}
      <Show condition={currentFilter.includes("PIM")}>
        <FormGroup className="mb-3">
          <label>PIM</label>
          <CronogramaPimSelect
            name="pimId"
            value={form?.pimId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter cargo */}
      <Show condition={currentFilter.includes("CARGO")}>
        <FormGroup className="mb-3">
          <label>Partición Presp.</label>
          <CronogramaCargoSelect
            name="cargoId"
            value={form?.cargoId || 0}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter agrupación */}
      <Show condition={currentFilter.includes("LABEL")}>
        <FormGroup className="mb-3">
          <label>Agrupación</label>
          <LabelSelect
            name="labelId"
            value={form?.labelId}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter negative */}
      <Show condition={currentFilter.includes("NEGATIVE")}>
        <FormGroup className="mb-3">
          <label>Mostrar</label>
          <CronogramaViewerSelect
            name="negative"
            value={form?.negative}
            onChange={onChange}
          />
        </FormGroup>
      </Show>
      {/* filter blouble */}
      <Show condition={currentFilter.includes("DOUBLE")}>
        <FormGroup className="mb-3">
          <label>Dobles</label>
          <div>
            <Toggle
              name="isDouble"
              isChecked={form.isDouble}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>
      </Show>
    </Show>
  );
};
