import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { Filter, RefreshCcw, Save } from "react-feather";
import {
  Button,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useCronogramaChangeDay } from "../hooks/use-cronograma-change-day";
import { useCronogramaPim } from "../hooks/use-cronograma-pim";
import { CronogramaPimSelect } from "./cronograma-pim-select";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: () => void;
}

export const CronogramaChangeDay = ({ isOpen, onClose, onSave }: IProps) => {
  const cronogramaPim = useCronogramaPim();
  const { form, changeForm, pending, execute, clear } =
    useCronogramaChangeDay();

  const handle = () => {
    execute()
      .then(async () => {
        clear();
        await cronogramaPim.fetch().catch(() => null);
        if (typeof onSave == "function") {
          onSave();
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Cambio de días</ModalHeader>
      <ModalBody>
        <FormGroup>
          <Filter className="icon" /> Filtros
          <hr />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>N° Resolución</label>
          <Input
            name="resolution"
            value={form.resolution || ""}
            onChange={({ target }) => changeForm(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>PIM</label>
          <CronogramaPimSelect
            name="pimId"
            value={form.pimId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Tipo Categoría</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={form.typeCategoryId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup>
          <hr />
          <RefreshCcw className="icon" /> Actualizar Información
          <hr />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Dias <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            name="days"
            value={form.days || ""}
            onChange={({ target }) => changeForm(target)}
          />
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          title="Actualizar"
          disabled={pending}
          onClick={handle}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
