import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useCronogramaPimCode } from "../hooks/use-cronograma-pim-code";
import { IPimCodeEntity } from "@modules/planilla/pims/dtos/pim-code.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const CronogramaPimCodeSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { pimCodes } = useSelector((state: RootState) => state.pim);

  const cronogramaPimCode = useCronogramaPimCode(defaultQuerySearch);

  const settingsData = (row: IPimCodeEntity) => {
    return {
      label: `${row.code}`.toLowerCase(),
      value: row.code,
      onj: row,
    };
  };

  useEffect(() => {
    if (cronograma.id) cronogramaPimCode.fetch();
  }, [cronograma, cronogramaPimCode.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={pimCodes?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={cronogramaPimCode.setQuerySearch}
    />
  );
};
