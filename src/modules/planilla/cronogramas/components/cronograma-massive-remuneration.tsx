import React from "react";
import { Filter, Save } from "react-feather";
import {
  Button,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useCronogramaPim } from "../hooks/use-cronograma-pim";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";
import { useCronogramaMassiveRemuneration } from "../hooks/use-cronograma-massive-remuneration";
import { CronogramaPimSelect } from "./cronograma-pim-select";
import Toggle from "@atlaskit/toggle";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { SelectBasic } from "@common/select/select-basic";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: () => void;
}

export const CronogramaMassiveRemuneration = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const cronogramaPim = useCronogramaPim();
  const { form, changeForm, pending, execute, clear } =
    useCronogramaMassiveRemuneration();

  const handle = () => {
    execute()
      .then(async () => {
        clear();
        await cronogramaPim.fetch().catch(() => null);
        if (typeof onSave == "function") {
          onSave();
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Remuneración Masiva</ModalHeader>
      <ModalBody>
        <FormGroup className="mb-3">
          <label>
            Tipo Remuneración <b className="text-danger">*</b>
          </label>
          <TypeRemunerationSelect
            name="typeRemunerationId"
            value={form.typeRemunerationId}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup>
          <hr />
          <Filter className="icon" /> Filtros
          <hr />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>PIM</label>
          <CronogramaPimSelect
            name="pimId"
            value={form.pimId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Tipo Categoría</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={form.typeCategoryId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Sincronización Global</label>
          <div>
            <Toggle
              name="isSync"
              isChecked={form.isSync}
              onChange={({ target }) =>
                changeForm({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <hr />
          <label>
            Modo <b className="text-danger">*</b>
          </label>
          <div>
            <SelectBasic
              name="mode"
              value={form.mode}
              onChange={changeForm}
              options={[
                {
                  label: "Normal",
                  value: "Normal",
                },
                {
                  label: "Aumentar",
                  value: "Increment",
                },
                {
                  label: "Descontar",
                  value: "Decrement",
                },
              ]}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            name="amount"
            value={form.amount || ""}
            onChange={({ target }) => changeForm(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Base imponible</label>
          <div>
            <SelectBasic
              name="isBase"
              value={form?.isBase}
              onChange={changeForm}
              options={[
                { value: undefined, label: "Sin modificación" },
                { value: true, label: "Afecto" },
                { value: false, label: "No Afecto" },
              ]}
            />
          </div>
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          title="Actualizar"
          disabled={pending}
          onClick={handle}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
