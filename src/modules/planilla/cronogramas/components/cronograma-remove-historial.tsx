/* eslint-disable react-hooks/exhaustive-deps */
import { infoActions } from "@modules/planilla/infos/store";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import {
  findHistorialsToCronograma,
  removeHistorialsToCronograma,
} from "../apis";
import { IRowSelected } from "@common/dtos/row-selected";
import { Collection } from "collect.js";
import { Show } from "@common/show";
import { toast } from "react-toastify";
import { cronogramaActions } from "../store";
import { HistorialTable } from "@modules/planilla/historials/components/historial-table";
import { IHistorialEntity } from "@modules/planilla/historials/dtos/historial.entity";
import { historialActions } from "@modules/planilla/historials/store";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
}

interface IPropsBtn {
  ids: number[];
  onSuccess: () => void;
  onError: () => void;
}

export const ButtonSave = ({ ids, onSuccess, onError }: IPropsBtn) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    toast.dismiss();
    await removeHistorialsToCronograma(cronograma.id, ids)
      .then(() => {
        toast.success("La configuración de pago se eliminó correctamente!");
        if (typeof onSuccess == "function") {
          onSuccess();
        }
      })
      .catch(() => {
        toast.error("No se pudo eliminar la configuración de pago");
        if (typeof onError == "function") {
          onError();
        }
      });
    setPending(false);
  };

  return (
    <ModalFooter className="text-right">
      <Button color="danger" onClick={handleSave} outline disabled={pending}>
        Eliminar
        <span className="badge badge-sm badge-danger ml-2">{ids.length}</span>
      </Button>
    </ModalFooter>
  );
};

export const CronogramaRemoveHistorial = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { historials } = useSelector((state: RootState) => state.historial);

  const [toggleCleared, setToggleCleared] = useState(false);
  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(30);
  const [querySearch, setQuerySearch] = useState<string>("");
  const [pending, setPending] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [historialIds, setHistorialIds] = useState<number[]>([]);
  const [pimId, setPimId] = useState<number>(0);

  const handleData = async () => {
    setPending(true);
    setHistorialIds([]);
    await findHistorialsToCronograma(cronograma?.id || 0, {
      page,
      limit,
      querySearch,
      pimIds: pimId ? [pimId] : [],
    })
      .then((data) => dispatch(historialActions.paginate(data)))
      .catch(() =>
        dispatch(
          infoActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 0,
              itemsPerPage: limit,
            },
          })
        )
      );
    setPending(false);
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPage(page);
    setLimit(limit);
    setIsRefresh(true);
  };

  const handleSearch = (props: { querySearch: string; pimId: number }) => {
    setPage(1);
    setQuerySearch(props.querySearch);
    setPimId(props.pimId);
    setToggleCleared(true);
    setIsRefresh(true);
  };

  const handlePage = (page: number) => {
    setPage(page);
    setIsRefresh(true);
  };

  const handleOnSelected = (selected: IRowSelected<IHistorialEntity>) => {
    const ids: number[] = new Collection(selected.selectedRows)
      .pluck("id")
      .toArray();
    setHistorialIds(ids);
    setToggleCleared(ids.length <= 0);
  };

  const handleSuccess = () => {
    const count = cronograma.historialsCount - historialIds.length;
    dispatch(cronogramaActions.setCount(count));
    dispatch(cronogramaActions.setCountItem({ id: cronograma.id, count }));
    setHistorialIds([]);
    setToggleCleared(true);
    setIsRefresh(true);
  };

  const handleClear = () => {
    setHistorialIds([]);
    setQuerySearch("");
    setPage(1);
  };

  const handleError = () => {};

  useEffect(() => {
    if (isOpen) handleData();
  }, [isOpen]);

  useEffect(() => {
    if (!isOpen) handleClear();
  }, [isOpen]);

  useEffect(() => {
    if (isRefresh) handleData();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (toggleCleared) setToggleCleared(false);
  }, [toggleCleared]);

  return (
    <Modal isOpen={isOpen} size="lg" style={{ minWidth: "90%" }}>
      <ModalHeader toggle={onClose}>
        <span className="mr-2 badge badge-sm badge-dark">
          # {cronograma?.id}
        </span>
        Eliminar pago del cronograma
      </ModalHeader>
      <ModalBody>
        <HistorialTable
          perPage={limit}
          totalItems={historials?.meta?.totalItems || 0}
          loading={pending}
          data={historials?.items || []}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onSelectedRowsChange={handleOnSelected}
          onSearch={handleSearch}
          clearSelectedRows={toggleCleared}
        />
      </ModalBody>
      <Show condition={historialIds.length > 0}>
        <ButtonSave
          ids={historialIds}
          onSuccess={handleSuccess}
          onError={handleError}
        />
      </Show>
    </Modal>
  );
};
