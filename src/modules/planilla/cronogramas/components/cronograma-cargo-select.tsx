/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useCronogramaCargo } from "../hooks/use-cronograma-cargo";
import { ICargoEntity } from "@modules/planilla/cargos/dtos/cargo.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const CronogramaCargoSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { cargos } = useSelector((state: RootState) => state.cargo);

  const cronogramaCargo = useCronogramaCargo(defaultQuerySearch);

  const settingsData = (row: ICargoEntity) => {
    return {
      label: `${row.name} [${row.extension}]`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    if (cronograma.id) cronogramaCargo.fetch();
  }, [cronograma, cronogramaCargo.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={cargos?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={cronogramaCargo.setQuerySearch}
    />
  );
};
