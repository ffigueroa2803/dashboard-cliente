import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { aportationEntityName } from "@modules/planilla/aportations/dtos/aportation.entity";
import { discountEntityName } from "@modules/planilla/discounts/dtos/discount.entity";
import { historialEntityName } from "@modules/planilla/historials/dtos/historial.entity";
import { useHistorialSendMail } from "@modules/planilla/historials/hooks/use-historial-send-mail";
import { remunerationEntityName } from "@modules/planilla/remunerations/dtos/remuneration.entity";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { useContext, useMemo, useState } from "react";
import {
  Award,
  Box,
  CheckCircle,
  Database,
  DownloadCloud,
  Edit,
  File,
  Lock,
  Repeat,
  Send,
  Sliders,
  Unlock,
  Upload,
} from "react-feather";
import { useSelector } from "react-redux";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { CronogramaContext } from "../cronograma.context";
import { cronogramaEntityName } from "../dtos/cronograma.entity";
import { useCronogramaClose } from "../hooks/use-cronograma-close";
import { useCronogramaOpen } from "../hooks/use-cronograma-open";
import { useCronogramaProcess } from "../hooks/use-cronograma-process";
import { useCronogramaReportTicket } from "../hooks/use-cronograma-report-ticket";
import { useCronogramaTogglePlame } from "../hooks/use-cronograma-toggle-plame";
import { useCronogramaToken } from "../hooks/use-cronograma-token";
import { CronogramaChangeDay } from "./cronograma-change-day";
import { CronogramaChangePim } from "./cronograma-change-pim";
import { CronogramaChangePimAportation } from "./cronograma-change-pim-aportation";
import { CronogramaChangePimRemuneration } from "./cronograma-change-pim-remuneration";
import { CronogramaMassiveDiscount } from "./cronograma-massive-discount";
import { CronogramaMassiveRemuneration } from "./cronograma-massive-remuneration";
import { CronogramaReport } from "./cronograma-report";
import { CronogramaImportRemuneration } from "./cronograma-import-remuneration";
import { CronogramaImportDiscount } from "./cronograma-import-discount";

export const CronogramaOptions = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [option, setOption] = useState<string>("");

  const switchOptions = {
    REPORT: "REPORT",
    CHANGE_PIM: "CHANGE_PIM",
    CHANGE_PIM_REMUNERATION: "CHANGE_PIM_REMUNERATION",
    CHANGE_PIM_APORTATION: "CHANGE_PIM_APORTATION",
    CHANGE_DAY_HISTORIAL: "CHANGE_DAY_HISTORIAL",
    MASSIVE_REMUNERATION: "MASSIVE_REMUNERATION",
    IMPORT_REMUNERATION: "IMPORT_REMUNERATION",
    MASSIVE_DISCOUNT: "MASSIVE_DISCOUNT",
    IMPORT_DISCOUNT: "IMPORT_DISCOUNT",
  };

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { historial } = useSelector((state: RootState) => state.historial);

  const ability = useContext(CaslContext);

  const { onRefresh } = useContext(CronogramaContext);

  const cronogramaOpen = useCronogramaOpen();
  const cronogramaClose = useCronogramaClose();
  const cronogramaProcess = useCronogramaProcess();
  const cronogramaTicket = useCronogramaReportTicket();
  const cronogramaToken = useCronogramaToken();
  const cronogramaPlame = useCronogramaTogglePlame();
  const historialSendMail = useHistorialSendMail();

  const canOpen = useMemo(() => {
    const currentDate = DateTime.now();
    const cronogramaDate = DateTime.fromFormat(
      `${cronograma.year}-${cronograma.month}-${cronograma.numberOfDays}`,
      "yyyy-M-dd"
    );
    // validar
    const diff = cronogramaDate.diff(currentDate, "days").days || 0;
    // response
    return diff > 0;
  }, [cronograma]);

  const handleProcess = async () => {
    await cronogramaProcess.execute();
    onRefresh();
  };

  const handleExport = async () => {
    cronogramaTicket.generateExcel().catch(() => null);
  };

  const handleGenerateToken = () => {
    cronogramaToken.execute().catch(() => null);
  };

  const handleTogglePlame = () => {
    cronogramaPlame
      .execute()
      .then(() => onRefresh())
      .catch(() => null);
  };

  const handleSendBoleta = () => {
    historialSendMail.execute().catch(() => null);
  };

  const handleCronogramaOpen = () => {
    cronogramaOpen
      .execute()
      .then(() => onRefresh())
      .catch(() => null);
  };

  const handleCronogramaClose = () => {
    cronogramaClose
      .execute()
      .then(() => onRefresh())
      .catch(() => null);
  };

  return (
    <>
      <Dropdown toggle={() => setIsOpen((prev) => !prev)} isOpen={isOpen}>
        <DropdownToggle color="dark" outline>
          <Sliders className="icon" /> Opciones
        </DropdownToggle>
        <DropdownMenu>
          {/* crongrama abierto  */}
          <Show condition={cronograma.state}>
            {/* cambio PIM Historial */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                historialEntityName
              )}
            >
              <DropdownItem onClick={() => setOption(switchOptions.CHANGE_PIM)}>
                <Repeat className="icon" /> Cambio de PIM
              </DropdownItem>
            </Show>
            {/* cambio PIM remuneration */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                remunerationEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.CHANGE_PIM_REMUNERATION)}
              >
                <Repeat className="icon" /> Cambio de PIM Rem.
              </DropdownItem>
            </Show>
            {/* cambio PIM aportation */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                aportationEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.CHANGE_PIM_APORTATION)}
              >
                <Repeat className="icon" /> Cambio de PIM Aport.
              </DropdownItem>
            </Show>
            {/* cambio days Historial */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                historialEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.CHANGE_DAY_HISTORIAL)}
              >
                <Edit className="icon" /> Cambio de Días
              </DropdownItem>
            </Show>
            {/* descuentos masivo*/}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                discountEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.MASSIVE_DISCOUNT)}
              >
                <DownloadCloud className="icon" /> Descuento Masivo
              </DropdownItem>
            </Show>
            {/* importar descuento */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                discountEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.IMPORT_DISCOUNT)}
              >
                <Upload className="icon" /> Imp. Descuentos
              </DropdownItem>
            </Show>
            {/* remuneración masiva */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                remunerationEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.MASSIVE_REMUNERATION)}
              >
                <DownloadCloud className="icon" /> Remuneración Masiva
              </DropdownItem>
            </Show>
            {/* importar remuneración */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                remunerationEntityName
              )}
            >
              <DropdownItem
                onClick={() => setOption(switchOptions.IMPORT_REMUNERATION)}
              >
                <Upload className="icon" /> Imp. Remuneraciones
              </DropdownItem>
            </Show>
            {/* procesar datos */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                Object.assign({ __typename: cronogramaEntityName }, cronograma)
              )}
            >
              <DropdownItem
                onClick={handleProcess}
                disabled={cronogramaProcess.pending}
              >
                <CheckCircle className="icon" /> Procesar Datos
              </DropdownItem>
            </Show>
          </Show>
          {/* cronograma cerrado */}
          <Show condition={!cronograma.state}>
            {/* generar token */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                historialEntityName
              )}
            >
              <DropdownItem onClick={handleGenerateToken}>
                <Award className="icon" /> Generar Token
              </DropdownItem>
            </Show>
            {/* enviar al plame */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                cronogramaEntityName
              )}
            >
              <DropdownItem onClick={handleTogglePlame}>
                <Box className="icon" />{" "}
                {cronograma?.isPlame ? "Quitar" : "Aplicar"} Plame
              </DropdownItem>
            </Show>
            {/* enviar boleta */}
            <Show condition={historial.tokenVerify ? true : false}>
              <Show
                condition={ability.can(
                  PermissionAction.READ,
                  historialEntityName
                )}
              >
                <DropdownItem
                  onClick={handleSendBoleta}
                  disabled={historialSendMail.pending}
                >
                  <Send className="icon" /> Enviar Boleta
                </DropdownItem>
              </Show>
            </Show>
          </Show>
          {/* exportar datos */}
          <DropdownItem
            disabled={cronogramaTicket.pending}
            onClick={handleExport}
          >
            <Database className="icon" /> Exportar Datos
          </DropdownItem>
          {/* validar report */}
          <Show
            condition={ability.can(
              PermissionAction.REPORT,
              Object.assign({ __typename: cronogramaEntityName }, cronograma)
            )}
          >
            <DropdownItem onClick={() => setOption(switchOptions.REPORT)}>
              <File className="icon" /> Reportes
            </DropdownItem>
          </Show>
          {/* cerrar cronograma */}
          <Show
            condition={cronograma.state}
            isDefault={
              <Show
                condition={
                  canOpen &&
                  ability.can(
                    PermissionAction.UPDATE,
                    Object.assign(
                      { __typename: cronogramaEntityName },
                      cronograma
                    )
                  )
                }
              >
                <DropdownItem
                  onClick={handleCronogramaOpen}
                  disabled={cronogramaOpen.pending}
                >
                  <Unlock className="icon" /> Abrir
                </DropdownItem>
              </Show>
            }
          >
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                Object.assign({ __typename: cronogramaEntityName }, cronograma)
              )}
            >
              <DropdownItem
                onClick={handleCronogramaClose}
                disabled={cronogramaClose.pending}
              >
                <Lock className="icon" /> Cerrar
              </DropdownItem>
            </Show>
          </Show>
        </DropdownMenu>
      </Dropdown>
      {/* reports */}
      <CronogramaReport
        isOpen={option == switchOptions.REPORT}
        onClose={() => setOption("")}
      />
      {/* change pim */}
      <CronogramaChangePim
        isOpen={option == switchOptions.CHANGE_PIM}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* change pim  remuneration*/}
      <CronogramaChangePimRemuneration
        isOpen={option == switchOptions.CHANGE_PIM_REMUNERATION}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* change pim  aportation*/}
      <CronogramaChangePimAportation
        isOpen={option == switchOptions.CHANGE_PIM_APORTATION}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* change day historial */}
      <CronogramaChangeDay
        isOpen={option == switchOptions.CHANGE_DAY_HISTORIAL}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* massive remuneration */}
      <CronogramaMassiveRemuneration
        isOpen={option == switchOptions.MASSIVE_REMUNERATION}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* import remuneration */}
      <CronogramaImportRemuneration
        isOpen={option == switchOptions.IMPORT_REMUNERATION}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* massive discount */}
      <CronogramaMassiveDiscount
        isOpen={option == switchOptions.MASSIVE_DISCOUNT}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
      {/* import discount */}
      <CronogramaImportDiscount
        isOpen={option == switchOptions.IMPORT_DISCOUNT}
        onClose={() => setOption("")}
        onSave={() => onRefresh()}
      />
    </>
  );
};
