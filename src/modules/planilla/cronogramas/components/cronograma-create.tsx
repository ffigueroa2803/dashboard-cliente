import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import React, { useState } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { createCronograma } from "../apis";
import { ICreateCronogramaDto } from "../dtos/create-cronograma.dto";
import { ICronogramaEntity } from "../dtos/cronograma.entity";
import { CronogramaForm } from "./cronograma-form";

interface IProps {
  isOpen: boolean;
  principal: boolean;
  onClose: () => void;
  onSave: (cronograma: ICronogramaEntity) => void;
}

const currentDate = DateTime.now();

export const CronogramaCreate = ({
  isOpen,
  onClose,
  onSave,
  principal = true,
}: IProps) => {
  const { user } = useSelector((state: RootState) => state.auth);
  const [pending, setPending] = useState<boolean>(false);

  const defaultData: ICreateCronogramaDto = {
    year: currentDate.year,
    month: currentDate.month,
    campusId: user?.campusId || 0,
    observation: "",
    planillaId: 0,
    adicional: false,
    remanente: false,
  };

  const [form, setForm] = useState<ICreateCronogramaDto>(defaultData);

  const handleChange = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    toast.dismiss();
    setPending(true);
    await createCronograma(form)
      .then((data) => {
        toast.success(`Los cambios se guardarón correctamente`);
        setForm(defaultData);
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch((err) => {
        console.log(err);
        toast.error(`No se pudo guardar los cambios`);
      });
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={pending ? undefined : onClose}>
        Nuevo Cronograma
      </ModalHeader>
      <ModalBody>
        <CronogramaForm
          principal={principal}
          form={form}
          onChange={handleChange}
          disabled={pending}
        />
        <div className="text-right">
          <Button
            color="primary"
            className="ml-2"
            onClick={handleSave}
            disabled={pending}
          >
            <Save className="icon" />
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
