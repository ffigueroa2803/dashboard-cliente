import React, { useState } from "react";
import { Repeat } from "react-feather";
import { CronogramaPimSelect } from "./cronograma-pim-select";
import {
  Button,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { DateTime } from "luxon";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { useCronogramaPim } from "../hooks/use-cronograma-pim";
import { useCronogramaChangePimRemuneration } from "../hooks/use-cronograma-change-pim-remuneration";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";
import Toggle from "@atlaskit/toggle";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: () => void;
}

const currentDate = DateTime.now();

export const CronogramaChangePimRemuneration = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const cronogramaPim = useCronogramaPim();
  const { form, changeForm, pending, execute, clear } =
    useCronogramaChangePimRemuneration();
  const [year, setYear] = useState<number>(currentDate.year);

  const handle = () => {
    execute()
      .then(async () => {
        clear();
        await cronogramaPim.fetch().catch(() => null);
        if (typeof onSave == "function") {
          onSave();
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Cambio de PIM Remunerativo</ModalHeader>
      <ModalBody>
        <FormGroup className="mb-3">
          <label>
            Tipo Remuneración <b className="text-danger">*</b>
          </label>
          <TypeRemunerationSelect
            name="typeRemunerationId"
            value={form.typeRemunerationId}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            PIM Actual <b className="text-danger">*</b>
          </label>
          <CronogramaPimSelect
            name="pimId"
            value={form.pimId}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup>
          <hr />
          <Repeat className="icon" /> Cambiar de PIM
          <hr />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Año <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            name="year"
            value={year}
            onChange={({ target }) => setYear(parseInt(target.value))}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            PIM a Cambiar <b className="text-danger">*</b>
          </label>
          <PimSelect
            year={year}
            name="nextPimId"
            value={form.nextPimId}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Sincronización Global</label>
          <div>
            <Toggle
              name="isSync"
              isChecked={form.isSync}
              onChange={({ target }) =>
                changeForm({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          title="Cambiar de PIM"
          disabled={pending}
          onClick={handle}
        >
          <Repeat className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
