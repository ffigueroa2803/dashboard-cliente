/* eslint-disable react-hooks/exhaustive-deps */
import { infoActions } from "@modules/planilla/infos/store";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { addInfosToCronograma, findInfosToCronograma } from "../apis";
import {
  FilterInfoTableType,
  InfoTable,
} from "@modules/planilla/infos/components/info-table";
import { IInfoEntity } from "@modules/planilla/infos/dtos/info.entity";
import { IRowSelected } from "@common/dtos/row-selected";
import { Collection } from "collect.js";
import { Show } from "@common/show";
import { toast } from "react-toastify";
import { cronogramaActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
}

interface IPropsBtn {
  ids: number[];
  onSuccess: () => void;
  onError: () => void;
}

export const ButtonSave = ({ ids, onSuccess, onError }: IPropsBtn) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    toast.dismiss();
    await addInfosToCronograma(cronograma.id, ids)
      .then(() => {
        toast.success("La configuración de pago se agregó correctamente!");
        if (typeof onSuccess == "function") {
          onSuccess();
        }
      })
      .catch(() => {
        toast.error("No se pudo guardar los datos");
        if (typeof onError == "function") {
          onError();
        }
      });
    setPending(false);
  };

  return (
    <ModalFooter className="text-right">
      <Button color="primary" onClick={handleSave} outline disabled={pending}>
        Agregar
        <span className="badge badge-sm badge-primary ml-2">{ids.length}</span>
      </Button>
    </ModalFooter>
  );
};

export const CronogramaAddInfo = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { infos } = useSelector((state: RootState) => state.info);

  const [toggleCleared, setToggleCleared] = useState(false);
  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(30);
  const [filters, setFilters] = useState<FilterInfoTableType>();
  const [pending, setPending] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [infoIds, setInfoIds] = useState<number[]>([]);

  const handleData = async () => {
    setPending(true);
    await findInfosToCronograma(cronograma?.id || 0, {
      page,
      limit,
      ...filters,
    })
      .then((data) => dispatch(infoActions.paginate(data)))
      .catch(() =>
        dispatch(
          infoActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 0,
              itemsPerPage: limit,
            },
          })
        )
      );
    setPending(false);
  };

  const handleChangePerPage = (limit: number, page: number) => {
    setPage(page);
    setLimit(limit);
    setIsRefresh(true);
  };

  const handleSearch = (props: FilterInfoTableType) => {
    setPage(1);
    setFilters(props);
    setIsRefresh(true);
    setToggleCleared(true);
  };

  const handlePage = (page: number) => {
    setPage(page);
    setIsRefresh(true);
    setToggleCleared(true);
  };

  const handleOnSelected = (selected: IRowSelected<IInfoEntity>) => {
    const ids: number[] = new Collection(selected.selectedRows)
      .pluck("id")
      .toArray();
    setInfoIds(ids);
    setToggleCleared(ids.length <= 0);
  };

  const handleSuccess = () => {
    const count = cronograma.historialsCount + infoIds.length;
    dispatch(cronogramaActions.setCount(count));
    dispatch(cronogramaActions.setCountItem({ id: cronograma.id, count }));
    setInfoIds([]);
    setToggleCleared(true);
    setIsRefresh(true);
  };

  const handleClear = () => {
    setInfoIds([]);
    setFilters({});
    setPage(1);
    setToggleCleared(true);
  };

  const handleError = () => {};

  useEffect(() => {
    if (isOpen) handleData();
  }, [isOpen]);

  useEffect(() => {
    if (!isOpen) handleClear();
  }, [isOpen]);

  useEffect(() => {
    if (isRefresh) handleData();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (toggleCleared) setToggleCleared(false);
  }, [toggleCleared]);

  return (
    <Modal isOpen={isOpen} size="lg" style={{ minWidth: "90%" }}>
      <ModalHeader toggle={onClose}>
        <span className="mr-2 badge badge-sm badge-dark">
          # {cronograma?.id}
        </span>
        Agregar pago al cronograma
      </ModalHeader>
      <ModalBody>
        <InfoTable
          clearSelectedRows={toggleCleared}
          perPage={limit}
          totalItems={infos?.meta?.totalItems || 0}
          loading={pending}
          data={infos?.items || []}
          onChangePage={handlePage}
          onChangeRowsPerPage={handleChangePerPage}
          onSelectedRowsChange={handleOnSelected}
          onSearch={handleSearch}
        />
      </ModalBody>
      <Show condition={infoIds.length > 0}>
        <ButtonSave
          ids={infoIds}
          onSuccess={handleSuccess}
          onError={handleError}
        />
      </Show>
    </Modal>
  );
};
