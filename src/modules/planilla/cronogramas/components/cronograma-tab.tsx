/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useMemo, useState } from "react";
import {
  Card,
  Nav,
  NavItem,
  NavLink,
  CardBody,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import {
  Info,
  Settings,
  DollarSign,
  ShoppingCart,
  Users,
  ShieldOff,
  Frown,
} from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { cronogramaActions } from "../store";
import { WorkInfo } from "@modules/scale/works/components/work-info";
import { useCronogramaProcess } from "../hooks/use-cronograma-process";
import { CronogramaContext } from "../cronograma.context";
import { HistorialInfo } from "@modules/planilla/historials/components/historial-info";
import { RemunerationContent } from "@modules/planilla/remunerations/components/remuneration-content";
import { DiscountContent } from "@modules/planilla/discounts/components/discount-content";
import { AffiliationContent } from "@modules/planilla/affiliations/components/affiliation-content";
import { AportationContent } from "@modules/planilla/aportations/components/aportation-content";
import { ObligationContent } from "@modules/planilla/obligations/components/obligation-content";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { remunerationEntityName } from "@modules/planilla/remunerations/dtos/remuneration.entity";
import { discountEntityName } from "@modules/planilla/discounts/dtos/discount.entity";
import { affiliationEntityName } from "@modules/planilla/affiliations/dtos/affiliation.entity";
import { obligationEntityName } from "@modules/planilla/obligations/dtos/obligation.entity";
import { aportationEntityName } from "@modules/planilla/aportations/dtos/aportation.entity";

export const CronogramaTab = () => {
  const dispatch = useDispatch();
  const cronogramaProcess = useCronogramaProcess();

  const ability = useContext(CaslContext);

  const { tabIndex, cronograma } = useSelector(
    (state: RootState) => state.cronograma
  );
  const { onRefresh } = useContext(CronogramaContext);

  const [isMore, setIsMore] = useState<boolean>(false);

  const tabItems = [
    {
      title: "Datos Per.",
      icon: Info,
      content: (
        <WorkInfo
          isBlock={!cronograma.state}
          onSave={async () => {
            await cronogramaProcess.execute();
            onRefresh();
          }}
        />
      ),
      dropdown: false,
    },
    {
      title: "Config. Pago",
      icon: Settings,
      content: <HistorialInfo isBlock={!cronograma.state} />,
      dropdown: false,
    },
    {
      title: "Remuneración",
      icon: DollarSign,
      content: <RemunerationContent />,
      dropdown: false,
      condition: remunerationEntityName,
    },
    {
      title: "Descuento",
      icon: ShoppingCart,
      content: <DiscountContent />,
      dropdown: false,
      condition: discountEntityName,
    },
    {
      title: "Afiliación",
      icon: Users,
      content: <AffiliationContent />,
      dropdown: true,
      condition: affiliationEntityName,
    },
    {
      title: "Obligación",
      icon: ShieldOff,
      content: <ObligationContent />,
      dropdown: true,
      condition: obligationEntityName,
    },
    {
      title: "Aportación",
      icon: Frown,
      content: <AportationContent />,
      dropdown: true,
      condition: aportationEntityName,
    },
  ];

  const currentContent = useMemo(() => {
    return tabItems[tabIndex] || {};
  }, [tabIndex]);

  const displayDropdown = useMemo(() => {
    const item = tabItems.find((item, index) => {
      if (!item.dropdown) return false;
      return index == tabIndex;
    });
    // display
    return item?.title;
  }, [tabIndex]);

  const toggleIsMore = () => setIsMore((prev) => !prev);

  const handleTabIndex = (index: number) => {
    dispatch(cronogramaActions.changeTab(index));
  };

  return (
    <>
      <Card>
        <CardBody>
          <Nav pills>
            {tabItems.map((item, index) => {
              if (item.dropdown) return null;
              // verificar permissions
              if (
                item.condition &&
                !ability.can(PermissionAction.READ, item.condition)
              ) {
                return null;
              }
              // response item
              return (
                <NavItem
                  className="cursor-pointer mb-1"
                  key={`item-tab-cronograma-${index}`}
                >
                  <NavLink
                    className="disabled-selection"
                    active={index == tabIndex}
                    onClick={() => handleTabIndex(index)}
                  >
                    <item.icon className="icon" />
                    {item.title}
                  </NavLink>
                </NavItem>
              );
            })}
            {/* mas */}
            <NavItem className="ml-2 mb-1">
              <Dropdown isOpen={isMore} toggle={toggleIsMore}>
                <DropdownToggle
                  caret
                  outline={displayDropdown ? false : true}
                  color="primary"
                >
                  {displayDropdown || "Más Acciones"}
                </DropdownToggle>
                <DropdownMenu>
                  {tabItems.map((item, index) => {
                    if (!item.dropdown) return null;

                    if (
                      item.condition &&
                      !ability.can(PermissionAction.READ, item.condition)
                    ) {
                      return null;
                    }

                    return (
                      <DropdownItem
                        key={`ìtem-option-${index}`}
                        onClick={() => handleTabIndex(index)}
                      >
                        <item.icon className="icon mr-2" />
                        {item.title}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </Dropdown>
            </NavItem>
          </Nav>
        </CardBody>
      </Card>
      {/* content */}
      {currentContent?.content || null}
    </>
  );
};
