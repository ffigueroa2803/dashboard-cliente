import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import Toggle from "@atlaskit/toggle";
import { InputDto } from "@services/dtos";
import { ICreateCronogramaDto } from "../dtos/create-cronograma.dto";
import { Show } from "@common/show";
import { DateTime } from "luxon";
import { PlanillaSelect } from "@modules/planilla/planillas/components/planilla-select";

interface IProps {
  form: ICreateCronogramaDto;
  disabled?: boolean;
  principal: boolean;
  // eslint-disable-next-line no-unused-vars
  onChange: (input: InputDto) => void;
}

const currentDate = DateTime.now();

export const CronogramaForm = ({
  form,
  disabled,
  onChange,
  principal,
}: IProps) => {
  const handleChange = (inputObj: InputDto) => {
    if (typeof onChange == "function") {
      onChange(inputObj);
    }
  };

  return (
    <Form>
      <FormGroup>
        <label>
          Planilla <b className="text-danger">*</b>
        </label>
        <PlanillaSelect
          name="planillaId"
          value={form?.planillaId || ""}
          onChange={handleChange}
          principal={principal}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha <b className="text-danger">*</b>
        </label>
        <Input type="text" value={currentDate.toFormat("MM/yyyy")} disabled />
      </FormGroup>

      <FormGroup>
        <label>¿Es una planilla adicional?</label>
        <div>
          <Toggle
            name="adicional"
            isChecked={form.adicional}
            onChange={({ target }) =>
              handleChange({ name: target.name, value: target.checked })
            }
            isDisabled={disabled}
          />
        </div>
      </FormGroup>

      <Show condition={form.adicional}>
        <FormGroup>
          <label>¿Es una planilla remanente?</label>
          <div>
            <Toggle
              name="remanente"
              isChecked={form.remanente}
              onChange={({ target }) =>
                handleChange({ name: target.name, value: target.checked })
              }
              isDisabled={disabled}
            />
          </div>
        </FormGroup>
      </Show>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => handleChange(target)}
          disabled={disabled}
        />
      </FormGroup>
    </Form>
  );
};
