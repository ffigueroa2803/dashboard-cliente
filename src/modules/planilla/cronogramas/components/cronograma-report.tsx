/* eslint-disable react-hooks/exhaustive-deps */
import { IInputHandle } from "@common/dtos/input-handle";
import { ModalPaginateReport } from "@common/modal/components/modal-paginate-report";
import { SelectBasic } from "@common/select/select-basic";
import { Show } from "@common/show";
import { ConditionContractType } from "@modules/scale/contracts/dtos/contract.entity";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useEffect, useMemo, useState } from "react";
import { File } from "react-feather";
import { useSelector } from "react-redux";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import urljoin from "url-join";
import { CronogramaReportFilter } from "./cronograma-report-filter";

const request = planillaRequest();

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
}

export interface IFormProps {
  isCheck: boolean;
  neto: boolean;
  negative?: boolean;
  pimCode: string | null;
  pimId: number | null;
  cargoId: number | null;
  typeCategoryId: number | null;
  code: string | null;
  typeRemunerationId: number | null;
  typeDiscountId: number | null;
  typeAffiliationId: number | null;
  typeAportationId: number | null;
  typeCargoId?: number;
  condition?: ConditionContractType;
  labelId?: number;
  isDouble: boolean;
}

interface IActionItem {
  label: string;
  type: "link" | "modal";
  link: string;
  icon: any;
  modal?: {
    limit: number;
    title: string;
  };
}

const defaultFilter: IFormProps = {
  pimCode: "",
  pimId: null,
  cargoId: null,
  typeCategoryId: null,
  code: "",
  typeRemunerationId: null,
  typeDiscountId: null,
  typeAffiliationId: null,
  typeAportationId: null,
  isCheck: false,
  neto: false,
  negative: false,
  condition: undefined,
  labelId: undefined,
  isDouble: false,
};

export const CronogramaReport = ({ isOpen, onClose }: IProps) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [isReport, setIsReport] = useState<boolean>(false);
  const [limit, setLimit] = useState<number>(120);
  const [title, setTitle] = useState<string>("");
  const [link, setLink] = useState<string>("");
  const [report, setReport] = useState<string>("");
  const [isToggle, setIsToggle] = useState<boolean>(false);
  const [form, setForm] = useState<IFormProps>(defaultFilter);

  const onToggle = () => setIsToggle((prev) => !prev);

  const options = [
    { label: "Reporte General", value: "GENERAL" },
    { label: "Reporte Planilla", value: "PLANILLA" },
    { label: "Reporte Boleta", value: "BOLETA" },
    { label: "Reporte Medio de Pago", value: "PAY" },
    { label: "Reporte Leyes Sociales", value: "LEY" },
    { label: "Reporte Remuneraciones", value: "REMUNERATION" },
    { label: "Reporte Descuentos", value: "DISCOUNT" },
    { label: "Reporte Obligaciones", value: "OBLIGATION" },
    { label: "Reporte Afiliaciones", value: "AFFILIATION" },
    { label: "Reporte Aportaciones", value: "APORTATION" },
    { label: "Reporte Personal", value: "WORK" },
    { label: "Reporte Ejecución", value: "EXECUTE" },
    { label: "Compromiso SIAF", value: "COMPROMISE" },
  ];

  const actions: { [key: string]: IActionItem[] } = {
    GENERAL: [
      {
        icon: <File className="icon" />,
        label: "General PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/general.pdf`
        ),
      },
    ],
    PLANILLA: [
      {
        icon: <File className="icon" />,
        label: "Planilla PDF",
        type: "modal",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/spreadsheets.pdf`
        ),
        modal: {
          title: "Reporte de Planilla",
          limit: 120,
        },
      },
    ],
    BOLETA: [
      {
        icon: <File className="icon" />,
        label: "Boleta PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/tickets.pdf`
        ),
      },
    ],
    PAY: [
      {
        icon: <File className="icon" />,
        label: "Medio de Pago",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/pay.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Descargar txt",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/pay.txt`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "MCPP",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/pay-mcpp.txt`
        ),
      },
    ],
    LEY: [
      {
        icon: <File className="icon" />,
        label: "Ley Social PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/afp.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "AFP NET",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/afp.xlsx?private=true`
        ),
      },
    ],
    REMUNERATION: [
      {
        icon: <File className="icon" />,
        label: "Remuneración PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/remuneration.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Remuneración Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/remuneration.xlsx`
        ),
      },
    ],
    DISCOUNT: [
      {
        icon: <File className="icon" />,
        label: "Descuentos PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/discount.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Descuentos Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/discount.xlsx`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Comparar PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/compareDiscount.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Comparar Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/compareDiscount.xlsx`
        ),
      },
    ],
    OBLIGATION: [
      {
        icon: <File className="icon" />,
        label: "Lista Beneficiarios",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/obligation.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Pagos PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/obligationPay.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Pagos txt",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/obligation.txt`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "MCPP",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/obligation-mcpp.txt`
        ),
      },
    ],
    AFFILIATION: [
      {
        icon: <File className="icon" />,
        label: "Afiliaciones PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/affiliation.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Afiliaciones Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/affiliation.xlsx`
        ),
      },
    ],
    APORTATION: [
      {
        icon: <File className="icon" />,
        label: "Aportaciones PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/aportation.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Aportaciones Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/aportation.xlsx`
        ),
      },
    ],
    WORK: [
      {
        icon: <File className="icon" />,
        label: "Personal PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/personal.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Personal Excel",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/personal.xlsx`
        ),
      },
    ],
    EXECUTE: [
      {
        icon: <File className="icon" />,
        label: "Ejecución PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/ejecucion.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Ejecución Pago PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/ejecucionPay.pdf`
        ),
      },
      {
        icon: <File className="icon" />,
        label: "Ejecución Total PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/ejecucionTotal.pdf`
        ),
      },
    ],
    COMPROMISE: [
      {
        icon: <File className="icon" />,
        label: "Generar PDF",
        type: "link",
        link: urljoin(
          request.urlBase,
          `/cronogramas/${cronograma.id}/reports/compromisoSiaf.pdf`
        ),
      },
    ],
  };

  const currentActions: IActionItem[] = useMemo(() => {
    return actions[report] || [];
  }, [report, cronograma]);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleLink = (type: string, link: string, modal?: any) => {
    const queryString = new URLSearchParams();

    const paramsObject: any = {
      typeCargoId: "contract[typeCargoId]",
      condition: "contract[condition]",
      typeCategoryId: "contract[typeCategoryId]",
    };

    Object.keys(form).forEach((attr: any) => {
      const newForm: any = Object.assign({}, form);
      const value = newForm[attr];

      const attribute = paramsObject[attr];

      // filter boolean
      if (typeof value == "boolean") {
        queryString.set(attr, `${value}`);
        if (attribute) queryString.set(attribute, `${value}`);
      } else if (value) {
        queryString.set(attr, `${value}`);
        if (attribute) queryString.set(attribute, `${value}`);
      }
    });
    // crear elemento a
    const isQuery = /[?]/.test(link);
    const url = `${link}${isQuery ? "&" : "?"}${queryString.toString()}`;
    // filter
    if (type == "link") {
      const a = document.createElement("a");
      a.href = url;
      a.target = "_blank";
      a.click();
    } else if (type == "modal" && modal) {
      setLimit(modal.limit);
      setTitle(modal.title);
      setLink(url);
      setIsReport(true);
    }
  };

  useEffect(() => {
    setForm(defaultFilter);
  }, [report]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        <span className="badge badge-dark mr-2">#{cronograma.id}</span>
        Reportes
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <label>Tipo de Reportes</label>
          <SelectBasic
            placeholder="Tipo de Reportes"
            name="reportId"
            value={report}
            onChange={(options) => setReport(options.value)}
            options={options}
          />
        </FormGroup>
        {/* filtros */}
        <CronogramaReportFilter
          form={form}
          isFilter={currentActions.length > 0}
          reportId={report}
          onChange={handleForm}
        />
      </ModalBody>
      {/* acciones */}
      <Show condition={currentActions.length > 0}>
        <ModalFooter>
          <Dropdown isOpen={isToggle} toggle={onToggle}>
            <DropdownToggle color="danger" outline caret>
              Reportes
            </DropdownToggle>
            <DropdownMenu>
              {currentActions.map((action, index) => (
                <DropdownItem
                  key={`option-item-${index}`}
                  onClick={() =>
                    handleLink(action.type, action.link, action.modal)
                  }
                >
                  {action.icon}
                  <span className="ml-2">{action.label}</span>
                </DropdownItem>
              ))}
            </DropdownMenu>
          </Dropdown>
        </ModalFooter>
      </Show>
      {/* reporte modal */}
      <ModalPaginateReport
        url={link}
        limit={limit}
        title={title}
        isOpen={isReport}
        onClose={() => setIsReport(false)}
      />
    </Modal>
  );
};
