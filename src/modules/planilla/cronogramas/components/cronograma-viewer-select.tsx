/* eslint-disable no-unused-vars */
import { SelectBasic } from "@common/select/select-basic";

interface IProps {
  name: string;
  value: any;
  onChange: (object: any) => void;
}

export const CronogramaViewerSelect = ({ name, value, onChange }: IProps) => {
  return (
    <SelectBasic
      name={name}
      value={value}
      onChange={onChange}
      placeholder="Todos"
      options={[
        { label: "Todos", value: undefined },
        { label: "Positivos", value: false },
        { label: "Menor/Igual a 0", value: true },
      ]}
    />
  );
};
