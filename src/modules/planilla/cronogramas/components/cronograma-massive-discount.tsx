import React from "react";
import { Filter, Save } from "react-feather";
import {
  Button,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useCronogramaPim } from "../hooks/use-cronograma-pim";
import { CronogramaPimSelect } from "./cronograma-pim-select";
import Toggle from "@atlaskit/toggle";
import { useCronogramaMassiveDiscount } from "../hooks/use-cronograma-massive-discount";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: () => void;
}

export const CronogramaMassiveDiscount = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const cronogramaPim = useCronogramaPim();
  const { form, changeForm, pending, execute, clear } =
    useCronogramaMassiveDiscount();

  const handle = () => {
    execute()
      .then(async () => {
        clear();
        await cronogramaPim.fetch().catch(() => null);
        if (typeof onSave == "function") {
          onSave();
        }
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Descuento Masivo</ModalHeader>
      <ModalBody>
        <FormGroup className="mb-3">
          <label>
            Tipo Descuento <b className="text-danger">*</b>
          </label>
          <TypeDiscountSelect
            name="typeDiscountId"
            value={form.typeDiscountId}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup>
          <hr />
          <Filter className="icon" /> Filtros
          <hr />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>PIM</label>
          <CronogramaPimSelect
            name="pimId"
            value={form.pimId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Tipo Categoría</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={form.typeCategoryId || ""}
            onChange={changeForm}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Sincronización Global</label>
          <div>
            <Toggle
              name="isSync"
              isChecked={form.isSync}
              onChange={({ target }) =>
                changeForm({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <hr />
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            name="amount"
            value={form.amount || ""}
            onChange={({ target }) => changeForm(target)}
          />
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          title="Actualizar"
          disabled={pending}
          onClick={handle}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
