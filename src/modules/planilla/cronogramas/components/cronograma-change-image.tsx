/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useCronogramaChangeImage } from "../hooks/use-person-change-image";
import { Media } from "reactstrap";
import ProfileImage from "@assets/images/perfil.jpg";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { Show } from "@common/show";

export const CronogramaChangeImage = () => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [isUpdate, setIsUpdate] = useState<boolean>(false);

  const cronogramaChangeImage = useCronogramaChangeImage();

  const handleFile = ({ files }: { files: FileList | null }) => {
    if (!files?.length) return cronogramaChangeImage.setImage("");
    cronogramaChangeImage.setImage(files[0]);
    setIsUpdate(true);
  };

  useEffect(() => {
    if (isUpdate) {
      cronogramaChangeImage.save();
    }
  }, [isUpdate]);

  useEffect(() => {
    if (isUpdate) setIsUpdate(false);
  }, [isUpdate]);

  return (
    <div
      style={{
        display: "inline-block",
        width: "auto",
        position: "relative",
      }}
    >
      <Show
        condition={cronograma.selloUrl != ""}
        isDefault={
          <div className="mb-1">
            <u className="text-primary cursor-pointer">
              {cronogramaChangeImage.pending
                ? "subiendo..."
                : "Cambiar de imagen"}
              <input
                style={{ width: "100%" }}
                disabled={cronogramaChangeImage.pending}
                onChange={({ target }) => handleFile(target)}
                className="upload cursor-pointer"
                type="file"
                accept="image/png,image/jpg,image/jpeg"
              />
            </u>
          </div>
        }
      >
        <div className="avatar">
          <Media
            body
            src={cronograma?.selloUrl ? cronograma?.selloUrl : ProfileImage.src}
            alt=""
            data-intro="This is Profile image"
            style={{
              objectFit: "cover",
              width: "100px",
              height: "100px",
              borderRadius: "50%",
              border: "0.5em solid white",
              boxShadow: "0px 0px 0px 1px black",
            }}
          />
        </div>
        <div
          className="icon-wrapper cursor-pointer"
          data-intro="Change Profile image here"
        >
          <i
            className="icofont icofont-pencil-alt-5 cursor-pointer"
            style={{
              position: "absolute",
              bottom: "5px",
              right: "40%",
            }}
          >
            <input
              id="change-sello"
              disabled={cronogramaChangeImage.pending}
              onChange={({ target }) => handleFile(target)}
              className="upload"
              type="file"
              accept="image/png,image/jpg,image/jpeg"
            />
          </i>
        </div>
      </Show>
    </div>
  );
};
