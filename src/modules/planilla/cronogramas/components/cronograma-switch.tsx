import { encrypt } from "@services/crypt";
import { RootState } from "@store/store";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { ICronogramaEntity } from "../dtos/cronograma.entity";
import { useCronogramaList } from "../hooks/use-cronograma-list";
import { CronogramaTable } from "./cronograma-table";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const CronogramaSwitch = ({ isOpen, onClose }: IProps) => {
  const { cronogramas, cronograma } = useSelector(
    (state: RootState) => state.cronograma
  );

  const cronogramaList = useCronogramaList();

  const router = useRouter();

  const [isFetch, setIsFetch] = useState<boolean>(false);

  const handleSearch = (year: number, month: number, principal: boolean) => {
    cronogramaList.setYear(year);
    cronogramaList.setMonth(month);
    cronogramaList.setPrincipal(principal);
    setIsFetch(true);
  };

  const handleClick = (cronogama: ICronogramaEntity) => {
    const { pathname } = router;
    router.push({
      pathname,
      query: { id: encrypt(`${cronogama.id}`) },
    });
    // close window
    if (typeof onClose == "function") {
      onClose();
    }
  };

  useEffect(() => {
    if (isOpen) setIsFetch(true);
  }, [isOpen]);

  useEffect(() => {
    if (cronograma.id) {
      cronogramaList.setPrincipal(cronograma?.planilla?.principal || false);
    }
  }, [cronograma]);

  useEffect(() => {
    if (isFetch) cronogramaList.fetch();
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <Modal isOpen={isOpen} style={{ minWidth: "90vh" }}>
      <ModalHeader toggle={onClose}>Cambiar de Cronograma</ModalHeader>
      <ModalBody>
        <CronogramaTable
          loading={cronogramaList.pending}
          defaultYear={cronogramaList.year}
          defaultMonth={cronogramaList.month}
          defaultPrincipal={cronogramaList.principal}
          onlyInfo={true}
          data={cronogramas?.items || []}
          onQuerySearch={handleSearch}
          onClick={handleClick}
        />
      </ModalBody>
    </Modal>
  );
};
