import React, { useEffect, useMemo, useState } from "react";
import { Save, X } from "react-feather";
import {
  Button,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Table,
} from "reactstrap";
import { Dropzone } from "@common/dropzone";
import { useExcelToJson } from "src/shared/excel/hooks/use-excel-to-json";
import { toast } from "react-toastify";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { format } from "currency-formatter";
import { RootState } from "@store/store";
import { Collection } from "collect.js";
import { CronogramaImportConceptInterface } from "../domain/interfaces/cronograma-import-concept.interface";
import { ButtonLoading } from "@common/button/button-loading";
import { useCronogramaImportDiscount } from "../hooks/use-cronograma-import-discount";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: () => void;
}

export const CronogramaImportDiscount = ({ isOpen, onClose }: IProps) => {
  const { dark } = useSelector((state: RootState) => state.screen);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [loadingImport, setLoadingImport] = useState(false);

  const [dataImport, setDataImport] = useState<
    CronogramaImportConceptInterface[]
  >([]);

  const excelToJson = useExcelToJson<{
    import: CronogramaImportConceptInterface[];
  }>();

  const { handle, isLoading, isSuccess } = useCronogramaImportDiscount();

  const total = useMemo(() => {
    return new Collection(dataImport).sum("MONTO").toString();
  }, [dataImport]);

  const loadImport = async (files: File[]) => {
    if (!files?.length) return;
    setLoadingImport(true);
    excelToJson
      .readFile(files[0])
      .then((data) => {
        const importData = data.import;
        if (!importData) throw new Error("No se encontró datos a importar!!!");
        setDataImport(importData);
      })
      .catch((err) => {
        setDataImport([]);
        toast.error(err.message || "No se pudo importar el archivo");
      })
      .finally(() => setLoadingImport(false));
  };

  useEffect(() => {
    if (isSuccess) setDataImport([]);
  }, [isSuccess]);

  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalHeader toggle={onClose}>Importar Descuentos</ModalHeader>
      <ModalBody>
        <FormGroup className="mb-3">
          <a
            className="text-primary cursor-pointer"
            onClick={() =>
              excelToJson.formatter(
                ["NUMERO_DOCUMENTO", "CONCEPTO", "REFERENCIA", "MONTO"],
                "import"
              )
            }
            target="_blank"
            rel="noreferrer"
          >
            <u>Formato de importación</u>
          </a>
        </FormGroup>

        <Show
          condition={dataImport?.length > 0}
          isDefault={
            <FormGroup className="mb-3">
              <Show
                condition={!loadingImport}
                isDefault={<LoadingSimple loading />}
              >
                <Dropzone
                  title="Subir Excel"
                  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                  multiple={false}
                  files={[]}
                  onFiles={loadImport}
                />
              </Show>
            </FormGroup>
          }
        >
          <div>
            <div>
              FILAS:{" "}
              <b>
                {dataImport.length}{" "}
                {dataImport.length <= 1 ? "Regístro" : "Regístros"}
              </b>
            </div>
            <div>
              TOTAL: <b>{format(parseFloat(total), { code: "PEN" })}</b>
            </div>
            <Table responsive>
              <thead>
                <tr>
                  <th className={dark ? "text-white" : ""}>N°</th>
                  <th className={dark ? "text-white" : ""}>N° DOCUMENTO</th>
                  <th className={dark ? "text-white" : ""}>CONCEPTO</th>
                  <th className={dark ? "text-white" : ""}>REFERENCIA</th>
                  <th className={dark ? "text-white text-right" : "text-right"}>
                    MONTO
                  </th>
                </tr>
              </thead>
              <tbody>
                {dataImport?.map((item, index) => (
                  <tr key={`import-table-${index}`}>
                    <td className={dark ? "text-white" : ""}>{index + 1}</td>
                    <td className={dark ? "text-white" : ""}>
                      {item.NUMERO_DOCUMENTO}
                    </td>
                    <td className={dark ? "text-white" : ""}>
                      {item.CONCEPTO}
                    </td>
                    <td className={dark ? "text-white uppercase" : ""}>
                      {item.REFERENCIA}
                    </td>
                    <td className={`text-right ${dark ? "text-white" : ""}`}>
                      {format(parseFloat(item.MONTO.toString()), {
                        code: "PEN",
                      })}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Show>
      </ModalBody>
      <Show condition={dataImport.length > 0}>
        <ModalFooter>
          <Show
            condition={!isLoading}
            isDefault={
              <ButtonLoading title="Importando..." color="primary" loading />
            }
          >
            <Button
              color="danger"
              outline
              disabled={isLoading}
              onClick={() => setDataImport([])}
            >
              <X className="icon" /> Cancelar
            </Button>
            <Button
              color="primary"
              title="Actualizar"
              disabled={isLoading}
              onClick={() => handle(cronograma, dataImport)}
            >
              <Save className="icon" /> Importar
            </Button>
          </Show>
        </ModalFooter>
      </Show>
    </Modal>
  );
};
