import { toast } from "react-toastify";
import { CronogramaImportConceptInterface } from "../domain/interfaces/cronograma-import-concept.interface";
import { CronogramaImportRemunerationBodyDto } from "../dtos/cronograma-import-remuneration.dto";
import { ICronogramaEntity } from "../dtos/cronograma.entity";
import { useImportRemunerationMutation } from "../features/cronograma.rtk";

export function useCronogramaImportRemuneration() {
  const [fetch, { isLoading, isSuccess }] = useImportRemunerationMutation();

  const handle = (
    cronograma: ICronogramaEntity,
    data: CronogramaImportConceptInterface[]
  ) => {
    const remunerations: CronogramaImportRemunerationBodyDto[] = data.map(
      (item) => ({
        documentNumber: item.NUMERO_DOCUMENTO.toString(),
        typeRemunerationId: parseInt(item.CONCEPTO.toString()),
        typeRemunerationName: item.REFERENCIA,
        amount: parseFloat(item.MONTO.toString()),
      })
    );

    fetch({ id: cronograma.id, remunerations })
      .unwrap()
      .then((data) => {
        if (data.errors?.length) {
          throw new Error("Se encontró inconsistencias");
        } else {
          toast.success("Importación completa!!!");
        }
      })
      .catch((err) =>
        toast.error(err.message || "No se pudo importar las remuneraciones")
      );
  };

  return { handle, isLoading, isSuccess };
}
