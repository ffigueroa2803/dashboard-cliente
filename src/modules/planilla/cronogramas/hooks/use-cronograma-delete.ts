import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";
import { useConfirm } from "@common/confirm/use-confirm";
import { cronogramaActions } from "../store";

const request = planillaRequest();

export const useCronogramaDelete = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Eliminar",
          message: "¿Estás seguro en elimnar el cronograma?",
          labelSuccess: "Eliminar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Eliminando Cronograma...");
          await request
            .destroy(`cronogramas/${cronograma.id}`)
            .then((res) => {
              toast.dismiss();
              toast.success("El cronograma se eliminó correctamente!");
              dispatch(cronogramaActions.deleteItem(cronograma.id));
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("No se pudo eliminar el cronograma");
              reject(err);
            });
          setPending(false);
        })
        .catch((err) => reject(err));
    });
  };

  return {
    pending,
    execute,
  };
};
