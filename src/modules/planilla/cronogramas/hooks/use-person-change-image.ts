import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { cronogramaActions } from "../store";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useCronogramaChangeImage = () => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [image, setImage] = useState<Blob | string>("");
  const [pending, setPending] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    const form = new FormData();
    form.append("file", image);
    // send request
    await request
      .put(`cronogramas/${cronograma.id}/changeImage`, form)
      .then(({ data }) => {
        dispatch(cronogramaActions.changeImage(data.selloUrl));
        toast.success("Imagen cambiada!!!");
      })
      .catch(() => toast.error("No se pudo cambiar la imagen"));
    setPending(false);
  };

  return {
    pending,
    image,
    setImage,
    save,
  };
};
