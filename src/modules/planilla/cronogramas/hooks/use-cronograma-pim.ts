import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { cronogramaActions } from "../store";

const request = planillaRequest();

export const useCronogramaPim = (defaultQuerySearch?: string) => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );
  const [pending, setPending] = useState<boolean>(false);

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`cronogramas/${cronograma.id}/pims`, { params })
      .then((res) => dispatch(cronogramaActions.paginatePim(res.data)))
      .catch(() => dispatch(cronogramaActions.paginatePim(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    page,
    setPage,
    limit,
    setLimit,
    querySearch,
    setQuerySearch,
    fetch,
  };
};
