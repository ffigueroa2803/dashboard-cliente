/* eslint-disable no-unused-vars */
import { useConfirm } from "@common/confirm/use-confirm";
import { IInputHandle } from "@common/dtos/input-handle";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { cronogramaActions } from "../store";

const request = planillaRequest();

interface IFormProps {
  resolution?: string;
  pimId?: number;
  typeCategoryId?: number;
  days: number;
}

const dataDefault: IFormProps = {
  resolution: undefined,
  pimId: undefined,
  typeCategoryId: undefined,
  days: 30,
};

export const useCronogramaChangeDay = () => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IFormProps>(dataDefault);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clear = () => {
    setForm(dataDefault);
  };

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Cambio de días",
          message: "¿Estás seguro en actualizar los datos?",
          labelSuccess: "Actualizar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Actualizando...");

          // settings filters
          const filters = {
            pimId: form.pimId,
            contract: {
              resolution: form.resolution,
              typeCategoryId: form.typeCategoryId
            }
          }

          // settings data
          const payload = {
            days: parseFloat(`${form.days}`)
          };

          // send
          await request
            .put(`cronogramas/${cronograma.id}/process/historialDays`, { filters, payload })
            .then((res) => {
              toast.dismiss();
              toast.success("Los cambios se guardarón correctamente!");
              dispatch(cronogramaActions.changeState(true));
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("Ocurrío un error al actualizar los días");
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    form,
    changeForm,
    pending,
    execute,
    clear,
  };
};
