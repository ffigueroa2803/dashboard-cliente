import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { processCronograma } from "../apis";

export const useCronogramaProcess = () => {
  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const execute = async () => {
    setPending(true);
    toast.info("Processando Cronograma");
    await processCronograma(cronograma.id)
      .then(() => {
        toast.dismiss();
        toast.success("El cronograma se procesó correctamente!");
      })
      .catch(() => toast.error("No se pudo procesar el cronograma"));
    setPending(false);
  };

  return {
    pending,
    execute,
  };
};
