import { toast } from "react-toastify";
import { CronogramaImportConceptInterface } from "../domain/interfaces/cronograma-import-concept.interface";
import { CronogramaImportDiscountBodyDto } from "../dtos/cronograma-import-discount.dto";
import { ICronogramaEntity } from "../dtos/cronograma.entity";
import { useImportDiscountMutation } from "../features/cronograma.rtk";

export function useCronogramaImportDiscount() {
  const [fetch, { isLoading, isSuccess }] = useImportDiscountMutation();

  const handle = (
    cronograma: ICronogramaEntity,
    data: CronogramaImportConceptInterface[]
  ) => {
    const discounts: CronogramaImportDiscountBodyDto[] = data.map((item) => ({
      documentNumber: item.NUMERO_DOCUMENTO.toString(),
      typeDiscountId: parseInt(item.CONCEPTO.toString()),
      typeDiscountName: item.REFERENCIA,
      amount: parseFloat(item.MONTO.toString()),
    }));

    fetch({ id: cronograma.id, discounts })
      .unwrap()
      .then((data) => {
        if (data.errors?.length) {
          throw new Error("Se encontró inconsistencias");
        } else {
          toast.success("Importación completa!!!");
        }
      })
      .catch((err) =>
        toast.error(err.message || "No se pudo importar los descuentos")
      );
  };

  return { handle, isLoading, isSuccess };
}
