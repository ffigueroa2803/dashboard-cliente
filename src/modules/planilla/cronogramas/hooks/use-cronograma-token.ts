import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useCronogramaToken = () => {
  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const execute = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .put(`cronogramas/${cronograma.id}/generateToken`)
        .then((res) => {
          toast.success(`Los tokens se generarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.error(`No se pudo generar los tokens`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    execute,
  };
};
