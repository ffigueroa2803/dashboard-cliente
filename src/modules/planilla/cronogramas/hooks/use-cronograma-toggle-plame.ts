import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";
import { useConfirm } from "@common/confirm/use-confirm";
import { cronogramaActions } from "../store";

const request = planillaRequest();

export const useCronogramaTogglePlame = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "PDT-PLAME",
          message: `¿Estás seguro en ${
            cronograma.isPlame ? "quitar" : "aplicar"
          } el cronograma al PDF-PLAME?`,
          labelSuccess: cronograma.isPlame ? "Quitar" : "Aplicar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info(
            `${
              cronograma.isPlame
                ? "Quitando de PDT-PLAME"
                : "Aplicando al PDT-PLAME"
            } `
          );
          await request
            .put(`cronogramas/${cronograma.id}/togglePlame`)
            .then((res) => {
              toast.dismiss();
              toast.success("Los cambios se guardaron correctamente!");
              dispatch(
                cronogramaActions.changePlame(cronograma.isPlame ? false : true)
              );
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("No se pudo guardar los cambios");
              reject(err);
            });
          setPending(false);
        })
        .catch((err) => reject(err));
    });
  };

  return {
    pending,
    execute,
  };
};
