/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";
import { useConfirm } from "@common/confirm/use-confirm";
import { cronogramaActions } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

export enum MassiveRemunerationEnum {
  Normal = "Normal",
  Increment = "Increment",
  Decrement = "Decrement"
}

interface IFormProps {
  typeRemunerationId: number;
  pimId?: number;
  typeCategoryId?: number;
  isBase?: boolean;
  isSync: boolean;
  mode: MassiveRemunerationEnum;
  amount: number;
}

const dataDefault: IFormProps = {
  typeRemunerationId: 0,
  pimId: undefined,
  typeCategoryId: undefined,
  isSync: false,
  mode: MassiveRemunerationEnum.Normal,
  isBase: undefined,
  amount: 0,
};

export const useCronogramaMassiveRemuneration = () => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IFormProps>(dataDefault);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clear = () => {
    setForm(dataDefault);
  };

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Remuneración Masiva",
          message: "¿Estás seguro en actualizar los datos?",
          labelSuccess: "Actualizar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Actualizando...");
          // settings data
          const payload: any = {
            amount: parseFloat(`${form.amount}`),
            isBase: form?.isBase,
            isSync: form.isSync,
            mode: form.mode,
          };
          // add filters
          if (form.typeRemunerationId) {
            payload.typeRemunerationIds = [form.typeRemunerationId];
          }

          if (form.typeCategoryId) {
            payload.typeCategoryIds = [form.typeCategoryId];
          }

          if (form.pimId) {
            payload.pimIds = [form.pimId];
          }
          // send
          await request
            .put(`cronogramas/${cronograma.id}/process/remunerations`, payload)
            .then((res) => {
              toast.dismiss();
              toast.success("Los cambios se guardarón correctamente!");
              dispatch(cronogramaActions.changeState(true));
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("Ocurrío un error al actualizar las remuneraciones");
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    form,
    changeForm,
    pending,
    execute,
    clear,
  };
};
