import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";
import { useConfirm } from "@common/confirm/use-confirm";
import { cronogramaActions } from "../store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

interface IFormProps {
  pimId: number;
  nextPimId: number;
}

const dataDefault = {
  pimId: 0,
  nextPimId: 0,
};

export const useCronogramaChangePim = () => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IFormProps>(dataDefault);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clear = () => {
    setForm(dataDefault);
  };

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Cambio de PIM",
          message: "¿Estás seguro en cambiar el PIM?",
          labelSuccess: "Cambiar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Cambiando de PIM...");
          // settings data
          const payload = {
            pimIds: [form.pimId],
            nextPimId: form.nextPimId,
          };
          // send
          await request
            .put(`cronogramas/${cronograma.id}/process/pims`, payload)
            .then((res) => {
              toast.dismiss();
              toast.success("Los cambios se guardarón correctamente!");
              dispatch(cronogramaActions.changeState(true));
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("Ocurrío un error al cambiar de PIM");
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    form,
    changeForm,
    pending,
    execute,
    clear,
  };
};
