import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { planillaRequest } from "@services/planilla.request";
import { useConfirm } from "@common/confirm/use-confirm";
import { cronogramaActions } from "../store";

const request = planillaRequest();

export const useCronogramaOpen = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Abrir",
          message: "¿Estás seguro en abrir el cronograma?",
          labelSuccess: "Abrir",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Cerrando Cronograma...");
          await request
            .put(`cronogramas/${cronograma.id}/open`)
            .then((res) => {
              toast.dismiss();
              toast.success("El cronograma se abrío correctamente!");
              dispatch(cronogramaActions.changeState(true));
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("No se pudo abrir el cronograma");
              reject(err);
            });
          setPending(false);
        })
        .catch((err) => reject(err));
    });
  };

  return {
    pending,
    execute,
  };
};
