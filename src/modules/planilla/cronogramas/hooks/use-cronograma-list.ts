import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch } from "react-redux";
import { cronogramaActions } from "../store";
import { DateTime } from "luxon";

const currentDate = DateTime.now();

const request = planillaRequest();

export const useCronogramaList = (defaultQuerySearch?: string) => {
  const dispatch = useDispatch();

  const [page, setPage] = useState<number>(1);
  const [year, setYear] = useState<number>(currentDate.year);
  const [month, setMonth] = useState<number>(currentDate.month);
  const [limit, setLimit] = useState<number>(100);
  const [principal, setPrincipal] = useState<boolean>(true);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );
  const [pending, setPending] = useState<boolean>(false);

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    params.set("year", `${year}`);
    params.set("month", `${month}`);
    params.set("principal", `${principal}`);
    // set request
    await request
      .get(`cronogramas`, { params })
      .then((res) => dispatch(cronogramaActions.paginate(res.data)))
      .catch(() => dispatch(cronogramaActions.paginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    page,
    limit,
    year,
    month,
    principal,
    querySearch,
    setPage,
    setLimit,
    setYear,
    setMonth,
    setPrincipal,
    setQuerySearch,
    fetch,
  };
};
