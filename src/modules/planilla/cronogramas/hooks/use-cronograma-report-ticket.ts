import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";

const request = planillaRequest();

export const useCronogramaReportTicket = () => {
  const [pending, setPending] = useState<boolean>(false);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const confirm = useConfirm();

  const generateExcel = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Exportar",
          message: "¿Estás seguro en exportar el cronograma?",
          labelSuccess: "Exportar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info(`Exportando datos...`);
          await request
            .get(`cronogramas/${cronograma.id}/reports/tickets.xlsx`, {
              responseType: "blob",
            })
            .then((res) => {
              const url = URL.createObjectURL(res.data);
              const a = document.createElement("a");
              a.href = url;
              a.download = `export-data.xlsx`;
              a.click();
              resolve(res.data);
            })
            .catch((err) => {
              toast.error(`No se pudo exportar los datos`);
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    pending,
    generateExcel,
  };
};
