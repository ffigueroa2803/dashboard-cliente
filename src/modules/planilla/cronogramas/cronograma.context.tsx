import { IInputHandle } from "@common/dtos/input-handle";
import { LoadingContent } from "@common/loading/components/loading-content";
import { workActions } from "@modules/scale/works/store";
import { RootState } from "@store/store";
import { createContext, FC, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { historialActions } from "../historials/store";
import { findHistorialsToCronograma } from "./apis";
import { IFilterHistorialToCronograma } from "./dtos/filter-cronogramas.dto";

export interface IContext {
  filters: IFilterHistorialToCronograma;
  changeFilter: (input: IInputHandle) => void;
  page: number;
  nextPage: () => void;
  prevPage: () => void;
  isLoading: boolean;
  setIsLoading: (value: boolean) => void;
  isBlock: boolean;
  setIsBlock: (value: boolean) => void;
  isRefresh: boolean;
  onRefresh: () => void;
  canNext: boolean;
  canPrev: boolean;
  isFilter: boolean;
  setIsFilter: (value: boolean) => void;
  resetFilter: () => void;
  searchData: () => void;
}

const filtersDefault: IFilterHistorialToCronograma = {
  querySearch: "",
};

export const contextDefaultValues: IContext = {
  page: 1,
  filters: filtersDefault,
  isLoading: false,
  isBlock: false,
  isRefresh: true,
  canNext: true,
  canPrev: true,
  isFilter: false,
  nextPage: () => {},
  prevPage: () => {},
  setIsLoading: (value: boolean) => {},
  setIsBlock: (value: boolean) => {},
  onRefresh: () => {},
  setIsFilter: (value: boolean) => {},
  changeFilter: (input: IInputHandle) => {},
  resetFilter: () => {},
  searchData: () => {},
};

export const CronogramaContext = createContext<IContext>(contextDefaultValues);

export const CronogramaProvider: FC = ({ children }) => {
  const [page, setPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isBlock, setIsBlock] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [isFilter, setIsFilter] = useState<boolean>(false);
  const [filters, setFilters] =
    useState<IFilterHistorialToCronograma>(filtersDefault);

  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { historials } = useSelector((state: RootState) => state.historial);

  const nextPage = () => {
    if (isLoading) return;
    setPage((prev) => prev + 1);
  };

  const prevPage = () => {
    if (isLoading) return;
    const isOne = page - 1 == 1;
    setPage((prev) => prev - 1);
    setIsRefresh(isOne);
  };

  const canPrev = useMemo(() => {
    return page > 1;
  }, [page, historials]);

  const canNext = useMemo(() => {
    const meta = historials.meta;
    return meta.totalPages > page;
  }, [page, historials]);

  const onRefresh = () => setIsRefresh(true);

  const resetFilter = () => {
    setPage(1);
    setIsFilter(false);
    setIsRefresh(true);
    setFilters(filtersDefault);
  };

  const searchData = () => {
    setPage(1);
    setIsRefresh(true);
  };

  const handleData = async () => {
    setIsLoading(true);
    await findHistorialsToCronograma(cronograma.id, {
      page,
      limit: 1,
      ...filters,
    })
      .then((data) => dispatch(historialActions.paginate(data)))
      .catch(() =>
        dispatch(
          historialActions.paginate({
            items: [],
            meta: {
              totalPages: 0,
              itemsPerPage: 1,
              totalItems: 0,
            },
          })
        )
      );
    setIsLoading(false);
  };

  const settings = () => {
    const data = historials.items?.[0] || {};
    // update settings
    dispatch(historialActions.find(data as any));
    dispatch(workActions.find(data?.info?.contract?.work || ({} as any)));
  };

  const clear = () => {
    dispatch(historialActions.find({} as any));
    dispatch(workActions.find({} as any));
  };

  const changeFilter = ({ value, name }: IInputHandle) => {
    setFilters((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  useEffect(() => {
    if (cronograma.id) setIsRefresh(true);
  }, [cronograma]);

  useEffect(() => {
    if (historials.items?.length) settings();
    else clear();
  }, [historials.items]);

  useEffect(() => {
    if (isRefresh) handleData();
  }, [isRefresh]);

  useEffect(() => {
    if (page > 1) handleData();
  }, [page]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <CronogramaContext.Provider
      value={{
        filters,
        page,
        isLoading,
        isBlock,
        isRefresh,
        canNext,
        canPrev,
        isFilter,
        changeFilter,
        nextPage,
        prevPage,
        setIsLoading,
        setIsBlock,
        onRefresh,
        setIsFilter,
        resetFilter,
        searchData,
      }}
    >
      <>
        {children}
        {/* loading historial */}
        <LoadingContent loading={isLoading} />
      </>
    </CronogramaContext.Provider>
  );
};
