import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IPimEntity } from "../pims/dtos/pim.entity";
import { ICronogramaEntity } from "./dtos/cronograma.entity";

export interface CronogramaState {
  cronogramas: ResponsePaginateDto<ICronogramaEntity>;
  cronograma: ICronogramaEntity;
  pims: ResponsePaginateDto<IPimEntity>;
  option: string;
  tabIndex: number;
}

const initialState: CronogramaState = {
  cronogramas: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  pims: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  cronograma: {} as any,
  option: "",
  tabIndex: 0,
};

const cronogramaStore = createSlice({
  name: "planilla@cronogramas",
  initialState,
  reducers: {
    paginate: (
      state: CronogramaState,
      { payload }: PayloadAction<ResponsePaginateDto<ICronogramaEntity>>
    ) => {
      state.cronogramas = payload;
    },
    find: (
      state: CronogramaState,
      { payload }: PayloadAction<ICronogramaEntity>
    ) => {
      state.cronograma = payload;
      return state;
    },
    deleteItem: (
      state: CronogramaState,
      { payload }: PayloadAction<number>
    ) => {
      state.cronogramas.items = state.cronogramas.items?.filter((item) => {
        return item.id !== payload;
      });
      // response state
      return state;
    },
    changeOption: (
      state: CronogramaState,
      { payload }: PayloadAction<string>
    ) => {
      state.option = payload;
      return state;
    },
    changeTab: (state: CronogramaState, { payload }: PayloadAction<number>) => {
      state.tabIndex = payload;
    },
    setCount: (state: CronogramaState, { payload }: PayloadAction<number>) => {
      state.cronograma.historialsCount = payload;
      return state;
    },
    changePlame: (
      state: CronogramaState,
      { payload }: PayloadAction<boolean>
    ) => {
      state.cronograma.isPlame = payload;
      return state;
    },
    setCountItem: (
      state: CronogramaState,
      { payload }: PayloadAction<{ id: number; count: number }>
    ) => {
      state.cronogramas?.items?.map((cronograma) => {
        if (cronograma.id != payload.id) return cronograma;
        cronograma.historialsCount = payload.count;
        return cronograma;
      });
      return state;
    },
    changeState: (
      state: CronogramaState,
      { payload }: PayloadAction<boolean>
    ) => {
      state.cronograma.state = payload;
      state.cronogramas?.items?.map((item) => {
        if (item.id == state.cronograma?.id) {
          item.state = payload;
        }
        return item;
      });
      return state;
    },
    changeImage: (
      state: CronogramaState,
      { payload }: PayloadAction<string>
    ) => {
      state.cronograma.selloUrl = payload;
      state.cronogramas.items.map((item) => {
        if (item.id == state.cronograma.id) {
          item.selloUrl = payload;
        }
        return item;
      });
      return state;
    },
    paginatePim: (
      state: CronogramaState,
      { payload }: PayloadAction<ResponsePaginateDto<IPimEntity>>
    ) => {
      state.pims = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.cronograma };
    },
  },
});

export const cronogramaReducer = cronogramaStore.reducer;

export const cronogramaActions = cronogramaStore.actions;
