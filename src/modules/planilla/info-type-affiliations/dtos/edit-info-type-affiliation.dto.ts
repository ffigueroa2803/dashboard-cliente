export interface IEditInfoTypeAffiliationDto {
  infoId: number;
  typeAffiliationId: number;
  isPercent: boolean;
  amount: number;
  terminationDate: string;
}
