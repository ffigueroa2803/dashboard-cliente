import { ITypeAffiliationEntity } from "@modules/planilla/type-affiliations/dtos/type-affiliation.entity";

export interface IInfoTypeAffiliationEntity {
  id: number;
  infoId: number;
  typeAffiliationId: number;
  isPercent: boolean;
  percent: number;
  amount: number;
  state: boolean;
  terminationDate?: string;
  typeAffiliation?: ITypeAffiliationEntity;
}
