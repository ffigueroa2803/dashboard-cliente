export interface ICreateInfoTypeAffiliationDto {
  infoId: number;
  typeAffiliationId: number;
  isPercent: boolean;
  amount: number;
  terminationDate: string;
}
