export interface IFormInfoTypeAffiliationDto {
  infoId: number;
  typeAffiliationId: number;
  isPercent: boolean;
  amount: number;
  terminationDate: string;
}
