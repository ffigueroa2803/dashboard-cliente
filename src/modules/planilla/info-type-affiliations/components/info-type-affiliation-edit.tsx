/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeAffiliationForm } from "./info-type-affiliation-form";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { RootState } from "@store/store";
import { useInfoTypeAffiliationEdit } from "../hooks/use-info-type-affiliation-edit";
import { useInfoTypeAffiliationFind } from "../hooks/use-info-type-affiliation-find";
import { infoTypeAffiliationActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeAffiliationEntity) => void;
}

export const InfoTypeAffiliationEdit = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const dispatch = useDispatch();

  const { infoTypeAffiliation } = useSelector(
    (state: RootState) => state.infoTypeAffiliation
  );

  const infoEdit = useInfoTypeAffiliationEdit();
  const infoFind = useInfoTypeAffiliationFind();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await infoEdit
      .update()
      .then(async (data) => {
        const result = await infoFind
          .find(infoTypeAffiliation.id)
          .then((res) => res)
          .catch(() => data);
        // update info type remuneration
        dispatch(infoTypeAffiliationActions.find(result));
        if (typeof onSave == "function") onSave(result);
      })
      .catch(() => null);
    setPending(false);
  };

  useEffect(() => {
    if (infoTypeAffiliation?.id && isOpen) infoEdit.clearForm();
  }, [infoTypeAffiliation, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Configuración Afiliación
      </ModalHeader>
      <ModalBody>
        <InfoTypeAffiliationForm
          isEdit={true}
          form={infoEdit.form}
          onChange={infoEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
