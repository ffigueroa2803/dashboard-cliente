import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { Plus, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { useInfoTypeAffiliationsToInfo } from "../hooks/use-info-type-affiliations-to-info";
import { infoTypeAffiliationActions } from "../store";
import { InfoTypeAffiliationCreate } from "./info-type-affiliation-create";
import { InfoTypeAffiliationEdit } from "./info-type-affiliation-edit";
import { InfoTypeAffiliationTable } from "./info-type-affiliation-table";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onSave?: (affiliation: IInfoTypeAffiliationEntity) => void;
  onCreated?: (affiliation: IInfoTypeAffiliationEntity) => void;
}

export const InfoTypeAffiliationContent = ({
  isOpen,
  onClose,
  onSave,
  onCreated,
}: IProps) => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const { infoTypeAffiliations, option } = useSelector(
    (state: RootState) => state.infoTypeAffiliation
  );

  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const { setPage, setLimit, fetch } = useInfoTypeAffiliationsToInfo();

  const handlePage = (newPage: number) => {
    setPage(newPage);
    setIsRefresh(true);
  };

  const handleRowPer = (newRowPer: number, currentPage: number) => {
    setLimit(newRowPer);
    setPage(currentPage);
    setIsRefresh(true);
  };

  const handleCreated = (
    createdInfoTypeAffiliation: IInfoTypeAffiliationEntity
  ) => {
    dispatch(infoTypeAffiliationActions.changeOption("REFRESH"));
    // events
    if (typeof onCreated == "function") {
      onCreated(createdInfoTypeAffiliation);
    }
  };

  const handleEdit = (infoTypeAffiliation: IInfoTypeAffiliationEntity) => {
    dispatch(infoTypeAffiliationActions.find(infoTypeAffiliation));
    dispatch(infoTypeAffiliationActions.changeOption("EDIT"));
  };

  const handleDeleted = () => {
    dispatch(infoTypeAffiliationActions.changeOption("REFRESH"));
  };

  const handleUpdated = (
    updatedInfoTypeAffiliation: IInfoTypeAffiliationEntity
  ) => {
    dispatch(infoTypeAffiliationActions.updateItem(updatedInfoTypeAffiliation));
    dispatch(infoTypeAffiliationActions.changeOption("REFRESH"));
    // events
    if (typeof onSave == "function") {
      onSave(updatedInfoTypeAffiliation);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setPage(1);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && info?.id) setIsRefresh(true);
  }, [info, isOpen]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(infoTypeAffiliationActions.changeOption(""));
    }
  }, [option]);

  useEffect(() => {
    if (isRefresh) fetch();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        style={{ minWidth: "90vh" }}
        backdrop={!["CREATE", "EDIT"].includes(option)}
      >
        <ModalHeader toggle={onClose}>
          <Settings className="icon" /> Configurarción Afiliaciones
        </ModalHeader>
        <ModalBody>
          <InfoTypeAffiliationTable
            perPage={infoTypeAffiliations?.meta?.itemsPerPage}
            totalItems={infoTypeAffiliations?.meta?.totalItems}
            data={infoTypeAffiliations?.items || []}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleRowPer}
            onDelete={handleDeleted}
            onClick={handleEdit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            outline
            onClick={() =>
              dispatch(infoTypeAffiliationActions.changeOption("CREATE"))
            }
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* create */}
      <InfoTypeAffiliationCreate
        isOpen={option == "CREATE"}
        onClose={() => dispatch(infoTypeAffiliationActions.changeOption(""))}
        onSave={handleCreated}
      />
      {/* edit */}
      <InfoTypeAffiliationEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(infoTypeAffiliationActions.changeOption(""))}
        onSave={handleUpdated}
      />
    </>
  );
};
