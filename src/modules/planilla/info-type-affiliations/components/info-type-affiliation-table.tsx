import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { Search, Edit, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import currencyFormatter from "currency-formatter";
import { useInfoTypeAffiliationDelete } from "../hooks/use-info-type-affiliation-delete";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (querySearch: string | string[]) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: IInfoTypeAffiliationEntity) => any;
}

interface IProps {
  data: IInfoTypeAffiliationEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: (infoTypeAffiliation: IInfoTypeAffiliationEntity) => void;
  onDelete?: (infoTypeAffiliation: IInfoTypeAffiliationEntity) => void;
}

export const InfoTypeAffiliationTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onDelete,
  options,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const infoDelete = useInfoTypeAffiliationDelete();

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IInfoTypeAffiliationEntity) => row.id,
      },
      {
        name: "Código",
        grow: true,
        center: true,
        selector: (row: IInfoTypeAffiliationEntity) => (
          <span className="badge badge-dark">
            {row?.typeAffiliation?.typeDiscount?.code || "N/A"}
          </span>
        ),
      },
      {
        name: "Descripción",
        wrap: true,
        selector: (row: IInfoTypeAffiliationEntity) => {
          return (
            <span className="capitalize">{row?.typeAffiliation?.name}</span>
          );
        },
      },
      {
        name: "Fecha de Fin",
        center: true,
        wrap: true,
        selector: (row: IInfoTypeAffiliationEntity) => {
          return <span>{row?.terminationDate || "N/A"}</span>;
        },
      },
      {
        name: "Porcentaje",
        center: true,
        wrap: true,
        selector: (row: IInfoTypeAffiliationEntity) => {
          return <span>{row?.isPercent ? "SI" : "NO"}</span>;
        },
      },
      {
        name: "Monto/Porcentaje",
        right: true,
        selector: (row: IInfoTypeAffiliationEntity) => (
          <Show
            condition={!row.isPercent}
            isDefault={
              <Show
                condition={parseFloat(`${row.percent}`) > 0}
                isDefault={<span>{row.amount}%</span>}
              >
                <b className="text-danger">{row.percent}%</b>
              </Show>
            }
          >
            <Show
              condition={parseFloat(`${row.amount}`) > 0}
              isDefault={
                <span>
                  {currencyFormatter.format(row.amount, { code: "PEN" })}
                </span>
              }
            >
              <b className="text-danger">
                {currencyFormatter.format(row.amount, { code: "PEN" })}
              </b>
            </Show>
          </Show>
        ),
      },
      {
        name: "Estado",
        center: true,
        wrap: true,
        selector: (row: IInfoTypeAffiliationEntity) => {
          return (
            <span className={`badge badge-${row.state ? "success" : "danger"}`}>
              {row?.state ? "Activo" : "Inactivo"}
            </span>
          );
        },
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: IInfoTypeAffiliationEntity) => (
          <>
            <Edit
              className="cursor-pointer icon"
              onClick={() => handleClick(row)}
            />
            <Trash
              className="cursor-pointer icon"
              onClick={() => handleDelete(row)}
            />
          </>
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (infoTypeAffiliation: IInfoTypeAffiliationEntity) => {
    if (typeof onClick == "function") onClick(infoTypeAffiliation);
  };

  const handleDelete = (infoTypeAffiliation: IInfoTypeAffiliationEntity) => {
    infoDelete
      .destroy(infoTypeAffiliation.id)
      .then(() => {
        if (typeof onDelete == "function") {
          onDelete(infoTypeAffiliation);
        }
      })
      .catch(() => null);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
