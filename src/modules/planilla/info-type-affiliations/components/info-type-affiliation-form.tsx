import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IFormInfoTypeAffiliationDto } from "../dtos/form-info-type-affiliation.dto";
import { TypeAffiliationSelect } from "@modules/planilla/type-affiliations/components/type-affiliation-select";
import Toggle from "@atlaskit/toggle";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const InfoTypeAffiliationForm = ({
  form,
  isEdit,
  onChange,
}: IProps<IFormInfoTypeAffiliationDto>) => {
  const { infoTypeAffiliation } = useSelector(
    (state: RootState) => state.infoTypeAffiliation
  );

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FormGroup className="mb-3">
          <label>
            Tipo Afiliación <b className="text-danger">*</b>
          </label>
          <Show
            condition={!isEdit}
            isDefault={
              <h6 className="capitalize">
                {infoTypeAffiliation?.typeAffiliation?.name || "N/A"}
                <hr />
              </h6>
            }
          >
            <TypeAffiliationSelect
              name="typeAffiliationId"
              value={form?.typeAffiliationId}
              onChange={onChange}
            />
          </Show>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Fecha de Fin <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="date"
              name="terminationDate"
              value={form?.terminationDate}
              onChange={({ target }) => onChange(target)}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Porcentaje</label>
          <div>
            <Toggle
              name="isPercent"
              isChecked={form.isPercent}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        {/* monto */}
        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({
                  name: target.name,
                  value: parseFloat(target.value),
                })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
