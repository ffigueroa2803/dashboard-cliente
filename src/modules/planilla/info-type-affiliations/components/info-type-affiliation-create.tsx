/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { useInfoTypeAffiliationCreate } from "../hooks/use-info-type-affiliation-create";
import { RootState } from "@store/store";
import { InfoTypeAffiliationForm } from "./info-type-affiliation-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (infoTypeAffiliation: IInfoTypeAffiliationEntity) => void;
}

export const InfoTypeAffiliationCreate = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const { info } = useSelector((state: RootState) => state.info);

  const infoCreate = useInfoTypeAffiliationCreate();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await infoCreate
      .save()
      .then((data) => {
        infoCreate.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => null);
    setPending(false);
  };

  const formDefault = () => {
    infoCreate.changeForm({ name: "infoId", value: info.id });
  };

  useEffect(() => {
    if (isOpen) formDefault();
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Configuración Afiliación</ModalHeader>
      <ModalBody>
        <InfoTypeAffiliationForm
          form={infoCreate.form}
          onChange={infoCreate.changeForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
