import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { toast } from "react-toastify";
import { IEditInfoTypeAffiliationDto } from "../dtos/edit-info-type-affiliation.dto";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

export const useInfoTypeAffiliationEdit = () => {
  const { infoTypeAffiliation } = useSelector(
    (state: RootState) => state.infoTypeAffiliation
  );

  const dataDefault: IEditInfoTypeAffiliationDto = {
    infoId: infoTypeAffiliation.infoId || 0,
    typeAffiliationId: infoTypeAffiliation.typeAffiliationId || 0,
    isPercent: infoTypeAffiliation.isPercent || false,
    amount: infoTypeAffiliation.isPercent
      ? infoTypeAffiliation.percent
      : infoTypeAffiliation.amount,
    terminationDate: infoTypeAffiliation.terminationDate || "",
  };

  const [form, setForm] = useState<IEditInfoTypeAffiliationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = (): Promise<IInfoTypeAffiliationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      const payload = Object.assign(form, {
        amount: parseFloat(`${form.amount}`),
        terminationDate: form.terminationDate || null,
      });
      // send
      setPending(true);
      await request
        .put(`infoTypeAffiliations/${infoTypeAffiliation.id}`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IInfoTypeAffiliationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  const clearForm = () => {
    setForm(dataDefault);
  };

  return {
    form,
    changeForm,
    clearForm,
    pending,
    update,
  };
};
