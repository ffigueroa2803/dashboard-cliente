import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";
import { toast } from "react-toastify";
import { IFormInfoTypeAffiliationDto } from "../dtos/form-info-type-affiliation.dto";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

const dataDefault: IFormInfoTypeAffiliationDto = {
  infoId: 0,
  typeAffiliationId: 0,
  isPercent: false,
  amount: 0,
  terminationDate: "",
};

export const useInfoTypeAffiliationCreate = () => {
  const [form, setForm] = useState<IFormInfoTypeAffiliationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const clearForm = () => {
    setForm(dataDefault);
  };

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const save = (): Promise<IInfoTypeAffiliationEntity> => {
    return new Promise(async (resolve, reject) => {
      const payload = Object.assign(form, {
        terminationDate: form.terminationDate || null,
      });
      // send
      setPending(true);
      await request
        .post(`infoTypeAffiliations`, payload)
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data as IInfoTypeAffiliationEntity);
        })
        .catch((err) => {
          toast.error(`No se pudo guardar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    form,
    setForm,
    clearForm,
    changeForm,
    save,
  };
};
