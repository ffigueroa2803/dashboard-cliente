import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeAffiliationEntity } from "../dtos/info-type-affiliation.entity";

const request = planillaRequest();

export const useInfoTypeAffiliationFind = () => {
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IInfoTypeAffiliationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`infoTypeAffiliations/${id}`)
        .then((res) => {
          resolve(res.data as IInfoTypeAffiliationEntity);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
