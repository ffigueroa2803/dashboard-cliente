import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IInfoTypeAffiliationEntity } from "./dtos/info-type-affiliation.entity";

export interface InfoTypeAffiliationState {
  infoTypeAffiliations: ResponsePaginateDto<IInfoTypeAffiliationEntity>;
  infoTypeAffiliation: IInfoTypeAffiliationEntity;
  option: string;
}

const initialState: InfoTypeAffiliationState = {
  infoTypeAffiliations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  infoTypeAffiliation: {} as any,
  option: "",
};

const InfoTypeAffiliationState = createSlice({
  name: "planilla@infoTypeAffiliations",
  initialState,
  reducers: {
    paginate: (
      state,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IInfoTypeAffiliationEntity>>
    ) => {
      state.infoTypeAffiliations = payload as any;
    },
    find: (state, { payload }: PayloadAction<IInfoTypeAffiliationEntity>) => {
      state.infoTypeAffiliation = payload as any;
    },
    updateItem: (
      state,
      { payload }: PayloadAction<IInfoTypeAffiliationEntity>
    ) => {
      state.infoTypeAffiliations?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }
        return item;
      });
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.infoTypeAffiliation };
    },
  },
});

export const infoTypeAffiliationReducer = InfoTypeAffiliationState.reducer;

export const infoTypeAffiliationActions = InfoTypeAffiliationState.actions;
