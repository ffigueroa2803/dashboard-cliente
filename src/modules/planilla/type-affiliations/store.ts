import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { ITypeAffiliationEntity } from "./dtos/type-affiliation.entity";

export interface TypeAffiliationState {
  typeAffiliations: ResponsePaginateDto<ITypeAffiliationEntity>;
  typeAffiliation: ITypeAffiliationEntity;
  option: string;
}

const initialState: TypeAffiliationState = {
  typeAffiliations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  typeAffiliation: {} as any,
  option: "",
};

const typeAffiliationStore = createSlice({
  name: "planilla@typeAffiliations",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<ITypeAffiliationEntity>>
    ) => {
      state.typeAffiliations = payload;
      return state;
    },
    updateItem: (state, { payload }: PayloadAction<ITypeAffiliationEntity>) => {
      state.typeAffiliations?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
      // response
      return state;
    },
    find: (state, { payload }: PayloadAction<ITypeAffiliationEntity>) => {
      state.typeAffiliation = payload;
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.typeAffiliation };
    },
  },
});

export const typeAffiliationReducer = typeAffiliationStore.reducer;

export const typeAffiliationActions = typeAffiliationStore.actions;
