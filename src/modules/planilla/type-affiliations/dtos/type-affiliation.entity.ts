import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";

export interface ITypeAffiliationEntity {
  id: number;
  name: string;
  amount: number;
  percent: number;
  isPercent: boolean;
  typeDiscountId: number;
  typeDiscount?: TypeDescuento;
}

export const typeAffiliationName = "TypeAffiliationEntity";
