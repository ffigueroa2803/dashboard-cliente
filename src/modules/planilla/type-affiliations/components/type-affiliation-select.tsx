import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useTypeAffiliation } from "../hooks/use-type-affiliation";
import { ITypeAffiliationEntity } from "../dtos/type-affiliation.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const TypeAffiliationSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { typeAffiliations } = useSelector(
    (state: RootState) => state.typeAffiliation
  );
  const typeAffiliation = useTypeAffiliation(defaultQuerySearch);

  const settingsData = (row: ITypeAffiliationEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    typeAffiliation.fetch();
  }, [typeAffiliation.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={typeAffiliations?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={typeAffiliation.setQuerySearch}
    />
  );
};
