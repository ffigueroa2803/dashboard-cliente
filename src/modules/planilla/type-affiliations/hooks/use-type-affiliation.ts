import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch } from "react-redux";
import { typeAffiliationActions } from "../store";

const request = planillaRequest();

export const useTypeAffiliation = (defaultQuerySearch?: string) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`typeAffiliations`, { params })
      .then((res) => dispatch(typeAffiliationActions.paginate(res.data)))
      .catch(() => dispatch(typeAffiliationActions.paginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    querySearch,
    setQuerySearch,
    fetch,
  };
};
