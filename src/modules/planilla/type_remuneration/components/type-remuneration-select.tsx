import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getTypeRemunerations } from "../apis";
import { TypeRemuneration } from "../dtos/type_remuneration.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: any;
  name: string;
  defaultQuerySearch?: string;
  isDisabled?: boolean;
}

export const TypeRemunerationSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
  isDisabled,
}: IProps) => {
  return (
    <SelectRemote
      isDisabled={isDisabled}
      defaultQuerySearch={defaultQuerySearch}
      handle={getTypeRemunerations}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: TypeRemuneration) =>
          `${row.code}.- ${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
