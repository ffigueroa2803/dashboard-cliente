/* eslint-disable no-unused-vars */
import Toggle from "@atlaskit/toggle";
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { AirhspSelect } from "@modules/planilla/airhsp/infrastructure/components/airhsp-select";
import { TypeRemuneration } from "@modules/planilla/type_remuneration/dtos/type_remuneration.entity";
import { InputDto } from "@services/dtos";
import { useState } from "react";
import { ArrowLeft, Save } from "react-feather";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { createTypeRemuneration } from "../apis";
import { ICreateTypeRemunerationDto } from "../dtos/create-type_remunerations.dto";

interface IProps {
  onClose: () => void;
  onSave: (typeremuneration: TypeRemuneration) => void;
}

export const defaultTypeRemuneration = {
  code: "",
  name: "",
  description: "",
  isBase: true,
  isBonification: false,
};

export const RemunerationCreate = ({ onClose, onSave }: IProps) => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ICreateTypeRemunerationDto>(
    defaultTypeRemuneration
  );

  const handleOnChange = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    setPending(true);
    await createTypeRemuneration(form as any)
      .then((data) => {
        setForm(defaultTypeRemuneration);
        toast.success(`Los datos se guardarón correctamente!`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`Ocurrió un error al guardar los datos`));
    setPending(false);
    onClose();
  };

  const ComponentCreateType_Remuneration = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            name="code"
            onChange={({ target }) =>
              handleOnChange({ name: target.name, value: target.value })
            }
            className="capitalize"
            value={form?.code}
          />
        </FormGroup>

        <FormGroup>
          <label>Nombre</label>
          <Input
            type="text"
            className="capitalize"
            onChange={({ target }) =>
              handleOnChange({ name: target.name, value: target.value })
            }
            name="name"
            value={form?.name}
          />
        </FormGroup>

        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            className="capitalize"
            onChange={({ target }) =>
              handleOnChange({ name: target.name, value: target.value })
            }
            name="description"
            value={form?.description}
          />
        </FormGroup>

        <FormGroup>
          <label>¿Aplica a la Base Imponible?</label>
          <div>
            <Toggle
              name="isBase"
              isChecked={form?.isBase || false}
              onChange={({ target }) =>
                handleOnChange({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>¿Es una Bonificación/Gratificación?</label>
          <div>
            <Toggle
              name="isBonification"
              isChecked={form?.isBonification || false}
              onChange={({ target }) =>
                handleOnChange({
                  name: target.name,
                  value: target.checked,
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup>
          <label>Código AIRHSP</label>
          <AirhspSelect
            name="airhspId"
            value={form?.airhspId || ""}
            query={{ page: 1, limit: 100, level: 1 }}
            onChange={(opt) =>
              handleOnChange({ name: opt.name, value: opt.value })
            }
          />
        </FormGroup>
        <hr />
      </div>
      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Show
            condition={!pending}
            isDefault={
              <ButtonLoading loading color="primary" title="Guardar datos" />
            }
          >
            <Button
              color="primary"
              title="Guardar datos"
              onClick={handleSave}
              disabled={pending}
            >
              <Save size={17} />
            </Button>
          </Show>
        </Col>
      </Row>
    </div>
  );

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Tip. Remuneración</ModalHeader>
      <ModalBody>{ComponentCreateType_Remuneration}</ModalBody>
    </Modal>
  );
};
