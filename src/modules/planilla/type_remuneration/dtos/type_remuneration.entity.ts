import { AirhspEntity } from "@modules/planilla/airhsp/domain/airhsp.entity";

export interface TypeRemuneration {
  id: number;
  code?: string;
  name?: string;
  description?: string;
  isBase?: boolean;
  isVisible?: boolean;
  isBonification?: boolean;
  isEdit?: boolean;
  airhspId?: number;
  state?: boolean;

  airhsp?: AirhspEntity;
}

export const typeRemunerationEntityName = "TypeRemuneration";
