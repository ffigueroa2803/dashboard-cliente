import React, { useState } from 'react'
import { Button, Col, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';
import { ICreateConfigApportationMaxDto } from './../dto/create-config_aportation_max.dto'
import { AportationMaxForm } from './aportation-max-form'
import { IInputHandle } from '@common/dtos/input-handle';
import { Save } from 'react-feather';
// import { typeForm, useAportationMax } from '../hooks/useAportationMax';
import { useCreate } from '../hooks/useCreate';

interface IProps {
  onClose: () => void
  onSave: (CreateConfigMax: ICreateConfigApportationMaxDto) => void
}

export const defaultConfigMax = {
  typeCategoryId: 0,
  typeAportationId: 0,
  uit: 0,
  percent: 0,
  year: 0,
}
export const AportationMaxCreate = ({ onClose, onSave }: IProps) => {

  // const [form, setForm] = useState<ICreateConfigApportationMaxDto>(defaultConfigMax)
  const { SaveAportationMax, configAportationMax, errors, handleOnChangeAportationMax, isCreate, pending } = useCreate()
  // const { configAportationMax, handleOnChangeAportationMax, saveAportationMax } = useAportationMax(typeForm.create)

  // const handleOnChange = ({ name, value }: IInputHandle) => {
  //   setForm(prev => ({
  //     ...prev,
  //     [name]: value
  //   }))
  // }

  // const handleSave = () => {

  // }

  return (
    <Modal isOpen={true} size='xl'>
      <ModalHeader toggle={onClose}>Editar Config. Máxima</ModalHeader>
      <ModalBody>
        <AportationMaxForm
          handleOnChange={handleOnChangeAportationMax}
          form={configAportationMax}
          save={SaveAportationMax}
        />
        {/* <ComponentConfigType_Aportacion />
        <ConfigPartPresu />
        <ListConfiguration /> */}

      </ModalBody>
    </Modal>
  )
}
