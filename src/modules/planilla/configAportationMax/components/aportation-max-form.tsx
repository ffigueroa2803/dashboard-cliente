import React from "react";
import { Col, Row, Button, FormGroup, Input } from "reactstrap";
import { IInputHandle } from "@common/dtos/input-handle";
import { ICreateConfigApportationMaxDto } from "../dto/create-config_aportation_max.dto";
import { Save, Settings } from "react-feather";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";

interface IProps {
  form: ICreateConfigApportationMaxDto;
  isEdit?: boolean;
  // eslint-disable-next-line no-unused-vars
  handleOnChange: (input: IInputHandle) => void;
  save: () => void;
}

export const AportationMaxForm = ({ form, handleOnChange, save }: IProps) => {
  return (
    <>
      <div className="mt-2">
        <div className="mb-3">
          <Row>
            <Col md="12">
              <p className="font-weight-bold">
                <Settings className="mr-2" size={13} />
                Configuracion por Categoría
              </p>
              <hr />
            </Col>
            <Col md="6">
              <FormGroup>
                <label>Categoria</label>
                <TypeCategorySelect
                  name="typeCategoryId"
                  value={form?.typeCategoryId || 0}
                  onChange={(e) =>
                    handleOnChange({ name: e.name, value: parseInt(e.value) })
                  }
                />
              </FormGroup>
            </Col>

            <Col md="6">
              <FormGroup>
                <label>Año</label>
                <Input
                  type="number"
                  name="year"
                  className="capitalize"
                  onChange={({ target }) =>
                    handleOnChange({
                      name: target.name,
                      value: parseInt(target.value),
                    })
                  }
                  value={form?.year || ""}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label>UIT</label>
                <Input
                  type="text"
                  className="capitalize"
                  name="uit"
                  onChange={({ target }) =>
                    handleOnChange({
                      name: target.name,
                      value: parseFloat(target.value),
                    })
                  }
                  value={form?.uit || ""}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label>Porcentaje (%)</label>
                <Input
                  type="text"
                  className="capitalize"
                  name="percent"
                  onChange={({ target }) =>
                    handleOnChange({
                      name: target.name,
                      value: parseFloat(target.value),
                    })
                  }
                  value={form?.percent || ""}
                />
              </FormGroup>
            </Col>
          </Row>
        </div>
      </div>
      <hr />
      <Row className="justify-content-center">
        <Col md="12 col-12 text-right">
          <Button color="primary" title="Guardar datos" onClick={save}>
            <Save size={17} />
          </Button>
        </Col>
      </Row>
    </>
  );
};
