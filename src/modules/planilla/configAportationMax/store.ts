import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { MetaDto } from '@services/dtos';
import { HYDRATE } from 'next-redux-wrapper';
import { EntityConfigMaxDto } from './dto/entity-config_aportation_max.dto'

export interface ConfigAportacionMax {
  config_max_aportations: {
    meta: MetaDto,
    items: EntityConfigMaxDto[]
  },
  config_max_aportation: EntityConfigMaxDto | any
}

const initialState: ConfigAportacionMax = {
  config_max_aportations: {
    items: [],
    meta: {
      itemsPerPage: 0,
      totalItems: 0,
      totalPages: 0
    }
  },
  config_max_aportation: {} as any
}

const ConfigAportationMax = createSlice({
  name: 'planilla@config_aportation_max',
  initialState,
  reducers: {
    paginate: (state, { payload }: PayloadAction<{ meta: MetaDto, items: EntityConfigMaxDto[] }>) => {
      state.config_max_aportations = payload
    },
    find: (state, { payload }: PayloadAction<EntityConfigMaxDto | null>) => {
      state.config_max_aportation = payload
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.config_max_aportation }
    }
  }
})
export const config_aportation_maxReducer = ConfigAportationMax.reducer

export const { find, paginate } = ConfigAportationMax.actions