export interface EntityConfigMaxDto {
  id: number;
  typeCategoryId: number;
  typeAportationId: number;
  uit: number;
  percent: number;
  year: number;
}
