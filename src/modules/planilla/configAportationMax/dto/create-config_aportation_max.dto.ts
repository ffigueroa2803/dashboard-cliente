export interface ICreateConfigApportationMaxDto {
  typeCategoryId: number,
  typeAportationId: number,
  uit: number,
  percent: number,
  year: number,
}