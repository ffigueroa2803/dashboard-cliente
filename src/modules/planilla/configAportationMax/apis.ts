import { planillaRequest } from "@services/planilla.request";
import { ICreateConfigApportationMaxDto } from "./dto/create-config_aportation_max.dto";
import { EntityConfigMaxDto } from "./dto/entity-config_aportation_max.dto";

const request = planillaRequest();

export const getAportationMax = async () => {};

export const findAportationMax = async (id: number) => {
  return await request
    .get(`configAportationMax/${id}`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createAportationMax = async (
  payload: ICreateConfigApportationMaxDto
): Promise<EntityConfigMaxDto> => {
  return await request
    .post(`configAportationMax`, payload)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const updateAportationMax = async (
  id: number,
  payload: ICreateConfigApportationMaxDto
) => {
  return await request
    .put(`configAportationMax/${id}`, payload)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
