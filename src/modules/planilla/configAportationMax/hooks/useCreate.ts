import { toast } from "react-toastify";
import { defaultConfigMax } from "./../components/aportation-max-create";
import { createAportationMax } from "./../apis";
import { InputDto } from "@services/dtos";
import { useState } from "react";
import { ICreateConfigApportationMaxDto } from "./../dto/create-config_aportation_max.dto";

const DefaultCreate = {
  typeCategoryId: 0,
  typeAportationId: 0,
  uit: 0,
  percent: 0,
  year: 0,
};

export const useCreate = () => {
  const [configAportationMax, SetConfigConfigMax] =
    useState<ICreateConfigApportationMaxDto>(DefaultCreate);
  const [pending, SetPending] = useState(false);
  const [errors, SetErrors] = useState({});
  const [isCreate, SetIsCreate] = useState(false);

  const handleOnChangeAportationMax = ({ name, value }: InputDto) => {
    SetConfigConfigMax((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const SaveAportationMax = async () => {
    SetPending(true);
    await createAportationMax(configAportationMax)
      .then(() => {
        SetConfigConfigMax(defaultConfigMax);
        toast.success("Los datos se guardaron correctamente");
        SetIsCreate(true);
      })
      .catch((err) => {
        SetErrors(err?.response?.data?.errors || {});
      });

    SetPending(false);
  };

  return {
    pending,
    errors,
    configAportationMax,
    SaveAportationMax,
    handleOnChangeAportationMax,
    isCreate,
  };
};
