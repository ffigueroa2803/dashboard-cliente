/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-unused-vars */
import { toast } from "react-toastify";
import { useState } from "react";
import { InputDto } from "@services/dtos";
import { ICreateConfigApportationMaxDto } from "@modules/planilla/configAportationMax/dto/create-config_aportation_max.dto";
import { EntityConfigMaxDto } from "@modules/planilla/configAportationMax/dto/entity-config_aportation_max.dto";
import { createAportationMax } from "./../apis";

export enum typeForm {
  create = "create",
  edit = "edit",
}

const DefaultConfigAportationMax = {
  typeCategoryId: 0,
  typeAportationId: 0,
  uit: 0,
  percent: 0,
  year: 0,
};

export const useAportationMax = () => {
  const option: any = {
    create: () => {
      // const { type_aportacion } = useSelector((state: RootState) => state.type_aportacion)
      const [configAportationMax, SetConfigAportationMax] =
        useState<ICreateConfigApportationMaxDto>(DefaultConfigAportationMax);

      const handleOnChangeAportationMax = ({ name, value }: InputDto) => {
        SetConfigAportationMax((prev) => ({
          ...prev,
          [name]: value,
        }));
      };

      const saveAportationMax = async () => {
        if (typeof configAportationMax == "undefined") return;
        await createAportationMax(configAportationMax)
          .then(() => {
            SetConfigAportationMax(DefaultConfigAportationMax);
            toast.success("Los datos se guardaron correctamente");
          })
          .catch(() => toast.error("Ocurrio un error al guardar los datos"));
      };

      return {
        configAportationMax,
        handleOnChangeAportationMax,
        saveAportationMax,
      };
    },
    edit: () => {
      const [configAportationMax, SetConfigAportationMax] =
        useState<EntityConfigMaxDto>(DefaultConfigAportationMax as any);

      const handleOnChangeAportationMax = ({ name, value }: InputDto) => {
        SetConfigAportationMax((prev) => ({
          ...prev,
          [name]: value,
        }));
      };

      return {
        configAportationMax,
        handleOnChangeAportationMax,
      };
    },
  };

  // return option[typeForm]()
};
