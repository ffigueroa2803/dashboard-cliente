import { toast } from "react-toastify";
import { updateAportationMax } from "../apis";
import { InputDto } from "@services/dtos";
import { useState } from "react";
import { ICreateConfigApportationMaxDto } from "../dto/create-config_aportation_max.dto";

const DefaultCreate = {
  typeCategoryId: 0,
  typeAportationId: 0,
  uit: 0,
  percent: 0,
  year: 0,
};

export const useEdit = (id: number) => {
  const [form, setForm] =
    useState<ICreateConfigApportationMaxDto>(DefaultCreate);
  const [pending, setPending] = useState(false);
  const [errors, SetErrors] = useState({});
  const [isCreate, SetIsCreate] = useState(false);

  const handleForm = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const execute = async () => {
    setPending(true);
    await updateAportationMax(id, form)
      .then(() => {
        toast.success("Los datos se guardaron correctamente");
        SetIsCreate(true);
      })
      .catch((err) => {
        toast.error("No se pudo actualizar los datos");
        SetErrors(err?.response?.data?.errors || {});
      });
    setPending(false);
  };

  return {
    pending,
    errors,
    form,
    execute,
    handleForm,
    setForm,
    isCreate,
  };
};
