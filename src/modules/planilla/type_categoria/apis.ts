import { scaleRequest } from "@services/scale.request";
import { PaginateDto } from "@services/dtos";

const request = scaleRequest();

export const getTypeCategorias = async ({
  page,
  limit,
  querySearch,
}: PaginateDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  return await request
    .get(`typeCategories`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
