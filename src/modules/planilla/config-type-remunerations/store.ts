import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IConfigTypeRemunerationEntity } from "./domain/config-type-remuneration.entity";

export interface ConfigTypeRemunerationState {
  configTypeRemunerations: ResponsePaginateDto<IConfigTypeRemunerationEntity>;
  configTypeRemuneration: IConfigTypeRemunerationEntity;
}

export const initialState: ConfigTypeRemunerationState = {
  configTypeRemunerations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  configTypeRemuneration: {} as any,
};

const configTypeRemunerationStore = createSlice({
  name: "planilla@configTypeRemunerations",
  initialState,
  reducers: {
    paginate: (
      state,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IConfigTypeRemunerationEntity>>
    ) => {
      state.configTypeRemunerations = payload;
    },
    setConfigTypeRemuneration: (
      state,
      { payload }: PayloadAction<IConfigTypeRemunerationEntity>
    ) => {
      state.configTypeRemuneration = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.configTypeRemuneration };
    },
  },
});

export const configTypeRemunerationReducer =
  configTypeRemunerationStore.reducer;

export const configTypeRemunerationActions =
  configTypeRemunerationStore.actions;
