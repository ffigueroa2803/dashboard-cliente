import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { IConfigTypeRemunerationEntity } from "../domain/config-type-remuneration.entity";

export interface IConfigTypeRemunerationTableProvider {
  data: ResponsePaginateDto<IConfigTypeRemunerationEntity>;
  fetch(): Promise<void>;
  pending: boolean;
  isError: boolean;
}
