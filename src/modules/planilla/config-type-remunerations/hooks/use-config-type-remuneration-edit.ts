/* eslint-disable no-async-promise-executor */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

const request = planillaRequest();

const defaultData = {
  amount: 0,
};

export const useConfigTypeRemunerationEdit = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ConfigTypeRemunerationRequest>(defaultData);

  const { configTypeRemuneration } = useSelector(
    (state: RootState) => state.configTypeRemuneration
  );

  const onChange = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({ ...prev, [name]: value }));
  };

  const clearForm = () => setForm(defaultData);

  const canExecute = () => !pending;

  const execute = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .put(`configTypeRemunerations/${configTypeRemuneration.id}`, {
          amount: parseFloat(`${form.amount}`),
        })
        .then((res) => resolve(res.data))
        .catch((err) => {
          const data = err?.response?.data;
          alerStatus(data?.status || 501, data?.message);
          reject(err);
        });
      setPending(false);
    });
  };

  useEffect(() => {
    setForm(defaultData);
  }, []);

  return {
    form,
    pending,
    setForm,
    canExecute,
    execute,
    onChange,
    clearForm,
  };
};

export interface ConfigTypeRemunerationRequest {
  amount: number;
}
