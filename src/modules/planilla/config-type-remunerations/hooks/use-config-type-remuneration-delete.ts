/* eslint-disable no-async-promise-executor */
import { useConfirm } from "@common/confirm/use-confirm";
import { alerStatus } from "@common/errors/utils/alert-status";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useConfigTypeRemunerationDelete = () => {
  const { configTypeRemuneration } = useSelector(
    (state: RootState) => state.configTypeRemuneration
  );

  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "¿Estás seguro en eliminar?",
          message: "Eliminar configuración remunerativa",
          labelError: "Cancelar",
          labelSuccess: "Elimnar",
        })
        .then(async () => {
          setPending(true);
          await request
            .destroy(`configTypeRemunerations/${configTypeRemuneration.id}`)
            .then(({ data }) => {
              toast.success(`Los datos se guardarón correctamente!`);
              resolve(data);
            })
            .catch((err) => {
              const data = err?.response?.data;
              alerStatus(data?.status || 501, data?.message);
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return { pending, execute };
};
