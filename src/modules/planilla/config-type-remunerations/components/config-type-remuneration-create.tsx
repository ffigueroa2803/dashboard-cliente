import React from "react";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";
import { RootState } from "@store/store";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  Col,
  Row,
  FormGroup,
  Input,
  Form,
} from "reactstrap";
import { useConfigTypeRemunerationCreate } from "../hooks/use-config-type-remuneration-create";
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";

interface Iprops {
  onSave: () => void;
  open: boolean;
  setOpen: () => void;
}

export const ConfigTypeRemunerationCreate = ({
  open,
  onSave,
  setOpen,
}: Iprops) => {
  const { planilla } = useSelector((state: RootState) => state.planilla);
  const { dark } = useSelector((state: RootState) => state.screen);
  const { form, onChange, execute, pending, canExecute, clearForm } =
    useConfigTypeRemunerationCreate();

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await execute()
      .then(() => onSave())
      .then(() => clearForm())
      .catch(() => null);
  };

  return (
    <Modal isOpen={open} style={{ marginTop: "6rem" }}>
      <ModalHeader toggle={setOpen}>
        Crear Config. Tip. Remuneración
      </ModalHeader>
      <ModalBody>
        <Form onSubmit={handleSubmit}>
          <FormGroup className="mb-4">
            <label>Planilla</label>
            <h6>{planilla?.name || "N/A"}</h6>
          </FormGroup>

          <FormGroup>
            <label>
              Tip. Remuneración <b className="text-danger">*</b>
            </label>
            <TypeRemunerationSelect
              name="typeRemunerationId"
              onChange={(obj) => onChange(obj)}
              value={form?.typeRemunerationId || ""}
            />
          </FormGroup>

          <FormGroup>
            <label>
              Monto <b className="text-danger">*</b>
            </label>
            <Input
              name="amount"
              type="number"
              style={{
                opacity: "1",
                backgroundColor: dark ? "" : "#FFF",
              }}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.value })
              }
              placeholder="Ingrese el monto"
              value={form?.amount || ""}
            />
          </FormGroup>

          <Row className="justify-content-center">
            <Col md="6 col-6 text-left"></Col>
            <Col md="6 col-6 text-right">
              <Show
                condition={!pending}
                isDefault={
                  <ButtonLoading
                    title="guardando..."
                    loading={true}
                    color="primary"
                  />
                }
              >
                <Button
                  color="primary"
                  title="Guardar datos"
                  disabled={!canExecute()}
                >
                  <Save className="icon" />
                </Button>
              </Show>
            </Col>
          </Row>
        </Form>
      </ModalBody>
    </Modal>
  );
};
