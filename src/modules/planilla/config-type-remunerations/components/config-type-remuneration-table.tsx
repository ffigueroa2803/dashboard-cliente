/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IConfigTypeRemunerationTableProvider } from "../interfaces/config-type-remuneration-table.interface";
import Skeleton from "react-loading-skeleton";
import { Col, Row } from "reactstrap";
import { Show } from "@common/show";
import { ConfigTypeRemunerationItem } from "./config-type-remuneration-item";

interface IProps {
  provider: IConfigTypeRemunerationTableProvider;
}

const LoadingComponent = () => (
  <>
    <Col md="6">
      <Skeleton height={30} />
    </Col>
    <Col md="4">
      <Skeleton height={30} />
    </Col>
    <Col md="2">
      <Skeleton height={30} />
    </Col>
  </>
);

const ConfigTypeRemunerationTable = ({ provider }: IProps) => {
  const { planilla } = useSelector((state: RootState) => state.planilla);

  const { data, fetch, pending } = provider;

  const handleEdit = () => fetch();

  const handleDelete = () => fetch();

  useEffect(() => {
    if (planilla?.id) fetch();
  }, [planilla]);

  return (
    <>
      {/* cargar */}
      <Show condition={pending}>
        <Row>
          <LoadingComponent />
          <LoadingComponent />
          <LoadingComponent />
        </Row>
      </Show>
      {/* list */}
      <div className="table-responsive">
        <table className="table">
          <tbody>
            {data.items?.map((item) => (
              <ConfigTypeRemunerationItem
                onEdit={handleEdit}
                onDelete={handleDelete}
                configTypeRemuneration={item}
                key={`configTypeRemuneration-config-type-remuneration-${item.id}`}
              />
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ConfigTypeRemunerationTable;
