/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { format } from "currency-formatter";
import { useEffect, useState } from "react";
import { Edit, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IConfigTypeRemunerationEntity } from "../domain/config-type-remuneration.entity";
import { useConfigTypeRemunerationDelete } from "../hooks/use-config-type-remuneration-delete";
import { configTypeRemunerationActions } from "../store";
import { ConfigTypeRemunerationEdit } from "./config-type-remuneration-edit";

interface IProps {
  configTypeRemuneration: IConfigTypeRemunerationEntity;
  onEdit?(): void;
  onDelete?(): void;
}

export const ConfigTypeRemunerationItem = ({
  configTypeRemuneration,
  onEdit,
  onDelete,
}: IProps) => {
  const dispatch = useDispatch();

  const [event, setEvent] = useState<string>("");
  const [openEdit, setOpenEdit] = useState<boolean>(false);
  const { dark } = useSelector((state: RootState) => state.screen);

  const configTypeRemunerationDelete = useConfigTypeRemunerationDelete();

  const dispatchItem = (e: any, event: string) => {
    e.preventDefault();
    setEvent(event);
    dispatch(
      configTypeRemunerationActions.setConfigTypeRemuneration(
        configTypeRemuneration
      )
    );
  };

  const handleEdit = () => setOpenEdit(true);

  const handleDelete = () => {
    configTypeRemunerationDelete
      .execute()
      .then(() => (typeof onDelete == "function" ? onDelete() : null))
      .catch(() => null);
  };

  const handleUpdate = () => {
    if (typeof onEdit === "function") onEdit();
  };

  useEffect(() => {
    if (event === "edit") handleEdit();
    if (event === "delete") handleDelete();
    return () => setEvent("");
  }, [event]);

  return (
    <>
      <tr>
        <td>
          <h6 className={dark ? "text-white" : ""}>
            {configTypeRemuneration?.typeRemuneration?.code}
          </h6>
          <p className={dark ? "text-white" : ""}>
            {configTypeRemuneration?.typeRemuneration?.name}
          </p>
        </td>
        <td>
          <h6
            className="badge badge-warning badge-lg"
            style={{ fontSize: "13px" }}
          >
            {format(configTypeRemuneration.amount, { code: "PEN" })}
          </h6>
        </td>
        <td width={50}>
          <a className="me-2" href="#" onClick={(e) => dispatchItem(e, "edit")}>
            <Edit className="icon" />
          </a>
        </td>
        <td width={50}>
          <a
            className="me-2"
            href="#"
            onClick={(e) => dispatchItem(e, "delete")}
          >
            <Trash className="icon" />
          </a>
        </td>
      </tr>
      {/* edit */}
      <Show condition={openEdit}>
        <ConfigTypeRemunerationEdit
          onClose={() => setOpenEdit(false)}
          open={true}
          onSave={handleUpdate}
        />
      </Show>
    </>
  );
};
