import { IPlanillaEntity } from "@modules/planilla/planillas/dtos/planilla.entity";
import { TypeRemuneration } from "@modules/planilla/type_remuneration/dtos/type_remuneration.entity";

export interface IConfigTypeRemunerationEntity {
  id: number;
  amount: number;
  typeRemuneration?: TypeRemuneration;
  planilla?: IPlanillaEntity;
}
