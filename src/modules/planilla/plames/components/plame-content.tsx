import { FloatButton } from "@common/button/float-button";
import { IRowSelected } from "@common/dtos/row-selected";
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { CronogramaTable } from "@modules/planilla/cronogramas/components/cronograma-table";
import {
  cronogramaEntityName,
  ICronogramaEntity,
} from "@modules/planilla/cronogramas/dtos/cronograma.entity";
import { RootState } from "@store/store";
import { Collection } from "collect.js";
import { useContext } from "react";
import { File } from "react-feather";
import { useSelector } from "react-redux";
import { PlameContext } from "./plame-context";
import { PlameReport } from "./plame-report";

interface IProps {
  onChangePage: any;
  onChangeRowsPerPage: any;
  onQuerySearch: any;
}

export const PlameContent = ({
  onChangePage,
  onChangeRowsPerPage,
  onQuerySearch,
}: IProps) => {
  const ability = useContext(CaslContext);
  const plameContext = useContext(PlameContext);

  const { cronogramas } = useSelector((state: RootState) => state.cronograma);

  const handleOnSelected = (selected: IRowSelected<ICronogramaEntity>) => {
    const ids: number[] = new Collection(selected.selectedRows)
      .pluck("id")
      .toArray();
    // add ids
    plameContext.setIds(ids);
    // clear select
    if (ids.length == 0) plameContext.setToggleCleared(true);
  };

  return (
    <>
      <div className="card card-body">
        <CronogramaTable
          selectableRows={true}
          clearSelectedRows={false}
          isOption={false}
          defaultYear={plameContext.year}
          defaultMonth={plameContext.month}
          defaultPrincipal={plameContext.principal}
          loading={plameContext.pending}
          data={cronogramas?.items || []}
          perPage={cronogramas?.meta?.itemsPerPage || 0}
          totalItems={cronogramas?.meta?.totalItems || 30}
          onChangePage={onChangePage}
          onChangeRowsPerPage={onChangeRowsPerPage}
          onQuerySearch={onQuerySearch}
          onSelectedRowsChange={handleOnSelected}
        />
      </div>
      {/* btn report */}
      <Show
        condition={
          plameContext.ids.length > 0 &&
          ability.can(PermissionAction.REPORT, cronogramaEntityName)
        }>
        <FloatButton
          icon={<File />}
          color="danger"
          onClick={() => plameContext.setIsReport(true)}
        />
        <PlameReport
          isOpen={plameContext.isReport}
          onClose={() => plameContext.setIsReport(false)}
        />
      </Show>
    </>
  );
};
