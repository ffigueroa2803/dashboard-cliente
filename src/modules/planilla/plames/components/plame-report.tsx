import { Show } from "@common/show";
import React, { useContext, useState } from "react";
import { File } from "react-feather";
import { planillaRequest } from "@services/planilla.request";
import {
  Button,
  Form,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import urljoin from "url-join";
import { SelectBasic } from "@common/select/select-basic";
import { PlameContext } from "./plame-context";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

const request = planillaRequest();

export const PlameReport = ({ isOpen, onClose }: IProps) => {
  const plameContext = useContext(PlameContext);

  const [typeReport, setTypeReport] = useState<string>("");

  const handleClick = () => {
    const urls: any = {
      "PERSONAL-PDF": "personal.pdf",
      "PERSONAL-EXCEL": "personal.xlsx",
      JOR: "jornary.jor",
      REM: "resume.rem",
      PEN: "resume.pen",
    };

    const params: string[] = [];
    plameContext.ids.forEach((id, index) =>
      params.push(`cronogramaIds[${index}]=${id}`)
    );

    const url = urljoin(
      request.urlBase,
      `/plames/reports/${plameContext.year}/${plameContext.month}/${
        urls[typeReport]
      }?${params.join("&")}`
    );
    const a = document.createElement("a");
    a.target = "__blank";
    a.href = url;
    a.click();
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Reporte PDT-PLAME</ModalHeader>
      <ModalBody>
        <Form>
          <FormGroup>
            <label>Tipo de Reporte</label>
            <SelectBasic
              options={[
                { label: "Personal PDF", value: "PERSONAL-PDF" },
                { label: "Personal Excel", value: "PERSONAL-EXCEL" },
                { label: "JOR", value: "JOR" },
                { label: "REM", value: "REM" },
                { label: "PEN", value: "PEN" },
              ]}
              value={typeReport}
              name="typeReport"
              onChange={(obj) => setTypeReport(obj.value)}
            />
          </FormGroup>
        </Form>
      </ModalBody>
      <Show condition={typeReport != ""}>
        <ModalFooter className="text-right">
          <Button color="danger" onClick={handleClick}>
            <File className="icon" /> Ver
          </Button>
        </ModalFooter>
      </Show>
    </Modal>
  );
};
