/* eslint-disable no-unused-vars */
import { DateTime } from "luxon";
import { createContext, FC, useEffect, useState } from "react";

export const PlameContext = createContext({
  year: 0,
  setYear: (value: number) => {},
  month: 0,
  setMonth: (value: number) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  principal: true,
  setPrincipal: (value: boolean) => {},
  pending: false,
  setPending: (value: boolean) => {},
  toggleCleared: false,
  setToggleCleared: (value: boolean) => {},
  isReport: false,
  setIsReport: (value: boolean) => {},
});

const currentDate = DateTime.now();

export const PlameProvider: FC = ({ children }) => {
  const [year, setYear] = useState<number>(currentDate.year);
  const [month, setMonth] = useState<number>(currentDate.month);
  const [principal, setPrincipal] = useState<boolean>(true);
  const [ids, setIds] = useState<number[]>([]);
  const [pending, setPending] = useState<boolean>(false);
  const [toggleCleared, setToggleCleared] = useState<boolean>(false);
  const [isReport, setIsReport] = useState<boolean>(false);

  useEffect(() => {
    if (toggleCleared) setToggleCleared(false);
  }, [toggleCleared]);

  return (
    <PlameContext.Provider
      value={{
        year,
        setYear,
        month,
        setMonth,
        ids,
        setIds,
        principal,
        setPrincipal,
        pending,
        setPending,
        toggleCleared,
        setToggleCleared,
        isReport,
        setIsReport,
      }}>
      {children}
    </PlameContext.Provider>
  );
};
