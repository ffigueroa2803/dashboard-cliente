export interface IListYearWork {
  year: number;
  workId: number;
  historialCounts: string;
}
