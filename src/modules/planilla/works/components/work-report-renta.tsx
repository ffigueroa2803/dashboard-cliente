import { Show } from "@common/show";
import React, { useState } from "react";
import { File } from "react-feather";
import { planillaRequest } from "@services/planilla.request";
import {
  Button,
  Form,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { WorkListYearSelect } from "./work-list-year-select";
import urljoin from "url-join";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

const request = planillaRequest();

export const WorkReportRenta = ({ isOpen, onClose }: IProps) => {
  const { work } = useSelector((state: RootState) => state.work);

  const [year, setYear] = useState<number>(0);

  const handleClick = (report: string) => {
    setYear(year);
    if (!year) return;
    const url = urljoin(
      request.urlBase,
      `/works/${work?.id}/${report}/${year}`
    );
    const a = document.createElement("a");
    a.target = "__blank";
    a.href = url;
    a.click();
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Reporte Renta</ModalHeader>
      <ModalBody>
        <Form>
          <FormGroup>
            <label>Pagos</label>
            <WorkListYearSelect
              name="year"
              value={year}
              onChange={(obj) => setYear(obj.value)}
            />
          </FormGroup>
        </Form>
      </ModalBody>
      <Show condition={year > 0}>
        <ModalFooter className="text-right">
          <Button color="danger" onClick={() => handleClick("historyResumen")}>
            <File className="icon" /> Resumen
          </Button>
          <Button color="danger" onClick={() => handleClick("historyRenta")}>
            <File className="icon" /> Renta
          </Button>
        </ModalFooter>
      </Show>
    </Modal>
  );
};
