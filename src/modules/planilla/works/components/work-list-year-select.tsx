import React, { useEffect } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useWorkListYear } from "../hooks/use-work-list-year";
import { IListYearWork } from "../dtos/list-year-work.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const WorkListYearSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { work, listYears } = useSelector((state: RootState) => state.work);

  const workListYear = useWorkListYear(defaultQuerySearch);

  const settingsData = (row: IListYearWork) => {
    return {
      label: `${row.year} [${row.historialCounts} pagos]`.toLowerCase(),
      value: row.year,
      onj: row,
    };
  };

  useEffect(() => {
    if (work?.id) workListYear.fetch();
  }, [work, workListYear.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={listYears?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={workListYear.setQuerySearch}
    />
  );
};
