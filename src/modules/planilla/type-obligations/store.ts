import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { ITypeObligationEntity } from "./dtos/type-obligation.entity";

export interface TypeObligationState {
  typeObligations: ResponsePaginateDto<ITypeObligationEntity>;
  typeObligation: ITypeObligationEntity;
  option: string;
}

const initialState: TypeObligationState = {
  typeObligations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  typeObligation: {} as any,
  option: "",
};

const TypeObligationState = createSlice({
  name: "planilla@typeObligations",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<ITypeObligationEntity>>
    ) => {
      state.typeObligations = payload;
    },
    find: (state, { payload }: PayloadAction<ITypeObligationEntity>) => {
      state.typeObligation = payload;
    },
    updateItem: (state, { payload }: PayloadAction<ITypeObligationEntity>) => {
      state.typeObligations?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }
        return item;
      });
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.infoTypeAffiliation };
    },
  },
});

export const typeObligationReducer = TypeObligationState.reducer;

export const typeObligationActions = TypeObligationState.actions;
