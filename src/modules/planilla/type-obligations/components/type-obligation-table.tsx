import React, { useCallback, useEffect, useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { ITypeObligationEntity } from "../dtos/type-obligation.entity";
import { Search, Edit, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import currencyFormatter from "currency-formatter";
import { useTypeObligationDelete } from "../hooks/use-type-obligation-delete";
import { useDispatch, useSelector } from "react-redux";
import { typeObligationActions } from "../store";
import { RootState } from "@store/store";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (querySearch: string | string[]) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: ITypeObligationEntity) => any;
}

interface IProps {
  data: ITypeObligationEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: (typeObligation: ITypeObligationEntity) => void;
  onDelete?: (typeObligation: ITypeObligationEntity) => void;
}

export const TypeObligationTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onDelete,
  options,
}: IProps) => {
  const dispatch = useDispatch();

  const { typeObligation } = useSelector(
    (state: RootState) => state.typeObligation
  );

  const [isDelete, setIsDelete] = useState<boolean>(false);
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const typeObligationDelete = useTypeObligationDelete();

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: ITypeObligationEntity) => row.id,
      },
      {
        name: "Código",
        grow: true,
        center: true,
        selector: (row: ITypeObligationEntity) => (
          <span className="badge badge-dark">
            {row?.typeDiscount?.code || "N/A"}
          </span>
        ),
      },
      {
        name: "Apellidos y Nombres",
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return (
            <span className="capitalize">{row?.person?.fullName || "N/A"}</span>
          );
        },
      },
      {
        name: "Tipo Documento",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.documentType?.name || "N/A"}</span>;
        },
      },
      {
        name: "N° Documento",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.documentNumber || "N/A"}</span>;
        },
      },
      {
        name: "Banco",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.bank?.name || "N/A"}</span>;
        },
      },
      {
        name: "Medio de Pago",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.isCheck ? "En Cheque" : "En Cuenta"}</span>;
        },
      },
      {
        name: "N° Cuenta",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.numberOfAccount || "N/A"}</span>;
        },
      },
      {
        name: "Fecha de Fin",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.terminationDate || "N/A"}</span>;
        },
      },
      {
        name: "Porcentaje",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return <span>{row?.isPercent ? "SI" : "NO"}</span>;
        },
      },
      {
        name: "Monto/Porcentaje",
        right: true,
        selector: (row: ITypeObligationEntity) => (
          <Show
            condition={!row.isPercent}
            isDefault={
              <Show
                condition={parseFloat(`${row.percent}`) > 0}
                isDefault={<span>{row.amount}%</span>}
              >
                <b className="text-danger">{row.percent}%</b>
              </Show>
            }
          >
            <Show
              condition={parseFloat(`${row.amount}`) > 0}
              isDefault={
                <span>
                  {currencyFormatter.format(row.amount, { code: "PEN" })}
                </span>
              }
            >
              <b className="text-danger">
                {currencyFormatter.format(row.amount, { code: "PEN" })}
              </b>
            </Show>
          </Show>
        ),
      },
      {
        name: "Estado",
        center: true,
        wrap: true,
        selector: (row: ITypeObligationEntity) => {
          return (
            <span className={`badge badge-${row.state ? "success" : "danger"}`}>
              {row?.state ? "Activo" : "Inactivo"}
            </span>
          );
        },
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: ITypeObligationEntity) => (
          <>
            <Edit
              className="cursor-pointer icon"
              onClick={() => handleClick(row)}
            />
            <Trash
              className="cursor-pointer icon"
              onClick={() => handleSelectDelete(row)}
            />
          </>
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (typeObligation: ITypeObligationEntity) => {
    if (typeof onClick == "function") onClick(typeObligation);
  };

  const handleSelectDelete = (typeObligation: ITypeObligationEntity) => {
    dispatch(typeObligationActions.find(typeObligation));
    setIsDelete(true);
  };

  const handleDelete = () => {
    typeObligationDelete
      .destroy()
      .then(() => {
        if (typeof onDelete == "function") {
          onDelete(typeObligation);
        }
      })
      .catch(() => null);
  };

  useEffect(() => {
    if (isDelete) handleDelete();
  }, [isDelete]);

  useEffect(() => {
    if (isDelete) setIsDelete(false);
  }, [isDelete]);

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
