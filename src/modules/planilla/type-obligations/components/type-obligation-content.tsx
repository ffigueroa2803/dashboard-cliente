import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { Plus, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { ITypeObligationEntity } from "../dtos/type-obligation.entity";
import { useTypeObligationsToInfo } from "../hooks/use-type-obligations-to-info";
import { typeObligationActions } from "../store";
import { TypeObligationEdit } from "./type-obligation-edit";
import { TypeObligationStepCreate } from "./type-obligation-step-create";
import { TypeObligationTable } from "./type-obligation-table";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onSave?: (typeObligation: ITypeObligationEntity) => void;
  onCreated?: (typeObligation: ITypeObligationEntity) => void;
}

export const TypeObligationContent = ({
  isOpen,
  onClose,
  onSave,
  onCreated,
}: IProps) => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const { typeObligations, option } = useSelector(
    (state: RootState) => state.typeObligation
  );

  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const { setPage, setLimit, fetch } = useTypeObligationsToInfo();

  const handlePage = (newPage: number) => {
    setPage(newPage);
    setIsRefresh(true);
  };

  const handleRowPer = (newRowPer: number, currentPage: number) => {
    setLimit(newRowPer);
    setPage(currentPage);
    setIsRefresh(true);
  };

  const handleCreated = (createdTypeObligation: ITypeObligationEntity) => {
    dispatch(typeObligationActions.changeOption("REFRESH"));
    // events
    if (typeof onCreated == "function") {
      onCreated(createdTypeObligation);
    }
  };

  const handleEdit = (typeObligation: ITypeObligationEntity) => {
    dispatch(typeObligationActions.find(typeObligation));
    dispatch(typeObligationActions.changeOption("EDIT"));
  };

  const handleDeleted = () => {
    dispatch(typeObligationActions.changeOption("REFRESH"));
  };

  const handleUpdated = (updatedTypeObligation: ITypeObligationEntity) => {
    dispatch(typeObligationActions.updateItem(updatedTypeObligation));
    dispatch(typeObligationActions.changeOption("REFRESH"));
    // events
    if (typeof onSave == "function") {
      onSave(updatedTypeObligation);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setPage(1);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && info?.id) setIsRefresh(true);
  }, [info, isOpen]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(typeObligationActions.changeOption(""));
    }
  }, [option]);

  useEffect(() => {
    if (isRefresh) fetch();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        style={{ minWidth: "90vh" }}
        backdrop={!["CREATE", "EDIT"].includes(option)}
      >
        <ModalHeader toggle={onClose}>
          <Settings className="icon" /> Configuración Obligaciones
        </ModalHeader>
        <ModalBody>
          <TypeObligationTable
            perPage={typeObligations?.meta?.itemsPerPage}
            totalItems={typeObligations?.meta?.totalItems}
            data={typeObligations?.items || []}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleRowPer}
            onDelete={handleDeleted}
            onClick={handleEdit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            outline
            onClick={() =>
              dispatch(typeObligationActions.changeOption("CREATE"))
            }
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* create */}
      <TypeObligationStepCreate
        isOpen={option == "CREATE"}
        onClose={() => dispatch(typeObligationActions.changeOption(""))}
        onSave={handleCreated}
      />
      {/* edit */}
      <TypeObligationEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(typeObligationActions.changeOption(""))}
        onSave={handleUpdated}
      />
    </>
  );
};
