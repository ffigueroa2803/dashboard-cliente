/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IFormTypeObligationDto } from "../dtos/form-type-obligation.dto";
import Toggle from "@atlaskit/toggle";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";
import { SelectBasic } from "@common/select/select-basic";
import { BankSelect } from "@modules/auth/banks/components/bank-select";
import { DocumentTypeSelect } from "@modules/auth/document-types/components/document-type-select";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const TypeObligationForm = ({
  form,
  isEdit,
  onChange,
}: IProps<IFormTypeObligationDto>) => {
  const { typeObligation } = useSelector(
    (state: RootState) => state.typeObligation
  );

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FormGroup className="mb-3">
          <label>
            Tipo Descuento <b className="text-danger">*</b>
          </label>
          <Show
            condition={!isEdit}
            isDefault={
              <h6 className="capitalize">
                {typeObligation?.typeDiscount?.description || "N/A"}
                <hr />
              </h6>
            }
          >
            <TypeDiscountSelect
              name="typeDiscountId"
              value={form?.typeDiscountId}
              onChange={onChange}
            />
          </Show>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Fecha de Fin</label>
          <div>
            <Input
              type="date"
              name="terminationDate"
              value={form?.terminationDate}
              onChange={({ target }) => onChange(target)}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Tipo Documento <b className="text-danger">*</b>
          </label>
          <DocumentTypeSelect
            name="documentTypeId"
            defaultQuerySearch={form?.documentTypeId}
            value={form?.documentTypeId || ""}
            onChange={onChange}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            N° Documento <b className="text-danger">*</b>
          </label>
          <Input
            type="text"
            name="documentNumber"
            value={form?.documentNumber}
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Banco <b className="text-danger">*</b>
          </label>
          <BankSelect name="bankId" value={form?.bankId} onChange={onChange} />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>N° Cuenta</label>
          <Input
            type="text"
            name="numberOfAccount"
            value={form?.numberOfAccount}
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Porcentaje</label>
          <div>
            <Toggle
              name="isPercent"
              isChecked={form.isPercent}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        {/* isBonification and Modo*/}
        <Show condition={form.isPercent}>
          <FormGroup className="mb-3">
            <label>
              Modo Descuento <b className="text-danger">*</b>
            </label>
            <SelectBasic
              options={[
                { label: "Bruto", value: "BRUTO" },
                { label: "Neto", value: "NETO" },
              ]}
              name="mode"
              value={form?.mode}
              onChange={onChange}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>Bonificación</label>
            <div>
              <Toggle
                name="isBonification"
                isChecked={form.isBonification}
                onChange={({ target }) =>
                  onChange({ name: target.name, value: target.checked })
                }
              />
            </div>
          </FormGroup>
        </Show>

        {/* monto */}
        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({
                  name: target.name,
                  value: parseFloat(target.value),
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Observación</label>
          <div>
            <Input
              type="textarea"
              name="observation"
              value={form?.observation}
              onChange={({ target }) => onChange(target)}
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
