import React, { useEffect, useState } from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Button,
  FormGroup,
  Input,
} from "reactstrap";
import { TypeObligationForm } from "./type-obligation-form";
import { InputDto } from "@services/dtos";
import { ICreateTypeObligationDto } from "../dtos/create-type-obligation.dto";
import { ProgressIndicator } from "@atlaskit/progress-indicator";
import { Save, ArrowLeft, User } from "react-feather";
import { PersonSearchSelect } from "@modules/auth/person/components/person-search-select";
import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { toast } from "react-toastify";
import { ITypeObligationEntity } from "../dtos/type-obligation.entity";
import { useSelector } from "react-redux";
import { useTypeObligationCreate } from "../hooks/use-type-obligation-create";
import { RootState } from "@store/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (typeObligation: ITypeObligationEntity) => void;
}

export const TypeObligationStepCreate = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const { info } = useSelector((state: RootState) => state.info);

  const typeObligationCreate = useTypeObligationCreate();

  const [steps] = useState(["person", "type-obligation"]);
  const [currentStep, setCurrentStep] = useState(0);
  const [person, setPerson] = useState<PersonEntity | undefined>(undefined);

  const handleAdd = (person: PersonEntity) => {
    setCurrentStep(1);
    setPerson(person);
    typeObligationCreate.changeForm({ name: "personId", value: person.id });
  };

  const handleSave = async () => {
    await typeObligationCreate
      .save()
      .then((data) => {
        typeObligationCreate.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => null);
  };

  const formDefault = () => {
    typeObligationCreate.changeForm({ name: "infoId", value: info.id });
    setCurrentStep(0);
  };

  useEffect(() => {
    if (isOpen) formDefault();
  }, [isOpen]);

  const ComponentCreateTypeObligation = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>Apellidos y Nombres</label>
          <Input
            type="text"
            className="capitalize"
            readOnly
            value={`${person?.fullName || ""}`.toLowerCase()}
          />
        </FormGroup>

        <FormGroup>
          <label>Tipo Documento</label>
          <Input
            type="text"
            className="uppercase"
            readOnly
            value={`${person?.documentType?.name || ""}`.toLowerCase()}
          />
        </FormGroup>

        <FormGroup>
          <label>N° Documento</label>
          <Input
            type="text"
            className="capitalize"
            readOnly
            value={person?.documentNumber}
          />
        </FormGroup>
        <hr />
      </div>
      <TypeObligationForm
        form={typeObligationCreate.form}
        onChange={typeObligationCreate.changeForm}
      />
      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => setCurrentStep(0)}
            disabled={typeObligationCreate.pending}
          >
            <ArrowLeft size={17} />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={typeObligationCreate.pending}
          >
            <Save size={17} />
          </Button>
        </Col>
      </Row>
    </div>
  );

  const ComponentSearch = (
    <div className="mt-5">
      <label>
        <User size={15} /> Validar Persona
      </label>
      <PersonSearchSelect onAdd={handleAdd} />
    </div>
  );

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Configuración Obligación</ModalHeader>
      <ModalBody>
        <ProgressIndicator
          selectedIndex={currentStep}
          values={steps}
          appearance={"primary"}
        />
        {currentStep == 0 ? ComponentSearch : null}
        {currentStep == 1 ? ComponentCreateTypeObligation : null}
      </ModalBody>
    </Modal>
  );
};
