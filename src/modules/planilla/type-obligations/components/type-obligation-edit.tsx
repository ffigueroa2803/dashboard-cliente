/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { TypeObligationForm } from "./type-obligation-form";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { ITypeObligationEntity } from "../dtos/type-obligation.entity";
import { RootState } from "@store/store";
import { useTypeObligationEdit } from "../hooks/use-type-obligation-edit";
import { useTypeObligationFind } from "../hooks/use-type-obligation-find";
import { typeObligationActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (typeObligation: ITypeObligationEntity) => void;
}

export const TypeObligationEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { typeObligation } = useSelector(
    (state: RootState) => state.typeObligation
  );

  const typeObligationEdit = useTypeObligationEdit();
  const typeObligationFind = useTypeObligationFind();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await typeObligationEdit
      .update()
      .then(async (data) => {
        const result = await typeObligationFind
          .find(typeObligation.id)
          .then((res) => res)
          .catch(() => data);
        if (typeof onSave == "function") onSave(result);
      })
      .catch(() => null);
    setPending(false);
  };

  useEffect(() => {
    if (typeObligation?.id && isOpen) typeObligationEdit.clearForm();
  }, [typeObligation, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Configuración Obligación
      </ModalHeader>
      <ModalBody>
        <TypeObligationForm
          isEdit={true}
          form={typeObligationEdit.form}
          onChange={typeObligationEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
