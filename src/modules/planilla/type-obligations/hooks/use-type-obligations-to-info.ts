import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { typeObligationActions } from "../store";
import { RootState } from "@store/store";

const request = planillaRequest();

export const useTypeObligationsToInfo = () => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [querySearch, setQuerySearch] = useState<string>("");

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("limit", `${limit}`);
    params.set("querySearch", querySearch || "");
    await request
      .get(`infos/${info.id}/typeObligations`, { params })
      .then((res) => dispatch(typeObligationActions.paginate(res.data)))
      .catch(() =>
        dispatch(
          typeObligationActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 1,
              itemsPerPage: 0,
            },
          })
        )
      );
    setPending(false);
  };

  return {
    pending,
    querySearch,
    limit,
    fetch,
    setPage,
    setLimit,
    setQuerySearch,
  };
};
