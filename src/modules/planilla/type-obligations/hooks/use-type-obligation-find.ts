import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { ITypeObligationEntity } from "../dtos/type-obligation.entity";
import { useDispatch } from "react-redux";
import { typeObligationActions } from "../store";

const request = planillaRequest();

export const useTypeObligationFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<ITypeObligationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`typeObligations/${id}`)
        .then((res) => {
          dispatch(typeObligationActions.find(res.data));
          resolve(res.data as ITypeObligationEntity);
        })
        .catch((err) => {
          dispatch(typeObligationActions.find({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
