import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import {
  ETypeObligationMode,
  ITypeObligationEntity,
} from "../dtos/type-obligation.entity";
import { toast } from "react-toastify";
import { IFormTypeObligationDto } from "../dtos/form-type-obligation.dto";
import { IInputHandle } from "@common/dtos/input-handle";
import { ICreateTypeObligationDto } from "../dtos/create-type-obligation.dto";

const request = planillaRequest();

const dataDefault: IFormTypeObligationDto = {
  personId: 0,
  infoId: 0,
  typeDiscountId: 0,
  documentTypeId: "01",
  documentNumber: "",
  bankId: 0,
  numberOfAccount: "",
  terminationDate: "",
  isPercent: false,
  amount: 0,
  mode: ETypeObligationMode.BRUTO,
  isBonification: false,
  observation: "",
};

export const useTypeObligationCreate = () => {
  const [form, setForm] = useState<IFormTypeObligationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const clearForm = () => {
    setForm(dataDefault);
  };

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const save = (): Promise<ITypeObligationEntity> => {
    return new Promise(async (resolve, reject) => {
      const payload = Object.assign(form, {
        terminationDate: form.terminationDate || null,
        numberOfAccount: form.numberOfAccount || null,
      });
      // send
      setPending(true);
      await request
        .post(`typeObligations`, payload)
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data as ITypeObligationEntity);
        })
        .catch((err) => {
          toast.error(`No se pudo guardar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    form,
    setForm,
    clearForm,
    changeForm,
    save,
  };
};
