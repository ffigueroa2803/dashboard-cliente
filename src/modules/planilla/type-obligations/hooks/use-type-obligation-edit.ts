import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import {
  ITypeObligationEntity,
  ETypeObligationMode,
} from "../dtos/type-obligation.entity";
import { toast } from "react-toastify";
import { IEditTypeObligationDto } from "../dtos/edit-type-obligation.dto";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";
import { IFormTypeObligationDto } from "../dtos/form-type-obligation.dto";

const request = planillaRequest();

export const useTypeObligationEdit = () => {
  const { typeObligation } = useSelector(
    (state: RootState) => state.typeObligation
  );

  const dataDefault: IFormTypeObligationDto = {
    personId: typeObligation.personId || 0,
    infoId: typeObligation.infoId || 0,
    typeDiscountId: typeObligation.typeDiscountId || 0,
    documentTypeId: typeObligation.documentTypeId || "01",
    documentNumber: typeObligation.documentNumber || "",
    bankId: typeObligation.bankId || 0,
    numberOfAccount: typeObligation.numberOfAccount || "",
    terminationDate: typeObligation.terminationDate || "",
    isPercent: typeObligation.isPercent || false,
    amount: typeObligation.isPercent
      ? typeObligation.percent
      : typeObligation.amount,
    mode: typeObligation.mode || ETypeObligationMode.DEFAULT,
    isBonification: false,
    observation: typeObligation.observation || "",
  };

  const [form, setForm] = useState<IFormTypeObligationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = (): Promise<ITypeObligationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      // settings
      const payload: IEditTypeObligationDto = Object.assign(form, {
        amount: parseFloat(`${form.amount}`),
        terminationDate: form.terminationDate || null,
        numberOfAccount: form.numberOfAccount || null,
      });
      // send
      setPending(true);
      await request
        .put(`typeObligations/${typeObligation.id}`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as ITypeObligationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  const clearForm = () => {
    setForm(dataDefault);
  };

  return {
    form,
    changeForm,
    clearForm,
    pending,
    update,
  };
};
