import { ETypeObligationMode } from "./type-obligation.entity";

export interface ICreateTypeObligationDto {
  personId: number;
  infoId: number;
  typeDiscountId: number;
  documentTypeId: string;
  documentNumber: string;
  bankId: number;
  numberOfAccount: string;
  terminationDate: string;
  isPercent: boolean;
  percent: number;
  amount: number;
  mode: ETypeObligationMode;
  isBonification: boolean;
  observation?: string;
}
