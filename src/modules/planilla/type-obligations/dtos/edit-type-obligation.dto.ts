import { ETypeObligationMode } from "./type-obligation.entity";

export interface IEditTypeObligationDto {
  documentTypeId: string;
  documentNumber: string;
  bankId: number;
  numberOfAccount: string;
  terminationDate: string;
  isPercent: boolean;
  amount: number;
  mode: ETypeObligationMode;
  isBonification: boolean;
  observation?: string;
}
