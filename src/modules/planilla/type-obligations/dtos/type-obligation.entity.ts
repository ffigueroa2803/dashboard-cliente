import { IBankEntity } from "@modules/auth/banks/dtos/bank.entity";
import { PersonEntity } from "@modules/auth/person/dtos/person.entity";
import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";
import { IDocumentTypeEntity } from "@services/entities/document-type.entity";

export enum ETypeObligationMode {
  DEFAULT = "DEFAULT",
  BRUTO = "BRUTO",
  NETO = "NETO",
}

export interface ITypeObligationEntity {
  id: number;
  personId: number;
  infoId: number;
  typeDiscountId: number;
  documentTypeId: string;
  documentNumber: string;
  bankId: number;
  isCheck: boolean;
  numberOfAccount: string;
  terminationDate: string;
  isOver: boolean;
  isPercent: boolean;
  percent: number;
  amount: number;
  mode: ETypeObligationMode;
  isBonification: boolean;
  observation?: string;
  orderBy: string;
  state: boolean;
  typeDiscount?: TypeDescuento;
  person?: PersonEntity;
  bank?: IBankEntity;
  documentType?: IDocumentTypeEntity;
}
