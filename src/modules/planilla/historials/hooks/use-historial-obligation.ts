import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { obligationActions } from "../../obligations/store";

const request = planillaRequest();

export const useHistorialObligation = () => {
  const [pending, setPending] = useState<boolean>(false);

  const dispatch = useDispatch();

  const { historial } = useSelector((state: RootState) => state.historial);

  const execute = async () => {
    setPending(true);
    await request
      .get(`historials/${historial.id}/obligations`)
      .then((res) => dispatch(obligationActions.paginate(res.data)))
      .catch(() =>
        dispatch(
          obligationActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 0,
              itemsPerPage: 0,
            },
          })
        )
      );
    setPending(false);
  };

  return {
    pending,
    execute,
  };
};
