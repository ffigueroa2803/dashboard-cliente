import { useConfirm } from "@common/confirm/use-confirm";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useHistorialSendMail = () => {
  const [pending, setPending] = useState<boolean>(false);

  const { historial } = useSelector((state: RootState) => state.historial);

  const confirm = useConfirm();

  const execute = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Enviar Boleta",
          message: "¿Estás seguro en enviar la boleta?",
          labelSuccess: "Enviar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info("Enviando Boleta...");
          await request
            .post(`historials/${historial.id}/sendMail`)
            .then((res) => {
              toast.dismiss();
              toast.success("El boleta se envio correctamente!");
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error("No se pudo enviar la boleta");
              reject(err);
            });
          setPending(false);
        })
        .catch((err) => reject(err));
    });
  };

  return {
    pending,
    execute,
  };
};
