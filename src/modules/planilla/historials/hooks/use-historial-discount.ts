import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { discountActions } from "../../discounts/store";

const request = planillaRequest();

export const useHistorialDiscount = () => {
  const dispatch = useDispatch();

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(100);
  const [pending, setPending] = useState<boolean>(false);

  const { historial } = useSelector((state: RootState) => state.historial);

  const execute = async () => {
    const params = new URLSearchParams();
    params.set(`page`, `${page}`);
    params.set(`limit`, `${limit}`);
    setPending(true);
    await request
      .get(`historials/${historial.id}/discounts`, { params })
      .then((res) => dispatch(discountActions.paginate(res.data)))
      .catch(() =>
        dispatch(
          discountActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 0,
              itemsPerPage: 0,
            },
          })
        )
      );
    setPending(false);
  };

  return {
    page,
    setPage,
    limit,
    setLimit,
    pending,
    execute,
  };
};
