import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { planillaRequest } from "@services/planilla.request";
import { historialActions } from "../store";

const request = planillaRequest();

export const useHistorialResume = () => {
  const dispatch = useDispatch();

  const { historial } = useSelector((state: RootState) => state.historial);

  const [pending, setPending] = useState<boolean>(false);

  const execute = async () => {
    setPending(true);
    await request
      .get(`historials/${historial.id}/resume`)
      .then((res) => dispatch(historialActions.setResume(res.data)))
      .catch(() =>
        dispatch(
          historialActions.setResume({
            totalAportation: 0,
            totalBase: 0,
            totalDiscount: 0,
            totalNeto: 0,
            totalRemuneration: 0,
          })
        )
      );
    setPending(false);
  };

  // response
  return {
    pending,
    execute,
  };
};
