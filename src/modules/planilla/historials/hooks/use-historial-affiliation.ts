import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { affiliationActions } from "../../affiliations/store";

const request = planillaRequest();

export const useHistorialAffiliation = () => {
  const [pending, setPending] = useState<boolean>(false);

  const dispatch = useDispatch();

  const { historial } = useSelector((state: RootState) => state.historial);

  const execute = async () => {
    setPending(true);
    await request
      .get(`historials/${historial.id}/affiliations`)
      .then((res) => dispatch(affiliationActions.paginate(res.data)))
      .catch(() =>
        dispatch(
          affiliationActions.paginate({
            items: [],
            meta: {
              totalItems: 0,
              totalPages: 0,
              itemsPerPage: 0,
            },
          })
        )
      );
    setPending(false);
  };

  return {
    pending,
    execute,
  };
};
