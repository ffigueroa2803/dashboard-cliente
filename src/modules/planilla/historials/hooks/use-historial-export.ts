import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";
import { DateTime } from "luxon";

const currentDate = DateTime.now();

const request = planillaRequest();

export const useHistorialExport = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [year, setYear] = useState<number>(currentDate.year);
  const [month, setMonth] = useState<number>(currentDate.month);

  const confirm = useConfirm();

  const generateExcel = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Exportar",
          message: "¿Estás seguro en exportar el historial mensual?",
          labelSuccess: "Exportar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          toast.info(`Exportando datos...`);
          await request
            .get(`historials/${year}/${month}/exports.xlsx`, {
              responseType: "blob",
            })
            .then((res) => {
              const url = URL.createObjectURL(res.data);
              const a = document.createElement("a");
              a.href = url;
              a.download = `export-data.xlsx`;
              a.click();
              resolve(res.data);
            })
            .catch((err) => {
              toast.dismiss();
              toast.error(`No se pudo exportar los datos`);
              reject(err);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    pending,
    year,
    setYear,
    month,
    setMonth,
    generateExcel,
  };
};
