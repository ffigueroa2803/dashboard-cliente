import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { IHistorialEntity } from "../dtos/historial.entity";
import { Search, ChevronRight, Plus, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { IRowSelected } from "@common/dtos/row-selected";
import { CronogramaPimSelect } from "@modules/planilla/cronogramas/components/cronograma-pim-select";
import { Show } from "@common/show";
import Skeleton from "react-loading-skeleton";

// eslint-disable-next-line no-unused-vars
declare type onClick = (historial: IHistorialEntity) => void;

interface IProps {
  data: IHistorialEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  defaultQuerySearch?: string;
  isOptions?: boolean;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSearch?: (data: { querySearch: string; pimId: number }) => void;
  onClick?: onClick;
  onSelectedRowsChange?: (selected: IRowSelected<IHistorialEntity>) => void;
  clearSelectedRows?: boolean;
}

const datosPlaceholder: any[] = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];

export const HistorialTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  clearSelectedRows,
  isOptions,
  onSearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onSelectedRowsChange,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );

  const [pimId, setPimId] = useState<number>(0);

  const columns = useMemo(() => {
    const options = {
      name: "Opciones",
      grow: true,
      selector: (row: IHistorialEntity) => (
        <Show condition={!loading} isDefault={<Skeleton width={"100%"} />}>
          <Plus className="icon" />
          <Trash className="icon ml-2" />
        </Show>
      ),
    };

    const rows: any = [
      {
        name: "#ID",
        grow: true,

        selector: (row: IHistorialEntity) => row.id,
      },
      {
        name: "Nombre Completo",
        wrap: true,
        selector: (row: IHistorialEntity) => (
          <span className="uppercase">
            {row?.info?.contract?.work?.person?.fullName || ""}
          </span>
        ),
      },
      {
        name: "N° Documento",
        grow: true,
        selector: (row: IHistorialEntity) =>
          `${row?.info?.contract?.work?.person?.documentNumber || ""}`,
      },
      {
        name: "Tipo Categoría",
        selector: (row: IHistorialEntity) =>
          row?.info?.contract?.typeCategory?.name || "",
      },
      {
        name: "Condición",
        selector: (row: IHistorialEntity) =>
          row?.info?.contract?.condition || "",
      },
      {
        name: "PIM",
        selector: (row: IHistorialEntity) => (
          <span>
            {row?.pim?.code || ""}
            <span className="ml-2 badge badge-sm badge-dark">
              {row?.pim?.cargo?.extension || ""}
            </span>
          </span>
        ),
      },
    ];

    if (isOptions) rows.push(options);

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  function handleSearch() {
    if (typeof onSearch == "function") {
      onSearch({
        querySearch,
        pimId,
      });
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (historial: IHistorialEntity) => {
    if (typeof onClick == "function") onClick(historial);
  };

  const handleOnSelectedRowsChange = (
    selected: IRowSelected<IHistorialEntity>
  ) => {
    if (typeof onSelectedRowsChange == "function")
      onSelectedRowsChange(selected);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="6 col-6" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="4 col-6" className="mb-2">
              <CronogramaPimSelect
                name="pimId"
                value={pimId}
                onChange={(obj) => setPimId(obj.value)}
              />
            </Col>
            <Col md="2 col-12" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        disabled={loading}
        progressPending={loading}
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        selectableRows={typeof onSelectedRowsChange == "function"}
        onSelectedRowsChange={handleOnSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
      />
    </>
  );
};
