/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-named-as-default-member */
import { RootState } from "@store/store";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { Button, Col, Row } from "reactstrap";
import { useHistorialResume } from "../hooks/use-historial-resume";
import currencyFormatter from "currency-formatter";
import Skeleton from "react-loading-skeleton";
import { Show } from "@common/show";

const PlaceholderItem = () => <Skeleton height={30} />;

export const HistorialResume = () => {
  const { execute, pending } = useHistorialResume();

  const { historial, resume } = useSelector(
    (state: RootState) => state.historial
  );

  useEffect(() => {
    if (historial?.id) execute();
  }, [historial]);

  return (
    <Row>
      <Col md="3" className="text-center">
        <Show condition={!pending} isDefault={<PlaceholderItem />}>
          <Button outline color="dark" block>
            Bruto{" "}
            {currencyFormatter.format(resume.totalRemuneration, {
              code: "PEN",
            })}
          </Button>
        </Show>
      </Col>
      <Col md="3" className="text-center">
        <Show condition={!pending} isDefault={<PlaceholderItem />}>
          <Button outline color="dark" block>
            Dscto{" "}
            {currencyFormatter.format(resume.totalDiscount, { code: "PEN" })}
          </Button>
        </Show>
      </Col>
      <Col md="3" className="text-center">
        <Show condition={!pending} isDefault={<PlaceholderItem />}>
          <Button outline color="dark" block>
            Base {currencyFormatter.format(resume.totalBase, { code: "PEN" })}
          </Button>
        </Show>
      </Col>
      <Col md="3" className="text-center">
        <Show condition={!pending} isDefault={<PlaceholderItem />}>
          <Button outline color="dark" block>
            Neto {currencyFormatter.format(resume.totalNeto, { code: "PEN" })}
          </Button>
        </Show>
      </Col>
    </Row>
  );
};
