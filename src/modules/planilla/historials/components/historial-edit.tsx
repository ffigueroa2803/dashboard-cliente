/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, FormGroup, Modal, ModalBody, ModalHeader } from "reactstrap";
import { editHistorial, findHistorial } from "../apis";
import { IHistorialFormDto } from "../dtos/historial-form.dto";
import { IHistorialEntity } from "../dtos/historial.entity";
import { historialActions } from "../store";
import { HistorialForm } from "./historial-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (historial: IHistorialEntity) => void;
}

export const HistorialEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { historial } = useSelector((state: RootState) => state.historial);

  const [pending, setPending] = useState<boolean>(false);

  const dataDefault: IHistorialFormDto = {
    pimId: historial?.pimId || 0,
    days: historial?.days || 0,
    bankId: historial?.bankId || 0,
    numberOfAccount: historial?.numberOfAccount || "",
    isPay: historial.isPay || false,
    isSync: historial.isSync || false,
    observation: historial.observation || undefined,
    labelId: historial.labelId || null,
  };

  const [form, setForm] = useState<IHistorialFormDto>(dataDefault);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const findData = async () => {
    await findHistorial(historial.id)
      .then((data) => {
        dispatch(historialActions.find(data));
        if (typeof onSave == "function") {
          onSave(data);
        }
      })
      .catch(() => null);
  };

  const handleSave = async () => {
    setPending(true);
    await editHistorial(historial.id, form)
      .then(async () => {
        toast.success(`Los cambios se guardarón correctamente!`);
        await findData();
      })
      .catch((err) =>
        toast.error(
          err?.response?.data?.message || `No se pudo actualizar los datos!`
        )
      );
    setPending(false);
  };

  useEffect(() => {
    if (isOpen) setForm(dataDefault);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        <span className="badge badge-sm badge-dark">
          {"#"}
          {historial.id}
        </span>{" "}
        Editar Configuración de Pago
      </ModalHeader>
      <ModalBody>
        <ContractDetail contract={historial.info?.contract || ({} as any)} />
        <hr />
        <HistorialForm onChange={handleForm} form={form} />
        <FormGroup className="text-right">
          <hr />
          <Button color="primary" disabled={pending} onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </FormGroup>
      </ModalBody>
    </Modal>
  );
};
