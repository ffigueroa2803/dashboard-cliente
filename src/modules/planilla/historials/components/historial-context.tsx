/* eslint-disable no-unused-vars */
import { createContext, FC, useState } from "react";

export enum historialContextEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
  PLUS = "PLUS",
  MINUS = "MINUS",
}

export const HistorialContext = createContext({
  querySearch: "",
  setQuerySearch: (value: string) => {},
  ids: [0],
  setIds: (value: number[]) => {},
  options: "",
  setOptions: (value: historialContextEnum) => {},
  clearRows: false,
  setClearRows: (value: boolean) => {},
});

export const HistorialProvider: FC = ({ children }) => {
  const [querySearch, setQuerySearch] = useState<string>("");
  const [clearRows, setClearRows] = useState<boolean>(false);
  const [ids, setIds] = useState<number[]>([]);
  const [options, setOptions] = useState<historialContextEnum>(
    historialContextEnum.DEFAULT
  );

  return (
    <HistorialContext.Provider
      value={{
        ids,
        setIds,
        querySearch,
        setQuerySearch,
        options,
        setOptions,
        clearRows,
        setClearRows,
      }}>
      {children}
    </HistorialContext.Provider>
  );
};
