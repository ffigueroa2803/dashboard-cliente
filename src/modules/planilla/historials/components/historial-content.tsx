/* eslint-disable react-hooks/exhaustive-deps */
import { FloatButton } from "@common/button/float-button";
import { Show } from "@common/show";
import { planillaRequest } from "@services/planilla.request";
import { Collection } from "collect.js";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import { File, Search } from "react-feather";
import { Button, Card, CardBody, Col, Form, Input, Row } from "reactstrap";
import urljoin from "url-join";
import { useHistorialList } from "../hooks/use-historial-list";
import { HistorialContext, HistorialProvider } from "./historial-context";
import { HistorialTableSimple } from "./historial-table-simple";

const request = planillaRequest();

const HistorialWrapper = () => {
  const historialContext = useContext(HistorialContext);

  const historialList = useHistorialList({
    page: 1,
    limit: 30,
    querySearch: historialContext.querySearch,
  });

  const router = useRouter();
  const [isFetch, setIsFetch] = useState<boolean>(true);

  const changePage = (page: number) => {
    historialList.changeQuery({ name: "page", value: page });
    historialContext.setClearRows(true);
    historialContext.setIds([]);
    setIsFetch(true);
  };

  const onChangeRowsPerPage = (limit: number) => {
    historialList.changeQuery({ name: "limit", value: limit });
    historialContext.setClearRows(true);
    historialContext.setIds([]);
    setIsFetch(true);
  };

  const onSelectedRowsChange = ({ selectedRows }: any) => {
    const ids: number[] = new Collection(selectedRows).pluck("id").toArray();
    historialContext.setIds(ids);
    if (ids.length == 0) historialContext.setClearRows(true);
  };

  const linkBoleta = () => {
    console.log(historialContext.ids);
    const paramsIds = historialContext.ids.join(":");
    const url = urljoin(
      request.urlBase,
      `/historials/${paramsIds}/ticketCollection.pdf`
    );
    // abrir reporte
    window.open(url, "report", "menubar=no,location=yes");
  };

  useEffect(() => {
    return () => setIsFetch(true);
  }, [router]);

  useEffect(() => {
    historialList.changeQuery({
      name: "querySearch",
      value: historialContext.querySearch || "",
    });
  }, [historialContext.querySearch]);

  useEffect(() => {
    if (isFetch) {
      historialContext.setIds([]);
      historialContext.setClearRows(true);
      historialList
        .fetch()
        .then(() => historialContext.setClearRows(false))
        .catch(() => null);
    }
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <>
      <Card>
        <CardBody>
          {/* search */}
          <Form
            className="mb-4"
            onSubmit={(e) => {
              e.preventDefault();
              setIsFetch(true);
            }}
          >
            <Row>
              <Col md="5">
                <Input
                  type="text"
                  name="querySearch"
                  value={historialContext.querySearch}
                  disabled={historialList.pending}
                  onChange={({ target }) =>
                    historialContext.setQuerySearch(target.value)
                  }
                />
              </Col>
              <Col md="3">
                <Button color="primary" onClick={() => setIsFetch(true)}>
                  <Search className="icon" />
                </Button>
              </Col>
            </Row>
          </Form>
          {/* table */}
          <HistorialTableSimple
            loading={historialList.pending}
            onChangePage={changePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
            onSelectedRowsChange={onSelectedRowsChange}
            clearSelectedRows={historialContext.clearRows}
            selectableRows
            isSendMail
          />
        </CardBody>
      </Card>
      {/* generar pdf */}
      <Show condition={historialContext.ids.length > 0}>
        <FloatButton color="danger" icon={<File />} onClick={linkBoleta} />
      </Show>
    </>
  );
};

export const HistorialContent = () => {
  return (
    <HistorialProvider>
      <HistorialWrapper />
    </HistorialProvider>
  );
};
