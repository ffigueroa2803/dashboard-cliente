import { Show } from "@common/show";
import { ContractSerialize } from "@modules/scale/contracts/dtos/contract-serialize";
import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { useState } from "react";
import { Edit, Info } from "react-feather";
import { useSelector } from "react-redux";
import { Card, CardBody, CardHeader, Col, FormGroup, Row } from "reactstrap";
import { HistorialEdit } from "./historial-edit";

interface IProps {
  isBlock: boolean;
}

export const HistorialInfo = ({ isBlock }: IProps) => {
  const { historial } = useSelector((state: RootState) => state.historial);

  const [isEdit, setIsEdit] = useState<boolean>(false);

  const contractSerialize = plainToClass(
    ContractSerialize,
    historial?.info?.contract || {}
  );

  const handleUpdate = () => {
    setIsEdit(false);
  };

  return (
    <>
      <Card>
        <CardHeader>
          <h6>
            <Info className="icon" /> Información de Config. Pago
            <span className="ml-2 badge badge-sm badge-warning">
              {historial?.info?.codeAIRHSP ? historial?.info?.codeAIRHSP : ""}
            </span>
            <Show condition={!isBlock}>
              <Edit
                className="icon close cursor-pointer"
                onClick={() => setIsEdit(true)}
              />
            </Show>
          </h6>
        </CardHeader>
        <CardBody>
          <Row>
            {/* Info afp */}
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Ley Social</label>
                <h6>
                  {historial?.afp?.name || ""}{" "}
                  {historial?.afp?.isPrivate ? historial?.afp?.typeAfp : ""}
                </h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>N° CUSSP</label>
                <h6>{historial?.numberOfCussp || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>F. Afiliación</label>
                <h6>{historial?.affiliationOfDate || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Prima Seguro</label>
                <h6>{historial?.isPrimaSeguro ? "SI" : "NO"}</h6>
              </FormGroup>
            </Col>
            {/* info contrato */}
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Fecha de Inicio</label>
                <h6>{contractSerialize.displayDateOfAdmission || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Fecha de Cese</label>
                <h6>
                  {contractSerialize.displayTerminationDate || "Indefinido"}
                </h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>N° Essalud</label>
                <h6>{historial?.numberOfEssalud || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Tipo Trabajador</label>
                <h6>
                  {historial?.info?.contract?.typeCategory?.typeCargo?.name ||
                    "N/A"}
                </h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Condición</label>
                <h6>{historial?.info?.contract?.condition || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Tipo Categoría</label>
                <h6 className="capitalize">
                  {historial?.info?.contract?.typeCategory?.name || "N/A"}
                </h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Plaza</label>
                <h6>{historial?.info?.contract?.plaza || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Agrupación</label>
                <h6>{historial?.label?.name || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="6" className="mb-3">
              <FormGroup>
                <label>Dependencia/Oficina</label>
                <h6>{historial?.info?.contract?.dependency?.name || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Perfil Lab.</label>
                <h6>{historial?.info?.contract?.profile?.name || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Días Lab.</label>
                <h6>{historial?.days || "N/A"}</h6>
              </FormGroup>
            </Col>
            {/* info pay */}
            <Col md="6" className="mb-3">
              <FormGroup>
                <label>Meta Presp.</label>
                <h6>
                  {historial?.pim?.code || "N/A"} {" - "}
                  {historial?.pim?.meta?.name || "N/A"}
                </h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Partición Presp.</label>
                <h6>{historial?.pim?.cargo?.name || ""}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Extensión Presp.</label>
                <h6>{historial?.pim?.cargo?.extension || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Medio de Pago</label>
                <h6>{historial.numberOfAccount ? "En Cuenta" : "En Cheque"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Banco</label>
                <h6>{historial?.bank?.name || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>N° Cuenta</label>
                <h6>{historial?.numberOfAccount || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup>
                <label>Remuneración</label>
                <h6>{historial.isPay ? "SI" : "NO"}</h6>
              </FormGroup>
            </Col>
            <Col md="9" className="mb-3">
              <FormGroup>
                <label>Observación</label>
                <h6>{historial.observation || "N/A"}</h6>
              </FormGroup>
            </Col>
            <Col md="3" className="mb-3">
              <FormGroup className="mt-3">
                <label>Enviar Boleta</label>
                <h6>{historial?.info?.isEmail ? "SI" : "NO"}</h6>
              </FormGroup>

              <FormGroup className="mt-3">
                <label>Sinconización de edición</label>
                <h6>{historial?.isSync ? "SI" : "NO"}</h6>
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>
      {/* editar */}
      <HistorialEdit
        isOpen={isEdit}
        onClose={() => setIsEdit(false)}
        onSave={handleUpdate}
      />
    </>
  );
};
