/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IHistorialFormDto } from "../dtos/historial-form.dto";
import Toggle from "@atlaskit/toggle";
import { BankSelect } from "@modules/auth/banks/components/bank-select";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { LabelSelect } from "@modules/planilla/labels/components/label-select";

interface IProps {
  form: IHistorialFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
}

export const HistorialForm = ({ form, onChange, isDisabled }: IProps) => {
  const { historial } = useSelector((state: RootState) => state.historial);

  return (
    <Form>
      <FormGroup className="mb-3">
        <label>
          PIM <b className="text-danger">*</b>
        </label>
        <PimSelect
          name="pimId"
          year={historial?.pim?.year || new Date().getFullYear()}
          value={form?.pimId}
          onChange={(target) => onChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Días Lab. <b className="text-danger">*</b>
        </label>
        <Input
          type="number"
          name="days"
          value={form?.days}
          onChange={({ target }) =>
            onChange({ name: target.name, value: parseInt(target.value) })
          }
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Banco <b className="text-danger">*</b>
        </label>
        <BankSelect
          name="bankId"
          value={form?.bankId}
          onChange={(target) => onChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>N° Cuenta</label>
        <Input
          type="text"
          name="numberOfAccount"
          value={form?.numberOfAccount || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>Agrupación</label>
        <LabelSelect
          name="labelId"
          value={form?.labelId || null}
          onChange={(target) => onChange(target)}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>¿Pago remunerado?</label>
        <div>
          <Toggle
            isDisabled={isDisabled}
            name="isPay"
            isChecked={form?.isPay || false}
            onChange={({ target }) =>
              onChange({ name: target.name, value: target.checked })
            }
          />
        </div>
      </FormGroup>

      <FormGroup className="mb-3">
        <label>¿Sinconizar configuración?</label>
        <div>
          <Toggle
            isDisabled={isDisabled}
            name="isSync"
            isChecked={form?.isSync || false}
            onChange={({ target }) =>
              onChange({ name: target.name, value: target.checked })
            }
          />
        </div>
      </FormGroup>

      <FormGroup className="mb-3">
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>
    </Form>
  );
};
