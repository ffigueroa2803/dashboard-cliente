/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from "@store/store";
import { useEffect, useMemo, useState } from "react";
import Datatable from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Send } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "reactstrap";
import { IHistorialEntity } from "../dtos/historial.entity";
import { useHistorialSendMail } from "../hooks/use-historial-send-mail";
import { historialActions } from "../store";

interface IProps {
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onSelectedRowsChange?: (args: any) => void;
  selectableRows?: boolean;
  clearSelectedRows?: boolean;
  loading: boolean;
  isSendMail?: boolean;
}

export const HistorialTableSimple = ({
  onChangePage,
  onChangeRowsPerPage,
  onSelectedRowsChange,
  clearSelectedRows,
  selectableRows,
  loading,
  isSendMail,
}: IProps) => {
  const dispatch = useDispatch();
  const [isSend, setIsSend] = useState<boolean>(false);
  const { historials } = useSelector((state: RootState) => state.historial);

  const historialMail = useHistorialSendMail();

  const handleClick = (history: IHistorialEntity) => {
    dispatch(historialActions.find(history));
    setIsSend(true);
  };

  const handleSend = async () => {
    await historialMail.execute().catch(() => null);
    setIsSend(false);
  };

  useEffect(() => {
    if (isSend) handleSend();
  }, [isSend]);

  const columns = useMemo(() => {
    const rows: any[] = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IHistorialEntity) => row.id,
      },
      {
        name: "Nombre Completo",
        wrap: true,
        selector: (row: IHistorialEntity) => (
          <span className="uppercase">
            {row?.info?.contract?.work?.person?.fullName || ""}
          </span>
        ),
      },
      {
        name: "N° Documento",
        selector: (row: IHistorialEntity) =>
          `${row?.info?.contract?.work?.person?.documentNumber || ""}`,
      },
      {
        name: "Planilla",
        grow: true,
        selector: (row: IHistorialEntity) => row?.info?.planilla?.name || "",
      },
      {
        name: "Tipo Categoría",
        selector: (row: IHistorialEntity) =>
          row?.info?.contract?.typeCategory?.name || "",
      },
      {
        name: "Condición",
        selector: (row: IHistorialEntity) =>
          row?.info?.contract?.condition || "",
      },
      {
        name: "PIM",
        selector: (row: IHistorialEntity) => (
          <span>
            {row?.pim?.code || ""}
            <span className="ml-2 badge badge-sm badge-dark">
              {row?.pim?.cargo?.extension || ""}
            </span>
          </span>
        ),
      },
      {
        name: "Fecha",
        grow: true,
        selector: (row: IHistorialEntity) => (
          <span>
            <span className="ml-2 badge badge-sm badge-primary">
              {row?.cronograma?.year || ""}
            </span>
          </span>
        ),
      },
      {
        name: "Mes",
        grow: true,
        selector: (row: IHistorialEntity) => (
          <span>
            <b className="ml-2 badge badge-sm badge-dark">
              {row?.cronograma?.month || ""}
            </b>
          </span>
        ),
      },
    ];

    if (isSendMail) {
      rows.push({
        name: "Envio de Mail",
        wrap: true,
        selector: (row: IHistorialEntity) => (
          <div className="flex justify-center">
            <Button
              color={row.sendEmail ? "success" : "danger"}
              outline={!row.sendEmail}
              disabled={historialMail.pending}
              onClick={() => handleClick(row)}
            >
              <Send className="icon" />
            </Button>
          </div>
        ),
      });
    }

    return rows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [historials]);

  return (
    <>
      <Datatable
        data={historials.items || []}
        columns={columns as any}
        striped
        responsive
        progressPending={loading || false}
        pagination
        paginationPerPage={historials?.meta?.itemsPerPage || 30}
        paginationServer
        paginationTotalRows={historials?.meta?.totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
        onSelectedRowsChange={onSelectedRowsChange}
        clearSelectedRows={clearSelectedRows}
        selectableRows={selectableRows}
      />
    </>
  );
};
