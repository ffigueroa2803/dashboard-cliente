import { planillaRequest } from "@services/planilla.request";
import { IEditHistorialDto } from "./dtos/edit-historial.dto";

const request = planillaRequest();

export const findHistorial = async (id: number) => {
  return await request.get(`historials/${id}`).then((res) => res.data);
};

export const editHistorial = async (id: number, payload: IEditHistorialDto) => {
  payload.labelId = payload.labelId || null;
  return await request.put(`historials/${id}`, payload).then((res) => res.data);
};
