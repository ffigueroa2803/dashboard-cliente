import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IHistorialEntity } from "./dtos/historial.entity";

interface IHistorialResume {
  totalAportation: number;
  totalBase: number;
  totalDiscount: number;
  totalRemuneration: number;
  totalNeto: number;
}

export interface HistorialState {
  historials: ResponsePaginateDto<IHistorialEntity>;
  historial: IHistorialEntity;
  option: string;
  resume: IHistorialResume;
}

export const initialState: HistorialState = {
  historials: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  historial: {} as any,
  option: "",
  resume: {
    totalAportation: 0,
    totalBase: 0,
    totalDiscount: 0,
    totalRemuneration: 0,
    totalNeto: 0,
  },
};

const historialStore = createSlice({
  name: "planilla@historials",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IHistorialEntity>>
    ) => {
      state.historials = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<IHistorialEntity>) => {
      state.historial = payload;
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
    setResume: (state, { payload }: PayloadAction<IHistorialResume>) => {
      state.resume = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.historial };
    },
  },
});

export const historialReducer = historialStore.reducer;

export const historialActions = historialStore.actions;
