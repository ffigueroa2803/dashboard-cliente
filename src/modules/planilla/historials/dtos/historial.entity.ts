import { IBankEntity } from "@modules/auth/banks/dtos/bank.entity";
import { AfpEntity } from "@modules/scale/afps/dtos/afp.entity";
import { IInfoEntity } from "@modules/planilla/infos/dtos/info.entity";
import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { ICronogramaEntity } from "@modules/planilla/cronogramas/dtos/cronograma.entity";
import { ILabelEntity } from "@modules/planilla/labels/dtos/label.entity";

export interface IHistorialEntity {
  id: number;
  infoId: number;
  pimId: number;
  bankId: number;
  numberOfAccount?: string;
  afpId: number;
  affiliationOfDate?: string;
  numberOfCussp?: string;
  isPrimaSeguro: boolean;
  numberOfEssalud?: string;
  tokenVerify: string;
  isPay: boolean;
  sendEmail: boolean;
  sendEmailContact?: string;
  isSync: boolean;
  observation?: string;
  labelId?: number;
  days: number;
  state: boolean;
  afp?: AfpEntity;
  bank?: IBankEntity;
  pim?: IPimEntity;
  info?: IInfoEntity;
  label?: ILabelEntity;
  cronograma?: ICronogramaEntity;
}

export const historialEntityName = "HistorialEntity";
