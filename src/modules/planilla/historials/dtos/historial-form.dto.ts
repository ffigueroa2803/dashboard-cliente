export interface IHistorialFormDto {
  pimId: number;
  days: number;
  bankId: number;
  numberOfAccount?: string;
  isPay: boolean;
  isSync: boolean;
  observation?: string;
  labelId: number | null;
}
