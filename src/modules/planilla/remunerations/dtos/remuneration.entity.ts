import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { TypeRemuneration } from "@modules/planilla/type_remuneration/dtos/type_remuneration.entity";

export interface IRemunerationEntity {
  id: number;
  historialId: number;
  typeRemunerationId: number;
  amount: number;
  isBase: boolean;
  isEdit: boolean;
  isBonification: boolean;
  pimId?: number;
  isVisibled: boolean;
  typeRemuneration?: TypeRemuneration;
  pim?: IPimEntity;
}

export const remunerationEntityName = "RemunerationEntity";
