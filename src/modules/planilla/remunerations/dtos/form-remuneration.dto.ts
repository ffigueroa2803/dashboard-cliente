export interface IFormRemunerationDto {
  typeRemunerationId: number;
  pimId?: number;
  amount: number;
  isBase: boolean;
  isEdit: boolean;
}
