import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { IFormRemunerationDto } from "../dtos/form-remuneration.dto";
import { planillaRequest } from "@services/planilla.request";

const request = planillaRequest();

export const useRemunerationEdit = () => {
  const { remuneration } = useSelector(
    (state: RootState) => state.remuneration
  );

  const dataDefault: IFormRemunerationDto = {
    typeRemunerationId: remuneration.typeRemunerationId,
    pimId: remuneration.pimId || undefined,
    amount: remuneration.amount || 0,
    isBase: remuneration.isBase || false,
    isEdit: remuneration.isEdit || false,
  };

  const [form, setForm] = useState<IFormRemunerationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = () => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando remuneración...`);
      setPending(true);
      await request
        .put(`remunerations/${remuneration.id}`, {
          ...form,
          pimId: form.pimId || null,
          amount: parseFloat(`${form.amount || 0}`),
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los cambios se guardarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo guardar los cambios`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    form,
    setForm,
    changeForm,
    update,
  };
};
