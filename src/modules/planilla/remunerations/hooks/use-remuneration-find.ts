import { useState } from "react";
import { useDispatch } from "react-redux";
import { planillaRequest } from "@services/planilla.request";
import { remunerationActions } from "../store";
import { IRemunerationEntity } from "../dtos/remuneration.entity";

const request = planillaRequest();

export const useRemunerationFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = (id: number): Promise<IRemunerationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`remunerations/${id}`)
        .then((res) => {
          dispatch(remunerationActions.find(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(remunerationActions.find({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
  };
};
