import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IRemunerationEntity } from "./dtos/remuneration.entity";

export interface RemunerationState {
  remunerations: ResponsePaginateDto<IRemunerationEntity>;
  remuneration: IRemunerationEntity;
  option: string;
}

const initialState: RemunerationState = {
  remunerations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  remuneration: {} as any,
  option: "",
};

const remunerationStore = createSlice({
  name: "planilla@remunerations",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IRemunerationEntity>>
    ) => {
      state.remunerations = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<IRemunerationEntity>) => {
      state.remuneration = payload;
      state.remunerations.items?.map((item) => {
        if (payload.id == item.id) {
          return Object.assign(item, payload);
        }
        // default
        return item;
      });
      // response
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.remuneration };
    },
  },
});

export const remunerationReducer = remunerationStore.reducer;

export const remunerationActions = remunerationStore.actions;
