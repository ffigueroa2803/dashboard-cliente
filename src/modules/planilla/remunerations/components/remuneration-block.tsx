import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { RootState } from "@store/store";
import React, { Fragment, useContext, useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import { useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { RemunerationContext } from "./remuneration-context";
import { RemunerationItem } from "./remuneration-item";

interface IProps {
  loading: boolean;
}

const PlaceholderItems = () => {
  const datos = [1, 2, 3, 4, 5];
  return (
    <>
      {datos.map((index) => (
        <Fragment key={`item-loading-${index}`}>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
        </Fragment>
      ))}
    </>
  );
};

export const RemunerationBlock = ({ loading }: IProps) => {
  const ability = useContext(CaslContext);

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { remunerations } = useSelector(
    (state: RootState) => state.remuneration
  );

  const { isEdit, setIsEdit, refreshResume } = useContext(RemunerationContext);

  useEffect(() => {
    if (cronograma?.id) setIsEdit(cronograma.state);
  }, [cronograma]);

  return (
    <Row>
      {/* loading */}
      <Show condition={!loading} isDefault={<PlaceholderItems />}>
        {/* items */}
        {remunerations?.items?.map((rem, index) => (
          <RemunerationItem
            key={`ìtem-${rem.id}-${index}`}
            isEdit={isEdit}
            remuneration={rem}
            onUpdate={refreshResume}
          />
        ))}
      </Show>
    </Row>
  );
};
