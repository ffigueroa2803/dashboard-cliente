/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-vars */
import React, { useContext, useEffect, useMemo, useState } from "react";
import currencyForrmater from "currency-formatter";
import { IRemunerationEntity } from "../dtos/remuneration.entity";
import { Show } from "@common/show";
import { CheckSquare, Clock, Edit } from "react-feather";
import { Col } from "reactstrap";
import { useDispatch } from "react-redux";
import { remunerationActions } from "../store";
import { RemunerationEdit } from "./remuneration-edit";

interface ItemProps {
  remuneration: IRemunerationEntity;
  isEdit: boolean;
  onUpdate?: (remuneration: IRemunerationEntity) => void;
}

export const RemunerationItem = ({
  remuneration,
  isEdit,
  onUpdate,
}: ItemProps) => {
  const dispatch = useDispatch();
  const [isDialog, setIsDialog] = useState<boolean>(false);

  const hasInfo = useMemo(() => {
    return !!remuneration.typeRemuneration?.airhspId || !!remuneration.pimId;
  }, [remuneration]);

  const displayAmount = () => {
    return currencyForrmater.format(remuneration.amount, { code: "PEN" });
  };

  const handleEdit = () => {
    if (!isEdit) return;
    dispatch(remunerationActions.find(remuneration));
    setIsDialog(true);
  };

  const isAmount = parseFloat(`${remuneration.amount}`) > 0;

  return (
    <>
      <Col md="3" className="mb-4 text-ellipsis">
        {/* info remuneration */}
        <label
          className={`capitalize ${isAmount ? "" : "text-muted"}`}
          title={remuneration?.typeRemuneration?.name}
        >
          <>
            <div className="text-left">
              {/* info base */}
              <Show condition={remuneration.isBase}>
                <span title="Base imponible">
                  <CheckSquare
                    className="icon"
                    color="#20c997"
                    style={{ top: "5px" }}
                  />
                </span>
              </Show>
              {/* info pim */}
              <Show
                condition={hasInfo}
                isDefault={<small className="badge badge-light">N/A</small>}
              >
                {/* mostrar info de codeAIRHSP */}
                <Show condition={!!remuneration.typeRemuneration?.airhspId}>
                  <small
                    className="badge badge-light"
                    title={`Codigo AIRHSP: ${remuneration.typeRemuneration?.airhsp?.name}`}
                  >
                    {remuneration.typeRemuneration?.airhsp?.code || ""}
                  </small>
                </Show>
                {/* mostrar info de pim */}
                <Show condition={!!remuneration?.pimId}>
                  <small className="badge badge-warning" title="PIM">
                    {remuneration?.pim?.code} [
                    {remuneration?.pim?.cargo?.extension}]
                  </small>
                </Show>
              </Show>
            </div>
            <span className="disabled-selection">
              <b className={isAmount ? "text-primary" : "text-muted"}>
                {remuneration?.typeRemuneration?.code}
              </b>
              .- {remuneration?.typeRemuneration?.name}
            </span>
          </>
        </label>
        <h6 onDoubleClick={handleEdit}>
          <Show
            condition={isAmount}
            isDefault={<span className="text-muted">{displayAmount()}</span>}
          >
            {displayAmount()}
          </Show>
          {/* action isEdit */}
          <span
            className="close"
            style={{ marginTop: "-3px" }}
            onClick={handleEdit}
          >
            <Show
              condition={remuneration.isEdit}
              isDefault={
                <Clock
                  className={
                    isEdit
                      ? "icon cursor-pointer text-danger"
                      : "icon text-muted"
                  }
                />
              }
            >
              <Edit
                className={
                  isEdit ? "icon cursor-pointer text-danger" : "icon text-muted"
                }
              />
            </Show>
          </span>
        </h6>
      </Col>
      {/* dialog edit */}
      <Show condition={isDialog}>
        <RemunerationEdit
          isOpen={isDialog}
          onClose={() => setIsDialog(false)}
          onSave={onUpdate}
        />
      </Show>
    </>
  );
};
