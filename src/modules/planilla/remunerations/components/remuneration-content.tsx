import React, { useContext, useEffect, useState } from "react";
import { DollarSign, Edit, Settings, X } from "react-feather";
import { Card, CardBody, CardHeader } from "reactstrap";
import { HistorialResume } from "@modules/planilla/historials/components/historial-resume";
import { RemunerationBlock } from "./remuneration-block";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useHistorialRemuneration } from "@modules/planilla/historials/hooks/use-historial-remuneration";
import { InfoTypeRemunerationContent } from "@modules/planilla/info-type-remunerations/components/info-type-remuneration-content";
import { Show } from "@common/show";
import { infoActions } from "@modules/planilla/infos/store";
import {
  RemunerationContext,
  RemunerationProvider,
} from "./remuneration-context";
import { toast } from "react-toastify";

const WrapperRemunerationContext = () => {
  const dispatch = useDispatch();

  const { historial } = useSelector((state: RootState) => state.historial);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const { refreshResume } = useContext(RemunerationContext);

  const [isConfig, setIsConfig] = useState<boolean>(false);

  const historialRemuneration = useHistorialRemuneration();

  const handleConfig = () => {
    dispatch(infoActions.find(historial?.info || ({} as any)));
    setIsConfig(true);
  };

  const handleSaveConfig = () => {
    toast.dismiss();
    toast.info(`Calculando...`);
    setTimeout(async () => {
      await historialRemuneration.execute();
      refreshResume();
    }, 2000);
  };

  useEffect(() => {
    if (historial?.id) historialRemuneration.execute();
  }, [historial]);

  return (
    <>
      <Card>
        <CardBody>
          <HistorialResume />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Show condition={cronograma.state}>
            <Settings
              className="icon close cursor-pointer ml-3"
              onClick={handleConfig}
            />
          </Show>
          <h6>
            <DollarSign className="icon" /> Remuneración
          </h6>
        </CardHeader>
        <CardBody>
          <RemunerationBlock loading={historialRemuneration.pending} />
        </CardBody>
      </Card>
      {/* config remuneration */}
      <Show condition={isConfig}>
        <InfoTypeRemunerationContent
          isOpen={isConfig}
          onClose={() => setIsConfig(false)}
          onSave={handleSaveConfig}
          onCreated={handleSaveConfig}
        />
      </Show>
    </>
  );
};

export const RemunerationContent = () => {
  return (
    <RemunerationProvider>
      <WrapperRemunerationContext />
    </RemunerationProvider>
  );
};
