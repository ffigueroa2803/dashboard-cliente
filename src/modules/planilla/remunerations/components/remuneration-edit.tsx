/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import React from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IRemunerationEntity } from "../dtos/remuneration.entity";
import { useRemunerationEdit } from "../hooks/use-remuneration-edit";
import { useRemunerationFind } from "../hooks/use-remuneration-find";
import { RemunerationForm } from "./remuneration-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (remuneration: IRemunerationEntity) => void;
}

export const RemunerationEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const remunerationEdit = useRemunerationEdit();
  const remunerationFind = useRemunerationFind();

  const { remuneration } = useSelector(
    (state: RootState) => state.remuneration
  );

  const handleUpdate = () => {
    remunerationEdit
      .update()
      .then(async () => {
        await remunerationFind
          .fetch(remuneration.id)
          .then((data) => {
            if (typeof onSave == "function") {
              remunerationEdit.setForm(data);
              onSave(data);
              onClose();
            }
          })
          .catch(() => null);
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Remuneración</ModalHeader>
      <ModalBody>
        <RemunerationForm
          yearDefault={remuneration?.pim?.year || 0}
          isEdit={true}
          form={remunerationEdit.form}
          onChange={remunerationEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          disabled={remunerationEdit.pending}
          onClick={handleUpdate}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
