import { IInputHandle } from "@common/dtos/input-handle";
import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import Toggle from "@atlaskit/toggle";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { DateTime } from "luxon";
import { IFormRemunerationDto } from "../dtos/form-remuneration.dto";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps<T> {
  form: T;
  yearDefault?: number;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
  onSubmit?: () => void;
}

const currentDate = DateTime.now();

export const RemunerationForm = ({
  form,
  yearDefault,
  isEdit,
  onChange,
}: IProps<IFormRemunerationDto>) => {
  const { remuneration } = useSelector(
    (state: RootState) => state.remuneration
  );

  const [year, setYear] = useState<number>(yearDefault || currentDate.year);

  useEffect(() => {
    if (yearDefault) setYear(yearDefault);
  }, [yearDefault]);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Remuneración <b className="text-danger">*</b>
          </label>
          <div>
            <Show
              condition={!isEdit}
              isDefault={
                <h6>
                  {remuneration?.typeRemuneration?.name || "N/A"}
                  <hr />
                </h6>
              }
            >
              <TypeRemunerationSelect
                isDisabled={isEdit}
                name="typeRemunerationId"
                value={form?.typeRemunerationId}
                onChange={onChange}
              />
            </Show>
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>¿Habilitar Edición manual?</label>
          <div>
            <Toggle
              name="isEdit"
              isChecked={form?.isEdit || false}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <Show
          condition={form.isEdit}
          isDefault={
            <FormGroup>
              <label className="w-100">PIM</label>
              <Show
                condition={!remuneration?.pimId}
                isDefault={
                  <span className="badge badge-warning">
                    {remuneration?.pim?.code || ""} [
                    {remuneration?.pim?.cargo?.extension || ""}]{" - "}
                    {remuneration?.pim?.cargo?.name || ""}
                  </span>
                }
              >
                <span className="badge badge-light">N/A</span>
              </Show>
            </FormGroup>
          }
        >
          <FormGroup className="mb-3">
            <label>Año del Pim</label>
            <Input
              type="number"
              value={year}
              onChange={({ target }) => setYear(parseInt(`${target.value}`))}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>Pim</label>
            <div>
              <PimSelect
                year={year}
                name="pimId"
                value={form?.pimId || 0}
                onChange={onChange}
              />
            </div>
          </FormGroup>
        </Show>

        <FormGroup className="mb-3">
          <label>¿Aplica a la base imponible?</label>
          <div>
            <Toggle
              isDisabled={!form.isEdit}
              name="isBase"
              isChecked={form?.isBase || false}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              disabled={!form.isEdit}
              onChange={({ target }) =>
                onChange({ name: target.name, value: parseFloat(target.value) })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
