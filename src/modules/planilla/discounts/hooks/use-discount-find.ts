import { useState } from "react";
import { useDispatch } from "react-redux";
import { planillaRequest } from "@services/planilla.request";
import { discountActions } from "../store";
import { IDiscountEntity } from "../dtos/discount.entity";

const request = planillaRequest();

export const useDiscountFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = (id: number): Promise<IDiscountEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`discounts/${id}`)
        .then((res) => {
          dispatch(discountActions.find(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          dispatch(discountActions.find({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
  };
};
