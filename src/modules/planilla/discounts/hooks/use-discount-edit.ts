import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { IFormDiscountDto } from "../dtos/form-discount.dto";
import { planillaRequest } from "@services/planilla.request";

const request = planillaRequest();

export const useDiscountEdit = () => {
  const { discount } = useSelector((state: RootState) => state.discount);

  const dataDefault: IFormDiscountDto = {
    typeDiscountId: discount.typeDiscountId,
    amount: discount.amount || 0,
    isEdit: discount.isEdit || false,
  };

  const [form, setForm] = useState<IFormDiscountDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = () => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando remuneración...`);
      setPending(true);
      await request
        .put(`discounts/${discount.id}`, {
          ...form,
          amount: parseFloat(`${form.amount || 0}`),
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los cambios se guardarón correctamente!`);
          resolve(res.data);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo guardar los cambios`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    form,
    setForm,
    changeForm,
    update,
  };
};
