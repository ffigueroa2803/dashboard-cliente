import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";

export interface IDiscountEntity {
  id: number;
  historialId: number;
  typeDiscountId: number;
  amount: number;
  isEdit: boolean;
  typeDiscount?: TypeDescuento;
}

export const discountEntityName = "DiscountEntity";
