export interface IFormDiscountDto {
  typeDiscountId: number;
  amount: number;
  isEdit: boolean;
}
