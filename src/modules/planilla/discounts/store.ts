import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IDiscountEntity } from "./dtos/discount.entity";

export interface DiscountState {
  discounts: ResponsePaginateDto<IDiscountEntity>;
  discount: IDiscountEntity;
  option: string;
}

const initialState: DiscountState = {
  discounts: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  discount: {} as any,
  option: "",
};

const discountStore = createSlice({
  name: "planilla@discounts",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IDiscountEntity>>
    ) => {
      state.discounts = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<IDiscountEntity>) => {
      state.discount = payload;
      state.discounts.items?.map((item) => {
        if (payload.id == item.id) {
          return Object.assign(item, payload);
        }
        // default
        return item;
      });
      // response
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.discount };
    },
  },
});

export const discountReducer = discountStore.reducer;

export const discountActions = discountStore.actions;
