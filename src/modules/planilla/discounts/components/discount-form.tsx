import { IInputHandle } from "@common/dtos/input-handle";
import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import Toggle from "@atlaskit/toggle";
import { DateTime } from "luxon";
import { IFormDiscountDto } from "../dtos/form-discount.dto";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
  onSubmit?: () => void;
}

const currentDate = DateTime.now();

export const DiscountForm = ({
  form,
  isEdit,
  onChange,
}: IProps<IFormDiscountDto>) => {
  const { discount } = useSelector((state: RootState) => state.discount);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Descuento <b className="text-danger">*</b>
          </label>
          <div>
            <Show
              condition={!isEdit}
              isDefault={
                <h6>
                  {discount?.typeDiscount?.description || "N/A"}
                  <hr />
                </h6>
              }
            >
              <TypeDiscountSelect
                isDisabled={isEdit}
                name="typeDiscountId"
                value={form?.typeDiscountId}
                onChange={onChange}
              />
            </Show>
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>¿Habilitar Edición manual?</label>
          <div>
            <Toggle
              name="isEdit"
              isChecked={form?.isEdit || false}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              disabled={!form.isEdit}
              onChange={({ target }) =>
                onChange({ name: target.name, value: parseFloat(target.value) })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
