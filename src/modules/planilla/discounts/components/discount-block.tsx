import { RootState } from "@store/store";
import React, { Fragment, useContext, useEffect } from "react";
import { useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { DiscountItem } from "./discount-item";
import { DiscountContext } from "./discount-context";
import Skeleton from "react-loading-skeleton";
import { Show } from "@common/show";

interface IProps {
  loading: boolean;
}

const PlaceholderItems = () => {
  const datos = [1, 2, 3, 4, 5];
  return (
    <>
      {datos.map((index) => (
        <Fragment key={`item-loading-${index}`}>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
        </Fragment>
      ))}
    </>
  );
};

export const DiscountBlock = ({ loading }: IProps) => {
  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { discounts } = useSelector((state: RootState) => state.discount);

  const { isEdit, setIsEdit, refreshResume } = useContext(DiscountContext);

  useEffect(() => {
    if (cronograma?.id) setIsEdit(cronograma?.state);
  }, [cronograma]);

  return (
    <Row>
      {/* loading */}
      <Show condition={!loading} isDefault={<PlaceholderItems />}>
        {/* items */}
        {discounts?.items?.map((dis, index) => (
          <DiscountItem
            key={`item-discount-${dis.id}-${index}`}
            isEdit={isEdit}
            discount={dis}
            onUpdate={refreshResume}
          />
        ))}
      </Show>
    </Row>
  );
};
