import React, { useContext, useEffect, useState } from "react";
import { Edit, Settings, ShoppingCart, X } from "react-feather";
import { Card, CardBody, CardHeader } from "reactstrap";
import { HistorialResume } from "@modules/planilla/historials/components/historial-resume";
import { DiscountBlock } from "./discount-block";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useHistorialDiscount } from "@modules/planilla/historials/hooks/use-historial-discount";
import { infoActions } from "@modules/planilla/infos/store";
import { Show } from "@common/show";
import { InfoTypeDiscountContent } from "@modules/planilla/info-type-discounts/components/info-type-discount-content";
import { DiscountContext, DiscountProvider } from "./discount-context";
import { toast } from "react-toastify";

export const WrapperDiscountContext = () => {
  const dispatch = useDispatch();

  const historialDiscount = useHistorialDiscount();

  const { historial } = useSelector((state: RootState) => state.historial);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const { refreshResume } = useContext(DiscountContext);

  const [isConfig, setIsConfig] = useState<boolean>(false);

  const handleConfig = () => {
    dispatch(infoActions.find(historial?.info || ({} as any)));
    setIsConfig(true);
  };

  const handleSaveConfig = () => {
    toast.dismiss();
    toast.info(`Calculando...`);
    setTimeout(async () => {
      await historialDiscount.execute();
      refreshResume();
    }, 2000);
  };

  useEffect(() => {
    if (historial?.id) historialDiscount.execute();
  }, [historial]);

  return (
    <>
      <Card>
        <CardBody>
          <HistorialResume />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Show condition={cronograma.state}>
            <Settings
              className="icon close cursor-pointer ml-3"
              onClick={handleConfig}
            />
          </Show>
          <ShoppingCart className="icon" /> Descuento
        </CardHeader>
        <CardBody>
          <DiscountBlock loading={historialDiscount.pending} />
        </CardBody>
      </Card>
      {/* config discount */}
      <Show condition={isConfig}>
        <InfoTypeDiscountContent
          isOpen={isConfig}
          onClose={() => setIsConfig(false)}
          onSave={handleSaveConfig}
          onCreated={handleSaveConfig}
        />
      </Show>
    </>
  );
};

export const DiscountContent = () => {
  return (
    <DiscountProvider>
      <WrapperDiscountContext />
    </DiscountProvider>
  );
};
