/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import React from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IDiscountEntity } from "../dtos/discount.entity";
import { useDiscountEdit } from "../hooks/use-discount-edit";
import { useDiscountFind } from "../hooks/use-discount-find";
import { DiscountForm } from "./discount-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (discount: IDiscountEntity) => void;
}

export const DiscountEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const discountEdit = useDiscountEdit();
  const discountFind = useDiscountFind();

  const { discount } = useSelector((state: RootState) => state.discount);

  const handleUpdate = () => {
    discountEdit
      .update()
      .then(async () => {
        await discountFind
          .fetch(discount.id)
          .then((data) => {
            if (typeof onSave == "function") {
              discountEdit.setForm(data);
              onSave(data);
              onClose();
            }
          })
          .catch(() => null);
      })
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Descuento</ModalHeader>
      <ModalBody>
        <DiscountForm
          isEdit={true}
          form={discountEdit.form}
          onChange={discountEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          disabled={discountEdit.pending}
          onClick={handleUpdate}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
