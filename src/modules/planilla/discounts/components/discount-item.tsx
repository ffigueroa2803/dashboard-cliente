/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import currencyForrmater from "currency-formatter";
import { Show } from "@common/show";
import { Clock, Edit } from "react-feather";
import { Col } from "reactstrap";
import { useDispatch } from "react-redux";
import { discountActions } from "../store";
import { DiscountEdit } from "./discount-edit";
import { IDiscountEntity } from "../dtos/discount.entity";

interface ItemProps {
  discount: IDiscountEntity;
  isEdit: boolean;
  onUpdate?: (discount: IDiscountEntity) => void;
}

export const DiscountItem = ({ discount, isEdit, onUpdate }: ItemProps) => {
  const dispatch = useDispatch();
  const [isDialog, setIsDialog] = useState<boolean>(false);

  const displayAmount = () => {
    return currencyForrmater.format(discount.amount, { code: "PEN" });
  };

  const handleEdit = () => {
    if (!isEdit) return;
    dispatch(discountActions.find(discount));
    setIsDialog(true);
  };

  const isAmount = parseFloat(`${discount.amount}`) > 0;

  return (
    <>
      <Col md="3" className="mb-4 text-ellipsis">
        {/* info discount */}
        <label
          className={`capitalize ${isAmount ? "" : "text-muted"}`}
          title={discount?.typeDiscount?.description}
        >
          <div className="text-left">
            <Show
              condition={!!discount.typeDiscount?.airhspId}
              isDefault={<small className="badge badge-danger">SIN CODE</small>}
            >
              <small
                className="badge badge-light"
                title={`Codigo AIRHSP: ${discount.typeDiscount?.airhsp?.name}`}
              >
                {discount.typeDiscount?.airhsp?.code || ""}
              </small>
            </Show>
          </div>
          <>
            <span className="disabled-selection">
              <b className={isAmount ? "text-danger" : "text-muted"}>
                {discount?.typeDiscount?.code}
              </b>
              .- {discount?.typeDiscount?.description}
            </span>
          </>
        </label>
        <h6 onDoubleClick={handleEdit}>
          <Show
            condition={isAmount}
            isDefault={<span className="text-muted">{displayAmount()}</span>}
          >
            {displayAmount()}
          </Show>
          {/* action isEdit */}
          <span
            className="close"
            style={{ marginTop: "-3px" }}
            onClick={handleEdit}
          >
            <Show
              condition={discount.isEdit}
              isDefault={
                <Clock
                  className={
                    isEdit
                      ? "icon cursor-pointer text-danger"
                      : "icon text-muted"
                  }
                />
              }
            >
              <Edit
                className={
                  isEdit ? "icon cursor-pointer text-danger" : "icon text-muted"
                }
              />
            </Show>
          </span>
        </h6>
      </Col>
      {/* dialog edit */}
      <Show condition={isDialog}>
        <DiscountEdit
          isOpen={isDialog}
          onClose={() => setIsDialog(false)}
          onSave={onUpdate}
        />
      </Show>
    </>
  );
};
