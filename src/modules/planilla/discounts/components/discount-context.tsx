import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { useHistorialResume } from "@modules/planilla/historials/hooks/use-historial-resume";
import { createContext, FC, useContext, useState } from "react";
import { discountEntityName } from "../dtos/discount.entity";

const dataDefault = {
  isEdit: true,
  setIsEdit: (value: boolean) => {},
  refreshResume: () => {},
};

export const DiscountContext = createContext(dataDefault);

export const DiscountProvider: FC = ({ children }) => {
  const ability = useContext(CaslContext);

  const canAction = ability.can(PermissionAction.UPDATE, discountEntityName);

  const [isEdit, setIsEdit] = useState<boolean>(canAction);
  const historialResume = useHistorialResume();

  const handleEdit = (value: boolean) => {
    if (!canAction) setIsEdit(false);
    setIsEdit(value);
  };

  const refreshResume = () => {
    historialResume.execute();
  };

  return (
    <DiscountContext.Provider
      value={{
        isEdit,
        setIsEdit: handleEdit,
        refreshResume,
      }}
    >
      {children || null}
    </DiscountContext.Provider>
  );
};
