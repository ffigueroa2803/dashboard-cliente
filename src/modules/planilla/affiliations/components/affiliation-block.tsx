import { Show } from "@common/show";
import { RootState } from "@store/store";
import React, { Fragment, useContext, useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { IAffiliationEntity } from "../dtos/affiliation.entity";
import { affiliationActions } from "../store";
import { AffiliationContext } from "./affiliation-context";
import { AffiliationItem } from "./affiliation-item";

interface IProps {
  loading: boolean;
}

const PlaceholderItems = () => {
  const datos = [1, 2, 3, 4, 5];
  return (
    <>
      {datos.map((index) => (
        <Fragment key={`item-loading-${index}`}>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
        </Fragment>
      ))}
    </>
  );
};

export const AffiliationBlock = ({ loading }: IProps) => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { affiliations } = useSelector((state: RootState) => state.affiliation);

  const { isEdit, setIsEdit, refreshResume } = useContext(AffiliationContext);

  const handleOnDelete = (affiliation: IAffiliationEntity) => {
    dispatch(affiliationActions.removeItem(affiliation.id));
  };

  useEffect(() => {
    if (cronograma?.id) setIsEdit(cronograma.state);
  }, [cronograma]);

  return (
    <Row>
      {/* loading */}
      <Show condition={!loading} isDefault={<PlaceholderItems />}>
        {/* items */}
        {affiliations?.items?.map((aff, index) => (
          <AffiliationItem
            key={`ìtem-${aff.id}-${index}`}
            isEdit={isEdit}
            affiliation={aff}
            onUpdate={refreshResume}
            onDelete={handleOnDelete}
          />
        ))}
      </Show>
    </Row>
  );
};
