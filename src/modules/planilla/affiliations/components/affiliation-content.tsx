import React, { useContext, useEffect, useState } from "react";
import { Edit, Settings, Users, X } from "react-feather";
import { Card, CardBody, CardHeader } from "reactstrap";
import { HistorialResume } from "@modules/planilla/historials/components/historial-resume";
import { AffiliationBlock } from "./affiliation-block";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useHistorialAffiliation } from "@modules/planilla/historials/hooks/use-historial-affiliation";
import { AffiliationContext, AffiliationProvider } from "./affiliation-context";
import { Show } from "@common/show";
import { toast } from "react-toastify";
import { infoActions } from "@modules/planilla/infos/store";
import { InfoTypeAffiliationContent } from "@modules/planilla/info-type-affiliations/components/info-type-affiliation-content";

const WrapperAffiliationContext = () => {
  const dispatch = useDispatch();

  const historialAffiliation = useHistorialAffiliation();

  const { historial } = useSelector((state: RootState) => state.historial);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const { refreshResume } = useContext(AffiliationContext);

  const [isConfig, setIsConfig] = useState<boolean>(false);

  const handleConfig = () => {
    dispatch(infoActions.find(historial?.info || ({} as any)));
    setIsConfig(true);
  };

  const handleSaveConfig = () => {
    toast.dismiss();
    toast.info(`Calculando...`);
    setTimeout(async () => {
      await historialAffiliation.execute();
      refreshResume();
    }, 2000);
  };

  useEffect(() => {
    if (historial?.id) historialAffiliation.execute();
  }, [historial]);

  return (
    <>
      <Card>
        <CardBody>
          <HistorialResume />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Show condition={cronograma.state}>
            <Settings
              className="icon close cursor-pointer ml-3"
              onClick={handleConfig}
            />
          </Show>
          <Users className="icon" /> Afiliación
        </CardHeader>
        <CardBody>
          <AffiliationBlock loading={historialAffiliation.pending} />
        </CardBody>
      </Card>
      {/* config discount */}
      <Show condition={isConfig}>
        <InfoTypeAffiliationContent
          isOpen={isConfig}
          onClose={() => setIsConfig(false)}
          onSave={handleSaveConfig}
          onCreated={handleSaveConfig}
        />
      </Show>
    </>
  );
};

export const AffiliationContent = () => {
  return (
    <AffiliationProvider>
      <WrapperAffiliationContext />
    </AffiliationProvider>
  );
};
