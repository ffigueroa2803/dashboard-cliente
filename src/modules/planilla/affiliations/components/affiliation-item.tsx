import React, { useState } from "react";
import currencyForrmater from "currency-formatter";
import { Show } from "@common/show";
import { Clock } from "react-feather";
import { Col } from "reactstrap";
import { useDispatch } from "react-redux";
import { affiliationActions } from "../store";
import { IAffiliationEntity } from "../dtos/affiliation.entity";
import { AffiliationEdit } from "./affiliation-edit";

interface ItemProps {
  affiliation: IAffiliationEntity;
  isEdit: boolean;
  onUpdate?: (affiliation: IAffiliationEntity) => void;
  onDelete?: (affiliation: IAffiliationEntity) => void;
}

export const AffiliationItem = ({
  affiliation,
  isEdit,
  onUpdate,
  onDelete,
}: ItemProps) => {
  const dispatch = useDispatch();
  const [isDialog, setIsDialog] = useState<boolean>(false);

  const displayAmount = () => {
    return currencyForrmater.format(affiliation.amount, { code: "PEN" });
  };

  const handleEdit = () => {
    if (!isEdit) return;
    dispatch(affiliationActions.find(affiliation));
    setIsDialog(true);
  };

  const isAmount = parseFloat(`${affiliation.amount}`) > 0;

  return (
    <>
      <Col md="3" className="mb-4 text-ellipsis">
        {/* info discount */}
        <label
          className={`capitalize ${isAmount ? "" : "text-muted"}`}
          title={affiliation?.infoTypeAffiliation?.typeAffiliation?.name || ""}
        >
          <>
            <span className="disabled-selection">
              <b className={isAmount ? "text-danger" : "text-muted"}>
                {affiliation?.discount?.typeDiscount?.code}
              </b>
              .-{" "}
              {affiliation?.infoTypeAffiliation?.typeAffiliation?.name || "N/A"}
            </span>
          </>
        </label>
        <h6 onDoubleClick={handleEdit}>
          <Show
            condition={isAmount}
            isDefault={<span className="text-muted">{displayAmount()}</span>}
          >
            {displayAmount()}
          </Show>
          {/* action isEdit */}
          <span
            className="close"
            style={{ marginTop: "-3px" }}
            onClick={handleEdit}
          >
            <Clock
              className={
                isEdit ? "icon cursor-pointer text-danger" : "icon text-muted"
              }
            />
          </span>
        </h6>
      </Col>
      {/* dialog edit */}
      <Show condition={isDialog}>
        <AffiliationEdit
          isOpen={isDialog}
          onClose={() => setIsDialog(false)}
          onSave={onUpdate}
          onDeleted={onDelete}
        />
      </Show>
    </>
  );
};
