import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IFormAffiliationDto } from "../dtos/form-affiliation.dto";
import Toggle from "@atlaskit/toggle";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const AffiliationForm = ({
  form,
  onChange,
}: IProps<IFormAffiliationDto>) => {
  const { affiliation } = useSelector((state: RootState) => state.affiliation);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FormGroup className="mb-3">
          <label>
            Tipo Afiliación <b className="text-danger">*</b>
          </label>
          <h6 className="capitalize">
            {affiliation?.infoTypeAffiliation?.typeAffiliation?.name || "N/A"}
            <hr />
          </h6>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Porcentaje</label>
          <div>
            <Toggle
              name="isPercent"
              isChecked={form.isPercent}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        {/* monto */}
        <FormGroup className="mb-3">
          <label>
            Valor <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({
                  name: target.name,
                  value: parseFloat(target.value),
                })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
