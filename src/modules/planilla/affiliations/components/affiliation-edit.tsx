/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { Save, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { affiliationActions } from "../store";
import { AffiliationForm } from "./affiliation-form";
import { IAffiliationEntity } from "../dtos/affiliation.entity";
import { useAffiliationEdit } from "../hooks/use-affiliation-edit";
import { useAffiliationFind } from "../hooks/use-affiliation-find";
import { useAffiliationDelete } from "../hooks/use-affiliation-delete";
import { toast } from "react-toastify";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (affiliation: IAffiliationEntity) => void;
  onDeleted?: (affiliation: IAffiliationEntity) => void;
}

export const AffiliationEdit = ({
  isOpen,
  onClose,
  onSave,
  onDeleted,
}: IProps) => {
  const dispatch = useDispatch();

  const { affiliation } = useSelector((state: RootState) => state.affiliation);

  const affiliationEdit = useAffiliationEdit();
  const affiliationFind = useAffiliationFind();
  const affiliationDelete = useAffiliationDelete();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await affiliationEdit
      .update()
      .then((data) => {
        setTimeout(async () => {
          toast.info("Calculado...");
          const result = await affiliationFind
            .find(affiliation.id)
            .then((res) => res)
            .catch(() => data);
          // update info type remuneration
          dispatch(affiliationActions.find(result));
          if (typeof onSave == "function") {
            onSave(result);
            onClose();
          }
        }, 1000);
      })
      .catch(() => null);
    setPending(false);
  };

  const handleDelete = () => {
    affiliationDelete
      .destroy()
      .then(async () => {
        if (typeof onDeleted == "function") {
          onDeleted(affiliation);
        }
      })
      .catch(() => null);
  };

  useEffect(() => {
    if (affiliation?.id && isOpen) affiliationEdit.clearForm();
  }, [affiliation, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Afiliación</ModalHeader>
      <ModalBody>
        <AffiliationForm
          isEdit={true}
          form={affiliationEdit.form}
          onChange={affiliationEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button
          color="danger"
          outline
          disabled={pending}
          onClick={handleDelete}>
          <Trash className="icon" />
        </Button>
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
