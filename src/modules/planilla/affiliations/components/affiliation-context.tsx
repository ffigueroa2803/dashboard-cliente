import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { useHistorialResume } from "@modules/planilla/historials/hooks/use-historial-resume";
import { createContext, FC, useContext, useState } from "react";
import { affiliationEntityName } from "../dtos/affiliation.entity";

const dataDefault = {
  isEdit: true,
  setIsEdit: (value: boolean) => {},
  refreshResume: () => {},
};

export const AffiliationContext = createContext(dataDefault);

export const AffiliationProvider: FC = ({ children }) => {
  const ability = useContext(CaslContext);

  const canAction = ability.can(PermissionAction.UPDATE, affiliationEntityName);

  const [isEdit, setIsEdit] = useState<boolean>(canAction);
  const historialResume = useHistorialResume();

  const refreshResume = () => {
    historialResume.execute();
  };

  const handleEdit = (value: boolean) => {
    if (!canAction) return setIsEdit(false);
    setIsEdit(value);
  };

  return (
    <AffiliationContext.Provider
      value={{
        isEdit,
        setIsEdit: handleEdit,
        refreshResume,
      }}
    >
      {children || null}
    </AffiliationContext.Provider>
  );
};
