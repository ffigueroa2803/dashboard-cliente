import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IAffiliationEntity } from "./dtos/affiliation.entity";

export interface AffiliationState {
  affiliations: ResponsePaginateDto<IAffiliationEntity>;
  affiliation: IAffiliationEntity;
  option: string;
}

const initialState: AffiliationState = {
  affiliations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  affiliation: {} as any,
  option: "",
};

const affiliationStore = createSlice({
  name: "planilla@affiliation",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IAffiliationEntity>>
    ) => {
      state.affiliations = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<IAffiliationEntity>) => {
      state.affiliation = payload;
      state.affiliations.items?.map((item) => {
        if (payload.id == item.id) {
          return Object.assign(item, payload);
        }
        // default
        return item;
      });
      return state;
    },
    removeItem: (state, { payload }: PayloadAction<number>) => {
      state.affiliations.items = state.affiliations?.items?.filter(
        (item) => item.id != payload
      );
      // change meta
      state.affiliations.meta.totalItems =
        state.affiliations.meta.totalItems || 0 - 1;
      // response
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.affiliation };
    },
  },
});

export const affiliationReducer = affiliationStore.reducer;

export const affiliationActions = affiliationStore.actions;
