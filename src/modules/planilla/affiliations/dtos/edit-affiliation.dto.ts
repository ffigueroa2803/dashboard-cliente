export interface IEditAffiliationDto {
  isPercent: boolean;
  amount: number;
}
