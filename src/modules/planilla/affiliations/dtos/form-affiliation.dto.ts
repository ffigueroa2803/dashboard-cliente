export interface IFormAffiliationDto {
  isPercent: boolean;
  amount: number;
}
