import { IDiscountEntity } from "@modules/planilla/discounts/dtos/discount.entity";
import { IInfoTypeAffiliationEntity } from "@modules/planilla/info-type-affiliations/dtos/info-type-affiliation.entity";

export interface IAffiliationEntity {
  id: number;
  discountId: number;
  infoTypeAffiliationId: number;
  isPercent: boolean;
  percent: number;
  amount: number;
  discount?: IDiscountEntity;
  infoTypeAffiliation?: IInfoTypeAffiliationEntity;
}

export class AffiliationEntity {
  constructor(partial: Partial<IAffiliationEntity>) {
    Object.assign(this, partial);
  }
}

export const affiliationEntityName = "AffiliationEntity";
