import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

const request = planillaRequest();

export const useAffiliationDelete = () => {
  const { affiliation } = useSelector((state: RootState) => state.affiliation);
  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async () => {
    return await confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en eliminar el regístro?",
        labelSuccess: "Eliminar",
        labelError: "No",
      })
      .then(async () => {
        toast.info(`Eliminando...`);
        setPending(true);
        const result = await request
          .destroy(`affiliations/${affiliation.id}`)
          .then((res) => {
            toast.dismiss();
            toast.success(`Los datos se eliminarón correctamente!`);
            return res.data;
          })
          .catch((err) => {
            toast.dismiss();
            toast.error(`No se pudo eliminar los datos`);
            return err;
          });
        setPending(false);
        return result;
      })
      .catch((err) => {
        throw new Error(err);
      });
  };

  return {
    pending,
    destroy,
  };
};
