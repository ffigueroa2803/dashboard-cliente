import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IAffiliationEntity } from "../dtos/affiliation.entity";
import { toast } from "react-toastify";
import { IEditAffiliationDto } from "../dtos/edit-affiliation.dto";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

export const useAffiliationEdit = () => {
  const { affiliation } = useSelector((state: RootState) => state.affiliation);

  const dataDefault: IEditAffiliationDto = {
    isPercent: affiliation.isPercent || false,
    amount: affiliation.isPercent ? affiliation.percent : affiliation.amount,
  };

  const [form, setForm] = useState<IEditAffiliationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = (): Promise<IAffiliationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      const payload = Object.assign(form, {
        amount: parseFloat(`${form.amount}`),
      });
      // send
      setPending(true);
      await request
        .put(`affiliations/${affiliation.id}`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IAffiliationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  const clearForm = () => {
    setForm(dataDefault);
  };

  return {
    form,
    changeForm,
    clearForm,
    pending,
    update,
  };
};
