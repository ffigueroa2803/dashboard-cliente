import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IAffiliationEntity } from "../dtos/affiliation.entity";

const request = planillaRequest();

export const useAffiliationFind = () => {
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IAffiliationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`affiliations/${id}`)
        .then((res) => {
          resolve(res.data as IAffiliationEntity);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
