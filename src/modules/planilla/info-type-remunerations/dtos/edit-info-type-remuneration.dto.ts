export interface IEditInfoTypeRemunerationDto {
  typeRemunerationId: number;
  infoId: number;
  pimId?: number | null;
  amount: number;
  isBase: boolean;
}
