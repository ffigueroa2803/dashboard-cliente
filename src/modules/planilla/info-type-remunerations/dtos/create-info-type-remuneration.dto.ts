export interface ICreateInfoTypeRemunerationDto {
  typeRemunerationId: number;
  infoId: number;
  pimId?: number;
  amount: number;
  isBase: boolean;
}
