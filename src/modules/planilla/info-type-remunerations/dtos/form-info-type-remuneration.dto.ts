export interface IFormInfoTypeRemunerationDto {
  typeRemunerationId: number;
  infoId: number;
  pimId?: number;
  amount: number;
  isBase: boolean;
}
