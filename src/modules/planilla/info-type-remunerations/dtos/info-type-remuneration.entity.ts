import { IPimEntity } from "@modules/planilla/pims/dtos/pim.entity";
import { TypeRemuneration } from "@modules/planilla/type_remuneration/dtos/type_remuneration.entity";

export interface IInfoTypeRemunerationEntity {
  id: number;
  typeRemunerationId: number;
  infoId: number;
  pimId?: number;
  amount: number;
  isBase: boolean;
  typeRemuneration?: TypeRemuneration;
  pim?: IPimEntity;
}
