import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IInfoTypeRemunerationEntity } from "./dtos/info-type-remuneration.entity";

export interface InfoTypeRemunerationState {
  infoTypeRemunerations: ResponsePaginateDto<IInfoTypeRemunerationEntity>;
  infoTypeRemuneration: IInfoTypeRemunerationEntity;
  option: string;
}

const initialState: InfoTypeRemunerationState = {
  infoTypeRemunerations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  infoTypeRemuneration: {} as any,
  option: "",
};

const infoTypeRemunerationStore = createSlice({
  name: "planilla@infoTypeRemuneration",
  initialState,
  reducers: {
    paginate: (
      state,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IInfoTypeRemunerationEntity>>
    ) => {
      state.infoTypeRemunerations = payload as any;
    },
    find: (state, { payload }: PayloadAction<IInfoTypeRemunerationEntity>) => {
      state.infoTypeRemuneration = payload as any;
    },
    updateItem: (
      state,
      { payload }: PayloadAction<IInfoTypeRemunerationEntity>
    ) => {
      state.infoTypeRemunerations?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign({}, payload);
        return item;
      });
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.infoTypeRemuneration };
    },
  },
});

export const infoTypeRemunerationReducer = infoTypeRemunerationStore.reducer;

export const infoTypeRemunerationActions = infoTypeRemunerationStore.actions;
