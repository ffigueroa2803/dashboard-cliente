import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { Search, Edit, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import currencyFormatter from "currency-formatter";
import { useInfoTypeRemunerationDelete } from "../hooks/use-info-type.remuneration-delete";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (querySearch: string | string[]) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: IInfoTypeRemunerationEntity) => any;
}

interface IProps {
  data: IInfoTypeRemunerationEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: (infoTypeRemuneration: IInfoTypeRemunerationEntity) => void;
  onDelete?: (infoTypeRemuneration: IInfoTypeRemunerationEntity) => void;
}

export const InfoTypeRemunerationTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onDelete,
  options,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const infoDelete = useInfoTypeRemunerationDelete();

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IInfoTypeRemunerationEntity) => row.id,
      },
      {
        name: "Código",
        grow: true,
        center: true,
        selector: (row: IInfoTypeRemunerationEntity) => (
          <span className="badge badge-dark">
            {row?.typeRemuneration?.code}
          </span>
        ),
      },
      {
        name: "Descripción",
        wrap: true,
        selector: (row: IInfoTypeRemunerationEntity) => {
          return <span>{row?.typeRemuneration?.name}</span>;
        },
      },
      {
        name: "PIM",
        wrap: true,
        selector: (row: IInfoTypeRemunerationEntity) => (
          <Show condition={(row?.pimId || 0) > 0} isDefault={<span>N/A</span>}>
            <span>
              {row?.pim?.code} [{row?.pim?.cargo?.extension}]
            </span>
          </Show>
        ),
      },
      {
        name: "Base",
        selector: (row: IInfoTypeRemunerationEntity) => (
          <span>{row.isBase ? "SI" : "NO"}</span>
        ),
      },
      {
        name: "Monto",
        selector: (row: IInfoTypeRemunerationEntity) => (
          <Show
            condition={parseFloat(`${row.amount}`) > 0}
            isDefault={
              <span>
                {currencyFormatter.format(row.amount, { code: "PEN" })}
              </span>
            }
          >
            <b className="text-primary">
              {currencyFormatter.format(row.amount, { code: "PEN" })}
            </b>
          </Show>
        ),
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: IInfoTypeRemunerationEntity) => (
          <>
            <Edit
              className="cursor-pointer icon"
              onClick={() => handleClick(row)}
            />
            <Trash
              className="cursor-pointer icon"
              onClick={() => handleDelete(row)}
            />
          </>
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (infoTypeRemuneration: IInfoTypeRemunerationEntity) => {
    if (typeof onClick == "function") onClick(infoTypeRemuneration);
  };

  const handleDelete = (infoTypeRemuneration: IInfoTypeRemunerationEntity) => {
    infoDelete
      .destroy(infoTypeRemuneration.id)
      .then(() => {
        if (typeof onDelete == "function") {
          onDelete(infoTypeRemuneration);
        }
      })
      .catch((err) => null);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
