/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeRemunerationForm } from "./info-type-remuneration-form";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { useInfotypeRemunerationForm } from "../hooks/use-info-type-remuneration-form";
import { useInfoTypeRemunerationCreate } from "../hooks/use-info-type.remuneration-create";
import { RootState } from "@store/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeRemunerationEntity) => void;
}

export const InfoTypeRemunerationCreate = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const { info } = useSelector((state: RootState) => state.info);

  const infoForm = useInfotypeRemunerationForm();
  const infoCreate = useInfoTypeRemunerationCreate();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // payload
    const payload = Object.assign(infoForm.form, {
      infoId: info.id,
    });
    // send create contract
    await infoCreate
      .save(payload)
      .then((data) => {
        infoForm.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => null);
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Nueva Configuración Remunerativa
      </ModalHeader>
      <ModalBody>
        <InfoTypeRemunerationForm
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
