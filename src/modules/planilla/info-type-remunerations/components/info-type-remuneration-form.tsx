import { IInputHandle } from "@common/dtos/input-handle";
import React, { useEffect, useState } from "react";
import { Form, FormGroup, Input } from "reactstrap";
import Toggle from "@atlaskit/toggle";
import { PimSelect } from "@modules/planilla/pims/components/pim-select";
import { DateTime } from "luxon";
import { IFormInfoTypeRemunerationDto } from "../dtos/form-info-type-remuneration.dto";
import { TypeRemunerationSelect } from "@modules/planilla/type_remuneration/components/type-remuneration-select";

interface IProps<T> {
  form: T;
  yearDefault?: number;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

const currentDate = DateTime.now();

export const InfoTypeRemunerationForm = ({
  form,
  yearDefault,
  isEdit,
  onChange,
}: IProps<IFormInfoTypeRemunerationDto>) => {
  const [year, setYear] = useState<number>(yearDefault || currentDate.year);

  useEffect(() => {
    if (yearDefault) setYear(yearDefault);
  }, [yearDefault]);

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Remuneración <b className="text-danger">*</b>
          </label>
          <div>
            <TypeRemunerationSelect
              isDisabled={isEdit}
              name="typeRemunerationId"
              value={form?.typeRemunerationId}
              onChange={onChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Año del Pim</label>
          <Input
            type="number"
            value={year}
            onChange={({ target }) => setYear(parseInt(`${target.value}`))}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Pim</label>
          <div>
            <PimSelect
              year={year}
              name="pimId"
              value={form?.pimId || 0}
              onChange={onChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>¿Aplica a la base imponible?</label>
          <div>
            <Toggle
              name="isBase"
              isChecked={form?.isBase || false}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({ name: target.name, value: parseFloat(target.value) })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
