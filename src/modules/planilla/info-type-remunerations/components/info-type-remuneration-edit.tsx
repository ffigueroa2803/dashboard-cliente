/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeRemunerationForm } from "./info-type-remuneration-form";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { useInfotypeRemunerationForm } from "../hooks/use-info-type-remuneration-form";
import { RootState } from "@store/store";
import { useInfoTypeRemunerationEdit } from "../hooks/use-info-type.remuneration-edit";
import { useInfoTypeRemunerationFind } from "../hooks/use-info-type.remuneration-find";
import { infoTypeRemunerationActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeRemunerationEntity) => void;
}

export const InfoTypeRemunerationEdit = ({
  isOpen,
  onClose,
  onSave,
}: IProps) => {
  const dispatch = useDispatch();

  const { infoTypeRemuneration } = useSelector(
    (state: RootState) => state.infoTypeRemuneration
  );

  const infoForm = useInfotypeRemunerationForm();
  const infoEdit = useInfoTypeRemunerationEdit();
  const infoFind = useInfoTypeRemunerationFind();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await infoEdit
      .update(infoTypeRemuneration.id, infoForm.form)
      .then(async (data) => {
        const result = await infoFind
          .find(infoTypeRemuneration.id)
          .then((res) => res)
          .catch(() => data);
        // update info type remuneration
        dispatch(infoTypeRemunerationActions.find(result));
        if (typeof onSave == "function") onSave(result);
      })
      .catch(() => null);
    setPending(false);
  };

  useEffect(() => {
    if (infoTypeRemuneration?.id && isOpen)
      infoForm.setForm(infoTypeRemuneration);
  }, [infoTypeRemuneration, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Configuración Remunerativa
      </ModalHeader>
      <ModalBody>
        <InfoTypeRemunerationForm
          isEdit={true}
          yearDefault={infoTypeRemuneration?.pim?.year || 0}
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
