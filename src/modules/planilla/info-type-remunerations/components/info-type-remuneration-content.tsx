import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { Plus, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { useInfoTypeRemunerationsToInfo } from "../hooks/use-info-type.remunerations-to-info";
import { infoTypeRemunerationActions } from "../store";
import { InfoTypeRemunerationCreate } from "./info-type-remuneration-create";
import { InfoTypeRemunerationEdit } from "./info-type-remuneration-edit";
import { InfoTypeRemunerationTable } from "./info-type-remuneration-table";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onCreated?: (infoTypeRemuneration: IInfoTypeRemunerationEntity) => void;
  onSave?: (infoTypeRemuneration: IInfoTypeRemunerationEntity) => void;
}

export const InfoTypeRemunerationContent = ({
  isOpen,
  onClose,
  onCreated,
  onSave,
}: IProps) => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const { infoTypeRemunerations, option } = useSelector(
    (state: RootState) => state.infoTypeRemuneration
  );

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(30);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const infoTypeRemuneration = useInfoTypeRemunerationsToInfo(info.id || 0, {
    page,
    limit,
  });

  const handlePage = (newPage: number) => {
    setPage(newPage);
    setIsRefresh(true);
  };

  const handleRowPer = (newRowPer: number, currentPage: number) => {
    setLimit(newRowPer);
    setPage(currentPage);
    setIsRefresh(true);
  };

  const handleCreated = (
    createdInfoTypeRemuneration: IInfoTypeRemunerationEntity
  ) => {
    dispatch(infoTypeRemunerationActions.changeOption("REFRESH"));
    // events
    if (typeof onCreated == "function") {
      onCreated(createdInfoTypeRemuneration);
    }
  };

  const handleEdit = (infoTypeRemuneration: IInfoTypeRemunerationEntity) => {
    dispatch(infoTypeRemunerationActions.find(infoTypeRemuneration));
    dispatch(infoTypeRemunerationActions.changeOption("EDIT"));
  };

  const handleDeleted = () => {
    dispatch(infoTypeRemunerationActions.changeOption("REFRESH"));
  };

  const handleUpdated = (
    updatedInfoTypeRemuneration: IInfoTypeRemunerationEntity
  ) => {
    dispatch(
      infoTypeRemunerationActions.updateItem(updatedInfoTypeRemuneration)
    );
    dispatch(infoTypeRemunerationActions.changeOption("REFRESH"));
    // events
    if (typeof onSave == "function") {
      onSave(updatedInfoTypeRemuneration);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setPage(1);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && info?.id) setIsRefresh(true);
  }, [info, isOpen]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(infoTypeRemunerationActions.changeOption(""));
    }
  }, [option]);

  useEffect(() => {
    if (isRefresh) infoTypeRemuneration.execute();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (infoTypeRemuneration.datos) {
      dispatch(
        infoTypeRemunerationActions.paginate(infoTypeRemuneration.datos)
      );
    }
  }, [infoTypeRemuneration.datos]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        style={{ minWidth: "90vh" }}
        backdrop={!["CREATE", "EDIT"].includes(option)}
      >
        <ModalHeader toggle={onClose}>
          <Settings className="icon" /> Configurarción Remunerativa
        </ModalHeader>
        <ModalBody>
          <InfoTypeRemunerationTable
            data={infoTypeRemunerations?.items || []}
            perPage={infoTypeRemunerations?.meta?.itemsPerPage}
            totalItems={infoTypeRemunerations?.meta?.totalItems}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleRowPer}
            onDelete={handleDeleted}
            onClick={handleEdit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            outline
            onClick={() =>
              dispatch(infoTypeRemunerationActions.changeOption("CREATE"))
            }
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* create */}
      <InfoTypeRemunerationCreate
        isOpen={option == "CREATE"}
        onClose={() => dispatch(infoTypeRemunerationActions.changeOption(""))}
        onSave={handleCreated}
      />
      {/* edit */}
      <InfoTypeRemunerationEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(infoTypeRemunerationActions.changeOption(""))}
        onSave={handleUpdated}
      />
    </>
  );
};
