import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { PaginateDto } from "@services/dtos";
import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";

const request = planillaRequest();

export const useInfoTypeRemunerationsToInfo = (
  id: number,
  { page, limit, querySearch }: PaginateDto
) => {
  const datosDefault: ResponsePaginateDto<IInfoTypeRemunerationEntity> = {
    items: [],
    meta: {
      itemsPerPage: limit || 30,
      totalItems: 0,
      totalPages: 0,
    },
  };

  const [pending, setPending] = useState<boolean>(false);
  const [datos, setDatos] =
    useState<ResponsePaginateDto<IInfoTypeRemunerationEntity>>(datosDefault);

  const execute = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("limit", `${limit}`);
    params.set("querySearch", querySearch || "");
    await request
      .get(`infos/${id}/typeRemunerations`, { params })
      .then((res) => setDatos(res.data))
      .catch(() => setDatos(datosDefault));
    setPending(false);
  };

  return {
    pending,
    execute,
    datos,
  };
};
