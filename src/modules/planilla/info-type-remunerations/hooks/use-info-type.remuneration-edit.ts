import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { toast } from "react-toastify";
import { IEditInfoTypeRemunerationDto } from "../dtos/edit-info-type-remuneration.dto";

const request = planillaRequest();

export const useInfoTypeRemunerationEdit = () => {
  const [pending, setPending] = useState<boolean>(false);

  const update = (
    id: number,
    payload: IEditInfoTypeRemunerationDto
  ): Promise<IInfoTypeRemunerationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      setPending(true);
      await request
        .put(`infoTypeRemunerations/${id}`, {
          ...payload,
          amount: parseFloat(`${payload.amount}`),
          pimId: payload.pimId || null,
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IInfoTypeRemunerationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    update,
  };
};
