import { IInputHandle } from "@common/dtos/input-handle";
import { useState } from "react";
import { IFormInfoTypeRemunerationDto } from "../dtos/form-info-type-remuneration.dto";

const dataDefaultCurrent: IFormInfoTypeRemunerationDto = {
  typeRemunerationId: 0,
  infoId: 0,
  amount: 0,
  isBase: false,
};

export const useInfotypeRemunerationForm = (
  dataDefault: IFormInfoTypeRemunerationDto = dataDefaultCurrent
) => {
  const [form, setForm] = useState<IFormInfoTypeRemunerationDto>(dataDefault);
  const [errors, setErrors] = useState<any>({});

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(dataDefault);

  return {
    dataDefault,
    form,
    setForm,
    errors,
    handleForm,
    clearForm,
  };
};
