import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";

const request = planillaRequest();

export const useInfoTypeRemunerationFind = () => {
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IInfoTypeRemunerationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`infoTypeRemunerations/${id}`)
        .then((res) => {
          resolve(res.data as IInfoTypeRemunerationEntity);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
