import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeRemunerationEntity } from "../dtos/info-type-remuneration.entity";
import { ICreateInfoTypeRemunerationDto } from "../dtos/create-info-type-remuneration.dto";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useInfoTypeRemunerationCreate = () => {
  const [pending, setPending] = useState<boolean>(false);

  const save = (
    payload: ICreateInfoTypeRemunerationDto
  ): Promise<IInfoTypeRemunerationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`infoTypeRemunerations`, payload)
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data as IInfoTypeRemunerationEntity);
        })
        .catch((err) => {
          toast.error(`No se pudo guardar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    save,
  };
};
