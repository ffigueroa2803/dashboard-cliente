import React, { useState } from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Button,
  FormGroup,
  Input,
} from "reactstrap";
import { TypeSindicato } from "@modules/planilla/type_sindicato/dtos/type_sindicato.entity";
import Toggle from "@atlaskit/toggle";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { Save, ArrowLeft, User } from "react-feather";
import { InputDto } from "@services/dtos";
import { updateTypeSindicato } from "@modules/planilla/type_sindicato/apis";
import { toast } from "react-toastify";
import { SelectBasic } from "@common/select/select-basic";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";


interface IProps {
  onClose: () => void;
  onSave: (typeSindicato: TypeSindicato) => void;
}

const optionsModo = [
  { label: "Porcentaje", value: "01" },
  { label: "Modo Estático", value: "02" },
];


export const SindicatoEdit = ({ onClose, onSave }: IProps) => {
  const { type_sindicato } = useSelector(
    (state: RootState) => state.type_sindicato
  );
  const [form, setForm] = useState<TypeSindicato>(type_sindicato);
  const [pending, setPending] = useState<boolean>(false);

  const handleOnChange = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setPending(true);
    await updateTypeSindicato(type_sindicato.id, form)
      .then((data) => {
        toast.success("Los datos se guardaron correctamente");
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error("Ocrruio un error al actulizar los datos"));
    setPending(false);
    onClose();
  };
  const ComponenteEditType_Remuneration = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            name="name"
            className="capitalize"
            value={form?.name || ""}
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>
            Modo <b className="text-danger">*</b>
          </label>
          <Toggle
            name="isPercent"
            isChecked={form?.isPercent || false}
            onChange={({ target }) =>
              handleOnChange({
                name: target.name,
                value: target.checked,
              })
            }
          />
        </FormGroup>

        <FormGroup>
          <label>{form.isPercent ? "Porcentaje" : "Monto"}</label>
          <Input
            name={form.isPercent ? "percent" : "amount"}
            value={form.isPercent ? form?.percent || "" : form?.amount || ""}
            type="text"
            className="capitalize"
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>Descuento</label>
          optionDscto
          <TypeDiscountSelect
            name="typeDiscountId"
            value={form?.typeDiscountId}
            onChange={(obj) => handleOnChange(obj)}
          />
        </FormGroup>

        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} />
          </Button>
        </Col>
      </Row>
    </div>
  );

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Editar Tip. Sindicato</ModalHeader>
      <ModalBody>
        {/* <ProgressIndicator
        selectedIndex={currentStep}
        values={steps}
        appearance={'primary'}
      /> */}
        {/* {currentStep == 0 ? ComponentSearch : null} */}
        {/* {currentStep == 1 ? ComponentCreateWork : null} */}
        {/* {ComponentCreateType_Remuneration} */}
        {ComponenteEditType_Remuneration}
      </ModalBody>
    </Modal>
  );
};
