import React, { useState } from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  Button,
  FormGroup,
  Input,
} from "reactstrap";
import { InputDto } from "@services/dtos";
import { ICreateTypeSindicatonDto } from "../dtos/create-type_sindicato.dto";
import { Save, ArrowLeft, User } from "react-feather";
import Toggle from "@atlaskit/toggle";
import { TypeSindicato } from "@modules/planilla/type_sindicato/dtos/type_sindicato.entity";
import { createTypeSindicato } from "../apis";
import { toast } from "react-toastify";
import { SelectBasic } from "@common/select/select-basic";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";

interface IProps {
  onClose: () => void;
  // eslint-disable-next-line no-unused-vars
  onSave: (typesindicato: TypeSindicato) => void;
}

const defaultSindicato: ICreateTypeSindicatonDto = {
  name: "",
  isPercent: false,
  // amount: '',
  // percent: '',
  typeDiscountId: 0,
};

export const SindicatoCreate = ({ onClose, onSave }: IProps) => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ICreateTypeSindicatonDto>(defaultSindicato);

  const optionDscto = [
    {
      key: "select-type_detalle-id-type_descuento_id_1_n2dw7hwwhz",
      value: 1,
      label: "30 - +VIDA SEG. ACCIDENTES",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_2_qi37adn4cb",
      value: 2,
      label: "31 - S.N.P (RET)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_3_quzynxoxqp",
      value: 3,
      label: "32 - AFP APORTE (10%)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_4_x5sgrocjfp",
      value: 4,
      label: "33 - COM. FIJA (AFP)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_5_jfdnn95l5n",
      value: 5,
      label: "34 - COM. VARIABLE (AFP)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_6_qah1zsao6g",
      value: 6,
      label: "35 - COM. SEGURO (AFP)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_7_bk73y27c8k",
      value: 7,
      label: "36 - FINAN. CONFIANZA SA",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_8_cl2kl25siv",
      value: 8,
      label: "37 - DSCTO JUDICIAL",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_9_4oi7akkayp",
      value: 9,
      label: "38 - PAGOS INDEBIDOS",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_10_fv0brxk55z",
      value: 10,
      label: "39 - DSTOS. VARIOS       ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_11_uega0ziw0f",
      value: 11,
      label: "40 - CAFAE",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_12_jeizzyaypo",
      value: 12,
      label: "41 - COOPERATIVA ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_13_fdoduf64m1",
      value: 13,
      label: "42 - BANCO RIPLEY DEL P. ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_14_0z2v031eh6",
      value: 14,
      label: "43 - ESSALUD (RET)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_15_nopcpvmm2d",
      value: 15,
      label: "44 - BANCO PICHINCHA      ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_16_9oah9y65r5",
      value: 16,
      label: "45 - TASA EDUC. ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_17_8uf6jw4c3g",
      value: 17,
      label: "46 - RIMAC INTER",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_18_xkliv8ayex",
      value: 18,
      label: "47 - SUTUNU DSTOS A      ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_19_93d01ghpwz",
      value: 19,
      label: "48 - CLUB UNU",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_20_7mvydzf736",
      value: 20,
      label: "49 - IMP. RENTA  ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_1_hqia91cswx",
      value: 21,
      label: "50 - CAJA AREQUIPA       ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_2_b5zhrk4k5j",
      value: 22,
      label: "51 - SIDUNU 1%",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_3_h25i7vjng5",
      value: 23,
      label: "52 - CAJA HUANCAYO       ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_4_rpc2ifgw41",
      value: 24,
      label: "53 - DLFP (RET)",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_5_3k7r6vccww",
      value: 25,
      label: "54 - SUD UNU             ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_6_02w6nbust3",
      value: 26,
      label: "55 - SUTUNU 1%           ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_7_0o5hhdshjo",
      value: 27,
      label: "56 - PRESTAMO CAFAE",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_8_z1f43tol2u",
      value: 28,
      label: "57 - SCOTIANBANK         ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_9_03xcc65lj6",
      value: 29,
      label: "58 - CAJA MAYNAS         ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_10_h6iacp9u2q",
      value: 30,
      label: "59 - ESC. MAESTRIA       ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_11_1vbl6bdxst",
      value: 31,
      label: "60 - INTERBANK           ",
    },
    {
      key: "select-type_detalle-id-type_descuento_id_13_qb1vqlwhnp",
      value: 37,
      label: "62 - C. SAN FRANCISCO",
    },
  ];

  const handleOnChange = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    setPending(true);
    await createTypeSindicato(form as any)
      .then((data) => {
        setForm(defaultSindicato);
        toast.success(`Los datos se guardarón correctamente!`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`Ocurrio un error al guardar los datos`));
    setPending(false);
    onClose();
  };

  const ComponentCreateType_Sindicato = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            name="name"
            className="capitalize"
            value={form?.name || ""}
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>
            Modo<b className="text-danger">*</b>
          </label>
          <Toggle
            name="isPercent"
            isChecked={form?.isPercent || false}
            onChange={({ target }) =>
              handleOnChange({
                name: target.name,
                value: target.checked,
              })
            }
          />
        </FormGroup>

        <FormGroup>
          <label>{form.isPercent ? "Porcentaje" : "Monto"}</label>
          <Input
            name={form.isPercent ? "percent" : "amount"}
            value={form.isPercent ? form?.percent || "" : form?.amount || ""}
            type="text"
            className="capitalize"
            onChange={({ target }) => handleOnChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>Descuento</label>

          <TypeDiscountSelect
            name="typeDiscountId"
            value={form?.typeDiscountId}
            onChange={(obj) => handleOnChange(obj)}
          />
        </FormGroup>

        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} />
          </Button>
        </Col>
      </Row>
    </div>
  );

  // const ComponentSearch = (
  //   <div className='mt-5'>
  //     <label><User size={15} /> Validar Persona</label>
  //     <PersonSearchSelect
  //       onAdd={handleAdd}
  //     />
  //   </div>
  // )

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Sindicato</ModalHeader>
      <ModalBody>
        {/* <ProgressIndicator
          selectedIndex={currentStep}
          values={steps}
          appearance={'primary'}
        /> */}
        {/* {currentStep == 0 ? ComponentSearch : null} */}
        {/* {currentStep == 1 ? ComponentCreateWork : null} */}
        {ComponentCreateType_Sindicato}
      </ModalBody>
    </Modal>
  );
};
