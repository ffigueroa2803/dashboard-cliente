export interface TypeSindicato {
  id: number;
  name?: string;
  amount?: string;
  percent?: string;
  isPercent: boolean;
  state: boolean;
  typeDiscountId: number;
}
