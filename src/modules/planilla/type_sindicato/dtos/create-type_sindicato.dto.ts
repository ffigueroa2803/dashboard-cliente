import { TypeDescuento } from '@modules/planilla/type_descuento/dtos/type_descuento.enitity'
export interface ICreateTypeSindicatonDto {
  name?: string,
  isPercent: boolean,
  amount?: string,
  percent?: string,
  typeDiscountId: number
}
