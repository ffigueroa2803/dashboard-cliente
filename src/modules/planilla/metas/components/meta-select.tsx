/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { SelectBasic } from "@common/select/select-basic";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useMetaList } from "../hooks/use-meta-list";
import { IMetaEntity } from "../dtos/meta.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const MetaSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const metaList = useMetaList();

  const { metas } = useSelector((state: RootState) => state.meta);

  const [isFetch, setIsFetch] = useState<boolean>(false);

  const settingsData = (row: IMetaEntity) => {
    return {
      label: `${row.name} [${row.actividadId}]`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  const handleSearch = (value: string) => {
    if (!value) return;
    metaList.changePim({ name: "querySearch", value: value });
    setIsFetch(true);
  };

  useEffect(() => {
    metaList.changePim({ value: defaultQuerySearch, name: "querySearch" });
    setIsFetch(true);
  }, [defaultQuerySearch]);

  useEffect(() => {
    if (isFetch) metaList.fetch().catch(() => null);
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={metas?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={handleSearch}
    />
  );
};
