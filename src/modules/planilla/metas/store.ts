import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IMetaEntity } from "./dtos/meta.entity";

export interface MetaState {
  metas: ResponsePaginateDto<IMetaEntity>;
  meta: IMetaEntity;
}

export const initialState: MetaState = {
  metas: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  meta: {} as any,
};

const metaStore = createSlice({
  name: "planilla@metas",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IMetaEntity>>
    ) => {
      state.metas = payload;
      return state;
    },
    updateItem: (state, { payload }: PayloadAction<IMetaEntity>) => {
      state.metas?.items?.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
      // response
      return state;
    },
    find: (state, { payload }: PayloadAction<IMetaEntity>) => {
      state.meta = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.meta };
    },
  },
});

export const metaReducer = metaStore.reducer;

export const metaActions = metaStore.actions;
