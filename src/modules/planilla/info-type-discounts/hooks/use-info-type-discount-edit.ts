import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { toast } from "react-toastify";
import { IEditInfoTypeDiscounDto } from "../dtos/edit-info-type-discount.dto";

const request = planillaRequest();

export const useInfoTypeDiscountEdit = () => {
  const [pending, setPending] = useState<boolean>(false);

  const update = (
    id: number,
    payload: IEditInfoTypeDiscounDto
  ): Promise<IInfoTypeDiscountEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      setPending(true);
      await request
        .put(`infoTypeDiscounts/${id}`, {
          ...payload,
          amount: parseFloat(`${payload.amount}`),
        })
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IInfoTypeDiscountEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    update,
  };
};
