import { IInputHandle } from "@common/dtos/input-handle";
import { useState } from "react";
import { IFormInfoTypeDiscountDto } from "../dtos/form-info-type-discount.dto";

const dataDefaultCurrent: IFormInfoTypeDiscountDto = {
  typeDiscountId: 0,
  infoId: 0,
  amount: 0,
};

export const useInfotypeDiscountForm = (
  dataDefault: IFormInfoTypeDiscountDto = dataDefaultCurrent
) => {
  const [form, setForm] = useState<IFormInfoTypeDiscountDto>(dataDefault);
  const [errors, setErrors] = useState<any>({});

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => setForm(dataDefault);

  return {
    dataDefault,
    form,
    setForm,
    errors,
    handleForm,
    clearForm,
  };
};
