import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { ICreateInfoTypeDiscountDto } from "../dtos/create-info-type-discount.dto";
import { toast } from "react-toastify";

const request = planillaRequest();

export const useInfoTypeDiscountCreate = () => {
  const [pending, setPending] = useState<boolean>(false);

  const save = (
    payload: ICreateInfoTypeDiscountDto
  ): Promise<IInfoTypeDiscountEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`infoTypeDiscounts`, payload)
        .then((res) => {
          toast.success(`Los datos se guardarón correctamente!`);
          resolve(res.data as IInfoTypeDiscountEntity);
        })
        .catch((err) => {
          toast.error(`No se pudo guardar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    save,
  };
};
