import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";

const request = planillaRequest();

export const useInfoTypeDiscountFind = () => {
  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IInfoTypeDiscountEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`infoTypeDiscounts/${id}`)
        .then((res) => {
          resolve(res.data as IInfoTypeDiscountEntity);
        })
        .catch((err) => {
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
