import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IInfoTypeDiscountEntity } from "./dtos/info-type-discount.entity";

export interface InfoTypeDiscountState {
  infoTypeDiscounts: ResponsePaginateDto<IInfoTypeDiscountEntity>;
  infoTypeDiscount: IInfoTypeDiscountEntity;
  option: string;
}

const initialState: InfoTypeDiscountState = {
  infoTypeDiscounts: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  infoTypeDiscount: {} as any,
  option: "",
};

const InfoTypeDiscountState = createSlice({
  name: "planilla@infoTypeDiscount",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IInfoTypeDiscountEntity>>
    ) => {
      state.infoTypeDiscounts = payload as any;
    },
    find: (state, { payload }: PayloadAction<IInfoTypeDiscountEntity>) => {
      state.infoTypeDiscount = payload as any;
    },
    updateItem: (
      state,
      { payload }: PayloadAction<IInfoTypeDiscountEntity>
    ) => {
      state.infoTypeDiscounts?.items?.map((item) => {
        if (item.id == payload.id) {
          return Object.assign(item, payload);
        }
        return item;
      });
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.infoTypeDiscount };
    },
  },
});

export const infoTypeDiscountReducer = InfoTypeDiscountState.reducer;

export const infoTypeDiscountActions = InfoTypeDiscountState.actions;
