/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeDiscountForm } from "./info-type-discount-form";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { useInfotypeDiscountForm } from "../hooks/use-info-type-discount-form";
import { RootState } from "@store/store";
import { useInfoTypeDiscountEdit } from "../hooks/use-info-type-discount-edit";
import { useInfoTypeDiscountFind } from "../hooks/use-info-type-discount-find";
import { infoTypeDiscountActions } from "../store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeDiscountEntity) => void;
}

export const InfoTypeDiscountEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { infoTypeDiscount } = useSelector(
    (state: RootState) => state.infoTypeDiscount
  );

  const infoForm = useInfotypeDiscountForm();
  const infoEdit = useInfoTypeDiscountEdit();
  const infoFind = useInfoTypeDiscountFind();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await infoEdit
      .update(infoTypeDiscount.id, infoForm.form)
      .then(async (data) => {
        const result = await infoFind
          .find(infoTypeDiscount.id)
          .then((res) => res)
          .catch(() => data);
        // update info type remuneration
        dispatch(infoTypeDiscountActions.find(result));
        if (typeof onSave == "function") onSave(result);
      })
      .catch(() => null);
    setPending(false);
  };

  useEffect(() => {
    if (infoTypeDiscount?.id && isOpen) infoForm.setForm(infoTypeDiscount);
  }, [infoTypeDiscount, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Configuración Remunerativa
      </ModalHeader>
      <ModalBody>
        <InfoTypeDiscountForm
          isEdit={true}
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
