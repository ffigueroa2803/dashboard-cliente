import { RootState } from "@store/store";
import React, { useEffect, useState } from "react";
import { Plus, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { useInfoTypeDiscountsToInfo } from "../hooks/use-info-type-discounts-to-info";
import { infoTypeDiscountActions } from "../store";
import { InfoTypeDiscountCreate } from "./info-type-discount-create";
import { InfoTypeDiscountEdit } from "./info-type-discount-edit";
import { InfoTypeDiscountTable } from "./info-type-discount-table";

interface IProps {
  isOpen: boolean;
  onClose?: () => void;
  onSave?: (discount: IInfoTypeDiscountEntity) => void;
  onCreated?: (discount: IInfoTypeDiscountEntity) => void;
}

export const InfoTypeDiscountContent = ({
  isOpen,
  onClose,
  onSave,
  onCreated,
}: IProps) => {
  const dispatch = useDispatch();

  const { info } = useSelector((state: RootState) => state.info);
  const { infoTypeDiscounts, option } = useSelector(
    (state: RootState) => state.infoTypeDiscount
  );

  const [page, setPage] = useState<number>(1);
  const [limit, setLimit] = useState<number>(30);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const infoTypeDiscount = useInfoTypeDiscountsToInfo(info.id || 0, {
    page,
    limit,
  });

  const handlePage = (newPage: number) => {
    setPage(newPage);
    setIsRefresh(true);
  };

  const handleRowPer = (newRowPer: number, currentPage: number) => {
    setLimit(newRowPer);
    setPage(currentPage);
    setIsRefresh(true);
  };

  const handleCreated = (createdInfoTypeDiscount: IInfoTypeDiscountEntity) => {
    dispatch(infoTypeDiscountActions.changeOption("REFRESH"));
    // events
    if (typeof onCreated == "function") {
      onCreated(createdInfoTypeDiscount);
    }
  };

  const handleEdit = (infoTypeDiscount: IInfoTypeDiscountEntity) => {
    dispatch(infoTypeDiscountActions.find(infoTypeDiscount));
    dispatch(infoTypeDiscountActions.changeOption("EDIT"));
  };

  const handleDeleted = () => {
    dispatch(infoTypeDiscountActions.changeOption("REFRESH"));
  };

  const handleUpdated = (updatedInfoTypeDiscount: IInfoTypeDiscountEntity) => {
    dispatch(infoTypeDiscountActions.updateItem(updatedInfoTypeDiscount));
    dispatch(infoTypeDiscountActions.changeOption("REFRESH"));
    // events
    if (typeof onSave == "function") {
      onSave(updatedInfoTypeDiscount);
    }
  };

  useEffect(() => {
    if (isOpen) {
      setPage(1);
    }
  }, [isOpen]);

  useEffect(() => {
    if (isOpen && info?.id) setIsRefresh(true);
  }, [info, isOpen]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(infoTypeDiscountActions.changeOption(""));
    }
  }, [option]);

  useEffect(() => {
    if (isRefresh) infoTypeDiscount.execute();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (infoTypeDiscount.datos) {
      dispatch(infoTypeDiscountActions.paginate(infoTypeDiscount.datos));
    }
  }, [infoTypeDiscount.datos]);

  return (
    <>
      <Modal
        isOpen={isOpen}
        style={{ minWidth: "90vh" }}
        backdrop={!["CREATE", "EDIT"].includes(option)}
      >
        <ModalHeader toggle={onClose}>
          <Settings className="icon" /> Configurarción Descuentos
        </ModalHeader>
        <ModalBody>
          <InfoTypeDiscountTable
            perPage={infoTypeDiscounts?.meta?.itemsPerPage}
            totalItems={infoTypeDiscounts?.meta?.totalItems}
            data={infoTypeDiscounts?.items || []}
            onChangePage={handlePage}
            onChangeRowsPerPage={handleRowPer}
            onDelete={handleDeleted}
            onClick={handleEdit}
          />
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            outline
            onClick={() =>
              dispatch(infoTypeDiscountActions.changeOption("CREATE"))
            }
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* create */}
      <InfoTypeDiscountCreate
        isOpen={option == "CREATE"}
        onClose={() => dispatch(infoTypeDiscountActions.changeOption(""))}
        onSave={handleCreated}
      />
      {/* edit */}
      <InfoTypeDiscountEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(infoTypeDiscountActions.changeOption(""))}
        onSave={handleUpdated}
      />
    </>
  );
};
