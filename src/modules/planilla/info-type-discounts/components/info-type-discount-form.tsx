import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IFormInfoTypeDiscountDto } from "../dtos/form-info-type-discount.dto";
import { TypeDiscountSelect } from "@modules/planilla/type_descuento/components/type-discount-select";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const InfoTypeDiscountForm = ({
  form,
  isEdit,
  onChange,
}: IProps<IFormInfoTypeDiscountDto>) => {
  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        {/* form info */}
        <FormGroup className="mb-3">
          <label>
            Tipo Descuento <b className="text-danger">*</b>
          </label>
          <div>
            <TypeDiscountSelect
              isDisabled={isEdit}
              name="typeDiscountId"
              value={form?.typeDiscountId}
              onChange={onChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({ name: target.name, value: parseFloat(target.value) })
              }
            />
          </div>
        </FormGroup>
      </Form>
    </>
  );
};
