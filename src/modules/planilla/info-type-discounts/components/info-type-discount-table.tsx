/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-vars */
import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { Search, Edit, Trash } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { Show } from "@common/show";
import currencyFormatter from "currency-formatter";
import { useInfoTypeDiscountDelete } from "../hooks/use-info-type-discount-delete";

// eslint-disable-next-line no-unused-vars
declare type onQuerySearch = (querySearch: string | string[]) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: IInfoTypeDiscountEntity) => any;
}

interface IProps {
  data: IInfoTypeDiscountEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: (infoTypeDiscount: IInfoTypeDiscountEntity) => void;
  onDelete?: (infoTypeDiscount: IInfoTypeDiscountEntity) => void;
}

export const InfoTypeDiscountTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  onDelete,
  options,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const infoDelete = useInfoTypeDiscountDelete();

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: IInfoTypeDiscountEntity) => row.id,
      },
      {
        name: "Código",
        grow: true,
        center: true,
        selector: (row: IInfoTypeDiscountEntity) => (
          <span className="badge badge-dark">{row?.typeDiscount?.code}</span>
        ),
      },
      {
        name: "Descripción",
        wrap: true,
        selector: (row: IInfoTypeDiscountEntity) => {
          return <span>{row?.typeDiscount?.description}</span>;
        },
      },
      {
        name: "Monto",
        selector: (row: IInfoTypeDiscountEntity) => (
          <Show
            condition={parseFloat(`${row.amount}`) > 0}
            isDefault={
              <span>
                {currencyFormatter.format(row.amount, { code: "PEN" })}
              </span>
            }
          >
            <b className="text-danger">
              {currencyFormatter.format(row.amount, { code: "PEN" })}
            </b>
          </Show>
        ),
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: IInfoTypeDiscountEntity) => (
          <>
            <Edit
              className="cursor-pointer icon"
              onClick={() => handleClick(row)}
            />
            <Trash
              className="cursor-pointer icon"
              onClick={() => handleDelete(row)}
            />
          </>
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (infoTypeDiscount: IInfoTypeDiscountEntity) => {
    if (typeof onClick == "function") onClick(infoTypeDiscount);
  };

  const handleDelete = (infoTypeDiscount: IInfoTypeDiscountEntity) => {
    infoDelete
      .destroy(infoTypeDiscount.id)
      .then(() => {
        if (typeof onDelete == "function") {
          onDelete(infoTypeDiscount);
        }
      })
      .catch(() => null);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}
              >
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
