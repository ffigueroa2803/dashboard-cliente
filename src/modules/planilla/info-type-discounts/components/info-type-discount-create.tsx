/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { InfoTypeDiscountForm } from "./info-type-discount-form";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { IInfoTypeDiscountEntity } from "../dtos/info-type-discount.entity";
import { useInfotypeDiscountForm } from "../hooks/use-info-type-discount-form";
import { useInfoTypeDiscountCreate } from "../hooks/use-info-type-discount-create";
import { RootState } from "@store/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (info: IInfoTypeDiscountEntity) => void;
}

export const InfoTypeDiscountCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const { info } = useSelector((state: RootState) => state.info);

  const infoForm = useInfotypeDiscountForm();
  const infoCreate = useInfoTypeDiscountCreate();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // payload
    const payload = Object.assign(infoForm.form, {
      infoId: info.id,
    });
    // send create contract
    await infoCreate
      .save(payload)
      .then((data) => {
        infoForm.clearForm();
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => null);
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Configuración Descuento</ModalHeader>
      <ModalBody>
        <InfoTypeDiscountForm
          form={infoForm.form}
          onChange={infoForm.handleForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save size={17} />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
