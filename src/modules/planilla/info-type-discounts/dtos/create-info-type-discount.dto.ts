export interface ICreateInfoTypeDiscountDto {
  typeDiscountId: number;
  infoId: number;
  amount: number;
}
