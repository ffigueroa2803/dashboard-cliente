export interface IFormInfoTypeDiscountDto {
  typeDiscountId: number;
  infoId: number;
  amount: number;
}
