import { TypeDescuento } from "@modules/planilla/type_descuento/dtos/type_descuento.enitity";

export interface IInfoTypeDiscountEntity {
  id: number;
  typeDiscountId: number;
  infoId: number;
  amount: number;
  typeDiscount?: TypeDescuento;
}
