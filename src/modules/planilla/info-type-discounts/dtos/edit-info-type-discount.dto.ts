export interface IEditInfoTypeDiscounDto {
  typeDiscountId: number;
  amount: number;
}
