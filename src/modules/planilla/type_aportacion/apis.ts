import { ICreateTypeAportacionDto } from "./dtos/create-type_aportacion.dto";
import { planillaRequest } from "@services/planilla.request";
import { PaginateDto } from "@services/dtos";
import { TypeAportacion } from "./dtos/type_aportacion.enitity";
import { IFilterFindConfigAportationMaxsDto } from "./dtos/filter-type-aportations.dto";

const request = planillaRequest();

export const getTypeAportaciones = async ({
  page,
  limit,
  querySearch,
}: PaginateDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  return await request
    .get(`typeAportations`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
export const findTypeAportacion = async (
  id: number
): Promise<TypeAportacion> => {
  return await request
    .get(`typeAportations/${id}`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createTypeAportacion = async (
  payload: ICreateTypeAportacionDto
): Promise<TypeAportacion> => {
  return await request
    .post(`typeAportations`, payload)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const updateTypeAportacion = async (
  id: number,
  payload: TypeAportacion
): Promise<TypeAportacion> => {
  return await request
    .put(`typeAportations/${id}`, payload)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const desactiveTypeAportacion = async (id: number) => {
  return await request
    .get(`typeAportations/${id}/desactive`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const findConfigAportionsMaxs = async (
  id: number,
  { page, limit, year }: IFilterFindConfigAportationMaxsDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("limit", `${limit}`);
  if (year) params.set("year", `${year}`);
  return await request
    .get(`typeAportations/${id}/configAportationMaxs`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
