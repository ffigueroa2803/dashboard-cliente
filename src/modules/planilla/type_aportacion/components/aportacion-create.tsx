/* eslint-disable no-unused-vars */
import { AirhspSelect } from "@modules/planilla/airhsp/infrastructure/components/airhsp-select";
import { TypeAportacion } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";
import { InputDto } from "@services/dtos";
import { useState } from "react";
import { ArrowLeft, Save } from "react-feather";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { createTypeAportacion } from "../apis";
import { ICreateTypeAportacionDto } from "../dtos/create-type_aportacion.dto";

interface IProps {
  onClose: () => void;
  onSave: (typedescuento: TypeAportacion) => void;
}

export const defaultTypeAportacion: ICreateTypeAportacionDto = {
  code: "",
  default: "",
  min: "",
  name: "",
  percent: "",
};

export const AportacionCreate = ({ onClose, onSave }: IProps) => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ICreateTypeAportacionDto>(
    defaultTypeAportacion
  );

  const handleOnChange = ({ name, value }: InputDto) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    setPending(true);
    await createTypeAportacion(form as any)
      .then((data) => {
        setForm(defaultTypeAportacion);
        toast.success(`Los datos se guardarón correctamente!`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`Ocurrio un error al guardar los datos`));
    setPending(false);
    onClose();
  };

  const ComponentCreateType_Aportacion = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            name="code"
            className="capitalize"
            onChange={({ target }) => handleOnChange(target)}
            value={form?.code || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            name="name"
            className="capitalize"
            onChange={({ target }) => handleOnChange(target)}
            value={form?.name || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Porcentaje</label>
          <Input
            type="text"
            className="capitalize"
            name="percent"
            onChange={({ target }) => handleOnChange(target)}
            value={form?.percent || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Mínimo</label>
          <Input
            type="text"
            className="capitalize"
            name="min"
            onChange={({ target }) => handleOnChange(target)}
            value={form?.min || ""}
          />
        </FormGroup>
        <FormGroup>
          <label>Predeterminado</label>
          <Input
            type="text"
            className="capitalize"
            name="default"
            onChange={({ target }) => handleOnChange(target)}
            value={form?.default || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Código AIRHSP</label>
          <AirhspSelect
            name="airhspId"
            value={form?.airhspId || ""}
            query={{ page: 1, limit: 100, level: 3 }}
            onChange={(opt) =>
              handleOnChange({ name: opt.name, value: opt.value })
            }
          />
        </FormGroup>
        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} className="icon" />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} className="icon" />
          </Button>
        </Col>
      </Row>
    </div>
  );

  // const ComponentSearch = (
  //   <div className='mt-5'>
  //     <label><User size={15} /> Validar Persona</label>
  //     <PersonSearchSelect
  //       onAdd={handleAdd}
  //     />
  //   </div>
  // )

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Aportación</ModalHeader>
      <ModalBody>
        {/* <ProgressIndicator
          selectedIndex={currentStep}
          values={steps}
          appearance={'primary'}
        /> */}
        {/* {currentStep == 0 ? ComponentSearch : null} */}
        {/* {currentStep == 1 ? ComponentCreateWork : null} */}
        {ComponentCreateType_Aportacion}
      </ModalBody>
    </Modal>
  );
};
