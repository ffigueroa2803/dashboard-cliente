import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getTypeAportaciones } from "../apis";
import { TypeAportacion } from "../dtos/type_aportacion.enitity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: any;
  name: string;
  isDisabled?: boolean;
}

export const TypeAportationSelect = ({
  name,
  value,
  isDisabled,
  onChange,
}: IProps) => {
  return (
    <SelectRemote
      handle={getTypeAportaciones}
      name={name}
      value={value}
      isDisabled={isDisabled}
      onChange={onChange}
      selectRow={{
        label: (row: TypeAportacion) => `${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
