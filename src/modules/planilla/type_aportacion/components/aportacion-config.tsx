/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
  FormGroup,
  Input,
} from "reactstrap";
import { TypeAportacion } from "../dtos/type_aportacion.enitity";
import { findConfigAportionsMaxs } from "../apis";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "@store/store";
import { paginate } from "@modules/planilla/configAportationMax/store";
import { AportationMaxForm } from "@modules/planilla/configAportationMax/components/aportation-max-form";
import { useCreate } from "@modules/planilla/configAportationMax/hooks/useCreate";
import { ListConfiguration } from "@modules/planilla/type_aportacion/components/list-configuration";
interface IProps {
  onClose: () => void;
  onSave: (typedescuento: TypeAportacion) => void;
}

export const defaultConfigMax = {
  typeCategoryId: 0,
  typeAportationId: 0,
  uit: 0,
  percent: 0,
  year: 0,
};

export const AportacionConfig = ({ onClose, onSave }: IProps) => {
  const { type_aportacion } = useSelector(
    (state: RootState) => state.type_aportacion
  );
  const [form] = useState<TypeAportacion>(type_aportacion);

  const {
    SaveAportationMax,
    configAportationMax,
    handleOnChangeAportationMax,
  } = useCreate();
  const dispatch = useDispatch();
  const [year, setyear] = useState<number>(0);

  useEffect(() => {
    if (type_aportacion?.id)
      handleOnChangeAportationMax({
        name: "typeAportationId",
        value: type_aportacion.id,
      });
  }, [type_aportacion]);

  useEffect(() => {
    if (type_aportacion?.id) {
      getTypeAportationMax();
    }
  }, [type_aportacion]);

  const ComponentConfigType_Aportacion = (
    <div className="mt-2">
      <div className="mb-3">
        <Row>
          <Col md="4">
            <FormGroup>
              <label>ID-MANUAL</label>
              <Input
                type="text"
                name="code"
                className="capitalize"
                value={form?.code || ""}
                readOnly
              />
            </FormGroup>
          </Col>

          <Col md="4">
            <FormGroup>
              <label>Descripción</label>
              <Input
                type="text"
                name="name"
                className="capitalize"
                value={form?.name || ""}
                readOnly
              />
            </FormGroup>
          </Col>
          <Col md="4">
            <FormGroup>
              <label>Porcentaje</label>
              <Input
                type="text"
                className="capitalize"
                name="percent"
                value={form?.percent || ""}
                readOnly
              />
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <label>Mínimo</label>
              <Input
                type="text"
                className="capitalize"
                name="min"
                value={form?.min || ""}
                readOnly
              />
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <label>Predeterminado</label>
              <Input
                type="text"
                className="capitalize"
                name="default"
                readOnly
                value={form?.default || ""}
              />
            </FormGroup>
          </Col>
        </Row>

        <hr />
      </div>
    </div>
  );

  const getTypeAportationMax = async () => {
    console.log(year);
    const data = await findConfigAportionsMaxs(type_aportacion?.id, {
      page: 1,
      limit: 30,
      year,
    });
    dispatch(paginate(data));
  };

  const handleSearchYear = () => {
    if (`${year}`.length == 4) {
      getTypeAportationMax();
    }
  };

  useEffect(() => {
    if (year) handleSearchYear();
  }, [year]);

  return (
    <Modal isOpen={true} size="xl">
      <ModalHeader toggle={onClose}>Editar Config. Máxima</ModalHeader>
      <ModalBody>
        {ComponentConfigType_Aportacion}

        <AportationMaxForm
          form={configAportationMax}
          handleOnChange={handleOnChangeAportationMax}
          save={SaveAportationMax}
        />

        {/* {ListConfiguration} */}
        <ListConfiguration year={year} setYear={setyear} />
      </ModalBody>
    </Modal>
  );
};
