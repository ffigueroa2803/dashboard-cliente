/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Button, FormGroup, Input } from "reactstrap";
import { EntityConfigMaxDto } from "@modules/planilla/configAportationMax/dto/entity-config_aportation_max.dto";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { Save, X } from "react-feather";
import { Show } from "@common/show";
import { useEdit } from "@modules/planilla/configAportationMax/hooks/useEdit";

interface Iprops {
  data: EntityConfigMaxDto;
}

export const DataConfigMax = ({ data }: Iprops) => {
  const [isEdit, setIsEdit] = useState<boolean>(false);

  const editConfig = useEdit(data.id);

  const resetForm = () => {
    setIsEdit(false);
    editConfig.setForm(data);
  };

  const handleForm = (target: any) => {
    editConfig.handleForm(target);
    setIsEdit(true);
  };

  useEffect(() => {
    if (data) editConfig.setForm(data);
  }, [data]);

  return (
    <div className="border border-black rounded p-3 mb-4">
      <FormGroup>
        <label>Partición Presupuestal</label>
        <TypeCategorySelect
          name="typeCategoryId"
          value={editConfig.form?.typeCategoryId || 0}
          onChange={(e) =>
            handleForm({ name: e.name, value: parseInt(e.value) })
          }
        />
      </FormGroup>
      <FormGroup>
        <label>Año</label>
        <Input
          type="text"
          name="year"
          className="capitalize"
          value={editConfig.form?.year || ""}
          onChange={({ target }) => handleForm(target)}
        />
      </FormGroup>
      <FormGroup>
        <label>UIT</label>
        <Input
          type="text"
          name="uit"
          className="capitalize"
          value={editConfig.form?.uit || ""}
          onChange={({ target }) => handleForm(target)}
        />
      </FormGroup>
      <FormGroup>
        <label>Porcentaje(%)</label>
        <Input
          type="text"
          name="percent"
          className="capitalize"
          value={editConfig.form?.percent || ""}
          onChange={({ target }) => handleForm(target)}
        />
      </FormGroup>
      <Show condition={isEdit}>
        <FormGroup>
          <Button
            size="md"
            color="primary"
            className="mr-1 mb-2"
            disabled={editConfig.pending}
            onClick={editConfig.execute}>
            <Save size={15} /> Actualizar
          </Button>
          <Button
            size="md"
            color="danger"
            onClick={resetForm}
            disabled={editConfig.pending}>
            <X size={15} />
          </Button>
        </FormGroup>
      </Show>
    </div>
  );
};
