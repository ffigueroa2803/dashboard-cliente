/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Row, Col, FormGroup, Input, Card, CardBody } from "reactstrap";
import { RefreshCw, List } from "react-feather";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { DataConfigMax } from "./data-config-max";

interface Iprops {
  year: number;
  setYear: (year: number) => void;
}
export const ListConfiguration = ({ setYear, year }: Iprops) => {
  const { config_max_aportations } = useSelector(
    (state: RootState) => state.aportationMax
  );
  return (
    <div className="mt-2">
      <div className="mb-3">
        <Row>
          <Col md="12">
            <p className="font-weight-bold">
              <List className="mr-2 icon" size={13} />
              Configuracion Máxima por Categoría
            </p>
            <hr />
          </Col>
          <Col md="12">
            <FormGroup>
              <Input
                type="number"
                name="code"
                className="capitalize"
                onChange={({ target }) => setYear(parseInt(target.value))}
                value={year || ""}
              />
            </FormGroup>
          </Col>
          <Col md="12">
            <Show
              condition={config_max_aportations.items.length > 0}
              isDefault={
                <Card className="text-center">
                  <CardBody>
                    No hay registro disponibles{" "}
                    <RefreshCw size={17} className="icon-hover icon" />
                  </CardBody>
                </Card>
              }>
              <Row>
                {config_max_aportations.items.map((obj, key) => (
                  <Col md="4" key={key}>
                    <DataConfigMax data={obj} />
                  </Col>
                ))}
              </Row>
            </Show>
          </Col>
        </Row>

        <hr />
      </div>
    </div>
  );
};
