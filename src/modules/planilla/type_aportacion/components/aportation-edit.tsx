/* eslint-disable no-unused-vars */
import { AirhspSelect } from "@modules/planilla/airhsp/infrastructure/components/airhsp-select";
import { updateTypeAportacion } from "@modules/planilla/type_aportacion/apis";
import { TypeAportacion } from "@modules/planilla/type_aportacion/dtos/type_aportacion.enitity";
import { InputDto } from "@services/dtos";
import { RootState } from "@store/store";
import { useState } from "react";
import { ArrowLeft, Save } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";

interface IProps {
  onClose: () => void;
  onSave: (typeAportation: TypeAportacion) => void;
}

export const AportationEdit = ({ onClose, onSave }: IProps) => {
  const { type_aportacion } = useSelector(
    (state: RootState) => state.type_aportacion
  );
  const [form, setForm] = useState<TypeAportacion>(type_aportacion);
  const [pending, setPending] = useState<boolean>(false);

  const handleInput = ({ name, value }: InputDto) => {
    setForm({ ...form, [name]: value });
  };

  const handleSave = async () => {
    setPending(true);
    await updateTypeAportacion(type_aportacion.id, form)
      .then((data) => {
        toast.success("Los datos se guardaron correctamente");
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error("Ocrruio un error al actulizar los datos"));
    setPending(false);
    onClose();
  };
  const ComponenteEditType_Remuneration = (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>ID-MANUAL</label>
          <Input
            type="text"
            className="capitalize"
            name="code"
            readOnly
            onChange={({ target }) => handleInput(target)}
            value={form?.code || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Alias</label>
          <Input
            type="text"
            className="capitalize"
            name="name"
            onChange={({ target }) => handleInput(target)}
            defaultValue={form?.name || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Porcentaje</label>
          <Input
            type="text"
            className="capitalize"
            name="percent"
            onChange={({ target }) => handleInput(target)}
            defaultValue={form?.percent || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Mínimo</label>
          <Input
            type="text"
            className="capitalize"
            name="min"
            onChange={({ target }) => handleInput(target)}
            defaultValue={form?.min || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Predeterminado</label>
          <Input
            type="text"
            className="capitalize"
            name="default"
            onChange={({ target }) => handleInput(target)}
            defaultValue={form?.default || ""}
          />
        </FormGroup>

        <FormGroup>
          <label>Código AIRHSP</label>
          <AirhspSelect
            name="airhspId"
            value={form?.airhspId || ""}
            query={{ page: 1, limit: 100, level: 3 }}
            onChange={(opt) =>
              handleInput({ name: opt.name, value: opt.value })
            }
          />
        </FormGroup>
        <hr />
      </div>

      <Row className="justify-content-center">
        <Col md="6 col-6 text-left">
          <Button
            color="secundary"
            title="Atras"
            onClick={() => onClose()}
            disabled={pending}
          >
            <ArrowLeft size={17} className="icon" />
          </Button>
        </Col>

        <Col md="6 col-6 text-right">
          <Button
            color="primary"
            title="Guardar datos"
            onClick={handleSave}
            disabled={pending}
          >
            <Save size={17} className="icon" />
          </Button>
        </Col>
      </Row>
    </div>
  );

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Editar Tip. Aportación</ModalHeader>
      <ModalBody>{ComponenteEditType_Remuneration}</ModalBody>
    </Modal>
  );
};
