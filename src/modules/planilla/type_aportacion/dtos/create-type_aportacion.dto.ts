export interface ICreateTypeAportacionDto {
  code?: string,
  min?: string,
  percent?: string,
  name?: string,
  airhspId?: number;
  state?: boolean,
  default?: string,
}