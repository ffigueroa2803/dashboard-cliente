import { PaginateDto } from '@services/dtos';

export interface IFilterFindConfigAportationMaxsDto extends PaginateDto {
  year?: number;
}