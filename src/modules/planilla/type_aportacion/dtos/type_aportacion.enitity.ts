import { AirhspEntity } from "@modules/planilla/airhsp/domain/airhsp.entity";

export interface TypeAportacion {
  id: number;
  name: string;
  code: string;
  min: number;
  percent: number;
  airhspId?: number;
  state: boolean;
  default: number;

  airhsp?: AirhspEntity;
}

export const typeAportationName = "TypeAportationEntity";
