import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { useHistorialResume } from "@modules/planilla/historials/hooks/use-historial-resume";
import { createContext, FC, useContext, useState } from "react";
import { ObligationEntity } from "../dtos/obligation.entity";

const dataDefault = {
  isEdit: true,
  setIsEdit: (value: boolean) => {},
  refreshResume: () => {},
};

export const ObligationContext = createContext(dataDefault);

export const ObligationProvider: FC = ({ children }) => {
  const ability = useContext(CaslContext);

  const canAction = ability.can(PermissionAction.UPDATE, ObligationEntity.name);

  const [isEdit, setIsEdit] = useState<boolean>(canAction);
  const historialResume = useHistorialResume();

  const refreshResume = () => {
    historialResume.execute();
  };

  const handleEdit = (value: boolean) => {
    if (!canAction) setIsEdit(false);
    setIsEdit(value);
  };

  return (
    <ObligationContext.Provider
      value={{
        isEdit,
        setIsEdit: handleEdit,
        refreshResume,
      }}
    >
      {children || null}
    </ObligationContext.Provider>
  );
};
