import { RootState } from "@store/store";
import React, { Fragment, useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { ObligationContext } from "./obligation-context";
import { IObligationEntity } from "../dtos/obligation.entity";
import { obligationActions } from "../store";
import Skeleton from "react-loading-skeleton";
import { Show } from "@common/show";
import { ObligationItem } from "./obligation-item";

interface IProps {
  loading: boolean;
}

const PlaceholderItems = () => {
  const datos = [1, 2, 3, 4, 5];
  return (
    <>
      {datos.map((index) => (
        <Fragment key={`item-loading-${index}`}>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
          <Col md="3" className="mb-3">
            <Skeleton height={10} />
            <Skeleton height={30} />
          </Col>
        </Fragment>
      ))}
    </>
  );
};

export const ObligationBlock = ({ loading }: IProps) => {
  const dispatch = useDispatch();

  const { cronograma } = useSelector((state: RootState) => state.cronograma);
  const { obligations } = useSelector((state: RootState) => state.obligation);

  const { isEdit, setIsEdit, refreshResume } = useContext(ObligationContext);

  const handleOnDelete = (obligation: IObligationEntity) => {
    setIsEdit(false);
    dispatch(obligationActions.removeItem(obligation.id));
  };

  useEffect(() => {
    if (cronograma?.id) setIsEdit(cronograma.state);
  }, [cronograma]);

  return (
    <Row>
      {/* loading */}
      <Show condition={!loading} isDefault={<PlaceholderItems />}>
        {/* items */}
        {obligations?.items?.map((obl, index) => (
          <ObligationItem
            key={`ìtem-${obl.id}-${index}`}
            isEdit={isEdit}
            obligation={obl}
            onUpdate={refreshResume}
            onDelete={handleOnDelete}
          />
        ))}
      </Show>
    </Row>
  );
};
