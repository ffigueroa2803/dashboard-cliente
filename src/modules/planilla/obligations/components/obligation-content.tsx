import React, { useContext, useEffect, useState } from "react";
import { Edit, Settings, ShieldOff, X } from "react-feather";
import { Card, CardBody, CardHeader } from "reactstrap";
import { HistorialResume } from "@modules/planilla/historials/components/historial-resume";
import { ObligationBlock } from "./obligation-block";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useHistorialObligation } from "@modules/planilla/historials/hooks/use-historial-obligation";
import { ObligationContext, ObligationProvider } from "./obligation-context";
import { infoActions } from "@modules/planilla/infos/store";
import { toast } from "react-toastify";
import { Show } from "@common/show";
import { TypeObligationContent } from "@modules/planilla/type-obligations/components/type-obligation-content";

export const WrapperObligationContext = () => {
  const dispatch = useDispatch();

  const historialObligation = useHistorialObligation();

  const { historial } = useSelector((state: RootState) => state.historial);
  const { cronograma } = useSelector((state: RootState) => state.cronograma);

  const { refreshResume } = useContext(ObligationContext);

  const [isConfig, setIsConfig] = useState<boolean>(false);

  const handleConfig = () => {
    dispatch(infoActions.find(historial?.info || ({} as any)));
    setIsConfig(true);
  };

  const handleSaveConfig = () => {
    toast.dismiss();
    toast.info(`Calculando...`);
    setTimeout(async () => {
      await historialObligation.execute();
      refreshResume();
    }, 2000);
  };

  useEffect(() => {
    if (historial?.id) historialObligation.execute();
  }, [historial]);

  return (
    <>
      <Card>
        <CardBody>
          <HistorialResume />
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          <Show condition={cronograma.state}>
            <Settings
              className="icon close cursor-pointer ml-3"
              onClick={handleConfig}
            />
          </Show>
          <ShieldOff className="icon" /> Obligación
        </CardHeader>
        <CardBody>
          <ObligationBlock loading={historialObligation.pending} />
        </CardBody>
      </Card>
      {/* config type-oglibation */}
      <Show condition={isConfig}>
        <TypeObligationContent
          isOpen={isConfig}
          onClose={() => setIsConfig(false)}
          onSave={handleSaveConfig}
          onCreated={handleSaveConfig}
        />
      </Show>
    </>
  );
};

export const ObligationContent = () => {
  return (
    <ObligationProvider>
      <WrapperObligationContext />
    </ObligationProvider>
  );
};
