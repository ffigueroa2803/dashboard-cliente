/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import currencyForrmater from "currency-formatter";
import { Show } from "@common/show";
import { Clock, DollarSign, Sliders } from "react-feather";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import { useDispatch } from "react-redux";
import { obligationActions } from "../store";
import { IObligationEntity } from "../dtos/obligation.entity";
import { ObligationEdit } from "./obligation-edit";
import { DateTime } from "luxon";

interface ItemProps {
  obligation: IObligationEntity;
  isEdit: boolean;
  onUpdate?: (obligation: IObligationEntity) => void;
  onDelete?: (obligation: IObligationEntity) => void;
}

export const ObligationItem = ({
  obligation,
  isEdit,
  onUpdate,
  onDelete,
}: ItemProps) => {
  const dispatch = useDispatch();
  const [isDialog, setIsDialog] = useState<boolean>(false);

  const displayAmount = () => {
    return currencyForrmater.format(obligation.amount, { code: "PEN" });
  };

  const handleEdit = () => {
    if (!isEdit) return;
    dispatch(obligationActions.find(obligation));
    setIsDialog(true);
  };

  const isAmount = parseFloat(`${obligation.amount}`) > 0;

  return (
    <>
      <Col md="6" className="mb-4">
        <Card>
          <CardHeader>
            <span
              className="close"
              style={{ marginTop: "-3px" }}
              onClick={handleEdit}>
              <Clock
                className={
                  isEdit ? "icon cursor-pointer text-danger" : "icon text-muted"
                }
              />
            </span>
            {/* info */}
            <label className="disabled-selection mb-0">
              <b className={isAmount ? "text-danger" : "text-muted"}>
                {obligation?.discount?.typeDiscount?.code}
              </b>
              .- {obligation?.discount?.typeDiscount?.description || "N/A"}
              <span className="badge badge-warning ml-1 mb-0">
                {displayAmount()}
              </span>
            </label>
          </CardHeader>
          <CardBody>
            <Row>
              <Col md="12" className="mb-2">
                <label>Apellidos y Nombres</label>
                <h6 className="capitalize">
                  {obligation?.typeObligation?.person?.fullName || ""}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>Tipo Documento</label>
                <h6 className="capitalize">
                  {obligation?.typeObligation?.person?.documentType?.name || ""}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>N° Documento</label>
                <h6 className="capitalize">
                  {obligation?.typeObligation?.person?.documentNumber || ""}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>F. Nacimiento</label>
                <h6 className="capitalize">
                  {obligation?.typeObligation?.person?.dateOfBirth
                    ? DateTime.fromSQL(
                        obligation?.typeObligation?.person?.dateOfBirth
                      ).toFormat("dd/MM/yyyy")
                    : ""}
                </h6>
              </Col>

              <Col md="12" className="mb-2">
                <hr />
                <Sliders className="icon" /> Método de Pago
                <hr />
              </Col>

              <Col md="6" className="mb-2">
                <label>Tipo Documento</label>
                <h6 className="capitalize">
                  {obligation?.documentType?.name || "N/A"}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>N° Documento</label>
                <h6 className="capitalize">
                  {obligation?.documentNumber || "N/A"}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>Medio de Pago</label>
                <h6 className="capitalize">
                  {obligation?.isCheck ? "En Cheque" : "En Cuenta"}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>Banco</label>
                <h6 className="capitalize">
                  {obligation?.bank?.name || "N/A"}
                </h6>
              </Col>

              <Col md="12" className="mb-2">
                <label>N° Cuenta</label>
                <h6 className="capitalize">
                  {obligation?.numberOfAccount || "N/A"}
                </h6>
              </Col>

              <Col md="12" className="mb-2">
                <label>Observación</label>
                <h6>{obligation?.observation || "N/A"}</h6>
              </Col>

              <Col md="12" className="mb-2">
                <hr />
                <DollarSign className="icon" /> Modo de Descuento
                <hr />
              </Col>

              <Col md="6" className="mb-2">
                <label>Modo</label>
                <h6 className="capitalize">
                  {obligation?.isPercent ? "Porcentaje" : "Monto Fijo"}
                </h6>
              </Col>

              <Col md="6" className="mb-2">
                <label>A Descontar</label>
                <h6 className="capitalize">{displayAmount()}</h6>
              </Col>

              <Show condition={obligation.isPercent}>
                <Col md="6" className="mb-2">
                  <label>Tipo de Calculo</label>
                  <h6 className="capitalize">
                    {obligation?.mode == "BRUTO"
                      ? "Calculo Bruto"
                      : "Calculo Neto"}
                  </h6>
                </Col>

                <Col md="6" className="mb-2">
                  <label>%</label>
                  <h6 className="capitalize">{obligation?.percent}</h6>
                </Col>

                <Col md="6" className="mb-2">
                  <label>Bonificación</label>
                  <h6 className="capitalize">
                    {obligation?.isBonification ? "SI" : "NO"}
                  </h6>
                </Col>
              </Show>
            </Row>
          </CardBody>
        </Card>
      </Col>
      {/* dialog edit */}
      <Show condition={isDialog}>
        <ObligationEdit
          isOpen={isDialog}
          onClose={() => setIsDialog(false)}
          onSave={onUpdate}
          onDeleted={onDelete}
        />
      </Show>
    </>
  );
};
