/* eslint-disable no-unused-vars */
import { IInputHandle } from '@common/dtos/input-handle'
import React from 'react'
import { Form, FormGroup, Input } from 'reactstrap'
import { IFormObligationDto } from '../dtos/form-type-obligation.dto'
import Toggle from '@atlaskit/toggle'
import { useSelector } from 'react-redux'
import { RootState } from '@store/store'
import { Show } from '@common/show'
import { SelectBasic } from '@common/select/select-basic'
import { BankSelect } from '@modules/auth/banks/components/bank-select'
import { DocumentTypeSelect } from '@modules/auth/document-types/components/document-type-select'

interface IProps<T> {
  form: T
  isEdit?: boolean
  onChange: (input: IInputHandle) => void
}

export const ObligationForm = ({
  form,
  onChange,
}: IProps<IFormObligationDto>) => {
  const { obligation } = useSelector((state: RootState) => state.obligation)

  return (
    <>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FormGroup className="mb-3">
          <label>
            Tipo Descuento <b className="text-danger">*</b>
          </label>
          <h6 className="capitalize">
            {obligation?.discount?.typeDiscount?.description || 'N/A'}
            <hr />
          </h6>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Tipo Documento <b className="text-danger">*</b>
          </label>
          <DocumentTypeSelect
            name="documentTypeId"
            defaultQuerySearch={form?.documentTypeId}
            value={form?.documentTypeId || ''}
            onChange={onChange}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            N° Documento <b className="text-danger">*</b>
          </label>
          <Input
            type="text"
            name="documentNumber"
            value={form?.documentNumber}
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>
            Banco <b className="text-danger">*</b>
          </label>
          <BankSelect name="bankId" value={form?.bankId} onChange={onChange} />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>N° Cuenta</label>
          <Input
            type="text"
            name="numberOfAccount"
            value={form?.numberOfAccount}
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Porcentaje</label>
          <div>
            <Toggle
              name="isPercent"
              isChecked={form.isPercent}
              onChange={({ target }) =>
                onChange({ name: target.name, value: target.checked })
              }
            />
          </div>
        </FormGroup>

        {/* isBonification and Modo*/}
        <Show condition={form.isPercent}>
          <FormGroup className="mb-3">
            <label>
              Modo Descuento <b className="text-danger">*</b>
            </label>
            <SelectBasic
              options={[
                { label: 'Bruto', value: 'BRUTO' },
                { label: 'Neto', value: 'NETO' },
              ]}
              name="mode"
              value={form?.mode}
              onChange={onChange}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>Bonificación</label>
            <div>
              <Toggle
                name="isBonification"
                isChecked={form.isBonification}
                onChange={({ target }) =>
                  onChange({ name: target.name, value: target.checked })
                }
              />
            </div>
          </FormGroup>
        </Show>

        {/* monto */}
        <FormGroup className="mb-3">
          <label>
            Monto <b className="text-danger">*</b>
          </label>
          <div>
            <Input
              type="number"
              name="amount"
              value={form?.amount}
              onChange={({ target }) =>
                onChange({
                  name: target.name,
                  value: parseFloat(target.value),
                })
              }
            />
          </div>
        </FormGroup>

        <FormGroup className="mb-3">
          <label>Observación</label>
          <div>
            <Input
              type="textarea"
              name="observation"
              value={form?.observation}
              onChange={({ target }) => onChange(target)}
            />
          </div>
        </FormGroup>
      </Form>
    </>
  )
}
