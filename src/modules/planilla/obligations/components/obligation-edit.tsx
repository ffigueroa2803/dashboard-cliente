/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import { Save, Trash } from "react-feather";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { ObligationForm } from "./obligation-form";
import { IObligationEntity } from "../dtos/obligation.entity";
import { useObligationEdit } from "../hooks/use-obligation-edit";
import { useObligationFind } from "../hooks/use-obligation-find";
import { useObligationDelete } from "../hooks/use-obligation-delete";
import { toast } from "react-toastify";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (obligation: IObligationEntity) => void;
  onDeleted?: (obligation: IObligationEntity) => void;
}

export const ObligationEdit = ({
  isOpen,
  onClose,
  onSave,
  onDeleted,
}: IProps) => {
  const { obligation } = useSelector((state: RootState) => state.obligation);

  const obligationEdit = useObligationEdit();
  const obligationFind = useObligationFind();
  const obligationDelete = useObligationDelete();

  const [pending, setPending] = useState<boolean>(false);

  const handleSave = async () => {
    setPending(true);
    // send create contract
    await obligationEdit
      .update()
      .then((data) => {
        setTimeout(async () => {
          toast.info("Calculado...");
          const result = await obligationFind
            .find(obligation.id)
            .then((res) => res)
            .catch(() => data);
          if (typeof onSave == "function") {
            onSave(result);
            onClose();
          }
        }, 1000);
      })
      .catch(() => null);
    setPending(false);
  };

  const handleDelete = () => {
    obligationDelete
      .destroy()
      .then(async () => {
        if (typeof onDeleted == "function") {
          onDeleted(obligation);
        }
      })
      .catch(() => null);
  };

  useEffect(() => {
    if (obligation?.id && isOpen) obligationEdit.clearForm();
  }, [obligation, isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Obligación</ModalHeader>
      <ModalBody>
        <ObligationForm
          isEdit={true}
          form={obligationEdit.form}
          onChange={obligationEdit.changeForm}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button
          color="danger"
          outline
          disabled={pending}
          onClick={handleDelete}>
          <Trash className="icon" />
        </Button>
        <Button color="primary" disabled={pending} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
