import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IObligationEntity } from "../dtos/obligation.entity";
import { useDispatch } from "react-redux";
import { obligationActions } from "../store";

const request = planillaRequest();

export const useObligationFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const find = (id: number): Promise<IObligationEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`obligations/${id}`)
        .then((res) => {
          dispatch(obligationActions.find(res.data));
          resolve(res.data as IObligationEntity);
        })
        .catch((err) => {
          dispatch(obligationActions.find({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    find,
  };
};
