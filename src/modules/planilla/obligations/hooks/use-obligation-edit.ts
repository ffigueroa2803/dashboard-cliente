import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { IObligationEntity } from "../dtos/obligation.entity";
import { toast } from "react-toastify";
import { IEditObligationDto } from "../dtos/edit-obligation.dto";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = planillaRequest();

export const useObligationEdit = () => {
  const { obligation } = useSelector((state: RootState) => state.obligation);

  const dataDefault: IEditObligationDto = {
    documentTypeId: obligation.documentTypeId || "",
    documentNumber: obligation.documentNumber || "",
    bankId: obligation.bankId || 0,
    numberOfAccount: obligation.numberOfAccount || "",
    isPercent: obligation.isPercent,
    amount: obligation.isPercent ? obligation.percent : obligation.amount,
    mode: obligation.mode,
    isBonification: obligation.isBonification,
    observation: obligation.observation || "",
  };

  const [form, setForm] = useState<IEditObligationDto>(dataDefault);
  const [pending, setPending] = useState<boolean>(false);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const update = (): Promise<IObligationEntity> => {
    return new Promise(async (resolve, reject) => {
      toast.info(`Actualizando...`);
      const payload = Object.assign(form, {
        amount: parseFloat(`${form.amount}`),
      });
      // send
      setPending(true);
      await request
        .put(`obligations/${obligation.id}`, payload)
        .then((res) => {
          toast.dismiss();
          toast.success(`Los datos se actualizarón correctamente!`);
          resolve(res.data as IObligationEntity);
        })
        .catch((err) => {
          toast.dismiss();
          toast.error(`No se pudo actualizar los datos`);
          reject(err);
        });
      setPending(false);
    });
  };

  const clearForm = () => {
    setForm(dataDefault);
  };

  return {
    form,
    changeForm,
    clearForm,
    pending,
    update,
  };
};
