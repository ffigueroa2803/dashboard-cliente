import { useState } from "react";
import { planillaRequest } from "@services/planilla.request";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

const request = planillaRequest();

export const useObligationDelete = () => {
  const { obligation } = useSelector((state: RootState) => state.obligation);
  const [pending, setPending] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async () => {
    return await confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en eliminar el regístro?",
        labelSuccess: "Eliminar",
        labelError: "No",
      })
      .then(async () => {
        toast.info(`Eliminando...`);
        setPending(true);
        await request
          .destroy(`obligations/${obligation.id}`)
          .then((res) => {
            toast.dismiss();
            toast.success(`Los datos se eliminarón correctamente!`);
            return res.data;
          })
          .catch((err) => {
            toast.dismiss();
            toast.error(`No se pudo eliminar los datos`);
            return err;
          });
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    destroy,
  };
};
