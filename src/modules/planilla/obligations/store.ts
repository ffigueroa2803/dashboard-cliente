import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IObligationEntity } from "./dtos/obligation.entity";

export interface ObligationState {
  obligations: ResponsePaginateDto<IObligationEntity>;
  obligation: IObligationEntity;
  option: string;
}

const initialState: ObligationState = {
  obligations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  obligation: {} as any,
  option: "",
};

const obligationStore = createSlice({
  name: "planilla@obligations",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IObligationEntity>>
    ) => {
      state.obligations = payload;
      return state;
    },
    find: (state, { payload }: PayloadAction<IObligationEntity>) => {
      state.obligation = payload;
      state.obligations.items?.map((item) => {
        if (payload.id == item.id) {
          return Object.assign(item, payload);
        }
        // default
        return item;
      });
      return state;
    },
    removeItem: (state, { payload }: PayloadAction<number>) => {
      state.obligations.items = state.obligations?.items?.filter(
        (item) => item.id != payload
      );
      // change meta
      state.obligations.meta.totalItems =
        state.obligations.meta.totalItems || 0 - 1;
      // response
      return state;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.obligation };
    },
  },
});

export const obligationReducer = obligationStore.reducer;

export const obligationActions = obligationStore.actions;
