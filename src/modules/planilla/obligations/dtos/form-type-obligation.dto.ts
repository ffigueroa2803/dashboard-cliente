import { ETypeObligationMode } from "@modules/planilla/type-obligations/dtos/type-obligation.entity";

export interface IFormObligationDto {
  documentTypeId: string;
  documentNumber: string;
  bankId: number;
  numberOfAccount: string;
  isPercent: boolean;
  amount: number;
  mode: ETypeObligationMode;
  isBonification: boolean;
  observation?: string;
}
