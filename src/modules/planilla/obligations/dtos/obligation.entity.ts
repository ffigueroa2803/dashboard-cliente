import { IBankEntity } from "@modules/auth/banks/dtos/bank.entity";
import { IDiscountEntity } from "@modules/planilla/discounts/dtos/discount.entity";
import {
  ETypeObligationMode,
  ITypeObligationEntity,
} from "@modules/planilla/type-obligations/dtos/type-obligation.entity";
import { IDocumentTypeEntity } from "@services/entities/document-type.entity";

export interface IObligationEntity {
  id: number;
  typeObligationId: number;
  discountId: number;
  documentTypeId: string;
  documentNumber: string;
  bankId: number;
  numberOfAccount?: string;
  isCheck: boolean;
  isPercent: boolean;
  percent: number;
  amount: number;
  observation?: string;
  mode: ETypeObligationMode;
  isBonification: boolean;
  typeObligation?: ITypeObligationEntity;
  discount?: IDiscountEntity;
  bank?: IBankEntity;
  documentType: IDocumentTypeEntity;
}

export class ObligationEntity {
  constructor(partial: Partial<IObligationEntity>) {
    Object.assign(this, partial);
  }
}

export const obligationEntityName = "ObligationEntity";
