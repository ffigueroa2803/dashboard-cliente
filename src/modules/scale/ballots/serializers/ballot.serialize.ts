import { DateTime } from "luxon";

export class BallotSerialize {
  private departureTime!: string;
  private returnTime!: string | undefined;

  get displayDepartureTime() {
    let format = DateTime.fromFormat(this.departureTime, "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.departureTime, "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  get displayReturnTime() {
    if (!this.returnTime) return null;
    let format = DateTime.fromFormat(this.returnTime, "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.returnTime, "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  get displayFormatter() {
    // response
    return `☷ ${this.displayDepartureTime} ${
      this.returnTime ? `- ${this.displayReturnTime}R` : ""
    }`;
  }
}
