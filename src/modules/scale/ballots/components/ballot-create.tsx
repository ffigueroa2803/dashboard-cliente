/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { createBallot } from "../apis";
import { IBallotFormDto } from "../dtos/ballot-form.dto";
import { BallotForm } from "./ballot-form";
import { toast } from "react-toastify";
import { IBallotEntity } from "../dtos/ballot.entity";
import { DateTime } from "luxon";
import ObjectId from "bson-objectid";

interface IProps {
  isOpen: boolean;
  dateDefault: DateTime;
  onClose: () => void;
  onSave: (license: IBallotEntity) => void;
}

export const BallotCreate = ({
  isOpen,
  dateDefault,
  onClose,
  onSave,
}: IProps) => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const dataDefault: IBallotFormDto = {
    typeBallotId: 0,
    contractId: contract?.id || 0,
    code: "",
    date: dateDefault.toSQLDate(),
    departureTime: "",
    presetTime: "",
  };

  const [form, setForm] = useState<IBallotFormDto>(dataDefault);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    // save schedule
    await createBallot(form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente`);
        setForm(dataDefault);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setLoading(false);
  };

  useEffect(() => {
    if (isOpen) {
      handleForm({ name: "code", value: new ObjectId().toHexString() });
    }
  }, [isOpen]);

  useEffect(() => {
    if (dateDefault) {
      handleForm({ name: "date", value: dateDefault.toSQLDate() });
    }
  }, [dateDefault]);

  useEffect(() => {
    if (contract?.id) {
      setForm(dataDefault);
    }
  }, [contract]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Papeleta</ModalHeader>
      <ModalBody>
        <BallotForm form={form} onChange={handleForm} isDisabled={loading} />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={loading} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
