import React, { useState } from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Input, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { DateTime } from "luxon";

const currentDate = DateTime.now();
const request = scaleRequest();

export const BallotReport = () => {
  const [year, setYear] = useState<number>(currentDate.year);
  const [month, setMonth] = useState<number>(currentDate.month);
  const [day, setDay] = useState<number>(currentDate.day);

  const handleClick = (extname: string) => {
    const query = new URLSearchParams();
    query.set("year", `${year}`);
    query.set("month", `${month}`);
    if (day) query.set("day", `${day}`);
    const pathBase = urljoin(
      request.urlBase,
      `/ballots/reports/list.${extname}`
    );
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Año <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            value={year}
            onChange={({ target }) => setYear(parseInt(target.value))}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Mes <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            value={month}
            onChange={({ target }) => setMonth(parseInt(target.value))}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Día</label>
          <Input
            type="number"
            value={day}
            onChange={({ target }) => setDay(parseInt(target.value))}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}
        >
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
