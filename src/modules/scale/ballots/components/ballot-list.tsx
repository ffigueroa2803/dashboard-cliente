/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useRef,
  useCallback,
  useEffect,
  useContext,
  useState,
} from "react";
import { FullCalendar } from "@common/fullcalendar";
import { DateClickArg } from "@fullcalendar/interaction";
import { CalendarApi, EventClickArg, EventInput } from "@fullcalendar/react";
import { plainToClass } from "class-transformer";
import { useDispatch, useSelector } from "react-redux";
import { IBallotEntity } from "../dtos/ballot.entity";
import { BallotSerialize } from "../serializers/ballot.serialize";
import { ballotActions } from "../store";
import { BallotEdit } from "./ballot-edit";
import { RootState } from "@store/store";
import { BallotContext, OptionsEnum } from "./ballot-context";
import { DateTime } from "luxon";

export const BallotList = () => {
  const calendarRef = useRef(null);
  const { ballots } = useSelector((state: RootState) => state.ballot);

  const ballotContext = useContext(BallotContext);

  const [isDraw, setIsDraw] = useState<boolean>(false);
  const [events, setEvents] = useState<any[]>([]);

  const dispatch = useDispatch();

  const handleEdit = async ({ event }: EventClickArg) => {
    dispatch(ballotActions.setBallot(event.extendedProps as any));
    ballotContext.setOptions(OptionsEnum.EDIT);
  };

  const formatterDate = (event: IBallotEntity) => {
    // response
    return {
      id: `${event.id}`,
      title: event.code,
      start: event.date,
      allDay: true,
      className: "bg-dark",
      borderColor: "transparent",
      extendedProps: event,
      classNames: "cursor-pointer",
    } as EventInput;
  };

  const handleDraw = () => {
    const datos: any[] = [];
    ballots.items.forEach((item) => {
      datos.push(formatterDate(item));
    });
    // add
    setEvents(datos);
    setIsDraw(false);
  };

  const handlePrev = () => {
    const current: any = calendarRef.current;
    const calendar: CalendarApi = current._calendarApi;
    calendar.prev();
    const newDate = DateTime.fromISO(calendar.getDate().toISOString());
    handleDate(newDate);
    ballotContext.setCurrentDate(newDate);
    ballotContext.setIsFetch(true);
  };

  const handleToday = () => {
    const current: any = calendarRef.current;
    const calendar: CalendarApi = current._calendarApi;
    calendar.today();
    const newDate = DateTime.fromISO(calendar.getDate().toISOString());
    handleDate(newDate);
    ballotContext.setCurrentDate(newDate);
    ballotContext.setIsFetch(true);
  };

  const handleNext = () => {
    const current: any = calendarRef.current;
    const calendar: CalendarApi = current._calendarApi;
    calendar.next();
    const newDate = DateTime.fromISO(calendar.getDate().toISOString());
    handleDate(newDate);
    ballotContext.setCurrentDate(newDate);
    ballotContext.setIsFetch(true);
  };

  const handleDate = (date: DateTime) => {
    ballotContext.changeQueryParams({ name: "year", value: date.year });
    ballotContext.changeQueryParams({ name: "month", value: date.month });
  };

  const handleAdd = (event: DateClickArg) => {
    ballotContext.setCurrentDate(DateTime.fromSQL(event.dateStr));
    ballotContext.setOptions(OptionsEnum.CREATE);
  };

  const handleSave = () => {
    ballotContext.setIsFetch(true);
  };

  const handleDelete = () => {
    ballotContext.setIsFetch(true);
    ballotContext.setOptions(OptionsEnum.DEFAULT);
  };

  useEffect(() => {
    if (!ballotContext.pending) setIsDraw(true);
  }, [ballotContext.pending]);

  useEffect(() => {
    if (isDraw) handleDraw();
  }, [isDraw]);

  return (
    <>
      <FullCalendar
        dateClick={handleAdd}
        myRef={calendarRef}
        eventClick={handleEdit}
        initialDate={ballotContext.currentDate.toSQLDate()}
        events={events}
        headerToolbar={{ right: "prev,today,next" }}
        customButtons={{
          prev: {
            click: handlePrev,
          },
          today: {
            text: "Hoy",
            click: handleToday,
          },
          next: {
            click: handleNext,
          },
        }}
      />
      {/* editar */}
      <BallotEdit
        isOpen={ballotContext.options == OptionsEnum.EDIT}
        onClose={() => ballotContext.setOptions(OptionsEnum.DEFAULT)}
        onSave={handleSave}
        onDelete={handleDelete}
      />
    </>
  );
};
