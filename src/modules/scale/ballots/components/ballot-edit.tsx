/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useContext, useEffect, useState } from "react";
import { AlertTriangle, Info, RefreshCw, Trash2 } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Badge,
} from "reactstrap";
import { deleteBallots, editBallots } from "../apis";
import { IBallotFormDto } from "../dtos/ballot-form.dto";
import { BallotForm } from "./ballot-form";
import { toast } from "react-toastify";
import { IBallotEntity } from "../dtos/ballot.entity";
import { plainToClass } from "class-transformer";
import { BallotSerialize } from "../serializers/ballot.serialize";
import { BallotAddDiscount } from "./ballot-add-discount";
import { BallotContext } from "./ballot-context";
import { Show } from "@common/show";
import { DiscountDetalle } from "@modules/scale/discounts/components/discount-detalle";
import { scaleDiscountActions } from "@modules/scale/discounts/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onDelete: (schedule: IBallotEntity) => void;
  onSave: (schedule: IBallotEntity) => void;
}

export const BallotEdit = ({ isOpen, onClose, onSave, onDelete }: IProps) => {
  const dispatch = useDispatch();

  const { ballot } = useSelector((state: RootState) => state.ballot);

  const [form, setForm] = useState<IBallotFormDto>(ballot as any);
  const [loading, setLoading] = useState<boolean>(false);
  const [addDiscount, setAddDiscount] = useState<boolean>(false);
  const [showDiscount, setShowDiscount] = useState<boolean>(false);

  const ballotContext = useContext(BallotContext);

  const hasDiscount = Object.keys(ballot.discount || {}).length > 0;

  const handleClose = () => {
    if (typeof onClose == "function") onClose();
    setForm(ballot as any);
  };

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    const formSerialize = plainToClass(BallotSerialize, form);
    // send data
    toast.dismiss();
    await editBallots(ballot?.id || 0, {
      ...form,
      departureTime: formSerialize.displayDepartureTime,
      returnTime: formSerialize.displayReturnTime,
    })
      .then((data) => {
        toast.success(`Los cambios se actualizarón correctamente`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo actualizar los datos`));
    setLoading(false);
  };

  const handleDelete = async () => {
    setLoading(true);
    toast.dismiss();
    await deleteBallots(ballot?.id || 0)
      .then(() => {
        toast.success(`El regístro se eliminó correctamente`);
        if (typeof onDelete == "function") onDelete(ballot);
      })
      .catch(() => toast.error(`No se pudo eliminar el regístro`));
    setLoading(false);
  };

  const handleAddDiscount = () => {
    ballotContext.setIsFetch(true);
    setAddDiscount(false);
    onClose();
  };

  const handleDiscountDetalle = () => {
    const currentDiscount = ballot.discount as any;
    dispatch(scaleDiscountActions.setDiscount(currentDiscount));
    setShowDiscount(true);
  };

  useEffect(() => {
    if (ballot?.id) setForm(ballot as any);
  }, [ballot]);

  return (
    <>
      <Modal isOpen={isOpen && !addDiscount && !showDiscount}>
        <ModalHeader toggle={handleClose}>Editar Papeleta</ModalHeader>
        <ModalBody>
          <BallotForm form={form} onChange={handleForm} isDisabled={loading} />
          <Show
            condition={!hasDiscount}
            isDefault={
              <div className="text-right">
                <Button
                  outline
                  color="dark"
                  title="Ver descuento"
                  onClick={handleDiscountDetalle}
                >
                  <Info className="icon" />
                </Button>
              </div>
            }
          >
            <div className="text-right">
              <Button
                color="dark"
                title="Añadir descuento"
                onClick={() => setAddDiscount(true)}
              >
                <AlertTriangle className="icon" />
              </Button>
            </div>
          </Show>
        </ModalBody>
        <ModalFooter className="text-right">
          <ButtonGroup>
            {/* delete */}
            <Button
              color="danger"
              disabled={loading}
              outline
              onClick={handleDelete}
            >
              <Trash2 className="icon" />
            </Button>
            {/* update */}
            <Button color="primary" disabled={loading} onClick={handleSave}>
              <RefreshCw className="icon" />
            </Button>
          </ButtonGroup>
        </ModalFooter>
      </Modal>
      {/* agregar descuento */}
      <BallotAddDiscount
        isOpen={addDiscount}
        onSave={handleAddDiscount}
        onClose={() => setAddDiscount(false)}
      />
      {/* info descuento */}
      <DiscountDetalle
        isOpen={showDiscount}
        onClose={() => setShowDiscount(false)}
      />
    </>
  );
};
