import { DiscountSerialize } from "@modules/scale/discounts/serializers/discount.serialize";
import { plainToClass } from "class-transformer";
import { Save } from "react-feather";
import {
  Button,
  Form,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useAddDiscount } from "../hooks/use-add-discount";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const BallotAddDiscount = ({ isOpen, onClose, onSave }: IProps) => {
  const { form, pending, handleForm, isValid, save } = useAddDiscount();

  const discount = plainToClass(DiscountSerialize, form);

  const handleSave = (e: any) => {
    e.preventDefault();
    if (!isValid()) return;
    save()
      .then(() => onSave())
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Agregar Descuento</ModalHeader>
      <Form onSubmit={handleSave}>
        <ModalBody>
          <FormGroup className="mb-3">
            <label>
              Hora esperada <b className="text-danger">*</b>
            </label>
            <Input
              type="time"
              name="presetTime"
              value={form.presetTime}
              onChange={({ target }) => handleForm(target)}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>
              Hora ejecutada <b className="text-danger">*</b>
            </label>
            <Input
              type="time"
              name="checkInTime"
              value={form.checkInTime}
              onChange={({ target }) => handleForm(target)}
            />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>
              Total Min <b className="text-danger">*</b>
            </label>
            <Input type="text" value={discount.displayTotal || 0} readOnly />
          </FormGroup>

          <FormGroup className="mb-3">
            <label>Observación</label>
            <Input
              type="textarea"
              name="observation"
              value={form.observation || ""}
              onChange={({ target }) => handleForm(target)}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter className="text-right">
          <Button color="success" disabled={!isValid() || pending}>
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
