/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IBallotFormDto } from "../dtos/ballot-form.dto";
import { IInputHandle } from "@common/dtos/input-handle";
import { plainToClass } from "class-transformer";
import { BallotSerialize } from "../serializers/ballot.serialize";
import { TypeBallotSelect } from "@modules/scale/type-ballots/components/type-ballot-select";

interface IProps {
  form: IBallotFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
  isEdit?: boolean;
}

export const BallotForm = ({ form, onChange, isDisabled, isEdit }: IProps) => {
  const handleTime = ({ name, value }: IInputHandle) => {
    const ballotSerialize: any = plainToClass(BallotSerialize, {
      ...form,
      [name]: value,
    });
    // options
    const options: any = {
      departureTime: "displayDepartureTime",
      presetTime: "displayPresetTime",
      returnTime: "displayReturnTime",
    };
    // nueve value
    const newValue: any = ballotSerialize[options[name]];
    // changa
    onChange({ name, value: newValue });
  };

  return (
    <Form>
      <FormGroup>
        <label>
          N° Papeleta <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="code"
          value={form?.code || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Tipo de Papeleta <b className="text-danger">*</b>
        </label>
        <TypeBallotSelect
          name="typeBallotId"
          value={form?.typeBallotId}
          onChange={onChange}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="date"
          value={form?.date || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled || isEdit}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Hora de Salida <b className="text-danger">*</b>
        </label>
        <Input
          type="time"
          name="departureTime"
          value={form?.departureTime}
          onChange={({ target }) => handleTime(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Hora de Retorno</label>
        <Input
          type="time"
          name="returnTime"
          value={form?.returnTime}
          onChange={({ target }) => handleTime(target)}
          disabled={isDisabled}
        />
      </FormGroup>
    </Form>
  );
};
