import { useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import { BallotContext, BallotProvider, OptionsEnum } from "./ballot-context";
import { BallotCreate } from "./ballot-create";
import { BallotList } from "./ballot-list";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const BallotWrapper = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();

  const ballotContext = useContext(BallotContext);

  const handleSave = () => {
    ballotContext.setOptions(OptionsEnum.DEFAULT);
    ballotContext.setIsFetch(true);
  };

  useEffect(() => {
    if (isOpen) ballotContext.setIsFetch(true);
  }, [isOpen]);

  return (
    <>
      <Modal isOpen={isOpen} size="lg" style={{ minWidth: "90%" }}>
        <ModalHeader toggle={onClose}>Papeletas</ModalHeader>
        <ModalBody>
          <BallotList />
        </ModalBody>
      </Modal>
      {/* crear schedule */}
      <BallotCreate
        dateDefault={ballotContext.currentDate}
        onClose={() => ballotContext.setOptions(OptionsEnum.DEFAULT)}
        isOpen={ballotContext.options == OptionsEnum.CREATE}
        onSave={handleSave}
      />
    </>
  );
};

export const BallotContent = (props: IProps) => {
  return (
    <BallotProvider>
      <BallotWrapper {...props} />
    </BallotProvider>
  );
};
