import { IInputHandle } from "@common/dtos/input-handle";
import { useContractBallotList } from "@modules/scale/contracts/hooks/use-contract-ballot-list";
import { DateTime } from "luxon";
import { createContext, FC, useEffect, useState } from "react";

export enum OptionsEnum {
  DEFAULT = "",
  CREATE = "CREATE",
  EDIT = "EDIT",
}

const date = DateTime.now();

export const BallotContext = createContext({
  pending: false,
  queryParams: {},
  options: OptionsEnum.DEFAULT,
  currentDate: date,
  setIsFetch: (value: boolean) => {},
  changeQueryParams: (value: IInputHandle) => {},
  setOptions: (value: OptionsEnum) => {},
  setCurrentDate: (date: DateTime) => {},
});

export const BallotProvider: FC = ({ children }) => {
  const [isFetch, setIsFetch] = useState<boolean>(false);
  const [options, setOptions] = useState<OptionsEnum>(OptionsEnum.DEFAULT);
  const [currentDate, setCurrentDate] = useState<DateTime>(date);
  const ballotList = useContractBallotList({
    page: 1,
    limit: 30,
    year: date.year,
    month: date.month,
  });

  useEffect(() => {
    if (isFetch) ballotList.fetch().catch(() => null);
  }, [isFetch]);

  useEffect(() => {
    if (isFetch) setIsFetch(false);
  }, [isFetch]);

  return (
    <BallotContext.Provider
      value={{
        options,
        pending: ballotList.pending,
        queryParams: ballotList.queryParams,
        currentDate,
        setIsFetch,
        changeQueryParams: ballotList.changeQueryParams,
        setOptions,
        setCurrentDate,
      }}
    >
      {children}
    </BallotContext.Provider>
  );
};
