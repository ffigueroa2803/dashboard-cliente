import { scaleRequest } from "@services/scale.request";
import { ICreateBallotDto } from "./dtos/create-ballot.dto";
import { IEditBallotDto } from "./dtos/edit-ballot.dto";
import { FilterGetBallotsDto } from "./dtos/filter-ballots.dto";
import { IBallotEntity } from "./dtos/ballot.entity";

const request = scaleRequest();

export const getBallotsToContract = async (
  id: number,
  { page, querySearch, limit, year, month }: FilterGetBallotsDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  if (month) params.set("month", `${month}`);
  // response
  return await request
    .get(`contracts/${id}/ballots`, { params })
    .then((res) => res.data);
};

export const createBallot = async (
  payload: ICreateBallotDto
): Promise<IBallotEntity> => {
  return await request.post(`ballots`, payload).then((res) => res.data);
};

export const editBallots = async (
  id: number,
  payload: IEditBallotDto
): Promise<IBallotEntity> => {
  return await request.put(`ballots/${id}`, payload).then((res) => res.data);
};

export const deleteBallots = async (
  id: number
): Promise<{ deleted: boolean }> => {
  return await request.destroy(`ballots/${id}`).then((res) => res.data);
};
