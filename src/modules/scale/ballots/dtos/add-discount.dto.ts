export interface IAddDiscountDto {
  checkInTime: string;
  presetTime: string;
  observation?: string;
}
