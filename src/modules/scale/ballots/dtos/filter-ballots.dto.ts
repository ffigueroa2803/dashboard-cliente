import { PaginateDto } from "@services/dtos";

export interface FilterGetBallotsDto extends PaginateDto {
  year?: number;
  month?: number;
}
