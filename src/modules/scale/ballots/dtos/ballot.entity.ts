import { IDiscountEntity } from "@modules/planilla/discounts/dtos/discount.entity";
import { ITypeBallotEntity } from "@modules/scale/type-ballots/dtos/type-ballot.entity";

export interface IBallotEntity {
  id: number;
  code: string;
  typeBallotId: number;
  contractId: number;
  discountId?: number;
  date: string;
  departureTime: string;
  returnTime?: string;
  discount?: IDiscountEntity;
  typeBallot?: ITypeBallotEntity;
}
