export interface ICreateBallotDto {
  typeBallotId: number;
  contractId: number;
  code: string;
  date: string;
  departureTime: string;
  presetTime: string;
  returnTime?: string;
}