export interface IEditBallotDto {
  typeBallotId: number;
  code: string;
  date: string;
  departureTime: string;
  presetTime: string;
  returnTime?: string | null;
}
