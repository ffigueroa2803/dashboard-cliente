import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IBallotEntity } from "./dtos/ballot.entity";

export interface BallotState {
  ballots: ResponsePaginateDto<IBallotEntity>;
  ballot: IBallotEntity;
  option: string;
}

export const initialState: BallotState = {
  ballots: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  option: "",
  ballot: {} as any,
};

const ballotStore = createSlice({
  name: "scale@ballot",
  initialState,
  reducers: {
    paginate: (
      state: BallotState,
      { payload }: PayloadAction<ResponsePaginateDto<IBallotEntity>>
    ) => {
      state.ballots = payload;
      return state;
    },
    setBallot: (
      state: BallotState,
      { payload }: PayloadAction<IBallotEntity>
    ) => {
      state.ballot = payload;
      return state;
    },
    changeOption: (state: BallotState, { payload }: PayloadAction<string>) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.ballot };
    },
  },
});

export const ballotReducer = ballotStore.reducer;

export const ballotActions = ballotStore.actions;
