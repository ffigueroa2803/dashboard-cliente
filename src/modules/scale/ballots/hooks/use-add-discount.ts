import { IInputHandle } from "@common/dtos/input-handle";
import { alerStatus } from "@common/errors/utils/alert-status";
import { scaleRequest } from "@services/scale.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { IAddDiscountDto } from "../dtos/add-discount.dto";

const request = scaleRequest();

export const useAddDiscount = () => {
  const { ballot } = useSelector((state: RootState) => state.ballot);

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IAddDiscountDto>({
    checkInTime: "",
    presetTime: "",
  });

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const isValid = () => {
    const datos = Object.keys(form).filter((attr) => {
      const obj: any = Object.assign(form, {});
      const value = obj[attr];
      return !value ? false : true;
    }).length;
    // validar
    return datos == 2;
  };

  const save = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      // send
      await request
        .post(`ballots/${ballot.id}/applyDiscount`, form)
        .then((res) => {
          resolve(res.data);
          toast.success(`Los datos se guardaron correctamente!`);
        })
        .catch((err) => {
          const status = err?.response?.data?.status || "";
          reject(err);
          if (status) return alerStatus(status);
          return toast.error(`algo salió mal`);
        });
      setPending(false);
    });
  };

  return {
    form,
    pending,
    handleForm,
    save,
    isValid,
  };
};
