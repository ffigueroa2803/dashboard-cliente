import { toast } from "react-toastify";
import { useCreateFolderMutation } from "../features/degree-rtk";

export const useDegreeFolderCreate = () => {
  const [fetch, { isLoading, isSuccess }] = useCreateFolderMutation();

  const handle = (id: number) => {
    fetch(id).unwrap().then((data) => {
      toast.success(`Folder creado!!!`)
      return data;
    }).catch((err) => toast.error(err?.data?.message || 'Algo salío mal!!!'))
  };

  return { isLoading, handle, isSuccess };
};
