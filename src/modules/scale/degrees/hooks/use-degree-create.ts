import { useFormik } from "formik";
import { toast } from "react-toastify";
import { degreeCreateInitital, degreeCreateYup } from "../dtos/degree-create.dto";
import { useCreateDegreesMutation } from "../features/degree-rtk";

export function useDegreeCreate() {

  const [fetch, { isLoading, isSuccess, data }] = useCreateDegreesMutation()

  const formik = useFormik({
    initialValues: degreeCreateInitital,
    validationSchema: degreeCreateYup,
    onSubmit: (values, helpers) => fetch(values)
      .unwrap()
      .then(() => {
        toast.success(`Los datos se guardaron correctamente!`);
        helpers.resetForm();
      }).catch((err) => {
        toast.error(err?.data?.message || 'Algo salió mal');
      })
  })

  return {
    isLoading,
    isSuccess,
    data,
    fetch,
    formik
  }
}