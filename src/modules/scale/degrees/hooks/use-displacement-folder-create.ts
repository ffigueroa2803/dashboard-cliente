/* eslint-disable no-async-promise-executor */
import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { IDisplacementEntityInterface } from "src/domain/scale/displacements/displacement.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = scaleRequest();

export const useDisplacementFolderCreate = (
  displacement: IDisplacementEntityInterface
) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFolderEntityInterface>(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .post(
          `displacements/${displacement.id}/folders`,
          {},
          { signal: abortController.signal }
        )
        .then(({ data }) => resolve(data))
        .catch((err) => reject(err));
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
