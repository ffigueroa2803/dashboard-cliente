import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";
import { useDeleteDegreeMutation } from "../features/degree-rtk";

export function useDegreeDelete() {
  const [fetch, { isLoading, isSuccess }] = useDeleteDegreeMutation();
  const confirm = useConfirm();

  const handle = (id: number) => {
    confirm.alert({
      title: "Eliminar",
      message: "¿Estás seguro?",
      labelSuccess: "Eliminar",
      labelError: "Cancelar"
    }).then(() => {
      fetch(id)
        .unwrap()
        .then(() => toast.success(`El registro se elimino!!!`))
        .catch((err) => toast.error(err?.data?.message || 'Algo salio mal!!!'))
    }).catch(() => null);
  }

  return {
    isLoading,
    isSuccess,
    handle
  }
}