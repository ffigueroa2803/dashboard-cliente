import { RootState } from "@store/store";
import { useFormik } from "formik";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { degreeEditInitital, degreeEditYup } from "../dtos/degree-edit.dto";
import { useEditDegreeMutation } from "../features/degree-rtk";

export function useDegreeEdit() {
  const [fetch, { isLoading, isSuccess, data }] = useEditDegreeMutation()

  const { degree } = useSelector((state: RootState) => state.degree);

  const id = useMemo(() => {
    return degree?.id || 0;
  }, [degree])

  const formik = useFormik({
    initialValues: degreeEditInitital,
    validationSchema: degreeEditYup,
    onSubmit: (body) => fetch({ id, body })
      .unwrap()
      .then(() => {
        toast.success(`Los datos se actualizaron correctamente!`);
      }).catch((err) => {
        toast.error(err?.data?.message || 'Algo salió mal');
      })
  })

  return {
    isLoading,
    isSuccess,
    data,
    fetch,
    formik
  }
}