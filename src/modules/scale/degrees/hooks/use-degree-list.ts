import { useState } from "react";
import { toast } from "react-toastify";
import { DegreePaginateParams } from "../domain/degree.params";
import { useLazyPaginateDegreesQuery } from "../features/degree-rtk";

export function useDegreeList(defaultFilters?: DegreePaginateParams) {
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateDegreesQuery();
  const [filters, setFilters] = useState<DegreePaginateParams>(
    Object.assign({ page: 1, limit: 100 }, defaultFilters || {})
  )

  const handle = () => {
    fetch(filters)
      .unwrap()
      .catch((err: any) => {
        toast.error(err.data?.message || "Algo salío mal")
      })
  }

  return {
    isFetching,
    isLoading,
    handle,
    filters,
    setFilters,
    data
  }
}