import * as Yup from "yup";

export const degreeCreateYup = Yup.object().shape({
  workId: Yup.number().required(),
  typeDegreeId: Yup.number().required(),
  institutionId: Yup.string().required(),
  dateStart: Yup.string().required(),
  dateOver: Yup.string().required(),
  dateDegree: Yup.string().required(),
  description: Yup.string().required()
});

export const degreeCreateInitital = {
  workId: 0,
  typeDegreeId: 0,
  institutionId: "",
  dateStart: "",
  dateOver: "",
  dateDegree: "",
  description: ""
}