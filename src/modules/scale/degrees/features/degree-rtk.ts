import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { FolderEntity } from "src/domain/storage/folders/folder.entity";
import { DegreeEntity } from "../domain/degree.entity";
import { DegreeCreateParams, DegreeEditParams, DegreePaginateParams } from "../domain/degree.params";

const baseUrl = process.env.NEXT_PUBLIC_SCALEV2_URL || "";

export const degreeRtk = createApi({
  reducerPath: "degreeRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    createDegrees: builder.mutation<DegreeEntity, DegreeCreateParams>({
      query: (body) => ({
        url: 'degrees',
        method: 'POST',
        body
      })
    }),
    paginateDegrees: builder.query<PaginateEntity<DegreeEntity>, DegreePaginateParams>({
      query: (params) => ({
        url: 'degrees',
        params
      })
    }),
    editDegree: builder.mutation<DegreeEntity, DegreeEditParams>({
      query: ({ id, body }) => ({
        url: `degrees/${id}`,
        method: 'PUT',
        body
      })
    }),
    deleteDegree: builder.mutation<number, number>({
      query: (id) => ({
        url: `degrees/${id}`,
        method: 'DELETE'
      })
    }),
    createFolder: builder.mutation<FolderEntity, number>({
      query: (id) => ({
        url: `degrees/${id}/folders`,
        method: 'POST'
      })
    })
  }),
});

export const {
  useLazyPaginateDegreesQuery,
  useCreateDegreesMutation,
  useEditDegreeMutation,
  useDeleteDegreeMutation,
  useCreateFolderMutation
} = degreeRtk;
