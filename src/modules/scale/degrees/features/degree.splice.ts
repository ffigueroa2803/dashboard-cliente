import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { DegreeEntity } from "../domain/degree.entity";

export interface DegreeState {
  degrees: ResponsePaginateDto<DegreeEntity>;
  degree: DegreeEntity | null;
  option: string;
}

const initialState: DegreeState = {
  degrees: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  degree: null,
  option: "",
};

const degreeStore = createSlice({
  name: "escalafon@degrees",
  initialState,
  reducers: {
    setPaginate: (state, { payload }: PayloadAction<ResponsePaginateDto<DegreeEntity>>) => {
      state.degrees = payload as any;
    },
    setDegree: (state, { payload }: PayloadAction<DegreeEntity | null>) => {
      state.degree = payload;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    updateItem: (state, { payload }: PayloadAction<DegreeEntity>) => {
      state.degrees.items = state.degrees.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.degree };
    },
  },
});

export const degreeReducer = degreeStore.reducer;

export const degreeActions = degreeStore.actions;
