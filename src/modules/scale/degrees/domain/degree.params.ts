export interface DegreeFindParams {
  id?: number;
  workId?: number;
}

export interface DegreeListParams extends DegreeFindParams {

}

export interface DegreePaginateParams extends DegreeListParams {
  page: number;
  limit: number;
}

export interface DegreeCreateParams {
  workId: number;
  typeDegreeId: number;
  institutionId: string;
  dateStart: string;
  dateOver: string;
  dateDegree: string;
  description: string;
}

export interface DegreeEditPayloadParams {
  typeDegreeId: number;
  institutionId: string;
  dateStart: string;
  dateOver: string;
  dateDegree: string;
  description: string;
}

export interface DegreeEditParams {
  id: number;
  body: DegreeEditPayloadParams
}