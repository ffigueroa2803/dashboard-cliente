import { WorkEntity } from "src/domain/scale/works/work.entity";

export interface DegreeEntity {
  id: number;
  workId: number;
  typeDegreeId: number;
  institutionId: string;
  description: string;
  dateStart: string;
  dateOver: string;
  dateDegree: string;
  folderId?: string;

  institution: any;
  typeDegree: any;
  work: WorkEntity;
}