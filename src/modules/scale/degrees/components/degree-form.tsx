import { InputComponent } from "@common/inputs/input.component";
import { InstitutionSelect } from "@modules/auth/institution/components/institution-select";
import { TypeDegreeSelect } from "@modules/scale/type-degrees/components/type-degree-select";
import { FormikProps } from "formik";
import { FormGroup } from "reactstrap";
import { DegreeCreateParams } from "../domain/degree.params";

export interface DegreeFormProps {
  formik: FormikProps<DegreeCreateParams>;
  isEdit?: boolean;
}

export function DegreeForm({ formik }: DegreeFormProps) {
  return (
    <div>
      <FormGroup className="mb-3">
        <label htmlFor="">Descripción</label>
        <InputComponent type="text" formik={formik} name="description" />
      </FormGroup>

      <FormGroup className="mb-3">
        <label htmlFor="">Tipo</label>
        <TypeDegreeSelect
          name="typeDegreeId"
          value={formik.values.typeDegreeId || ""}
          onChange={(target) => formik.handleChange({ target })}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label htmlFor="">Institución</label>
        <InstitutionSelect
          name="institutionId"
          value={formik.values.institutionId || ""}
          onChange={(target) => formik.handleChange({ target })}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label htmlFor="">Fecha Ingreso</label>
        <InputComponent type="date" formik={formik} name="dateStart" />
      </FormGroup>

      <FormGroup className="mb-3">
        <label htmlFor="">Fecha Cese</label>
        <InputComponent type="date" formik={formik} name="dateOver" />
      </FormGroup>

      <FormGroup className="mb-3">
        <label htmlFor="">Fecha de entrega Grado/Titulo</label>
        <InputComponent type="date" formik={formik} name="dateDegree" />
      </FormGroup>
    </div>
  );
}
