import { RootState } from "@store/store";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { degreeActions } from "../features/degree.splice";
import { DegreeCreate } from "./degree-create";
import { DegreeList } from "./degree-list";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const DegreeContent = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();
  const { option } = useSelector((state: RootState) => state.degree);

  return (
    <>
      <Modal isOpen={isOpen} size="lg" style={{ minWidth: "70%" }}>
        <ModalHeader toggle={onClose}>Gratos/Titulos</ModalHeader>
        <ModalBody>
          <DegreeList />
        </ModalBody>
        <ModalFooter className="text-right">
          <Button
            outline
            color="success"
            onClick={() => dispatch(degreeActions.changeOption("CREATE"))}
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* crear license */}
      <DegreeCreate
        onClose={() => dispatch(degreeActions.changeOption(""))}
        isOpen={option == "CREATE"}
        onSave={() => dispatch(degreeActions.changeOption("REFRESH"))}
      />
    </>
  );
};
