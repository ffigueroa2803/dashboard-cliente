/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Form, Modal, ModalBody, ModalHeader } from "reactstrap";
import { DegreeEntity } from "../domain/degree.entity";
import { useDegreeEdit } from "../hooks/use-degree-edit";
import { DegreeDelete } from "./degree-delete";
import { DegreeForm } from "./degree-form";

export interface DegreeEditProps {
  isOpen: boolean;
  onClose(): void;
  onSave(data: DegreeEntity): void;
}

export function DegreeEdit({ onClose, isOpen, onSave }: DegreeEditProps) {
  const { formik, isLoading, isSuccess, data } = useDegreeEdit();
  const { degree } = useSelector((state: RootState) => state.degree);

  useEffect(() => {
    if (degree?.id) formik.setValues(degree);
  }, [degree]);

  useEffect(() => {
    if (isSuccess && data) onSave(data);
  }, [isSuccess]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Crear Grados/Titulos</ModalHeader>
      <ModalBody>
        <Form onSubmit={formik.handleSubmit}>
          <DegreeForm formik={formik} />
          <div className="text-right">
            <Show
              condition={!isLoading}
              isDefault={
                <ButtonLoading color="primary" loading title="Guardando..." />
              }
            >
              {degree ? <DegreeDelete data={degree as any} /> : null}

              <Button
                color="primary"
                className="ml-2"
                type="submit"
                disabled={isLoading}
              >
                <Save className="icon" /> Guardar
              </Button>
            </Show>
          </div>
        </Form>
      </ModalBody>
    </Modal>
  );
}
