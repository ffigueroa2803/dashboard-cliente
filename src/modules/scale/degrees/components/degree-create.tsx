/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Form, Modal, ModalBody, ModalHeader } from "reactstrap";
import { DegreeEntity } from "../domain/degree.entity";
import { useDegreeCreate } from "../hooks/use-degree-create";
import { DegreeForm } from "./degree-form";

export interface DegreeCreateProps {
  isOpen: boolean;
  onClose(): void;
  onSave(data: DegreeEntity): void;
}

export function DegreeCreate({ onClose, isOpen, onSave }: DegreeCreateProps) {
  const { formik, isLoading, isSuccess, data } = useDegreeCreate();
  const { work } = useSelector((state: RootState) => state.work);

  useEffect(() => {
    if (work?.id) formik.setFieldValue("workId", work?.id);
  }, [work]);

  useEffect(() => {
    if (isSuccess && data) onSave(data);
  }, [isSuccess]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Crear Grados/Titulos</ModalHeader>
      <ModalBody>
        <Form onSubmit={formik.handleSubmit}>
          <DegreeForm formik={formik} />
          <div className="text-right">
            <Show
              condition={!isLoading}
              isDefault={
                <ButtonLoading color="primary" loading title="Guardando..." />
              }
            >
              <Button color="primary" type="submit" disabled={isLoading}>
                <Save className="icon" /> Guardar
              </Button>
            </Show>
          </div>
        </Form>
      </ModalBody>
    </Modal>
  );
}
