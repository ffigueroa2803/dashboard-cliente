import { Show } from "@common/show";
import { VacationUserError } from "@modules/scale/vacations/components/vacation-user-error";
import { RootState } from "@store/store";
import { Edit } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";
import { DegreeEntity } from "../domain/degree.entity";
import { degreeActions } from "../features/degree.splice";
import { DegreeFolderCreate } from "./degree-folder-create";

export interface DegreeItemProps {
  data: DegreeEntity;
}

export function DegreeItem({ data }: DegreeItemProps) {
  const dispatch = useDispatch();

  const { user } = useSelector((state: RootState) => state.person);

  const handleEdit = () => {
    dispatch(degreeActions.setDegree(data));
    dispatch(degreeActions.changeOption("EDIT"));
  };

  return (
    <div className="media">
      <Edit
        className="icon close"
        style={{ zIndex: 99 }}
        onClick={handleEdit}
      />

      <div className="media-body p-3">
        <div className="mb-2">
          <label>Tipo</label>
          <h6 className="capitalize">{data?.typeDegree?.name || ""}</h6>
        </div>

        <div className="mb-2">
          <label>Descripción</label>
          <h6 className="capitalize">{data?.description}</h6>
        </div>

        <div className="mb-2">
          <label>Institución</label>
          <h6 className="capitalize">{data?.institution?.name || ""}</h6>
        </div>

        <div className="mb-2">
          <label>F. Inicio</label>
          <h6 className="capitalize">{data?.dateStart || "--"}</h6>
        </div>

        <div className="mb-2">
          <label>F. Cese</label>
          <h6 className="capitalize">{data?.dateOver || "--"}</h6>
        </div>

        <div className="mb-2">
          <label>F. Diploma</label>
          <h6 className="capitalize">{data?.dateDegree || "--"}</h6>
        </div>

        <Show
          condition={user.id != undefined}
          isDefault={<VacationUserError />}
        >
          <Show
            condition={!data.folderId}
            isDefault={<FolderInfoComponent id={data.folderId || ""} />}
          >
            <DegreeFolderCreate data={data} />
          </Show>
        </Show>
      </div>
    </div>
  );
}
