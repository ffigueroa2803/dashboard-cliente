/* eslint-disable react-hooks/exhaustive-deps */
import { ButtonLoading } from "@common/button/button-loading";
import { useEffect } from "react";
import { Trash } from "react-feather";
import { useDispatch } from "react-redux";
import { Button } from "reactstrap";
import { DegreeEntity } from "../domain/degree.entity";
import { degreeActions } from "../features/degree.splice";
import { useDegreeDelete } from "../hooks/use-degree-delete";

export interface DegreeDeleteProps {
  data: DegreeEntity;
}

export function DegreeDelete({ data }: DegreeDeleteProps) {
  const dispatch = useDispatch();

  const { handle, isLoading, isSuccess } = useDegreeDelete();

  useEffect(() => {
    if (isSuccess) dispatch(degreeActions.changeOption("REFRESH"));
  }, [isSuccess]);

  if (isLoading) {
    return <ButtonLoading title="Eliminando" color="danger" loading />;
  }

  return (
    <Button color="danger" outline onClick={() => handle(data.id)}>
      <Trash className="icon" />
    </Button>
  );
}
