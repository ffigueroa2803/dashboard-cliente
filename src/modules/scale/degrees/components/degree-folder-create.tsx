/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { Button, Card } from "reactstrap";
import { DegreeEntity } from "../domain/degree.entity";
import { degreeActions } from "../features/degree.splice";
import { useDegreeFolderCreate } from "../hooks/use-degree-folder-create";

export interface DegreeFolderCreateProps {
  data: DegreeEntity;
}

export const DegreeFolderCreate = ({ data }: DegreeFolderCreateProps) => {
  const dispatch = useDispatch();
  const degreeFolder = useDegreeFolderCreate();

  const handle = () => degreeFolder.handle(data.id);

  useEffect(() => {
    if (degreeFolder.isSuccess) dispatch(degreeActions.changeOption("REFRESH"));
  }, [degreeFolder.isSuccess]);

  return (
    <Card className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={degreeFolder.isLoading}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </Card>
  );
};
