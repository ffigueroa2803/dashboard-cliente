/* eslint-disable react-hooks/exhaustive-deps */
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { degreeActions } from "../features/degree.splice";
import { useDegreeList } from "../hooks/use-degree-list";
import { DegreeEdit } from "./degree-edit";
import { DegreeItem } from "./degree-item";

export function DegreeList() {
  const { work } = useSelector((state: RootState) => state.work);
  const { option } = useSelector((state: RootState) => state.degree);

  const dispatch = useDispatch();

  const hook = useDegreeList({
    page: 1,
    limit: 100,
    workId: work?.id,
  });

  useEffect(() => {
    if (work?.id) hook.handle();
  }, [work]);

  useEffect(() => {
    if (option == "REFRESH") hook.handle();
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") dispatch(degreeActions.changeOption(""));
  }, [option]);

  return (
    <Row className="prooduct-details-box mb-3">
      {/* loading */}
      <Show condition={hook.isLoading || hook.isFetching}>
        <Col md="12" lg="12" className="text-center mb-5 mt-5">
          <LoadingSimple loading />
        </Col>
      </Show>
      {/* not data */}
      <Show
        condition={
          (!hook.isLoading || !hook.isFetching) && !hook.data?.items?.length
        }
      >
        <Col md="12" lg="12">
          <RegisterEmptyError
            onRefresh={() => dispatch(degreeActions.changeOption("REFRESH"))}
          />
        </Col>
      </Show>
      {/* data */}
      {hook.data?.items?.map((item) => (
        <Col key={`item-degree-${item.id}`} lg="6" md="6" className="mb-3">
          <DegreeItem data={item} />
        </Col>
      ))}
      {/* editar */}
      <DegreeEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(degreeActions.changeOption(""))}
        onSave={() => dispatch(degreeActions.changeOption("REFRESH"))}
      />
    </Row>
  );
}
