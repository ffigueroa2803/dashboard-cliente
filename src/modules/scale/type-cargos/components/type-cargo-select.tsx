/* eslint-disable no-unused-vars */
import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getTypeCargos } from "../apis";
import { ITypeCargoEntity } from "../dtos/type-cargo.entity";

interface IProps {
  onChange: (option: any) => void;
  onBlur?: (option: any) => void;
  value: any;
  name: string;
  placeholder?: string;
}

export const TypeCargoSelect = ({
  name,
  value,
  placeholder,
  onChange,
  onBlur,
}: IProps) => {
  return (
    <SelectRemote
      handle={getTypeCargos}
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      placeholder={placeholder}
      selectRow={{
        label: (row: ITypeCargoEntity) => `${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
