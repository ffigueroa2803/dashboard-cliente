/* eslint-disable no-unused-vars */
export enum DiscountStatusEnum {
  CREATED = "CREATED",
  JUSTIFIED = "JUSTIFIED",
  SEND = "SEND",
  APPLIED = "APPLIED",
}
