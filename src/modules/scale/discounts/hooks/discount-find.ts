import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { scaleDiscountActions } from "../store";

const request = scaleRequest();

export const useDiscountFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const fetch = (id: number) => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      await request
        .get(`discounts/${id}`)
        .then(({ data }) => {
          dispatch(scaleDiscountActions.setDiscount(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(scaleDiscountActions.setDiscount({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    isError,
    fetch,
  };
};
