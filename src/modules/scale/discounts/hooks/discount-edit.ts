/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-async-promise-executor */
import { useEffect, useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";
import { IEditDiscountDto } from "../dtos/edit-discount.dto";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";

const request = scaleRequest();

export const useDiscountEdit = () => {
  const { discount } = useSelector((state: RootState) => state.scaleDiscount);

  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const dataDefault = {
    presetTime: discount.presetTime || "",
    checkInTime: discount.checkInTime || "",
    observation: discount.observation || undefined,
  };

  const [form, setForm] = useState<IEditDiscountDto>(dataDefault);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const save = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      await request
        .put(`discounts/${discount.id}`, form)
        .then(({ data }) => {
          resolve(data);
          toast.success(`El descuento se actualizó correctamente!`);
        })
        .catch((err) => {
          setIsError(false);
          reject(err);
          // alert
          if (err?.response?.status) {
            return alerStatus(err?.response?.status);
          }
          // alert default
          return toast.error(`Algo salió mal...`);
        });
      setPending(false);
    });
  };

  useEffect(() => {
    if (discount) setForm(dataDefault);
  }, [discount]);

  return {
    form,
    pending,
    isError,
    handleForm,
    setForm,
    save,
  };
};
