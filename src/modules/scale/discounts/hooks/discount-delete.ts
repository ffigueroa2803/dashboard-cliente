/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";

const request = scaleRequest();

export const useDiscountDelete = () => {
  const { discount } = useSelector((state: RootState) => state.scaleDiscount);

  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = () => {
    return new Promise(async (resolve, reject) => {
      confirm
        .alert({
          title: "Eliminar Descuento",
          message: "¿Estás seguro en eliminar el descuento?",
          labelError: "Cancelar",
          labelSuccess: "Eliminar",
        })
        .then(async () => {
          setPending(true);
          setIsError(false);
          await request
            .destroy(`discounts/${discount.id}`)
            .then(({ data }) => {
              resolve(data);
              toast.success(`El descuento se elimino correctamente!`);
            })
            .catch((err) => {
              setIsError(false);
              reject(err);
              // alert
              if (err?.response?.status) {
                return alerStatus(err?.response?.status);
              }
              // alert default
              return toast.error(`Algo salió mal...`);
            });
          setPending(false);
        })
        .catch(() => null);
    });
  };

  return {
    pending,
    isError,
    destroy,
  };
};
