/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React from "react";
import { Form, FormGroup, Input } from "reactstrap";
import { IInputHandle } from "@common/dtos/input-handle";
import { plainToClass } from "class-transformer";
import { Clock } from "react-feather";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { ScheduleModeEnum } from "@modules/scale/schedules/dtos/schedule.entity";
import { ScheduleSerialize } from "@modules/scale/schedules/serializers/schedule.serialize";
import { Show } from "@common/show";
import { ContractScheduleSelect } from "@modules/scale/contracts/components/contract-schedule-select";
import { DiscountSerialize } from "../serializers/discount.serialize";
import { IDiscountFormDto } from "../dtos/discount-form.dto";

interface IProps {
  form: IDiscountFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
  isEdit?: boolean;
}

export const DiscountForm = ({
  form,
  onChange,
  isDisabled,
  isEdit = false,
}: IProps) => {
  const { discount } = useSelector((state: RootState) => state.scaleDiscount);
  const discountSerialize = plainToClass(DiscountSerialize, discount || {});

  const handleCheckInTime = ({ name, value }: IInputHandle) => {
    const assistanceSerialize = plainToClass(DiscountSerialize, {
      ...form,
      checkInTime: value,
    });

    onChange({ name, value: assistanceSerialize.displayCheckInTime });
  };

  const displayTotal = () => {
    const serialize = plainToClass(DiscountSerialize, form);
    return serialize.displayTotal;
  };

  return (
    <Form>
      <Show condition={isEdit}>
        <FormGroup>
          <label>Contracto</label>
          <h6>{discount?.contract?.code || "N/A"}</h6>
        </FormGroup>

        <FormGroup>
          <label>Tipo Descuento</label>
          <h6>{discountSerialize.displaydiscountableType || "N/A"}</h6>
        </FormGroup>

        <FormGroup>
          <label>Fecha</label>
          <h6>{discountSerialize.displayDate || "N/A"}</h6>
        </FormGroup>
      </Show>

      <FormGroup>
        <label>
          Hora Establecida <b className="text-danger">*</b>
        </label>
        <Input
          type="time"
          name="presetTime"
          value={form?.presetTime}
          onChange={({ target }) => handleCheckInTime(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Hora de regístro <b className="text-danger">*</b>
        </label>
        <Input
          type="time"
          name="checkInTime"
          value={form?.checkInTime}
          onChange={({ target }) => handleCheckInTime(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Total Minutos <b className="text-danger">*</b>
        </label>
        <Input type="text" name="checkInTime" readOnly value={displayTotal()} />
      </FormGroup>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>
    </Form>
  );
};
