import { RootState } from "@store/store";
import { useContext } from "react";
import { useSelector } from "react-redux";
import { DiscountEdit } from "./discount-edit";
import { DiscountTable } from "./discount-table";
import {
  DiscountContext,
  DiscountProvider,
  switchOptions,
} from "./discount-context";
import { DiscountDetalle } from "./discount-detalle";

export const DiscountWrapper = () => {
  const { setOptions, options } = useContext(DiscountContext);

  return (
    <>
      <div className="card card-body">
        <DiscountTable />
      </div>
      {/* edit */}
      <DiscountEdit
        isOpen={options == switchOptions.EDIT}
        onClose={() => setOptions("")}
      />
      {/* info */}
      <DiscountDetalle
        isOpen={options == switchOptions.INFO}
        onClose={() => setOptions("")}
      />
    </>
  );
};

export const DiscountContent = () => {
  return (
    <DiscountProvider>
      <DiscountWrapper />
    </DiscountProvider>
  );
};
