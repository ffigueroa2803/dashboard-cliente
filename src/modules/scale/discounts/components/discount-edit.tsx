/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { RootState } from "@store/store";
import { useEffect } from "react";
import { Save, Trash } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { DiscountForm } from "./discount-form";
import { IDiscountEntity } from "../dtos/discount.entity";
import { useDiscountEdit } from "../hooks/discount-edit";
import { useDiscountFind } from "../hooks/discount-find";
import { useDiscountDelete } from "../hooks/discount-delete";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (discount: IDiscountEntity) => void;
}

export const DiscountEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { discount } = useSelector((state: RootState) => state.scaleDiscount);

  const discountFind = useDiscountFind();
  const discountEdit = useDiscountEdit();
  const discountDelete = useDiscountDelete();

  const handleFetch = () => {
    discountFind.fetch(discount.id).catch(() => null);
  };

  const handleSave = () => {
    discountEdit
      .save()
      .then(() => handleFetch())
      .catch(() => null);
  };

  const handleDelete = () => {
    discountDelete
      .destroy()
      .then(() => onClose())
      .catch(() => null);
  };

  useEffect(() => {
    if (isOpen) handleFetch();
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Descuento</ModalHeader>
      <ModalBody>
        <DiscountForm
          isEdit={true}
          form={discountEdit.form}
          onChange={discountEdit.handleForm}
          isDisabled={discountEdit.pending}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        {/* delete */}
        <Button
          color="danger"
          outline
          disabled={discountDelete.pending}
          onClick={handleDelete}
        >
          <Trash className="icon" />
        </Button>
        {/* edit */}
        <Button
          color="primary"
          disabled={discountEdit.pending}
          onClick={handleSave}
        >
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
