/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { DateTime } from "luxon";
import { useRouter } from "next/router";
import { createContext, FC, useEffect, useState } from "react";
import { FilterGetDiscountsDto } from "../dtos/filter-discounts.dto";
import { useDiscountList } from "../hooks/discounts-list";

const currentDate = DateTime.now();

const currentQuery: FilterGetDiscountsDto = {
  page: 1,
  limit: 30,
  querySearch: "",
  dateStart: currentDate.toFormat("yyyy-MM-dd"),
  dateOver: currentDate.plus({ day: 1 }).toFormat("yyyy-MM-dd"),
};

export const DiscountContext = createContext({
  query: currentQuery,
  pending: false,
  options: "",
  setQuery: (filter: FilterGetDiscountsDto) => {},
  handleQuery: (option: IInputHandle) => {},
  onRefresh: () => {},
  setOptions: (value: string) => {},
  fetch: () => {},
});

export const switchOptions = {
  DEFAULT: "",
  CREATE: "CREATE",
  EDIT: "EDIT",
  INFO: "INFO",
};

export const DiscountProvider: FC = ({ children }) => {
  const discountList = useDiscountList({
    page: 1,
    limit: 30,
    dateStart: currentQuery.dateStart,
    dateOver: currentQuery.dateOver,
  });

  const router = useRouter();

  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [options, setOptions] = useState<string>(switchOptions.DEFAULT);

  const handleOnRefresh = () => {
    discountList.clearQueryToPage();
    setIsRefresh(true);
  };

  useEffect(() => {
    if (router) setIsRefresh(true);
  }, [router]);

  useEffect(() => {
    if (isRefresh) discountList.fetch().catch(() => null);
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <DiscountContext.Provider
      value={{
        query: discountList.query,
        pending: discountList.pending,
        options,
        handleQuery: discountList.handleQuery,
        setQuery: discountList.setQuery,
        onRefresh: handleOnRefresh,
        setOptions,
        fetch: () => setIsRefresh(true),
      }}
    >
      {children}
    </DiscountContext.Provider>
  );
};
