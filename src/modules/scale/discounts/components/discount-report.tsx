import { useContext, useState } from "react";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { Database, File } from "react-feather";
import urljoin from "url-join";
import { scaleRequest } from "@services/scale.request";
import { DiscountContext } from "./discount-context";
import { toast } from "react-toastify";
import { useProcessDiscountMutation } from "../features/discount-feature";

interface IActionItem {
  label: string;
  link: string;
  icon: any;
}

const request = scaleRequest();

export const DiscountReport = () => {
  const [fetch, { isLoading }] = useProcessDiscountMutation();
  const { query } = useContext(DiscountContext);

  const [isToggle, setIsToggle] = useState<boolean>(false);

  const onToggle = () => setIsToggle((prev) => !prev);

  const actions: IActionItem[] = [
    {
      icon: <File className="icon" />,
      label: "Reporte",
      link: urljoin(request.urlBase, `/discounts/reports/execute.pdf`),
    },
  ];

  const handleLink = (link: string) => {
    if (!canReport()) {
      return toast.info(
        `Debe haber un rango válido de fecha inicio y fecha fin`
      );
    }

    const queryString = new URLSearchParams();
    Object.keys(query).forEach((attr: any) => {
      const newForm: any = Object.assign({}, query);
      const value = newForm[attr];
      const allowed = ["typeCargoId", "condition"];
      // filter boolean
      if (allowed.includes(attr) && value) {
        queryString.append(`${attr}s[0]`, `${value}`);
      } else if (typeof value == "boolean") {
        queryString.set(attr, `${value}`);
      } else if (value) {
        queryString.set(attr, `${value}`);
      }
    });
    // crear elemento a
    const isQuery = /[?]/.test(link);
    const url = `${link}${isQuery ? "&" : "?"}${queryString.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  const handleProcess = () => {
    fetch(query)
      .unwrap()
      .then(() =>
        toast.success("Los descuentos está siendo procesados, vuelva más tarde")
      )
      .catch(() => toast.error("No se pudo procesar los descuentos"));
  };

  const canReport = () => {
    return query.dateStart && query.dateOver;
  };

  return (
    <Dropdown isOpen={isToggle} toggle={onToggle}>
      <DropdownToggle color="danger" outline caret className="btn-block">
        Opciones
      </DropdownToggle>
      <DropdownMenu>
        {actions.map((action, index) => (
          <DropdownItem
            key={`option-item-${index}`}
            onClick={() => handleLink(action.link)}
          >
            {action.icon}
            <span className="ml-2">{action.label}</span>
          </DropdownItem>
        ))}
        {/* procesar descuentos */}
        <DropdownItem onClick={handleProcess} disabled={isLoading}>
          <Database className="icon" />
          <span className="ml-2">Procesar</span>
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  );
};
