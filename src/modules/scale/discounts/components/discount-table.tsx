/* eslint-disable no-unused-vars */
import React, { useContext, useMemo } from "react";
import DataTable, { Direction } from "react-data-table-component";
import { Search, Edit2, Info, Check, CheckCircle } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { plainToClass } from "class-transformer";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { DiscountContext } from "./discount-context";
import { scaleDiscountActions } from "../store";
import { IDiscountEntity } from "../dtos/discount.entity";
import { DiscountSerialize } from "../serializers/discount.serialize";
import { Show } from "@common/show";
import { DiscountReport } from "./discount-report";

export enum DiscountActionEnum {
  EDIT = "EDIT",
  INFO = "INFO",
}

export const DiscountTable = () => {
  const dispatch = useDispatch();

  const { discounts } = useSelector((state: RootState) => state.scaleDiscount);

  const { query, setOptions, handleQuery, pending, fetch } =
    useContext(DiscountContext);

  const handleOnClick = (
    discount: IDiscountEntity,
    action: DiscountActionEnum
  ) => {
    dispatch(scaleDiscountActions.setDiscount(discount));
    setOptions(action);
  };

  const columns = useMemo(() => {
    return [
      {
        name: "#",
        grow: true,
        selector: (row: IDiscountEntity) => row.id,
      },
      {
        name: "Apellidos",
        wrap: true,
        selector: (row: IDiscountEntity) => {
          const contract = row?.contract;
          return (
            <span className="uppercase">
              <span>{contract?.work?.person?.lastname || ""}</span>
              <span className="ml-1">
                {contract?.work?.person?.secondaryName || ""}
              </span>
            </span>
          );
        },
      },
      {
        name: "Nombres",
        wrap: true,
        selector: (row: IDiscountEntity) => {
          const contract = row?.contract;
          return (
            <span className="uppercase">
              {contract?.work?.person?.name || ""}
            </span>
          );
        },
      },
      {
        name: "Tip. Cat",
        selector: (row: IDiscountEntity) => {
          const contract = row?.contract;
          return contract?.typeCategory?.name || "";
        },
      },
      {
        name: "Tipo",
        center: true,
        selector: (row: IDiscountEntity) => {
          const serialize = plainToClass(DiscountSerialize, row);
          return serialize.displaydiscountableType;
        },
      },
      {
        name: "Fecha",
        center: true,
        selector: (row: IDiscountEntity) => {
          const serialize = plainToClass(DiscountSerialize, row);
          return serialize.displayDate;
        },
      },
      {
        name: "H. Establecida",
        center: true,
        selector: (row: IDiscountEntity) => (
          <span className="badge badge-primary">{row.presetTime}</span>
        ),
      },
      {
        name: "H. Ejecutada",
        center: true,
        selector: (row: IDiscountEntity) => (
          <span className="badge badge-danger">{row.checkInTime}</span>
        ),
      },
      {
        name: "Total Min.",
        right: true,
        selector: (row: IDiscountEntity) => <b>{row.total} min</b>,
      },
      {
        name: "Estado",
        right: true,
        selector: (row: IDiscountEntity) => {
          const discountSerialize = plainToClass(DiscountSerialize, row);
          return (
            <span
              className={`badge badge-sm badge-${discountSerialize.displayStatusBg}`}
            >
              {discountSerialize.displayStatusText}
            </span>
          );
        },
      },
      {
        name: "Opciones",
        center: true,
        selector: (row: IDiscountEntity) => {
          const hasJustification =
            Object.keys(row.justification || {}).length > 0;
          return (
            <>
              <Info
                className="icon cursor-pointer"
                onClick={() => handleOnClick(row, DiscountActionEnum.INFO)}
              />
              {/* editar */}
              <Show
                condition={!hasJustification}
                isDefault={
                  <span title="Descuento justificado">
                    <CheckCircle className="icon cursor-pointer ml-2" />
                  </span>
                }
              >
                <Edit2
                  className="icon cursor-pointer ml-2"
                  onClick={() => handleOnClick(row, DiscountActionEnum.EDIT)}
                />
              </Show>
            </>
          );
        },
      },
    ];
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [discounts]);

  const isDisabled = () => {
    if (pending) return true;
    // dateStart
    if (!query.dateStart) return true;
    // dateOver
    if (!query.dateOver) return true;
    return false;
  };

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    fetch();
  };

  const handleOnPage = (page: number) => {
    handleQuery({ name: "page", value: page });
    fetch();
  };

  const handleOnLimit = (limit: number) => {
    handleQuery({ name: "limit", value: limit });
    fetch();
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                name="querySearch"
                value={query?.querySearch}
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
                placeholder="Buscar Apellidos y Nombres..."
              />
            </Col>
            {/* filtro cargo */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <TypeCargoSelect
                value={query?.typeCargoId || ""}
                name="typeCargoId"
                onChange={(target) => handleQuery(target)}
              />
            </Col>
            {/* filtro condition */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <ContractConditionSelect
                value={query?.condition}
                name="condition"
                onChange={(target) => handleQuery(target)}
              />
            </Col>
            {/* filter start date */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                value={query?.dateStart || ""}
                type="date"
                name="dateStart"
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
              />
            </Col>
            {/* filter over date */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                value={query?.dateOver || ""}
                type="date"
                name="dateOver"
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
              />
            </Col>
            <Col md="2 col-12 col-lg-2" className="mb-2">
              <Button color="primary" block disabled={isDisabled()}>
                <Search className="icon" />
              </Button>
            </Col>
            <Col md="2 col-12 col-lg-2" className="mb-2">
              <DiscountReport />
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={pending || false}
        data={discounts?.items || []}
        pagination
        paginationPerPage={query.limit || 30}
        paginationServer
        paginationTotalRows={discounts.meta?.totalItems || 30}
        onChangePage={handleOnPage}
        onChangeRowsPerPage={handleOnLimit}
        conditionalRowStyles={[
          {
            when: (row) => Object.keys(row.justification || {}).length > 0,
            classNames: ["text-mute bg-info text-white"],
          },
        ]}
      />
    </>
  );
};
