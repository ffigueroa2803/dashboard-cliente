/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo, useState } from "react";
import { RootState } from "@store/store";
import { Button, FormGroup, Modal, ModalBody, ModalHeader } from "reactstrap";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { plainToClass } from "class-transformer";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { FileText, Info } from "react-feather";
import { useDiscountFind } from "../hooks/discount-find";
import { DiscountSerialize } from "../serializers/discount.serialize";
import { JustificationCreate } from "@modules/scale/justifications/components/justification-create";
import { JustificationInfo } from "@modules/scale/justifications/components/justification-info";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const DiscountDetalle = ({ isOpen, onClose }: IProps) => {
  const discountFind = useDiscountFind();

  const { discount } = useSelector((state: RootState) => state.scaleDiscount);

  const [isDiscount, setIsDiscount] = useState<boolean>(false);

  const hasJustification: boolean =
    Object.keys(discount?.justification || {}).length > 0;

  const discountSerialize = useMemo(() => {
    if (!discount) return;
    return plainToClass(DiscountSerialize, discount || {});
  }, [discount]);

  const handleFetch = () => {
    discountFind.fetch(discount.id).catch(() => null);
  };

  const handleSave = () => {
    setIsDiscount(false);
    handleFetch();
  };

  useEffect(() => {
    if (isOpen) handleFetch();
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Detalle de Descuento</ModalHeader>
      <ModalBody>
        <Show
          condition={!discountFind.pending}
          isDefault={
            <div className="text-center">
              <LoadingSimple loading />
            </div>
          }
        >
          <FormGroup>
            <label htmlFor="">Contrato</label>
            <h6>{discount?.contract?.code || "N/A"}</h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Tipo</label>
            <h6>{discountSerialize?.displaydiscountableType || "N/A"}</h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Fecha</label>
            <h6 className="capitalize">{discountSerialize?.displayDate}</h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Hora Establecida</label>
            <h6 className="capitalize">
              {discountSerialize?.displayPresetTime}
            </h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Hora Registrada</label>
            <h6 className="capitalize">
              {discountSerialize?.displayCheckInTime}
            </h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Total Min</label>
            <h6>{discount.total} min</h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">Observación</label>
            <h6 className="capitalize">{discount.observation || "N/A"}</h6>
          </FormGroup>

          <Show condition={!hasJustification}>
            <div className="mt-3 text-right">
              <Button
                color="dark"
                title="Justificar Assistencia"
                onClick={() => setIsDiscount(true)}
              >
                <FileText className="icon" />
              </Button>
            </div>

            <JustificationCreate
              isOpen={isDiscount}
              onClose={() => setIsDiscount(false)}
              onSave={handleSave}
            />
          </Show>
        </Show>

        <Show condition={hasJustification}>
          <h6>
            <hr />
            <Info className="icon" /> Justificación
            <hr />
          </h6>
          <JustificationInfo id={discount?.justification?.id || 0} />
        </Show>
      </ModalBody>
    </Modal>
  );
};
