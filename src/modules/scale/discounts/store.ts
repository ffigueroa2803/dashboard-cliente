import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IDiscountEntity } from "./dtos/discount.entity";

export interface DiscountState {
  discounts: ResponsePaginateDto<IDiscountEntity>;
  discount: IDiscountEntity;
  option: string;
}

export const initialState: DiscountState = {
  discounts: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  option: "",
  discount: {} as any,
};

const discountStore = createSlice({
  name: "scale@discounts",
  initialState,
  reducers: {
    paginate: (
      state: DiscountState,
      { payload }: PayloadAction<ResponsePaginateDto<IDiscountEntity>>
    ) => {
      state.discounts = payload;
      return state;
    },
    updateDiscountItem: (
      state: DiscountState,
      { payload }: PayloadAction<IDiscountEntity>
    ) => {
      state.discounts?.items?.map((item) => {
        if (payload.id != item.id) return item;
        return Object.assign(item, payload);
      });
      return state;
    },
    setDiscount: (
      state: DiscountState,
      { payload }: PayloadAction<IDiscountEntity>
    ) => {
      state.discount = payload;
      state.discounts.items.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });

      return state;
    },
    findDiscount: (
      state: DiscountState,
      { payload }: PayloadAction<number>
    ) => {
      state.discount =
        state.discounts?.items?.find((item) => {
          return item.id == payload;
        }) || ({} as any);
      return state;
    },
    changeOption: (
      state: DiscountState,
      { payload }: PayloadAction<string>
    ) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.scaleDiscount };
    },
  },
});

export const scaleDiscountReducer = discountStore.reducer;

export const scaleDiscountActions = discountStore.actions;
