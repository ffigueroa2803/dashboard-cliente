import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { DiscountProcessDto } from "../dtos/discount-process.dto";

const baseUrl = process.env.NEXT_PUBLIC_SCALEV2_URL || "";

export const discountRtk = createApi({
  reducerPath: "discountRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    processDiscount: builder.mutation<any, DiscountProcessDto>({
      query: (body) => ({
        url: `discounts/process`,
        method: "POST",
        headers: BaseHeaders,
        body,
      }),
    }),
  }),
});

export const { useProcessDiscountMutation } = discountRtk;
