import { DateTime } from "luxon";
import { DiscountStatusEnum } from "../hooks/discount.enum";

export class DiscountSerialize {
  public status!: DiscountStatusEnum;
  public checkInTime: string = "";
  public presetTime: string = "";
  public presetDate: string = "";
  public discountableType!: string;

  private statusOptions = {
    CREATED: { text: "creado", bg: "light" },
    JUSTIFIED: { text: "Justificado", bg: "warning" },
    SEND: { text: "Envidado", bg: "info" },
    APPLIED: { text: "Aplicado", bg: "success" },
  };

  get displayCheckInTime() {
    let format = DateTime.fromFormat(this.checkInTime, "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.checkInTime, "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  get displayPresetTime() {
    let format = DateTime.fromFormat(this.presetTime, "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.presetTime, "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  get displaydiscountableType() {
    const texts: any = {
      AssistanceEntity: "Tardanza",
      ConfigHourhandEntity: "Falta",
      BallotEntity: "Papeleta",
    };
    // response
    return texts[this.discountableType] || this.discountableType;
  }

  get displayDate() {
    let format = DateTime.fromFormat(this.presetDate, "yyyy-MM-dd");
    // response
    return format.toFormat("dd/MM/yyyy");
  }

  get displayTotal() {
    const checkInTime = DateTime.fromFormat(this.displayCheckInTime, "HH:mm");
    const presetTime = DateTime.fromFormat(this.displayPresetTime, "HH:mm");
    // response
    return checkInTime.diff(presetTime, "minutes").minutes;
  }

  get displayStatusText() {
    return this.statusOptions[this.status].text || "";
  }

  get displayStatusBg() {
    return this.statusOptions[this.status].bg || "";
  }
}
