import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { IJustificationEntityInterface } from "src/domain/scale/justifications/justification.entity.interface";

export interface IDiscountEntity {
  id: number;
  contractId: number;
  presetDate: string;
  presetTime: string;
  checkInDate: string;
  checkInTime: string;
  discountableId: number;
  discountableType: string;
  total: number;
  observation?: string;
  contract?: IContractEntity;
  justification?: IJustificationEntityInterface;
}

export const discountEntityName = "DiscountEntity";
