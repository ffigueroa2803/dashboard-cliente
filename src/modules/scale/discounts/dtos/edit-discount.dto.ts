export interface IEditDiscountDto {
  presetTime: string;
  checkInTime: string;
  observation?: string;
}
