export interface DiscountProcessDto {
  dateStart: string;
  dateOver: string;
}