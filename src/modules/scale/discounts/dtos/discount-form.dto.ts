export interface IDiscountFormDto {
  presetTime: string;
  checkInTime: string;
  observation?: string;
}
