import { PaginateDto } from "@services/dtos";

export interface FilterGetDiscountsDto extends PaginateDto {
  year?: number;
  month?: number;
  dateStart: string;
  dateOver: string;
  condition?: any;
  typeCargoId?: any;
}
