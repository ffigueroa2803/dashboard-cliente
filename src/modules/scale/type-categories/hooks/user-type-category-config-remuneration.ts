import { IConfigTypeDiscountTableProvider } from "@modules/planilla/config-type-discounts/interfaces/config-type-discount-table.interface";
import { planillaRequest } from "@services/planilla.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { initialState, typeCategoryActions } from "../store";

const request = planillaRequest();

export const useTypeCategoryConfigRemuneration =
  (): IConfigTypeDiscountTableProvider => {
    const dispatch = useDispatch();

    const { type_categoria } = useSelector(
      (state: RootState) => state.type_categoria
    );
    const { planilla } = useSelector((state: RootState) => state.planilla);
    const { configRemunerations } = useSelector(
      (state: RootState) => state.type_categoria
    );

    const [isError, setIsError] = useState(false);
    const [pending, setPending] = useState(false);

    const fetch = async () => {
      setPending(true);
      setIsError(false);
      await request
        .get(
          `typeCategories/${type_categoria.id}/configRemunerations?planillaIds[0]=${planilla.id}&limit=100`
        )
        .then(({ data }) =>
          dispatch(typeCategoryActions.configRemunerationPaginate(data))
        )
        .catch(() => {
          setIsError(true);
          dispatch(
            typeCategoryActions.configRemunerationPaginate(
              initialState.configRemunerations
            )
          );
        });
      setPending(false);
    };

    return { fetch, data: configRemunerations, pending, isError };
  };
