/* eslint-disable no-async-promise-executor */
import { IInputHandle } from "@common/dtos/input-handle";
import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { find } from "../store";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

const request = scaleRequest();

export const useTypeCategoryEntityUpdate = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const { type_categoria } = useSelector(
    (state: RootState) => state.type_categoria
  );
  const [form, setForm] = useState<ITypeCategoryEntity>(type_categoria);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({ ...prev, [name]: value }));
  };

  const update = async (
    id: number,
    payload: ITypeCategoryEntity
  ): Promise<ITypeCategoryEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .put(`typeCategories/${id}`, payload)
        .then((res) => {
          resolve(res.data as ITypeCategoryEntity);
          toast.success("Los datos se guardaron correctamente");
          dispatch(find(res.data));
        })
        .catch((err) => {
          reject(err);
          dispatch(find({} as any));
          toast.error("No se pudo guardar los datos");
        });
      setPending(false);
    });
  };

  return {
    pending,
    update,
    changeForm,
    setForm,
    form,
  };
};
