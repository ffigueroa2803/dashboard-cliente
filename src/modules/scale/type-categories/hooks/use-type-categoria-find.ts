/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { find } from "../store";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

const request = scaleRequest();

export const useTypeCategoriaFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);

  const fetch = (id: number): Promise<ITypeCategoryEntity> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .get(`typeCategories/${id}`)
        .then((res) => {
          resolve(res.data as ITypeCategoryEntity);
          dispatch(find(res.data));
        })
        .catch((err) => {
          reject(err);
          dispatch(find({} as any));
        });
      setPending(false);
    });
  };
  return {
    pending,
    fetch,
  };
};
