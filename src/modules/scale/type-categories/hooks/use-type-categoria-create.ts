import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { ICreateTypeCategoriaDto } from "../dtos/create-type_categoria.dto";
import { toast } from "react-toastify";
import { IInputHandle } from "@common/dtos/input-handle";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

const request = scaleRequest();

const defaultData: ICreateTypeCategoriaDto = {
  dedication: "",
  description: "",
  name: "",
  state: false,
  typeCargoId: 0,
};

export const useTypeCategoriaCreate = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ICreateTypeCategoriaDto>(defaultData);

  const clearForm = () => setForm(defaultData);

  const changeForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({ ...prev, [name]: value }));
  };

  const save = async (
    payload: ICreateTypeCategoriaDto
  ): Promise<ITypeCategoryEntity> => {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
      setPending(true);
      await request
        .post(`typeCategories`, payload)
        .then((res) => {
          toast.success("Los datos se guardaron correctamente");
          resolve(res.data as ITypeCategoryEntity);
        })
        .catch((err) => {
          reject(err);
          toast.error("No se pudo guardar los datos");
        });
      setPending(false);
    });
  };
  return {
    pending,
    save,
    form,
    setForm,
    clearForm,
    changeForm,
  };
};
