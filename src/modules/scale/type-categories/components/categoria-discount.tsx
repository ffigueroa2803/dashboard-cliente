/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Settings, Plus } from "react-feather";
import {
  Button,
  Col,
  FormGroup,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { PlanillaSelect } from "@modules/planilla/planillas/components/planilla-select";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { planillaActions } from "@modules/planilla/planillas/store";
import { IPlanillaEntity } from "@modules/planilla/planillas/dtos/planilla.entity";
import { Show } from "@common/show";
import { useTypeCategoryConfigDiscount } from "../hooks/user-type-category-config-discount";
import { ConfigTypeDiscountCreate } from "@modules/planilla/config-type-discounts/components/config-type-discount-create";
import ConfigTypeDiscountTable from "@modules/planilla/config-type-discounts/components/config-type-discount-table";

interface IProps {
  onClose: () => void;
}

export const CategoriaDiscount = ({ onClose }: IProps) => {
  const dispatch = useDispatch();

  const { planilla } = useSelector((state: RootState) => state.planilla);

  const { type_categoria } = useSelector(
    (state: RootState) => state.type_categoria
  );

  const typeCategoryRemuneration = useTypeCategoryConfigDiscount();

  const [viewModal, setViewModal] = useState(false);

  const handlePlanilla = ({ row }: { row: IPlanillaEntity }) => {
    dispatch(planillaActions.setPlanilla(row));
  };

  return (
    <Modal isOpen={true} size="lg">
      <ModalHeader toggle={onClose}>
        Configurar Retenciones Tip. Categoría
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <label>Nombre</label>
          <h6 className="uppercase">{type_categoria?.name || "N/A"}</h6>
        </FormGroup>
        <FormGroup>
          <label>Dedicación</label>
          <h6 className="uppercase">{type_categoria?.dedication || "N/A"}</h6>
          <label className="w-100">
            <hr />
            <Settings className="icon" /> Configuración de Planilla
            <hr />
          </label>
        </FormGroup>

        <FormGroup>
          <Row>
            <Col md="10">
              <PlanillaSelect
                name="planillaId"
                onChange={handlePlanilla}
                value={planilla?.id || ""}
              />
            </Col>
            <Col md="2">
              <Button
                color="primary"
                outline
                disabled={!planilla?.id}
                onClick={() => setViewModal(true)}
              >
                <Plus className="icon" />
              </Button>
            </Col>
          </Row>
        </FormGroup>

        {/* crear config */}
        <Show condition={viewModal}>
          <ConfigTypeDiscountCreate
            onSave={typeCategoryRemuneration.fetch}
            open={true}
            setOpen={() => setViewModal(false)}
          />
        </Show>

        {/* listar configs */}
        <Show condition={planilla?.id != undefined}>
          <ConfigTypeDiscountTable provider={typeCategoryRemuneration} />
        </Show>
      </ModalBody>
    </Modal>
  );
};
