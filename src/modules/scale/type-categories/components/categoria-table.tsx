/* eslint-disable no-unused-vars */
import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { Search, DollarSign, Edit3, Tag } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

declare type onQuerySearch = (querySearch: string | string[]) => void;

declare type onClick = (ITypeCategoryEntity: ITypeCategoryEntity) => void;

interface IProps {
  data: ITypeCategoryEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  defaultQuerySearch?: string | string[];
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: onQuerySearch;
  onClick?: {
    edit: (type_Categoria: ITypeCategoryEntity) => void;
    mont: (type_Categoria: ITypeCategoryEntity) => void;
    discount: (type_Categoria: ITypeCategoryEntity) => void;
    delet: (type_Categoria: ITypeCategoryEntity) => void;
  };
}

export const CategoriaTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultQuerySearch,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
}: IProps) => {
  const [querySearch, setQuerySearch] = useState<string | string[]>(
    defaultQuerySearch || ""
  );

  const columns = useMemo(() => {
    return [
      {
        name: "#ID",
        selector: (row: ITypeCategoryEntity) => row.id,
      },
      {
        name: "Descripción",
        selector: (row: ITypeCategoryEntity) =>
          `${row?.name || ""}`.toUpperCase(),
      },
      {
        name: "Dedicación",
        selector: (row: ITypeCategoryEntity) =>
          `${row?.dedication || ""}`.toUpperCase(),
      },
      {
        name: "Estado",
        selector: (row: ITypeCategoryEntity) => (
          <span className={`badge badge-${row.state ? "success" : "danger"}`}>
            {row.state ? "Activo" : "Inactivo"}
          </span>
        ),
      },
      {
        name: "Opción",
        selector: (row: ITypeCategoryEntity) => (
          <>
            {" "}
            <Edit3
              className="cursor-pointer icon"
              onClick={() => onClick?.edit(row)}
            />
            <DollarSign
              className="cursor-pointer icon"
              onClick={() => onClick?.mont(row)}
            />
            <Tag
              className="cursor-pointer icon"
              onClick={() => onClick?.discount(row)}
            />
          </>
        ),
      },
    ];
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(querySearch);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="10 col-9" className="mb-2">
              <Input
                value={querySearch}
                onChange={({ target }) => setQuerySearch(target.value)}
                disabled={loading}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button
                color="primary"
                block
                disabled={loading}
                onClick={handleSearch}>
                <Search size={15} />
              </Button>
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
