/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import React from "react";
import { FormGroup, Input } from "reactstrap";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

interface IProps<T> {
  form: T;
  onChange: (input: IInputHandle) => void;
  isEdit?: boolean;
}

export const CategoriaData = ({ form, onChange, isEdit }: IProps<ITypeCategoryEntity>) => {
  return (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>IDescripción</label>
          <Input
            type="text"
            className="capitalize"
            // value={form?.name}
            name="name"
            // onChange={({ target }) => onChange(target)}
          />
        </FormGroup>
        <FormGroup>
          <label>Dedicación</label>
          <Input
            type="text"
            className="capitalize"
            // value={form?.name}
            name="name"
            // onChange={({ target }) => onChange(target)}
          />
        </FormGroup>
      </div>
    </div>
  );
};
