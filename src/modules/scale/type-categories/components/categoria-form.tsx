/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import React from "react";
import { FormGroup, Input } from "reactstrap";
import { ICreateTypeCategoriaDto } from "../dtos/create-type_categoria.dto";
import Toggle from "@atlaskit/toggle";

interface IProps<T> {
  form: T;
  onChange: (input: IInputHandle) => void;
  isEdit?: boolean;
}
export const CategoriaForm = ({
  form,
  onChange,
  isEdit,
}: IProps<ICreateTypeCategoriaDto>) => {
  return (
    <div className="mt-2">
      <div className="mb-3">
        <FormGroup>
          <label>Informacion Detallada</label>
          <Input
            type="textarea"
            className="capitalize"
            value={form?.name}
            name="name"
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>
        <FormGroup>
          <label>Descripción</label>
          <Input
            type="text"
            className="capitalize"
            value={form?.description}
            name="description"
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>

        <FormGroup>
          <label>Dedicación</label>
          <Input
            type="text"
            className="capitalize"
            value={form?.dedication}
            name="dedication"
            onChange={({ target }) => onChange(target)}
          />
        </FormGroup>
        <FormGroup>
          <label>Tipo de Cargo</label>
          <TypeCargoSelect
            name="typeCargoId"
            onChange={(a) => onChange(a)}
            value={form.typeCargoId}
          />
        </FormGroup>
        <FormGroup>
          <label>Estado</label>
          <Toggle
            name="state"
            isChecked={form?.state || false}
            onChange={({ target }) =>
              onChange({
                name: target.name,
                value: target.checked,
              })
            }
          />
        </FormGroup>

        <hr />
      </div>
    </div>
  );
};
