/* eslint-disable no-unused-vars */
import React from "react";
import { ArrowLeft, Save } from "react-feather";
import { useSelector } from "react-redux";
import { Modal, ModalHeader, ModalBody, Row, Col, Button } from "reactstrap";
import { CategoriaForm } from "./categoria-form";
import { RootState } from "@store/store";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";
import { useTypeCategoryEntityUpdate } from "../hooks/use-type-categoria.update";

interface IProps {
  onClose: () => void;
  onSave: (type_categoria: ITypeCategoryEntity) => void;
}

export const CategoriaEdit = ({ onClose, onSave }: IProps) => {
  const { type_categoria } = useSelector(
    (state: RootState) => state.type_categoria
  );
  const { changeForm, form, pending, update } = useTypeCategoryEntityUpdate();
  const handleSave = async () => {
    await update(type_categoria.id, form).then((data) => {
      onClose();
      if (typeof onSave === "function") onSave(data);
    });
  };

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Editar Categoría</ModalHeader>
      <ModalBody>
        <CategoriaForm form={form} onChange={changeForm} />
        <Row className="justify-content-center">
          <Col md="6 col-6 text-left">
            <Button
              color="secundary"
              title="Atras"
              onClick={() => onClose()}
              disabled={pending}
            >
              <ArrowLeft size={17} />
            </Button>
          </Col>

          <Col md="6 col-6 text-right">
            <Button
              color="primary"
              title="Guardar datos"
              onClick={handleSave}
              disabled={pending}
            >
              <Save size={17} />
            </Button>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
