/* eslint-disable no-unused-vars */
import React from "react";
import { Col, Modal, ModalBody, ModalHeader, Row, Button } from "reactstrap";
import { Save, ArrowLeft } from "react-feather";
import { CategoriaForm } from "./categoria-form";
import { useTypeCategoriaCreate } from "../hooks/use-type-categoria-create";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

interface IProps {
  onClose: () => void;
  onSave: (typecategoria: ITypeCategoryEntity) => void;
}

export const CategoriaCreate = ({ onClose, onSave }: IProps) => {
  const { save, changeForm, form, clearForm, pending } =
    useTypeCategoriaCreate();

  const handleSave = async () => {
    if (typeof form == "undefined") return;
    await save(form).then((res) => {
      clearForm();
      onClose();
      if (typeof onSave === "function") onSave(res);
    });
  };

  return (
    <Modal isOpen={true}>
      <ModalHeader toggle={onClose}>Crear Categoría</ModalHeader>
      <ModalBody>
        <CategoriaForm form={form} onChange={changeForm} />
        <Row className="justify-content-center">
          <Col md="6 col-6 text-left">
            <Button
              color="secundary"
              title="Atras"
              onClick={() => onClose()}
              disabled={pending}
            >
              <ArrowLeft size={17} />
            </Button>
          </Col>

          <Col md="6 col-6 text-right">
            <Button
              color="primary"
              title="Guardar datos"
              onClick={handleSave}
              disabled={pending}
            >
              <Save size={17} />
            </Button>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
