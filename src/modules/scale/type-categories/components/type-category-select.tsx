/* eslint-disable no-unused-vars */
import React from "react";
import { SelectRemote } from "@common/select/select-remote";
import { getTypeCategories } from "../apis";
import { ITypeCategoryEntity } from "../dtos/type-category.entity";

interface IProps {
  onChange: (option: any) => void;
  onBlur?: (option: any) => void;
  value: any;
  name: string;
  placeholder?: string;
  defaultQuerySearch?: string;
}

export const TypeCategorySelect = ({
  name,
  value,
  placeholder,
  onChange,
  onBlur,
  defaultQuerySearch,
}: IProps) => {
  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={getTypeCategories}
      name={name}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      placeholder={placeholder}
      selectRow={{
        label: (row: ITypeCategoryEntity) =>
          `${row.name.toUpperCase()} ${
            row.dedication ? `- ${row.dedication}` : ""
          }`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
