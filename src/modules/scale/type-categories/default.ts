import { Store } from "redux";
import { paginate } from "@modules/scale/type-categories/store";
import { getTypeCategories } from "@modules/scale/type-categories/apis";

export const configDefaultServer = async (ctx: any, store: Store) => {
  const page = ctx.query?.page || 1;
  const querySearch = ctx.query?.querySearch || "";
  const limit = ctx.query?.limit || 30;
  const result = await getTypeCategories({ page, querySearch, limit });
  store.dispatch(paginate(result));
};
