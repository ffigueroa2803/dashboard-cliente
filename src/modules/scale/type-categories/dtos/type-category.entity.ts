import { ITypeCargoEntity } from "@modules/scale/type-cargos/dtos/type-cargo.entity";

export interface ITypeCategoryEntity {
  id: number;
  name: string;
  dedication: string;
  description: string;
  state: boolean;
  typeCargoId: number;
  typeCargo?: ITypeCargoEntity;
}

export const typeCategoryEntityName = "TypeCategoryEntity";
