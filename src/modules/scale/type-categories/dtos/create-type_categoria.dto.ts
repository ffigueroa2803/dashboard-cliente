export interface ICreateTypeCategoriaDto {
  name?: string;
  dedication?: string;
  description?: string;
  state?: boolean;
  typeCargoId: number;
}
