import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { IConfigTypeRemunerationEntity } from "@modules/planilla/config-type-remunerations/domain/config-type-remuneration.entity";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { MetaDto } from "@services/dtos";
import { HYDRATE } from "next-redux-wrapper";
import { ITypeCategoryEntity } from "./dtos/type-category.entity";

export interface TypeCategoriaState {
  type_categorias: {
    meta: MetaDto;
    items: ITypeCategoryEntity[];
  };
  type_categoria: ITypeCategoryEntity | any;
  configRemunerations: ResponsePaginateDto<IConfigTypeRemunerationEntity>;
}

export const initialState: TypeCategoriaState = {
  type_categorias: {
    meta: {
      totalItems: 0,
      itemsPerPage: 0,
      totalPages: 0,
    },
    items: [],
  },
  type_categoria: {} as any,
  configRemunerations: {
    meta: {
      totalItems: 0,
      itemsPerPage: 0,
      totalPages: 0,
    },
    items: [],
  },
};
const TypeCategoriaStore = createSlice({
  name: "scale@type_categoria",
  initialState,
  reducers: {
    paginate: (
      state: TypeCategoriaState,
      { payload }: PayloadAction<ResponsePaginateDto<ITypeCategoryEntity>>
    ) => {
      state.type_categorias = payload;
    },
    find: (
      state: TypeCategoriaState,
      { payload }: PayloadAction<ITypeCategoryEntity | null>
    ) => {
      state.type_categoria = payload;
    },
    configRemunerationPaginate: (
      state: TypeCategoriaState,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IConfigTypeRemunerationEntity>>
    ) => {
      state.configRemunerations = payload;
      return state;
    },
    configDiscountPaginate: (
      state: TypeCategoriaState,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IConfigTypeRemunerationEntity>>
    ) => {
      state.configRemunerations = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.type_categoria };
    },
  },
});

export const type_categoriaReducer = TypeCategoriaStore.reducer;

export const typeCategoryActions = TypeCategoriaStore.actions;

export const { find, paginate } = TypeCategoriaStore.actions;
