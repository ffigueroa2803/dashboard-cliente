import { scaleRequest } from "@services/scale.request";
import { ICreateAscentDto } from "./dtos/create-ascent.dto";
import { IEditAscentDto } from "./dtos/edit-ascent.dto";
import { FilterGetAscentsDto } from "./dtos/filter-ascent.dto";
import { IAscentEntity } from "./dtos/ascent.entity";

const request = scaleRequest();

export const getAscentsToWork = async (
  id: number,
  { page, querySearch, limit, year }: FilterGetAscentsDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  // response
  return await request
    .get(`works/${id}/ascents`, { params })
    .then((res) => res.data);
};

export const createAscent = async (
  payload: ICreateAscentDto
): Promise<IAscentEntity> => {
  return await request.post(`ascents`, payload).then((res) => res.data);
};

export const editAscent = async (
  id: number,
  payload: IEditAscentDto
): Promise<IAscentEntity> => {
  return await request.put(`ascents/${id}`, payload).then((res) => res.data);
};

export const deleteAscent = async (id: number): Promise<any> => {
  return await request.destroy(`ascents/${id}`).then((res) => res.data);
};
