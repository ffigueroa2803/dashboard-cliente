export interface IAscentFormDto {
  contractSourceId: number;
  contractTargetId: number;
  observation?: string;
}
