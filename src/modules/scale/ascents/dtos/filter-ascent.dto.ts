import { PaginateDto } from "@services/dtos";

export interface FilterGetAscentsDto extends PaginateDto {
  year?: number;
}
