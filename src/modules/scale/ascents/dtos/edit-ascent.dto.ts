export interface IEditAscentDto {
  contractTargetId: number;
  observation?: string;
}
