import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";

export interface IAscentEntity {
  id: number;
  contractSourceId: number;
  contractTargetId: number;
  observation?: string;
  contractSource?: IContractEntity;
  contractTarget?: IContractEntity;
}
