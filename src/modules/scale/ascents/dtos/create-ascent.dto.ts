export interface ICreateAscentDto {
  contractSourceId: number;
  contractTargetId: number;
  observation?: string;
}
