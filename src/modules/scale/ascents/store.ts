import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IAscentEntity } from "./dtos/ascent.entity";

export interface AscentState {
  ascents: ResponsePaginateDto<IAscentEntity>;
  ascent: IAscentEntity;
  option: string;
}

const initialState: AscentState = {
  ascents: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  ascent: {} as any,
  option: "",
};

const ascentStore = createSlice({
  name: "escalafon@ascent",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IAscentEntity>>
    ) => {
      state.ascents = payload as any;
    },
    setAscent: (state, { payload }: PayloadAction<IAscentEntity>) => {
      state.ascent = payload;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.ascent };
    },
  },
});

export const ascentReducer = ascentStore.reducer;

export const ascentActions = ascentStore.actions;
