/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Save } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { createAscent } from "../apis";
import { IAscentFormDto } from "../dtos/ascent-form.dto";
import { AscentForm } from "./ascent-form";
import { toast } from "react-toastify";
import { IAscentEntity } from "../dtos/ascent.entity";
import { contractActions } from "@modules/scale/contracts/store";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (ascent: IAscentEntity) => void;
}

export const AscentCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const dataDefault: IAscentFormDto = {
    contractSourceId: 0,
    contractTargetId: 0,
    observation: "",
  };

  const [form, setForm] = useState<IAscentFormDto>(dataDefault);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await createAscent(form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente`);
        setForm(dataDefault);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setLoading(false);
  };

  useEffect(() => {
    if (!isOpen) setForm(dataDefault);
  }, [isOpen]);

  useEffect(() => {
    if (isOpen) dispatch(contractActions.find({} as any));
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nuevo Acenso</ModalHeader>
      <ModalBody>
        {/* formulario */}
        <AscentForm form={form} onChange={handleForm} isDisabled={loading} />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={loading} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
