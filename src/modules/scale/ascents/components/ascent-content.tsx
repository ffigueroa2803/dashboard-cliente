import { RootState } from "@store/store";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { ascentActions } from "../store";
import { AscentCreate } from "./ascent-create";
import { AscentList } from "./ascent-list";

interface IProps {
  isOpen: boolean
  onClose: () => void
}

export const AscentContent = ({ isOpen, onClose }: IProps) => {

  const dispatch = useDispatch();
  const { option } = useSelector((state: RootState) => state.ascent);

  return (
    <>
      <Modal isOpen={isOpen}
        size="lg"
        style={{ minWidth: "90%" }}
      >
        <ModalHeader toggle={onClose}>
          Acensos
        </ModalHeader>
        <ModalBody>
          <AscentList/>
        </ModalBody>
        <ModalFooter className="text-right">
          <Button outline
            color="success"
            onClick={() => dispatch(ascentActions.changeOption("CREATE"))}
          >
            <Plus className="icon"/>
          </Button>
        </ModalFooter>
      </Modal>
      {/* crear license */}
      <AscentCreate 
        onClose={() => dispatch(ascentActions.changeOption(""))}
        isOpen={option == "CREATE"}
        onSave={() => dispatch(ascentActions.changeOption("REFRESH"))}
      />
    </>
  )
}