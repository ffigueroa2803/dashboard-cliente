/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { RefreshCw, Trash2 } from "react-feather";
import { useSelector } from "react-redux";
import { Button, ButtonGroup, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteAscent, editAscent } from "../apis";
import { IAscentFormDto } from "../dtos/ascent-form.dto";
import { AscentForm } from "./ascent-form";
import { toast } from "react-toastify";
import { IAscentEntity } from "../dtos/ascent.entity";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (ascent: IAscentEntity) => void
}

export const AscentEdit = ({ isOpen, onClose, onSave }: IProps) => {

  const { ascent } = useSelector((state: RootState) => state.ascent);

  const [form, setForm] = useState<IAscentFormDto>(ascent);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm(prev => ({
      ...prev,
      [name]: value
    }))
  }

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await editAscent(ascent?.id || 0, form)
      .then(data => {
        toast.success(`Los cambios se actualizarón correctamente`)
        if (typeof onSave == 'function') onSave(data);
    }).catch(() => toast.error(`No se pudo actualizar los datos`))
    setLoading(false);
  }

  const handleDelete = async () => {
    setLoading(true);
    toast.dismiss();
    await deleteAscent(ascent?.id || 0)
      .then(data => {
        toast.success(`El regístro se eliminó correctamente`)
        if (typeof onSave == 'function') onSave(data);
    }).catch(() => toast.error(`No se pudo eliminar el regístro`))
    setLoading(false);
  }

  useEffect(() => {
    if (ascent?.id) setForm(ascent);
  }, [ascent]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Acenso
      </ModalHeader>
      <ModalBody>
        <AscentForm
          isEdit={true}
          form={form}
          onChange={handleForm}
          isDisabled={loading}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <ButtonGroup>
          <Button color="danger"
            outline
            disabled={loading}
            onClick={handleDelete}
          >
            <Trash2 className="icon"/>
          </Button>
          <Button color="primary"
            disabled={loading}
            onClick={handleSave}
          >
            <RefreshCw className="icon"/>
          </Button>
        </ButtonGroup>
      </ModalFooter>
    </Modal>
  )
}