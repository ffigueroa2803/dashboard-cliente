/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import { ContractChange } from "@modules/scale/contracts/components/contract-change";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { useState } from "react";
import { ArrowUpRight, Bookmark, Settings } from "react-feather";
import { Button, Form, FormGroup, Input } from "reactstrap";
import { IAscentFormDto } from "../dtos/ascent-form.dto";

interface IProps {
  form: IAscentFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
  isEdit?: boolean;
}

const dataDefault: IContractEntity = {
  id: 0,
  workId: 0,
  dependencyId: 0,
  profileId: 0,
  typeCategoryId: 0,
  condition: "CONTRATADO",
  ley: "",
  plaza: "",
  hourhandId: 0,
  code: "",
  resolution: "",
  dateOfResolution: "",
  dateOfAdmission: "",
  terminationDate: "",
  observation: "",
  hours: 8,
  fileId: 0,
  state: false,
};

export const AscentForm = ({ form, onChange, isDisabled, isEdit }: IProps) => {
  const [contractSource, setContractSource] =
    useState<IContractEntity>(dataDefault);
  const [contractTarget, setContractTarget] =
    useState<IContractEntity>(dataDefault);
  const [isContract, setIsContract] = useState<string>("");

  const handleAdd = (contract: IContractEntity) => {
    if (isContract == "SOURCE") {
      setContractSource(contract);
      onChange({ name: "contractSourceId", value: contract.id });
    }
    // contract target
    if (isContract == "TARGET") {
      setContractTarget(contract);
      onChange({ name: "contractTargetId", value: contract.id });
    }
    // clear contract select
    setIsContract("");
  };

  const handleRemoveSource = () => {
    setContractSource(dataDefault);
    onChange({ name: "contractSourceId", value: 0 });
  };

  const handleRemoveTarget = () => {
    setContractTarget(dataDefault);
    onChange({ name: "contractTargetId", value: 0 });
  };

  return (
    <Form onSubmit={(e) => e.preventDefault()}>
      <Show condition={!isEdit}>
        <FormGroup>
          <Bookmark className="icon" /> Contrato de Orígen
          <hr />
        </FormGroup>

        <FormGroup>
          <Button outline color="dark" onClick={() => setIsContract("SOURCE")}>
            <Settings className="icon" />
          </Button>

          {/* info source */}
          <Show condition={contractSource.id != 0}>
            <div className="mt-2">
              <ContractDetail
                contract={contractSource}
                onClose={handleRemoveSource}
              />
            </div>
          </Show>
        </FormGroup>
      </Show>

      <Show condition={!isEdit}>
        <FormGroup>
          <hr />
          <ArrowUpRight className="icon" /> Contrato de Acenso
          <hr />
        </FormGroup>

        <FormGroup>
          <Button outline color="dark" onClick={() => setIsContract("TARGET")}>
            <Settings className="icon" />
          </Button>

          {/* info target */}
          <Show condition={contractTarget.id != 0}>
            <div className="mt-2">
              <ContractDetail
                contract={contractTarget}
                onClose={handleRemoveTarget}
              />
            </div>
          </Show>
        </FormGroup>
      </Show>

      <FormGroup>
        <label>Observación</label>
        <Input
          name="observation"
          type="textarea"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      {/* Selecionar contrato */}
      <ContractChange
        isOpen={isContract == "TARGET" || isContract == "SOURCE"}
        onClose={() => setIsContract("")}
        onAdd={handleAdd}
        arrayDisabled={[contractSource.id, contractTarget.id]}
      />
    </Form>
  );
};
