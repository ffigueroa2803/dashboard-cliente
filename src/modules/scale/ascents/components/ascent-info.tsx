/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { Edit, Home, Info, Trash } from "react-feather";
import { Col, Row } from "reactstrap";
import { IAscentEntity } from "../dtos/ascent.entity";

interface IProps {
  ascent: IAscentEntity;
  onEdit: (ascent: IAscentEntity) => void;
  onDelete?: (ascent: IAscentEntity) => void;
}

export const AscentInfo = ({ ascent, onEdit, onDelete }: IProps) => {
  const handleAscent = () => {
    if (typeof onEdit == "function") {
      onEdit(ascent);
    }
  };

  const handleDelete = () => {
    if (typeof onDelete == "function") {
      onDelete(ascent);
    }
  };

  return (
    <div className="media mb-1">
      <Show condition={typeof onDelete == "function"}>
        <Trash
          className="icon close"
          style={{ zIndex: 99, right: "40px" }}
          onClick={handleDelete}
        />
      </Show>

      <Edit
        className="icon close"
        style={{ zIndex: 99 }}
        onClick={handleAscent}
      />
      <div className="media-body ml-3">
        <Row className="w-100">
          {/* datos origen */}
          <Col md="12" className="mb-1">
            <Info className="icon" /> Origen
            <hr />
          </Col>
          <Col md="12" className="">
            <ContractDetail contract={ascent.contractSource || ({} as any)} />
          </Col>
          {/* datos destino */}
          <Col md="12">
            <hr />
            <Home className="icon" /> Destino
            <hr />
          </Col>
          <Col md="12">
            <ContractDetail contract={ascent.contractTarget || ({} as any)} />
          </Col>

          <Col md="12">
            <label htmlFor="">Observacion</label>
            <h6>{ascent?.observation || "N/A"}</h6>
          </Col>
        </Row>
      </div>
    </div>
  );
};
