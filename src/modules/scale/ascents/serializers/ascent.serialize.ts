import { DateTime } from "luxon";

export class AscentSerialize {
  private date!: string;

  get displayDate() {
    return DateTime.fromSQL(`${this.date}`).toFormat(`dd/MM/yyyy`);
  }
}
