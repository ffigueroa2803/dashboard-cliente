/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import { ContractChange } from "@modules/scale/contracts/components/contract-change";
import { ContractDetail } from "@modules/scale/contracts/components/contract-detail";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { contractActions } from "@modules/scale/contracts/store";
import { RootState } from "@store/store";
import { useState } from "react";
import { Calendar, Save, Settings } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { createVacation } from "../apis";
import { IVacationFormDto } from "../dtos/vacation-form.dto";
import { IVacationEntity } from "../dtos/vacation.entity.dto";
import { VacationForm } from "./vacation-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (configVacation: IVacationEntity) => void;
}

export const VacationCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const dispatch = useDispatch();

  const { configVacation } = useSelector(
    (state: RootState) => state.configVacation
  );
  const { contract } = useSelector((state: RootState) => state.contract);

  const defaultData: IVacationFormDto = {
    configVacationId: configVacation?.id || 0,
    contractId: contract?.id || 0,
    documentDate: "",
    documentNumber: "",
    startDate: "",
    terminationDate: "",
    observation: "",
    daysUsed: 0,
  };

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IVacationFormDto>(defaultData);
  const [isContract, setIsContract] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    toast.dismiss();
    setPending(true);
    await createVacation(form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente!`);
        setForm(defaultData);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setPending(false);
  };

  const handleAdd = (contract: IContractEntity) => {
    dispatch(contractActions.find(contract));
    handleForm({ name: "contractId", value: contract.id });
    setIsContract(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Vacación</ModalHeader>
      <ModalBody>
        {/* config contract */}
        <div className="mb-3">
          <Button color="dark" onClick={() => setIsContract(true)} outline>
            <Settings className="icon" />
          </Button>
          {/* info contract */}
          <Show condition={form?.contractId == contract?.id}>
            <div className="mt-3">
              <ContractDetail
                contract={contract || ({} as any)}
                onClick={handleAdd}
              />
            </div>
          </Show>
          <hr />
          <Calendar className="icon" /> Vacación
          <hr />
        </div>
        {/* vacación */}
        <VacationForm form={form} onChange={handleForm} isDisabled={pending} />
        {/* save */}
        <div className="text-right">
          <Button color="primary" disabled={pending} onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </div>
      </ModalBody>
      {/* contracto */}
      <ContractChange
        isOpen={isContract}
        onClose={() => setIsContract(false)}
        onAdd={handleAdd}
      />
    </Modal>
  );
};
