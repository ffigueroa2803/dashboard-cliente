/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Row } from "reactstrap";
import { scaleRequestV2 } from "@services/scale.request";
import urljoin from "url-join";
import { DateTime } from "luxon";
import { useFormik } from "formik";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { toast } from "react-toastify";
import { InputErrorComponent } from "@common/inputs/input-error.component";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";
import {
  VacationExecuteReportDto,
  VacationExecuteReportYup,
} from "../dtos/vacation-execute-report.dto";

const currentDate = DateTime.now();
const request = scaleRequestV2();

const dataDefault: VacationExecuteReportDto = {
  dateCurrent: currentDate.toFormat("yyyy-MM-dd"),
};

export const VacationExecuteReport = () => {
  const formik = useFormik({
    initialValues: dataDefault,
    validationSchema: VacationExecuteReportYup,
    onSubmit: () => {},
  });

  const handleClick = (extname: string) => {
    if (!formik.isValid) return toast.warning("Filtros incorrectos");
    const filters = formik.values;
    const query = new URLSearchParams();
    query.set("dateCurrent", `${filters.dateCurrent}`);

    const contractFilter = Object.assign(filters.contract || {});
    // filter a
    Object.keys(contractFilter).forEach((key) => {
      query.set(`contract[${key}]`, `${contractFilter[key]}`);
    });
    // generate url
    const pathBase = urljoin(
      request.urlBase,
      `/vacations/reports/vigente.${extname}`
    );
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  const handleSelect = (option: any) => {
    option.value = option.value || undefined;
    formik.handleChange({ target: option });
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Cargo</label>
          <TypeCargoSelect
            name="contract.typeCargoId"
            value={formik.values?.contract?.typeCargoId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="typeCargoId" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Condición</label>
          <ContractConditionSelect
            name="contract.condition"
            value={formik.values?.contract?.condition || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="condition" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Categoria</label>
          <TypeCategorySelect
            name="contract.typeCategoryId"
            value={formik.values?.contract?.typeCategoryId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Dependencia</label>
          <DependencySelect
            name="contract.dependencyId"
            value={formik.values?.contract?.dependencyId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        {/* <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button> */}
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}
        >
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
