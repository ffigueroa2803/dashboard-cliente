/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card } from "reactstrap";
import { VacationEntity } from "../dtos/vacation";
import { IVacationEntity } from "../dtos/vacation.entity.dto";
import { useVacationFolderCreate } from "../hooks/use-vacation-folder-create";
import { vacationActions } from "../store";

export interface IVacationFolderCreateProps {
  vacation: IVacationEntity;
}

export const VacationFolderCreate = ({
  vacation,
}: IVacationFolderCreateProps) => {
  const dispatch = useDispatch();
  const vacationFolderCreate = useVacationFolderCreate(vacation);

  const handle = () => {
    vacationFolderCreate
      .execute()
      .then((data) => {
        const newVacation = new VacationEntity(vacation);
        newVacation.setFolder(data);
        dispatch(vacationActions.updateItem(newVacation));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => vacationFolderCreate.abort();
  }, []);

  return (
    <Card className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={vacationFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </Card>
  );
};
