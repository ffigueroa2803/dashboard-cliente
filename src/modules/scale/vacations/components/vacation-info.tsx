/* eslint-disable no-unused-vars */
import { Col, FormGroup, Row } from "reactstrap";
import { IVacationEntity } from "../dtos/vacation.entity.dto";
import { Edit, Trash } from "react-feather";
import { deleteVacation } from "../apis";
import { toast } from "react-toastify";
import { Show } from "@common/show";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { VacationUserError } from "./vacation-user-error";
import { VacationFolderCreate } from "./vacation-folder-create";
import { useVacationDelete } from "../hooks/use-vacation-delete";
import { useConfirm } from "@common/confirm/use-confirm";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";

interface IProps {
  vacation: IVacationEntity;
  onEdit?: (vacation: IVacationEntity) => void;
  onDelete?: (vacation: IVacationEntity) => void;
}

export const VacationInfo = ({ vacation, onEdit, onDelete }: IProps) => {
  const { user } = useSelector((state: RootState) => state.person);
  const confirm = useConfirm();

  const vacationDelete = useVacationDelete(vacation);

  const handleConfig = () => {
    if (typeof onEdit == "function") {
      onEdit(vacation);
    }
  };

  const handleDelete = async () => {
    confirm
      .alert({
        title: "Eliminar",
        message: "¿Estás seguro en elimnar?",
        labelError: "Cancelar",
        labelSuccess: "Eliminar",
      })
      .then(() => {
        vacationDelete
          .execute()
          .then(() => toast.success(`La vacación se eliminó`))
          .catch(() => toast.error(`No se pudó eliminar la vacación`));
      })
      .catch(() => null);
  };

  return (
    <div className="media mb-1">
      {/* eliminar */}
      <Trash
        className="icon close"
        onClick={handleDelete}
        style={{ zIndex: 99, right: "40px" }}
      />
      {/* editar */}
      <Edit
        className="icon close"
        onClick={handleConfig}
        style={{ zIndex: 99 }}
      />
      <div className="media-body ml-3 mt-2">
        <Row>
          <Col md="12">
            <FormGroup>
              <label>N° Documento</label>
              <h6>{vacation.documentNumber}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Fecha del Documento</label>
              <h6>{vacation.documentDate}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Fecha de Inicio</label>
              <h6>{vacation.startDate}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Fecha de Termino</label>
              <h6>{vacation.terminationDate}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Dias Usados</label>
              <h6>{vacation.daysUsed}</h6>
            </FormGroup>
          </Col>

          <Col md="12">
            <FormGroup>
              <label>Observación</label>
              <h6>{vacation.observation || "N/A"}</h6>
            </FormGroup>
          </Col>

          <Col md="12">
            <Show
              condition={user.id != undefined}
              isDefault={<VacationUserError />}
            >
              <Show
                condition={!vacation.folderId}
                isDefault={<FolderInfoComponent id={vacation.folderId || ""} />}
              >
                <VacationFolderCreate vacation={vacation} />
              </Show>
            </Show>
          </Col>
        </Row>
      </div>
    </div>
  );
};
