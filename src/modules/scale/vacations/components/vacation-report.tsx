/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { DateTime } from "luxon";
import {
  VacationReportDto,
  VacationReportYup,
} from "../dtos/vacation-report.dto";
import { useFormik } from "formik";
import { InputComponent } from "@common/inputs/input.component";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { toast } from "react-toastify";
import { InputErrorComponent } from "@common/inputs/input-error.component";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";

const currentDate = DateTime.now();
const request = scaleRequest();

const dataDefault: VacationReportDto = {
  year: currentDate.year,
};

export const VacationReport = () => {
  const formik = useFormik({
    initialValues: dataDefault,
    validationSchema: VacationReportYup,
    onSubmit: () => {},
  });

  const handleClick = (extname: string) => {
    if (!formik.isValid) return toast.warning("Filtros incorrectos");
    const filters = formik.values;
    const query = new URLSearchParams();
    query.set("year", `${filters.year}`);
    // filter mes
    if (filters?.month) query.set("month", `${filters.month}`);
    // filter cargos
    if (filters?.typeCargoId) {
      query.set("typeCargoIds[0]", `${filters.typeCargoId}`);
    }
    // filters conditions
    if (filters?.condition) {
      query.set("conditions[0]", `${filters.condition}`);
    }
    // filters type categories
    if (filters?.typeCategoryId) {
      query.set("typeCategoryIds[0]", `${filters.typeCargoId}`);
    }
    // filters dependency
    if (filters?.dependencyId) {
      query.set("dependencyIds[0]", `${filters.dependencyId}`);
    }
    // generate url
    const pathBase = urljoin(
      request.urlBase,
      `/vacations/reports/list.${extname}`
    );
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  const handleSelect = (option: any) => {
    option.value = option.value || undefined;
    formik.handleChange({ target: option });
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Año <b className="text-danger">*</b>
          </label>
          <InputComponent name="year" type="text" formik={formik} />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Mes</label>
          <InputComponent name="mes" type="text" formik={formik} />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Cargo</label>
          <TypeCargoSelect
            name="typeCargoId"
            value={formik.values?.typeCargoId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="typeCargoId" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Condición</label>
          <ContractConditionSelect
            name="condition"
            value={formik.values?.condition || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="condition" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Categoria</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={formik.values?.typeCategoryId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Dependencia</label>
          <DependencySelect
            name="dependencyId"
            value={formik.values?.dependencyId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}>
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
