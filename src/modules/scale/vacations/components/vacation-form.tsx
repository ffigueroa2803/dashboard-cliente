/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Form, FormGroup, Input } from "reactstrap";
import { DateTime } from "luxon";
import { useEffect } from "react";
import { IVacationFormDto } from "../dtos/vacation-form.dto";

interface IProps {
  form: IVacationFormDto;
  isEdit?: boolean;
  isDisabled?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const VacationForm = ({ form, onChange, isDisabled }: IProps) => {
  const calcDaysUsed = () => {
    const dateOfAdmission = DateTime.fromSQL(`${form?.startDate}`);
    const terminationDate = DateTime.fromSQL(`${form?.terminationDate}`);
    const diff = terminationDate.diff(dateOfAdmission, "days").days + 1;
    onChange({ name: "daysUsed", value: diff || 0 });
  };

  useEffect(() => {
    calcDaysUsed();
  }, [form?.startDate, form?.terminationDate]);

  return (
    <Form>
      <FormGroup>
        <label>
          N° Documento <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="documentNumber"
          value={form?.documentNumber || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Documento <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="documentDate"
          value={form?.documentDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Inicio <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="startDate"
          value={form?.startDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Termino <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="terminationDate"
          value={form?.terminationDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Dias usados</label>
        <Input
          type="number"
          name="scheduledDays"
          value={form?.daysUsed || 0}
          readOnly
        />
      </FormGroup>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>
    </Form>
  );
};
