/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { RefreshCw } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { editVacation } from "../apis";
import { IVacationFormDto } from "../dtos/vacation-form.dto";
import { IVacationEntity } from "../dtos/vacation.entity.dto";
import { VacationForm } from "./vacation-form";

interface IProps {
  isOpen: boolean,
  onClose: () => void,
  onSave?: (vacation: IVacationEntity) => void
}

export const VacationEdit = ({ isOpen, onClose, onSave }: IProps) => {

  const { vacation } = useSelector((state: RootState) => state.vacation);

  const defaultData: IVacationFormDto = {
    configVacationId: vacation?.configVacationId || 0,
    contractId: vacation.contractId,
    documentDate: vacation?.documentDate,
    documentNumber: vacation?.documentNumber,
    startDate: vacation?.startDate,
    terminationDate: vacation?.terminationDate,
    observation: vacation?.observation,
    daysUsed: vacation?.daysUsed,
  }

  const [pending, setPending] = useState<boolean>(false)
  const [form, setForm] = useState<IVacationFormDto>(defaultData);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm(prev => ({
      ...prev,
      [name]: value
    }))
  }

  const handleSave = async () => {
    toast.dismiss();
    setPending(true)
    await editVacation(vacation.id, form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente!`)
        setForm({
          ...defaultData,
          ...data
        })
        if (typeof onSave == 'function') onSave(data)
      })
      .catch(() => toast.error(`No se pudo guardar los datos`))
    setPending(false)
  }

  useEffect(() => {
    if (vacation?.id) setForm(defaultData);
  }, [vacation]);

  useEffect(() => {
    if (!isOpen) setForm(defaultData);
  }, [isOpen])

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Vacación</ModalHeader>
      <ModalBody>
        <VacationForm
          form={form}
          onChange={handleForm}
          isDisabled={pending}
        />
        {/* save */}
        <div className='text-right'>
          <Button color='primary'
            disabled={pending}
            onClick={handleSave}
          > 
            <RefreshCw className="icon"/>
          </Button>
        </div>
      </ModalBody>
    </Modal>
  )
}