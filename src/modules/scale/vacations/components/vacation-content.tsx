/* eslint-disable react-hooks/exhaustive-deps */
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import { getVacationsByConfigVacation } from "../apis";
import { vacationActions } from "../store";
import { VacationInfo } from "./vacation-info";
import { VacationCreate } from "./vacation-create";
import { IVacationEntity } from "../dtos/vacation.entity.dto";
import { VacationEdit } from "./vacation-edit";
import { Plus } from "react-feather";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onCreated: () => void;
  onUpdated: () => void;
  onDeleted: () => void;
}

export const VacationContent = ({
  isOpen,
  onClose,
  onCreated,
  onUpdated,
  onDeleted,
}: IProps) => {
  const [pending, setPending] = useState<boolean>(true);
  const [, setIsError] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(true);
  const [isEdit, setIsEdit] = useState<boolean>(false);

  const dispatch = useDispatch();

  const { configVacation } = useSelector(
    (state: RootState) => state.configVacation
  );
  const { vacations, option } = useSelector(
    (state: RootState) => state.vacation
  );

  const handleVacations = async () => {
    setPending(true);
    setIsError(false);
    await getVacationsByConfigVacation(configVacation?.id || 0, {
      page: 1,
      limit: 30,
    })
      .then((data) => dispatch(vacationActions.paginate(data)))
      .catch(() => setIsError(true));
    setPending(false);
  };

  const handleSave = () => {
    setIsRefresh(true);
    dispatch(vacationActions.changeOption(""));
    onCreated();
  };

  const handleEdit = (vacation: IVacationEntity) => {
    dispatch(vacationActions.setVacation(vacation));
    setIsEdit(true);
  };

  const handleUpdate = () => {
    setIsRefresh(true);
    onUpdated();
  };

  const handleDetele = () => {
    dispatch(vacationActions.setVacation({} as any));
    setIsRefresh(true);
    onDeleted();
  };

  useEffect(() => {
    if (isRefresh) handleVacations();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <Modal isOpen={isOpen} size="lg" style={{ minWidth: "90%" }}>
      <ModalHeader toggle={onClose}>
        <h5>Listado de vacaciones</h5>
      </ModalHeader>
      <ModalBody>
        <div className="prooduct-details-box mb-3">
          <Row>
            {/* data empty */}
            <Show condition={!pending && !vacations?.items?.length}>
              <Col md="12">
                <RegisterEmptyError />
              </Col>
            </Show>
            {/* listar vacations */}
            {vacations?.items?.map((i) => (
              <Col md="12" lg="6" key={`list-vacation-${i}`}>
                <VacationInfo
                  vacation={i}
                  onEdit={handleEdit}
                  onDelete={handleDetele}
                />
              </Col>
            ))}
            {/* crear vacation */}
            <VacationCreate
              isOpen={option == "CREATE"}
              onClose={() => dispatch(vacationActions.changeOption(""))}
              onSave={handleSave}
            />
            {/* edit vacation */}
            <VacationEdit
              isOpen={isEdit}
              onClose={() => setIsEdit(false)}
              onSave={handleUpdate}
            />
          </Row>
        </div>
      </ModalBody>
      <ModalFooter className="text-right">
        <Button
          color="success"
          outline
          onClick={() => dispatch(vacationActions.changeOption("CREATE"))}
        >
          <Plus className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
