import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IVacationEntity } from "../dtos/vacation.entity.dto";

const request = scaleRequest();

export const useVacationDelete = (vacation: IVacationEntity) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFolderEntityInterface>((resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      request
        .destroy(`vacations/${vacation.id}`, { signal: abortController.signal })
        .then(({ data }) => resolve(data))
        .catch((err) => {
          reject(err);
        });
    });
  };

  return { pending, execute, abort };
};
