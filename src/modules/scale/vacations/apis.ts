import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { PaginateDto } from "@services/dtos";
import { scaleRequest } from "@services/scale.request";
import { IVacationCreateDto } from "./dtos/vacation-create.dto";
import { IVacationEditDto } from "./dtos/vacation-edit.dto";
import { IVacationEntity } from "./dtos/vacation.entity.dto";

const request = scaleRequest();

export const getVacationsByConfigVacation = async (
  id: number,
  { page, querySearch, limit }: PaginateDto
): Promise<ResponsePaginateDto<IVacationEntity>> => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  return await request
    .get(`configVacations/${id}/vacations`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createVacation = async (
  payload: IVacationCreateDto
): Promise<IVacationEntity> => {
  return await request.post(`vacations`, payload).then((res) => res.data);
};

export const editVacation = async (
  id: number,
  payload: IVacationEditDto
): Promise<IVacationEntity> => {
  return await request.put(`vacations/${id}`, payload).then((res) => res.data);
};

export const deleteVacation = async (
  id: number
): Promise<{ deleted: boolean }> => {
  return await request.destroy(`vacations/${id}`).then((res) => res.data);
};
