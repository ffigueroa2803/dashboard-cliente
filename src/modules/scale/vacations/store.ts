import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IVacationEntity } from "./dtos/vacation.entity.dto";

export interface VacationState {
  vacations: ResponsePaginateDto<IVacationEntity>;
  vacation: IVacationEntity;
  folder: IFolderEntityInterface;
  option: string;
}

const initialState: VacationState = {
  vacations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  vacation: {} as any,
  folder: {} as any,
  option: "",
};

const vacationStore = createSlice({
  name: "escalafon@vacations",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IVacationEntity>>
    ) => {
      state.vacations = payload as any;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    setVacation: (state, { payload }: PayloadAction<IVacationEntity>) => {
      state.vacation = payload;
    },
    updateItem: (
      state: VacationState,
      { payload }: PayloadAction<IVacationEntity>
    ) => {
      state.vacations.items = state.vacations.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.vacation };
    },
  },
});

export const vacationReducer = vacationStore.reducer;

export const vacationActions = vacationStore.actions;
