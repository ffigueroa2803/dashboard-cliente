/* eslint-disable no-unused-vars */
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

export interface IVacationEntity {
  id: number;
  configVacationId: number;
  contractId: number;
  documentDate: string;
  documentNumber: string;
  startDate: string;
  terminationDate: string;
  observation: string;
  daysUsed: number;
  folderId?: string;
  folder?: IFolderEntityInterface;
  setFolder(folder: IFolderEntityInterface): void;
}

export const vacationEntityName = "VacationEntity";
