import { Yup } from "@common/yup";

export interface VacationReportDto {
  year: number;
  month?: number;
  typeCargoId?: number;
  typeCategoryId?: number;
  condition?: string;
  dependencyId?: number;
}

export const VacationReportYup = Yup.object({
  year: Yup.number().required(),
  month: Yup.number().optional(),
  typeCargoId: Yup.number().optional(),
  typeCategoryId: Yup.number().optional(),
  condition: Yup.string().optional(),
  dependencyId: Yup.number().optional(),
});
