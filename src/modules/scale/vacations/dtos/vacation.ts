import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { IVacationEntity } from "./vacation.entity.dto";

export class VacationEntity implements IVacationEntity {
  id!: number;
  configVacationId!: number;
  contractId!: number;
  documentDate!: string;
  documentNumber!: string;
  startDate!: string;
  terminationDate!: string;
  observation!: string;
  daysUsed!: number;
  folderId?: string | undefined;
  folder?: IFolderEntityInterface | undefined;

  constructor(partial?: Partial<any>) {
    if (partial) Object.assign(this, partial);
  }

  setFolder(folder: IFolderEntityInterface): void {
    this.folder = folder;
    this.folderId = folder.id;
  }
}
