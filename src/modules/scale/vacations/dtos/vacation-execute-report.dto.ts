import { Yup } from "@common/yup";

export interface VacationExecuteReportDto {
  dateCurrent: string;
  contract?: {
    typeCargoId?: number;
    typeCategoryId?: number;
    condition?: string;
    dependencyId?: number;
  }
}

export const VacationExecuteReportYup = Yup.object({
  dateCurrent: Yup.string().required(),
  contract: Yup.object().shape({
    typeCargoId: Yup.number().optional(),
    typeCategoryId: Yup.number().optional(),
    condition: Yup.string().optional(),
    dependencyId: Yup.number().optional(),
  }).optional()
});
