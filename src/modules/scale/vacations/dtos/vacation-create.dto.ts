export interface IVacationCreateDto {
  configVacationId: number;
  contractId: number;
  documentDate: string;
  documentNumber: string;
  startDate: string;
  terminationDate: string;
  observation: string;
}
