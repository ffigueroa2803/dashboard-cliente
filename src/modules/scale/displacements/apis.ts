import { scaleRequest } from "@services/scale.request";
import { ICreateDisplacementDto } from "./dtos/create-displacement.dto";
import { IEditDisplacementDto } from "./dtos/edit-displacement.dto";
import { FilterGetDisplacementsDto } from "./dtos/filter-displacement.dto";
import { IDisplacementEntity } from "./dtos/displacement.entity";

const request = scaleRequest();

export const getDisplacementsToContract = async (
  id: number,
  { page, querySearch, limit, year }: FilterGetDisplacementsDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  // response
  return await request
    .get(`contracts/${id}/displacements`, { params })
    .then((res) => res.data);
};

export const createDisplacement = async (
  payload: ICreateDisplacementDto
): Promise<IDisplacementEntity> => {
  return await request.post(`displacements`, payload).then((res) => res.data);
};

export const editDisplacement = async (
  id: number,
  payload: IEditDisplacementDto
): Promise<IDisplacementEntity> => {
  return await request.put(`displacements/${id}`, payload).then((res) => res.data);
};

export const deleteDisplacement = async (
  id: number
): Promise<any> => {
  return await request.destroy(`displacements/${id}`).then((res) => res.data);
};
