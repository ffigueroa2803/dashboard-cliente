import { DateTime } from "luxon";

export class DisplacementSerialize {
  private date!: string;
  private resolutionDate!: string;

  get displayDate() {
    return DateTime.fromSQL(`${this.date}`).toFormat(`dd/MM/yyyy`);
  }

  get displayResolutionDate() {
    return DateTime.fromSQL(`${this.resolutionDate}`).toFormat(`dd/MM/yyyy`);
  }
}
