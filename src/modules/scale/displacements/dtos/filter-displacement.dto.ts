import { PaginateDto } from "@services/dtos";

export interface FilterGetDisplacementsDto extends PaginateDto {
  year?: number;
}
