export interface ICreateDisplacementDto {
  contractId: number;
  sourceDependencyId: number;
  sourceProfileId: number;
  targetDependencyId: number;
  targetProfileId: number;
  date: string;
  resolutionDate: string;
  resolutionNumber: string;
  isApplied: boolean;
}
