export interface IEditDisplacementDto {
  sourceDependencyId: number;
  sourceProfileId: number;
  targetDependencyId: number;
  targetProfileId: number;
  date: string;
  resolutionDate: string;
  resolutionNumber: string;
}
