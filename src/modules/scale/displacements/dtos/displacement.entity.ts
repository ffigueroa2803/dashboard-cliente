import { IDependencyEntity } from "@modules/auth/dependencies/dtos/dependency.entity";
import { IProfileEntity } from "@modules/scale/profiles/dtos/profile.entity";

export interface IDisplacementEntity {
  id: number;
  contractId: number;
  sourceDependencyId: number;
  sourceProfileId: number;
  targetDependencyId: number;
  targetProfileId: number;
  date: string;
  resolutionDate: string;
  resolutionNumber: string;
  sourceDependency?: IDependencyEntity;
  sourceProfile?: IProfileEntity;
  targetDependency?: IDependencyEntity;
  targetProfile?: IProfileEntity;
}
