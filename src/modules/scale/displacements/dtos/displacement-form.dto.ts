export interface IDisplacementFormDto {
  contractId: number;
  sourceDependencyId: number;
  sourceProfileId: number;
  targetDependencyId: number;
  targetProfileId: number;
  date: string;
  resolutionDate: string;
  resolutionNumber: string;
}
