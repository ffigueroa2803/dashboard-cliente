/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Form, FormGroup, Input } from "reactstrap";
import { IDisplacementFormDto } from "../dtos/displacement-form.dto";
import { IInputHandle } from "@common/dtos/input-handle";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";
import { ProfileSelect } from "@modules/scale/profiles/components/profile-select";
import { Home, Info } from "react-feather";

interface IProps {
  form: IDisplacementFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
}

export const DisplacementForm = ({ form, onChange, isDisabled }: IProps) => {
  return (
    <Form>
      <FormGroup>
        <label>
          Fecha Documento <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="resolutionDate"
          value={form?.resolutionDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          N° Documento <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="resolutionNumber"
          value={form?.resolutionNumber || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Rotación <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="date"
          value={form?.date || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <hr />
        <Info className="icon" /> Origen
        <hr />
      </FormGroup>

      <FormGroup>
        <label>
          Dependencia <b className="text-danger">*</b>
        </label>
        <DependencySelect
          name="sourceDependencyId"
          value={form?.sourceDependencyId || ""}
          onChange={(data) => onChange(data)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Perfil Laboral <b className="text-danger">*</b>
        </label>
        <ProfileSelect
          name="sourceProfileId"
          value={form?.sourceProfileId || ""}
          onChange={(data) => onChange(data)}
        />
      </FormGroup>

      <FormGroup>
        <hr />
        <Home className="icon" /> Destino
        <hr />
      </FormGroup>

      <FormGroup>
        <label>
          Dependencia <b className="text-danger">*</b>
        </label>
        <DependencySelect
          name="targetDependencyId"
          value={form?.targetDependencyId || ""}
          onChange={(data) => onChange(data)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Perfil Laboral <b className="text-danger">*</b>
        </label>
        <ProfileSelect
          name="targetProfileId"
          value={form?.targetProfileId || ""}
          onChange={(data) => onChange(data)}
        />
      </FormGroup>
    </Form>
  );
};
