/* eslint-disable react-hooks/exhaustive-deps */
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Row } from "reactstrap";
import { getDisplacementsToContract } from "../apis";
import { IDisplacementEntity } from "../dtos/displacement.entity";
import { displacementActions } from "../store";
import { DisplacementEdit } from "./displacement-edit";
import { DisplacementInfo } from "./displacement-info";

export const DisplacementList = () => {
  const dispatch = useDispatch();
  const { displacements, option } = useSelector(
    (state: RootState) => state.displacement
  );
  const { contract } = useSelector((state: RootState) => state.contract);

  const [loading, setLoading] = useState<boolean>(true);
  const [isRefresh, setIsRefresh] = useState<boolean>(true);
  const [perPage] = useState<number>(30);
  const [page] = useState<number>(1);
  const [year] = useState<number | undefined>(undefined);
  const [querySearch] = useState<string>("");
  const [, setIsError] = useState<boolean>(false);

  const handleData = async () => {
    setLoading(true);
    setIsError(false);
    await getDisplacementsToContract(contract?.id || 0, {
      page,
      limit: perPage,
      year,
      querySearch,
    })
      .then((data) => dispatch(displacementActions.paginate(data)))
      .catch(() => setIsError(true));
    setLoading(false);
  };

  const handleEdit = async (displacement: IDisplacementEntity) => {
    dispatch(displacementActions.setLicense(displacement));
    dispatch(displacementActions.changeOption("EDIT"));
  };

  useEffect(() => {
    if (isRefresh) handleData();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(displacementActions.changeOption(""));
    }
  }, [option]);

  return (
    <>
      <div className="prooduct-details-box mb-3">
        <Row>
          {/* buscador */}
          <Col md="10" className="mb-2"></Col>
          <Col md="2" className="mb-2">
            <Button
              outline
              block
              color="primary"
              disabled={loading}
              onClick={() => setIsRefresh(true)}
            >
              Actualizar
            </Button>
          </Col>
          {/* limite */}
          <Col md="12">
            <hr />
          </Col>
          {/* not licenses */}
          <Show condition={!loading && !displacements.items?.length}>
            <Col md="12">
              <RegisterEmptyError onRefresh={() => setIsRefresh(true)} />
            </Col>
          </Show>
          {/* loading */}
          <Show condition={loading}>
            <Col md="12" className="text-center">
              Obtener datos...
            </Col>
          </Show>
          {/* listar licenses */}
          {displacements?.items?.map((i) => (
            <Col key={`list-license-${i.id}`} md="6">
              <DisplacementInfo displacement={i} onEdit={handleEdit} />
            </Col>
          ))}
        </Row>
      </div>
      {/* editar */}
      <DisplacementEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(displacementActions.changeOption(""))}
        onSave={() => dispatch(displacementActions.changeOption("REFRESH"))}
      />
    </>
  );
};
