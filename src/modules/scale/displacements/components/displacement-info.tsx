/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { VacationUserError } from "@modules/scale/vacations/components/vacation-user-error";
import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { Edit, Home, Info } from "react-feather";
import { useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";
import { DisplacementEntity } from "src/domain/scale/displacements/displacement.entity";
import { IDisplacementEntity } from "../dtos/displacement.entity";
import { DisplacementSerialize } from "../serializers/displacement.serialize";
import { DisplacementFolderCreate } from "./displacement-folder-create";

interface IProps {
  displacement: IDisplacementEntity;
  onEdit: (displacement: IDisplacementEntity) => void;
}

export const DisplacementInfo = ({ displacement, onEdit }: IProps) => {
  const { user } = useSelector((state: RootState) => state.person);
  const displacementEntity = new DisplacementEntity(displacement);
  const displacementSerialize = plainToClass(
    DisplacementSerialize,
    displacement
  );

  const handleDisplacement = () => {
    if (typeof onEdit == "function") {
      onEdit(displacement);
    }
  };

  return (
    <div className="media mb-1">
      <Edit
        className="icon close"
        style={{ zIndex: 99 }}
        onClick={handleDisplacement}
      />
      <div className="media-body ml-3">
        <Row className="w-100">
          <Col md="12" className="mb-1">
            <label>Fecha Documento</label>
            <h6>{displacementSerialize?.displayResolutionDate || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>N° Documento</label>
            <h6>{displacement.resolutionNumber || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Fecha</label>
            <h6>{displacementSerialize?.displayDate || ""}</h6>
          </Col>
          {/* datos origen */}
          <Col md="12" className="mb-1">
            <hr />
            <Info className="icon" /> Origen
            <hr />
          </Col>
          <Col md="12" className="mb-1">
            <label>Dependencia</label>
            <h6 className="capitalize">
              {displacement?.sourceDependency?.name || ""}
            </h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Perfil Laboral</label>
            <h6 className="capitalize">
              {displacement?.sourceProfile?.name || ""}
            </h6>
          </Col>
          {/* datos destino */}
          <Col md="12" className="mb-1">
            <hr />
            <Home className="icon" /> Destino
            <hr />
          </Col>
          <Col md="12" className="mb-1">
            <label>Dependencia</label>
            <h6 className="capitalize">
              {displacement?.targetDependency?.name || ""}
            </h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Perfil Laboral</label>
            <h6 className="capitalize">
              {displacement?.targetProfile?.name || ""}
            </h6>
          </Col>
          <Col md="12">
            <Show
              condition={user.id != undefined}
              isDefault={<VacationUserError />}
            >
              <Show
                condition={!displacementEntity.folderId}
                isDefault={
                  <FolderInfoComponent id={displacementEntity.folderId || ""} />
                }
              >
                <DisplacementFolderCreate displacement={displacementEntity} />
              </Show>
            </Show>
          </Col>
        </Row>
      </div>
    </div>
  );
};
