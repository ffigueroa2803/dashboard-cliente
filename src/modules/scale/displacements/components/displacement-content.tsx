import { RootState } from "@store/store";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { displacementActions } from "../store";
import { DisplacementCreate } from "./displacement-create";
import { DisplacementList } from "./displacement-list";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const DisplacementContent = ({ isOpen, onClose }: IProps) => {
  const dispatch = useDispatch();
  const { option } = useSelector((state: RootState) => state.displacement);

  return (
    <>
      <Modal isOpen={isOpen} size="lg" style={{ minWidth: "90%" }}>
        <ModalHeader toggle={onClose}>Rotaciones</ModalHeader>
        <ModalBody>
          <DisplacementList />
        </ModalBody>
        <ModalFooter className="text-right">
          <Button
            outline
            color="success"
            onClick={() => dispatch(displacementActions.changeOption("CREATE"))}
          >
            <Plus className="icon" />
          </Button>
        </ModalFooter>
      </Modal>
      {/* crear license */}
      <DisplacementCreate
        onClose={() => dispatch(displacementActions.changeOption(""))}
        isOpen={option == "CREATE"}
        onSave={() => dispatch(displacementActions.changeOption("REFRESH"))}
      />
    </>
  );
};
