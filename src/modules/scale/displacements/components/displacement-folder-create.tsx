/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card } from "reactstrap";
import { DisplacementEntity } from "src/domain/scale/displacements/displacement.entity";
import { IDisplacementEntityInterface } from "src/domain/scale/displacements/displacement.entity.interface";
import { useDisplacementFolderCreate } from "../hooks/use-displacement-folder-create";
import { displacementActions } from "../store";

export interface IDisplacementFolderCreateProps {
  displacement: IDisplacementEntityInterface;
}

export const DisplacementFolderCreate = ({
  displacement,
}: IDisplacementFolderCreateProps) => {
  const dispatch = useDispatch();
  const displacementFolderCreate = useDisplacementFolderCreate(displacement);

  const handle = () => {
    displacementFolderCreate
      .execute()
      .then((data) => {
        const newLicense = new DisplacementEntity(displacement);
        newLicense.setFolder(data);
        dispatch(displacementActions.updateItem(newLicense));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => displacementFolderCreate.abort();
  }, []);

  return (
    <Card className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={displacementFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </Card>
  );
};
