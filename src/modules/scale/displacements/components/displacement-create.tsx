/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useContext, useEffect, useState } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { createDisplacement } from "../apis";
import { IDisplacementFormDto } from "../dtos/displacement-form.dto";
import { DisplacementForm } from "./displacement-form";
import { toast } from "react-toastify";
import { IDisplacementEntity } from "../dtos/displacement.entity";
import Toggle from "@atlaskit/toggle";
import { ContractContext } from "@modules/scale/contracts/components/contract-context";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (license: IDisplacementEntity) => void;
}

export const DisplacementCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const { setIsRefresh } = useContext(ContractContext);

  const dataDefault: IDisplacementFormDto = {
    contractId: contract?.id || 0,
    sourceDependencyId: 0,
    sourceProfileId: 0,
    targetDependencyId: 0,
    targetProfileId: 0,
    date: "",
    resolutionDate: "",
    resolutionNumber: "",
  };

  const [form, setForm] = useState<IDisplacementFormDto>(dataDefault);
  const [loading, setLoading] = useState<boolean>(false);
  const [isApplied, setIsApplied] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await createDisplacement({
      ...form,
      isApplied,
    })
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente`);
        setForm(dataDefault);
        if (isApplied) setIsRefresh(true);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setLoading(false);
  };

  useEffect(() => {
    if (contract?.id) setForm(dataDefault);
  }, [contract]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Rotación</ModalHeader>
      <ModalBody>
        <DisplacementForm
          form={form}
          onChange={handleForm}
          isDisabled={loading}
        />
        {/* applied */}
        <FormGroup>
          <label htmlFor="">Aplicar al contrato</label>
          <div>
            <Toggle
              name="is_applied"
              isChecked={isApplied}
              onChange={({ target }) => setIsApplied(target.checked)}
            />
          </div>
        </FormGroup>
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={loading} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
