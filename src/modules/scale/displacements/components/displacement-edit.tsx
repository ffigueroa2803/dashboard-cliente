/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { RefreshCw, Trash2 } from "react-feather";
import { useSelector } from "react-redux";
import { Button, ButtonGroup, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { deleteDisplacement, editDisplacement } from "../apis";
import { IDisplacementFormDto } from "../dtos/displacement-form.dto";
import { DisplacementForm } from "./displacement-form";
import { toast } from "react-toastify";
import { IDisplacementEntity } from "../dtos/displacement.entity";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (displacement: IDisplacementEntity) => void
}

export const DisplacementEdit = ({ isOpen, onClose, onSave }: IProps) => {

  const { displacement } = useSelector((state: RootState) => state.displacement);

  const [form, setForm] = useState<IDisplacementFormDto>(displacement);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm(prev => ({
      ...prev,
      [name]: value
    }))
  }

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await editDisplacement(displacement?.id || 0, form)
      .then(data => {
        toast.success(`Los cambios se actualizarón correctamente`)
        if (typeof onSave == 'function') onSave(data);
    }).catch(() => toast.error(`No se pudo actualizar los datos`))
    setLoading(false);
  }

  const handleDelete = async () => {
    setLoading(true);
    toast.dismiss();
    await deleteDisplacement(displacement?.id || 0)
      .then(data => {
        toast.success(`El regístro se eliminó correctamente`)
        if (typeof onSave == 'function') onSave(data);
    }).catch(() => toast.error(`No se pudo eliminar el regístro`))
    setLoading(false);
  }

  useEffect(() => {
    if (displacement?.id) setForm(displacement);
  }, [displacement]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>
        Editar Rotación
      </ModalHeader>
      <ModalBody>
        <DisplacementForm
          form={form}
          onChange={handleForm}
          isDisabled={loading}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <ButtonGroup>
          <Button color="danger"
            outline
            disabled={loading}
            onClick={handleDelete}
          >
            <Trash2 className="icon"/>
          </Button>
          <Button color="primary"
            disabled={loading}
            onClick={handleSave}
          >
            <RefreshCw className="icon"/>
          </Button>
        </ButtonGroup>
      </ModalFooter>
    </Modal>
  )
}