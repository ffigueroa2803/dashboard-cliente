import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IDisplacementEntityInterface } from "src/domain/scale/displacements/displacement.entity.interface";
import { IDisplacementEntity } from "./dtos/displacement.entity";

export interface DisplacementState {
  displacements: ResponsePaginateDto<IDisplacementEntity>;
  displacement: IDisplacementEntity;
  option: string;
}

const initialState: DisplacementState = {
  displacements: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  option: "",
  displacement: {} as any,
};

const displacementStore = createSlice({
  name: "escalafon@displacement",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IDisplacementEntity>>
    ) => {
      state.displacements = payload as any;
    },
    setLicense: (state, { payload }: PayloadAction<IDisplacementEntity>) => {
      state.displacement = payload;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    updateItem: (
      state: DisplacementState,
      { payload }: PayloadAction<IDisplacementEntityInterface>
    ) => {
      state.displacements.items = state.displacements.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.displacement };
    },
  },
});

export const displacementReducer = displacementStore.reducer;

export const displacementActions = displacementStore.actions;
