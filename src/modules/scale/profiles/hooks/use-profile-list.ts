import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { profileActions } from "../store";
import { PaginateDto } from "@services/dtos";

const request = scaleRequest();

export const useProfileList = (defaultPaginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(defaultPaginate?.page || 1);
  const [limit, setLimit] = useState<number>(defaultPaginate?.limit || 100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultPaginate?.querySearch || ""
  );

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`profiles`, { params })
      .then((res) => dispatch(profileActions.paginate(res.data)))
      .catch(() => dispatch(profileActions.paginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    querySearch,
    setPage,
    setLimit,
    setQuerySearch,
    fetch,
  };
};
