import React, { useEffect } from "react";
import { IProfileEntity } from "../dtos/profile.entity";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { useProfileList } from "../hooks/use-profile-list";
import { SelectBasic } from "@common/select/select-basic";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const ProfileSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { profiles } = useSelector((state: RootState) => state.profile);
  const profileList = useProfileList({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
  });

  const settingsData = (row: IProfileEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.id,
      onj: row,
    };
  };

  useEffect(() => {
    profileList.fetch();
  }, [profileList.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={profiles?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={profileList.setQuerySearch}
    />
  );
};
