import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IProfileEntity } from "./dtos/profile.entity";

export interface ProfilesState {
  profiles: ResponsePaginateDto<IProfileEntity>;
}

const initialState: ProfilesState = {
  profiles: {
    meta: {
      totalItems: 0,
      itemsPerPage: 30,
      totalPages: 0,
    },
    items: [],
  },
};

export const profileSlice = createSlice({
  name: "scale@profiles",
  initialState,
  reducers: {
    paginate: (
      state: ProfilesState,
      action: PayloadAction<ResponsePaginateDto<IProfileEntity>>
    ) => {
      state.profiles = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.profile };
    },
  },
});

export const profileReducer = profileSlice.reducer;

export const profileActions = profileSlice.actions;
