import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IHourhandEntity } from "./dtos/hourhand.entity";
import { IConfigHourhandEntity } from "../config-hourhands/domain/config-hourhand.entity";

export interface HourhandState {
  hourhand: IHourhandEntity;
  hourhandConfigs: ResponsePaginateDto<IConfigHourhandEntity>;
  configHourhand: IConfigHourhandEntity | undefined | null;
}

export const initialState: HourhandState = {
  hourhand: {} as any,
  hourhandConfigs: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  configHourhand: null,
};

const hourhandState = createSlice({
  name: "escalafon@hourhands",
  initialState,
  reducers: {
    setConfigHourhandsPaginate: (
      state: HourhandState,
      { payload }: PayloadAction<ResponsePaginateDto<IConfigHourhandEntity>>
    ) => {
      state.hourhandConfigs = payload;
    },
    setHourhand: (
      state: HourhandState,
      { payload }: PayloadAction<IHourhandEntity>
    ) => {
      state.hourhand = payload;
    },
    setConfigHourhand: (
      state,
      { payload }: PayloadAction<IConfigHourhandEntity | undefined>
    ) => {
      state.configHourhand = payload;
    },
  },
  extraReducers: {
    [HYDRATE](state: any, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.contract };
    },
  },
});

export const hourhandReducer = hourhandState.reducer;

export const hourhandActions = hourhandState.actions;
