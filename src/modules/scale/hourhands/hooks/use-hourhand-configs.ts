/* eslint-disable no-async-promise-executor */
/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { scaleRequest } from "@services/scale.request";
import { hourhandActions, initialState } from "../store";
import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { IConfigHourhandEntity } from "@modules/scale/config-hourhands/domain/config-hourhand.entity";

const request = scaleRequest();

export interface HourhandConfigsRequest {
  dateStart?: string;
  dateOver?: string;
}

export const useHourhandConfigs = () => {
  const dispatch = useDispatch();

  const { hourhand } = useSelector((state: RootState) => state.hourhand);

  const [pending, setPending] = useState<boolean>(false);
  const [isSucces, setIsSuccess] = useState<boolean>(false);

  const fetch = async (
    filters: HourhandConfigsRequest
  ): Promise<ResponsePaginateDto<IConfigHourhandEntity>> => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsSuccess(false);
      const params = new URLSearchParams();
      params.set("limit", "31");
      if (filters?.dateStart) params.set("dateStart", `${filters.dateStart}`);
      if (filters?.dateOver) params.set("dateOver", `${filters.dateOver}`);
      // get request
      await request
        .get(`hourhands/${hourhand?.id}/configHourhands`, { params })
        .then((res) => {
          setIsSuccess(true);
          dispatch(hourhandActions.setConfigHourhandsPaginate(res.data));
          resolve(res.data);
        })
        .catch((err) => {
          setIsSuccess(false);
          dispatch(
            hourhandActions.setConfigHourhandsPaginate(
              initialState.hourhandConfigs
            )
          );
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
    isSucces,
  };
};
