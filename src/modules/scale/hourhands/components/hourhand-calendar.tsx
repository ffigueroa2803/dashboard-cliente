/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { FullCalendar } from "@common/fullcalendar";
import { CalendarApi, EventInput } from "@fullcalendar/react";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHourhandConfigs } from "../hooks/use-hourhand-configs";
import { IConfigHourhandEntity } from "@modules/scale/config-hourhands/domain/config-hourhand.entity";
import { ConfigHourhandSerialize } from "@modules/scale/config-hourhands/serializes/config-hourhand.serialize";
import { plainToClass } from "class-transformer";
import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { hourhandActions, initialState } from "../store";
import { HourhandEdit } from "./hourhand-edit";

export function HourhandCalendar() {
  const dispatch = useDispatch();
  const calendarRef = useRef(null);
  const hourhandConfig = useHourhandConfigs();

  const { hourhand } = useSelector((state: RootState) => state.hourhand);

  const [option, setOption] = useState<string | undefined>();

  const handleData = () => {
    const current: any = calendarRef?.current;
    const calendarApi: CalendarApi = current?._calendarApi;
    const dateCurrent = DateTime.fromJSDate(calendarApi.getDate());
    const dateStart = dateCurrent.set({ day: 1 }).toSQLDate();
    const dateOver = dateCurrent
      .set({ day: dateCurrent.daysInMonth })
      .toSQLDate();

    hourhandConfig
      .fetch({ dateStart, dateOver })
      .then((data) => draw(data))
      .catch(() => draw(initialState.hourhandConfigs));
  };

  const formatterDate = (
    event: IConfigHourhandEntity,
    obj: IConfigHourhandEntity
  ) => {
    const entryEvent = plainToClass(ConfigHourhandSerialize, event);
    // response
    return {
      id: `${event.id}`,
      title: entryEvent.displayFormatter,
      start: event.date,
      allDay: true,
      className: entryEvent.displayBackground,
      borderColor: "transparent",
      extendedProps: obj,
      classNames: "cursor-pointer",
    } as EventInput;
  };

  const draw = (data: ResponsePaginateDto<IConfigHourhandEntity>) => {
    const current: any = calendarRef?.current;
    const calendarApi: CalendarApi = current?._calendarApi;
    calendarApi.removeAllEvents();
    const events: EventInput[] = [];
    data?.items?.forEach((item) => {
      const { exit } = item;
      events.push(formatterDate(item, item));
      if (exit?.id) {
        events.push(formatterDate(exit, item));
      }
    });
    // add events
    calendarApi.addEventSource({ events });
  };

  const handlePrev = () => {
    const current: any = calendarRef?.current;
    const calendarApi: CalendarApi = current?._calendarApi;
    calendarApi.prev();
    handleData();
  };

  const handleNext = () => {
    const current: any = calendarRef?.current;
    const calendarApi: CalendarApi = current?._calendarApi;
    calendarApi.next();
    handleData();
  };

  const handleToday = () => {
    const current: any = calendarRef?.current;
    const calendarApi: CalendarApi = current?._calendarApi;
    calendarApi.today();
    handleData();
  };

  const handleEvent = ({ event }: any) => {
    dispatch(hourhandActions.setConfigHourhand(event.extendedProps));
    setOption("EDIT");
  };

  useEffect(() => {
    if (calendarRef.current && hourhand?.id) handleData();
  }, [calendarRef, hourhand]);

  return (
    <div>
      <FullCalendar
        myRef={calendarRef}
        eventClick={handleEvent}
        customButtons={{
          next: {
            text: "Next",
            click: handleNext,
          },
          prev: {
            text: "Prev",
            click: handlePrev,
          },
          today: {
            text: "Today",
            click: handleToday,
          },
        }}
      />
      {/* edit */}
      <HourhandEdit
        isOpen={option == "EDIT"}
        onClose={() => setOption(undefined)}
        onSave={handleData}
      />
    </div>
  );
}
