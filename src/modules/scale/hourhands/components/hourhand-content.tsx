import { Plus } from "react-feather";
import { Button, Col, Row } from "reactstrap";
import { HourhandSelect } from "./hourhand-select";
import { HourhandCalendar } from "./hourhand-calendar";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { hourhandActions } from "../store";
import { useState } from "react";
import { Show } from "@common/show";
import { ConfigHourhandCreate } from "@modules/scale/config-hourhands/infrastructure/components/config-hourhand-create";

export function HourhandContent() {
  const dispatch = useDispatch();
  const { hourhand } = useSelector((state: RootState) => state.hourhand);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <>
      <div className="card">
        <div className="card-header">
          <Row>
            <Col>
              <HourhandSelect
                name="hourhandId"
                value={hourhand?.id}
                onChange={(obj) =>
                  dispatch(hourhandActions.setHourhand(obj.row))
                }
              />
            </Col>
            <Col>
              <Show condition={!!hourhand?.id}>
                <Button color="primary" onClick={() => setIsOpen(true)}>
                  <Plus className="icon" />
                </Button>
              </Show>
            </Col>
          </Row>
        </div>
        <div className="card-body">
          {hourhand?.id ? (
            <HourhandCalendar />
          ) : (
            <div className="text-center">
              Seleccionar Turno para mostrar calendario
            </div>
          )}
        </div>
      </div>
      {/* crear */}
      <ConfigHourhandCreate
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        onSave={() => setIsOpen(false)}
      />
    </>
  );
}
