/* eslint-disable react-hooks/exhaustive-deps */
import Toggle from "@atlaskit/toggle";
import { RootState } from "@store/store";
import { useFormik } from "formik";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import Select from "react-select";
import {
  Button,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useEffect, useState } from "react";
import {
  HourhandConfigEditBody,
  HourhandConfigEditYup,
  hourhandConfigEditInitial,
} from "@modules/scale/config-hourhands/infrastructure/dtos/hourhand-config-edit.dto";
import { useEditConfigHourhandMutation } from "@modules/scale/config-hourhands/infrastructure/features/config-hourhand-rtk";
import { toast } from "react-toastify";
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";

export interface HourhandEditProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

const options = [
  { label: "Ingreso", value: "ENTRY" },
  { label: "Salida", value: "EXIT" },
];

export function HourhandEdit({ isOpen, onClose, onSave }: HourhandEditProps) {
  const { configHourhand } = useSelector((state: RootState) => state.hourhand);
  const [mode, setMode] = useState<{ label: string; value: any }>(options[0]);
  const [configHourhandId, setConfigHourhandId] = useState<number | undefined>(
    configHourhand?.id
  );

  const [fetch, { isLoading }] = useEditConfigHourhandMutation();

  const formik = useFormik<HourhandConfigEditBody>({
    initialValues: hourhandConfigEditInitial,
    validationSchema: HourhandConfigEditYup,
    onSubmit: (body) => {
      if (!configHourhandId) return;
      fetch({ id: configHourhandId, body })
        .unwrap()
        .then(() => {
          toast.success(`Datos guardados correctamente!`);
          onSave();
        })
        .catch(() => toast.error(`No se pudó guardar los datos!`));
    },
  });

  const handleData = () => {
    if (!configHourhand) formik.setValues(hourhandConfigEditInitial);
    if (mode && mode.value == "ENTRY") {
      setConfigHourhandId(configHourhand?.id);
      formik.setValues({
        date: configHourhand?.date || "",
        checkInTime: configHourhand?.checkInTime || "",
        tolerance: configHourhand?.tolerance || 0,
        isApplied: configHourhand?.isApplied || false,
        forDays: configHourhand?.forDays || 1,
      });
    } else if (mode && mode.value == "EXIT") {
      setConfigHourhandId(configHourhand?.exit?.id);
      formik.setValues({
        date: configHourhand?.exit?.date || "",
        checkInTime: configHourhand?.exit?.checkInTime || "",
        tolerance: configHourhand?.exit?.tolerance || 0,
        isApplied: configHourhand?.exit?.isApplied || false,
        forDays: configHourhand?.exit?.forDays || 1,
      });
    } else {
      setConfigHourhandId(configHourhand?.exit?.id);
      formik.setValues(hourhandConfigEditInitial);
    }
  };

  useEffect(() => {
    if (isOpen) handleData();
  }, [isOpen, mode]);

  useEffect(() => {
    if (!isOpen) setMode(options[0]);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Información del Horario</ModalHeader>
      <form onSubmit={formik.handleSubmit}>
        <ModalBody>
          <FormGroup>
            <label>Tipo</label>
            <Select
              value={mode}
              options={options}
              onChange={(value: any) => setMode(value)}
            />
          </FormGroup>
          <FormGroup>
            <label>Fecha</label>
            <Input
              type="date"
              name="date"
              value={formik.values.date}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </FormGroup>
          <FormGroup>
            <label>Hora</label>
            <Input
              type="time"
              name="checkInTime"
              value={formik.values.checkInTime}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </FormGroup>
          <FormGroup>
            <label>Tolerancia</label>
            <Input
              type="number"
              name="tolerance"
              value={formik.values.tolerance}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </FormGroup>
          <FormGroup>
            <label>Días de descuentos</label>
            <Input
              type="number"
              name="forDays"
              value={formik.values.forDays}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </FormGroup>
          <FormGroup>
            <label>¿Aplica a descuento?</label>
            <div>
              <Toggle
                name="isApplied"
                isChecked={formik.values.isApplied}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </div>
          </FormGroup>
        </ModalBody>
        <ModalFooter className="text-right">
          <Show
            condition={!isLoading}
            isDefault={
              <ButtonLoading loading color="primary" title="Actualizando..." />
            }
          >
            <Button color="primary" type="submit">
              <Save className="icon" /> Actualizar
            </Button>
          </Show>
        </ModalFooter>
      </form>
    </Modal>
  );
}
