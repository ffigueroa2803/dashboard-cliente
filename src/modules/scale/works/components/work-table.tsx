/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useMemo, useState } from "react";
import DataTable, { Direction } from "react-data-table-component";
import {
  PaginationChangePage,
  PaginationChangeRowsPerPage,
} from "react-data-table-component/dist/src/DataTable/types";
import { WorkEntity } from "../dtos/work.entity";
import { Search, ChevronRight } from "react-feather";
import { Input, Form, FormGroup, Row, Col, Button } from "reactstrap";
import { SelectBasic } from "@common/select/select-basic";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { PersonGenderSelect } from "@modules/auth/person/components/person-gender-select";

// eslint-disable-next-line no-unused-vars
declare type onClick = (work: WorkEntity) => void;

interface IPropsRow {
  name: string;
  grow: boolean;
  center: boolean;
  selector: (row: WorkEntity) => any;
}

interface IParamOnQuerySearch {
  querySearch?: string | string[];
  typeCargoId?: any;
  condition?: any;
  gender?: "F" | "M";
  state?: boolean;
}

interface IProps {
  data: WorkEntity[];
  perPage?: number;
  totalItems?: number;
  loading?: boolean;
  options?: IPropsRow[];
  defaultFilters?: IParamOnQuerySearch;
  onChangePage?: PaginationChangePage;
  onChangeRowsPerPage?: PaginationChangeRowsPerPage;
  onQuerySearch?: (params: IParamOnQuerySearch) => void;
  onClick?: onClick;
}

export const WorkTable = ({
  data,
  perPage,
  totalItems,
  loading,
  defaultFilters,
  onQuerySearch,
  onChangePage,
  onChangeRowsPerPage,
  onClick,
  options,
}: IProps) => {
  const [filter, setFilter] = useState<any>({
    state: defaultFilters?.state,
    typeCargoId: defaultFilters?.typeCargoId,
    condition: defaultFilters?.condition,
    querySearch: defaultFilters?.querySearch,
  });

  const handleFilter = ({ name, value }: any) => {
    setFilter((filter: any) => ({ ...filter, [name]: value }));
  };

  const columns = useMemo(() => {
    const result = [
      {
        name: "#ID",
        grow: true,
        selector: (row: WorkEntity) => row.id,
      },
      {
        name: "Apellidos",
        wrap: true,
        selector: (row: WorkEntity) => {
          return (
            <span className="uppercase">
              <span>{row?.person?.lastname || ""}</span>
              <span className="ml-1">{row?.person?.secondaryName || ""}</span>
            </span>
          );
        },
      },
      {
        name: "Nombres",
        wrap: true,
        selector: (row: WorkEntity) => (
          <span className="uppercase">{row?.person?.name || ""}</span>
        ),
      },
      {
        name: "N° Documento",
        selector: (row: WorkEntity) => `${row?.person?.documentNumber || ""}`,
      },
      {
        name: "N° CUSSP",
        selector: (row: WorkEntity) => `${row.numberOfCussp || ""}`,
      },
      {
        name: "Estado",
        grow: true,
        selector: (row: WorkEntity) => (
          <span className={`badge badge-${row.state ? "success" : "danger"}`}>
            {row.state ? "Activo" : "Inactivo"}
          </span>
        ),
      },
      {
        name: "Opción",
        center: true,
        grow: true,
        selector: (row: WorkEntity) => (
          <ChevronRight
            className="cursor-pointer"
            onClick={() => handleClick(row)}
          />
        ),
      },
    ];
    // add options
    if (options?.length) {
      result.push(...options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return result;
  }, [data]);

  function handleSearch() {
    if (typeof onQuerySearch == "function") {
      onQuerySearch(filter);
    }
  }

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    handleSearch();
  };

  const handleClick = (work: WorkEntity) => {
    if (typeof onClick == "function") onClick(work);
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="6 col-4" className="mb-2">
              <Input
                name="querySearch"
                value={filter?.querySearch || ""}
                onChange={({ target }) => handleFilter(target)}
                disabled={loading}
              />
            </Col>
            <Col md="4 col-6" className="mb-2">
              <SelectBasic
                name="state"
                value={filter?.state}
                options={[
                  { label: "Activos", value: true },
                  { label: "Inactivos", value: false },
                ]}
                onChange={(obj) => handleFilter(obj)}
              />
            </Col>
            <Col md="2 col-3" className="mb-2">
              <Button color="primary" block disabled={loading}>
                <Search size={15} />
              </Button>
            </Col>
            {/* filtro cargo */}
            <Col md="6 col-6 col-lg-6" className="mb-2">
              <TypeCargoSelect
                value={filter?.typeCargoId || ""}
                name="typeCargoId"
                onChange={(target) => handleFilter(target)}
              />
            </Col>
            {/* filtro condition */}
            <Col md="4 col-6 col-lg-4 " className="mb-2">
              <ContractConditionSelect
                value={filter.condition || ""}
                name="condition"
                onChange={(target) => handleFilter(target)}
              />
            </Col>
            {/* filtro sexo */}
            <Col md="2 col-6 col-lg-2" className="mb-2">
              <PersonGenderSelect
                placeholder="Sexo"
                name="gender"
                value={filter?.gender}
                onChange={handleFilter}
              />
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={loading || false}
        data={data}
        pagination
        paginationPerPage={perPage || 30}
        paginationServer
        paginationTotalRows={totalItems || 30}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </>
  );
};
