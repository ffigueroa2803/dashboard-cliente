import { DateTime } from "luxon";
import React, { useState } from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Input, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";

const currentDate = DateTime.now();
const request = scaleRequest();

export const WorkReportOnomastico = () => {
  const [year, setYear] = useState<number>(currentDate.year);
  const [month, setMonth] = useState<number | undefined>(currentDate.month);
  const [typeCargoId, setTypeCargoId] = useState<string | undefined>();
  const [condition, setCondition] = useState<string | undefined>();
  const [typeCategoryId, setTypeCategoryId] = useState<string | undefined>();
  const [dependencyId, setDependencyId] = useState<string | undefined>();

  const handleClick = () => {
    const params = new URLSearchParams();
    params.set("year", `${year}`);
    if (month) params.set("month", `${month}`);
    if (typeCargoId) params.append("typeCargoIds[0]", typeCargoId);
    if (condition) params.append("conditions[0]", condition);
    if (typeCategoryId) params.append("typeCategoryIds[0]", typeCategoryId);
    if (dependencyId) params.append("dependencyIds[0]", dependencyId);
    // crear url
    const pathBase = urljoin(request.urlBase, "/works/reports/onomastico.pdf");
    const url = `${pathBase}?${params.toString()}`;
    // generate
    const a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Año <b className="text-danger">*</b>
          </label>
          <Input
            type="number"
            value={year}
            onChange={({ target }) => setYear(parseInt(target.value))}
          />
        </FormGroup>
      </Col>

      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Mes</label>
          <Input
            type="number"
            value={month}
            onChange={({ target }) => setMonth(parseInt(target.value))}
          />
        </FormGroup>
      </Col>

      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Cargo</label>
          <TypeCargoSelect
            name="typeCargoId"
            value={typeCargoId}
            onChange={({ value }) => setTypeCargoId(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Condición</label>
          <ContractConditionSelect
            name="condition"
            value={condition}
            onChange={({ value }) => setCondition(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Categoria</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={typeCategoryId}
            onChange={({ value }) => setTypeCategoryId(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Dependencia</label>
          <DependencySelect
            name="dependencyId"
            value={dependencyId}
            onChange={({ value }) => setDependencyId(value)}
          />
        </FormGroup>
      </Col>

      <FormGroup>
        <Button color="danger" onClick={handleClick}>
          <File className="icon" /> PDF
        </Button>
      </FormGroup>
    </Row>
  );
};
