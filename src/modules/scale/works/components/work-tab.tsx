/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { InfoContent } from "@modules/planilla/infos/components/info-content";
import { infoEntityName } from "@modules/planilla/infos/dtos/info.entity";
import { ConfigVacationContent } from "@modules/scale/config-vacations/components/config-vacation-content";
import { findContract } from "@modules/scale/contracts/apis";
import { ContractContent } from "@modules/scale/contracts/components/contract-content";
import {
  IContractEntity,
  contractEntityName,
} from "@modules/scale/contracts/dtos/contract.entity";
import { contractActions } from "@modules/scale/contracts/store";
import { FamilyContent } from "@modules/scale/families/components/family-content";
import { familyEntityName } from "@modules/scale/families/dtos/family.entity.dto";
import { vacationEntityName } from "@modules/scale/vacations/dtos/vacation.entity.dto";
import { RootState } from "@store/store";
import { useContext, useMemo } from "react";
import { File, Home, PlusSquare, Send, Settings, Target } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Card,
  Col,
  FormGroup,
  Nav,
  NavItem,
  NavLink,
  Row,
} from "reactstrap";
import { ContractCreate } from "../../contracts/components/contract-create";
import { workEntityName } from "../dtos/work.entity";
import { workActions } from "../store";
import { WorkContent } from "./work-content";

const switchOptions = {
  CREATE_CONTRACT: "CREATE_CONTRACT",
  CREATE_FAMILY: "CREATE_FAMILY",
  CREATE_CONFIG_VACATION: "CREATE_CONFIG_VACATION",
  CREATE_INFO: "CREATE_INFO",
};

interface IPropsItem {
  Icon: any;
  title: string;
  index: number;
}

const ItemTab = ({ Icon, title, index }: IPropsItem) => {
  const dispatch = useDispatch();
  const { tabIndex } = useSelector((state: RootState) => state.work);

  return (
    <NavItem
      className="cursor-pointer"
      onClick={() => dispatch(workActions.changeTab(index))}
    >
      <NavLink className={tabIndex == index ? "active" : ""}>
        <Icon /> {title}
      </NavLink>
    </NavItem>
  );
};

export const WorkTab = () => {
  const dispatch = useDispatch();

  const ability = useContext(CaslContext);

  const { mode } = useSelector((state: RootState) => state.screen);
  const { tabIndex, option } = useSelector((state: RootState) => state.work);

  const tabItems = (): any[] => {
    const currentItems: any[] = [];
    // validar work
    if (ability.can(PermissionAction.READ, workEntityName)) {
      currentItems.push({
        title: "General",
        icon: Target,
        content: <WorkContent />,
        btnCreate: "Nuevo Contrato",
        eventBtnCreate: switchOptions.CREATE_CONTRACT,
        condition: workEntityName,
      });
    }
    // validar contract
    if (ability.can(PermissionAction.READ, contractEntityName)) {
      currentItems.push({
        title: "Contratos",
        icon: File,
        content: <ContractContent />,
        btnCreate: "Nuevo Contrato",
        eventBtnCreate: switchOptions.CREATE_CONTRACT,
        condition: contractEntityName,
      });
    }
    // validar family
    if (ability.can(PermissionAction.READ, familyEntityName)) {
      currentItems.push({
        title: "Familia",
        icon: Home,
        content: <FamilyContent />,
        btnCreate: "Nuevo Familiar",
        eventBtnCreate: switchOptions.CREATE_FAMILY,
        condition: familyEntityName,
      });
    }
    // validar vacations
    if (ability.can(PermissionAction.READ, vacationEntityName)) {
      currentItems.push({
        title: "Vacaciones",
        icon: Send,
        content: <ConfigVacationContent />,
        btnCreate: "Nueva Vacación",
        eventBtnCreate: switchOptions.CREATE_CONFIG_VACATION,
        condition: vacationEntityName,
      });
    }
    // validar Info
    if (ability.can(PermissionAction.READ, infoEntityName)) {
      currentItems.push({
        title: "Config. Pago",
        icon: Settings,
        content: <InfoContent />,
        btnCreate: "Nueva Configuración",
        eventBtnCreate: switchOptions.CREATE_INFO,
        condition: infoEntityName,
      });
    }
    // response
    return currentItems;
  };

  const currentContent = useMemo(() => {
    return tabItems()[tabIndex] || {};
  }, [tabIndex]);

  const displayTextBtn = () => {
    const allower = ["sm", "xs"];
    const isAllower = allower.includes(`${mode}`);
    if (isAllower) return "";
    return currentContent?.btnCreate || "";
  };

  const handleSave = (data: IContractEntity) => {
    dispatch(workActions.changeOption(""));
    dispatch(workActions.changeTab(1));
    dispatch(contractActions.find(data));
    findContract(data.id)
      .then((contract) => dispatch(contractActions.find(contract)))
      .catch(() => dispatch(contractActions.find({} as any)));
  };

  return (
    <>
      <div className="project-list">
        <Card>
          <Row>
            <Col md="8" sm="9">
              <Nav tabs className="border-tab">
                {tabItems().map((i, index) => (
                  <ItemTab
                    key={`item-tab-index-${index}`}
                    title={i.title}
                    Icon={i.icon}
                    index={index}
                  />
                ))}
              </Nav>
            </Col>
            <Show condition={currentContent?.btnCreate ? true : false}>
              <Show
                condition={ability.can(
                  PermissionAction.CREATE,
                  currentContent?.condition
                )}
              >
                <Col md="4" sm="3">
                  <div className="text-center">
                    <FormGroup className="mb-0 mr-0"></FormGroup>
                    <Button
                      color="primary"
                      title="Nuevo contrato"
                      onClick={() =>
                        dispatch(
                          workActions.changeOption(
                            currentContent?.eventBtnCreate || ""
                          )
                        )
                      }
                    >
                      <PlusSquare /> {displayTextBtn()}
                    </Button>
                  </div>
                </Col>
              </Show>
            </Show>
          </Row>
        </Card>
      </div>
      {/* content */}
      {currentContent?.content || null}
      {/* crear contrato */}
      <ContractCreate
        isOpen={switchOptions.CREATE_CONTRACT == option}
        onSave={handleSave}
        onClose={() => dispatch(workActions.changeOption(""))}
      />
    </>
  );
};
