/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button } from "reactstrap";
import { WorkEntity } from "src/domain/scale/works/work.entity";
import { IWorkEntityInterface } from "src/domain/scale/works/work.entity.interface";
import { useWorkFolderCreate } from "../hooks/use-work-folder-create";
import { workActions } from "../store";

export interface IWorkFolderCreateProps {
  work: IWorkEntityInterface;
}

export const WorkFolderCreate = ({ work }: IWorkFolderCreateProps) => {
  const dispatch = useDispatch();
  const contractFolderCreate = useWorkFolderCreate(work);

  const handle = () => {
    contractFolderCreate
      .execute()
      .then((data) => {
        const newWork = new WorkEntity(work);
        newWork.setFolder(data);
        dispatch(workActions.find(newWork));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => contractFolderCreate.abort();
  }, []);

  return (
    <div className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={contractFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </div>
  );
};
