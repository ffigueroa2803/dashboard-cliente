import { LoadingSimple } from "@common/loading/components/loading-simple";
import { ContractEdit } from "@modules/scale/contracts/components/contract-edit";
import { ContractSerialize } from "@modules/scale/contracts/dtos/contract-serialize";
import { contractActions } from "@modules/scale/contracts/store";
import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { useEffect, useState } from "react";
import { Edit, File } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { useWorkAppointment } from "../hooks/work-appointment";

export const WorkAppointment = () => {
  const dispatch = useDispatch();

  const { work, appointment } = useSelector((state: RootState) => state.work);

  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);

  const workAppointment = useWorkAppointment();

  const contractSerialize = plainToClass(ContractSerialize, appointment || {});

  const handleEdit = () => {
    dispatch(contractActions.find(appointment));
    setIsEdit(true);
  };

  const handleSave = () => {
    setIsRefresh(true);
    setIsEdit(false);
  };

  useEffect(() => {
    if (work?.id) setIsRefresh(true);
  }, [work]);

  useEffect(() => {
    if (isRefresh) workAppointment.fetch();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  if (workAppointment.pending) {
    return (
      <div className="text-center mt-3">
        <LoadingSimple loading={true} />
      </div>
    );
  }

  if (!appointment?.id) {
    return null;
  }

  return (
    <>
      <Row>
        <Col md="12" className="mb-1 mt-3">
          <hr />
          <h6>
            <File className="icon" /> Datos de Nombramiento
            <span className="close cursor-pointer">
              <Edit className="icon" onClick={handleEdit} />
            </span>
          </h6>
          <hr />
        </Col>

        <Col md="6" className="mb-3">
          <label>N° Resolución</label>
          <h6>{appointment?.resolution || "N/A"}</h6>
        </Col>

        <Col md="6" className="mb-3">
          <label>Fecha Resolución</label>
          <h6>{contractSerialize?.displayDateOfResolution || "N/A"}</h6>
        </Col>

        <Col md="6" className="mb-3">
          <label>Fecha de Ingreso</label>
          <h6>{contractSerialize?.displayDateOfAdmission || "N/A"}</h6>
        </Col>

        <Col md="6" className="mb-3">
          <label>Fecha de Cese</label>
          <h6>{contractSerialize?.displayTerminationDate || "N/A"}</h6>
        </Col>
      </Row>
      {/* edit contract */}
      <ContractEdit
        isOpen={isEdit}
        onClose={() => setIsEdit(false)}
        onSave={handleSave}
      />
    </>
  );
};
