/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { MarkerCreate } from "@modules/zkteco/marker/components/marker-create";
import { MarkerList } from "@modules/zkteco/marker/components/marker-list";
import { useLazyPaginateMarkerQuery } from "@modules/zkteco/marker/features/marker-rkt";
import { MarkerCreateInitial } from "@modules/zkteco/marker/yup/marker-create.yup";
import { paginateInitial } from "@services/entities/paginate.entity";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Clock, Plus } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card, CardBody, CardFooter, CardHeader } from "reactstrap";

export const WorkZkteco = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateMarkerQuery();

  const { work } = useSelector((state: RootState) => state.work);

  const handle = () => {
    fetch({ page: 1, limit: 100, token: `${work?.id}` })
      .unwrap()
      .catch(() => toast.error("No se pudo obtener los registros del agente"));
  };

  const onSave = () => {
    handle();
    setIsOpen(false);
  };

  useEffect(() => {
    if (work?.id) handle();
  }, [work?.id]);

  return (
    <Card className="widget-joins widget-arrow">
      <CardHeader>
        <h6>
          <Clock className="icon" /> Reloj
        </h6>
      </CardHeader>
      <CardBody>
        <MarkerList
          data={data || paginateInitial}
          isLoading={isLoading || isFetching}
          onRefresh={handle}
        />
      </CardBody>
      <CardFooter className="text-right">
        <Button color="success" outline onClick={() => setIsOpen(true)}>
          <Plus className="icon" />
        </Button>
      </CardFooter>
      {/* crear marker  */}
      <MarkerCreate
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        onSave={onSave}
        valueInitial={{
          ...MarkerCreateInitial,
          token: `${work?.id}`,
          credentialNumber: parseInt(`1${work?.person?.documentNumber || ""}`),
        }}
      />
    </Card>
  );
};
