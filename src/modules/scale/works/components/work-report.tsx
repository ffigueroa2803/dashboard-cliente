/* eslint-disable react-hooks/exhaustive-deps */
import { NavbarContext } from "@common/navbar/navbar-context";
import { Show } from "@common/show";
import { BallotReport } from "@modules/scale/ballots/components/ballot-report";
import { ConfigVacationReport } from "@modules/scale/config-vacations/components/config-vacation-report";
import { ContractReportAdmission } from "@modules/scale/contracts/components/contract-report-admission";
import { ContractReportTermination } from "@modules/scale/contracts/components/contract-report-termination";
import { LicenseReport } from "@modules/scale/licenses/components/license-report";
import { VacationReport } from "@modules/scale/vacations/components/vacation-report";
import React, { FC, useContext, useEffect, useState } from "react";
import {
  ArrowLeft,
  Calendar,
  Cpu,
  File,
  Gift,
  Settings,
  UserMinus,
  UserPlus,
  CreditCard,
  CheckSquare,
  RefreshCcw,
} from "react-feather";
import { ContractReportActive } from "../../contracts/components/contract-report-active";
import { WorkReportOnomastico } from "./work-report-onomastico";
import { VacationExecuteReport } from "@modules/scale/vacations/components/vacation-execute-report";
import { InfoCreateMassive } from "@modules/planilla/infos/components/info-create-massive";
import { InfoSyncConfigPay } from "@modules/planilla/infos/components/info-sync-config-pay";

interface IOptionProps {
  key: string;
  name: string;
  icon: any;
  content: any;
}

export const ListOptions = () => {
  const navbarContext = useContext(NavbarContext);

  const [currentOption, setCurrentOption] = useState<IOptionProps | undefined>(
    undefined
  );

  const options: IOptionProps[] = [
    {
      key: "new_contract",
      name: "Nuevo Ingreso",
      icon: <UserPlus className="icon" />,
      content: <ContractReportAdmission />,
    },
    {
      key: "dateTermination",
      name: "Termino de Contrato",
      icon: <UserMinus className="icon" />,
      content: <ContractReportTermination />,
    },
    {
      key: "contracts",
      name: "Contratos Activos",
      icon: <CheckSquare className="icon" />,
      content: <ContractReportActive />,
    },
    {
      key: "sync_config_infos",
      name: "Sincronizar config. de pagos",
      icon: <RefreshCcw className="icon" />,
      content: <InfoSyncConfigPay />,
    },
    {
      key: "config_infos",
      name: "Configuración de Pagos",
      icon: <CreditCard className="icon" />,
      content: <InfoCreateMassive />,
    },
    {
      key: "onomastico",
      name: "Onomástico",
      icon: <Gift className="icon" />,
      content: <WorkReportOnomastico />,
    },
    {
      key: "config-vacation",
      name: "Vacaciones Prog.",
      icon: <Settings className="icon" />,
      content: <ConfigVacationReport />,
    },
    {
      key: "vacation",
      name: "Vacaciones",
      icon: <Calendar className="icon" />,
      content: <VacationReport />,
    },
    {
      key: "vacation-execute",
      name: "Vacaciones en Ejecución",
      icon: <Cpu className="icon" />,
      content: <VacationExecuteReport />,
    },
    {
      key: "license",
      name: "Licencias",
      icon: <Calendar className="icon" />,
      content: <LicenseReport />,
    },
    {
      key: "ballot",
      name: "Papeletas",
      icon: <File className="icon" />,
      content: <BallotReport />,
    },
  ];

  const handleClick = (e: any, opt: IOptionProps) => {
    e.preventDefault();
    switch (opt.key) {
      default:
        setCurrentOption(opt);
    }
  };

  useEffect(() => {
    if (navbarContext.show) {
      setCurrentOption(undefined);
      navbarContext.setTitle("Opciones");
    }
  }, [navbarContext.show]);

  return (
    <div className="link-section">
      <Show condition={typeof currentOption?.name == "undefined"}>
        <ul className="svg-icon">
          {options.map((opt, index) => (
            <li key={`ul-item-${index}`} className="mb-2">
              <div>
                <a href="#" onClick={(e) => handleClick(e, opt)}>
                  {opt.icon}
                  {opt.name}
                </a>
              </div>
            </li>
          ))}
        </ul>
      </Show>
      {/* current-option */}
      <Show condition={typeof currentOption?.name != "undefined"}>
        <div>
          <h6
            className="cursor-pointer"
            onClick={() => setCurrentOption(undefined)}
          >
            <ArrowLeft className="icon" /> {currentOption?.name || "N/A"}
          </h6>
          <hr />
          {currentOption?.content || null}
        </div>
      </Show>
    </div>
  );
};

export const WorkReport: FC = ({ children }) => {
  const navbarContext = useContext(NavbarContext);

  useEffect(() => {
    navbarContext.setShow(true);
    return () => {
      navbarContext.setShow(false);
      navbarContext.setContent(null);
    };
  }, []);

  useEffect(() => {
    if (navbarContext.isOpen) {
      navbarContext.setContent(<ListOptions />);
    }
  }, [navbarContext.isOpen]);

  useEffect(() => {
    if (!navbarContext.isOpen) {
      navbarContext.setContent(null);
    }
  }, [navbarContext.isOpen]);

  return <>{children}</>;
};
