import { AscentContent } from "@modules/scale/ascents/components/ascent-content";
import { DegreeContent } from "@modules/scale/degrees/components/degree-content";
import { RootState } from "@store/store";
import { Activity, ArrowUpRight, Book } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import { workActions } from "../store";
import { WorkAppointment } from "./work-appointment";
import { WorkInfo } from "./work-info";
import { WorkZkteco } from "./work-zkteco";

const optionActions = {
  DEGREE: "DEGREE",
  ASCENT: "ASCENT",
};

const ListActions = [
  {
    label: "Grados/Titulos",
    icon: Book,
    key: optionActions.DEGREE,
  },
  {
    label: "Acensos",
    icon: ArrowUpRight,
    key: optionActions.ASCENT,
  },
];

export const WorkContent = () => {
  const { work, option } = useSelector((state: RootState) => state.work);

  const dispatch = useDispatch();

  const handleClick = (e: MouseEvent, key: string) => {
    e.preventDefault();
    console.log(key);
    if (work?.id) {
      dispatch(workActions.changeOption(key));
    }
  };

  return (
    <>
      {/* body */}
      <Row>
        <Col md="8">
          <WorkInfo isFile>
            <WorkAppointment />
          </WorkInfo>
        </Col>
        <Col md="4">
          <Row>
            {/* acciones */}
            <Col>
              <Card>
                <CardHeader>
                  <h6>
                    <Activity className="icon" /> Acciones
                  </h6>
                </CardHeader>
                <CardBody>
                  {ListActions.map((l, index: number) => (
                    <div className="mb-2 capitalize" key={`list-${index}`}>
                      <a
                        href="#"
                        onClick={(e: any) => handleClick(e, l.key)}
                        className={work?.id ? "" : "text-muted"}
                      >
                        <l.icon className="icon" /> {l.label}
                      </a>
                    </div>
                  ))}
                </CardBody>
              </Card>
            </Col>
            <Col md="12 col-6">
              <WorkZkteco />
            </Col>
          </Row>
        </Col>
      </Row>
      {/* contents */}
      <AscentContent
        onClose={() => dispatch(workActions.changeOption(""))}
        isOpen={optionActions.ASCENT == option}
      />
      {/* degrees */}
      <DegreeContent
        onClose={() => dispatch(workActions.changeOption(""))}
        isOpen={optionActions.DEGREE == option}
      />
    </>
  );
};
