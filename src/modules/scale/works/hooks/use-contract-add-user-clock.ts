import { useState } from "react";
import { zktecoRequest } from "@services/zkteco.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IInputHandle } from "@common/dtos/input-handle";
import { toast } from "react-toastify";
import { useConfirm } from "@common/confirm/use-confirm";

const request = zktecoRequest();

interface ContractAddUser {
  clockId: number;
  token: string;
  credentialNumber: number;
  permission: 0;
  dateStart: string;
  dateOver?: string;
}

const defaultData: ContractAddUser = {
  clockId: 0,
  token: "",
  credentialNumber: 0,
  permission: 0,
  dateStart: "",
  dateOver: undefined,
};

export const useContractUserAddClock = () => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const confirm = useConfirm();

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<ContractAddUser>({
    ...defaultData,
    token: contract.code,
    dateStart: contract.dateOfAdmission,
    dateOver: contract.terminationDate || undefined,
  });

  const isValid = () => {
    return (
      Object.keys(form).filter((attr: any) => {
        const tmp: any = form;
        const value: any = tmp[attr];
        if (!value) return false;
        return true;
      }).length > 0
    );
  };

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const clearForm = () => {
    setForm({
      ...defaultData,
      token: contract.code,
      dateStart: contract.dateOfAdmission,
      dateOver: contract.terminationDate || undefined,
    });
  };

  const save = () => {
    return new Promise((resolve, reject) => {
      confirm
        .alert({
          title: "Agregar Contrato",
          message: "¿Estás seguro en agregar el contrato al reloj ZKTECO?",
          labelSuccess: "Confirmar",
          labelError: "Cancelar",
        })
        .then(async () => {
          setPending(true);
          await request
            .post(`markers`, form)
            .then((res) => {
              clearForm();
              toast.success(`El contrató se agregará al reloj ZKTECO`);
              resolve(res.data);
            })
            .catch((err) => {
              toast.error(`No se pudó enviar el contrato al reloj ZKTECO`);
              reject(err);
            });
          setPending(false);
        })
        .catch(() => {
          clearForm();
          reject();
        });
    });
  };

  return {
    pending,
    setForm,
    handleForm,
    clearForm,
    form,
    save,
    isValid,
  };
};
