import { RootState } from "@store/store";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { scaleRequest } from "@services/scale.request";
import { workActions } from "../store";

const request = scaleRequest();

export const useWorkAppointment = () => {
  const dispatch = useDispatch();
  const { work } = useSelector((state: RootState) => state.work);

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async () => {
    setPending(true);
    await request
      .get(`works/${work?.id}/appointment`)
      .then((res) => dispatch(workActions.setAppointment(res.data)))
      .catch(() => dispatch(workActions.setAppointment({} as any)));
    setPending(false);
  };

  return {
    pending,
    fetch,
  };
};
