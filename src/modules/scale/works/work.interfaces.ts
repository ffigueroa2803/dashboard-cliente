import { PaginateDto } from "@services/dtos";
import { ContractConditionEnum } from "src/domain/scale/contracts/contract.enum";

export interface IWorkPaginate extends PaginateDto {
  typeCargoIds?: string[] | number[];
  conditions?: ContractConditionEnum[];
  gender?: "M" | "F";
}
