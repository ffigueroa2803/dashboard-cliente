import { scaleRequest } from "@services/scale.request";
import { PaginateDto } from "@services/dtos";
import { WorkEntity } from "./dtos/work.entity";
import { ICreateWorkDto } from "./dtos/create-work.dto";
import { IContractEntity } from "../contracts/dtos/contract.entity";
import { IWorkPaginate } from "./work.interfaces";

const request = scaleRequest();

export const getWorks = async ({
  page,
  querySearch,
  typeCargoIds,
  conditions,
  limit,
  gender,
  state,
}: IWorkPaginate) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  typeCargoIds?.forEach((value, index) =>
    params.append(`typeCargoIds[${index}]`, `${value}`)
  );
  conditions?.forEach((value, index) =>
    params.append(`conditions[${index}]`, `${value}`)
  );
  if (typeof state == "boolean") {
    params.set(`state`, `${state}`);
  }
  // filter gender
  if (gender) params.set("gender", gender);
  // send
  return request
    .get(`works`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const findWork = async (id: number): Promise<WorkEntity> => {
  return request
    .get(`works/${id}`)
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createWork = async (
  payload: ICreateWorkDto
): Promise<WorkEntity> => {
  return request.post(`works`, payload).then((res) => res.data);
};

export const editWork = async (
  id: number,
  payload: ICreateWorkDto
): Promise<WorkEntity> => {
  return request.put(`works/${id}`, payload).then((res) => res.data);
};

export const getWorkToContracts = async (
  id: number,
  { page, querySearch, limit }: PaginateDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  params.append("orders[0][attribute]", "state");
  params.append("orders[0][value]", "DESC");
  params.append("orders[1][attribute]", "dateOfAdmission");
  params.append("orders[1][value]", "DESC");
  return request
    .get(`works/${id}/contracts`, { params })
    .then((res) => res.data);
};

export const findLastContract = async (
  id: number
): Promise<IContractEntity> => {
  const params = new URLSearchParams();
  params.set("page", "1");
  params.set("limit", "1");
  params.append("orders[0][attribute]", "state");
  params.append("orders[0][value]", "DESC");
  params.append("orders[1][attribute]", "dateOfAdmission");
  params.append("orders[1][value]", "DESC");
  return request.get(`works/${id}/contracts`, { params }).then((res: any) => {
    const items: IContractEntity[] = res.data?.items || [];
    return items[0];
  });
};
