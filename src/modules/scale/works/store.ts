import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { WorkEntity } from "./dtos/work.entity";
import { IListYearWork } from "@modules/planilla/works/dtos/list-year-work.entity";
import { IContractEntity } from "../contracts/dtos/contract.entity";
import { IWorkEntityInterface } from "src/domain/scale/works/work.entity.interface";

export interface WorkState {
  works: ResponsePaginateDto<WorkEntity>;
  work: WorkEntity | null;
  appointment: IContractEntity;
  listYears: ResponsePaginateDto<IListYearWork>;
  tabIndex: number;
  option: string;
}

const initialState: WorkState = {
  works: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  work: {} as any,
  appointment: {} as any,
  tabIndex: 0,
  option: "",
  listYears: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
};

const workStore = createSlice({
  name: "escalafon@works",
  initialState,
  reducers: {
    paginate: (
      state: WorkState,
      { payload }: PayloadAction<ResponsePaginateDto<WorkEntity>>
    ) => {
      state.works = payload as any;
    },
    find: (state: WorkState, { payload }: PayloadAction<WorkEntity | null>) => {
      state.work = payload as any;
    },
    changeTab: (state: WorkState, { payload }: PayloadAction<number>) => {
      state.tabIndex = payload;
    },
    changeOption: (state: WorkState, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    paginateListYear: (
      state: WorkState,
      { payload }: PayloadAction<ResponsePaginateDto<IListYearWork>>
    ) => {
      state.listYears = payload;
    },
    setAppointment: (
      state: WorkState,
      { payload }: PayloadAction<IContractEntity>
    ) => {
      state.appointment = payload;
      return state;
    },
    updateItem: (
      state: WorkState,
      { payload }: PayloadAction<IWorkEntityInterface>
    ) => {
      state.works.items = state.works.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.work };
    },
  },
});

export const workReducer = workStore.reducer;

export const workActions = workStore.actions;
