export interface IConfigHourhandEntity {
  id: number;
  date: string;
  checkInTime: string;
  tolerance: number;
  forDays: number;
  isApplied: boolean;
  mode: "ENTRY" | "EXIT";
  exit?: IConfigHourhandEntity;
}
