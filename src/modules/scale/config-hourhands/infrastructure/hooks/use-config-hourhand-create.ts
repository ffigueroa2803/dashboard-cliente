/* eslint-disable react-hooks/exhaustive-deps */
import { useFormik } from "formik";
import {
  HourhandConfigCreateYup,
  configHourhandCreateInitial,
} from "../dtos/config-hourhand-create.dto";
import { DateTime } from "luxon";
import { useCreateConfigHourhandMutation } from "../features/config-hourhand-rtk";
import { toast } from "react-toastify";
import { useEffect } from "react";

export function useConfigHourhandCreate(hourhandId?: number) {
  const [fetch, { isLoading, isSuccess }] = useCreateConfigHourhandMutation();

  const formik = useFormik({
    initialValues: configHourhandCreateInitial,
    validationSchema: HourhandConfigCreateYup,
    onSubmit: (value, helpers) =>
      fetch(value)
        .unwrap()
        .then((data) => {
          const errors = data?.errors?.length || 0;
          if (!errors) throw new Error("Se encontrarón inconsistencias!!!");
          toast.success("Horario programado!!!");
          helpers.resetForm();
          calcDate();
          setHourhand();
        })
        .catch((err) => {
          toast.error(err?.data?.message || "No se pudo programar el horario");
        }),
  });

  const calcDate = () => {
    const dateCurrent = DateTime.now();
    const dateStart = dateCurrent.set({ day: 1 });
    const dataOver = dateCurrent.set({ day: dateCurrent.daysInMonth });
    formik.setFieldValue("dateStart", dateStart.toSQLDate());
    formik.setFieldValue("dateOver", dataOver.toSQLDate());
  };

  const setHourhand = () => {
    formik.setFieldValue("hourhandId", hourhandId);
  };

  useEffect(() => {
    if (hourhandId) setHourhand();
  }, [hourhandId]);

  return { formik, isLoading, isSuccess, calcDate };
}
