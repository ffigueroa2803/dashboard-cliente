import { DateTime } from "luxon";
import { Yup } from "@common/yup";

export interface ConfigHourhandCreateDto {
  hourhandId: number;
  dateStart: string;
  dateOver: string;
  entry: ConfigHourhandBody;
  exit: ConfigHourhandBody;
}

export interface ConfigHourhandBody {
  checkInTime: string;
  tolerance: number;
  isApplied: boolean;
}

export const configHourhandBodyEditInitial = {
  checkInTime: DateTime.now().toFormat("HH:mm"),
  tolerance: 0,
  isApplied: true,
};

export const configHourhandCreateInitial = {
  hourhandId: 0,
  dateStart: DateTime.now().toSQLDate(),
  dateOver: DateTime.now().toSQLDate(),
  entry: configHourhandBodyEditInitial,
  exit: configHourhandBodyEditInitial,
};

export const HourhandConfigBodyCreateYup = Yup.object({
  checkInTime: Yup.string().required(),
  tolerance: Yup.number().required(),
  isApplied: Yup.bool().required(),
});

export const HourhandConfigCreateYup = Yup.object({
  hourhandId: Yup.number().required(),
  dateStart: Yup.string().required(),
  dateOver: Yup.string().required(),
  entry: HourhandConfigBodyCreateYup,
  exit: HourhandConfigBodyCreateYup,
});
