import { DateTime } from "luxon";
import { Yup } from "@common/yup";

export interface HourhandConfigEditDto {
  id: number;
  body: HourhandConfigEditBody;
}

export interface HourhandConfigEditBody {
  date: string;
  checkInTime: string;
  tolerance: number;
  isApplied: boolean;
  forDays: number;
}

export const hourhandConfigEditInitial = {
  date: DateTime.now().toSQLDate(),
  checkInTime: DateTime.now().toFormat("HH:mm:ss"),
  tolerance: 0,
  isApplied: true,
  forDays: 1,
};

export const HourhandConfigEditYup = Yup.object({
  date: Yup.string().required(),
  checkInTime: Yup.string().required(),
  tolerance: Yup.number().required(),
  isApplied: Yup.bool().required(),
  forDays: Yup.number().required(),
});
