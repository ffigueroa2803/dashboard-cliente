import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { HourhandConfigEditDto } from "../dtos/hourhand-config-edit.dto";
import { ConfigHourhandCreateDto } from "../dtos/config-hourhand-create.dto";

const baseUrl = process.env.NEXT_PUBLIC_SCALEV2_URL || "";
const baseUrlAlterno = process.env.NEXT_PUBLIC_SCALE_URL || "";

export const configHourhandRtk = createApi({
  reducerPath: "configHourhandRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    createConfigHourhand: builder.mutation<any, ConfigHourhandCreateDto>({
      query: (body) => ({
        url: `${baseUrlAlterno}configHourhands`,
        method: "POST",
        headers: BaseHeaders,
        body,
      }),
    }),
    editConfigHourhand: builder.mutation<boolean, HourhandConfigEditDto>({
      query: ({ id, body }) => ({
        url: `configHourhands/${id}`,
        method: "PUT",
        headers: BaseHeaders,
        body,
      }),
    }),
  }),
});

export const {
  useCreateConfigHourhandMutation,
  useEditConfigHourhandMutation,
} = configHourhandRtk;
