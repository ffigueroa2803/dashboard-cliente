/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Form,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useConfigHourhandCreate } from "../hooks/use-config-hourhand-create";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import Toggle from "@atlaskit/toggle";
import { Clock, Save } from "react-feather";
import { useEffect } from "react";
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";

export interface ConfigHourhandCreateProps {
  isOpen: boolean;
  onClose(): void;
  onSave(): void;
}

export function ConfigHourhandCreate({
  isOpen,
  onClose,
  onSave,
}: ConfigHourhandCreateProps) {
  const { hourhand } = useSelector((state: RootState) => state.hourhand);
  const { formik, isLoading, isSuccess, calcDate } = useConfigHourhandCreate(
    hourhand?.id
  );

  useEffect(() => {
    if (hourhand?.id) calcDate();
  }, [hourhand]);

  useEffect(() => {
    if (isSuccess) onSave();
  }, [isSuccess]);

  if (!hourhand?.id) return null;

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Crear Horario Mensual</ModalHeader>
      <Form onSubmit={formik.handleSubmit}>
        <ModalBody>
          <FormGroup>
            <label>Horario</label>
            <Input readOnly value={hourhand.name} />
          </FormGroup>

          <FormGroup>
            <label>Fecha Inicio</label>
            <Input readOnly type="date" value={formik.values?.dateStart} />
          </FormGroup>

          <FormGroup>
            <label>Fecha Fin</label>
            <Input readOnly type="date" value={formik.values?.dateOver} />
          </FormGroup>

          <FormGroup>
            <hr />
            <h5>
              <Clock className="icon" /> Ingreso
            </h5>
          </FormGroup>

          <FormGroup>
            <label>
              Hora <b className="text-danger">*</b>
            </label>
            <Input
              type="time"
              name="entry.checkInTime"
              disabled={isLoading}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values?.entry?.checkInTime}
            />
          </FormGroup>

          <FormGroup>
            <label>
              Tolerancia <b className="text-danger">*</b>
            </label>
            <Input
              type="number"
              name="entry.tolerance"
              disabled={isLoading}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values?.entry?.tolerance}
            />
          </FormGroup>

          <FormGroup>
            <label>¿Aplica a descuento?</label>
            <div>
              <Toggle
                isChecked={formik.values?.entry?.isApplied}
                name="entry.isApplied"
                isDisabled={isLoading}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
            </div>
          </FormGroup>

          <FormGroup>
            <hr />
            <h5>
              <Clock className="icon" /> Salida
            </h5>
          </FormGroup>

          <FormGroup>
            <label>
              Hora <b className="text-danger">*</b>
            </label>
            <Input
              type="time"
              name="exit.checkInTime"
              disabled={isLoading}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values?.exit?.checkInTime}
            />
          </FormGroup>

          <FormGroup>
            <label>
              Tolerancia <b className="text-danger">*</b>
            </label>
            <Input
              type="number"
              name="exit.tolerance"
              disabled={isLoading}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values?.exit?.tolerance}
            />
          </FormGroup>

          <FormGroup>
            <label>¿Aplica a descuento?</label>
            <div>
              <Toggle
                name="exit.isApplied"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isChecked={formik.values?.exit?.isApplied}
                isDisabled={isLoading}
              />
            </div>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Show
            condition={isLoading}
            isDefault={
              <Button color="primary" type="submit" disabled={isLoading}>
                <Save className="icon" /> Guardar
              </Button>
            }
          >
            <ButtonLoading loading title="Guardando..." color="primary" />
          </Show>
        </ModalFooter>
      </Form>
    </Modal>
  );
}
