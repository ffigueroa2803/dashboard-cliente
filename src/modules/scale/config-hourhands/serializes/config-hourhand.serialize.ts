import { ScheduleModeEnum } from "@modules/scale/schedules/dtos/schedule.entity";
import { DateTime } from "luxon";

export class ConfigHourhandSerialize {
  private checkInTime!: string;
  private date!: string;
  private mode!: ScheduleModeEnum;
  private assistance?: { id: number };
  private isApplied!: boolean;

  get displayCheckInTime() {
    let format = DateTime.fromFormat(this.checkInTime || "", "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.checkInTime || "", "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  get displayFormatter() {
    // response
    return `${this.assistance?.id ? "✔" : ""} ${this.displayCheckInTime} ${
      this.mode == ScheduleModeEnum.ENTRY ? "Entrada" : "Salida"
    }`;
  }

  get displayDate() {
    return DateTime.fromSQL(this.date).toFormat("dd/MM/yyyy");
  }

  get displayBackground() {
    if (!this.isApplied) return "bg-primary";
    return this.mode == ScheduleModeEnum.ENTRY ? "bg-danger" : "bg-primary";
  }
}
