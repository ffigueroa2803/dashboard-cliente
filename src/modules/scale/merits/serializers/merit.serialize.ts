import { DateTime } from "luxon";
import { MeritEnumMode } from "../dtos/merit.entity";

export class MeritSerialize {
  private documentDate!: string;
  private startDate!: string;
  private terminationDate!: string;
  private mode!: MeritEnumMode;

  get displayDocumentDate() {
    return DateTime.fromSQL(`${this.documentDate}`).toFormat(`dd/MM/yyyy`);
  }

  get displayStartDate() {
    return DateTime.fromSQL(`${this.startDate}`).toFormat(`dd/MM/yyyy`);
  }

  get displayTerminatinDate() {
    return DateTime.fromSQL(`${this.terminationDate}`).toFormat(`dd/MM/yyyy`);
  }

  get displayMode() {
    return MeritEnumMode.MERIT == this.mode ? "Mérito" : "Demérito";
  }
}
