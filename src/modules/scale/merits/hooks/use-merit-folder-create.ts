/* eslint-disable no-async-promise-executor */
import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { IMeritEntityInterface } from "src/domain/scale/merits/merit.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = scaleRequest();

export const useMeritFolderCreate = (merit: IMeritEntityInterface) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFolderEntityInterface>(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .post(
          `merits/${merit.id}/folders`,
          {},
          { signal: abortController.signal }
        )
        .then(({ data }) => resolve(data))
        .catch((err) => reject(err));
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
