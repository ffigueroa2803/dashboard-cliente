import { MeritEnumMode } from "./merit.entity";

export interface IEditMeritDto {
  title: string;
  description: string;
  documentNumber: string;
  documentDate: string;
  startDate: string;
  terminationDate: string;
  mode: MeritEnumMode;
}
