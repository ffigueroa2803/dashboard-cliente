import { MeritEnumMode } from "./merit.entity";

export interface IMeritFormDto {
  contractId: number;
  title: string;
  description: string;
  documentNumber: string;
  documentDate: string;
  startDate: string;
  terminationDate: string;
  mode: MeritEnumMode;
}
