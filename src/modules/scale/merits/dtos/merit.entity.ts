export enum MeritEnumMode {
  MERIT = "MERIT",
  DEMERIT = "DEMERIT",
}

export interface IMeritEntity {
  id: number;
  contractId: number;
  title: string;
  description: string;
  documentNumber: string;
  documentDate: string;
  startDate: string;
  terminationDate: string;
  mode: MeritEnumMode;
}
