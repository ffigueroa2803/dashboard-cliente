import { PaginateDto } from "@services/dtos";

export interface FilterGetMeritsDto extends PaginateDto {
  year?: number;
}
