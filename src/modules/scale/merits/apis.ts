import { scaleRequest } from "@services/scale.request";
import { ICreateMeritDto } from "./dtos/create-merit.dto";
import { IEditMeritDto } from "./dtos/edit-merit.dto";
import { FilterGetMeritsDto } from "./dtos/filter-merits.dto";
import { IMeritEntity } from "./dtos/merit.entity";

const request = scaleRequest();

export const getMeritsToContract = async (
  id: number,
  { page, querySearch, limit, year }: FilterGetMeritsDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  // response
  return await request
    .get(`contracts/${id}/merits`, { params })
    .then((res) => res.data);
};

export const createMerit = async (
  payload: ICreateMeritDto
): Promise<IMeritEntity> => {
  return await request.post(`merits`, payload).then((res) => res.data);
};

export const editMerit = async (
  id: number,
  payload: IEditMeritDto
): Promise<IMeritEntity> => {
  return await request.put(`merits/${id}`, payload).then((res) => res.data);
};

export const deleteMerit = async (id: number): Promise<any> => {
  return await request.destroy(`merits/${id}`).then((res) => res.data);
};
