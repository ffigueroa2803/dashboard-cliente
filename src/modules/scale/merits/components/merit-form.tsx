/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { Form, FormGroup, Input } from "reactstrap";
import { IMeritFormDto } from "../dtos/merit-form.dto";
import { IInputHandle } from "@common/dtos/input-handle";
import { SelectBasic } from "@common/select/select-basic";
import { MeritEnumMode } from "../dtos/merit.entity";

interface IProps {
  form: IMeritFormDto,
  onChange: (input: IInputHandle) => void,
  isDisabled?: boolean;
}

export const MeritForm = ({ form, onChange, isDisabled }: IProps) => {

  return (
    <Form>
      <FormGroup>
        <label>Titulo <b className="text-danger">*</b></label>
        <Input type="text"
          name="title"
          value={form?.title || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>N° Documento <b className="text-danger">*</b></label>
        <Input type="text"
          name="documentNumber"
          value={form?.documentNumber || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Fecha del Documento <b className="text-danger">*</b></label>
        <Input type="date"
          name="documentDate"
          value={form?.documentDate || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Fecha de Inicio <b className="text-danger">*</b></label>
        <Input type="date"
          name="startDate"
          value={form?.startDate || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Fecha de Termino <b className="text-danger">*</b></label>
        <Input type="date"
          name="terminationDate"
          value={form?.terminationDate || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Descripción <b className="text-danger">*</b></label>
        <Input type="textarea"
          name="description"
          value={form?.description || ''}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>Modo <b className="text-danger">*</b></label>
        <SelectBasic
          name="mode"
          value={form?.mode}
          onChange={(obj) => onChange(obj)}
          options={[
            { label: "Mérito", value: MeritEnumMode.MERIT },
            { label: "Demérito", value: MeritEnumMode.DEMERIT }
          ]}
        />
      </FormGroup>
    </Form>
  )
}