import { RootState } from "@store/store";
import { Plus } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { meritActions } from "../store";
import { MeritCreate } from "./merit-create";
import { MeritList } from "./merit-list";

interface IProps {
  isOpen: boolean
  onClose: () => void
}

export const MeritContent = ({ isOpen, onClose }: IProps) => {

  const dispatch = useDispatch();
  const { option } = useSelector((state: RootState) => state.merit);

  return (
    <>
      <Modal isOpen={isOpen}
        size="lg"
        style={{ minWidth: "90%" }}
      >
        <ModalHeader toggle={onClose}>
          Mérito/Demérito
        </ModalHeader>
        <ModalBody>
          <MeritList/>
        </ModalBody>
        <ModalFooter className="text-right">
          <Button outline
            color="success"
            onClick={() => dispatch(meritActions.changeOption("CREATE"))}
          >
            <Plus className="icon"/>
          </Button>
        </ModalFooter>
      </Modal>
      {/* crear license */}
      <MeritCreate 
        onClose={() => dispatch(meritActions.changeOption(""))}
        isOpen={option == "CREATE"}
        onSave={() => dispatch(meritActions.changeOption("REFRESH"))}
      />
    </>
  )
}