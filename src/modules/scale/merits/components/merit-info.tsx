/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { Edit, Home, Info } from "react-feather";
import { useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { WorkUserError } from "src/components/shared/work-user-error";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";
import { MeritEntity } from "src/domain/scale/merits/merit.entity";
import { IMeritEntity } from "../dtos/merit.entity";
import { MeritSerialize } from "../serializers/merit.serialize";
import { MeritFolderCreate } from "./merit-folder-create";

interface IProps {
  merit: IMeritEntity;
  onEdit: (merit: IMeritEntity) => void;
}

export const MeritInfo = ({ merit, onEdit }: IProps) => {
  const { user } = useSelector((state: RootState) => state.person);
  const meritSerialize = plainToClass(MeritSerialize, merit);
  const meritEntity = new MeritEntity(merit);

  const handleMerit = () => {
    if (typeof onEdit == "function") {
      onEdit(merit);
    }
  };

  return (
    <div className="media mb-1">
      <Edit
        className="icon close"
        style={{ zIndex: 99 }}
        onClick={handleMerit}
      />
      <div className="media-body ml-3">
        <Row className="w-100">
          <Col md="12" className="mb-1">
            <label>Titulo</label>
            <h6>{merit?.title || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>N° del Documento</label>
            <h6>{merit?.documentNumber || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Fecha del Documento</label>
            <h6>{meritSerialize?.displayDocumentDate || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Fecha de Inicio</label>
            <h6>{meritSerialize?.displayStartDate || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Fecha de Termino</label>
            <h6>{meritSerialize?.displayTerminatinDate || ""}</h6>
          </Col>
          <Col md="12" className="mb-1">
            <label>Descripción</label>
            <h6>{merit?.description || "N/A"}</h6>
          </Col>

          <Col md="12" className="mb-1">
            <label>Modo</label>
            <h6>{meritSerialize.displayMode || "N/A"}</h6>
          </Col>
        </Row>

        <Col md="12">
          <Show condition={user.id != undefined} isDefault={<WorkUserError />}>
            <Show
              condition={!meritEntity.folderId}
              isDefault={
                <FolderInfoComponent id={meritEntity.folderId || ""} />
              }
            >
              <MeritFolderCreate merit={meritEntity} />
            </Show>
          </Show>
        </Col>
      </div>
    </div>
  );
};
