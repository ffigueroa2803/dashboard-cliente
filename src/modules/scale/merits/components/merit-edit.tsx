/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { RefreshCw, Trash2 } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  ButtonGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { deleteMerit, editMerit } from "../apis";
import { IMeritFormDto } from "../dtos/merit-form.dto";
import { toast } from "react-toastify";
import { IMeritEntity } from "../dtos/merit.entity";
import { MeritForm } from "./merit-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (merit: IMeritEntity) => void;
}

export const MeritEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { merit } = useSelector((state: RootState) => state.merit);

  const [form, setForm] = useState<IMeritFormDto>(merit as any);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await editMerit(merit?.id || 0, form)
      .then((data) => {
        toast.success(`Los cambios se actualizarón correctamente`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo actualizar los datos`));
    setLoading(false);
  };

  const handleDelete = async () => {
    setLoading(true);
    toast.dismiss();
    await deleteMerit(merit?.id || 0)
      .then((data) => {
        toast.success(`El regístro se eliminó correctamente`);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo eliminar el regístro`));
    setLoading(false);
  };

  useEffect(() => {
    if (merit?.id) setForm(merit as any);
  }, [merit]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Mérito/Demérito</ModalHeader>
      <ModalBody>
        <MeritForm form={form} onChange={handleForm} isDisabled={loading} />
      </ModalBody>
      <ModalFooter className="text-right">
        <ButtonGroup>
          <Button
            color="danger"
            outline
            disabled={loading}
            onClick={handleDelete}
          >
            <Trash2 className="icon" />
          </Button>
          <Button color="primary" disabled={loading} onClick={handleSave}>
            <RefreshCw className="icon" />
          </Button>
        </ButtonGroup>
      </ModalFooter>
    </Modal>
  );
};
