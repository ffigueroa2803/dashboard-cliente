/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card } from "reactstrap";
import { MeritEntity } from "src/domain/scale/merits/merit.entity";
import { IMeritEntityInterface } from "src/domain/scale/merits/merit.entity.interface";
import { useMeritFolderCreate } from "../hooks/use-merit-folder-create";
import { meritActions } from "../store";

export interface IMeritFolderCreateProps {
  merit: IMeritEntityInterface;
}

export const MeritFolderCreate = ({ merit }: IMeritFolderCreateProps) => {
  const dispatch = useDispatch();
  const meritFolderCreate = useMeritFolderCreate(merit);

  const handle = () => {
    meritFolderCreate
      .execute()
      .then((data) => {
        const newMerit = new MeritEntity(merit);
        newMerit.setFolder(data);
        dispatch(meritActions.updateItem(newMerit));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => meritFolderCreate.abort();
  }, []);

  return (
    <Card className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={meritFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </Card>
  );
};
