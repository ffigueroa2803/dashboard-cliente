/* eslint-disable react-hooks/exhaustive-deps */
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { Search } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Input, Row } from "reactstrap";
import { getMeritsToContract } from "../apis";
import { IMeritEntity } from "../dtos/merit.entity";
import { meritActions } from "../store";
import { MeritEdit } from "./merit-edit";
import { MeritInfo } from "./merit-info";

export const MeritList = () => {
  const dispatch = useDispatch();
  const { merits, option } = useSelector((state: RootState) => state.merit);
  const { contract } = useSelector((state: RootState) => state.contract);

  const [loading, setLoading] = useState<boolean>(true);
  const [isRefresh, setIsRefresh] = useState<boolean>(true);
  const [perPage] = useState<number>(30);
  const [page] = useState<number>(1);
  const [year, setYear] = useState<number | undefined>(undefined);
  const [querySearch, setQuerySearch] = useState<string>("");
  const [, setIsError] = useState<boolean>(false);

  const handleData = async () => {
    setLoading(true);
    setIsError(false);
    await getMeritsToContract(contract?.id || 0, {
      page,
      limit: perPage,
      year,
      querySearch,
    })
      .then((data) => dispatch(meritActions.paginate(data)))
      .catch(() => setIsError(true));
    setLoading(false);
  };

  const handleEdit = async (merit: IMeritEntity) => {
    dispatch(meritActions.setMerit(merit));
    dispatch(meritActions.changeOption("EDIT"));
  };

  useEffect(() => {
    if (isRefresh) handleData();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  useEffect(() => {
    if (option == "REFRESH") {
      setIsRefresh(true);
    }
  }, [option]);

  useEffect(() => {
    if (option == "REFRESH") {
      dispatch(meritActions.changeOption(""));
    }
  }, [option]);

  return (
    <>
      <div className="prooduct-details-box mb-3">
        <Row>
          {/* buscador */}
          <Col md="6" className="mb-2">
            <Input
              type="text"
              placeholder="Búsqueda por N° Documento..."
              value={querySearch}
              onChange={({ target }) => setQuerySearch(target.value)}
            />
          </Col>
          <Col md="4" className="mb-2">
            <Input
              type="number"
              placeholder="Búsqueda por año..."
              value={year || ""}
              onChange={({ target }) => setYear(parseInt(target.value))}
            />
          </Col>
          <Col md="2" className="mb-2">
            <Button
              block
              color="primary"
              disabled={loading}
              onClick={() => setIsRefresh(true)}
            >
              <Search className="icon" />
            </Button>
          </Col>
          {/* limite */}
          <Col md="12">
            <hr />
          </Col>
          {/* not licenses */}
          <Show condition={!loading && !merits?.items?.length}>
            <Col md="12">
              <RegisterEmptyError onRefresh={() => setIsRefresh(true)} />
            </Col>
          </Show>
          {/* loading */}
          <Show condition={loading}>
            <Col md="12" className="text-center">
              Obtener datos...
            </Col>
          </Show>
          {/* listar licenses */}
          {merits?.items?.map((i) => (
            <Col key={`list-license-${i.id}`} md="6">
              <MeritInfo merit={i as any} onEdit={handleEdit} />
            </Col>
          ))}
        </Row>
      </div>
      {/* editar */}
      <MeritEdit
        isOpen={option == "EDIT"}
        onClose={() => dispatch(meritActions.changeOption(""))}
        onSave={() => dispatch(meritActions.changeOption("REFRESH"))}
      />
    </>
  );
};
