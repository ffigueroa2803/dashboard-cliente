/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useMemo, useState } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { createMerit } from "../apis";
import { IMeritFormDto } from "../dtos/merit-form.dto";
import { MeritForm } from "./merit-form";
import { toast } from "react-toastify";
import { IMeritEntity, MeritEnumMode } from "../dtos/merit.entity";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (merit: IMeritEntity) => void;
}

export const MeritCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const dataDefault: IMeritFormDto = {
    contractId: contract?.id || 0,
    title: "",
    description: "",
    documentNumber: "",
    documentDate: "",
    startDate: "",
    terminationDate: "",
    mode: MeritEnumMode.MERIT,
  };

  const [form, setForm] = useState<IMeritFormDto>(dataDefault);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await createMerit(form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente`);
        setForm(dataDefault);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setLoading(false);
  };

  useEffect(() => {
    if (contract?.id) setForm(dataDefault);
  }, [contract]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nuevo Mérito/Demérito</ModalHeader>
      <ModalBody>
        <MeritForm form={form} onChange={handleForm} isDisabled={loading} />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={loading} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
