import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IMeritEntityInterface } from "src/domain/scale/merits/merit.entity.interface";
import { IMeritEntity } from "./dtos/merit.entity";

export interface MeritState {
  merits: ResponsePaginateDto<IMeritEntity | IMeritEntityInterface>;
  merit: IMeritEntity | IMeritEntityInterface;
  option: string;
}

const initialState: MeritState = {
  merits: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  merit: {} as any,
  option: "",
};

const meritStore = createSlice({
  name: "escalafon@merit",
  initialState,
  reducers: {
    paginate: (
      state,
      { payload }: PayloadAction<ResponsePaginateDto<IMeritEntity>>
    ) => {
      state.merits = payload as any;
    },
    setMerit: (state, { payload }: PayloadAction<IMeritEntity>) => {
      state.merit = payload;
    },
    changeOption: (state, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    updateItem: (
      state: MeritState,
      { payload }: PayloadAction<IMeritEntityInterface>
    ) => {
      state.merits.items = state.merits.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.merit };
    },
  },
});

export const meritReducer = meritStore.reducer;

export const meritActions = meritStore.actions;
