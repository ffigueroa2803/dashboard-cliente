/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { RefreshCw } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { editConfigVacation } from "../apis";
import { IConfigVacationFormDto } from "../dtos/config-vacation-form.dto";
import { IConfigVacationEntity } from "../dtos/config-vacation.entity.dto";
import { ConfigVacationForm } from "./config-vacation-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (configVacation: IConfigVacationEntity) => void;
}

export const ConfigVacationEdit = ({ isOpen, onClose, onSave }: IProps) => {
  const { configVacation } = useSelector(
    (state: RootState) => state.configVacation
  );

  const defaultData: IConfigVacationFormDto = {
    workId: configVacation?.workId || 0,
    year: configVacation?.year,
    typeCargoId: configVacation.typeCargoId,
    startDate: configVacation?.startDate,
    terminationDate: configVacation?.terminationDate,
    observation: configVacation?.observation,
    scheduledDays: configVacation?.scheduledDays,
  };

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IConfigVacationFormDto>(defaultData);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    toast.dismiss();
    setPending(true);
    await editConfigVacation(configVacation.id, form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente!`);
        setForm({
          ...defaultData,
          ...data,
        });
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setPending(false);
  };

  useEffect(() => {
    if (configVacation?.id) setForm(defaultData);
  }, [configVacation]);

  useEffect(() => {
    if (!isOpen) setForm(defaultData);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Editar Vacación</ModalHeader>
      <ModalBody>
        <ConfigVacationForm
          form={form}
          onChange={handleForm}
          isDisabled={pending}
        />
        {/* save */}
        <div className="text-right">
          <Button color="primary" disabled={pending} onClick={handleSave}>
            <RefreshCw className="icon" />
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
