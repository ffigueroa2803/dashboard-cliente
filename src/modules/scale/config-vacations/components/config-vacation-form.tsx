/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Form, FormGroup, Input } from "reactstrap";
import { DateTime } from "luxon";
import { useEffect } from "react";
import { IConfigVacationFormDto } from "../dtos/config-vacation-form.dto";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";

interface IProps {
  form: IConfigVacationFormDto;
  isEdit?: boolean;
  isDisabled?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const ConfigVacationForm = ({ form, onChange, isDisabled }: IProps) => {
  const calcScheduledDays = () => {
    const dateOfAdmission = DateTime.fromSQL(`${form?.startDate}`);
    const terminationDate = DateTime.fromSQL(`${form?.terminationDate}`);
    const diff = terminationDate.diff(dateOfAdmission, "days").days + 1;
    onChange({ name: "scheduledDays", value: diff || 0 });
  };

  useEffect(() => {
    calcScheduledDays();
  }, [form?.startDate, form?.terminationDate]);

  return (
    <Form>
      <FormGroup>
        <label>
          Año <b className="text-danger">*</b>
        </label>
        <Input
          type="number"
          name="year"
          value={form?.year || ""}
          disabled={isDisabled}
          onChange={({ target }) =>
            onChange({
              name: target.name,
              value: parseInt(target.value),
            })
          }
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Inicio <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="startDate"
          value={form?.startDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Termino <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="terminationDate"
          value={form?.terminationDate || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Tipo de Vacación <b className="text-danger">*</b>
        </label>
        <TypeCargoSelect
          name="typeCargoId"
          value={form?.typeCargoId}
          onChange={(obj) => onChange(obj)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Dias Programados <b className="text-danger">*</b>
        </label>
        <Input
          type="number"
          name="scheduledDays"
          value={form?.scheduledDays || 0}
          readOnly
        />
      </FormGroup>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
          disabled={isDisabled}
        />
      </FormGroup>
    </Form>
  );
};
