/* eslint-disable react-hooks/exhaustive-deps */
import { RegisterEmptyError } from "@common/errors/register-empty.error";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "reactstrap";
import { getConfigVacationsByWork } from "../apis";
import { configVacationActions } from "../store";
import { ConfigVacationInfo } from "./config-vacation-info";
import { ConfigVacationCreate } from "./config-vacation-create";
import { workActions } from "@modules/scale/works/store";
import { IConfigVacationEntity } from "../dtos/config-vacation.entity.dto";
import { ConfigVacationEdit } from "./config-vacation-edit";
import { FloatButton } from "@common/button/float-button";
import { File } from "react-feather";
import urljoin from "url-join";
import { scaleRequest } from "@services/scale.request";

const request = scaleRequest();

export const ConfigVacationContent = () => {
  const [pending, setPending] = useState<boolean>(true);
  const [, setIsError] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(true);
  const [isEdit, setIsEdit] = useState<boolean>(false);

  const dispatch = useDispatch();

  const { work, option } = useSelector((state: RootState) => state.work);
  const { configVacations, reports } = useSelector(
    (state: RootState) => state.configVacation
  );

  const handleFamilies = async () => {
    setPending(true);
    setIsError(false);
    await getConfigVacationsByWork(work?.id || 0, { page: 1, limit: 30 })
      .then((data) => dispatch(configVacationActions.paginate(data)))
      .catch(() => setIsError(true));
    setPending(false);
  };

  const handleEdit = (configVacation: IConfigVacationEntity) => {
    dispatch(configVacationActions.setConfigVacation(configVacation));
    setIsEdit(true);
  };

  const handleDetele = () => {
    dispatch(configVacationActions.setConfigVacation({} as any));
    setIsRefresh(true);
  };

  const handleReport = () => {
    const query = new URLSearchParams();
    reports.forEach((report, index) =>
      query.append(`configVacationIds[${index}]`, `${report.id}`)
    );
    // generate url
    const pathBase = urljoin(request.urlBase, `/vacations/reports/list.pdf`);
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
    dispatch(configVacationActions.clearReport());
  };

  useEffect(() => {
    if (isRefresh) handleFamilies();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <Row>
      {/* data empty */}
      <Show condition={!pending && !configVacations?.items?.length}>
        <Col md="12">
          <RegisterEmptyError onRefresh={() => setIsRefresh(true)} />
        </Col>
      </Show>
      {/* listar family */}
      {configVacations?.items?.map((i) => (
        <Col md="12" key={`list-family-${i}`}>
          <ConfigVacationInfo
            configVacation={i}
            onEdit={handleEdit}
            onDelete={handleDetele}
          />
        </Col>
      ))}
      {/* crear config vacation */}
      <ConfigVacationCreate
        isOpen={option == "CREATE_CONFIG_VACATION"}
        onClose={() => dispatch(workActions.changeOption(""))}
        onSave={() => setIsRefresh(true)}
      />
      {/* edit config vacation */}
      <ConfigVacationEdit
        isOpen={isEdit}
        onClose={() => setIsEdit(false)}
        onSave={() => setIsRefresh(true)}
      />
      <Show condition={reports.length > 0}>
        <FloatButton color="danger" icon={<File />} onClick={handleReport} />
      </Show>
    </Row>
  );
};
