/* eslint-disable no-unused-vars */
import {
  Button,
  ButtonGroup,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Row,
} from "reactstrap";
import { IConfigVacationEntity } from "../dtos/config-vacation.entity.dto";
import { Edit, Trash, Settings } from "react-feather";
import { deleteConfigVacation } from "../apis";
import { toast } from "react-toastify";
import { VacationContent } from "@modules/scale/vacations/components/vacation-content";
import { Show } from "@common/show";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { configVacationActions } from "../store";
import { useConfigVacationFind } from "../hooks/config-vacation-find";
import { LoadingSimple } from "@common/loading/components/loading-simple";

interface IProps {
  configVacation: IConfigVacationEntity;
  onEdit?: (configVacation: IConfigVacationEntity) => void;
  onDelete?: (configVacation: IConfigVacationEntity) => void;
}

export const ConfigVacationInfo = ({
  configVacation,
  onEdit,
  onDelete,
}: IProps) => {
  const dispatch = useDispatch();

  const configVacationFind = useConfigVacationFind();

  const [isVacations, setIsVacations] = useState<boolean>(false);

  const handleConfig = () => {
    if (typeof onEdit == "function") {
      onEdit(configVacation);
    }
  };

  const handleDelete = async () => {
    toast.dismiss();
    await deleteConfigVacation(configVacation.id)
      .then(() => {
        toast.success(`La configuración se eliminó correctamente`);
        if (typeof onDelete == "function") onDelete(configVacation);
      })
      .catch(() => toast.error(`No se pudo eliminar, intente más tarde`));
  };

  const handleVacations = () => {
    setIsVacations(true);
    dispatch(configVacationActions.setConfigVacation(configVacation));
  };

  const handleRefresh = () => {
    configVacationFind.fetch(configVacation.id).catch(() => null);
  };

  const handleCheckReport = ({ target }: any) => {
    if (target.checked) {
      checkReport();
    } else {
      uncheckReport();
    }
  };

  const checkReport = () => {
    dispatch(configVacationActions.addReport(configVacation));
  };

  const uncheckReport = () => {
    dispatch(configVacationActions.removeReport(configVacation));
  };

  return (
    <Card>
      <CardHeader>
        <h6>
          <Input
            type="checkbox"
            onChange={handleCheckReport}
            checked={configVacation.isReport}
          />
          Configuración del año {configVacation?.year}
          <Trash className="icon close cursor-pointer" onClick={handleDelete} />
        </h6>
      </CardHeader>
      <CardBody>
        <Row>
          <Col md="12">
            <FormGroup>
              <label>Tipo Vacación</label>
              <h6>{configVacation.typeCargoName || "N/A"}</h6>
            </FormGroup>
          </Col>
          <Col md="6">
            <FormGroup>
              <label>Año</label>
              <h6>{configVacation.year}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Dias Programados</label>
              <h6>{configVacation.scheduledDays}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Dias Usados</label>
              <h6>{configVacation.daysUsed || 0}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Dias Disponibles</label>
              <h6>{configVacation.diffDays || 0}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Fecha de Inicio</label>
              <h6>{configVacation.startDate}</h6>
            </FormGroup>
          </Col>

          <Col md="6">
            <FormGroup>
              <label>Fecha de Termino</label>
              <h6>{configVacation.terminationDate}</h6>
            </FormGroup>
          </Col>

          <Col md="12">
            <FormGroup>
              <label>Observación</label>
              <h6>{configVacation.observation || "N/A"}</h6>
            </FormGroup>
          </Col>
        </Row>
      </CardBody>
      <CardFooter className="text-right">
        <Show
          condition={!configVacationFind.pending}
          isDefault={<LoadingSimple loading />}>
          <ButtonGroup>
            <Button color="dark" outline onClick={handleVacations}>
              <Settings className="icon" />
            </Button>

            <Button color="info" outline onClick={handleConfig}>
              <Edit className="icon" />
            </Button>
          </ButtonGroup>
        </Show>
      </CardFooter>
      {/* vacations */}
      <Show condition={isVacations}>
        <VacationContent
          isOpen={true}
          onClose={() => setIsVacations(false)}
          onCreated={handleRefresh}
          onUpdated={handleRefresh}
          onDeleted={handleRefresh}
        />
      </Show>
    </Card>
  );
};
