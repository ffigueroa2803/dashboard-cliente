/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { RootState } from "@store/store";
import { useState } from "react";
import { Save } from "react-feather";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalHeader } from "reactstrap";
import { createConfigVacation } from "../apis";
import { IConfigVacationFormDto } from "../dtos/config-vacation-form.dto";
import { IConfigVacationEntity } from "../dtos/config-vacation.entity.dto";
import { ConfigVacationForm } from "./config-vacation-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (configVacation: IConfigVacationEntity) => void;
}

export const ConfigVacationCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const { work } = useSelector((state: RootState) => state.work);

  const defaultData: IConfigVacationFormDto = {
    workId: work?.id || 0,
    year: 0,
    startDate: "",
    typeCargoId: 1,
    terminationDate: "",
    observation: "",
    scheduledDays: 0,
  };

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IConfigVacationFormDto>(defaultData);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    toast.dismiss();
    setPending(true);
    await createConfigVacation(form)
      .then((data) => {
        toast.success(`Los datos se guardarón correctamente!`);
        setForm(defaultData);
        if (typeof onSave == "function") onSave(data);
      })
      .catch(() => toast.error(`No se pudo guardar los datos`));
    setPending(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Vacación</ModalHeader>
      <ModalBody>
        <ConfigVacationForm
          form={form}
          onChange={handleForm}
          isDisabled={pending}
        />
        {/* save */}
        <div className="text-right">
          <Button color="primary" disabled={pending} onClick={handleSave}>
            <Save className="icon" />
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
