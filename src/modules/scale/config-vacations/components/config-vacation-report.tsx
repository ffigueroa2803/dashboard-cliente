import React from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { DateTime } from "luxon";
import { useFormik } from "formik";
import {
  ConfigVacationReportDto,
  ConfigVacationReportYup,
} from "../dtos/config-vacation-report.dto";
import { toast } from "react-toastify";
import { InputComponent } from "@common/inputs/input.component";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { InputErrorComponent } from "@common/inputs/input-error.component";
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";

const currentDate = DateTime.now();
const request = scaleRequest();

const dataDefault: ConfigVacationReportDto = {
  yearStart: currentDate.year,
  yearOver: currentDate.year,
};

export const ConfigVacationReport = () => {
  const formik = useFormik({
    initialValues: dataDefault,
    validationSchema: ConfigVacationReportYup,
    onSubmit: () => {},
  });

  const handleClick = (extname: string) => {
    if (!formik.isValid) return toast.warning("Filtros incorrectos");
    const filters = formik.values;
    const query = new URLSearchParams();
    query.set("yearStart", `${filters.yearStart}`);
    query.set("yearOver", `${filters.yearOver}`);
    // filter mes
    if (filters?.month) query.set("month", `${filters.month}`);
    // filter cargos
    if (filters?.typeCargoId) {
      query.set("typeCargoIds[0]", `${filters.typeCargoId}`);
    }
    // filters conditions
    if (filters?.condition) {
      query.set("conditions[0]", `${filters.condition}`);
    }
    // filters type categories
    if (filters?.typeCategoryId) {
      query.set("typeCategoryIds[0]", `${filters.typeCargoId}`);
    }
    // filters dependencias
    if (filters?.dependencyId) {
      query.set("dependencyIds[0]", `${filters.dependencyId}`);
    }
    // generate url
    const pathBase = urljoin(
      request.urlBase,
      `/configVacations/reports/list.${extname}`
    );
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  const handleSelect = (option: any) => {
    option.value = option.value || undefined;
    formik.handleChange({ target: option });
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Año Inicio <b className="text-danger">*</b>
          </label>
          <InputComponent name="yearStart" type="text" formik={formik} />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Año Termino <b className="text-danger">*</b>
          </label>
          <InputComponent name="yearOver" type="text" formik={formik} />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Cargo</label>
          <TypeCargoSelect
            name="typeCargoId"
            value={formik.values?.typeCargoId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="typeCargoId" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Condición</label>
          <ContractConditionSelect
            name="condition"
            value={formik.values?.condition || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
          <InputErrorComponent formik={formik} name="condition" />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Categoria</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={formik.values?.typeCategoryId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Dependencia</label>
          <DependencySelect
            name="dependencyId"
            value={formik.values?.dependencyId || ""}
            onChange={handleSelect}
            onBlur={formik.handleBlur}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}>
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
