import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IConfigVacationEntity } from "./dtos/config-vacation.entity.dto";

export interface ConfigVacationState {
  configVacations: ResponsePaginateDto<IConfigVacationEntity>;
  configVacation: IConfigVacationEntity;
  option: string;
  reports: IConfigVacationEntity[];
}

const initialState: ConfigVacationState = {
  configVacations: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  configVacation: {} as any,
  option: "",
  reports: [],
};

const configVacationStore = createSlice({
  name: "escalafon@configVacations",
  initialState,
  reducers: {
    paginate: (
      state: ConfigVacationState,
      { payload }: PayloadAction<ResponsePaginateDto<IConfigVacationEntity>>
    ) => {
      state.configVacations = payload as any;
    },
    changeOption: (
      state: ConfigVacationState,
      { payload }: PayloadAction<string>
    ) => {
      state.option = payload;
    },
    setConfigVacation: (
      state: ConfigVacationState,
      { payload }: PayloadAction<IConfigVacationEntity>
    ) => {
      state.configVacation = payload;
      state.configVacations.items.map((item) => {
        if (item.id == payload.id) return Object.assign(item, payload);
        return item;
      });
    },
    clearReport: (state: ConfigVacationState) => {
      state.reports = [];
      state.configVacations.items = state.configVacations.items?.map((item) => {
        item.isReport = false;
        return item;
      }) as any[];
      return state;
    },
    addReport: (
      state: ConfigVacationState,
      { payload }: PayloadAction<IConfigVacationEntity>
    ) => {
      state.reports.push(payload);
      state.configVacations.items = state.configVacations.items?.map((item) => {
        if (item.id == payload.id) {
          item.isReport = true;
        }
        return item;
      }) as any[];
      return state;
    },
    removeReport: (
      state: ConfigVacationState,
      { payload }: PayloadAction<IConfigVacationEntity>
    ) => {
      state.reports = state.reports.filter(
        (report) => report.id != payload.id
      ) as any[];
      state.configVacations.items = state.configVacations.items?.map((item) => {
        if (item.id == payload.id) {
          item.isReport = false;
        }
        return item;
      }) as any[];
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.configVacation };
    },
  },
});

export const configVacationReducer = configVacationStore.reducer;

export const configVacationActions = configVacationStore.actions;
