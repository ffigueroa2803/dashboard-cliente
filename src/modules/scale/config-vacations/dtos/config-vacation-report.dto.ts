import { Yup } from "@common/yup";

export interface ConfigVacationReportDto {
  yearStart: number;
  yearOver: number;
  month?: number;
  typeCargoId?: number;
  typeCategoryId?: number;
  condition?: string;
  dependencyId?: number;
}

export const ConfigVacationReportYup = Yup.object({
  yearStart: Yup.number().required(),
  yearOver: Yup.number().required(),
  month: Yup.number().optional(),
  typeCargoId: Yup.number().optional(),
  typeCategoryId: Yup.number().optional(),
  condition: Yup.string().optional(),
  dependencyId: Yup.number().optional(),
});
