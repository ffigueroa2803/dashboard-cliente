export interface IConfigVacationCreateDto {
  workId: number;
  year: number;
  startDate: string;
  terminationDate: string;
  observation: string;
  scheduledDays: number;
}
