export interface IConfigVacationEntity {
  id: number;
  workId: number;
  year: number;
  typeCargoId: number;
  typeCargoName?: string;
  startDate: string;
  terminationDate: string;
  observation: string;
  scheduledDays: number;
  daysUsed: number;
  diffDays: number;
  isReport?: boolean;
}
