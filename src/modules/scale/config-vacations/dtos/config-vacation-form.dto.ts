export interface IConfigVacationFormDto {
  workId: number;
  year: number;
  typeCargoId: number;
  startDate: string;
  terminationDate: string;
  observation: string;
  scheduledDays: number;
}
