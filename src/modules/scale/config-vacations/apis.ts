import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { PaginateDto } from "@services/dtos";
import { scaleRequest } from "@services/scale.request";
import { IConfigVacationCreateDto } from "./dtos/config-vacation-create.dto";
import { IConfigVacationEntity } from "./dtos/config-vacation.entity.dto";

const request = scaleRequest();

export const getConfigVacationsByWork = async (
  id: number,
  { page, querySearch, limit }: PaginateDto
): Promise<ResponsePaginateDto<IConfigVacationEntity>> => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  return await request
    .get(`works/${id}/configVacations`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};

export const createConfigVacation = async (
  payload: IConfigVacationCreateDto
): Promise<IConfigVacationEntity> => {
  return await request.post(`configVacations`, payload).then((res) => res.data);
};

export const editConfigVacation = async (
  id: number,
  payload: any
): Promise<IConfigVacationEntity> => {
  return await request
    .put(`configVacations/${id}`, payload)
    .then((res) => res.data);
};

export const deleteConfigVacation = async (
  id: number
): Promise<{ deleted: boolean }> => {
  return await request.destroy(`configVacations/${id}`).then((res) => res.data);
};
