import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { configVacationActions } from "../store";

const request = scaleRequest();

export const useConfigVacationFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async (id: number) => {
    return new Promise((resolve, reject) => {
      setPending(true);
      request
        .get(`configVacations/${id}`)
        .then((res) => {
          dispatch(configVacationActions.setConfigVacation(res.data));
          resolve(res.data);
        })
        .catch((err) => reject(err));
      setPending(false);
    });
  };

  return {
    pending,
    fetch,
  };
};
