/* eslint-disable no-unused-vars */
import { SelectRemote } from "@common/select/select-remote";
import { TypeDegreeEntity } from "../domain/type-degree.entity";
import { useTypeDegreePaginate } from "../hooks/use-type-degree-paginate";

interface IProps {
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const TypeDegreeSelect = ({
  name,
  value,
  onChange,
  defaultQuerySearch,
}: IProps) => {
  const hook = useTypeDegreePaginate();

  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={hook.handle}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{
        label: (row: TypeDegreeEntity) => `${row.name}`.toLowerCase(),
        value: (row) => row.id,
      }}
    />
  );
};
