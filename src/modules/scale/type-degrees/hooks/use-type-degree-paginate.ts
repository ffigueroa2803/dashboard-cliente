import { useState } from "react";
import { toast } from "react-toastify";
import { TypeDegreePaginateParams } from "../domain/type-degree.params";
import { useLazyPaginateTypeDegreesQuery } from "../features/type-degree-rtk";

export function useTypeDegreePaginate(defaultFilters?: TypeDegreePaginateParams) {
  const [fetch, { isLoading, isFetching, data }] = useLazyPaginateTypeDegreesQuery();

  const [filters, setFilters] = useState<TypeDegreePaginateParams>(
    Object.assign({ page: 1, limit: 100 }, defaultFilters || {})
  )

  const handle = async () => {
    return fetch(filters)
      .unwrap()
      .catch((err: any) => toast.error(err.data?.message || "Algo salío mal"))
  }

  return {
    isFetching,
    isLoading,
    handle,
    filters,
    setFilters,
    data
  }
}