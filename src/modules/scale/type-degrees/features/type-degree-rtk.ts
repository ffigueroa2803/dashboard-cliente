import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { TypeDegreeEntity } from "../domain/type-degree.entity";
import { TypeDegreePaginateParams } from "../domain/type-degree.params";

const baseUrl = process.env.NEXT_PUBLIC_SCALEV2_URL || "";

export const typeDegreeRtk = createApi({
  reducerPath: "typeDegreeRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    paginateTypeDegrees: builder.query<PaginateEntity<TypeDegreeEntity>, TypeDegreePaginateParams>({
      query: (params) => ({
        url: 'typeDegrees',
        params
      })
    })
  }),
});

export const { useLazyPaginateTypeDegreesQuery } = typeDegreeRtk;
