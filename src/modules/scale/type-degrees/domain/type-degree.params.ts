export interface TypeDegreeFindParams {

}

export interface TypeDegreeListParams {

}

export interface TypeDegreePaginateParams {
  page: number;
  limit: number;
}