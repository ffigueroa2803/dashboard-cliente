export interface TypeDegreeEntity {
  id: number;
  name: string;
}