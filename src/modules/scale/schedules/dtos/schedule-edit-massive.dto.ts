import { Yup } from "@common/yup";
import { ConditionContractType } from "@modules/scale/contracts/dtos/contract.entity";
import { ScheduleModeEnum } from "./schedule.entity";

export const scheduleEditMassiveData = Yup.object().shape({
  checkInTime: Yup.string().required(),
  tolerance: Yup.number().required(),
  isApplied: Yup.boolean().required(),
  observation: Yup.string(),
});

export const scheduleEditMassiveFilter = Yup.object().shape({
  dateStart: Yup.date().required(),
  dateOver: Yup.date().required(),
  mode: Yup.string().required(),
  typeCategoryId: Yup.number(),
  typeCargoId: Yup.number(),
  condition: Yup.string(),
  dependencyId: Yup.number(),
});

export const scheduleEditMassiveValidate = Yup.object().shape({
  data: scheduleEditMassiveData,
  filter: scheduleEditMassiveFilter,
});

export interface IScheduleEditMassiveData {
  checkInTime: string;
  tolerance: number;
  isApplied: boolean;
  observation?: string;
}

export interface IScheduleEditMassiveFilter {
  dateStart: string;
  dateOver: string;
  mode: ScheduleModeEnum;
  typeCategoryId?: number;
  typeCargoId?: number;
  condition?: ConditionContractType;
  dependencyId?: number;
}

export interface IScheduleEditMassive {
  data: IScheduleEditMassiveData;
  filter: IScheduleEditMassiveFilter;
}
