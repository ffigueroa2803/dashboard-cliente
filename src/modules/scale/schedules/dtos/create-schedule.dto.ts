import { ScheduleModeEnum } from "./schedule.entity";

export interface ICreateScheduleDto {
  date: string;
  checkInTime: string;
  tolerance: number;
  observation?: string;
  mode: ScheduleModeEnum;
}

export interface ICreateCollectionScheduleDto {
  contractId: number;
  scheduleableId: number;
  scheduleableType: string;
  entry: ICreateScheduleDto;
  exit: ICreateScheduleDto;
}
