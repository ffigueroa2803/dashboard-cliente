import { IAssistanceEntity } from "@modules/scale/assistances/dtos/assistance.entity";
import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";

/* eslint-disable no-unused-vars */
export enum ScheduleModeEnum {
  ENTRY = "ENTRY",
  EXIT = "EXIT",
}

export enum ScheduleEnumType {
  ContractEntity = "ContractEntity",
}

export interface IScheduleEntity {
  id: number;
  contractId: number;
  scheduleableType: ScheduleEnumType;
  scheduleableId: number;
  scheduleableObject: any;
  index: number;
  date: string;
  checkInTime: string;
  tolerance: number;
  observation?: string;
  mode: ScheduleModeEnum;
  description: string;
  isApplied: boolean;
  state: boolean;
  entry?: IScheduleEntity;
  exit?: IScheduleEntity;
  assistance?: IAssistanceEntity;
  contract?: IContractEntity;
}
