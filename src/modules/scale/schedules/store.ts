import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IScheduleEntity } from "./dtos/schedule.entity";

export interface ScheduleState {
  schedules: ResponsePaginateDto<IScheduleEntity>;
  schedule: IScheduleEntity;
  option: string;
}

export const initialState: ScheduleState = {
  schedules: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  option: "",
  schedule: {} as any,
};

const scheduleStore = createSlice({
  name: "escalafon@schedule",
  initialState,
  reducers: {
    paginate: (
      state: ScheduleState,
      { payload }: PayloadAction<ResponsePaginateDto<IScheduleEntity>>
    ) => {
      state.schedules = payload as any;
    },
    setItemPaginate(
      state: ScheduleState,
      { payload }: PayloadAction<{ id: number; item: IScheduleEntity }>
    ) {
      state.schedules.items?.map((i) => {
        if (payload.id != i.id) return i;
        return Object.assign(i, payload.item);
      });
    },
    setSchedule: (
      state: ScheduleState,
      { payload }: PayloadAction<IScheduleEntity>
    ) => {
      state.schedule = payload;
      return state;
    },
    findSchedule: (
      state: ScheduleState,
      { payload }: PayloadAction<number>
    ) => {
      const schedule = state.schedules?.items?.find((i) => i.id == payload);
      if (schedule) state.schedule = schedule;
      return state;
    },
    changeOption: (
      state: ScheduleState,
      { payload }: PayloadAction<string>
    ) => {
      state.option = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.schedule };
    },
  },
});

export const scheduleReducer = scheduleStore.reducer;

export const scheduleActions = scheduleStore.actions;
