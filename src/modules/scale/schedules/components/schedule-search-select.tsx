import React, { useState } from 'react';
import { Input, Button } from 'reactstrap';
import { getSchedules } from '../apis';
import { IScheduleEntity, ScheduleEnumType } from '../dtos/schedule.entity';

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onAdd: (person: IScheduleEntity) => void
}

export const ScheduleSearchSelect = ({ onAdd }: IProps) => {

  const [people, setPeople] = useState<IScheduleEntity[]>([]);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [pending, setPending] = useState<boolean>(false);

  const displayText = (schedule: IScheduleEntity) => {
    if (schedule?.scheduleableType == ScheduleEnumType.ContractEntity) {
      return schedule?.scheduleableObject?.work?.person?.fullName;
    }
  }

  const handleSearch = async (value: string) => {
    if (value.length > 3) {
      setIsOpen(true);
      setPending(true)
      await getSchedules({ page: 1, querySearch: value })
        .then(({ items }) => setPeople(items))
        .catch(err => console.log(err))
    } else setPeople([])
    // quitar dialog
    if (!value) setIsOpen(false);
    // quitar pending
    setPending(false);
  }

  const handleAdd = async (schedule: IScheduleEntity) => {
    setIsOpen(false);
    if (typeof onAdd == 'function') onAdd(schedule);
  }

  const ComponentAddPeople = (
    <div className='text-center'>
      No hay regístros
    </div>
  )

  const ComponentDialog = () => {
    if (!isOpen) return null;
    return (
      <div className='dropdown-body card card-body'>
        {people?.map(person => 
          <div key={`item-search-${person.id}`}
            className="capitalize cursor-pointer dropdown-item"
            onClick={() => handleAdd(person)}
          >
            <span className='badge badge-primary mr-2'>
              {/* {person?.documentNumber} */}
            </span>
            <span className='capitalize'>{displayText(person)}</span>
          </div>  
        )}
        {/* loading */}
        <span className='text-center'>
          {pending ? 'Buscando...' : ''}
        </span>
        {/* no hay datos */}
        <span className='text-center'>
          {!pending && !people.length ? ComponentAddPeople : ''}
        </span>
      </div>
    )
  }

  return (
    <div className='dropdown-content'>
      <Input type="text"
        placeholder='buscar...'
        onChange={({ target }) => handleSearch(target.value)}
      />
      {ComponentDialog()}
    </div>
  )
}