import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { scheduleActions } from "../store";

const request = scaleRequest();

export const useScheduleFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const fetch = (id: number) => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      await request
        .get(`schedules/${id}`)
        .then(({ data }) => {
          dispatch(scheduleActions.setSchedule(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(scheduleActions.setSchedule({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    isError,
    fetch,
  };
};
