/* eslint-disable no-undef */
/* eslint-disable react-hooks/exhaustive-deps */
import { useFormik } from "formik";
import { useState } from "react";
import { toast } from "react-toastify";
import { alerStatus } from "@common/errors/utils/alert-status";
import { scaleRequest } from "@services/scale.request";
import {
  IScheduleEditMassive,
  scheduleEditMassiveValidate,
} from "../dtos/schedule-edit-massive.dto";
import { ScheduleModeEnum } from "../dtos/schedule.entity";

const request = scaleRequest();

export const useScheduleMassive = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isUpdated, setIsUpdated] = useState<boolean>(false);

  const save = async (values: any): Promise<any> => {
    setPending(true);
    setIsUpdated(false);
    await request
      .put(`schedules/process/schema`, values)
      .then(() => {
        setIsUpdated(true);
        toast.success(`Los datos se guardarón correctamente`);
      })
      .catch((err: any) => {
        if (err?.response?.data) {
          const data = err?.response?.data;
          const status = data?.status || 501;
          return alerStatus(status);
        }

        return toast.error(`Algo salió mal!!!`);
      });
    setPending(false);
  };

  const formik = useFormik({
    initialValues: {
      data: { checkInTime: "", tolerance: 0, isApplied: true },
      filter: {
        dateStart: "",
        dateOver: "",
        mode: ScheduleModeEnum.ENTRY,
      },
    } as IScheduleEditMassive,
    validationSchema: scheduleEditMassiveValidate,
    onSubmit: save,
  });

  return {
    pending,
    isUpdated,
    form: formik.values,
    formik,
    setForm: formik.setValues,
    isValid: formik.isValid,
    errors: formik.errors,
    handleSubmit: formik.handleSubmit,
    handleChange: formik.handleChange,
    handleBlur: formik.handleBlur,
  };
};
