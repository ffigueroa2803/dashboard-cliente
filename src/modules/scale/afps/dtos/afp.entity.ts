export interface AfpEntity {
  id: number;
  name: string;
  typeAfp: string;
  isPrivate: boolean;
}

export const afpEntityName = "AfpEntity";
