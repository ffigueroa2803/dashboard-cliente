import { DateTime } from "luxon";

export class AssistanceSerialize {
  private token!: string;
  private checkInTime!: string;

  get displayCheckInTime() {
    if (!this.checkInTime) return null;
    let format = DateTime.fromFormat(this.checkInTime, "HH:mm:ss");
    if (!format.isValid) {
      format = DateTime.fromFormat(this.checkInTime, "HH:mm");
    }
    // response
    return format.toFormat("HH:mm");
  }

  exists() {
    return typeof this.token != "undefined";
  }
}
