import { AttendanceEntity } from "src/domain/zkteco/attendance.entity";

export interface AssistancesListResponse {
  id: number;
  personId: number;
  personLastname: string;
  personSecondaryName: string;
  personName: string;
  workId: number;
  contractId: number;
  typeCategoryId: number;
  typeCategoryName: string;
  entryDate: string;
  entryCheckInTime: string;
  exitDate: string;
  exitCheckInTime: string;

  attendances: AttendanceEntity[];
  activities: {
    activityId: string | number;
    title: string;
    description: string;
    tag: string;
  }[];
}
