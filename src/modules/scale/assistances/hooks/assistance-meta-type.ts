import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { assistanceActions } from "../store";
import { IZktecoObject } from "../dtos/assistance-meta-type.object";

const request = scaleRequest();

export const useAssistanceMetaType = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const fetch = (token: string) => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      await request
        .get(`assistances/${token}/metaType`)
        .then(({ data }) => {
          dispatch(assistanceActions.setMetaType(data as IZktecoObject));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(assistanceActions.setMetaType({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    isError,
    fetch,
  };
};
