/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { IInputHandle } from "@common/dtos/input-handle";
import { PaginateDto } from "@services/dtos";
import { RootState } from "@store/store";

const request = scaleRequest();

export interface AssistanceListRequest extends PaginateDto {}

export const useAssistanceActivityList = (
  defaultQuery?: AssistanceListRequest
) => {
  const { assistance } = useSelector((state: RootState) => state.assistance);

  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const defaultData = {
    page: defaultQuery?.page || 1,
    limit: defaultQuery?.limit || 30,
  };

  const [query, setQuery] = useState<AssistanceListRequest>(defaultData);

  const handleQuery = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("limit", `${query.limit}`);
      params.set("querySearch", `${query.querySearch || ""}`);
      // request
      await request
        .get(
          `assistances/${assistance.contractId}/activities/${assistance.id}`,
          { params }
        )
        .then(({ data }) => {
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          reject(err);
        });
      setPending(false);
    });
  };

  const clearQuery = () => {
    setQuery(defaultData);
  };

  const clearQueryToPage = () => {
    setQuery((prev) => ({
      ...prev,
      page: defaultData.page,
    }));
  };

  return {
    pending,
    query,
    isError,
    fetch,
    handleQuery,
    setQuery,
    clearQuery,
    clearQueryToPage,
  };
};
