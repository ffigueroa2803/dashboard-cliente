import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch } from "react-redux";
import { assistanceActions } from "../store";

const request = scaleRequest();

export const useAssistanceFind = () => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const fetch = (token: string) => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      await request
        .get(`assistances/${token}`)
        .then(({ data }) => {
          dispatch(assistanceActions.setAssistance(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(assistanceActions.setAssistance({} as any));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    isError,
    fetch,
  };
};
