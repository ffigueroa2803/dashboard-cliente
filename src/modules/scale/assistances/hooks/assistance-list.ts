/* eslint-disable no-async-promise-executor */
import { IInputHandle } from "@common/dtos/input-handle";
import { scaleRequestV2 } from "@services/scale.request";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { FilterGetAssistancesDto } from "../dtos/filter-assistances.dto";
import { assistanceActions, initialState } from "../store";

const request = scaleRequestV2();

export const useAssistanceList = (defaultQuery?: FilterGetAssistancesDto) => {
  const dispatch = useDispatch();
  const [pending, setPending] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const defaultData = {
    page: defaultQuery?.page || 1,
    limit: defaultQuery?.limit || 30,
    dateStart: defaultQuery?.dateStart || "",
    dateOver: defaultQuery?.dateOver || "",
    querySearch: defaultQuery?.querySearch || "",
    typeCargoId: defaultQuery?.typeCargoId || null,
    condition: defaultQuery?.condition || null,
  };

  const [query, setQuery] = useState<FilterGetAssistancesDto>(defaultData);

  const handleQuery = ({ name, value }: IInputHandle) => {
    setQuery((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      setIsError(false);
      const params = new URLSearchParams();
      params.set("page", `${query.page}`);
      params.set("limit", `${query.limit}`);
      params.set("querySearch", `${query.querySearch}`);
      // filter type cargo
      if (query.typeCargoId) {
        params.set("contract[typeCargoId]", `${query.typeCargoId}`);
      }
      // filter condition
      if (query.condition) {
        params.set("contract[condition]", `${query.condition}`);
      }
      // filter dateStart
      if (query.dateStart) {
        params.set("dateStart", query.dateStart);
      }
      // filter dateOver
      if (query.dateOver) {
        params.set("dateOver", query.dateOver);
      }
      // request
      await request
        .get(`attendances`, { params })
        .then(({ data }) => {
          dispatch(assistanceActions.paginate(data));
          resolve(data);
        })
        .catch((err) => {
          setIsError(false);
          dispatch(assistanceActions.paginate(initialState.assistances));
          reject(err);
        });
      setPending(false);
    });
  };

  const clearQuery = () => {
    setQuery(defaultData);
  };

  const clearQueryToPage = () => {
    setQuery((prev) => ({
      ...prev,
      page: defaultData.page,
    }));
  };

  return {
    pending,
    query,
    isError,
    fetch,
    handleQuery,
    setQuery,
    clearQuery,
    clearQueryToPage,
  };
};
