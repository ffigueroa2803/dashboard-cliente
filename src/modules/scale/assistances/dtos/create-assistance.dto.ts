export interface ICreateAssistanceDto {
  scheduleId: number;
  checkInTime: string;
  observation?: string;
}
