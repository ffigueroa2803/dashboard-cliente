import { IClockEntity } from "@modules/clock/clocks/dtos/clock.entity";
import { IUserEntity } from "@modules/clock/users/dtos/user.entity";

export interface IZktecoObject {
  id: number;
  clockId: number;
  userId: number;
  date: string;
  checkInTime: string;
  clock: IClockEntity;
  user: IUserEntity;
}
