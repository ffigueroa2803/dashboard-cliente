import { Yup } from "@common/yup";
import { DateTime } from "luxon";

export interface AssistanceImportZktecoDto {
  dateStart: string;
  dateOver: string;
}

export const AssistanceImportZktecoYup = Yup.object({
  dateStar: Yup.string(),
  dateOver: Yup.string(),
});

const dateOver = DateTime.now();
const dateStart = dateOver.set({ day: 1 });

export const AssistanceImportZktecoData: AssistanceImportZktecoDto = {
  dateStart: dateStart.toFormat("yyyy-MM-dd"),
  dateOver: dateOver.toFormat("yyyy-MM-dd"),
};
