export interface IEditAssistanceDto {
  checkInTime: string;
  observation?: string;
}
