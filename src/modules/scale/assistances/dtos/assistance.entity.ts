import { IDiscountEntity } from "@modules/planilla/discounts/dtos/discount.entity";
import { IScheduleEntity } from "@modules/scale/schedules/dtos/schedule.entity";

export interface IAssistanceEntity {
  scheduleId: number;
  clientId: number;
  discountId?: number;
  metaId?: number;
  token: string;
  checkInTime: string;
  observation?: string;
  schedule?: IScheduleEntity;
  client?: any;
  discount?: IDiscountEntity;
}

export const assistanceEntityName = "AssistanceEntity";
