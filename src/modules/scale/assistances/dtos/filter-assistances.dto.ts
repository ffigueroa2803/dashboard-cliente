import { PaginateDto } from "@services/dtos";

export interface FilterGetAssistancesDto extends PaginateDto {
  year?: number;
  month?: number;
  dateStart: string;
  dateOver: string;
  condition?: any;
  typeCargoId?: any;
}
