export interface IAssistanceFormDto {
  token?: string;
  markerId: string;
  date: string;
  checkInTime: string;
}
