/* eslint-disable no-unused-vars */
import { ProgressIndicator } from "@atlaskit/progress-indicator";
import { Show } from "@common/show";
import { MarkerSearchByToken } from "@modules/zkteco/marker/components/marker-search-by-token";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { ArrowLeft, Calendar } from "react-feather";
import { useSelector } from "react-redux";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  Row,
} from "reactstrap";
import { AttendanceEntity } from "src/domain/zkteco/attendance.entity";
import { AssistanceCreate } from "./assistance-create";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave?: (assistance: AttendanceEntity) => void;
}

export const AssistanseStepCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const { markerSelected } = useSelector((state: RootState) => state.marker);

  const [currentStep, setCurrentStep] = useState(0);
  const [isCalendar, setIsCalendar] = useState<boolean>(false);

  const nextCreateAssistance = () => {
    setCurrentStep(1);
    setIsCalendar(true);
  };

  const handleSave = (assistance: AttendanceEntity) => {
    if (typeof onSave == "function") {
      onSave(assistance);
    }
  };

  useEffect(() => {
    if (!isOpen) setCurrentStep(0);
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Crear Asistencia</ModalHeader>
      <ModalBody>
        <ProgressIndicator selectedIndex={currentStep} values={[1, 2]} />
        {/* body */}
        {currentStep == 0 ? (
          <MarkerSearchByToken onSuccess={nextCreateAssistance} />
        ) : null}
        {/* mostrar info del contrato */}
        <Show condition={currentStep == 1}>
          <div className="mt-3">
            <FormGroup>
              <label>ID Marcación</label>
              <Input
                value={markerSelected?.token || ""}
                disabled
                className="capitalize"
              />
            </FormGroup>

            <FormGroup>
              <label>N° Credencial</label>
              <Input
                value={markerSelected?.credentialNumber || ""}
                disabled
                className="capitalize"
              />
            </FormGroup>

            <FormGroup>
              <label>Inicio</label>
              <Input
                type="date"
                value={markerSelected?.dateStart || ""}
                disabled
                className="capitalize"
              />
            </FormGroup>

            <FormGroup>
              <label>Fin</label>
              <Input
                type="date"
                value={markerSelected?.dateOver || ""}
                disabled
                className="capitalize"
              />
            </FormGroup>
          </div>
          {/* atrás */}
          <Row className="justify-content-around">
            <Col md="6">
              <Button
                color="dark"
                title="Atras"
                outline
                onClick={() => setCurrentStep(0)}
              >
                <ArrowLeft className="icon" />
              </Button>
            </Col>

            <Col md="6" className="text-right">
              <Button
                color="primary"
                title="Atras"
                onClick={() => setIsCalendar(true)}
              >
                <Calendar className="icon" />
              </Button>
            </Col>
          </Row>
        </Show>
      </ModalBody>
      {/* open schedule */}
      <AssistanceCreate
        isOpen={isCalendar}
        onSave={handleSave}
        onClose={() => setIsCalendar(false)}
      />
    </Modal>
  );
};
