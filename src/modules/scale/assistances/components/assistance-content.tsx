import { FloatButton } from "@common/button/float-button";
import { Show } from "@common/show";
import { useContext } from "react";
import { Plus } from "react-feather";
import {
  AssistanceContext,
  AssistanceProvider,
  switchOptions,
} from "./assistance-context";
import { AssistanceCreate } from "./assistance-create";
import { AssistanceDetalle } from "./assistance-detalle";
import { AssistanceTable } from "./assistance-table";

export const AssistanceWrapper = () => {
  const { pending, setOptions, options, onRefresh } =
    useContext(AssistanceContext);

  return (
    <>
      <div className="card card-body">
        <AssistanceTable />
      </div>
      {/* btn flotante */}
      <Show condition={!pending}>
        <FloatButton
          icon={<Plus />}
          color="success"
          onClick={() => setOptions(switchOptions.CREATE)}
        />
      </Show>
      {/* modal create */}
      <AssistanceCreate
        isOpen={switchOptions.CREATE == options}
        onClose={() => setOptions("")}
        onSave={onRefresh}
      />
      {/* info */}
      <AssistanceDetalle
        isOpen={options == switchOptions.INFO}
        onClose={() => setOptions("")}
      />
    </>
  );
};

export const AssistanceContent = () => {
  return (
    <AssistanceProvider>
      <AssistanceWrapper />
    </AssistanceProvider>
  );
};
