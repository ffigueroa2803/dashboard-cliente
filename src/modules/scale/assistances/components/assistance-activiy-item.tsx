import { ListGroupItem } from "reactstrap";

interface IActivityItemProps {
  type: string;
  title: string;
  description: string;
}

export const AssistanceActivityItem = ({
  type,
  title,
  description,
}: IActivityItemProps) => {
  return (
    <ListGroupItem>
      <small className="text-muted">{type}</small>
      <br />
      <h6>{title}</h6>
      <div className="text-muted">{description}</div>
    </ListGroupItem>
  );
};
