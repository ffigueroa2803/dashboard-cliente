/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { DateTime } from "luxon";
import { useState } from "react";
import { Save } from "react-feather";
import { toast } from "react-toastify";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { AttendanceEntity } from "src/domain/zkteco/attendance.entity";
import { IAssistanceFormDto } from "../dtos/assistance-form.dto";
import { useCreateAttendanceMutation } from "../features/attendance.rtk";
import { AssistanceForm } from "./assistance-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: (assistance: AttendanceEntity) => void;
}

export const AssistanceCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const [fetch] = useCreateAttendanceMutation();

  const dataDefault: IAssistanceFormDto = {
    markerId: "",
    date: DateTime.now().toFormat("yyyy-MM-dd"),
    checkInTime: DateTime.now().toFormat("HH:mm:ss"),
  };

  const [form, setForm] = useState<IAssistanceFormDto>(dataDefault);
  const [loading, setLoading] = useState<boolean>(false);

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev: any) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleSave = async () => {
    setLoading(true);
    toast.dismiss();
    await fetch(form)
      .unwrap()
      .then((data) => {
        toast.success(`Asistencia registrada!`);
        onSave(data);
      })
      .catch(() => toast.error(`No se puede registrar`));
    setLoading(false);
  };

  return (
    <Modal isOpen={isOpen}>
      <ModalHeader toggle={onClose}>Nueva Asistencia</ModalHeader>
      <ModalBody>
        <AssistanceForm
          form={form}
          onChange={handleForm}
          isDisabled={loading}
        />
      </ModalBody>
      <ModalFooter className="text-right">
        <Button color="primary" disabled={loading} onClick={handleSave}>
          <Save className="icon" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};
