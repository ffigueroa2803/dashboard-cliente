/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { IBallotEntity } from "@modules/scale/ballots/dtos/ballot.entity";
import { IDiscountEntity } from "@modules/scale/discounts/dtos/discount.entity";
import { DiscountSerialize } from "@modules/scale/discounts/serializers/discount.serialize";
import { ILicenseEntity } from "@modules/scale/licenses/dtos/license.entity";
import { LicenseSerialize } from "@modules/scale/licenses/serializers/license.serialize";
import { IVacationEntity } from "@modules/scale/vacations/dtos/vacation.entity.dto";
import { RootState } from "@store/store";
import { plainToClass } from "class-transformer";
import { DateTime } from "luxon";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListGroup } from "reactstrap";
import { ActivityEntity } from "src/domain/scale/activities/activity.entity";
import { IActivityInterface } from "src/domain/scale/activities/activity.interface";
import { assistanceActions, initialState } from "../store";
import { AssistanceActivityItem } from "./assistance-activiy-item";
import { useAssistanceActivityList } from "../hooks/assistance-activity-list";

export const AssistanceActivity = () => {
  const dispatch = useDispatch();

  const { assistance, activities } = useSelector(
    (state: RootState) => state.assistance
  );
  const assistanceActivityList = useAssistanceActivityList();

  useEffect(() => {
    if (assistance.id)
      assistanceActivityList
        .fetch()
        .then((res: any) => {
          const dataActivities: IActivityInterface[] = [];
          // activities
          res.assistances?.items?.forEach((obj: any) => {
            dataActivities.push(
              new ActivityEntity().load({
                id: obj.id,
                type: "Zkteco",
                title: obj?.marker?.clock?.name || "Reloj",
                description: `Marcaje: ${obj.checkInTime}`,
              })
            );
          });
          // ballots
          res.ballots?.items?.forEach((obj: IBallotEntity) => {
            dataActivities.push(
              new ActivityEntity().load({
                id: obj.id,
                type: "Papeleta",
                title: obj.typeBallot?.name,
                description: `Hora salida: ${obj.departureTime}`,
              })
            );
          });
          // discounts
          res.discounts?.items?.forEach((obj: IDiscountEntity) => {
            const objClass = plainToClass(DiscountSerialize, obj);
            dataActivities.push(
              new ActivityEntity().load({
                id: obj.id,
                type: "Descuento",
                title: objClass.displaydiscountableType,
                description: `Total a descontar: ${obj.total} min`,
              })
            );
          });
          // licenses
          res.licenses?.items?.forEach((obj: ILicenseEntity) => {
            const objClass = plainToClass(LicenseSerialize, obj);
            dataActivities.push(
              new ActivityEntity().load({
                id: obj.id,
                type: "Licencia",
                title: obj.typeLicense?.name || "",
                description: `"${objClass.displayDateOfAdmission}" a "${objClass.displayTerminationDate}"`,
              })
            );
          });
          // vacations
          res.vacations?.items?.forEach((obj: IVacationEntity) => {
            const start = DateTime.fromSQL(obj.startDate);
            const over = DateTime.fromSQL(obj.terminationDate);
            dataActivities.push(
              new ActivityEntity().load({
                id: obj.id,
                type: "Vacaciones",
                title: obj.documentNumber,
                description: `"${over.toFormat(
                  "dd/MM/yyyy"
                )}" a "${start.toFormat("dd/MM/yyyy")}"`,
              })
            );
          });
          // set data
          dispatch(assistanceActions.paginateActivities(dataActivities));
        })
        .catch(() =>
          dispatch(
            assistanceActions.paginateActivities(initialState.activities)
          )
        );
  }, [assistance]);

  return (
    <Show
      condition={!assistanceActivityList.pending}
      isDefault={
        <div className="text-center">
          <LoadingSimple loading />
        </div>
      }>
      <Show condition={!activities.length}>
        <div className="text-center">
          <b>No hay actividades disponibles</b>
        </div>
      </Show>
      {/* list info */}
      <ListGroup variant="flush">
        {/* iterar licenses */}
        {activities?.map((activity) => (
          <AssistanceActivityItem
            key={`item-activities-${activity.getId()}`}
            type={activity.getType()}
            title={activity.getTitle()}
            description={activity.getDescription()}
          />
        ))}
      </ListGroup>
    </Show>
  );
};
