/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { Show } from "@common/show";
import { MarkerClockSelect } from "@modules/zkteco/marker/components/marker-clock-select";
import { useState } from "react";
import { Check } from "react-feather";
import { Button, Form, FormGroup, Input } from "reactstrap";
import { IAssistanceFormDto } from "../dtos/assistance-form.dto";

interface IProps {
  form: IAssistanceFormDto;
  onChange: (input: IInputHandle) => void;
  isDisabled?: boolean;
  isEdit?: boolean;
}

export const AssistanceForm = ({ form, onChange, isDisabled }: IProps) => {
  const [token, setToken] = useState<string>("");
  const [isToken, setIsToken] = useState<boolean>(false);

  const handleCheck = () => {
    onChange({ name: "token", value: token });
    setIsToken(true);
  };

  return (
    <Form>
      <FormGroup>
        <label>
          Token <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="token"
          value={token}
          onChange={({ target }) => setToken(target.value)}
          disabled={isDisabled || isToken}
        />
        <Show condition={!isToken}>
          <Button block className="mt-2" color="success" onClick={handleCheck}>
            <Check className="icon" />
          </Button>
        </Show>
      </FormGroup>

      <Show condition={isToken}>
        <FormGroup>
          <label>Reloj</label>
          <MarkerClockSelect
            token={token}
            name="markerId"
            value={form.markerId}
            onChange={onChange}
          />
        </FormGroup>

        <FormGroup>
          <label>
            Fecha de regístro <b className="text-danger">*</b>
          </label>
          <Input
            type="date"
            name="date"
            value={form?.date}
            onChange={({ target }) => onChange(target)}
            disabled={isDisabled}
          />
        </FormGroup>

        <FormGroup>
          <label>
            Hora de regístro <b className="text-danger">*</b>
          </label>
          <Input
            type="time"
            name="checkInTime"
            value={form?.checkInTime}
            onChange={({ target }) => onChange(target)}
            disabled={isDisabled}
          />
        </FormGroup>
      </Show>
    </Form>
  );
};
