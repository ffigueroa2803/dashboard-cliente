import { ClockSelect } from "@modules/zkteco/clocks/components/clock-select";
import { Save } from "react-feather";
import { Button, Form, FormGroup, Input } from "reactstrap";
import { useImportZktecoMutation } from "../features/clock.rtk";
import { useFormik } from "formik";
import {
  AssistanceImportZktecoData,
  AssistanceImportZktecoYup,
} from "../dtos/assistance-import-zkteco.dto";
import { useState } from "react";
import { Show } from "@common/show";
import { IClockEntity } from "@modules/clock/clocks/dtos/clock.entity";
import { toast } from "react-toastify";
import { ButtonLoading } from "@common/button/button-loading";

export function AssistanceImportZkteco() {
  const [fetch, { isLoading }] = useImportZktecoMutation();
  const [clock, setClock] = useState<IClockEntity | undefined>(undefined);

  const { values, handleBlur, handleChange, handleSubmit } = useFormik({
    initialValues: AssistanceImportZktecoData,
    validationSchema: AssistanceImportZktecoYup,
    onSubmit: (values) =>
      fetch({ id: clock?.id || "", body: values })
        .unwrap()
        .then(() =>
          toast.success(
            `La importación puede demorar unos minutos, vuelva más tarde`
          )
        )
        .catch((err) => {
          if (err.status == 400) {
            return toast.warn(`Datos incorrectos`);
          } else {
            return toast.error(err?.data?.message || "Algo salió mal");
          }
        }),
  });

  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <ClockSelect
          name="clockId"
          value={clock?.id}
          onChange={(data) => setClock(data?.obj || undefined)}
        />
      </FormGroup>
      <Show condition={typeof clock != "undefined"}>
        <FormGroup>
          <label>
            Fecha Inicio <b className="text-danger">*</b>
          </label>
          <Input
            name="dateStart"
            type="date"
            value={values.dateStart || ""}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </FormGroup>
        <FormGroup>
          <label>
            Fecha Fin <b className="text-danger">*</b>
          </label>
          <Input
            name="dateOver"
            type="date"
            value={values.dateOver || ""}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </FormGroup>
        <FormGroup className="text-right">
          <Show
            condition={!isLoading}
            isDefault={
              <ButtonLoading loading color="primary" title="Importando..." />
            }>
            <Button color="primary">
              <Save className="icon" />
              Importar
            </Button>
          </Show>
        </FormGroup>
      </Show>
    </Form>
  );
}
