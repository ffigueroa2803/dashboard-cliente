/* eslint-disable no-unused-vars */
import { ContractConditionSelect } from "@modules/scale/contracts/components/contract-condition-select";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { useContext, useMemo } from "react";
import DataTable, { Direction } from "react-data-table-component";
import { Search, X } from "react-feather";
import { useSelector } from "react-redux";
import { Button, Col, Form, FormGroup, Input, Row } from "reactstrap";
import { AssistancesListResponse } from "../interfaces/assistances-list-response";
import { AssistanceContext } from "./assistance-context";
import { AssistanceReport } from "./assistance-report";

export enum AssistanceActionEnum {
  EDIT = "EDIT",
  INFO = "INFO",
}

export const AssistanceTable = () => {
  const { assistances } = useSelector((state: RootState) => state.assistance);

  const { query, handleQuery, pending, fetch } = useContext(AssistanceContext);

  const isDisabled = () => {
    if (pending) return true;
    // dateStart
    if (!query.dateStart) return true;
    // dateOver
    if (!query.dateOver) return true;
    return false;
  };

  const columns = useMemo(() => {
    return [
      {
        name: "Apellidos",
        wrap: true,
        cell: (row: AssistancesListResponse) => (
          <span className="uppercase">
            <span>{row?.personLastname || ""}</span>
            <span className="ml-1">{row?.personSecondaryName || ""}</span>
          </span>
        ),
      },
      {
        name: "Nombres",
        wrap: true,
        cell: (row: AssistancesListResponse) => (
          <span className="uppercase">{row?.personName || ""}</span>
        ),
      },
      {
        name: "Tip. Cat",
        cell: (row: AssistancesListResponse) => row?.typeCategoryName || "",
      },
      {
        name: "Día",
        cell: (row: AssistancesListResponse) =>
          DateTime.fromISO(row.entryDate).setLocale("ES").toFormat("EEEE"),
      },
      {
        name: "Ingreso",
        wrap: true,
        cell: (row: AssistancesListResponse) => {
          return (
            <span>
              <span className="badge badge-dark">
                {DateTime.fromISO(row.entryDate)
                  .setLocale("ES")
                  .toFormat("dd/MM/yyyy")}
              </span>{" "}
              <span className="badge badge-primary mt-1">
                {DateTime.fromSQL(row.entryCheckInTime)
                  .setLocale("ES")
                  .toFormat("HH:mm")}
              </span>
            </span>
          );
        },
      },
      {
        name: "Salida",
        wrap: true,
        cell: (row: AssistancesListResponse) => {
          return (
            <span>
              <span className="badge badge-dark">
                {DateTime.fromISO(row.exitDate)
                  .setLocale("ES")
                  .toFormat("dd/MM/yyyy")}
              </span>{" "}
              <span className="badge badge-danger mt-1">
                {DateTime.fromSQL(row.exitCheckInTime)
                  .setLocale("ES")
                  .toFormat("HH:mm")}
              </span>
            </span>
          );
        },
      },
      {
        name: "Marcaje",
        center: true,
        wrap: true,
        cell: (row: AssistancesListResponse) => {
          if (!row.attendances?.length) {
            return <X className="icon text-danger" color="#dc3545" />;
          }

          return (
            <div>
              {row.attendances?.map((attendance) => (
                <div key={`list-${attendance.id}`} className="mb-2 mt-2">
                  {attendance.checkInTime}
                </div>
              ))}
            </div>
          );
        },
      },
      {
        name: "Observaciones",
        center: true,
        selector: (row: AssistancesListResponse) => {
          if (!row.activities?.length) return null;
          return (
            <table className="table table-responsive">
              <thead>
                <tr>
                  <td>#ID</td>
                  <td>Titulo</td>
                  <td>Descripción</td>
                  <td>Tipo</td>
                </tr>
              </thead>
              <tbody>
                {row.activities?.map((activity) => (
                  <tr key={`list-activity-${activity.activityId}`}>
                    <td>{activity.activityId}</td>
                    <td>{activity.title}</td>
                    <td>{activity.description}</td>
                    <td>{activity.tag}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          );
        },
      },
    ];
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [assistances]);

  const handleSearchBySubmit = (e: any) => {
    e.preventDefault();
    fetch();
  };

  const handleOnPage = (page: number) => {
    handleQuery({ name: "page", value: page });
    fetch();
  };

  const handleOnLimit = (limit: number) => {
    handleQuery({ name: "limit", value: limit });
    fetch();
  };

  return (
    <>
      <Form onSubmit={handleSearchBySubmit}>
        <FormGroup className="mb-3">
          <Row>
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                name="querySearch"
                value={query?.querySearch}
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
                placeholder="Buscar Apellidos y Nombres..."
              />
            </Col>
            {/* filtro cargo */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <TypeCargoSelect
                value={query?.typeCargoId || ""}
                name="typeCargoId"
                onChange={(target) => handleQuery(target)}
              />
            </Col>
            {/* filtro condition */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <ContractConditionSelect
                value={query?.condition}
                name="condition"
                onChange={(target) => handleQuery(target)}
              />
            </Col>
            {/* filter start date */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                value={query?.dateStart || ""}
                type="date"
                name="dateStart"
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
              />
            </Col>
            {/* filter over date */}
            <Col md="4 col-12 col-lg-4" className="mb-2">
              <Input
                value={query?.dateOver || ""}
                type="date"
                name="dateOver"
                onChange={({ target }) => handleQuery(target)}
                disabled={pending}
              />
            </Col>
            <Col md="2 col-12 col-lg-2" className="mb-2">
              <Button color="primary" block disabled={isDisabled()}>
                <Search className="icon" />
              </Button>
            </Col>
            <Col md="2 col-12 col-lg-2" className="mb-2">
              <AssistanceReport />
            </Col>
          </Row>
        </FormGroup>
      </Form>
      <DataTable
        striped
        direction={Direction.AUTO}
        responsive
        columns={columns as any}
        progressPending={pending || false}
        data={assistances?.items || []}
        pagination
        paginationPerPage={query.limit || 30}
        paginationServer
        paginationTotalRows={assistances.meta?.totalItems || 30}
        onChangePage={handleOnPage}
        onChangeRowsPerPage={handleOnLimit}
      />
    </>
  );
};
