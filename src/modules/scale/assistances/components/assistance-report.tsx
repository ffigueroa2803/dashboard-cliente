import { useContext, useState } from "react";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";
import { File } from "react-feather";
import urljoin from "url-join";
import { scaleRequestV2 } from "@services/scale.request";
import { AssistanceContext } from "./assistance-context";

interface IActionItem {
  label: string;
  link: string;
  icon: any;
}

const request = scaleRequestV2();

export const AssistanceReport = () => {
  const { query } = useContext(AssistanceContext);

  const [isToggle, setIsToggle] = useState<boolean>(false);

  const onToggle = () => setIsToggle((prev) => !prev);

  const actions: IActionItem[] = [
    {
      icon: <File className="icon" />,
      label: "Generar Pdf",
      link: urljoin(request.urlBase, `/attendances/reports/summary.html`),
    },
    {
      icon: <File className="icon" />,
      label: "Generar Excel",
      link: urljoin(request.urlBase, `/attendances/reports/summary.xlsx`),
    },
  ];

  const handleLink = (link: string) => {
    const queryString = new URLSearchParams();
    const arrays = ["typeCargoId", "condition"];
    Object.keys(query).forEach((attr: any) => {
      const newForm: any = Object.assign({}, query);
      const value = newForm[attr];
      // filter boolean
      if (arrays.includes(attr) && value) {
        queryString.set(`contract${attr}`, `${value}`);
      } else if (typeof value == "boolean") {
        queryString.set(attr, `${value}`);
      } else if (value) {
        queryString.set(attr, `${value}`);
      }
    });
    // crear elemento a
    const isQuery = /[?]/.test(link);
    const url = `${link}${isQuery ? "&" : "?"}${queryString.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  return (
    <Dropdown isOpen={isToggle} toggle={onToggle}>
      <DropdownToggle color="danger" outline caret className="btn-block">
        Opciones
      </DropdownToggle>
      <DropdownMenu>
        {actions.map((action, index) => (
          <DropdownItem
            key={`option-item-${index}`}
            onClick={() => handleLink(action.link)}>
            {action.icon}
            <span className="ml-2">{action.label}</span>
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  );
};
