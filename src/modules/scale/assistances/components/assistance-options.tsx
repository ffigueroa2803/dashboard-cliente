/* eslint-disable react-hooks/exhaustive-deps */
import { NavbarContext } from "@common/navbar/navbar-context";
import React, { FC, useContext, useEffect, useState } from "react";
import { AssistnceImportTxt } from "./assistance-import-txt";
import { AssistanceImportZkteco } from "./assistance-import-zkteco";
import { Button } from "reactstrap";
import { Show } from "@common/show";

export const ListOptions = () => {
  const { setTitle } = useContext(NavbarContext);

  const [option, setOption] = useState<string | undefined>(undefined);

  useEffect(() => {
    setTitle("Importar Asistencia");
  }, []);

  return (
    <div className="link-section h-100">
      <Show
        condition={option == undefined}
        isDefault={
          <div className="mb-3">
            <Button
              block
              outline
              color="dark"
              onClick={() => setOption(undefined)}>
              Volver
            </Button>
          </div>
        }>
        <Button block outline color="info" onClick={() => setOption("zkteco")}>
          Importación ZKTECO
        </Button>
        <Button block outline color="info" onClick={() => setOption("txt")}>
          Importación TXT
        </Button>
      </Show>
      {/* import zkteco */}
      <Show condition={option == "zkteco"}>
        <AssistanceImportZkteco />
      </Show>
      {/* import txt */}
      <Show condition={option == "txt"}>
        <AssistnceImportTxt />
      </Show>
    </div>
  );
};

export const AssistanceOptions: FC = ({ children }) => {
  const navbarContext = useContext(NavbarContext);

  useEffect(() => {
    navbarContext.setShow(true);
    return () => {
      navbarContext.setShow(false);
      navbarContext.setContent(null);
    };
  }, []);

  useEffect(() => {
    if (navbarContext.isOpen) {
      navbarContext.setContent(<ListOptions />);
    }
  }, [navbarContext.isOpen]);

  useEffect(() => {
    if (!navbarContext.isOpen) {
      navbarContext.setContent(null);
    }
  }, [navbarContext.isOpen]);

  return <>{children}</>;
};
