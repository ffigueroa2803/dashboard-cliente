/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { RootState } from "@store/store";
import { FormGroup, Modal, ModalBody, ModalHeader } from "reactstrap";
import { useSelector } from "react-redux";
import { Activity, Clock } from "react-feather";
import { AssistanceActivity } from "./assistance-activity";
import { DateTime } from "luxon";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
}

export const AssistanceDetalle = ({ isOpen, onClose }: IProps) => {
  const { assistance } = useSelector((state: RootState) => state.assistance);

  return (
    <>
      <Modal isOpen={isOpen}>
        <ModalHeader toggle={onClose}>Detalle de asistencia</ModalHeader>
        <ModalBody>
          <FormGroup>
            <label htmlFor="">
              <Clock className="icon" /> Ingreso
            </label>
            <h6 className="capitalize">
              <span className="mr-4">
                {DateTime.fromISO(assistance.entryDate)
                  .setLocale("ES")
                  .toFormat("dd/MM/yyyy")}
              </span>
              {DateTime.fromSQL(assistance.entryCheckInTime)
                .setLocale("ES")
                .toFormat("HH:mm")}
            </h6>
          </FormGroup>

          <FormGroup>
            <label htmlFor="">
              <Clock className="icon" /> Salida
            </label>
            <h6 className="capitalize">
              <span className="mr-4">
                {DateTime.fromISO(assistance.exitDate)
                  .setLocale("ES")
                  .toFormat("dd/MM/yyyy")}
              </span>
              {DateTime.fromSQL(assistance.exitCheckInTime)
                .setLocale("ES")
                .toFormat("HH:mm")}
            </h6>
          </FormGroup>
        </ModalBody>
        <ModalHeader>
          <Activity className="icon" /> Actividades
        </ModalHeader>
        <ModalBody>
          <AssistanceActivity />
        </ModalBody>
      </Modal>
    </>
  );
};
