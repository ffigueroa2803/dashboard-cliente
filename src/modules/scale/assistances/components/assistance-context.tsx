/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { DateTime } from "luxon";
import { useRouter } from "next/router";
import { createContext, FC, useEffect, useState } from "react";
import { FilterGetAssistancesDto } from "../dtos/filter-assistances.dto";
import { useAssistanceList } from "../hooks/assistance-list";

const currentDate = DateTime.now();

const currentQuery: FilterGetAssistancesDto = {
  page: 1,
  limit: 30,
  querySearch: "",
  dateStart: currentDate.toFormat("yyyy-MM-dd"),
  dateOver: currentDate.toFormat("yyyy-MM-dd"),
};

export const AssistanceContext = createContext({
  query: currentQuery,
  pending: false,
  options: "",
  setQuery: (filter: FilterGetAssistancesDto) => {},
  handleQuery: (option: IInputHandle) => {},
  onRefresh: () => {},
  setOptions: (value: string) => {},
  fetch: () => {},
});

export const switchOptions = {
  DEFAULT: "",
  CREATE: "CREATE",
  EDIT: "EDIT",
  INFO: "INFO",
};

export const AssistanceProvider: FC = ({ children }) => {
  const assistanceList = useAssistanceList({
    page: currentQuery.page,
    limit: currentQuery.limit,
    dateStart: currentQuery.dateStart,
    dateOver: currentQuery.dateOver,
  });

  const router = useRouter();

  const [isRefresh, setIsRefresh] = useState<boolean>(false);
  const [options, setOptions] = useState<string>(switchOptions.DEFAULT);

  const handleOnRefresh = () => {
    assistanceList.clearQueryToPage();
    setIsRefresh(true);
  };

  useEffect(() => {
    if (router) setIsRefresh(true);
  }, [router]);

  useEffect(() => {
    if (isRefresh) assistanceList.fetch().catch(() => null);
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <AssistanceContext.Provider
      value={{
        query: assistanceList.query,
        pending: assistanceList.pending,
        options,
        handleQuery: assistanceList.handleQuery,
        setQuery: assistanceList.setQuery,
        onRefresh: handleOnRefresh,
        setOptions,
        fetch: () => setIsRefresh(true),
      }}>
      {children}
    </AssistanceContext.Provider>
  );
};
