import { scaleRequest } from "@services/scale.request";
import { ICreateAssistanceDto } from "./dtos/create-assistance.dto";
import { IEditAssistanceDto } from "./dtos/edit-assistance.dto";
import { FilterGetAssistancesDto } from "./dtos/filter-assistances.dto";
import { IAssistanceEntity } from "./dtos/assistance.entity";
import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";

const request = scaleRequest();

export const getAssistances = async ({
  ids,
  page,
  querySearch,
  limit,
  year,
  month,
  dateStart,
  dateOver,
}: FilterGetAssistancesDto): Promise<
  ResponsePaginateDto<IAssistanceEntity>
> => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  if (month) params.set("month", `${month}`);
  if (dateStart) params.set("dateStart", dateStart);
  if (dateOver) params.set("dateOver", dateOver);
  // add ids
  ids?.forEach((id) => params.set("ids[]", `${id}`));
  // response
  return await request.get(`assistances`, { params }).then((res) => res.data);
};

export const getAssistancesToContract = async (
  id: number,
  { page, querySearch, limit, year, month }: FilterGetAssistancesDto
) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  if (year) params.set("year", `${year}`);
  if (month) params.set("month", `${month}`);
  // response
  return await request
    .get(`contracts/${id}/assistances`, { params })
    .then((res) => res.data);
};

export const createAssistance = async (
  payload: ICreateAssistanceDto
): Promise<IAssistanceEntity> => {
  return await request.post(`assistances`, payload).then((res) => res.data);
};

export const findAssistance = async (
  id: number
): Promise<IAssistanceEntity> => {
  return await request.get(`assistances/${id}`).then((res) => res.data);
};

export const editAssistance = async (
  token: string,
  payload: IEditAssistanceDto
): Promise<IAssistanceEntity> => {
  return await request
    .put(`assistances/${token}`, payload)
    .then((res) => res.data);
};

export const deleteAssistance = async (
  id: number
): Promise<{ deleted: boolean }> => {
  return await request.destroy(`assistances/${id}`).then((res) => res.data);
};
