import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { AttendanceEntity } from "src/domain/zkteco/attendance.entity";

const baseUrl = process.env.NEXT_PUBLIC_ZKTECO_URL || "";

export const attendanceRtk = createApi({
  reducerPath: "attendanceRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    createAttendance: builder.mutation<AttendanceEntity, any>({
      query: (body) => ({
        url: `attendances/`,
        method: "POST",
        headers: BaseHeaders,
        body,
      }),
    }),
  }),
});

export const { useCreateAttendanceMutation } = attendanceRtk;
