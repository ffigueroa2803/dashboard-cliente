import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { AssistanceImportZktecoDto } from "../dtos/assistance-import-zkteco.dto";

const baseUrl = process.env.NEXT_PUBLIC_ZKTECO_URL || "";

export const clockRtk = createApi({
  reducerPath: "clockRtk",
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    importZkteco: builder.mutation<
      any,
      { id: string; body: AssistanceImportZktecoDto }
    >({
      query: ({ id, body }) => ({
        url: `clocks/${id}/importZkteco`,
        method: "POST",
        headers: BaseHeaders,
        body,
      }),
    }),
  }),
});

export const { useImportZktecoMutation } = clockRtk;
