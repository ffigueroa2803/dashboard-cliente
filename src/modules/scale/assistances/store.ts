import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IActivityInterface } from "src/domain/scale/activities/activity.interface";
import { IZktecoObject } from "./dtos/assistance-meta-type.object";
import { AssistancesListResponse } from "./interfaces/assistances-list-response";

export interface AssistanceState {
  assistances: ResponsePaginateDto<AssistancesListResponse>;
  assistance: AssistancesListResponse;
  activities: IActivityInterface[];
  metaType: {
    type: string;
    data: IZktecoObject;
  };
  option: string;
}

export const initialState: AssistanceState = {
  assistances: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  activities: [],
  option: "",
  assistance: {} as any,
  metaType: {} as any,
};

const assistanceStore = createSlice({
  name: "escalafon@assistance",
  initialState,
  reducers: {
    paginate: (
      state: AssistanceState,
      { payload }: PayloadAction<ResponsePaginateDto<AssistancesListResponse>>
    ) => {
      state.assistances = payload;
      return state;
    },
    updateAssistanceItem: (
      state: AssistanceState,
      { payload }: PayloadAction<AssistancesListResponse>
    ) => {
      state.assistances?.items?.map((item) => {
        if (payload.id != item.id) return item;
        return Object.assign(item, payload);
      });
      return state;
    },
    setAssistance: (
      state: AssistanceState,
      { payload }: PayloadAction<AssistancesListResponse>
    ) => {
      state.assistance = payload;
      return state;
    },
    setMetaType: (state: AssistanceState, { payload }: PayloadAction<any>) => {
      state.metaType = payload;
      return state;
    },
    findAssistance: (
      state: AssistanceState,
      { payload }: PayloadAction<number>
    ) => {
      state.assistance =
        state.assistances?.items?.find((item) => {
          return item.id == payload;
        }) || ({} as any);
      return state;
    },
    changeOption: (
      state: AssistanceState,
      { payload }: PayloadAction<string>
    ) => {
      state.option = payload;
      return state;
    },
    paginateActivities: (
      state: AssistanceState,
      { payload }: PayloadAction<IActivityInterface[]>
    ) => {
      state.activities = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.assistance };
    },
  },
});

export const assistanceReducer = assistanceStore.reducer;

export const assistanceActions = assistanceStore.actions;
