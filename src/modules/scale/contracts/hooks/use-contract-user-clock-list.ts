/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { contractActions } from "../store";
import { PaginateDto } from "@services/dtos";
import { RootState } from "@store/store";
import { zktecoRequest } from "@services/zkteco.request";

const request = zktecoRequest();

export const userContractUserClockList = (defaultPaginate?: PaginateDto) => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(defaultPaginate?.page || 1);
  const [limit, setLimit] = useState<number>(defaultPaginate?.limit || 100);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultPaginate?.querySearch || ""
  );

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    await request
      .get(`markers/${contract?.code}/clocks`, { params })
      .then((res) => dispatch(contractActions.zktecoPaginate(res.data)))
      .catch(() => dispatch(contractActions.zktecoPaginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    querySearch,
    setPage,
    setLimit,
    setQuerySearch,
    fetch,
  };
};
