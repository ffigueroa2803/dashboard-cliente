/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { contractActions } from "../store";
import { RootState } from "@store/store";
import { zktecoRequest } from "@services/zkteco.request";

const request = zktecoRequest();

export const useContractUserMarker = () => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async () => {
    setPending(true);
    await request
      .get(`markers/${contract?.code}`)
      .then((res) => dispatch(contractActions.setMarker(res.data)))
      .catch(() => dispatch(contractActions.setMarker({} as any)));
    setPending(false);
  };

  return {
    pending,
    fetch,
  };
};
