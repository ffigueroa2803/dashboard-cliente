import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { scheduleActions } from "@modules/scale/schedules/store";
import { FilterGetSchedulesDto } from "@modules/scale/schedules/dtos/filter-schedule.dto";

const request = scaleRequest();

export const useContractScheduleList = (
  defaultPaginate?: FilterGetSchedulesDto
) => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);
  const [page, setPage] = useState<number>(defaultPaginate?.page || 1);
  const [limit, setLimit] = useState<number>(defaultPaginate?.limit || 100);
  const [year, setYear] = useState<number | undefined>(defaultPaginate?.year);
  const [month, setMonth] = useState<number | undefined>(
    defaultPaginate?.month
  );
  const [querySearch, setQuerySearch] = useState<string>(
    defaultPaginate?.querySearch || ""
  );

  const dataDefault = {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: limit || 30,
    },
  };

  const fetch = async () => {
    setPending(true);
    const params = new URLSearchParams();
    params.set("page", `${page}`);
    params.set("querySearch", querySearch || "");
    params.set("limit", `${limit || 30}`);
    if (year) params.set("year", `${year}`);
    if (month) params.set("month", `${month}`);
    await request
      .get(`contracts/${contract?.id}/schedules`, { params })
      .then((res) => dispatch(scheduleActions.paginate(res.data)))
      .catch(() => dispatch(scheduleActions.paginate(dataDefault)));
    setPending(false);
  };

  return {
    pending,
    querySearch,
    year,
    month,
    setPage,
    setLimit,
    setQuerySearch,
    setYear,
    setMonth,
    fetch,
  };
};
