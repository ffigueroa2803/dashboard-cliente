/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";

const request = scaleRequest();

export const useContractDelete = () => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async () => {
    return await confirm
      .alert({
        title: "Eliminar",
        message: `¿Estás seguro en eliminar el contracto?`,
        labelError: "Cancelar",
        labelSuccess: "Eliminar",
      })
      .then(async () => {
        setPending(true);
        setIsSuccess(false);
        await request
          .destroy(`contracts/${contract?.id}`)
          .then(() => {
            setIsSuccess(true);
            toast.success(`El contrato se eliminó correctamente!!!`);
          })
          .catch(() => setIsError(false));
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    isSuccess,
    isError,
    destroy,
  };
};
