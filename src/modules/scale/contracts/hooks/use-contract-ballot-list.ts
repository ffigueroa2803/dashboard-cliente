/* eslint-disable no-async-promise-executor */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { FilterGetBallotsDto } from "@modules/scale/ballots/dtos/filter-ballots.dto";
import { ballotActions, initialState } from "@modules/scale/ballots/store";
import { IInputHandle } from "@common/dtos/input-handle";

const request = scaleRequest();

export const defaultQueryParamsBallot: FilterGetBallotsDto = {
  page: 1,
  limit: 30,
};

export const useContractBallotList = (
  defaultPaginate?: FilterGetBallotsDto
) => {
  const dispatch = useDispatch();

  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);
  const [queryParams, setQueryParams] = useState<FilterGetBallotsDto>(
    defaultPaginate || defaultQueryParamsBallot
  );

  const changeQueryParams = ({ name, value }: IInputHandle) => {
    setQueryParams((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const fetch = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const params = new URLSearchParams();
      params.set("page", `${queryParams.page}`);
      params.set("querySearch", queryParams.querySearch || "");
      params.set("limit", `${queryParams.limit}`);
      if (queryParams.year) params.set("year", `${queryParams.year}`);
      if (queryParams.month) params.set("month", `${queryParams.month}`);
      await request
        .get(`contracts/${contract?.id}/ballots`, { params })
        .then((res) => {
          dispatch(ballotActions.paginate(res.data));
          resolve(res);
        })
        .catch((err) => {
          dispatch(ballotActions.paginate(initialState.ballots));
          reject(err);
        });
      setPending(false);
    });
  };

  return {
    pending,
    queryParams,
    setQueryParams,
    changeQueryParams,
    fetch,
  };
};
