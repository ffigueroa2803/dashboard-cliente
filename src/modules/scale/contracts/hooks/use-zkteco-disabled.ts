/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";
import { zktecoRequest } from "@services/zkteco.request";

const request = zktecoRequest();

export const useZktecoDisabled = () => {
  const [pending, setPending] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async (id: string, clockId: string) => {
    return await confirm
      .alert({
        title: "Deshabilitar",
        message: `¿Estás seguro en deshabilitar el reloj?`,
        labelError: "Cancelar",
        labelSuccess: "Deshabilitar",
      })
      .then(async () => {
        setPending(true);
        setIsSuccess(false);
        await request
          .put(`markers/${id}/disabled/${clockId}`)
          .then(() => {
            setIsSuccess(true);
            toast.success(`El reloj se deshabilitó correctamente!!!`);
          })
          .catch(() => {
            toast.error(`El reloj se pudo deshabilitar!!!`);
            setIsError(false);
          });
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    isSuccess,
    isError,
    destroy,
  };
};
