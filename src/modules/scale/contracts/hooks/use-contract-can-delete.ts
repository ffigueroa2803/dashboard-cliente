/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";

const request = scaleRequest();

export const useContractCanDelete = () => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const [pending, setPending] = useState<boolean>(false);
  const [result, setResult] = useState<boolean>(true);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const fetch = async () => {
    setPending(true);
    abortController = new AbortController();
    await request
      .get(`contracts/${contract?.id}/canDelete`, {
        signal: abortController.signal,
      })
      .then((res) => setResult(res.data))
      .catch(() => setResult(false));
    setPending(false);
  };

  return {
    pending,
    result,
    fetch,
    abort,
  };
};
