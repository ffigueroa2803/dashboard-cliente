import { useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { toast } from "react-toastify";
import { scaleRequest } from "@services/scale.request";

const request = scaleRequest();

export const useContractChangeFile = () => {
  const { contract } = useSelector((state: RootState) => state.contract);

  const [file, setFile] = useState<Blob | string>("");
  const [pending, setPending] = useState<boolean>(false);
  const [isUpload, setIsUpload] = useState<boolean>(false);

  const save = async () => {
    setPending(true);
    setIsUpload(false);
    const form = new FormData();
    form.append("file", file);
    // send request
    await request
      .post(`contracts/${contract.id}/changeFile`, form)
      .then(() => {
        toast.success("Archivo subido!!!");
        setIsUpload(true);
      })
      .catch(() => toast.error("No se pudo subir el archivo"));
    setPending(false);
  };

  return {
    pending,
    file,
    setFile,
    save,
    isUpload,
  };
};
