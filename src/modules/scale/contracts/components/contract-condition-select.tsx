/* eslint-disable no-unused-vars */
import { SelectBasic } from "@common/select/select-basic";

interface IProps {
  name: string;
  value: any;
  placeholder?: string;
  onChange: (object: any) => void;
  onBlur?: (option: any) => void;
}

export const ContractConditionSelect = ({
  name,
  value,
  placeholder,
  onChange,
  onBlur,
}: IProps) => {
  return (
    <SelectBasic
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      onBlur={onBlur}
      options={[
        { label: "Contratado", value: "CONTRATADO" },
        { label: "Nombrado", value: "NOMBRADO" },
        { label: "Cas", value: "CAS" },
        { label: "Pensionista", value: "PENSIONISTA" },
      ]}
    />
  );
};
