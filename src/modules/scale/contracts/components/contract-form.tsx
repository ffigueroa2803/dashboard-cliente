/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";
import { HourhandSelect } from "@modules/scale/hourhands/components/hourhand-select";
import { ProfileSelect } from "@modules/scale/profiles/components/profile-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { MapPin } from "react-feather";
import { Form, FormGroup, Input } from "reactstrap";
import { ICreateContractDto } from "../dtos/create-contract.dto";
import { ContractConditionSelect } from "./contract-condition-select";

interface IProps<T> {
  form: T;
  isEdit?: boolean;
  onChange: (input: IInputHandle) => void;
}

export const ContractForm = ({
  form,
  onChange,
}: IProps<ICreateContractDto>) => {
  return (
    <Form>
      <FormGroup>
        <label>
          Resolución <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="resolution"
          value={form?.resolution}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Resolución <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="dateOfResolution"
          value={form?.dateOfResolution}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Fecha de Ingreso <b className="text-danger">*</b>
        </label>
        <Input
          type="date"
          name="dateOfAdmission"
          value={form?.dateOfAdmission}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>Fecha de Cese</label>
        <Input
          type="date"
          name="terminationDate"
          value={form?.terminationDate || ""}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Tip. Categoría <b className="text-danger">*</b>
        </label>
        <TypeCategorySelect
          name="typeCategoryId"
          value={form?.typeCategoryId || ""}
          onChange={onChange}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Condición <b className="text-danger">*</b>
        </label>
        <ContractConditionSelect
          name="condition"
          value={form?.condition || ""}
          onChange={onChange}
        />
      </FormGroup>

      <FormGroup>
        <label>Ley</label>
        <Input
          type="text"
          name="ley"
          value={form?.ley || ""}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>Plaza</label>
        <Input
          type="text"
          name="plaza"
          value={form?.plaza || ""}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <FormGroup>
        <label>Observación</label>
        <Input
          type="textarea"
          name="observation"
          value={form?.observation || ""}
          onChange={({ target }) => onChange(target)}
        />
      </FormGroup>

      <div>
        <hr />
        <MapPin size={17} /> <b>Entorno de Trabajo</b>
        <hr />
      </div>

      <FormGroup>
        <label>
          Dependencia <b className="text-danger">*</b>
        </label>
        <DependencySelect
          name="dependencyId"
          value={form?.dependencyId || ""}
          onChange={onChange}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Perfil Laboral <b className="text-danger">*</b>
        </label>
        <ProfileSelect
          name="profileId"
          value={form?.profileId || ""}
          onChange={onChange}
        />
      </FormGroup>

      <FormGroup>
        <label>
          Horario <b className="text-danger">*</b>
        </label>
        <HourhandSelect
          name="hourhandId"
          value={form?.hourhandId || ""}
          onChange={onChange}
        />
      </FormGroup>
    </Form>
  );
};
