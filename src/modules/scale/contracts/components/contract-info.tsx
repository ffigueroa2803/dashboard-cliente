import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { useContext, useMemo, useState } from "react";
import { AlertOctagon, Edit, MapPin, Settings } from "react-feather";
import Skeleton from "react-loading-skeleton";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Row,
} from "reactstrap";
import { WorkUserError } from "src/components/shared/work-user-error";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";
import { ContractEntity } from "src/domain/scale/contracts/contract.entity";
import { IContractEntity, contractEntityName } from "../dtos/contract.entity";
import { contractActions } from "../store";
import { ContractFolderCreate } from "./contrac-folder-create";
import { ContractChange } from "./contract-change";
import { ContractDelete } from "./contract-delete";
import { ContractEdit } from "./contract-edit";

interface IProps {
  pending: boolean;
  onDelete?: () => void;
}

export const ContractInfo = ({ pending }: IProps) => {
  const dispatch = useDispatch();
  const { contract } = useSelector((state: RootState) => state.contract);
  const { user } = useSelector((state: RootState) => state.person);

  const ability = useContext(CaslContext);
  const contractEntity = new ContractEntity(contract);

  const [option, setOption] = useState<string | undefined>(undefined);

  const switchOptions = {
    CHANGE: "CHANGE",
    EDIT: "EDIT",
  };

  const showFooter = () => {
    if (pending) return false;
    if (!isContract) return false;
    if (ability.can(PermissionAction.UPDATE, contractEntityName)) {
      return true;
    }
    if (ability.can(PermissionAction.DELETE, contractEntityName)) {
      return true;
    }

    return false;
  };

  const isContract = useMemo(() => {
    return Object.keys(contract || {}).length > 1;
  }, [contract]);

  const displayDateOfResolution = useMemo(() => {
    if (!contract?.dateOfResolution) return "";
    return DateTime.fromSQL(`${contract?.dateOfResolution}`).toFormat(
      "dd/MM/yyyy"
    );
  }, [contract]);

  const displayDateOfAdmission = useMemo(() => {
    if (!contract?.dateOfAdmission) return "";
    return DateTime.fromSQL(`${contract?.dateOfAdmission}`).toFormat(
      "dd/MM/yyyy"
    );
  }, [contract]);

  const displayTerminationDate = useMemo(() => {
    if (!contract?.terminationDate) return "Permanente";
    return DateTime.fromSQL(`${contract?.terminationDate}`).toFormat(
      "dd/MM/yyyy"
    );
  }, [contract]);

  const handleAdd = (cont: IContractEntity) => {
    dispatch(contractActions.find(cont));
    setOption(undefined);
  };

  const ComponentErrorHeader = (
    <div className="text-center">
      <AlertOctagon size={30} className="text-danger" />
      <div className="text-center">
        <b>Intente más tarde</b>
      </div>
    </div>
  );

  const ComponentErrorBody = (
    <div className="text-center">No hay registros disponibles</div>
  );

  return (
    <>
      <Card>
        <CardHeader>
          <h6 className="text-left">
            <Show
              condition={isContract}
              isDefault={pending ? <Skeleton /> : ComponentErrorHeader}>
              <span
                className={`ml-1 badge badge-${
                  contract?.state ? "success" : "danger"
                }`}>
                {contract?.code}
              </span>

              <Show condition={!pending}>
                <span
                  className="close"
                  title="Cambiar de contrato"
                  onClick={() => setOption(switchOptions.CHANGE)}>
                  <Settings className="icon cursor-pointer" />
                </span>
              </Show>
            </Show>
          </h6>
        </CardHeader>
        <CardBody>
          <Show
            condition={isContract}
            isDefault={pending ? "obteniendo datos" : ComponentErrorBody}>
            <Row>
              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Tipo. Trabajador</label>
                  <h6>{contract?.typeCategory?.typeCargo?.name || ""}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Condición</label>
                  <h6>{contract?.condition || ""}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <FormGroup className="mb-3">
                  <label>Categoría</label>
                  <h6>
                    {contract?.typeCategory?.name || "N/A"}
                    <Show
                      condition={Boolean(
                        contract?.typeCategory?.dedication?.length || 0
                      )}>
                      <span className="ml-1">
                        - {contract?.typeCategory?.dedication}
                      </span>
                    </Show>
                  </h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>N° Resolución</label>
                  <h6>{contract?.resolution || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Fecha de Resolución</label>
                  <h6>{displayDateOfResolution}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Fecha de Ingreso</label>
                  <h6>{displayDateOfAdmission}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Fecha de Cese</label>
                  <h6>{displayTerminationDate}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Plaza</label>
                  <h6>{contract?.plaza || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="6">
                <FormGroup className="mb-3">
                  <label>Ley</label>
                  <h6>{contract?.ley || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <FormGroup className="mb-3">
                  <label>Observación</label>
                  <h6>{contract?.observation || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <hr />
                <MapPin className="icon" /> Entorno de Trabajo
                <hr />
              </Col>

              <Col md="12">
                <FormGroup className="mb-3">
                  <label>Dependencia</label>
                  <h6>{contract?.dependency?.name || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <FormGroup className="mb-3">
                  <label>Perfil Laboral</label>
                  <h6>{contract?.profile?.name || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <FormGroup className="mb-3">
                  <label>Horario</label>
                  <h6>{contract?.hourhand?.name || "N/A"}</h6>
                </FormGroup>
              </Col>

              <Col md="12">
                <Show
                  condition={user.id != undefined}
                  isDefault={<WorkUserError />}>
                  <Show
                    condition={!contractEntity.folderId}
                    isDefault={
                      <FolderInfoComponent id={contractEntity.folderId || ""} />
                    }>
                    <ContractFolderCreate contract={contractEntity} />
                  </Show>
                </Show>
              </Col>
            </Row>
          </Show>
        </CardBody>
        {/* footer */}
        <Show condition={showFooter()}>
          <CardFooter className="text-right">
            {/* eliminar */}
            <Show
              condition={ability.can(
                PermissionAction.DELETE,
                contractEntityName
              )}>
              <span className="mr-2">
                <ContractDelete />
              </span>
            </Show>
            {/* editar */}
            <Show
              condition={ability.can(
                PermissionAction.UPDATE,
                contractEntityName
              )}>
              <Button
                color="info"
                size="sm"
                onClick={() => setOption(switchOptions.EDIT)}>
                <Edit className="icon" />
              </Button>
            </Show>
          </CardFooter>
        </Show>
      </Card>
      {/* options */}
      <ContractChange
        onAdd={handleAdd}
        isOpen={switchOptions.CHANGE == option}
        onClose={() => setOption(undefined)}
      />
      {/* edit */}
      <ContractEdit
        isOpen={switchOptions.EDIT == option}
        onClose={() => setOption(undefined)}
      />
    </>
  );
};
