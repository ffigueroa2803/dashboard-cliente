/* eslint-disable no-unused-vars */
import { getWorkToContracts } from "@modules/scale/works/apis";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import React, { useEffect, useState } from "react";
import { Check } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Modal, ModalBody, ModalHeader } from "reactstrap";
import { IContractEntity } from "../dtos/contract.entity";
import { contractActions } from "../store";
import { ContractDetail } from "./contract-detail";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  arrayDisabled?: number[];
  onAdd: (contract: IContractEntity) => void;
}

export const ContractChange = ({
  isOpen,
  onClose,
  onAdd,
  arrayDisabled,
}: IProps) => {
  const [, setPending] = useState<boolean>(true);
  const [, setIsError] = useState<boolean>(false);
  const [page] = useState<number>(1);
  const { work } = useSelector((state: RootState) => state.work);
  const { contracts } = useSelector((state: RootState) => state.contract);
  const dispatch = useDispatch();

  const getData = async () => {
    setPending(true);
    setIsError(false);
    await getWorkToContracts(work?.id || 0, { page })
      .then((data) => {
        dispatch(contractActions.paginate(data));
        setIsError(false);
      })
      .catch(() => setIsError(true));
    setPending(false);
  };

  const handleAdd = (cont: IContractEntity) => {
    if (typeof onAdd == "function") {
      onAdd(cont);
    }
  };

  useEffect(() => {
    if (work?.id && isOpen) getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [work, isOpen]);

  return (
    <Modal isOpen={isOpen} toggle={onClose}>
      <ModalHeader toggle={onClose}>Cambiar de contrato</ModalHeader>
      <ModalBody>
        {contracts?.items?.map((cont, index) => (
          <Col xl="12" md="12" key={`list-item-contract-change-${index}`}>
            <ContractDetail
              contract={cont}
              onClick={handleAdd}
              isDisabled={arrayDisabled?.includes(cont.id) || false}
            />
          </Col>
        ))}
      </ModalBody>
    </Modal>
  );
};
