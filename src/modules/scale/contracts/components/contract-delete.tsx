/* eslint-disable react-hooks/exhaustive-deps */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { CaslContext } from "@modules/auth/casls/casl.context";
import { PermissionAction } from "@modules/auth/permissions/dtos/permission.action";
import { RootState } from "@store/store";
import { useContext, useEffect } from "react";
import { Trash } from "react-feather";
import { useSelector } from "react-redux";
import { Button } from "reactstrap";
import { contractEntityName } from "../dtos/contract.entity";
import { useContractCanDelete } from "../hooks/use-contract-can-delete";
import { useContractDelete } from "../hooks/use-contract-delete";
import { ContractContext } from "./contract-context";

export const ContractDelete = () => {
  const { contract } = useSelector((state: RootState) => state.contract);
  const ability = useContext(CaslContext);

  const canDelete = useContractCanDelete();
  const contractDelete = useContractDelete();

  const contractContext = useContext(ContractContext);

  useEffect(() => {
    if (contract.id) canDelete.fetch();
    return () => canDelete.abort();
  }, [contract]);

  useEffect(() => {
    if (contractDelete.isSuccess) {
      contractContext.setIsRefresh(true);
    }
  }, [contractDelete.isSuccess]);

  if (!ability.can(PermissionAction.DELETE, contractEntityName)) {
    return null;
  }

  if (canDelete.pending) {
    return (
      <span>
        <ButtonLoading loading color="danger" title="Verificando" />
      </span>
    );
  }

  if (canDelete.result) {
    return (
      <Show
        condition={!contractDelete.pending}
        isDefault={<ButtonLoading title="Elimando" color="danger" loading />}
      >
        <Button color="danger" outline onClick={contractDelete.destroy}>
          <Trash className="icon" />
        </Button>
      </Show>
    );
  }

  return null;
};
