/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { findLastContract } from "@modules/scale/works/apis";
import { RootState } from "@store/store";
import { createContext, FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IContractEntity } from "../dtos/contract.entity";
import { contractActions } from "../store";

export const ContractContext = createContext({
  pending: false,
  setIsRefresh: (value: boolean) => {},
});

export const ContractProvider: FC = ({ children }) => {
  const dispatch = useDispatch();
  const { work } = useSelector((state: RootState) => state.work);

  const [pending, setPending] = useState<boolean>(false);
  const [isRefresh, setIsRefresh] = useState<boolean>(false);

  const handleFindLastContract = async () => {
    setPending(true);
    await findLastContract(work?.id || 0)
      .then((data) => {
        const contract: IContractEntity = data;
        dispatch(contractActions.find(contract));
      })
      .catch(() => dispatch(contractActions.find({} as any)));
    setPending(false);
  };

  useEffect(() => {
    if (work?.id) setIsRefresh(true);
  }, [work]);

  useEffect(() => {
    if (isRefresh) handleFindLastContract();
  }, [isRefresh]);

  useEffect(() => {
    if (isRefresh) setIsRefresh(false);
  }, [isRefresh]);

  return (
    <ContractContext.Provider value={{ pending, setIsRefresh }}>
      {children}
    </ContractContext.Provider>
  );
};
