/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button } from "reactstrap";
import { ContractEntity } from "src/domain/scale/contracts/contract.entity";
import { IContractEntityInterface } from "src/domain/scale/contracts/contract.entity.interface";
import { useContractFolderCreate } from "../hooks/use-contract-folder-create";
import { contractActions } from "../store";

export interface IContractFolderCreateProps {
  contract: IContractEntityInterface;
}

export const ContractFolderCreate = ({
  contract,
}: IContractFolderCreateProps) => {
  const dispatch = useDispatch();
  const contractFolderCreate = useContractFolderCreate(contract);

  const handle = () => {
    contractFolderCreate
      .execute()
      .then((data) => {
        const newContract = new ContractEntity(contract);
        newContract.setFolder(data);
        dispatch(contractActions.find(newContract));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => contractFolderCreate.abort();
  }, []);

  return (
    <div className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={contractFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </div>
  );
};
