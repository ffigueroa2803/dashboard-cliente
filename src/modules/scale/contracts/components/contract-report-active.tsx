import React, { useState } from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { TypeCargoSelect } from "@modules/scale/type-cargos/components/type-cargo-select";
import { ContractConditionSelect } from "./contract-condition-select";
import { TypeCategorySelect } from "@modules/scale/type-categories/components/type-category-select";
import { DependencySelect } from "@modules/auth/dependencies/components/dependency-select";

const request = scaleRequest();

export const ContractReportActive = () => {
  const [typeCargoId, setTypeCargoId] = useState<string | undefined>();
  const [condition, setCondition] = useState<string | undefined>();
  const [typeCategoryId, setTypeCategoryId] = useState<string | undefined>();
  const [dependencyId, setDependencyId] = useState<string | undefined>();

  const handleClick = (extname: string) => {
    // filters
    const params = new URLSearchParams();
    if (typeCargoId) params.append("typeCargoIds[0]", typeCargoId);
    if (condition) params.append("conditions[0]", condition);
    if (typeCategoryId) params.append("typeCategoryIds[0]", typeCategoryId);
    if (dependencyId) params.append("dependencyIds[0]", dependencyId);
    // url
    const url = urljoin(
      request.urlBase,
      `/contracts/reports/active.${extname}?${params.toString()}`
    );
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Cargo</label>
          <TypeCargoSelect
            name="typeCargoId"
            value={typeCargoId}
            onChange={({ value }) => setTypeCargoId(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Condición</label>
          <ContractConditionSelect
            name="condition"
            value={condition}
            onChange={({ value }) => setCondition(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Tip. Categoria</label>
          <TypeCategorySelect
            name="typeCategoryId"
            value={typeCategoryId}
            onChange={({ value }) => setTypeCategoryId(value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">Dependencia</label>
          <DependencySelect
            name="dependencyId"
            value={dependencyId}
            onChange={({ value }) => setDependencyId(value)}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}>
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
