import { BallotContent } from "@modules/scale/ballots/components/ballot-content";
import { DisplacementContent } from "@modules/scale/displacements/components/displacement-content";
import { LicenseContent } from "@modules/scale/licenses/components/license-content";
import { MeritContent } from "@modules/scale/merits/components/merit-content";
import { ScheduleContent } from "@modules/scale/schedules/components/schedule-content";
import { RootState } from "@store/store";
import { MouseEvent, useContext } from "react";
import {
  Activity,
  Award,
  Calendar,
  Clipboard,
  FileText,
  Repeat,
} from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import { contractActions } from "../store";
import { ContractContext, ContractProvider } from "./contract-context";
import { ContractInfo } from "./contract-info";

const optionActions = {
  SCHEDULE: "SCHEDULE",
  BALLOT: "BALLOT",
  VACATION: "VACATION",
  LICENSE: "LICENSE",
  ASCENT: "ASCENT",
  DISPLACEMENT: "DISPLACEMENT",
  MERIT: "MERIT",
};

const ListActions = [
  {
    label: "Horarios",
    icon: Calendar,
    key: optionActions.SCHEDULE,
  },
  {
    label: "Papeletas",
    icon: FileText,
    key: optionActions.BALLOT,
  },
  {
    label: "Licencias",
    icon: Clipboard,
    key: optionActions.LICENSE,
  },
  {
    label: "Rotaciones",
    icon: Repeat,
    key: optionActions.DISPLACEMENT,
  },
  {
    label: "Mérito/Demérito",
    icon: Award,
    key: optionActions.MERIT,
  },
];

export const ContractWrapper = () => {
  const dispatch = useDispatch();
  const { option, contract } = useSelector(
    (state: RootState) => state.contract
  );

  const contractContext = useContext(ContractContext);

  const handleClick = (e: MouseEvent, key: string) => {
    e.preventDefault();
    if (contract?.id) {
      dispatch(contractActions.changeOption(key));
    }
  };

  return (
    <>
      <Row>
        <Col md="7">
          <ContractInfo pending={contractContext.pending} />
        </Col>
        {/* acciones */}
        <Col md="5">
          <Card>
            <CardHeader>
              <h6>
                <Activity className="icon" /> Acciones
              </h6>
            </CardHeader>
            <CardBody>
              {ListActions.map((l, index: number) => (
                <div className="mb-2 capitalize" key={`list-${index}`}>
                  <a
                    href="#"
                    onClick={(e) => handleClick(e, l.key)}
                    className={contract?.id ? "" : "text-muted"}
                  >
                    <l.icon className="icon" /> {l.label}
                  </a>
                </div>
              ))}
            </CardBody>
          </Card>
        </Col>
      </Row>
      {/* schedules */}
      <ScheduleContent
        isOpen={optionActions.SCHEDULE == option}
        onClose={() => dispatch(contractActions.changeOption(""))}
      />
      {/* license */}
      <LicenseContent
        isOpen={optionActions.LICENSE == option}
        onClose={() => dispatch(contractActions.changeOption(""))}
      />
      {/* ballot */}
      <BallotContent
        isOpen={optionActions.BALLOT == option}
        onClose={() => dispatch(contractActions.changeOption(""))}
      />
      {/* displacements */}
      <DisplacementContent
        isOpen={optionActions.DISPLACEMENT == option}
        onClose={() => dispatch(contractActions.changeOption(""))}
      />
      {/* merits */}
      <MeritContent
        isOpen={optionActions.MERIT == option}
        onClose={() => dispatch(contractActions.changeOption(""))}
      />
    </>
  );
};

export const ContractContent = () => {
  return (
    <ContractProvider>
      <ContractWrapper />
    </ContractProvider>
  );
};
