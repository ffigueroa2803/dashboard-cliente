/* eslint-disable no-unused-vars */
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { DateTime } from "luxon";
import { Check, X } from "react-feather";
import { useSelector } from "react-redux";
import { Button } from "reactstrap";
import { IContractEntity } from "../dtos/contract.entity";

interface IProps {
  contract: IContractEntity;
  onClick?: (contract: IContractEntity) => void;
  onClose?: (contract: IContractEntity) => void;
  isDisabled?: boolean;
  outline?: boolean;
}

export const ContractDetail = ({
  contract,
  isDisabled,
  outline = false,
  onClick,
  onClose,
}: IProps) => {
  const currentContract = useSelector(
    (state: RootState) => state.contract.contract
  );

  const handleClick = () => {
    if (typeof onClick == "function") {
      onClick(contract);
    }
  };

  const handleOnClose = () => {
    if (typeof onClose == "function") {
      onClose(contract);
    }
  };

  return (
    <div className="prooduct-details-box mb-3">
      <div className="media">
        <Show condition={typeof onClose == "function"}>
          <X className="icon close" onClick={handleOnClose} />
        </Show>
        <div className="media-body ml-3">
          <Show condition={!outline}>
            <div className="product-name">
              <h6>
                <span
                  className={`badge badge-${
                    contract?.state ? "success" : "danger"
                  }`}
                >
                  {contract.code}
                </span>
              </h6>
            </div>
          </Show>
          <Show condition={outline}>
            <div>
              Código:
              <b className="ml-1">{contract.code}</b>
            </div>
          </Show>
          <div>
            Tipo de Trabajador:
            <b className="ml-1">
              {contract?.typeCategory?.typeCargo?.name || ""}
            </b>
          </div>
          <div>
            Categoría:
            <b className="ml-1">{contract?.typeCategory?.name || ""}</b>
          </div>
          <div>
            Condición:
            <b className="ml-1">{contract?.condition || ""}</b>
          </div>
          <div>
            N° Resolución:
            <b className="ml-1">{contract.resolution || ""}</b>
          </div>
          <div>
            Fecha de Resolución:
            <b className="ml-1">
              {DateTime.fromSQL(`${contract.dateOfResolution}`).toFormat(
                "dd/MM/yyyy"
              )}
            </b>
          </div>
          <div>
            Fecha de Ingreso:
            <b className="ml-1">
              {DateTime.fromSQL(`${contract.dateOfAdmission}`).toFormat(
                "dd/MM/yyyy"
              )}
            </b>
          </div>
          <div>
            Fecha de Cese:
            <b className="ml-1">
              {contract.terminationDate
                ? DateTime.fromSQL(`${contract.terminationDate}`).toFormat(
                    "dd/MM/yyyy"
                  )
                : "Indefinido"}
            </b>
          </div>
          <Show condition={outline}>
            <div>
              Estado:
              <b className="ml-1">
                {contract.state ? "Habilitado" : "Deshabilitado"}
              </b>
            </div>
          </Show>
          {/* btn */}
          <Show condition={typeof onClick == "function"}>
            <Button
              color="primary"
              className="btn-bottom-right"
              outline={contract?.id !== currentContract?.id}
              size="xs"
              disabled={contract?.id === currentContract?.id || isDisabled}
              onClick={handleClick}
            >
              <Check className="icon" />
            </Button>
          </Show>
        </div>
      </div>
    </div>
  );
};
