import React, { useState } from "react";
import { Input } from "reactstrap";
import { getContracts } from "../apis";
import { IContractEntity } from "../dtos/contract.entity";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "@store/store";
import { contractActions } from "../store";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onAdd: (contract: IContractEntity) => void;
}

export const ContractSearchSelect = ({ onAdd }: IProps) => {
  const dispatch = useDispatch();
  const { contracts } = useSelector((state: RootState) => state.contract);

  const [inputSearch, setInputSearch] = useState<string>("");
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [pending, setPending] = useState<boolean>(false);

  const handleSearch = async (value: string) => {
    setInputSearch(value);
    if (value.length > 3) {
      setIsOpen(true);
      setPending(true);
      await getContracts({ page: 1, querySearch: value, state: true })
        .then((data) => dispatch(contractActions.paginate(data)))
        .catch(() => dispatch(contractActions.paginate({} as any)));
    } else dispatch(contractActions.paginate({} as any));
    // quitar dialog
    if (!value) setIsOpen(false);
    // quitar pending
    setPending(false);
  };

  const handleAdd = async (contract: IContractEntity) => {
    setIsOpen(false);
    setInputSearch("");
    if (typeof onAdd == "function") onAdd(contract);
  };

  const ComponentAddPeople = (
    <div className="text-center">No hay regístros</div>
  );

  const ComponentDialog = () => {
    if (!isOpen) return null;
    return (
      <div className="dropdown-body card card-body">
        {contracts?.items?.map((item) => (
          <div
            key={`item-search-${item.id}`}
            className="capitalize cursor-pointer dropdown-item"
            onClick={() => handleAdd(item)}
          >
            {/* info work */}
            <div>
              <span className="badge badge-primary mr-2">
                {item?.work?.person?.documentNumber || ""}
              </span>
              <span className="uppercase">
                {item?.work?.person?.fullName || ""}
              </span>
            </div>
            {/* info contract */}
            <div>
              <span className="badge badge-dark mr-2">{item.code}</span>
              <span className="capitalize">
                {item?.typeCategory?.name || ""}
              </span>
            </div>
          </div>
        ))}
        {/* loading */}
        <span className="text-center">{pending ? "Buscando..." : ""}</span>
        {/* no hay datos */}
        <span className="text-center">
          {!pending && !contracts?.items?.length ? ComponentAddPeople : ""}
        </span>
      </div>
    );
  };

  return (
    <div className="dropdown-content">
      <Input
        type="text"
        placeholder="buscar contrato..."
        onChange={({ target }) => handleSearch(target.value)}
        value={inputSearch}
      />
      {ComponentDialog()}
    </div>
  );
};
