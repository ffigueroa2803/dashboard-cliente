import React, { useEffect } from "react";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { SelectBasic } from "@common/select/select-basic";
import { useContractScheduleList } from "../hooks/use-contract-schedule-list";
import { IScheduleEntity } from "@modules/scale/schedules/dtos/schedule.entity";
import { plainToClass } from "class-transformer";
import { ScheduleSerialize } from "@modules/scale/schedules/serializers/schedule.serialize";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: number | string;
  name: string;
  defaultQuerySearch?: string;
}

export const ContractScheduleSelect = ({
  name,
  value,
  defaultQuerySearch,
  onChange,
}: IProps) => {
  const { schedules } = useSelector((state: RootState) => state.schedule);
  const contractScheduleList = useContractScheduleList({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
    year: new Date().getFullYear(),
    month: new Date().getMonth() + 1,
  });

  const displayLabel = (row: IScheduleEntity) => {
    const scheduleSerialize = plainToClass(ScheduleSerialize, row);
    return `${scheduleSerialize.displayDate} ${scheduleSerialize.displayFormatter}`;
  };

  const settingsData = () => {
    const datos: any[] = [];
    schedules?.items?.map((row) => {
      datos.push({
        label: `${displayLabel(row)}`.toLowerCase(),
        value: row.id,
        obj: row,
      });
      // add exit
      if (row?.exit) {
        datos.push({
          label: `${displayLabel(row?.exit)}`.toLowerCase(),
          value: row?.exit.id,
          obj: row?.exit,
        });
      }
    });
    // response
    return datos;
  };

  useEffect(() => {
    contractScheduleList.fetch();
  }, [contractScheduleList.querySearch]);

  return (
    <SelectBasic
      name={name}
      value={value}
      options={settingsData()}
      onChange={onChange}
      onSearch={contractScheduleList.setQuerySearch}
    />
  );
};
