import React from "react";
import { File } from "react-feather";
import { Button, Col, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";

const request = scaleRequest();

export const ContractReportAdmission = () => {
  const handleClick = (extname: string) => {
    const url = urljoin(
      request.urlBase,
      `/contracts/reports/admission.${extname}`
    );
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  return (
    <Row>
      <Col md="6">
        <Button color="danger" block onClick={() => handleClick("pdf")}>
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}
        >
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
