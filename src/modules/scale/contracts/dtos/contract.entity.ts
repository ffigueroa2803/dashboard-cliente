import { IDependencyEntity } from "@modules/auth/dependencies/dtos/dependency.entity";
import { IHourhandEntity } from "@modules/scale/hourhands/dtos/hourhand.entity";
import { IProfileEntity } from "@modules/scale/profiles/dtos/profile.entity";
import { ITypeCategoryEntity } from "@modules/scale/type-categories/dtos/type-category.entity";
import { WorkEntity } from "@modules/scale/works/dtos/work.entity";

export type ConditionContractType =
  | "CONTRATADO"
  | "NOMBRADO"
  | "CAS"
  | "PENSIONISTA";

export interface IContractEntity {
  id: number;
  workId: number;
  dependencyId: number;
  profileId: number;
  typeCategoryId: number;
  condition: ConditionContractType;
  ley?: string;
  plaza?: string;
  codeAIRHSP?: string;
  hourhandId: number;
  code: string;
  resolution: string;
  dateOfResolution: string;
  dateOfAdmission: string;
  terminationDate: string;
  observation?: string;
  hours: number;
  fileId?: number;
  state: boolean;
  work?: WorkEntity;
  typeCategory?: ITypeCategoryEntity;
  dependency?: IDependencyEntity;
  profile?: IProfileEntity;
  hourhand?: IHourhandEntity;
  file?: any;
}

export const contractEntityName = "ContractEntity";
