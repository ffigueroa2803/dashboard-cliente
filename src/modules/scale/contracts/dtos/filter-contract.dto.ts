import { PaginateDto } from "@services/dtos";

export interface FilterGetContractsDto extends PaginateDto {
  state?: boolean;
}
