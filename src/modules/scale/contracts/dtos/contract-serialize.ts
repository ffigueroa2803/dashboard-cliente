import { DateTime } from "luxon";

export class ContractSerialize {
  private dateOfResolution!: string;
  private dateOfAdmission!: string;
  private terminationDate!: string;

  get displayDateOfResolution() {
    const data = DateTime.fromFormat(this.dateOfResolution || "", "yyyy-MM-dd");
    if (!data.isValid) return null;
    return data.toFormat("dd/MM/yyyy");
  }

  get displayDateOfAdmission() {
    const data = DateTime.fromFormat(this.dateOfAdmission || "", "yyyy-MM-dd");
    if (!data.isValid) return null;
    return data.toFormat("dd/MM/yyyy");
  }

  get displayTerminationDate() {
    const data = DateTime.fromFormat(this.terminationDate || "", "yyyy-MM-dd");
    if (!data.isValid) return null;
    return data.toFormat("dd/MM/yyyy");
  }
}
