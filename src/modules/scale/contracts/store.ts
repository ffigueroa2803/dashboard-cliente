import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { MetaDto } from "@services/dtos";
import { HYDRATE } from "next-redux-wrapper";
import { IContractEntityInterface } from "src/domain/scale/contracts/contract.entity.interface";
import { IContractEntity } from "./dtos/contract.entity";
import { IClockEntity } from "@modules/clock/clocks/dtos/clock.entity";
import { IUserEntity } from "@modules/clock/users/dtos/user.entity";

export interface WorkState {
  contracts: {
    meta: MetaDto;
    items: IContractEntity[];
  };
  contract: IContractEntity | IContractEntityInterface;
  marker: IUserEntity;
  zktecos: ResponsePaginateDto<IClockEntity>;
  option: string;
}

const initialState: WorkState = {
  contracts: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  marker: {} as any,
  zktecos: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  contract: {} as any,
  option: "",
};

const contractStore = createSlice({
  name: "escalafon@contracts",
  initialState,
  reducers: {
    paginate: (
      state: WorkState,
      { payload }: PayloadAction<{ meta: MetaDto; items: IContractEntity[] }>
    ) => {
      state.contracts = payload;
    },
    find: (
      state: WorkState,
      { payload }: PayloadAction<IContractEntity | IContractEntityInterface>
    ) => {
      state.contract = payload;
    },
    setMarker: (state: WorkState, { payload }: PayloadAction<IUserEntity>) => {
      console.log("ok", payload);
      state.marker = payload;
    },
    zktecoPaginate: (
      state: WorkState,
      { payload }: PayloadAction<ResponsePaginateDto<IClockEntity>>
    ) => {
      state.zktecos = payload;
    },
    changeOption: (state: WorkState, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    updateItem: (
      state: WorkState,
      { payload }: PayloadAction<IContractEntityInterface>
    ) => {
      state.contracts.items = state.contracts.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload) as any;
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state: any, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.contract };
    },
  },
});

export const contractReducer = contractStore.reducer;

export const contractActions = contractStore.actions;
