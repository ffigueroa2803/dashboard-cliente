import { scaleRequest } from "@services/scale.request";
import { IContractEntity } from "./dtos/contract.entity";
import { ICreateContractDto } from "./dtos/create-contract.dto";
import { IEditContractDto } from "./dtos/edit-contract.dto";
import { FilterGetContractsDto } from "./dtos/filter-contract.dto";

const request = scaleRequest();

export const getContracts = async ({
  page,
  querySearch,
  limit,
  state,
}: FilterGetContractsDto) => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("querySearch", querySearch || "");
  params.set("limit", `${limit || 30}`);
  params.set("state", `${state}`);
  // response
  return await request.get(`contracts`, { params }).then((res) => res.data);
};

export const createContract = async (
  payload: ICreateContractDto
): Promise<IContractEntity> => {
  // send request
  return await request.post(`contracts`, payload).then((res) => res.data);
};

export const findContract = async (id: number) => {
  return await request.get(`contracts/${id}`).then((res) => res.data);
};

export const editContract = async (
  id: number,
  payload: IEditContractDto
): Promise<IContractEntity> => {
  // send request
  payload.terminationDate = payload.terminationDate || null;
  return await request.put(`contracts/${id}`, payload).then((res) => res.data);
};

export const findContractFile = async (id: number): Promise<any> => {
  return await request.get(`contracts/${id}/file`).then((res) => res.data);
};
