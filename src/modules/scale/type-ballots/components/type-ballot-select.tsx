import React from 'react';
import { SelectRemote } from '@common/select/select-remote';
import { getTypeBallots } from '../apis';
import { ITypeBallotEntity } from '../dtos/type-ballot.entity';

interface IProps { 
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void
  value: number | string
  name: string,
  defaultQuerySearch?: string
}

export const TypeBallotSelect = ({ name, value, onChange, defaultQuerySearch }: IProps) => {

  return (
    <SelectRemote
      defaultQuerySearch={defaultQuerySearch}
      handle={getTypeBallots}
      name={name}
      value={value}
      onChange={onChange}
      selectRow={{ 
        label: (row: ITypeBallotEntity) => `${row.name}`.toLowerCase(),
        value: (row) => row.id
      }}
    />
  )
}