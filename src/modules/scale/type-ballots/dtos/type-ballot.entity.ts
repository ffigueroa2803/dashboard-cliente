export interface ITypeBallotEntity {
  id: number;
  name: string;
  description: string;
  state: boolean;
}
