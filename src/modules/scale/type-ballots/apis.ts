import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { PaginateDto } from "@services/dtos";
import { scaleRequest } from "@services/scale.request";
import { ITypeBallotEntity } from "./dtos/type-ballot.entity";

const request = scaleRequest();

export const getTypeBallots = async ({
  page,
  limit,
  querySearch,
}: PaginateDto): Promise<ResponsePaginateDto<ITypeBallotEntity>> => {
  const params = new URLSearchParams();
  params.set("page", `${page}`);
  params.set("limit", `${limit || 30}`);
  params.set("querySearch", querySearch || "");
  return await request
    .get(`typeBallots`, { params })
    .then((res) => res.data)
    .catch(() => ({ err: true }));
};
