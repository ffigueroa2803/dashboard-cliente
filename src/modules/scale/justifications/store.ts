import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IJustificationEntityInterface } from "src/domain/scale/justifications/justification.entity.interface";

export interface JustificationState {
  justifications: ResponsePaginateDto<IJustificationEntityInterface>;
  justification: IJustificationEntityInterface;
}

export const initialState: JustificationState = {
  justifications: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  justification: {} as any,
};

const scaleJustificationStore = createSlice({
  name: "scale@justifications",
  initialState,
  reducers: {
    paginate: (
      state: JustificationState,
      {
        payload,
      }: PayloadAction<ResponsePaginateDto<IJustificationEntityInterface>>
    ) => {
      state.justifications = payload;
      return state;
    },
    setJustification: (
      state: JustificationState,
      { payload }: PayloadAction<IJustificationEntityInterface>
    ) => {
      state.justification = payload;
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.scaleJustification };
    },
  },
});

export const scaleJustificationReducer = scaleJustificationStore.reducer;

export const scaleJustificationActions = scaleJustificationStore.actions;
