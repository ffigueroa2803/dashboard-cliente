/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button } from "reactstrap";
import { JustificationEntity } from "src/domain/scale/justifications/justification.entity";
import { IJustificationEntityInterface } from "src/domain/scale/justifications/justification.entity.interface";
import { useJustificationFolderCreate } from "../hooks/use-justification-folder-create";
import { scaleJustificationActions } from "../store";

export interface IJustificationFolderCreateProps {
  justification: IJustificationEntityInterface;
}

export const JustificationFolderCreate = ({
  justification,
}: IJustificationFolderCreateProps) => {
  const dispatch = useDispatch();
  const contractFolderCreate = useJustificationFolderCreate(justification);

  const handle = () => {
    contractFolderCreate
      .execute()
      .then((data) => {
        const newJustification = new JustificationEntity();
        newJustification.fill(justification);
        newJustification.setFolder(data);
        dispatch(scaleJustificationActions.setJustification(newJustification));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => contractFolderCreate.abort();
  }, []);

  return (
    <div className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={contractFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </div>
  );
};
