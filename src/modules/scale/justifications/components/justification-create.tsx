import { Save } from "react-feather";
import {
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { useJustificationCreate } from "../hooks/use-justification-create";
import { JustificationForm } from "./justification-form";

interface IProps {
  isOpen: boolean;
  onClose: () => void;
  onSave: () => void;
}

export const JustificationCreate = ({ isOpen, onClose, onSave }: IProps) => {
  const justificationCreate = useJustificationCreate();

  const handleSave = (e: any) => {
    e.preventDefault();
    justificationCreate
      .save()
      .then(() => onSave())
      .catch(() => null);
  };

  return (
    <Modal isOpen={isOpen}>
      <Form onSubmit={handleSave}>
        <ModalHeader toggle={onClose}>Crear Justificación</ModalHeader>
        <ModalBody>
          <JustificationForm
            pending={justificationCreate.pending}
            form={justificationCreate.form}
            onChange={justificationCreate.handleForm}
          />
        </ModalBody>
        <ModalFooter className="text-right">
          <Button
            color="primary"
            disabled={
              justificationCreate.pending || !justificationCreate.isValid()
            }
          >
            <Save className="icon" />
          </Button>
        </ModalFooter>
      </Form>
    </Modal>
  );
};
