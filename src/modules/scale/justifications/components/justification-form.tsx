/* eslint-disable no-unused-vars */
import { IInputHandle } from "@common/dtos/input-handle";
import { FormGroup, Input } from "reactstrap";
import { IJustificationFormDto } from "../dtos/justification-form.dto";

interface IProps {
  onChange: (value: IInputHandle) => void;
  form: IJustificationFormDto;
  pending: boolean;
}

export const JustificationForm = ({ onChange, form, pending }: IProps) => {
  return (
    <>
      <FormGroup className="mb-3">
        <label>
          Titulo <b className="text-danger">*</b>
        </label>
        <Input
          type="text"
          name="title"
          value={form.title || ""}
          onChange={({ target }) => onChange(target)}
          disabled={pending}
        />
      </FormGroup>

      <FormGroup className="mb-3">
        <label>
          Descripción <b className="text-danger">*</b>
        </label>
        <Input
          type="textarea"
          name="description"
          value={form.description || ""}
          onChange={({ target }) => onChange(target)}
          disabled={pending}
        />
      </FormGroup>
    </>
  );
};
