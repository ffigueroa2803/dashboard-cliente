/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { RootState } from "@store/store";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { FormGroup } from "reactstrap";
import { WorkUserError } from "src/components/shared/work-user-error";
import { FolderInfoComponent } from "src/components/storage/folders/folder-info.component";
import { JustificationEntity } from "src/domain/scale/justifications/justification.entity";
import { useJustificationFind } from "../hooks/use-justification-find";
import { JustificationFolderCreate } from "./justification-folder-create";

interface IProps {
  id: number;
}

export const JustificationInfo = ({ id }: IProps) => {
  const { user } = useSelector((state: RootState) => state.person);

  const { justification } = useSelector(
    (state: RootState) => state.scaleJustification
  );

  const justificationFind = useJustificationFind();

  const justificationEntity = new JustificationEntity();
  justificationEntity.fill(justification);

  useEffect(() => {
    if (id) justificationFind.fetch(id);
  }, [id]);

  if (justificationFind.pending) {
    return (
      <div className="text-center">
        <LoadingSimple loading />
      </div>
    );
  }

  return (
    <>
      <FormGroup className="mb-3">
        <label>Titulo</label>
        <h6>{justification.title || "N/A"}</h6>
      </FormGroup>

      <FormGroup className="mb-3">
        <label>Descripción</label>
        <h6>{justification.description || "N/A"}</h6>
      </FormGroup>

      {/* add file */}
      <Show condition={user.id != undefined} isDefault={<WorkUserError />}>
        <Show
          condition={!justificationEntity.folderId}
          isDefault={
            <FolderInfoComponent id={justificationEntity.folderId || ""} />
          }
        >
          <JustificationFolderCreate justification={justificationEntity} />
        </Show>
      </Show>
    </>
  );
};
