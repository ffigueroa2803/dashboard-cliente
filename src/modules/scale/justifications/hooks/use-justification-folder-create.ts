/* eslint-disable no-async-promise-executor */
import { scaleRequest } from "@services/scale.request";
import { RootState } from "@store/store";
import { useState } from "react";
import { useSelector } from "react-redux";
import { IJustificationEntityInterface } from "src/domain/scale/justifications/justification.entity.interface";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";

const request = scaleRequest();

export const useJustificationFolderCreate = (
  justification: IJustificationEntityInterface
) => {
  const { user } = useSelector((state: RootState) => state.person);
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFolderEntityInterface>(async (resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      await request
        .post(
          `justifications/${justification.id}/folders`,
          { userId: user.id },
          { signal: abortController.signal }
        )
        .then(({ data }) => resolve(data))
        .catch((err) => reject(err));
      setPending(false);
    });
  };

  return { pending, execute, abort };
};
