/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-async-promise-executor */
import { IInputHandle } from "@common/dtos/input-handle";
import { useEffect, useState } from "react";
import { IJustificationFormDto } from "../dtos/justification-form.dto";
import { scaleRequest } from "@services/scale.request";
import { alerStatus } from "@common/errors/utils/alert-status";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { IDiscountEntity } from "@modules/scale/discounts/dtos/discount.entity";

const request = scaleRequest();

export const useJustificationCreate = () => {
  const { discount } = useSelector((state: RootState) => state.scaleDiscount);

  const [pending, setPending] = useState<boolean>(false);
  const [form, setForm] = useState<IJustificationFormDto>({
    title: "",
    description: "",
    discounts: [],
  });

  const handleForm = ({ name, value }: IInputHandle) => {
    setForm((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const isValid = () => {
    return form.description && form.title && form.discounts.length;
  };

  const addDiscount = (discount: IDiscountEntity) => {
    setForm((prev) => ({
      ...prev,
      discounts: [...prev.discounts, discount],
    }));
  };

  const save = () => {
    return new Promise(async (resolve, reject) => {
      setPending(true);
      const payload = JSON.parse(JSON.stringify(form as any));
      payload.discountIds = [];
      payload.discounts = [];
      // assign discounts
      form.discounts.forEach((discount) =>
        payload.discountIds.push(discount.id)
      );
      // send
      await request
        .post(`justifications`, payload)
        .then((res) => {
          resolve(res.data);
          toast.success(`Los datos se guardaron correctamente!`);
        })
        .catch((err) => {
          const status = err?.response?.data?.status || "";
          reject(err);
          if (status) return alerStatus(status);
          return toast.error(`algo salió mal`);
        });
      setPending(false);
    });
  };

  useEffect(() => {
    if (discount.id) addDiscount(discount);
  }, [discount.id]);

  return {
    form,
    pending,
    handleForm,
    addDiscount,
    save,
    isValid,
  };
};
