import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { initialState, scaleJustificationActions } from "../store";
import { useDispatch } from "react-redux";

const request = scaleRequest();

export const useJustificationFind = () => {
  const dispatch = useDispatch();

  const [pending, setPending] = useState<boolean>(false);

  const fetch = async (id: number) => {
    setPending(true);
    // send
    await request
      .get(`justifications/${id}`)
      .then((res) =>
        dispatch(scaleJustificationActions.setJustification(res.data))
      )
      .catch(() =>
        dispatch(
          scaleJustificationActions.setJustification(initialState.justification)
        )
      );
    setPending(false);
  };

  return {
    pending,
    fetch,
  };
};
