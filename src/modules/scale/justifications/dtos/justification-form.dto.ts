import { IDiscountEntity } from "@modules/scale/discounts/dtos/discount.entity";

export interface IJustificationFormDto {
  title: string;
  description: string;
  discounts: IDiscountEntity[];
}
