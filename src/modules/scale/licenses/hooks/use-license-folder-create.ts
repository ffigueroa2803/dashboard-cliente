import { scaleRequest } from "@services/scale.request";
import { useState } from "react";
import { IFolderEntityInterface } from "src/domain/storage/folders/folder.entity.interface";
import { ILicenseEntity } from "../dtos/license.entity";

const request = scaleRequest();

export const useLicenseFolderCreate = (license: ILicenseEntity) => {
  const [pending, setPending] = useState<boolean>(false);

  let abortController: AbortController;

  const abort = () => abortController?.abort();

  const execute = () => {
    return new Promise<IFolderEntityInterface>((resolve, reject) => {
      setPending(true);
      abortController = new AbortController();
      request
        .post(
          `licenses/${license.id}/folders`,
          {},
          { signal: abortController.signal }
        )
        .then(({ data }) => resolve(data))
        .then(() => setPending(false))
        .catch(() => setPending(false))
        .catch((err) => reject(err));
    });
  };

  return { pending, execute, abort };
};
