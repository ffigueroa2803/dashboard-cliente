/* eslint-disable react-hooks/rules-of-hooks */
import { useState } from "react";
import { scaleRequest } from "@services/scale.request";
import { useSelector } from "react-redux";
import { RootState } from "@store/store";
import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";

const request = scaleRequest();

export const useLicenseDelete = () => {
  const { license } = useSelector((state: RootState) => state.license);

  const [pending, setPending] = useState<boolean>(false);
  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  const confirm = useConfirm();

  const destroy = async () => {
    return await confirm
      .alert({
        title: "Eliminar",
        message: `¿Estás seguro en eliminar la licencia?`,
        labelError: "Cancelar",
        labelSuccess: "Eliminar",
      })
      .then(async () => {
        setPending(true);
        setIsSuccess(false);
        await request
          .destroy(`licenses/${license?.id}`)
          .then(() => {
            setIsSuccess(true);
            toast.success(`La licencia se eliminó correctamente!!!`);
          })
          .catch(() => {
            toast.error(`No se pudo eliminar la licencia!!!`);
            setIsError(false);
          });
        setPending(false);
      })
      .catch(() => null);
  };

  return {
    pending,
    isSuccess,
    isError,
    destroy,
  };
};
