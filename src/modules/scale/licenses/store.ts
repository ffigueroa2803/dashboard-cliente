import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { ILicenseEntityInterface } from "src/domain/scale/licenses/license.entity.interface";
import { ILicenseEntity } from "./dtos/license.entity";

export interface LicenseState {
  licenses: ResponsePaginateDto<ILicenseEntity>;
  license: ILicenseEntity | ILicenseEntityInterface;
  option: string;
}

const initialState: LicenseState = {
  licenses: {
    items: [],
    meta: {
      totalItems: 0,
      totalPages: 0,
      itemsPerPage: 0,
    },
  },
  option: "",
  license: {} as any,
};

const licenseStore = createSlice({
  name: "escalafon@license",
  initialState,
  reducers: {
    paginate: (
      state: LicenseState,
      { payload }: PayloadAction<ResponsePaginateDto<ILicenseEntity>>
    ) => {
      state.licenses = payload as any;
    },
    setLicense: (
      state: LicenseState,
      { payload }: PayloadAction<ILicenseEntity>
    ) => {
      state.license = payload;
    },
    changeOption: (state: LicenseState, { payload }: PayloadAction<string>) => {
      state.option = payload;
    },
    updateItem: (
      state: LicenseState,
      { payload }: PayloadAction<ILicenseEntityInterface>
    ) => {
      state.licenses.items = state.licenses.items.map((item) => {
        if (item.id != payload.id) return item;
        return Object.assign({}, payload);
      });
      return state;
    },
  },
  extraReducers: {
    [HYDRATE](state, { payload }: PayloadAction<any>) {
      return { ...state, ...payload.license };
    },
  },
});

export const licenseReducer = licenseStore.reducer;

export const licenseActions = licenseStore.actions;
