import { IContractEntity } from "@modules/scale/contracts/dtos/contract.entity";
import { ITypeLicenseEntity } from "@modules/scale/type-licenses/dtos/type-license.entity";

export interface ILicenseEntity {
  id: number;
  contractId: number;
  typeLicenseId: number;
  resolution: string;
  dateOfResolution: string;
  dateOfAdmission: string;
  terminationDate: string;
  daysUsed: number;
  description: string;
  isPay: boolean;
  state: boolean;
  contract?: IContractEntity;
  typeLicense?: ITypeLicenseEntity;
}
