/* eslint-disable react-hooks/exhaustive-deps */
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { useEffect } from "react";
import { FolderPlus } from "react-feather";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { Button, Card } from "reactstrap";
import { LicenseEntity } from "src/domain/scale/licenses/license.entity";
import { ILicenseEntity } from "../dtos/license.entity";
import { useLicenseFolderCreate } from "../hooks/use-license-folder-create";
import { licenseActions } from "../store";

export interface ILicenseFolderCreateProps {
  license: ILicenseEntity;
}

export const LicenseFolderCreate = ({ license }: ILicenseFolderCreateProps) => {
  const dispatch = useDispatch();
  const licenseFolderCreate = useLicenseFolderCreate(license);

  const handle = () => {
    licenseFolderCreate
      .execute()
      .then((data) => {
        const newLicense = new LicenseEntity(license);
        newLicense.setFolder(data);
        dispatch(licenseActions.updateItem(newLicense));
      })
      .then(() => toast.success(`El folder se creo correctamente!`))
      .catch(() => toast.error(`No se pudo crear el folder`));
  };

  useEffect(() => {
    return () => licenseFolderCreate.abort();
  }, []);

  return (
    <Card className="mt-4">
      <h6 className="text-muted text-center">
        {/* crear */}
        <Show
          condition={licenseFolderCreate.pending}
          isDefault={
            <Button color="primary" outline onClick={handle}>
              <FolderPlus />
            </Button>
          }
        >
          Creando folder de vacaciones
          <div className="mt-3">
            <LoadingSimple loading />
          </div>
        </Show>
      </h6>
    </Card>
  );
};
