import React, { useState } from "react";
import { File } from "react-feather";
import { Button, Col, FormGroup, Input, Row } from "reactstrap";
import { scaleRequest } from "@services/scale.request";
import urljoin from "url-join";
import { DateTime } from "luxon";

const currentDate = DateTime.now().set({ day: 1 });
const request = scaleRequest();

export const LicenseReport = () => {
  const [dateOfAdmission, setDateOfAdmission] = useState<string>(
    currentDate.toSQLDate()
  );
  const [terminationDate, setTerminationDate] = useState<string>(
    currentDate.plus({ month: 1 }).toSQLDate()
  );

  const handleClick = (extname: string) => {
    const query = new URLSearchParams();
    query.set("dateOfAdmission", `${dateOfAdmission}`);
    query.set("terminationDate", `${terminationDate}`);
    const pathBase = urljoin(
      request.urlBase,
      `/licenses/reports/list.${extname}`
    );
    const url = `${pathBase}?${query.toString()}`;
    const a = document.createElement("a");
    a.href = url;
    a.target = "_blank";
    a.click();
  };

  const isCan = () => {
    return dateOfAdmission && terminationDate;
  };

  return (
    <Row>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Fecha de Inicio <b className="text-danger">*</b>
          </label>
          <Input
            type="date"
            value={dateOfAdmission}
            onChange={({ target }) => setDateOfAdmission(target.value)}
          />
        </FormGroup>
      </Col>
      <Col md="12">
        <FormGroup className="mb-3">
          <label htmlFor="">
            Fecha de Fin <b className="text-danger">*</b>
          </label>
          <Input
            type="date"
            value={terminationDate}
            onChange={({ target }) => setTerminationDate(target.value)}
          />
        </FormGroup>
      </Col>
      <Col md="6">
        <Button
          color="danger"
          block
          onClick={() => handleClick("pdf")}
          disabled={!isCan()}
        >
          <File className="icon" /> PDF
        </Button>
      </Col>
      <Col md="6">
        <Button
          color="success"
          block
          outline
          onClick={() => handleClick("xlsx")}
          disabled={!isCan()}
        >
          <File className="icon" /> Excel
        </Button>
      </Col>
    </Row>
  );
};
