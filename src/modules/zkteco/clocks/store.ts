import { ResponsePaginateDto } from "@common/dtos/response-paginate.dto";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";
import { IClockEntity } from "./dtos/clock.entity";

export interface ZktecoState {
  clocks: ResponsePaginateDto<IClockEntity>;
}

const initialState: ZktecoState = {
  clocks: {
    meta: {
      totalItems: 0,
      itemsPerPage: 30,
      totalPages: 0,
    },
    items: [],
  },
};

export const zktecoClockSlice = createSlice({
  name: "zkteco@clocks",
  initialState,
  reducers: {
    paginate: (
      state: ZktecoState,
      action: PayloadAction<ResponsePaginateDto<IClockEntity>>
    ) => {
      state.clocks = action.payload;
    },
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.zktecoClock };
    },
  },
});

export const zktecoClockReducer = zktecoClockSlice.reducer;

export const zktecoClockActions = zktecoClockSlice.actions;
