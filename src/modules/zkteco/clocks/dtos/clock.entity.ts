export interface IClockEntity {
  id: string;
  name: string;
  isOnline: boolean;
}
