/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { RootState } from "@store/store";
import { useSelector } from "react-redux";
import { useClockPaginate } from "../hooks/use-clock-paginate";
import { SelectBasic } from "@common/select/select-basic";
import { IClockEntity } from "../dtos/clock.entity";

interface IProps {
  // eslint-disable-next-line no-unused-vars
  onChange: (option: any) => void;
  value: any;
  name: string;
  isDisabled?: boolean;
  defaultQuerySearch?: string;
}

export const ClockSelect = ({
  name,
  value,
  defaultQuerySearch,
  isDisabled,
  onChange,
}: IProps) => {
  const { clocks } = useSelector((state: RootState) => state.zktecoClock);
  const zktecoList = useClockPaginate({
    page: 1,
    limit: 100,
    querySearch: defaultQuerySearch,
  });

  const settingsData = (row: IClockEntity) => {
    return {
      label: `${row.name}`.toLowerCase(),
      value: row.id,
      obj: row,
    };
  };

  useEffect(() => {
    zktecoList.fetch();
  }, [zktecoList.querySearch]);

  return (
    <SelectBasic
      name={name}
      isLoading={zktecoList.pending}
      isDisabled={isDisabled}
      value={value}
      options={clocks?.items?.map((row) => settingsData(row))}
      onChange={onChange}
      onSearch={zktecoList.setQuerySearch}
      placeholder="Seleccionar Reloj"
    />
  );
};
