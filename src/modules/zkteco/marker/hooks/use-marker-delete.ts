import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";
import { useDeleteMarkerMutation } from "../features/marker-rkt";

export function useMarkerDelete() {
  const confirm = useConfirm();
  const [fetch, { isLoading, isSuccess }] = useDeleteMarkerMutation();

  const handle = (id: string) => {
    confirm
      .alert({
        title: "Eliminar Marcación",
        message: "¿Estás seguro en eliminar la marcación",
        labelSuccess: "Confirmar",
        labelError: "Cancelar",
      })
      .then(() => {
        fetch(id)
          .unwrap()
          .then(() => toast.success(`Se eliminó la marcación del reloj ZKTECO`))
          .catch(() =>
            toast.error(`No se pudó eliminar la marcación del reloj ZKTECO`)
          );
      });
  };

  return {
    isLoading,
    isSuccess,
    handle,
  };
}
