import { useConfirm } from "@common/confirm/use-confirm";
import { toast } from "react-toastify";
import { useRestoreMarkerMutation } from "../features/marker-rkt";

export function useMarkerRestore() {
  const confirm = useConfirm();
  const [fetch, { isLoading, isSuccess }] = useRestoreMarkerMutation();

  const handle = (id: string) => {
    confirm
      .alert({
        title: "Restaurar Marcación",
        message: "¿Estás seguro en restaurar la marcación?",
        labelSuccess: "Confirmar",
        labelError: "Cancelar",
      })
      .then(() => {
        fetch(id)
          .unwrap()
          .then(() => toast.success(`Se restauró la marcación`))
          .catch(() => toast.error(`No se pudó restaurar la marcación`));
      });
  };

  return {
    isLoading,
    isSuccess,
    handle,
  };
}
