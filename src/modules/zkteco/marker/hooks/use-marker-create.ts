import { useConfirm } from "@common/confirm/use-confirm";
import { useFormik } from "formik";
import { toast } from "react-toastify";
import { useCreateMarkerMutation } from "../features/marker-rkt";
import { MarkerCreateInitial, MarkerCreateYup } from "../yup/marker-create.yup";

export const useMarkerCreate = () => {
  const confirm = useConfirm();
  const [fetch, { isLoading, isSuccess, isError }] = useCreateMarkerMutation();

  const formik = useFormik({
    initialValues: MarkerCreateInitial,
    validationSchema: MarkerCreateYup,
    onSubmit: (body, helpers) => confirm.alert({
      title: "Agregar Contrato",
      message: "¿Estás seguro en agregar el contrato al reloj ZKTECO?",
      labelSuccess: "Confirmar",
      labelError: "Cancelar",
    }).then(() => {
      fetch(body).unwrap().then(() => {
        helpers.resetForm();
        toast.success(`El trabajador se agregará al reloj ZKTECO`)
      }).catch(() => {
        toast.error(`No se pudó enviar el trabajador al reloj ZKTECO`);
      })
    })
  })

  return {
    isLoading,
    isError,
    isSuccess,
    submit: formik.handleSubmit,
    onChange: formik.handleChange,
    onBlur: formik.handleBlur,
    values: formik.values,
    isValid: formik.isValid,
    setValues: formik.setValues,
    errors: formik.errors
  };
};
