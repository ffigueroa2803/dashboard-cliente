export interface MarkerClockPaginateDto {
  token: string;
  filter: {
    page: number;
    limit: number;
  };
}
