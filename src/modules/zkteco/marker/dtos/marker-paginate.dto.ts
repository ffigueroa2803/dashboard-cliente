export interface MarkerPaginateDto {
  page: number;
  limit: number;
  token?: string;
}