export interface MarkerCreateDto {
  clockId: string;
  token: string;
  credentialNumber: number;
  permission: string;
}
