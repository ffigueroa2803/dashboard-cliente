import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PaginateEntity, paginateInitial } from "@services/entities/paginate.entity";
import { HYDRATE } from "next-redux-wrapper";
import { MarkerEntity } from "../domain/marker.entity";

export interface MarkerState {
  markerSelected: MarkerEntity | null;
  markerPaginate: PaginateEntity<MarkerEntity>;
}

const initialState: MarkerState = {
  markerSelected: null,
  markerPaginate: paginateInitial
};

export const markerSlice = createSlice({
  name: "zkteco@marker",
  initialState,
  reducers: {
    setMarkerSelected: (state, action: PayloadAction<MarkerEntity | null>) => {
      state.markerSelected = action.payload;
    },
    setMarkerPaginate: (state, action: PayloadAction<PaginateEntity<MarkerEntity>>) => {
      state.markerPaginate = action.payload;
    }
  },
  extraReducers: {
    [HYDRATE]: (state, { payload }: PayloadAction<any>) => {
      return { ...state, ...payload.marker };
    },
  },
});

export const markerReducer = markerSlice.reducer;

export const markerActions = markerSlice.actions;
