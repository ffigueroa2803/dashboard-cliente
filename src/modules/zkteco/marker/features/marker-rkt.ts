import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BaseHeaders } from "@services/base-request";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { MarkerEntity } from "../domain/marker.entity";
import { MarkerCreateDto } from "../dtos/marker-create.dto";
import { MarkerPaginateDto } from "../dtos/marker-paginate.dto";

const baseUrl = process.env.NEXT_PUBLIC_ZKTECO_URL || "";

export const markerRtk = createApi({
  reducerPath: "markerRtk",
  baseQuery: fetchBaseQuery({ baseUrl: baseUrl }),
  endpoints: (builder) => ({
    paginateMarker: builder.query<
      PaginateEntity<MarkerEntity>,
      MarkerPaginateDto
    >({
      query: (params) => ({
        url: `/markers`,
        method: "GET",
        headers: BaseHeaders,
        params,
      }),
    }),
    findMarker: builder.query<MarkerEntity, string>({
      query: (token) => ({
        url: `/markers/${token}`,
        method: "GET",
        headers: BaseHeaders,
      }),
    }),
    createMarker: builder.mutation<MarkerEntity, MarkerCreateDto>({
      query: (body) => ({
        url: `/markers`,
        method: "POST",
        body,
      }),
    }),
    restoreMarker: builder.mutation<boolean, string>({
      query: (id) => ({
        url: `/markers/${id}/restore`,
        method: "PUT",
        headers: BaseHeaders,
      }),
    }),
    deleteMarker: builder.mutation<boolean, string>({
      query: (id) => ({
        url: `/markers/${id}`,
        method: "DELETE",
        headers: BaseHeaders,
      }),
    }),
  }),
});

export const {
  useLazyFindMarkerQuery,
  useCreateMarkerMutation,
  useLazyPaginateMarkerQuery,
  useDeleteMarkerMutation,
  useRestoreMarkerMutation,
} = markerRtk;
