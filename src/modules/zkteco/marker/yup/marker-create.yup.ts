import { Yup } from "@common/yup";
import { MarkerCreateDto } from "../dtos/marker-create.dto";

export const MarkerCreateYup = Yup.object().shape({
  clockId: Yup.string().uuid().required(),
  token: Yup.string().required().min(1).max(100),
  credentialNumber: Yup.number().max(999999999).required(),
  permission: Yup.string().required(),
});

export const MarkerCreateInitial: MarkerCreateDto = {
  clockId: "",
  token: "",
  credentialNumber: 0,
  permission: "0"
}