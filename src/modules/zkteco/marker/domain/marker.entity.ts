import { IClockEntity } from "@modules/zkteco/clocks/dtos/clock.entity";

export interface MarkerEntity {
  id: string;
  credentialNumber: string;
  dateStart: string;
  dateOver: string;
  token: string;
  state: boolean;
  clock: IClockEntity;
}
