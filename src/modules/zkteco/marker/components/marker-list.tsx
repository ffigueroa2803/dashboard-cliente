import { LoadingSimple } from "@common/loading/components/loading-simple";
import { Show } from "@common/show";
import { PaginateEntity } from "@services/entities/paginate.entity";
import { ListGroup } from "reactstrap";
import { MarkerEntity } from "../domain/marker.entity";
import { MarkerItem } from "./marker-item";

export interface MarkerListProps {
  data: PaginateEntity<MarkerEntity>;
  isLoading: boolean;
  onRefresh(): void;
}

export function MarkerList({ data, isLoading, onRefresh }: MarkerListProps) {
  return (
    <ListGroup flush>
      {/* cargando */}
      {isLoading ? (
        <div className="mt-2 mb-3 text-center">
          <LoadingSimple loading />
        </div>
      ) : null}
      {/* no data */}
      <Show condition={!isLoading && !data?.items?.length}>
        <div className="text-center">No hay regístros disponibles</div>
      </Show>
      {/* lista */}
      {data?.items?.map((marker) => (
        <MarkerItem
          key={marker.id}
          data={marker}
          onDelete={onRefresh}
          onRestore={onRefresh}
        />
      ))}
    </ListGroup>
  );
}
