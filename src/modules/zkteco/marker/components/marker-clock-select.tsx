/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { SelectBasic } from "@common/select/select-basic";
import { RootState } from "@store/store";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { MarkerEntity } from "../domain/marker.entity";
import { useLazyPaginateMarkerQuery } from "../features/marker-rkt";

interface IProps {
  onChange: (option: any) => void;
  value: any;
  name: string;
  isDisabled?: boolean;
  token?: string;
  defaultQuerySearch?: string;
}

export const MarkerClockSelect = ({
  name,
  value,
  token,
  defaultQuerySearch,
  isDisabled,
  onChange,
}: IProps) => {
  const { markerSelected } = useSelector((state: RootState) => state.marker);
  const [querySearch, setQuerySearch] = useState<string>(
    defaultQuerySearch || ""
  );

  const [fetch, { isFetching, isLoading, data }] = useLazyPaginateMarkerQuery();

  const settingsData = (row: MarkerEntity) => {
    return {
      label: `${row.credentialNumber} - ${row.clock.name}`.toLowerCase(),
      value: row.id,
      obj: row,
    };
  };

  const handle = async () => {
    const filter = {
      token,
      page: 1,
      limit: 100,
    };
    fetch(filter).catch(() => null);
  };

  useEffect(() => {
    handle();
  }, [querySearch, token]);

  return (
    <SelectBasic
      name={name}
      isLoading={isFetching || isLoading}
      isDisabled={isDisabled}
      value={value}
      options={data?.items?.map((row) => settingsData(row)) || []}
      onChange={onChange}
      onSearch={(value) => setQuerySearch(value)}
      placeholder="Seleccionar Reloj"
    />
  );
};
