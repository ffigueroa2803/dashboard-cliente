/* eslint-disable react-hooks/exhaustive-deps */
import { Check } from "react-feather";
import { MarkerEntity } from "../domain/marker.entity";
import { useEffect } from "react";
import { LoadingSimple } from "@common/loading/components/loading-simple";
import { useMarkerRestore } from "../hooks/use-marker-restore";

interface MarkerRestoreButtonProps {
  data: MarkerEntity;
  onSuccess(): void;
}

export function MarkerRestoreButton({
  data,
  onSuccess,
}: MarkerRestoreButtonProps) {
  const { handle, isLoading, isSuccess } = useMarkerRestore();

  useEffect(() => {
    if (isSuccess) onSuccess();
  }, [isSuccess]);

  if (isLoading) {
    return (
      <div className="close">
        <LoadingSimple loading />
      </div>
    );
  }

  return (
    <Check
      className="icon close cursor-pointer"
      onClick={() => handle(data.id)}
    />
  );
}
