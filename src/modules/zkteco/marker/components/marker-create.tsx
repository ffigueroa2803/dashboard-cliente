/* eslint-disable react-hooks/exhaustive-deps */
import { ButtonLoading } from "@common/button/button-loading";
import { Show } from "@common/show";
import { ClockSelect } from "@modules/zkteco/clocks/components/clock-select";
import { InputDto } from "@services/dtos";
import { useEffect } from "react";
import { Save } from "react-feather";
import {
  Button,
  Form,
  FormGroup,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { MarkerCreateDto } from "../dtos/marker-create.dto";
import { useMarkerCreate } from "../hooks/use-marker-create";

export interface MarkerCreateProps {
  isOpen: boolean;
  onClose(): void;
  onSave(): void;
  valueInitial?: MarkerCreateDto;
}

export function MarkerCreate({
  valueInitial,
  isOpen,
  onClose,
  onSave,
}: MarkerCreateProps) {
  const { onChange, isLoading, values, setValues, submit, isSuccess } =
    useMarkerCreate();

  const handleInt = (target: InputDto) => {
    target.value = parseInt(target.value);
    onChange({ target });
  };

  useEffect(() => {
    if (valueInitial) setValues(valueInitial);
  }, [valueInitial]);

  useEffect(() => {
    if (isSuccess) onSave();
  }, [isSuccess]);

  return (
    <Modal isOpen={isOpen}>
      <Form onSubmit={submit}>
        <ModalHeader toggle={!isLoading ? onClose : undefined}>
          Crear Agente
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <label>
              Reloj <b className="text-danger">*</b>
            </label>
            <ClockSelect
              name="clockId"
              onChange={(target) => onChange({ target })}
              value={values?.clockId}
              isDisabled={isLoading}
            />
          </FormGroup>

          <FormGroup>
            <label>
              N° Credencial <b className="text-danger">*</b>
            </label>
            <Input
              type="number"
              name="credentialNumber"
              value={values?.credentialNumber || ""}
              onChange={({ target }) => handleInt(target)}
              disabled={isLoading}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Show
            condition={!isLoading}
            isDefault={
              <ButtonLoading loading title="Creando" color="primary" />
            }
          >
            <Button color="primary" type="submit">
              <Save className="icon" />
            </Button>
          </Show>
        </ModalFooter>
      </Form>
    </Modal>
  );
}
