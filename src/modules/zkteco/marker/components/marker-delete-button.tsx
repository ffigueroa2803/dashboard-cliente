/* eslint-disable react-hooks/exhaustive-deps */
import { X } from "react-feather";
import { MarkerEntity } from "../domain/marker.entity";
import { useMarkerDelete } from "../hooks/use-marker-delete";
import { useEffect } from "react";
import { LoadingSimple } from "@common/loading/components/loading-simple";

interface MarkerDeleteButtonProps {
  data: MarkerEntity;
  onSuccess(): void;
}

export function MarkerDeleteButton({
  data,
  onSuccess,
}: MarkerDeleteButtonProps) {
  const { handle, isLoading, isSuccess } = useMarkerDelete();

  useEffect(() => {
    if (isSuccess) onSuccess();
  }, [isSuccess]);

  if (isLoading) {
    return (
      <div className="close">
        <LoadingSimple loading />
      </div>
    );
  }

  return (
    <X className="icon close cursor-pointer" onClick={() => handle(data.id)} />
  );
}
