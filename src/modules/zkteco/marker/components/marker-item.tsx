/* eslint-disable react-hooks/exhaustive-deps */
import { Show } from "@common/show";
import { ArrowRight } from "react-feather";
import { ListGroupItem } from "reactstrap";
import { MarkerEntity } from "../domain/marker.entity";
import { MarkerDeleteButton } from "./marker-delete-button";
import { MarkerRestoreButton } from "./marker-restore-button";

export interface MarkerItemProps {
  data: MarkerEntity;
  onDelete(): void;
  onRestore(): void;
}

export function MarkerItem({ data, onDelete, onRestore }: MarkerItemProps) {
  return (
    <ListGroupItem>
      {data.clock?.name}
      <ArrowRight className="icon mr-2" />
      <span className="badge badge-light">{data.credentialNumber}</span>
      <span className={`badge badge-${data.state ? "success" : "danger"}`}>
        {data.state ? "Activo" : "Inactivo"}
      </span>
      <Show
        condition={data.state}
        isDefault={<MarkerRestoreButton data={data} onSuccess={onRestore} />}
      >
        <MarkerDeleteButton data={data} onSuccess={onDelete} />
      </Show>
    </ListGroupItem>
  );
}
