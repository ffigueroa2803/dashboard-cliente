import { ArrowRight } from "react-feather";
import { Button, FormGroup, Input } from "reactstrap";
import { useLazyFindMarkerQuery } from "../features/marker-rkt";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { markerActions } from "../features/marker-slice";
import { toast } from "react-toastify";

export interface MarkerSearchByTokenProps {
  onSuccess: () => void;
}

export function MarkerSearchByToken({ onSuccess }: MarkerSearchByTokenProps) {
  const dispatch = useDispatch();
  const [token, setToken] = useState<string>("");
  const [fetch] = useLazyFindMarkerQuery();

  const handleFetch = () => {
    fetch(token)
      .unwrap()
      .then((data) => {
        dispatch(markerActions.setMarkerSelected(data));
        onSuccess();
      })
      .catch(() => toast.error(`No se encontró el ID Marcación`));
  };

  return (
    <div>
      <FormGroup>
        <label>ID Marcación</label>
        <Input
          value={token}
          onChange={({ target }) => setToken(target.value)}
        />
      </FormGroup>
      <FormGroup className="text-right">
        <Button color="primary" onClick={handleFetch}>
          <ArrowRight className="icon" />
        </Button>
      </FormGroup>
    </div>
  );
}
