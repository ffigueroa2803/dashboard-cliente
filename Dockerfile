# dependencies
FROM node:14.19-alpine AS deps

WORKDIR /app
COPY package*.json ./
RUN npm ci

# builder
FROM node:14.19-alpine AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

ARG NEXT_PUBLIC_URL
ARG NEXT_PUBLIC_AUTH_URL
ARG NEXT_PUBLIC_SCALE_URL
ARG NEXT_PUBLIC_PLANILLA_URL
ARG NEXT_PUBLIC_KEY
ARG NEXT_PUBLIC_CLIENT_ID
ARG NEXT_PUBLIC_CLIENT_SECRET

RUN npm run build

# runner
FROM node:14.19-alpine AS runner
WORKDIR /app

ENV NODE_ENV production

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules

COPY --from=builder --chown=nextjs:nodejs /app/.next/ ./.next
COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

COPY --from=builder /app/.next ./.next
COPY --from=builder /app/.next/static ./.next/static

USER nextjs

EXPOSE 3000

CMD ["npm", "start"]
